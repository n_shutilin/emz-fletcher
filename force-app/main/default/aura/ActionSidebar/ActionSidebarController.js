({
    toggleDock: function(component, event, helper) {
        const dockIsOpen = component.get('v.dockIsOpen');
        component.set('v.dockIsOpen', !dockIsOpen);
    },

    doInit: function (component, event, helper) {
        component.set('v.showSidebar', false);
        component.set('v.dockIsOpen', false);

        const getParamFunc = function getJsonFromUrl(url) {
            if(!url) url = location.search;
            var query = url.substr(1);
            var result = {};
            query.split("&").forEach(function(part) {
                var item = part.split("=");
                result[item[0]] = decodeURIComponent(item[1]);
            });
            return result;
        };

        const teamId = getParamFunc(location.search)["c__team_id"];
        let processName = getParamFunc(location.search)["c__action_process_name"];
        let subProcessName = getParamFunc(location.search)["c__action_sub_process_name"];
        let date = getParamFunc(location.search)["c__date"];
        let endDate = getParamFunc(location.search)["c__end_date"];
        if (processName && subProcessName && date) {
            processName = decodeURI(processName);
            subProcessName = decodeURI(subProcessName);
            helper.getActionParentRecords(component, processName, subProcessName, teamId, date, endDate);
            component.set('v.showSidebar', true);
            component.set('v.dockIsOpen', false);
            component.set('v.processName', processName);
            component.set('v.subProcessName', subProcessName);
            component.set('v.teamId', teamId);
            component.set('v.date', $A.localizationService.formatDate(new Date(date), "YYYY-MM-DD"));
            component.set('v.endDate', $A.localizationService.formatDate(new Date(endDate), "YYYY-MM-DD"));
        }
    },

    dateChangeHandler: function (component, event, helper) {
        const processName = component.get('v.processName');
        const subProcessName = component.get('v.subProcessName');
        const teamId = component.get('v.teamId');
        let date = component.find("datapicker").get("v.value");
        let endDate = component.find("todatapicker").get("v.value");
        component.set('v.date', date);
        helper.getActionParentRecords(component, processName, subProcessName, teamId, date, endDate);
    },

    openNextRecord: function (component, event, helper) {
        helper.redirect(component, 1);
    },

    openPreviousRecord: function (component, event, helper) {
        helper.redirect(component, -1);
    }
});