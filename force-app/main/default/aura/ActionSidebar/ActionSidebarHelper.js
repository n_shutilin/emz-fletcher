({
    getActionParentRecords: function (component, processName, subProcessName, teamId, date, endDate) {
        component.set('v.isProcess', true);
        component.set('v.parentRecords', []);
        const action = component.get('c.getParentsInfo');
        action.setParams({
            recordId: component.get('v.recordId'),
            processName: processName,
            subProcessName: subProcessName,
            teamId: teamId,
            dateForSearch: date,
            endDateString: endDate
        });
        action.setCallback(this, function (response) {
            const state = response.getState();
            if (state === 'SUCCESS') {
                const result = response.getReturnValue();
                if (result) {
                    console.log(result);
                    component.set('v.actionParentRecords', result);
                }
            } else if (state === 'ERROR') {
                console.log(response.getError());
            }
            component.set('v.isProcess', false);
        });
        $A.enqueueAction(action);
    },

    redirect: function (component, direction) {
        const actionParentRecords = component.get('v.actionParentRecords');
        const recordId = component.get('v.recordId');
        if(actionParentRecords.length){
            let currentRecord = actionParentRecords.findIndex(record => record.isCurrentRecord);
            if(currentRecord < 0) {
                for(let i = 0; i < actionParentRecords.length; i++){
                    if(actionParentRecords[i].convertedIdList.includes(recordId)){
                        currentRecord = i; 
                    }
                }
            }
            let nextIndex = (currentRecord + direction) < 0 ? actionParentRecords.length - 1 : currentRecord + direction ;
            const eUrl= $A.get('e.force:navigateToURL');
            eUrl.setParams({
                'url': actionParentRecords[nextIndex % actionParentRecords.length].link
            });
            eUrl.fire();
        }
    }
});