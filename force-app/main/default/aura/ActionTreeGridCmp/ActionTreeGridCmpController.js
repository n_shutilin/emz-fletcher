({
    doInit: function (component, event, helper) {
        component.set('v.isProcess', true);
        helper.settingColumns(component, event, helper);
        helper.getTeams(component, event, helper);
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.startDate', today);
        component.set('v.endDate', today);
        component.find("select").set('v.value','');
        helper.getDataToDisplay(component, event, helper);
    },

    dateChangeHandler: function (component, event, helper) {
        component.set('v.isProcess', true);
        helper.getDataToDisplay(component, event, helper);
    },  

    teamChangeHandler: function (component, event, helper) {
        component.set('v.isProcess', true);
        helper.getDataToDisplay(component, event, helper);
    },
});