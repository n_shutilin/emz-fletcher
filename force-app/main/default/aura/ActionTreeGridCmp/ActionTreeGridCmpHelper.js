({
    settingColumns: function (component, event, helper) {
        var columns = [
            {
                type: 'url',
                fieldName: 'linkToParentRecords',
                label: $A.get("$Label.c.ActionTreeGrid_Task_List"),
                initialWidth: 500,
                typeAttributes: {
                    label: {fieldName: 'name'},
                    target: '_blank'
                }
            },
            {
                type: 'number',
                fieldName: 'countWithOpenStatus',
                label: $A.get("$Label.c.ActionTreeGrid_To_Be_Made"),
                cellAttributes: {alignment: 'center'}
            },
            {
                type: 'number',
                fieldName: 'countWithAttemptStatus',
                label: $A.get("$Label.c.ActionTreeGrid_Attempt"),
                cellAttributes: {alignment: 'center'}
            },
            {
                type: 'number',
                fieldName: 'countWithCompletedStatus',
                label: $A.get("$Label.c.ActionTreeGrid_Completed"),
                cellAttributes: {alignment: 'center'}
            },
            {
                type: 'number',
                fieldName: 'total',
                label: $A.get("$Label.c.ActionTreeGrid_Total"),
                cellAttributes: {alignment: 'center'}
            }
        ];
        component.set('v.gridColumns', columns);
    },

    getDataToDisplay: function (component, event, helper) {
        var action = component.get("c.getActionsDataToDisplay");
        action.setParams({
            teamId: component.find("select").get("v.value"),
            startDate: component.find("datapicker").get("v.value"),
            endDate: component.find("todatapicker").get("v.value")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.gridExpandedRows', []);
                if (response.getReturnValue()) {
                    var result = response.getReturnValue();
                    result.forEach(function recurse(item) {
                        if (item.children) {
                            item.children.forEach(recurse);
                            item._children = item.children;
                            delete item.children;
                        }
                    });
                    component.set('v.gridData', result);
                    console.log(JSON.stringify(result));
                } else {
                    component.set('v.gridData', []);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set('v.isProcess', false);
        });
        $A.enqueueAction(action);
    },

    getTeams: function (component, event, helper) {
        var action = component.get("c.getTeamsForShowing");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.teams', response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set('v.isProcess', false);
        });
        $A.enqueueAction(action);
    },
});