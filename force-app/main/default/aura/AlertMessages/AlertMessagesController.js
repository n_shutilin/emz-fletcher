({
    init : function (component, event, helper) {
        helper.doInit(component);
    },

    openModel : function (component, event, helper) {
        component.set('v.isModelOpen', true);
    },

    checkInput : function (component, event, helper) {
        var alertMessage = component.get('v.newAlertMessage');
        if (!(alertMessage && alertMessage.trim())) {
            return;
        } else {
            component.set('v.modelSpinner', true);
            helper.createAlert(component);
        }
    },

    removeAlert : function (component, event, helper) {
        event.preventDefault();
        try {
            component.set('v.isSpinnerShown', true);
            var alertId = event.currentTarget.dataset.id;
            var index = event.currentTarget.dataset.index;
            helper.deleteAlert(component, alertId, index);
        } catch (e) {
           console.error(e);
        }

    },

    closeModel : function (component, event, helper) {
        component.set('v.isModelOpen', false);
    }
})