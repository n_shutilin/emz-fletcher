({
    doInit : function (component) {
        var action = component.get("c.getWrapper");
        action.setParams({
            'recordId': component.get('v.recordId')
        });
        action.setCallback(this, function (response) {
            component.set('v.isSpinnerShown', false);
            var messages = response.getReturnValue();
            if (response.getState() === "SUCCESS") {
                component.set('v.newWrapper', messages);
            } else {
                console.log(response.getState());
            }
        });
        $A.enqueueAction(action);
    },

    createAlert : function (component) {
        var action = component.get('c.createAlert');
        action.setParams({
            "recordId" : component.get('v.recordId'),
            "alertMessage" : component.get('v.newAlertMessage')
        });
        action.setCallback(this, function (response) {
            component.set('v.modelSpinner', false);
            try {
                component.set('v.isSpinnerShown', false);
                if (response.getState() === "SUCCESS") {
                    var value = response.getReturnValue();
                    if (value === null) {
                        console.log('error');
                        this.showToastEvent(component, 'Error!', 'error', 'error');
                    } else {
                        console.log(value);
                        var wrapper = component.get('v.newWrapper');
                        wrapper.messages.push(value);
                        component.set('v.newWrapper', wrapper);
                        component.set('v.newAlertMessage', '');
                        component.set('v.isModelOpen', false);
                        this.showToastEvent(component, 'Success!', 'New Alert created.', 'success');
                    }
                }
            } catch (e) {
                console.error(e);
            }

        });
        $A.enqueueAction(action);
    },
    
    deleteAlert : function (component, alertId, index) {
        var action = component.get('c.deleteAlert');
        action.setParams({
            "alertId" : alertId
        });
        action.setCallback(this, function (response) {
            component.set('v.isSpinnerShown', false);
            if (response.getState() === "SUCCESS") {
                var wrapper = component.get('v.newWrapper');
                wrapper.messages.splice(index, 1);
                component.set('v.newWrapper', wrapper);
                this.showToastEvent(component, 'Success!', 'Alert deleted.', 'success');
            } else {
                this.showToastEvent(component, 'Error!', 'error', 'error');
            }
        });
        $A.enqueueAction(action);
    },

    showToastEvent : function (component, title, message, type) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            "title" : title,
            "message" : message,
            "type" : type
        });
        toastEvent.fire();
    }
})