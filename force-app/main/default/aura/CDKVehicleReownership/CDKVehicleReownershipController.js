({
    doInit : function(component,event, helper) {
        var action = component.get("c.sendVehicleReownershipRequest");
        action.setParams({
            'recordId' : component.get('v.recordId')
        });
        action.setCallback(this, function (response) {
            helper.showToast(response.getReturnValue());
            $A.get('e.force:refreshView').fire();
            $A.get("e.force:closeQuickAction").fire();
        });
        $A.enqueueAction(action);
    },

    
})