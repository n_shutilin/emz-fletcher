({
    showToast : function(response) {debugger;
        var type = response == 'OK' ? 'success' : 'error';
        var title = response == 'OK' ? 'Success!' : 'Error!';
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": response,
            "mode": "sticky"
        });
        toastEvent.fire();
    }
})