({
    init : function(component, event, helper) {
        console.log('ACTIVITY INIT => 1')
        component.set('v.showSpinner', true);
        component.set('v.subscription', null);
        component.set('v.notifications', []);
        component.set('v.columns', [
            {label: '', initialWidth: 30, hideDefaultActions : 'true', fieldName : '', cellAttributes: { class : {fieldName : 'iconType'}} },
            {label: 'Date', initialWidth: 160, fieldName: 'recordId', type: 'url', sortable : true,
            typeAttributes: {label:{fieldName: 'activityDate'}, target:'_blank'}},
            {label: 'Activity Type', initialWidth: 150, fieldName: 'activityType', type: 'text', sortable : true},
            {label: 'Performed By', initialWidth: 200, fieldName: 'performedById', type: 'url', sortable : true, 
            typeAttributes: {label:{fieldName: 'performedBy'}, target:'_blank'}},
            {label: 'Dept', initialWidth: 150, fieldName: 'department', type: 'text', sortable : true},
            {label: 'Description', fieldName: 'description', type: 'text', wrapText: true}
        ]);
        console.log('ACTIVITY INIT => 2')
        helper.doInit(component);
        component.set('v.showSpinner', false);
        helper.subscribe(component, helper);
        console.log('ACTIVITY INIT => 3')
    },

    handleSort : function(component, event, helper) {
        var sortBy = event.getParam("fieldName");
        var sortDirection = event.getParam("sortDirection");
        component.set("v.sortBy",sortBy);
        component.set("v.sortDirection",sortDirection);
        helper.sortData(component, sortBy, sortDirection);
    },

    // deptChange : function(component, event, helper) {
    //     var dept = component.find('deptFilter').get('v.value');
    //     var startDate = component.get('v.startDate');
    //     var endDate = component.get('v.endDate');
    //     if (dept) {
    //         tasks.forEach(element => {
    //             if (element.department === dept) {
    //                 tempList.push(element);
    //             }
    //         });
    //         component.set('v.tasks', tempList);
    //     } else {
    //         if (!startDate && !endDate) {
    //             var alltasks = component.get('v.allTasks');
    //             component.set('v.tasks', alltasks);
    //         }
    //     }
    // }, 
    
    dateChange : function(component, event, helper) {
        var startDate = component.get('v.startDate');
        var endDate = component.get('v.endDate');

        var tasks = component.get('v.allTasks');
        var tempList = [];
        if (tasks.length) {
            if (startDate || endDate) {
                if (startDate) {
                    startDate = new Date(startDate);
                }
                if (endDate) {
                    endDate = new Date(endDate);
                }
                
                if (startDate && endDate) {
                    tasks.forEach(element => {
                        if (element.filterDate >= startDate && element.filterDate <= endDate) {
                            tempList.push(element);
                        }
                    });
                }
                if (startDate && !endDate) {
                    var day = startDate.getDate();
                    var month = startDate.getMonth();
                    var year = startDate.getFullYear();
                    tasks.forEach(element => {
                        console.log(startDate);
                        console.log(element.filterDate);
                        if (element.filterDate.getDate() === day && element.filterDate.getMonth() === month && element.filterDate.getFullYear() === year) {
                            tempList.push(element);
                        }
                    });
                }
                if (!startDate && endDate) {
                    tasks.forEach(element => {
                        if (element.filterDate <= endDate) {
                            tempList.push(element);
                        }
                    });
                }
                tempList = helper.deptChange(component, tempList);
            } else {
                tempList = helper.deptChange(component, tasks);
            }
            component.set('v.tasks', tempList);
        }
    }
})