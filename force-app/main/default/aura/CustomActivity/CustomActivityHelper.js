({
    doInit : function (component) {
        var action = component.get('c.getTasks');
        action.setParams({
            'recordId' : component.get('v.recordId')
        });

        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                var tasks = response.getReturnValue();
                console.log('ACTIVITY HISTORY => ' + JSON.stringify(tasks))
                try{
                    tasks.forEach(element => {
                        let date;
                        if(element.activityDate) {
                            date = element.activityDate.slice(0, 10).split('/');
                            if (date[0].length == 1) {
                                date[0] = '0' + date[0]; 
                            }
                            if (date[1].length == 1) {
                                date[1] = '0' + date[1]; 
                            }
                            if (date[2].length > 4) {
                                date[2] = date[2].slice(0, 4); 
                            }
                            element.filterDate = new Date(date.join('/'));
                        } else {
                            element.filterDate = null;
                        }
                        
                    });
                } catch (e){
                    console.log('ACTIVITY HISTORY ERROR => ' + JSON.stringify(e))
                    console.error(e);
                }
                
                component.set('v.tasks', tasks);
                component.set('v.allTasks', tasks);

                var sortBy = 'recordId';
                var sortDirection = 'desc';
                component.set("v.sortBy",sortBy);
                component.set("v.sortDirection",sortDirection);
                this.sortData(component, sortBy, sortDirection);

                if (!tasks.length) {
                    component.find('card').set('v.footer', 'No activity history records.');
                }
                var filter = component.get('c.dateChange');
                
                $A.enqueueAction(filter);
            } else {
                console.log('ACTIVITY HISTORY ERROR => ' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },

    sortData : function(component,fieldName,sortDirection){
        debugger;
        let data = component.get("v.tasks");
        let reverse = sortDirection == 'asc' ? 1: -1;
        let key = function(a) { return a[fieldName]; }
        if (fieldName === 'recordId') {
            fieldName = 'filterDate';
            data.sort(function(a,b){
                if (key(a) && key(b)) {
                    return reverse * (key(a).getTime() - key(b).getTime())
                }
                if (!key(a)) {
                    return reverse * -1
                }
                if (!key(b)) {
                    return reverse * 1
                }
            });
        } else {
            if (fieldName === 'performedById') {
                fieldName = 'performedBy';
            }
            
            data.sort(function(a,b){ 
                var a = key(a) ? key(a).toLowerCase() : '';
                var b = key(b) ? key(b).toLowerCase() : '';
                return reverse * (a-b);
            }); 
        }
           
        component.set("v.tasks",data);
        component.set("v.allTasks",data);
    },

    subscribe : function (component, helper) {
        const empApi = component.find('empApi');
        const channel = component.get('v.channel');
        const replayId = -1;
        const callback = function (message) {
            var relatedIds = message.data.payload.Related_To__c;
            if (relatedIds.includes(component.get('v.recordId'))) {
                helper.doInit(component);
            }
        };
        empApi.subscribe(channel, replayId, $A.getCallback(callback)).then($A.getCallback(function (newSubscription) {
            component.set('v.subscription', newSubscription);
        }));
    },
    
    deptChange : function(component, tasks) {
        var dept = component.find('deptFilter').get('v.value');
        var tempList = [];
        if (dept) {
                tasks.forEach(element => {
                    if (element.department === dept) {
                        tempList.push(element);
                    }
                });
                return tempList;
        }
        return tasks;
    }   
})