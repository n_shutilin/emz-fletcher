({
	init : function (component, event, helper) {
		component.set('v.showSpinner', true);
		let action = component.get('c.getTrade');
        action.setParams({
            'recordId': component.get("v.recordId")
        }); 
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                let result = response.getReturnValue();
				component.set("v.trade", result);
            } else {
                helper.showToast('Error!', response.getError(), 'Error');
            }
			component.set('v.showSpinner', false);
        });

        $A.enqueueAction(action);
	},

	sendToInventary : function (component, event, helper) {
		component.set('v.showSpinner', true);
		let action = component.get('c.sendTrade');
        action.setParams({
            'recordId': component.get("v.recordId")
        }); 
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
				helper.showToast('Success!', 'Trade successfuly posting.', 'Success');
            } else {
                helper.showToast('Error!', response.getError(), 'Error');
            }
			component.set('v.showSpinner', false);
			$A.get('e.force:refreshView').fire();
        });

        $A.enqueueAction(action);
	},

	retrieveFromInventary : function (component, event, helper) {
		component.set('v.showSpinner', true);
		let action = component.get('c.retrieveTrade');
        action.setParams({
            'recordId': component.get("v.recordId")
        }); 
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                helper.showToast('Success!', 'Trade successfuly retrieving.', 'Success');
            } else {
                helper.showToast('Error!', response.getError(), 'Error');
            }
			component.set('v.showSpinner', false);
			$A.get('e.force:refreshView').fire();
        });

        $A.enqueueAction(action);
	}

})