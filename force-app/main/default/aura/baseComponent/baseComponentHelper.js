({
    /*
        {
            "variant": error, // string
            "message": message, // string
            "errors": errors // list
        }
     */
    showToast: function (childCmp, toastParams) {
        console.log(toastParams);
        let toastData = {
            title: '',
            message: 'Unknown error',
            variant: 'error'
        };
        if (toastParams.variant) {
            toastData.variant = toastParams.variant;
        }

        let message;
        let title;

        if (toastData.variant === 'error') {
            let errors = toastParams.errors;
            if (errors && Array.isArray(errors) && errors.length > 0) {
                let error = errors[0];

                if (error.isUserDefinedException) {
                    let serverResponse = JSON.parse(error.message);
                    if (serverResponse && serverResponse.data) {
                        message = serverResponse.data;
                    } else if (serverResponse && serverResponse.message) {
                        title = serverResponse.message;
                        message = JSON.stringify(serverResponse.errors);
                    }
                } else {
                    message = error.message;
                }

            }
        } else {
            message = toastParams.message;
        }
        if (toastData.variant === 'error' && toastParams.message) {
            message = toastParams.message;
        }
        if (message) {
            // curly braces are not applicable in toast message
            let regex = new RegExp('[{}]', 'g');
            message = message.replace(regex, '');
            toastData.message = message;
        }

        if (title) {
            toastData.title = title;
        }

        console.log(toastData);
        childCmp.getSuper().find('notifLib').showToast(toastData);
    },

    spinnerShow: function (childCmp) {
        childCmp.getSuper().set("v.isSpinnerShown", true);
    },

    spinnerHide: function (childCmp) {
        childCmp.getSuper().set("v.isSpinnerShown", false);
    }
});