({
    doInit : function(component, event, helper) {
        var action = component.get("c.updateEmailMessage");
        action.setParams({
            'recordId' : component.get('v.recordId')
        });
        action.setCallback(this, function (response) {
            $A.get('e.force:refreshView').fire();
            $A.get("e.force:closeQuickAction").fire();
        });
        $A.enqueueAction(action);

    }
})