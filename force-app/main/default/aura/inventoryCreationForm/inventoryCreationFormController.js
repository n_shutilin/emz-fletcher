({
    handleInit: function (component, event, helper) {
        helper.getInventoryFields(component, event);
    },

    handleInventoryCreate: function (component, event, helper) {

    },

    handleInventoryInsertError: function (component, event, helper) {
        console.log('~');
    },

    handleModalCancel: function (component, event, helper) {

    }
});