({
    getInventoryFields: function (component, event) {
        const that = this;
        const action = component.get('c.getInventoryFields');
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                component.set('v.inventoryFields', response.getReturnValue());
                component.set('v.showInventoryCreationForm', true);
            } else {
                that.showError(response.getError());
            }
        });
        $A.enqueueAction(action);
    }
});