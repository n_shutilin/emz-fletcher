({
    handleInit: function (component, event, helper) {
        helper.getSearchParams(component, event);
        helper.setTableHeaders(component, event);
    },

    formPress: function(component, event, helper) {
        if (event.keyCode === 13) {
			helper.search(component, event);
		}
    },

    handleMakeChange: function (component, event, helper) {
        helper.getModelOptions(component, event);
    },

    handlePageClick: function (component, event, helper) {
        helper.handlePageClick(component, event);
    }, 

    handleSearch: function (component, event, helper) {
        helper.search(component, event);
    },

    handleSelect: function (component, event, helper) {
        helper.handleSelect(component, event);
    },

    handleClearSelection: function (component, event, helper) {
        helper.handleClearSelection(component, event);
    },

    handleClear: function (component, event, helper) {
        helper.getSearchParams(component, event);
        component.set('v.inventories', []);
    }
});