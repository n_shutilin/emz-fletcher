({
    getSearchParams: function (component, event) {
        const that = this;
        const action = component.get('c.getSearchParams');
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                component.set('v.searchParams', JSON.parse(response.getReturnValue()));
                console.log(JSON.parse(response.getReturnValue()));
            } else {
                that.showError(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    setTableHeaders: function(component, event) {
        component.set('v.columns', [{label: 'Stock Number', fieldName: 'stockNumber', type: 'text'},
            {label: 'Type', fieldName: 'type', type: 'text'},
            {label: 'Year', fieldName: 'year', type: 'text'},
            {label: 'Make', fieldName: 'make', type: 'text'},
            {label: 'Model', fieldName: 'model', type: 'text'},
            {label: 'Trim', fieldName: 'trim', type: 'text'},
            {label: 'Body Type', fieldName: 'body', type: 'text'},
            {label: 'Drive', fieldName: 'drive', type: 'number'},
            {label: 'Color', fieldName: 'color', type: 'text'},
            {label: 'VIN', fieldName: 'vin', type: 'text'},
            {label: 'Mileage', fieldName: 'mileage', type: 'number'},
            {label: 'List Price', fieldName: 'price', type: 'number'},
            {label: 'Days In Stock', fieldName: 'daysInStock', type: 'number'},
            {label: 'Status', fieldName: 'status', type: 'text'}]);
    },

    getModelOptions: function(component, event) {
        const that = this;
        const action = component.get('c.getModelOptionsByMake');
        action.setParams({makeId: component.get('v.searchParams.selectedMake')});
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                component.set('v.searchParams.modelOptions', response.getReturnValue());
            } else {
                that.showError(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    search: function(component, event) {
        const that = this;
        let yearFrom = component.get('v.searchParams.selectedYear');
        let yearTo = component.get('v.searchParams.selectedYearTo');
        if(+yearTo >= +yearFrom) {
            component.set('v.pagination', false);
            component.set('v.isSearching', true);
            const action = component.get('c.search');
            action.setParams({searchParamsJson: JSON.stringify(component.get('v.searchParams')), recordId: component.get('v.recordId')});
            action.setCallback(this, function (response) {
                if (response.getState() === 'SUCCESS') {
                    let result = JSON.parse(response.getReturnValue());
                    component.set('v.isSearching', false);
                    component.set('v.inventories', result.invenotyList);
                    if(result.numberOfRecords > 100) {
                        that.handlePages(result.numberOfRecords, component);
                    }
                } else {
                    that.showError(response.getError());
                }
            });
            $A.enqueueAction(action);
        } else {
            this.showToast('The start year must be higher or equal to the end year.','error');
        }

    }, 

    handlePages: function(numberOfRecords, component) {
        component.set('v.pagination', true);
        let numberOfPages =  Math.trunc(numberOfRecords/100) + 1;
        let pages = [];
        for(let i = 0; i<numberOfPages; i++) {
            pages.push({
                number: i+1,
                id:'pageN'+(i+1) 
            });
        }
        component.set('v.pages', pages);
    },

    handlePageClick: function(component, event) {
        console.log(event.getSource().get("v.name"));
    },

    handleClearSelection: function(component, event) {
        console.log(JSON.stringify(component.get('v.selectedRows')));
        component.set('v.isNoSelection', true);
        component.set('v.selectedRows', []);
        let inventories = component.get('v.inventories');
        for (let i = 0; i < inventories.length; i++) {
            inventories[i].selected = false;
        }
    },

    handleSelect: function(component, event) {
        component.set('v.isNoSelection', false);
        let inventories = component.get('v.inventories');
        const selectedRows = event.getParam("selectedRows");  
        let prevRow = component.get('v.selectedInventoryId');  
        let selectedRow;
        if (selectedRows && selectedRows.length) {
            selectedRow = selectedRows.length > 1 
                        ? selectedRows.find(x => x.inventoryId !== prevRow).inventoryId 
                        : selectedRows[0].inventoryId;
        } else {
            component.set('v.isNoSelection', true);
            return;
        }
        component.set('v.selectedInventoryId', selectedRow);
        for (let i = 0; i < inventories.length; i++) {
            if (inventories[i].inventoryId === selectedRow) {
                inventories[i].selected = true;
            } else {
                inventories[i].selected = false;
            }
        }
        component.set('v.selectedRows', [selectedRow]);
        component.set('v.inventories', inventories);
    },

    showError: function(errorList) {
        if (errorList.length > 0 && errorList[0].message) {
            this.showToast(errorList[0].message, 'error');
        }
    },

    showToast: function(message, type) {
        this.showToastWithParams(message, type, 'sticky');
    },

    showToastWithParams: function(message, type, mode) {
        $A.get('e.force:showToast').setParams({
            mode: mode,
            type: type,
            message: message
        }).fire();
    }
});