({
    doInit: function(component, event, helper) {
        helper.getOpp(component, helper);
    },
        
    sendData: function(component, event, helper) {
        helper.sendOpportunityData(component, helper);
    }, 
    
    syncData: function(component, event, helper) {
        helper.updateOpportunityData(component, helper);
    },

});