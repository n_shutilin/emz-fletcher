({
    getOpp: function (component, helper) {
        let action = component.get('c.getOppData');

        action.setParams({
            'recordId': component.get("v.recordId")
        });

        action.setCallback(this, function (response) {
            helper.spinnerHide(component);

            if (response.getState() === "SUCCESS") {
                var result = response.getReturnValue();

                component.set("v.opp", result);
            } else {
                helper.showToast(component, {errors: response.getError()});
            }

        });

        $A.enqueueAction(action);
    },

    sendOpportunityData: function (component, helper) {
        let action = component.get('c.createAdventOpp');

        action.setParams({
            'recordId': component.get("v.recordId")
        });

        action.setCallback(this, function (response) {
            helper.spinnerHide(component);

            if (response.getState() === "SUCCESS") {
                console.log('--- SUCCESS');
                let createRelatedRecords = component.get('c.createTradeInAndCreditApp');

                createRelatedRecords.setParams({
                    'recordId': component.get("v.recordId")
                });
                createRelatedRecords.setCallback(this, function (res) {
                    if (!res.getReturnValue()) { 
                        /* $A.get("$Label.c.") */
                        helper.showToast(component, {variant: 'success', message: 'Opportunity data was sent!'});
                        $A.get('e.force:refreshView').fire();
                        helper.getOpp(component, helper);
                    } else {
                        helper.showToast(component, {variant: 'error',message: res.getReturnValue()});
                        $A.get('e.force:refreshView').fire();
                        helper.getOpp(component, helper);
                    }
                });
                helper.spinnerShow(component);
                $A.enqueueAction(createRelatedRecords);
            } else {
                helper.showToast(component, {errors: response.getError()});
            }

        });

        helper.spinnerShow(component);
        $A.enqueueAction(action);
    },

    updateOpportunityData: function (component, helper) {
        let action = component.get('c.updateAdventOpp');

        action.setParams({
            'recordId': component.get("v.recordId")
        });

        action.setCallback(this, function (response) {
            helper.spinnerHide(component);

            if (response.getState() === "SUCCESS") {
                if (!response.getReturnValue()) {
                    /* $A.get("$Label.c.") */
                    helper.showToast(component, {variant: 'success', message: 'Opportunity data was sent!'});
                    $A.get('e.force:refreshView').fire();
                    helper.getOpp(component, helper);
                } else {
                    helper.showToast(component, {variant: 'error',message: response.getReturnValue()});
                    $A.get('e.force:refreshView').fire();
                    helper.getOpp(component, helper);
                }

            } else {
                helper.showToast(component, {errors: response.getError()});
            }

        });

        helper.spinnerShow(component);
        $A.enqueueAction(action);
    },
});