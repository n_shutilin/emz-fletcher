({
    doInit: function(component, event, helper) {
        helper.retrieveInfo(component, event, helper);
    },

    sendAppointmentInsertToCDK: function(component, event, helper) {
        helper.sendAppointmentToCDK(component, 'insert');
    },

    sendAppointmentUpdateToCDK: function(component, event, helper) {
        helper.sendAppointmentToCDK(component, 'update');
    },

    sendAppointmentDeleteToCDK: function(component, event, helper) {
        helper.sendAppointmentToCDK(component, 'delete');
    },
});