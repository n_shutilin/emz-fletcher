({
    retrieveInfo : function(component) {
        var action = component.get('c.retrieveServiceAppointment');
        var oppId = component.get("v.recordId");

        action.setParams({
            'recordId': oppId
        });
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                var appointment = response.getReturnValue();
                component.set('v.appointment', appointment);
                console.log('Updated');
            } else {
                console.error(response.getError());
            }

            component.set('v.isSpinner', false);
        });
        component.set('v.isSpinner', true);
        $A.enqueueAction(action);
    },

    sendAppointmentToCDK : function(component, actionType) {
        console.log('~');
        var action = component.get('c.sendAppointmentIntoCDK');
        var self = this;
        action.setParams({
            'recordId': component.get("v.recordId"),
            'actionType': actionType
        });
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                var result = response.getReturnValue();

                console.log(result);

                if (result === 'OK') {
                    self.retrieveInfo(component);
                    $A.get('e.force:refreshView').fire();
                } else {
                    component.set('v.warning', result);
                }

                component.set('v.isSpinner', false);
            } else {
                console.error(response.getError());
            }

        });
        component.set('v.isSpinner', true);
        $A.enqueueAction(action);
    }
});