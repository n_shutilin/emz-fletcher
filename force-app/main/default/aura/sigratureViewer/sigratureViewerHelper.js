({
    getSignature: function (component) {
        const action = component.get("c.getSignatureImage");
        action.setParams({appointmentId: component.get('v.recordId')});
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                component.set("v.signatureImage", 'data:image/png;base64, ' + response.getReturnValue());
                console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
});