({
    initHandler: function(component, event, helper) {
        helper.getYearOptions(component, event);
        helper.getMakeOptions(component, event);
    },

    makeChangeHandler: function (component, event, helper) {
        helper.getModelOptions(component, event);
    },

    handleVehicleChange: function(component, event) {
        const vehicleId = component.find("vehicleId").get("v.value");
        component.set('v.selectedVehicleId', vehicleId);
    },

    handleSave: function (component, event, helper) {
        helper.updateVehicleInfo(component, event);
    },

    handleCancel: function (component, event) {
        $A.get("e.force:closeQuickAction").fire();
    }
});