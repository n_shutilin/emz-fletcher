({
    getMakeOptions: function(component, event) {
        const that = this;
        const action = component.get('c.getMakeOptions');
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                component.set('v.makeOptions', response.getReturnValue());
            } else {
                that.showError(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    getModelOptions: function(component, event) {
        const that = this;
        const action = component.get('c.getModelOptionsByMake');
        action.setParams({makeId: component.get('v.selectedMake')});
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                component.set('v.modelOptions', response.getReturnValue());
            } else {
                that.showError(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    getYearOptions: function(component, event) {
        const that = this;
        const action = component.get('c.getYearOptions');
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                component.set('v.yearOptions', response.getReturnValue());
            } else {
                that.showError(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    updateVehicleInfo: function(component, event) {
        if (!component.get('v.isNoSelection')) {
            this.updateVehicleInfoFromExistingInventory(component, event);
        } else {
            this.updateVehicleInfoFromParams(component, event);
        }
    },

    updateVehicleInfoFromExistingInventory: function(component, event) {
        const that = this;
        const action = component.get('c.updateInventoryInfo');
        action.setParams({
            recordId: component.get('v.recordId'),
            inventoryId: component.get('v.selectedInventoryId')
        });
        action.setCallback(this, function (response) {
            if (response.getState() === 'SUCCESS') {
                that.showToast('The record has been successfully updated', 'success');
                $A.get('e.force:refreshView').fire();
                window.setTimeout(
                    $A.getCallback(function() {
                        $A.get('e.force:closeQuickAction').fire();
                    }), 100
                );
            } else {
                that.showError(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    updateVehicleInfoFromParams: function(component, event) {
        const that = this;
        const action = component.get("c.updateInventoryInfoFromParams");
        const selectedMake = component.get('v.searchParams.selectedMake');
        let selectedMakeLabel = null;
        const makeOptions = component.get('v.makeOptions');
        if (selectedMake) {
            selectedMakeLabel = makeOptions.find(x => x.value === selectedMake).label;
        }
        action.setParams({
            recordId: component.get('v.recordId'),
            inventoryJson: JSON.stringify({
                year: component.get('v.searchParams.selectedYear'),
                make: selectedMakeLabel,
                model: component.get('v.searchParams.selectedModel'),
                interiorColor: component.get('v.searchParams.interiorColor'),
                exteriorColor: component.get('v.searchParams.exteriorColor1')
            })
        });
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                that.showToast('The record has been successfully updated', 'success');
                $A.get('e.force:refreshView').fire();
                window.setTimeout(
                    $A.getCallback(function() {
                        $A.get("e.force:closeQuickAction").fire();
                    }), 100
                );
            } else {
                that.showError(response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    showError: function(errorList) {
        if (errorList.length > 0 && errorList[0].message) {
            this.showToast(errorList[0].message, 'error');
        }
    },

    showToast: function(message, type) {
        this.showToastWithParams(message, type, 'sticky');
    },

    showToastWithParams: function(message, type, mode) {
        $A.get('e.force:showToast').setParams({
            mode: mode,
            type: type,
            message: message
        }).fire();
    }
});