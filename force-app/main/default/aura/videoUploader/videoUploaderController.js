({
    handleUploadFinished: function (component, event, helper) {
        const uploadedFiles = event.getParam("files");
        uploadedFiles.forEach(file => helper.saveVideo(component, file.documentId));
    }
});