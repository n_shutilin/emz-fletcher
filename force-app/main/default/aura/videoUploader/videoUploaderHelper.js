({
    saveVideo: function (component, documentId) {
        const action = component.get("c.saveVideo");
        action.setParams({documentId: documentId, appointmentId: component.get('v.recordId')});
        const toastEvent = $A.get("e.force:showToast");
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                toastEvent.setParams({
                    "type" : "success",
                    "mode" : "dismissible",
                    "title" : "Success",
                    "message" : "The video has been successfully saved"
                });
            } else {
                toastEvent.setParams({
                    "type" : "error",
                    "mode" : "dismissible",
                    "title" : "Error",
                    "message" : "The issue occurred during video uploading"
                });
            }
            toastEvent.fire();
        });
        $A.enqueueAction(action);
    }
});