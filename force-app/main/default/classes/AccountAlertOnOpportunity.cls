public class AccountAlertOnOpportunity {
        private final Opportunity OppObj ;
        public List<Alert__c> Alerts {get;set;}
        
        public AccountAlertOnOpportunity (ApexPages.StandardController stdController)  {   
             this.OppObj = (Opportunity)stdController.getRecord();                        
    
             Alerts = [Select Id, Alert_Message__c, Active__c from Alert__c where Active__c = true and Account__c = :OppObj.AccountId limit 500];  
        
}

}