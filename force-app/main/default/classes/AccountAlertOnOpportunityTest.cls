@isTest
public class AccountAlertOnOpportunityTest{

    static testMethod void testMetod1() {
    Account acc = New Account();
    acc.Name = 'Test Account';
    insert acc;
    
    Opportunity opp = New Opportunity();
    opp.Name = 'Test Opp';
    opp.AccountId = acc.Id;
    opp.CloseDate = system.today() ;
    opp.StageName = 'Registered Sale';
    insert opp;
    
    Alert__c al = New Alert__c();
    al.Alert_Message__c = 'Test Alert';
    al.Active__c = True;
    al.Account__c = acc.Id;
    insert al;
    
    PageReference pageRef = Page.OpportunityAlertView;
    Test.setCurrentPage(pageRef);
	pageRef.getParameters().put('Id', String.valueOf(opp.Id));
	ApexPages.StandardController sc = new ApexPages.StandardController(opp);
    AccountAlertOnOpportunity acl = new AccountAlertOnOpportunity(sc);
    }
}