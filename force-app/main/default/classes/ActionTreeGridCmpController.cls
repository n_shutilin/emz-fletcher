public class ActionTreeGridCmpController {

    private static final String PROCESS_NAME_API_NAME = 'Process_Name__c';
    private static final String OPEN_STATUS_NAME = 'Open';
    private static final String ATTEMPTED_STATUS_NAME = 'Attempted';
    private static final String COMPLETED_STATUS_NAME = 'Completed';
    private static final String QUEUE_GROUP_TYPE_NAME = 'Queue';
    private static final List<String> STATUS_NAMES = new List<String>{
        OPEN_STATUS_NAME,
        ATTEMPTED_STATUS_NAME,
        COMPLETED_STATUS_NAME
    };

    @AuraEnabled
    public static List<ResultDto> getActionsDataToDisplay(String teamId, Date startDate, Date endDate) {
        List<AggregateResult> resultsByProcessName = new List<AggregateResult>();
        resultsByProcessName = Database.query(queryStringBuilder(teamId, startDate, endDate, true));
        List<String> processNames = new List<String>();
        for (AggregateResult item : resultsByProcessName) {
            processNames.add(String.valueOf(item.get(PROCESS_NAME_API_NAME)));
        }
        if (!processNames.isEmpty()) {
            Map<String, ActionDataWrapper> processNameToActionInfo = new Map<String, ActionDataWrapper>();
            Map<String, Map<String, ActionDataWrapper>> processNameToSubjectInfo = new Map<String, Map<String, ActionDataWrapper>>();
            for (Action__c item : Database.query(queryStringBuilder(teamId, startDate, endDate, false))) {
                if (processNameToActionInfo.containsKey(item.Process_Name__c)) {
                    ActionDataWrapper actionDataWrapper = processNameToActionInfo.get(item.Process_Name__c);
                    actionDataWrapper = calcStatusCounts(item.Status__c, actionDataWrapper);
                    processNameToActionInfo.put(item.Process_Name__c, actionDataWrapper);
                } else {
                    processNameToActionInfo.put(item.Process_Name__c, calcStatusCounts(item.Status__c, new ActionDataWrapper()));
                }
                if (processNameToSubjectInfo.containsKey(item.Process_Name__c)) {
                    Map<String, ActionDataWrapper> subjectToActionDataWrapper = processNameToSubjectInfo.get(item.Process_Name__c);
                    if (subjectToActionDataWrapper.containsKey(item.Name)) {
                        ActionDataWrapper actionDataWrapper = subjectToActionDataWrapper.get(item.Name);
                        List<Id> listIds = actionDataWrapper.listIds;
                        if (item.Status__c == OPEN_STATUS_NAME) {
                            if (item.Opportunity__c != null) {
                                listIds.add(item.Opportunity__c);
                            } else if (item.Lead__c != null) {
                                listIds.add(item.Lead__c);
                            } else if (item.Service_Appointment__c!= null) {
                                listIds.add(item.Service_Appointment__c);
                            } else if (item.Account__c != null) {
                                listIds.add(item.Account__c);
                            }
                        }
                        actionDataWrapper.listIds = listIds;
                        actionDataWrapper = calcStatusCounts(item.Status__c, actionDataWrapper);
                        subjectToActionDataWrapper.put(item.Name, actionDataWrapper);
                    } else {
                        ActionDataWrapper actionDataWrapper = new ActionDataWrapper();
                        if (item.Status__c == OPEN_STATUS_NAME) {
                            if (item.Opportunity__c != null) {
                                actionDataWrapper.listIds = new List<Id>{
                                    item.Opportunity__c
                                };
                            } else if (item.Lead__c != null) {
                                actionDataWrapper.listIds = new List<Id>{
                                    item.Lead__c
                                };
                            } else if (item.Service_Appointment__c!= null) {
                                actionDataWrapper.listIds = new List<Id>{
                                    item.Service_Appointment__c
                                };
                            } else if (item.Account__c!= null) {
                                actionDataWrapper.listIds = new List<Id>{
                                    item.Account__c
                                };
                            }
                        }
                        subjectToActionDataWrapper.put(item.Name, calcStatusCounts(item.Status__c, actionDataWrapper));
                    }
                    processNameToSubjectInfo.put(item.Process_Name__c, subjectToActionDataWrapper);
                } else {
                    Map<String, ActionDataWrapper> subjectToActionDataWrapper = new Map<String, ActionDataWrapper>();
                    ActionDataWrapper actionDataWrapper = new ActionDataWrapper();
                    if (item.Status__c == OPEN_STATUS_NAME) {
                        if (item.Opportunity__c != null) {
                            actionDataWrapper.listIds = new List<Id>{
                                item.Opportunity__c
                            };
                        } else if (item.Lead__c != null) {
                            actionDataWrapper.listIds = new List<Id>{
                                item.Lead__c
                            };
                        } else if (item.Service_Appointment__c!= null) {
                            actionDataWrapper.listIds = new List<Id>{
                                item.Service_Appointment__c
                            };
                        } else if (item.Account__c!= null) {
                            actionDataWrapper.listIds = new List<Id>{
                                item.Account__c
                            };
                        }
                    }
                    subjectToActionDataWrapper.put(item.Name, calcStatusCounts(item.Status__c, actionDataWrapper));
                    processNameToSubjectInfo.put(item.Process_Name__c, subjectToActionDataWrapper);
                    System.debug(processNameToSubjectInfo);
                }
            }
            List<ResultDto> resultToDisplay = new List<ResultDto>();
            for (AggregateResult item : resultsByProcessName) {
                String processName = String.valueOf(item.get(PROCESS_NAME_API_NAME));
                List<ResultDto> childRecords = new List<ResultDto>();
                for (String itemKey : processNameToSubjectInfo.get(processName).keySet()) {
                    childRecords.add(new ResultDto(
                        itemKey,
                        processNameToSubjectInfo.get(processName).get(itemKey).countWithOpenStatus,
                        processNameToSubjectInfo.get(processName).get(itemKey).countWithAttemptStatus,
                        processNameToSubjectInfo.get(processName).get(itemKey).countWithCompletedStatus,
                        processNameToSubjectInfo.get(processName).get(itemKey).total,
                        generateLink(processNameToSubjectInfo.get(processName).get(itemKey).listIds, processName, itemKey, teamId, startDate, endDate)
                    ));
                }
                resultToDisplay.add(new ResultDto(
                    processName,
                    processNameToActionInfo.get(processName).countWithOpenStatus,
                    processNameToActionInfo.get(processName).countWithAttemptStatus,
                    processNameToActionInfo.get(processName).countWithCompletedStatus,
                    processNameToActionInfo.get(processName).total,
                    childRecords,
                    '/'
                ));
            }
            return resultToDisplay;
        }
        return null;
    }

    @AuraEnabled
    public static List<TeamDto> getTeamsForShowing() {
        List<User> users = [SELECT Id, Name FROM User];
        List<Group> groups = [SELECT Id, Name FROM Group WHERE Type = :QUEUE_GROUP_TYPE_NAME];
        List<TeamDto> result = new List<TeamDto>();
        for (User item : users) {
            result.add(new TeamDto(
                item.Id,
                item.Name
            ));
        }
        for (Group item : groups) {
            result.add(new TeamDto(
                item.Id,
                item.Name
            ));
        }
        return result;
    }

    private static String generateLink(List<Id> ids, String processName, String subProcessName, String teamId, Date actionDate, Date endDate) {
        if (ids.isEmpty()) {
            return '/';
        }
        Id idToOpen = ids.get(0);
        return '/lightning/r/' + idToOpen.getSobjectType() 
                + '/' + idToOpen 
                + '/view?c__action_process_name=' + processName 
                + '&c__action_sub_process_name=' + subProcessName
                + '&c__team_id=' + teamId 
                + '&c__date=' + String.valueOf(actionDate) 
                + '&c__end_date=' + String.valueOf(endDate);
    }

    private static String queryStringBuilder(String teamId, Date startDate,  Date endDate, Boolean isGroupBy) {
        String query = '';
        if (isGroupBy) {
            query += 'SELECT Process_Name__c FROM Action__c WHERE Status__c IN :STATUS_NAMES';
        } else {
            query += 'SELECT Id, Opportunity__c, Lead__c, Account__c, Service_Appointment__c, Status__c, Process_Name__c, Name  FROM Action__c WHERE Status__c IN :STATUS_NAMES';
        }
        if (startDate != null && endDate != null) {
            query += ' AND Due_Date__c >= :startDate AND Due_Date__c <= :endDate ';
        } else {
            query += ' AND Due_Date__c = TODAY ';
        }
        if (teamId != null && teamId != '') {
            query += ' AND OwnerId = :teamId ';
        }
        if (isGroupBy) {
            query += ' GROUP BY Process_Name__c';
        }
        return query;
    }

    private static ActionDataWrapper calcStatusCounts(String status, ActionDataWrapper actionDataWrapper) {
        switch on status {
            when 'Open' {
                actionDataWrapper.countWithOpenStatus += 1;
                actionDataWrapper.total += 1;
            }
            when 'Attempted' {
                actionDataWrapper.countWithAttemptStatus += 1;
                actionDataWrapper.total += 1;
            }
            when 'Completed' {
                actionDataWrapper.countWithCompletedStatus += 1;
                actionDataWrapper.total += 1;
            }
            when else {

            }
        }
        return actionDataWrapper;
    }

    private class ActionDataWrapper {
        private List<Id> listIds;
        private Integer countWithOpenStatus;
        private Integer countWithAttemptStatus;
        private Integer countWithCompletedStatus;
        private Integer total;

        private ActionDataWrapper() {
            this.countWithOpenStatus = 0;
            this.countWithAttemptStatus = 0;
            this.countWithCompletedStatus = 0;
            this.total = 0;
            this.listIds = new List<Id>();
        }
    }

    public class TeamDto {
        @AuraEnabled
        public Id id;
        @AuraEnabled
        public String name;

        public TeamDto(Id id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    public class ResultDto {
        @AuraEnabled
        public String name;
        @AuraEnabled
        public Integer countWithOpenStatus;
        @AuraEnabled
        public Integer countWithAttemptStatus;
        @AuraEnabled
        public Integer countWithCompletedStatus;
        @AuraEnabled
        public Integer total;
        @AuraEnabled
        public List<ResultDto> children;
        @AuraEnabled
        public String linkToParentRecords;

        public ResultDto(String name, Integer countWithOpenStatus, Integer countWithAttemptStatus, Integer countWithCompletedStatus, Integer total, String linkToParentRecords) {
            this.name = name == null ? 'Others' : name;
            this.countWithOpenStatus = countWithOpenStatus;
            this.countWithAttemptStatus = countWithAttemptStatus;
            this.countWithCompletedStatus = countWithCompletedStatus;
            this.total = total;
            this.linkToParentRecords = linkToParentRecords;
        }

        public ResultDto(String name, Integer countWithOpenStatus, Integer countWithAttemptStatus, Integer countWithCompletedStatus, Integer total, List<ResultDto> children, String linkToParentRecords) {
            this.name = name == null ? 'Others' : name;
            this.countWithOpenStatus = countWithOpenStatus;
            this.countWithAttemptStatus = countWithAttemptStatus;
            this.countWithCompletedStatus = countWithCompletedStatus;
            this.total = total;
            this.children = children;
            this.linkToParentRecords = linkToParentRecords;
        }
    }
}