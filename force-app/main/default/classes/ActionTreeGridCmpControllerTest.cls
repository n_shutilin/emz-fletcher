@IsTest
private class ActionTreeGridCmpControllerTest {
    @TestSetup
    static void setup() {
        Opportunity testOpportunity = new Opportunity(
                Name = 'Test',
                StageName = 'Test',
                CloseDate = Date.newInstance(2021, 12, 12),
                Dealer_ID__c = 'CHAUD'
        );
        insert testOpportunity;
        
        Lead testLead = new Lead(
                Dealer_ID__c = 'CHAUD',
                LastName = 'Test'
        );
        insert testLead;
        
        Action__c openedtestAction = new Action__c(
                Process_Name__c = 'Service_Follow_Up_Flow',
                Status__c = 'Open',
                Lead__c = testLead.Id,
                Due_Date__c = Date.newInstance(2019, 09, 13),
            	Name = 'TEST NAME'
        );
        
		Action__c openedTestAction2 = new Action__c(
                Process_Name__c = 'Service_Follow_Up_Flow_2',
                Status__c = 'Open',
                Opportunity__c = testOpportunity.Id,
                Due_Date__c = Date.newInstance(2019, 09, 13),
                Name = 'TEST NAME 2'
        );

        Action__c attemptedTestAction = new Action__c(
                Process_Name__c = 'Service_Follow_Up_Flow',
                Status__c = 'Attempted',
                Lead__c = testLead.Id,
                Due_Date__c = Date.newInstance(2019, 09, 13),
            	Name = 'TEST NAME'
        );
        
        Action__c attemptedTestAction2 = new Action__c(
                Process_Name__c = 'Service_Follow_Up_Flow_2',
                Status__c = 'Attempted',
                Opportunity__c = testOpportunity.Id,
                Due_Date__c = Date.newInstance(2019, 09, 13),
            	Name = 'TEST NAME'
        );
        
        Action__c completedTestAction = new Action__c(
                Process_Name__c = 'Service_Follow_Up_Flow',
                Status__c = 'Completed',
                Opportunity__c = testOpportunity.Id,
                Due_Date__c = Date.newInstance(2019, 09, 13),
            	Name = 'TEST NAME'
        );
        
        Action__c completedTestAction2 = new Action__c(
                Process_Name__c = 'Service_Follow_Up_Flow_2',
                Status__c = 'Completed',
                Lead__c = testLead.Id,
                Due_Date__c = Date.newInstance(2019, 09, 13),
            	Name = 'TEST NAME'
        );
        
        insert new List<Action__c>{openedtestAction, openedtestAction2, attemptedTestAction, attemptedTestAction2, completedTestAction, completedTestAction2};
    }
    
    @IsTest
    static void getActionsDataToDisplay() {
        Action__c action = [SELECT OwnerId FROM Action__c WHERE Status__c = 'Open' LIMIT 1];
        ActionTreeGridCmpController.getActionsDataToDisplay(action.OwnerId, Date.newInstance(2019, 03, 02), Date.newInstance(2020, 03, 02));
    }

    @IsTest
    static void getTeamsForShowing() {
        ActionTreeGridCmpController.getTeamsForShowing();
    }
}