public with sharing class ActionTriggerHandler {
    public static void publishEvent(List<Action__c> newActions, Map<Id, Action__c> oldActionsMap) {
        Set<String> relatedIds = new Set<String>();

        Boolean oldMapExist = !oldActionsMap.isEmpty();
        for (Action__c action : newActions) {
            if (String.isNotBlank(action.Account__c)) {
                relatedIds.add(action.Account__c);
            }
            if (String.isNotBlank(action.Opportunity__c)) {
                relatedIds.add(action.Opportunity__c);
            }
            if (String.isNotBlank(action.Lead__c)) {
                relatedIds.add(action.Lead__c);
            }
            
            if (oldMapExist) {
                Action__c oldAction = oldActionsMap.get(action.Id);
                if (String.isNotBlank(oldAction.Account__c)) {
                    relatedIds.add(oldAction.Account__c);
                }
                if (String.isNotBlank(oldAction.Opportunity__c)) {
                    relatedIds.add(oldAction.Opportunity__c);
                }
                if (String.isNotBlank(oldAction.Lead__c)) {
                    relatedIds.add(oldAction.Lead__c);
                }
            }
        }

        if (!relatedIds.isEmpty()) {
            List<String> recordIdsToRefresh = new List<String>(relatedIds);
            Action_Event__e ev = new Action_Event__e(Parent_Record_Id__c = String.join(recordIdsToRefresh, ', '));
            Database.SaveResult result = EventBus.publish(ev);
        }
    }
}