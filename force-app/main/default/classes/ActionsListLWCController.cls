public with sharing class ActionsListLWCController {

    private static final String NEXT_N_DAYS = 'NEXT_N_DAYS';
    private static final Map<String, String> ACTION_STATUSES = new Map<String, String>{'cancel' => 'Cancelled', 'complete' => 'Completed', 'attempted' => 'Attempted'};
    
    @AuraEnabled 
    public static String getActionsList(String recId, String condition) {
        String finalCondition = '';
        Id recordId = Id.valueOf(recId);
        String objectEntity = String.valueOf(recordId.getSobjectType());
        if (condition != '' && condition.startsWith(NEXT_N_DAYS)) {
            finalCondition = ' AND (Due_Date__c = ' + condition + ' OR Due_Date__c = TODAY)';
        } else if (condition != '') {
            finalCondition = ' AND Due_Date__c = ' + condition;
        }
        String query = 'SELECT Id, Name, Icon__c, Due_Date__c, Status__c, OwnerId, Owner.Alias, Owner.Name FROM Action__c WHERE ' + objectEntity + '__c = :recId '
            + ' AND Status__c = \'Open\' AND Due_Date__c != NULL ' + finalCondition + ' ORDER BY Due_Date__c ASC LIMIT 50000 ';
        List<Action__c> actionList = Database.query(query);
        //[SELECT Id, Name, Icon__c, Due_Date__c, Status__c, OwnerId, Owner.Alias, Owner.Name FROM Action__c WHERE Lead__c =: recId LIMIT 50000];
        List<ActionWrapper> result = new List<ActionWrapper>();
        for (Action__c act : actionList) {
            ActionWrapper aw = new ActionWrapper();
            aw.Id = act.Id;
            aw.Name = act.Name;
            aw.Icon = act.Icon__c;
            aw.DueDate = act.Due_Date__c;
            aw.Status = act.Status__c;
            aw.OwnerId = act.OwnerId;
            aw.OwnerAlias = act.Owner.Alias != null ? act.Owner.Alias : act.Owner.Name;

            result.add(aw);
        }
        return JSON.serialize(result);
    }

    @AuraEnabled
    public static void changeActionStatus(String recId, String actionName) {
        List<Action__c> actions = [SELECT Id, Status__c FROM Action__c WHERE Id = :recId];
        if (actions[0] != null) {
            actions[0].Status__c = ACTION_STATUSES.get(actionName);
            try {
                update actions[0];
            } catch (Exception e) {
                throw new AuraHandledException('Error ' + e.getMessage());
            }
        }
    }

    @AuraEnabled
    public static String processCancel(String recordId, String parentRecordId, String userId) {
        changeActionStatus(recordId, 'cancel');
        return linkHistory(recordId, parentRecordId, userId);
    }

    private static String linkHistory(String recordId, String parentRecordId, String userId) {
        List<Action__c> actionList = [SELECT Id, Name, Due_Date_Time__c, Type__c, Status__c FROM Action__c WHERE Id = :recordId];
        String newTaskId = 'task wasnt created';
        if(!actionList.isEmpty()) {
            Action__c action = actionList[0];
            Task newTask = new Task(
                WhatId = parentRecordId,
                Subject = action.Type__c,
                Activity_Type__c = action.Type__c,
                Status = 'Completed',
                Priority = 'Normal',
                OwnerId = userId,
                Description = action.Type__c + ' status = ' + action.Status__c + ' Original ' + action.Type__c + ' Date = ' + action.Due_Date_Time__c
            );
            insert newTask;
            newTaskId = newTask.Id;
            // risePlatformEvent(parentRecordId);
        }
        return newTaskId;
    }

    private static void risePlatformEvent(String parentRecordId){
        Activity_History_Update__e event = new Activity_History_Update__e(
            Related_To__c = parentRecordId
        );
        EventBus.publish(event); 
    }

    public class ActionWrapper {
        String Id;
        String Name;
        String Icon;
        Date DueDate;
        String Status;
        String OwnerId;
        String OwnerAlias;
    }
}