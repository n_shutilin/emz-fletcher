@IsTest
private class ActionsListLWCControllerTest {
    @TestSetup
    static void setup() {
        Opportunity testOpportunity = new Opportunity(
                Name = 'Test',
                StageName = 'Test',
                CloseDate = Date.newInstance(2021, 12, 12),
                Dealer_ID__c = 'CHAUD'
        );
        insert testOpportunity;
        Lead testLead = new Lead(
                Dealer_ID__c = 'CHAUD',
                LastName = 'Test'
        );
        insert testLead;
        Action__c testAction = new Action__c(
                Process_Name__c = 'Service_Follow_Up_Flow',
                Status__c = 'Open',
                Opportunity__c = testOpportunity.Id,
                Lead__c = testLead.Id,
                Due_Date__c = Date.today()
        );
        insert testAction;
    }
    @IsTest
    static void getActionsListTest() {
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        ActionsListLWCController.getActionsList(opportunity.Id, 'today');
    }

    @IsTest
    static void changeActionStatusTest() {
        Action__c action = [SELECT Id FROM Action__c LIMIT 1];
        ActionsListLWCController.changeActionStatus(action.Id, 'cancel');
    }
    
    @IsTest
    static void processCancelTest() {
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        Action__c action = [SELECT Id FROM Action__c LIMIT 1];
        ActionsListLWCController.processCancel(action.Id, opportunity.Id, UserInfo.getUserId());
    }
}