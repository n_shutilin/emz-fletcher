public class AdventApi {

    public static final Integer API_TIMEOUT = 120000;

    public String authHeader;

    private AdventApi() {
    }

    public AdventApi(String username, String password) {
        this.authHeader = AdventApi.getAuthHeader(username, password);
    }

    public AdventApi(String authHeader) {
        this.authHeader = authHeader;
    }

    public static String getAuthHeader(String username, String password) {
        String authHeader = EncodingUtil.base64Encode(Blob.valueOf(username + ':' + password));
        return authHeader;
    }

    public static String buildParams(Map<String, String> paramsMap) {
        System.debug(paramsMap);
        List<String> params = new List<String>();
        for (String paramName : paramsMap.keySet()) {
            String param = paramsMap.get(paramName);
            if (paramName == 'business_name' || paramName == 'individual_first_name'  || paramName == 'individual_last_name') {
                param = EncodingUtil.urlEncode(param, 'UTF-8');
            }
            if (String.isNotBlank(param)) {
                params.add(paramName + '=' + param);
            }
        }

        String paramsStr = '';

        if (!params.isEmpty()) {
            paramsStr = '?' + String.join(params, '&');
        }

        return paramsStr;
    }

    public HttpResponse get(String path, Map<String, String> paramsMap) {
        System.debug('i was here');
        String requestParams = AdventApi.buildParams(paramsMap) != null ? AdventApi.buildParams(paramsMap).replaceAll('\\s', '') : '';
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setTimeout(API_TIMEOUT);
        req.setHeader('Authorization', authHeader);
        req.setEndpoint('callout:Advent_Api' + path + requestParams);
        System.debug('~~~~~~~~ ' + req.getHeader('Authorization'));
        System.debug('~~~~~~~~ ' + req.getHeader('content-type'));
        System.debug('~~~~~~~~ ' + req.getEndpoint());

        HttpResponse res = AdventApi.send(req);

        if (res.getStatus() == 'OK') {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'Advent Integration', res.getBody(), req.getBody());
        } else {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'Advent Integration', res.getBody(), req.getBody());
        }
        return res;
    }

    public HttpResponse getNoParams(String path) {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setTimeout(API_TIMEOUT);
        req.setHeader('Authorization', authHeader);
        req.setEndpoint('callout:Advent_Api' + path);
        System.debug('~~~~~~~~ ' + req.getHeader('Authorization'));
        System.debug('~~~~~~~~ ' + req.getHeader('content-type'));
        System.debug('~~~~~~~~ ' + req.getEndpoint());
        HttpResponse res = AdventApi.send(req);

        if (res.getStatus() == 'OK') {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'Advent Integration', res.getBody(), req.getBody());
        } else {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'Advent Integration', res.getBody(), req.getBody());
        }
        return res;
    }

    public HttpResponse post(String path, String body) {
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');  
        req.setTimeout(API_TIMEOUT);
        req.setHeader('Authorization', authHeader);
        req.setEndpoint('callout:Advent_Api' + path);
        req.setBody(body);

        System.debug('authHeader ' + authHeader);
        System.debug('body ' + body);

        System.debug('~~~~~~~~ ' + req.getHeader('Authorization'));
        System.debug('~~~~~~~~ ' + req.getHeader('content-type'));
        System.debug('~~~~~~~~ ' + req.getEndpoint());
        HttpResponse res = AdventApi.send(req);

        if (res.getStatus() == 'OK') {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'Advent Integration', res.getBody(), req.getBody());
        } else {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'Advent Integration', res.getBody(), req.getBody());
        }
        return res;
    }

    public HttpResponse put(String path, String body, Map<String, String> paramsMap) {
        String requestParams = AdventApi.buildParams(paramsMap);

        HttpRequest req = new HttpRequest();
        req.setMethod('PUT');
        req.setTimeout(API_TIMEOUT);
        req.setHeader('Authorization', authHeader);
        req.setEndpoint('callout:Advent_Api' + path + requestParams);
        req.setBody(body);
        System.debug('~~~~~~~~ ' + req);
        System.debug('~~~~~~~~ ' + req.getHeader('Authorization'));
        System.debug('~~~~~~~~ ' + req.getHeader('Content-Type'));
        System.debug('~~~~~~~~ ' + req.getEndpoint());
        HttpResponse res = AdventApi.send(req);
        
        if (res.getStatus() == 'OK') {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'Advent Integration', res.getBody(), req.getBody());
        } else {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'Advent Integration', res.getBody(), req.getBody());
        }
        return res;
    }

    public static HttpResponse send(HttpRequest req) {
        HttpResponse res;
        //added two lines below
        res = new Http().send(req);
        return res;
        /*
        try {
            res = new Http().send(req);
        } catch (CalloutException ce) {
            System.debug(LoggingLevel.ERROR, ce.getMessage());
            throw new AdventApiException(ce.getMessage(), ce);
        } finally {
            AdventApi.log(req, res);
        }

        return res;*/
    }

    public static void log(HttpRequest req, HttpResponse res) {
        System.debug('📡');
        System.debug(LoggingLevel.INFO, 'endpoint: ' + req.getEndpoint());
        System.debug(LoggingLevel.INFO, 'method: ' + req.getMethod());
        System.debug(LoggingLevel.INFO, 'request: ' + req.getBody());

        if (res != null) {
            if (res.getStatusCode() != 200) {
                System.debug(LoggingLevel.ERROR, '🔴 status code: ' + res.getStatusCode());
                System.debug(LoggingLevel.ERROR, 'status: ' + res.getStatus());

                System.debug(LoggingLevel.ERROR, 'response: ' + res.getBody());
                throw new AdventApiException(res.getBody());
            } else {
                System.debug(LoggingLevel.INFO, '✅ status code: ' + res.getStatusCode());

            }

            System.debug(LoggingLevel.INFO, 'response: ' + res.getBody());
        }

    }

    public static String generateDealDate(Date input) {
        String year = String.valueOf(input.year());
        String month = String.valueOf(input.month());
        if (month.length() == 1) {
            month = '0' + month;
        }
        String day = String.valueOf(input.day());
        if (day.length() == 1) {
            day = '0' + day;
        }
        return month + '/' + day + '/' + year;
    }

    public class AdventApiException extends Exception {

    }

    public enum Flag {
        Y, N
    }

    public static void createIntegrationLog(String result, String errorMessage, String endpoint, String process, String resBody, String reqBody) {
        IntegrationLogger.addLog(
            new Integration_Logs__c(
                Result__c = result,
                Error_Message__c = errorMessage,
                Endpoint__c = endpoint,
                Process__c = process,
                Posix_Time__c = DateTime.now().getTime()
            ),
            new List<Attachment>{
                new Attachment(
                    Name = 'Request', 
                    ContentType = 'text/plain',
                    Body = Blob.valueOf(reqBody)
                ),
                new Attachment(
                    Name = 'Response',
                    ContentType = 'text/plain',
                    Body = Blob.valueOf(resBody)
                )
            }
        );
        try {
        //    IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }
}