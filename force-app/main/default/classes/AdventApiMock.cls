@isTest
global class AdventApiMock implements HttpCalloutMock {
    private static final String EMPTY_BODY = '';
    private static final String FIRST_TEST_BODY = '{'+
  					+'"success": true,'+
  					+'"message": "test",'+
  					+'"errors": ['+
  						+'{'+
  							+'"msg": null,'+
  							+'"id": null'+
  						+'}'+
  					+'],'+
  					+'"data": {'+
    					+'"opportunity_number": null,'+
    					+'"opportunity_id": null,'+
    					+'"client_number": null,'+
    					+'"client_id": null'+
  					+'}'+
				+'}';
    
    private String body;
    
    global AdventApiMock(){
        this(EMPTY_BODY);
    }
    
    global AdventApiMock(String body){
        this.body = body;
    }
    
     global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
         if(req.getEndpoint() == 'callout:Advent_Api/opportunity'){
            res.setBody(FIRST_TEST_BODY); 
         }else {
            res.setBody(this.body);    
         }
        res.setStatusCode(200);
        res.setStatus('Success');
        return res;
	}
}