public abstract class AdventBaseHandler {

    public AdventApi api;

    public AdventBaseHandler() {
    }

    public AdventBaseHandler(AdventApi api) {
        this.api = api;
    }

}