public class AdventClientHandler extends AdventBaseHandler {

    public static String path = '/client';

    public static String BUSINESS_F_PARAM = 'business_f';
    public static String BUSINESS_NAME_PARAM = 'business_name';
    public static String INDIVIDUAL_FIRST_NAME_PARAM = 'individual_first_name';
    public static String INDIVIDUAL_LAST_NAME_PARAM = 'individual_last_name';
    public static String PHONE_PARAM = 'phone_number';
    public static String EMAIL_PARAM = 'email_address';
    public static String DRIVER_LICENSE_NUMBER_PARAM = 'drivers_license_number';

    private AdventClientHandler() {
    }

    public AdventClientHandler(AdventApi api) {
        super(api);
    }

    public GetClientResponse getClients(Map<String,String> paramsMap) {
        HttpResponse res = api.get(path, paramsMap);
        system.debug(res.getBody());
        GetClientResponse wrapper = (GetClientResponse) JSON.deserialize(res.getBody(), GetClientResponse.class);
        System.debug('~ ' + wrapper);
        return wrapper;
    }

    public InsertClientResponse insertClient(AdventClientHandler.ClientResource client) {
        HttpResponse res = api.post('/client', JSON.serialize(client));
        System.debug('client ' + client);
        System.debug('response ' + res.getBody());
        return (InsertClientResponse) JSON.deserialize(res.getBody(), InsertClientResponse.class);
    }

    public class GetClientResponse {
        public Boolean success;
        public List<ClientResource> data;
        public Integer totalProperty;
    }

    public class InsertClientResponse {
        public Boolean success;
        public ClientResource data;
    }

    public class ClientResource {
        public String client_id;
        public AdventApi.Flag business_f;
        public Date birthdate;
        public String business_contact_name;
        public String business_contact_position;
        public String business_name;
        public String cell_phone;
        public String client_generation_c;
        public String client_number;
        public String current_address_address_state_c;
        public String current_address_city;
        public String current_address_postal_code;
        public String current_address_street;
        public AdventApi.Flag customer_f;
        public String email_address_list;
        public String fax_phone;
        public String home_phone;
        public Integer home_phone_ext;
        public String individual_first_name;
        public String individual_gender_c;
        public String individual_last_name;
        public String individual_middle_name;
        public Integer individual_spouse;
        public String work_phone;
        public Integer work_phone_ext;
    }

}