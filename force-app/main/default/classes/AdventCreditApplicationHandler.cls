public class AdventCreditApplicationHandler extends AdventBaseHandler {

    public static String path = '/credit-application';

    public static String ID_PARAM = 'id';

    private AdventCreditApplicationHandler() {
    }

    public AdventCreditApplicationHandler(AdventApi api) {
        super(api);
    }

    public PostCreditApplicationResponse updateCreditApplication(CreditApplicationResource ar, Map<String,String> paramsMap) {
        System.debug('--- ar: ' + ar);
        System.debug('--- paramsMap: ' + paramsMap);
        String requestBody = JSON.serialize(ar);
        System.debug('--- requestBody: ' + requestBody);
        HttpResponse res = api.put(path, requestBody, paramsMap);
        PostCreditApplicationResponse wrapper = (PostCreditApplicationResponse) JSON.deserialize(res.getBody(), PostCreditApplicationResponse.class);

        return wrapper;
    }

    public class PostCreditApplicationResponse {
        public Boolean success;
        public String data;
    }

    public class CreditApplicationResource {
        public Decimal gross_income;
        public String gross_income_interval_c;
        public Decimal alimony_income;
        public String alimony_income_interval_c;
        public String alimony_income_court_order_f;
        public String alimony_income_written_agreement_f;
        public String alimony_income_verbal_agreement_f;
        public Decimal other_income;
        public String other_income_description;
        public String other_income_interval_c;
        public String education_level_c;
        public String marital_status_c;
        public Decimal number_of_dependants;
        public Decimal dependant_1_age;
        public Decimal dependant_2_age;
        public Decimal dependant_3_age;
        public String personal_reference_1_name;
        public String personal_reference_1_street;
        public String personal_reference_1_city;
        public String personal_reference_1_state_c;
        public String personal_reference_1_postal_code;
        public String personal_reference_1_phone_number;
        public String personal_reference_1_relationship;
        public String personal_reference_2_name;
        public String personal_reference_2_street;
        public String personal_reference_2_city;
        public String personal_reference_2_state_c;
        public String personal_reference_2_postal_code;
        public String personal_reference_2_phone_number;
        public String personal_reference_2_relationship;
        public String personal_reference_3_name;
        public String personal_reference_3_street;
        public String personal_reference_3_city;
        public String personal_reference_3_state_c;
        public String personal_reference_3_postal_code;
        public String personal_reference_3_phone_number;
        public String personal_reference_3_relationship;
        public String credit_reference_1_type;
        public String credit_reference_1_closed_f;
        public String credit_reference_1_company_name;
        public String credit_reference_1_account_number;
        public String credit_reference_1_address1;
        public String credit_reference_1_address2;
        public Decimal credit_reference_1_current_balance;
        public Decimal credit_reference_1_high_balance;
        public Decimal credit_reference_1_payment;
        public String credit_reference_1_closed_date;
        public String credit_reference_2_type;
        public String credit_reference_2_closed_f;  
        public String credit_reference_2_company_name;
        public String credit_reference_2_account_number;
        public String credit_reference_2_address1;
        public String credit_reference_2_address2;
        public Decimal credit_reference_2_current_balance;
        public Decimal credit_reference_2_high_balance;
        public Decimal credit_reference_2_payment;
        public String credit_reference_2_closed_date;
        public String credit_reference_3_type;
        public String credit_reference_3_closed_f;
        public String credit_reference_3_company_name;
        public String credit_reference_3_account_number;
        public String credit_reference_3_address1;
        public String credit_reference_3_address2;
        public Decimal credit_reference_3_current_balance;
        public Decimal credit_reference_3_high_balance;
        public Decimal credit_reference_3_payment;
        public String credit_reference_3_closed_date;
        public String credit_reference_4_type;
        public String credit_reference_4_closed_f;
        public String credit_reference_4_company_name;
        public String credit_reference_4_account_number;
        public String credit_reference_4_address1;
        public String credit_reference_4_address2;
        public Decimal credit_reference_4_current_balance;
        public Decimal credit_reference_4_high_balance;
        public Decimal credit_reference_4_payment;
        public String credit_reference_4_closed_date;
        public String bank_reference_1_name;
        public String bank_reference_1_account_number;
        public String bank_reference_1_account_type_c;
        public String bank_reference_1_branch;
        public String bank_reference_1_address;
        public Decimal bank_reference_1_balance;
        public String bank_reference_2_name;
        public String bank_reference_2_account_number;
        public String bank_reference_2_account_type_c;
        public String bank_reference_2_branch;
        public String bank_reference_2_address;
        public Decimal bank_reference_2_balance;
        public String residence_type_c;
        public String mortgage_company_name;
        public String mortgage_account_number;
        public String mortgage_street;
        public String mortgage_city;
        public String mortgage_state_c;
        public String mortgage_zip;
        public String mortgage_purchase_date;
        public Decimal mortgage_home_age;
        public Decimal mortgage_home_price;
        public Decimal mortgage_home_market_value;
        public Decimal mortgage_balance;
        public Decimal mortgage_monthly_payment;
        public Decimal mortgage_2nd_balance;
        public Decimal mortgage_2nd_payment;
        public Decimal monthly_rent;
        public String current_vehicle_1_finance_company;
        public String current_vehicle_1_account_number;
        public String current_vehicle_1_address;
        public Decimal current_vehicle_1_payment;
        public String current_vehicle_2_finance_company;
        public String current_vehicle_2_account_number;
        public String current_vehicle_2_address;
        public Decimal current_vehicle_2_payment;
        public String insurance_company_name;
        public String insurance_agent;
        public String insurance_street;
        public String insurance_city;
        public String insurance_state_c;
        public String insurance_zip;
        public String insurance_phone_number;
        public String insurance_policy_number;
        public String insurance_garaged_at;
        public String insurance_cancelled_f;
        public String insurance_cancelled_reason;
        public Decimal insurance_losses_number;
        public Decimal insurance_losses_amount;
        public String property_repossessed_f;
        public String bankrupcy_filed_f;
        public String pending_lawsuits_f;
        public String military_active_f;
        public String military_inactive_f;
        public String military_reserve_f;
    }

    public Map<Id, CreditApplicationResource> createCreditApplicationWrappers(List<Credit_Application__c> creditApplications) {
        Map<Id, CreditApplicationResource> creditApplicationResources = new Map<Id, CreditApplicationResource>();
        for (Credit_Application__c creditApplication : creditApplications) {
            CreditApplicationResource resource = new CreditApplicationResource();
            resource.gross_income  = creditApplication.Gross_Income__c;
            resource.gross_income_interval_c  = creditApplication.Gross_Income_Interval__c;
            resource.alimony_income  = creditApplication.Alimony_Income__c;
            resource.alimony_income_interval_c  = creditApplication.Alimony_Income_Interval__c;
            resource.alimony_income_court_order_f  = booleanToString(creditApplication.Alimony_Income_Court_Order__c);
            resource.alimony_income_written_agreement_f  = booleanToString(creditApplication.Alimony_Income_Written_Agreement__c);
            resource.alimony_income_verbal_agreement_f  = booleanToString(creditApplication.Alimony_Income_Verbal_Agreement__c);
            resource.other_income  = creditApplication.Other_Income__c;
            resource.other_income_description  = creditApplication.Other_Income_Description__c;
            resource.other_income_interval_c  = creditApplication.Other_Income_Interval__c;
            resource.education_level_c  = creditApplication.Education_Level__c;
            resource.marital_status_c  = creditApplication.Marital_Status__c;
            resource.number_of_dependants  = creditApplication.Number_of_Dependents__c;
            resource.dependant_1_age  = creditApplication.Dependent_1_Age__c;
            resource.dependant_2_age  = creditApplication.Dependent_2_Age__c;
            resource.dependant_3_age  = creditApplication.Dependent_3_Age__c;
            resource.personal_reference_1_name  = concatSafely(creditApplication.Personal_Reference_1_First_Name__c, creditApplication.Personal_Reference_1_Last_Name__c);
            resource.personal_reference_1_street  = creditApplication.Personal_Reference_1_Street__c;
            resource.personal_reference_1_city  = creditApplication.Personal_Reference_1_City__c;
            resource.personal_reference_1_state_c  = creditApplication.Personal_Reference_1_State__c;
            resource.personal_reference_1_postal_code  = creditApplication.Personal_Reference_1_Postal_Code__c;
            resource.personal_reference_1_phone_number  = creditApplication.Personal_Reference_1_Phone_Number__c;
            resource.personal_reference_1_relationship  = creditApplication.Personal_Reference_1_Relationship__c;
            resource.personal_reference_2_name  = concatSafely(creditApplication.Personal_Reference_2_First_Name__c, creditApplication.Personal_Reference_2_Last_Name__c);
            resource.personal_reference_2_street  = creditApplication.Personal_Reference_2_Street__c;
            resource.personal_reference_2_city  = creditApplication.Personal_Reference_2_City__c;
            resource.personal_reference_2_state_c  = creditApplication.Personal_Reference_2_State__c;
            resource.personal_reference_2_postal_code  = creditApplication.Personal_Reference_2_Postal_Code__c;
            resource.personal_reference_2_phone_number  = creditApplication.Personal_Reference_2_Phone_Number__c;
            resource.personal_reference_2_relationship  = creditApplication.Personal_Reference_2_Relationship__c;
            resource.personal_reference_3_name  = concatSafely(creditApplication.Personal_Reference_3_First_Name__c, creditApplication.Personal_Reference_3_Last_Name__c);
            resource.personal_reference_3_street  = creditApplication.Personal_Reference_3_Street__c;
            resource.personal_reference_3_city  = creditApplication.Personal_Reference_3_City__c;
            resource.personal_reference_3_state_c  = creditApplication.Personal_Reference_3_State__c;
            resource.personal_reference_3_postal_code  = creditApplication.Personal_Reference_3_Postal_Code__c;
            resource.personal_reference_3_phone_number  = creditApplication.Personal_Reference_3_Phone_Number__c;
            resource.personal_reference_3_relationship  = creditApplication.Personal_Reference_3_Relationship__c;
            resource.credit_reference_1_type  = creditApplication.Credit_Reference_1_Type__c;
            resource.credit_reference_1_closed_f  = booleanToString(creditApplication.Credit_Reference_1_Closed__c);
            resource.credit_reference_1_company_name  = creditApplication.Credit_Reference_1_Company__c;
            resource.credit_reference_1_account_number  = creditApplication.Credit_Reference_1_Account_Number__c;
            resource.credit_reference_1_address1  = creditApplication.Credit_Reference_1_Address_1__c;
            resource.credit_reference_1_address2  = creditApplication.Credit_Reference_1_Address_2__c;
            resource.credit_reference_1_current_balance  = creditApplication.Credit_Reference_1_Current_Balance__c;
            resource.credit_reference_1_high_balance  = creditApplication.Credit_Reference_1_High_Balance__c;
            resource.credit_reference_1_payment  = creditApplication.Credit_Reference_1_Payment__c;
            resource.credit_reference_1_closed_date  = formatDate(creditApplication.Credit_Reference_1_Closed_Date__c);
            resource.credit_reference_2_type  = creditApplication.Credit_Reference_2_Type__c;
            resource.credit_reference_2_closed_f  = booleanToString(creditApplication.Credit_Reference_2_Closed__c);
            resource.credit_reference_2_company_name  = creditApplication.Credit_Reference_2_Company__c;
            resource.credit_reference_2_account_number  = creditApplication.Credit_Reference_2_Account_Number__c;
            resource.credit_reference_2_address1  = creditApplication.Credit_Reference_2_Address_1__c;
            resource.credit_reference_2_address2  = creditApplication.Credit_Reference_2_Address_2__c;
            resource.credit_reference_2_current_balance  = creditApplication.Credit_Reference_2_Current_Balance__c;
            resource.credit_reference_2_high_balance  = creditApplication.Credit_Reference_2_High_Balance__c;
            resource.credit_reference_2_payment  = creditApplication.Credit_Reference_2_Payment__c;
            resource.credit_reference_2_closed_date  = formatDate(creditApplication.Credit_Reference_2_Closed_Date__c);
            resource.credit_reference_3_type  = creditApplication.Credit_Reference_3_Type__c;
            resource.credit_reference_3_closed_f  = booleanToString(creditApplication.Credit_Reference_3_Closed__c);
            resource.credit_reference_3_company_name  = creditApplication.Credit_Reference_3_Company__c;
            resource.credit_reference_3_account_number  = creditApplication.Credit_Reference_3_Account_Number__c;
            resource.credit_reference_3_address1  = creditApplication.Credit_Reference_3_Address_1__c;
            resource.credit_reference_3_address2  = creditApplication.Credit_Reference_3_Address_2__c;
            resource.credit_reference_3_current_balance  = creditApplication.Credit_Reference_3_Current_Balance__c;
            resource.credit_reference_3_high_balance  = creditApplication.Credit_Reference_3_High_Balance__c;
            resource.credit_reference_3_payment  = creditApplication.Credit_Reference_3_Payment__c;
            resource.credit_reference_3_closed_date  = formatDate(creditApplication.Credit_Reference_3_Closed_Date__c);
            resource.credit_reference_4_type  = creditApplication.Credit_Reference_4_Type__c;
            resource.credit_reference_4_closed_f  = booleanToString(creditApplication.Credit_Reference_4_Closed__c);
            resource.credit_reference_4_company_name  = creditApplication.Credit_Reference_4_Company__c;
            resource.credit_reference_4_account_number  = creditApplication.Credit_Reference_4_Account_Number__c;
            resource.credit_reference_4_address1  = creditApplication.Credit_Reference_4_Address_1__c;
            resource.credit_reference_4_address2  = creditApplication.Credit_Reference_4_Address_2__c;
            resource.credit_reference_4_current_balance  = creditApplication.Credit_Reference_4_Current_Balance__c;
            resource.credit_reference_4_high_balance  = creditApplication.Credit_Reference_4_High_Balance__c;
            resource.credit_reference_4_payment  = creditApplication.Credit_Reference_4_Payment__c;
            resource.credit_reference_4_closed_date  = formatDate(creditApplication.Credit_Reference_4_Closed_Date__c);
            resource.bank_reference_1_name  = creditApplication.Bank_Reference_1_Name__c;
            resource.bank_reference_1_account_number  = creditApplication.Bank_Reference_1_Account_Number__c;
            resource.bank_reference_1_account_type_c  = creditApplication.Bank_Reference_1_Account_Type__c;
            resource.bank_reference_1_branch  = creditApplication.Bank_Reference_1_Branch__c;
            resource.bank_reference_1_address  = creditApplication.Bank_Reference_1_Address__c;
            resource.bank_reference_1_balance  = creditApplication.Bank_Reference_1_Balance__c;
            resource.bank_reference_2_name  = creditApplication.Bank_Reference_2_Name__c;
            resource.bank_reference_2_account_number  = creditApplication.Bank_Reference_2_Account_Number__c;
            resource.bank_reference_2_account_type_c  = creditApplication.Bank_Reference_2_Account_Type__c;
            resource.bank_reference_2_branch  = creditApplication.Bank_Reference_2_Branch__c;
            resource.bank_reference_2_address  = creditApplication.Bank_Reference_2_Address__c;
            resource.bank_reference_2_balance  = creditApplication.Bank_Reference_2_Balance__c;
            resource.residence_type_c  = creditApplication.Residence_Type__c;
            resource.mortgage_company_name  = creditApplication.Mortgage_Company_Name__c;
            resource.mortgage_account_number  = creditApplication.Mortgage_Account_Number__c;
            resource.mortgage_street  = creditApplication.Mortgage_Street__c;
            resource.mortgage_city  = creditApplication.Mortgage_City__c;
            resource.mortgage_state_c  = creditApplication.Mortgage_State__c;
            resource.mortgage_zip  = creditApplication.Mortgage_Zip__c;
            resource.mortgage_purchase_date  = formatDate(creditApplication.Mortgage_Purchase_Date__c);
            resource.mortgage_home_age  = creditApplication.Mortgage_Home_Age__c;
            resource.mortgage_home_price  = creditApplication.Mortgage_Home_Price__c;
            resource.mortgage_home_market_value  = creditApplication.Mortgage_Home_Market_Value__c;
            resource.mortgage_balance  = creditApplication.Mortgage_Balance__c;
            resource.mortgage_monthly_payment  = creditApplication.Mortgage_Monthly_Payment__c;
            resource.mortgage_2nd_balance  = creditApplication.Mortgage_2nd_Balance__c;
            resource.mortgage_2nd_payment  = creditApplication.Mortgage_2nd_Payment__c;
            resource.monthly_rent  = creditApplication.Monthly_Rent__c;
            resource.current_vehicle_1_finance_company  = creditApplication.Current_Vehicle_1_Finance_Company__c;
            resource.current_vehicle_1_account_number  = creditApplication.Current_Vehicle_1_Account_Number__c;
            resource.current_vehicle_1_address  = creditApplication.Current_Vehicle_1_Address__c;
            resource.current_vehicle_1_payment  = creditApplication.Current_Vehicle_1_Payment__c;
            resource.current_vehicle_2_finance_company  = creditApplication.Current_Vehicle_2_Finance_Company__c;
            resource.current_vehicle_2_account_number  = creditApplication.Current_Vehicle_2_Account_Number__c;
            resource.current_vehicle_2_address  = creditApplication.Current_Vehicle_2_Address__c;
            resource.current_vehicle_2_payment  = creditApplication.Current_Vehicle_2_Payment__c;
            resource.insurance_company_name  = creditApplication.Insurance_Company_Name__c;
            resource.insurance_agent  = creditApplication.Insurance_Agent__c;
            resource.insurance_street  = creditApplication.Insurance_Street__c;
            resource.insurance_city  = creditApplication.Insurance_City__c;
            resource.insurance_state_c  = creditApplication.Insurance_State__c;
            resource.insurance_zip  = creditApplication.Insurance_Zip__c;
            resource.insurance_phone_number  = creditApplication.Insurance_Phone_Number__c;
            resource.insurance_policy_number  = creditApplication.Insurance_Policy_Number__c;
            resource.insurance_garaged_at  = creditApplication.Insurance_Garaged_At__c;
            resource.insurance_cancelled_f  = booleanToString(creditApplication.Insurance_Cancelled__c);
            resource.insurance_cancelled_reason  = creditApplication.Insurance_Cancelled_Reason__c;
            resource.insurance_losses_number  = creditApplication.Insurance_Losses_Number__c;
            resource.insurance_losses_amount  = creditApplication.Insurance_Losses_Amount__c;
            resource.property_repossessed_f  = booleanToString(creditApplication.Property_Repossessed__c);
            resource.bankrupcy_filed_f  = booleanToString(creditApplication.Bankruptcy_Filed__c);
            resource.pending_lawsuits_f  = booleanToString(creditApplication.Lawsuit_s_Pending__c);
            resource.military_active_f  = booleanToString(creditApplication.Active_Military__c);
            resource.military_inactive_f  = booleanToString(creditApplication.Inactive_Military__c);
            resource.military_reserve_f  = booleanToString(creditApplication.Military_Reserve__c);
            creditApplicationResources.put(creditApplication.Contact__c, resource);
        }
        return creditApplicationResources;
    }

    private String booleanToString(Boolean value) {
        return value ? 'Y' : 'N';
    }

    private String concatSafely(String value1, String value2) {
        String result = String.isBlank(value1) ? '' : value1;
        result += String.isBlank(value2) ? '' : ' ' + value2;
        return result;
    }

    private String formatDate(Date value) {
        if (value == null) {
            return null;
        }
        return addLeadingZero(value.day()) + '/' + addLeadingZero(value.month()) + '/' + value.year();
    }

    private String addLeadingZero(Integer value) {
        String result = '';
        result += value < 10 ? '0' : '';
        result += value;
        return result;
    }

}