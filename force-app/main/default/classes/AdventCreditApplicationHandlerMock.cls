@isTest
global class AdventCreditApplicationHandlerMock implements HttpCalloutMock {
		
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{ "success" : true, "data" : 1111}');
        res.setStatusCode(200);
        return res;
    }
}