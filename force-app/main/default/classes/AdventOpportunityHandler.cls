public class AdventOpportunityHandler extends AdventBaseHandler {

    public static String path = '/opportunity';

    public static String ID_PARAM = 'id';

    public static String VEHICLE_TYPE_NEW = 'New';
    public static String VEHICLE_TYPE_USED = 'Used';

    private AdventOpportunityHandler() {
    }

    public AdventOpportunityHandler(AdventApi api) {
        super(api);
    }

    public PostOpportunityResponse createOpp(OpportunityResource opr) {
        String requestBody = JSON.serialize(opr);
        System.debug('requestBody ' + requestBody);
        System.debug('path ' + path);
        HttpResponse res = api.post(path, requestBody);
        System.debug('res MAIN == before deserialize = ' + res.getBody());
        System.debug('Wrapper =    BEFORE REQUEST');
        PostOpportunityResponse wrapper = (PostOpportunityResponse) JSON.deserialize(res.getBody(), PostOpportunityResponse.class);
        System.debug('Wrapper =    ' + wrapper);
        return wrapper;
    }

    public PostOpportunityResponse updateOpp(OpportunityResource opr, Map<String,String> paramsMap) {
        String requestBody = JSON.serialize(opr);
        System.debug('requestBody ' + requestBody);
        System.debug('path ' + path);
        System.debug('paramsMap ' + paramsMap);
        HttpResponse res = api.put(path, requestBody, paramsMap);
        System.debug('res MAIN == before deserialize = ' + res.getBody());
        System.debug('Wrapper =    BEFORE REQUEST');
        PostOpportunityResponse wrapper = (PostOpportunityResponse) JSON.deserialize(res.getBody(), PostOpportunityResponse.class);
        System.debug('Wrapper =    ' + wrapper);

        return wrapper;
    }

    public OpportunityCobuyerResult addCobyerToOpportunity(String opportunityId, String cobuyerId) {
        HttpResponse res = api.post('/opportunity-cobuyer', JSON.serialize(new Map<String, String> {
            'opportunity_id' => opportunityId,
            'cobuyer_id' => cobuyerId
        }));
        System.debug('response ' + res.getBody());
        return (OpportunityCobuyerResult) JSON.deserialize(res.getBody(), OpportunityCobuyerResult.class);
    }

    public class OpportunityCobuyerResult {
        public String success;
    }

    public class OpportunityResource {
        public String client_id;
        // buyer
        public AdventApi.Flag business_f;
        public String business_name;
        public String business_contact_name;
        public String business_contact_position;
        public String business_dmv_license_number;
        public String business_duns_number;
        public Date business_establish_date;
        public String business_resale_number;
        public String business_tax_id_number;
        public String business_type_c;
        // individual
        public String individual_first_name;
        public String individual_gender_c;
        public String individual_last_name;
        public String individual_middle_name;
        public String individual_spouse;
        // cobuyer
        public AdventApi.Flag cobuyer_business_f;
        public String cobuyer_business_name;
        public String cobuyer_business_contact_name;
        public String cobuyer_business_contact_position;
        public String cobuyer_business_dmv_license_number;
        public String cobuyer_business_duns_number;
        public Date cobuyer_business_establish_date;
        public String cobuyer_business_resale_number;
        public String cobuyer_business_tax_id_number;
        public String cobuyer_business_type_c;
        public String cobuyer_primary_email_address;
        //individual cobuyer
        public String cobuyer_individual_first_name;
        public String cobuyer_individual_last_name;
        public String cobuyer_individual_middle_name;
        // interested vehicle
        public String interested_vehicle_color;
        public String interested_vehicle_make;
        public String interested_vehicle_model;
        public String interested_vehicle_stock_number;
        public Vehicle_Type interested_vehicle_type_c;
        public Integer interested_vehicle_year;
        // additional fields
        public String lead_parser_id; // undocumented
        public String work_phone;
        public String home_phone;
        public String fax_phone;
        public String cell_phone;
        public String primary_email_address;

        public String opportunity_type_c;
    }

    public static OpportunityResource buildOpportunityResource(String opportunityId, String clientId) {
        Opportunity opportunity = OppToAdventCtrl.getOppData(opportunityId);
        AdventOpportunityHandler.OpportunityResource opr = new AdventOpportunityHandler.OpportunityResource();
        opr.client_id = clientId;
        Contact buyer = OppToAdventCtrl.getBuyerData(opportunityId);
        Map<String, Contact> contactByRole = OppToAdventCtrl.getContactsByRole(opportunityId);

        if (opportunity.Is_Business__c) {
            opr.business_f = AdventApi.Flag.Y;

            Account acc = OppToAdventCtrl.getAccountData(opportunityId);
            opr.business_name = acc.Name;

            Contact cobuyer = OppToAdventCtrl.getCobuyerData(opportunityId);
            opr.cobuyer_business_f = AdventApi.Flag.Y;
            opr.cobuyer_business_name = cobuyer.Name;

        } else {
            opr.business_f = AdventApi.Flag.N;

            opr.individual_first_name = buyer.FirstName;
            opr.individual_last_name = buyer.LastName;
            opr.individual_middle_name = buyer.MiddleName;
            Contact cobuyer = contactByRole.get(OppToAdventCtrl.COBUYER_ROLE);
            if (cobuyer != null) {
                opr.cobuyer_individual_first_name = cobuyer.FirstName;
                opr.cobuyer_individual_last_name = cobuyer.LastName;
                opr.cobuyer_individual_middle_name = cobuyer.MiddleName;
            }
        }

        Vehicle__c vehicle = OppToAdventCtrl.getVehicleData(opportunity.Vehicle__c);
        if (vehicle != null) {
            opr.interested_vehicle_stock_number = vehicle.Stock_Number__c;
            // opr.interested_vehicle_color = vehicle.Exterior_Color__c;
            // opr.interested_vehicle_make = vehicle.Make__c;
            // opr.interested_vehicle_model = vehicle.Model__c;
            // if (vehicle.New_Used__c == VEHICLE_TYPE_NEW) {
            //     opr.interested_vehicle_type_c = AdventOpportunityHandler.Vehicle_Type.N;
            // } else if (vehicle.New_Used__c == VEHICLE_TYPE_USED) {
            //     opr.interested_vehicle_type_c = AdventOpportunityHandler.Vehicle_Type.U;
            // }
            // if (String.isNotBlank(vehicle.Year__c)) {
            //     opr.interested_vehicle_year = Integer.valueOf(vehicle.Year__c);
            // }

        }
        // fill in client data to identify later
        opr.work_phone = buyer.MobilePhone;
        opr.home_phone = buyer.HomePhone;
        opr.fax_phone = buyer.Fax;
        opr.cell_phone = buyer.Phone;
        opr.primary_email_address = buyer.Email;
        opr.opportunity_type_c = opportunity.DMS_Opportunity_Type__c;

        return opr;
    }

    // Interested vehicle new/used indicator
    public enum Vehicle_Type {
        N, U
    }

    public class PostOpportunityResponse {
        public Boolean success;
        public PostData data;
        public String message;
        public List<ErrorData> errors;
    }

    public class ErrorData {
        public String id;
        public String msg;
    }

    public class PostData {
        public String opportunity_id;
        public String client_id;
        public String opportunity_number;
        public String client_number;
        public String cobuyer_client_number;
    }

    public class PutOpportunityResponse {
        public Boolean success;
        public List<PutData> data;
    }

    public class PutData {

    }

}