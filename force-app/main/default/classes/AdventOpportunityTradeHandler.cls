public class AdventOpportunityTradeHandler extends AdventBaseHandler {

    public static String path = '/opportunity-trade';

    private AdventOpportunityTradeHandler() {
    }

    public AdventOpportunityTradeHandler(AdventApi api) {
        super(api);
    }

    public PostTradeResponse postTrade(TradeResource tr) {
        String requestBody = JSON.serialize(tr);
        HttpResponse res = api.post(path, requestBody);
        PostTradeResponse wrapper = (PostTradeResponse) JSON.deserialize(res.getBody(), PostTradeResponse.class);
        return wrapper;
    }

    public class PostTradeResponse {
        public Boolean success;
        public Data data;
    }

    public class Data {
        public String TradeUUID;
    }

    public class TradeResource {
        public String opportunity_id;
        public String TradeNumber;
        public String InvtVIN;
        public String InvtTypeVehicle;
        public String InvtTypeNU;
        public Integer InvtYYYY;
        public String InvtMakeCode;
        public String InvtMakeName;
        public String InvtModelName;
    }

}