/**
 * Created by Test on 10.12.2020.
 */

@IsTest
private class AlertMessagesCntrlTest {
    @TestSetup
    static void createTestData() {
        Account[] accounts = new List<Account>();
        accounts.add(new Account(
                Name = 'test Account'));
        insert accounts;
    }

    @IsTest
    static void controllerMethodsTest() {
        Account testAccount = [
                SELECT Id, Name
                FROM Account
                LIMIT 1
        ];
        String testMessage = 'message text';
        Alert__c newAlert = AlertMessagesController.createAlert(testAccount.Id, testMessage);
        System.assertEquals(newAlert.Alert_Message__c, testMessage);
        System.assertEquals(newAlert.Account__c, testAccount.Id);
        AlertMessagesController.Wrapper newWrapper = AlertMessagesController.getWrapper(testAccount.Id);
        System.assertEquals(newWrapper.accountName, testAccount.Name);
        AlertMessagesController.deleteAlert(newAlert.Id);
        Alert__c[] deletedAlert = [
                SELECT Id
                FROM Alert__c
                WHERE Id = :newAlert.Id
        ];
        System.assertEquals(deletedAlert.size(), 0);
    }
}