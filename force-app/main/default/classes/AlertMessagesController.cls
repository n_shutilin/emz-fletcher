public with sharing class AlertMessagesController {

    @AuraEnabled
    public static Wrapper getWrapper(String recordId) {
        Account currentAccount = [
            SELECT Name,
            (SELECT Alert_Message__c, CreatedBy.Name, CreatedDate, CreatedById
            FROM Alerts__r)
            FROM Account
            WHERE Id = :recordId
            LIMIT 1
        ];
        Wrapper newWrapper = new Wrapper(currentAccount);
        return newWrapper;
    }

    @AuraEnabled
    public static Alert__c createAlert(String recordId, String alertMessage) {
        Alert__c newAlert = new Alert__c(Account__c = recordId, Alert_Message__c = alertMessage, Active__c = true);
        Database.SaveResult sr = Database.insert(newAlert);
        if (sr.isSuccess()) {
            Alert__c createdAlert = [
                SELECT Alert_Message__c, CreatedBy.Name, CreatedDate, CreatedById, Account__r.Name
                FROM Alert__c
                WHERE Id = :newAlert.Id
                LIMIT 1
            ];
            return createdAlert;
        } else {
            return null;
        }
    }

    @AuraEnabled
    public static void deleteAlert(String alertId) {
        Alert__c alertToDelete = new Alert__c(Id = alertId);
        delete alertToDelete;
    }

    public class Wrapper {
        @AuraEnabled
        public List<Alert__c> messages;
        @AuraEnabled
        public String accountName;

        public Wrapper(Account currentAccount) {
            if (currentAccount.Alerts__r.isEmpty()) {
                this.messages = new List<Alert__c>();
            } else {
                this.messages = currentAccount.Alerts__r;
            }
            this.accountName = currentAccount.Name;
        }

    }
}