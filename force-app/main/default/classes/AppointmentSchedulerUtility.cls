public with sharing class AppointmentSchedulerUtility {
    private final static String VEHICLE_SERVICE_APPOINTMENT_LABEL = 'Vehicle Service Appointment';
    private final static String EMAIL_PARSE_REGEX = '\\{([^}]*)\\}';
    private final static String SPLIT_BY_DOT_REGEX = '\\.';
    private final static String CLOSING_CURLY_BRACE = '}';
    private final static String QUOTATION_MARK = '"';
    private final static String EMPTY_SPACE = ' ';
    private final static String SOQL_FIELD_DELIMITER = ', ';
    private final static Integer FIRST_INDEX = 0;
    private final static Integer SECOND_INDEX = 1;

    public Enum TimeZoneType { OPPERATION_HOURS, DEALER }


    // public static String getParsedEmailTemplateBody(String appointmentId, Boolean isUpdate){
    //     String emailBody = '';
    //     String emailTemplateId = getEmailTemplateIdByServiceAppointmentId(appointmentId, isUpdate);
    //     // String emailTemplateId = '00X3J000000E1M9';
    //     Map<String, Object> mapPlaceholderToValue = new Map<String, Object>();
    //     if(!String.isBlank(emailTemplateId)){
    //     emailBody = [SELECT HTMLValue FROM EmailTemplate WHERE Id = :emailTemplateId].HTMLValue;
    //         mapPlaceholderToValue = formMapOfPlaceHoldersToVluesForEmailTemplate(emailTemplateId, appointmentId);
    //         System.debug(mapPlaceholderToValue);
    //         if(!mapPlaceholderToValue.isEmpty()){
    //             emailBody = parseMapOnBody(mapPlaceholderToValue, emailBody);
    //         }
    //     }
    //     return emailBody;
    // }

    // public static void sendConfirmationEmail(List<String> toAddresses, List<String> ccAddresses, String appointmentId, String contactId, Boolean isUpdate){
    //     String emailTemplateId = getEmailTemplateIdByServiceAppointmentId(appointmentId, isUpdate);
    //     if(!String.isBlank(emailTemplateId)){
    //         Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
    //         semail.setTemplateId(emailTemplateId);
    //         semail.setToAddresses(toAddresses);
    //         semail.setCcAddresses(ccAddresses);
    //         semail.setWhatId(appointmentId);
    //         semail.setTargetObjectId(contactId);
    //         Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
    //     }
    // }

    // public static void sendConfirmationEmail(List<String> ccAddresses, String appointmentId, String contactId, Boolean isUpdate){
    //     String emailTemplateId = getEmailTemplateIdByServiceAppointmentId(appointmentId, isUpdate);
    //     if(!String.isBlank(emailTemplateId)){
    //         Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
    //         semail.setTemplateId(emailTemplateId);
    //         semail.setCcAddresses(ccAddresses);
    //         semail.setWhatId(appointmentId);
    //         semail.setTargetObjectId(contactId);
    //         Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
    //     }
    // }

    public static Integer calculateTimezoneDifferenceMinutes(String value, Boolean isSales) {
        String userTz = UserInfo.getTimeZone().toString();
        Integer userDif = timezoneToGMTDifference(userTz);
        String timeZone;
        if(isSales){
            timeZone = getOpHoursTimezone(value);
        }
        else{
            timeZone = getDealerTimezone(value);
        }
        Integer dealerDif = timezoneToGMTDifference(timeZone);

        return (dealerDif - userDif)/60000;
    }

    public static DateTime getStartDate(String dateString) {
        Date todayDate  = Date.valueOf(dateString);
        Time startTime  = Time.newInstance(0, 0, 0, 0);
        return DateTime.newInstance(todayDate, startTime);
    }

    public static DateTime getEndDate(String dateString) {
        Date todayDate  = Date.valueOf(dateString);
        Date endDate    = todayDate.addDays(4);
        Time endTime    = Time.newInstance(23, 59, 59, 0);
        
        return DateTime.newInstance(endDate, endTime);
    }
    
    // private static String getEmailTemplateIdByServiceAppointmentId(String appointmentId, Boolean isUpdate){
    //     ServiceAppointment serviceAppointment = [SELECT DealerId__c, RecordType.Name FROM ServiceAppointment WHERE Id = :appointmentId];
    //     String templateId = '';
    //     System.debug(isUpdate);
    //     System.debug(serviceAppointment.RecordType.Name);
    //     List<Confirmation_Email_Templates_Settings__mdt>  mdtList = [SELECT EmailTemplateId__c 
    //                                                        FROM Confirmation_Email_Templates_Settings__mdt 
    //                                                        WHERE Dealer__c = :serviceAppointment.DealerId__c 
    //                                                        AND Is_Rescheduled__c = :isUpdate
    //                                                        AND ServiceAppointmentType__c = :serviceAppointment.RecordType.Name LIMIT 1];
    //     System.debug(mdtList);                                                           
    //     if(!mdtList.isEmpty()){
    //         templateId = mdtList[FIRST_INDEX].EmailTemplateId__c;
    //     }
    //     else{
    //         mdtList = [SELECT EmailTemplateId__c FROM Confirmation_Email_Templates_Settings__mdt WHERE IsDefault__c = true LIMIT 1];
    //         if(!mdtList.isEmpty()){
    //              templateId = mdtList[FIRST_INDEX].EmailTemplateId__c;       
    //         }
    //     }
    //     return templateId;
    // }
    
    // private static List<String> formListOfFieldsBasedOnTemplate(List<String> plaecHolderList){
    //     List<String> fieldList = new List<String>();
    //     for(String placeholder : plaecHolderList){
    //         List<String> tempList = placeholder.replace(CLOSING_CURLY_BRACE,'').split(SPLIT_BY_DOT_REGEX);
    //         fieldList.add(tempList[SECOND_INDEX]);
    //     }
    //     List<String> validFieldList = validateFieldList(fieldList, plaecHolderList);
    //     return validFieldList;
    // }

    // private static List<String> formListOfPlaceHolders(String emailBody){
    //     List<String> allMatches = new List<String>();
    //     Matcher m = Pattern.compile(EMAIL_PARSE_REGEX).matcher(emailBody);
    //     while (m.find()) {
    //         allMatches.add(m.group());
    //     }
    //     return allMatches;
    // }
    
    // private static List<String> validateFieldList(List<String> fieldList, List<String> plaecHolderList) {
    //     List<String> validFieldList = new List<String>();
    //     List<Integer> indexListToRemove = new List<Integer>();
    //     Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	// 	Map <String, Schema.SObjectField> fieldMap = schemaMap.get('ServiceAppointment').getDescribe().fields.getMap();
    //     for(String field : fieldList) {
    //         if(fieldMap.containsKey(field.toLowerCase()) && !validFieldList.contains(field)) {
    //      		validFieldList.add(field);       
    //         } else {
    //             indexListToRemove.add(fieldList.indexOf(field));
    //         }
    //     }
    //     if(!indexListToRemove.isEmpty()){
    //         for(Integer index : indexListToRemove) {
    //         	plaecHolderList.remove(index);
    //         }
    //     }
    //     return validFieldList;
    // }

    // private static String fromSOQLQueryBasedOnEMailTemplateFieldList(List<String> fieldList, String appointmentId){
    //    String finalQuery = 'SELECT ';
    //    finalQuery = finalQuery + String.join(fieldList, SOQL_FIELD_DELIMITER);
    //    finalQuery = finalQuery + ' FROM ServiceAppointment WHERE Id = ' + '\'' + appointmentId + '\'';
    //    return finalQuery;
    // }

    // private static Map<String, Object> formMapOfPlaceHoldersToVluesForEmailTemplate(String emailTemplateId, String appointmentId){
    //     String emailBody = [SELECT Body FROM EmailTemplate WHERE Id = :emailTemplateId].Body;
    //     Map<String,Object> tempMap = new Map<String,Object>();
    //     List<String> placeHolderList = formListOfPlaceHolders(emailBody);
    //     if(!placeHolderList.isEmpty()){
    //     List<String> valuesList = formListOfFieldsBasedOnTemplate(placeHolderList);
    //         String query = fromSOQLQueryBasedOnEMailTemplateFieldList(valuesList, appointmentId);
    //         System.debug(query);
    //         if(!String.isBlank(query)) {
    //         	ServiceAppointment appointment = (ServiceAppointment) Database.query(query)[FIRST_INDEX];
    //         	for(Integer i = 0; i < placeHolderList.size(); i++){
    //             	tempMap.put(placeHolderList[i],appointment.get(valuesList[i]));
    //         	}
    //         }
    //     }
    //     return tempMap;
    // }

    // private static String parseMapOnBody(Map<String, Object> mapPlaceholderToValue, String emailBody){
    //     String tempBody = emailBody;
    //     for(String key: mapPlaceholderToValue.keySet()){
    //         if(mapPlaceholderToValue.get(key) == null){
    //             tempBody = tempBody.replace(key, EMPTY_SPACE);
    //         } else{
    //             tempBody = tempBody.replace(key, JSON.serialize(mapPlaceholderToValue.get(key)).replace(QUOTATION_MARK,''));
    //         }
    //     }
    //     return tempBody;
    // }

    private static String getDealerTimezone(String dealer) {
        return [SELECT OperatingHours.Timezone FROM ServiceTerritory WHERE IsActive = true AND Dealer__c =: dealer AND IsSalesTerritory__c = false].OperatingHours.Timezone;
    }

    private static String getOpHoursTimezone(String opHoursId) {
        String temp = [SELECT Timezone FROM OperatingHours WHERE Id = :opHoursId].Timezone;
        System.debug(temp);
        return [SELECT Timezone FROM OperatingHours WHERE Id = :opHoursId].Timezone;
    }

    private static Integer timezoneToGMTDifference(String timezone) {
        Timezone tz = System.TimeZone.getTimeZone(timezone);
        Datetime dttz = Datetime.now();

        return tz.getOffset(dttz);
    }
}