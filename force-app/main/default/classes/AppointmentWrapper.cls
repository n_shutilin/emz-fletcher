public with sharing class AppointmentWrapper{
    @AuraEnabled
    public ServiceAppointment appointment;
    @AuraEnabled
    public String url;
}