public with sharing class AssignAdvisorController {

    private class AssignAdvisorControllerResponse {
        private Integer code;
        private ServiceResource resource;
        private String errorMessage;
        private Object body;

        public AssignAdvisorControllerResponse () {
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public void setErrorMessage (String message) {
            this.errorMessage = message;
        }

        public void setBody (Object body) {
            this.body = body;
        }
    }

    @AuraEnabled
    public static String assignAdvisor(String resourceId, String appointmentId){
        AssignAdvisorControllerResponse response = new AssignAdvisorControllerResponse();
        ServiceResource resource = [SELECT Id, DefaultAdvisorTime__c FROM ServiceResource WHERE Id = :resourceId];
        ServiceAppointment serviceAppointment = [SELECT Id, SchedStartTime, SchedEndTime FROM ServiceAppointment WHERE Id = :appointmentId];
        String assignedResourceId = '';
        if(checkAppointmentAssigment(serviceAppointment)) {
            assignedResourceId = updateAssignedResource(serviceAppointment, resource);
        } else {
            assignedResourceId = insertAssignedResource(serviceAppointment, resource);
        }
        response.setCode(200);
        response.setBody(assignedResourceId);
        return JSON.serialize(response);
    }

    @AuraEnabled
    public static String getResourceByAppointmentId(String appointmentId){
        AssignAdvisorControllerResponse response = new AssignAdvisorControllerResponse();
        ServiceAppointment appointment = [SELECT Id, DealerId__c, RecordType.DeveloperName, Status  
                                          FROM ServiceAppointment 
                                          WHERE Id = :appointmentId];
        if(appointment.Status != 'Scheduled') {
            response.setCode(403);
            response.setErrorMessage('The appointment is not scheduled yet.');
            return JSON.serialize(response);
        }
        Boolean isSales;
        if(appointment.RecordType.DeveloperName != null) {
            isSales = appointment.RecordType.DeveloperName != 'VehicleServiceAppointment';
        } else {
            isSales = false;
        }
        ServiceResource resource = findResourceByDealer(appointment.DealerId__c, isSales);
        if(resource == null) {
            response.setCode(404);
            response.setErrorMessage('Resource not found, probably there are no active queues assigned to this appointment\'s territory.');
            return JSON.serialize(response);
        } else {
            if(checkAppointmentAssigment(appointment)) {
                ServiceResource assighetResource = getAssignetResource(appointment);
                response.setCode(200);
                response.setBody(new List<ServiceResource> {assighetResource, resource});
                return JSON.serialize(response);
            }
            response.setCode(200);
            response.setBody(resource);
            return JSON.serialize(response);
        }
    }

    private static ServiceResource getAssignetResource(ServiceAppointment appointment) {
        AssignedResource assignedResource = [SELECT Id, ServiceResourceId 
                                             FROM AssignedResource
                                             WHERE ServiceAppointmentId = :appointment.Id];
        return [SELECT Id, Name FROM ServiceResource WHERE Id = :assignedResource.ServiceResourceId];
    }

    private static Boolean checkResourceAssigment(ServiceResource resource) {
        List<AssignedResource> assignedResource = [SELECT Id 
                                                   FROM AssignedResource
                                                   WHERE ServiceResourceId = :resource.Id];
        return !assignedResource.isEmpty(); 
    }

    private static Boolean checkAppointmentAssigment(ServiceAppointment appointment)  {
        List<AssignedResource> assignedResource = [SELECT Id 
                                                   FROM AssignedResource
                                                   WHERE ServiceAppointmentId = :appointment.Id];
        return !assignedResource.isEmpty();                    
    }

    private static ServiceResource findResourceByDealer (String dealer, Boolean isSales) {
        List<Queue__c> queueList = [SELECT Id, Service_Territory_Id__c 
                                    FROM Queue__c 
                                    WHERE Dealer__c = :dealer AND isActive__c = true AND Service_Territory_Id__r.IsSalesTerritory__c = : isSales];
        if(!queueList.isEmpty()) {
            String resourceId = findReourceByQueueId(queueList[0].Id);
            if(resourceId != null) {
                return [SELECT Id, Name FROM ServiceResource WHERE Id = :resourceId];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private static String findReourceByQueueId(String queueId) {
        List<Resource_State_In_Queue__c> rsourceStateList = [SELECT Id, Service_Resource_Id__c 
                                                             FROM Resource_State_In_Queue__c
                                                             WHERE Queue_Id__c = :queueId AND Is_Actual_State__c = true AND Position__c = 1];
        if(!rsourceStateList.isEmpty()) {
            return rsourceStateList[0].Service_Resource_Id__c;
        } else {
            return null;
        }
    }

    private static void resceduleAppointment(ServiceAppointment serviceAppointment, DateTime startTime, DateTime endTime ) {
        serviceAppointment.SchedStartTime = startTime;
        serviceAppointment.SchedEndTime = endTime;
        update serviceAppointment;
    }

    private static String updateAssignedResource(ServiceAppointment appointment, ServiceResource resource) {
        DateTime startTime = appointment.SchedStartTime;
        DateTime endTime = appointment.SchedEndTime;
        AssignedResource assignedResource = [SELECT Id 
                                             FROM AssignedResource 
                                             WHERE ServiceAppointmentId = :appointment.Id];
        delete assignedResource;
        resceduleAppointment(appointment, startTime, endTime);
        return insertAssignedResource(appointment, resource);
    }

    private static String insertAssignedResource(ServiceAppointment appointment, ServiceResource resource) {
        AssignedResource assignedResource = new AssignedResource(ServiceAppointmentId = appointment.Id , 
                                                                 ServiceResourceId = resource.Id);
        insert assignedResource;
        return assignedResource.Id;
    }
}