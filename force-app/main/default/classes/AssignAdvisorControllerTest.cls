@IsTest
public class AssignAdvisorControllerTest {

    private class ExpectedWrapper {
        private Integer code;
        private ServiceResource resource;
        private String errorMessage;
        private Object body;

        public ExpectedWrapper () {
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public void setErrorMessage (String message) {
            this.errorMessage = message;
        }

        public void setBody (Object body) {
            this.body = body;
        }
    }

    @TestSetup
    static void init(){
        UpSystemTestDataProvider provider = new UpSystemTestDataProvider('CDKTS');
    }

    @IsTest
    static void getResourceByAppointmentIdTest(){
        //given
        ServiceAppointment appointment = [SELECT Id 
                                          FROM ServiceAppointment LIMIT 1];
        //when
        //then
        String rawData = AssignAdvisorController.getResourceByAppointmentId(appointment.Id);
    }

    @IsTest
    static void assignAdvisorTest(){
        //given
        ServiceAppointment appointment = [SELECT Id 
                                          FROM ServiceAppointment LIMIT 1];
        ServiceResource resource = [SELECT Id 
                                    FROM ServiceResource LIMIT 1];
        //when
        //then
        String rawData = AssignAdvisorController.assignAdvisor(resource.Id, appointment.Id);
    }
}