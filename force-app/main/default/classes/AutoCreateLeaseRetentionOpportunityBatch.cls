public with sharing class AutoCreateLeaseRetentionOpportunityBatch implements Database.Batchable<SObject>, Database.Stateful{
    public List<String> dealers;
    public Map<String, Decimal> autoCreateLeaseRetentionDaysMap;
    public Integer currentDealerIndex;

    public AutoCreateLeaseRetentionOpportunityBatch() {
        this.autoCreateLeaseRetentionDaysMap = getAutoCreateLeaseRetentionDaysMap();
        this.dealers = new List<String>(autoCreateLeaseRetentionDaysMap.keySet());
        this.currentDealerIndex = 0;
    }

    public AutoCreateLeaseRetentionOpportunityBatch(Integer index, List<String> dealersList, Map<String, Decimal> leaseRetentionDaysMap ) {
        this.autoCreateLeaseRetentionDaysMap = leaseRetentionDaysMap;
        this.dealers = dealersList;
        this.currentDealerIndex = index;
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        String currentDealer = dealers[currentDealerIndex];
        Decimal dealerLeaseRetentionDays = autoCreateLeaseRetentionDaysMap.get(currentDealer);
        Date requiredLeaseMaturityDate = Date.today().addDays( (Integer) dealerLeaseRetentionDays * (-1));

        List<Schema.SObjectType> objects = new List<Schema.SObjectType>{ Opportunity.SObjectType};
        List<String> fieldsList = new List<String>();
        for(Schema.SObjectType objType: objects){
            for(Schema.SObjectField fld: objType.getDescribe().fields.getMap().values()){
                fieldsList.add(fld.getDescribe().getName());
            }
        }
        String allFieldsString = String.join(fieldsList, ', ');

        String query = 'SELECT ' + allFieldsString + ' FROM Opportunity ' +
            'WHERE Dealer_ID__c =: currentDealer AND LeaseMaturityDate__c =: requiredLeaseMaturityDate';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Opportunity> oppList) {
        Id recTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByDeveloperName().get('Lease_Retention').getRecordTypeId();
        List<Opportunity> newOppsList = new List<Opportunity>();
        List<String> accountIds = new List<String>();
        for(Opportunity opp : oppList) {
            Opportunity newOpp = opp.clone(false, true, false, false);
            newOpp.recordTypeId = recTypeId;
            newOpp.Original_Opportunity_Id__c = opp.id;
            newOpp.StageName = 'New';
            newOpp.CloseDate = opp.CloseDate.addDays((Integer) autoCreateLeaseRetentionDaysMap.get(dealers[currentDealerIndex]));

            newOppsList.add(newOpp);
            accountIds.add(newOpp.AccountId);
        }
        insert newOppsList;

        Map<String, Account> accs = new Map<String, Account>([SELECT Id, PersonContactId FROM Account WHERE Id IN :accountIds AND IsPersonAccount = true]);
        Set<String> personAccsIds = accs.keySet();

        List<OpportunityContactRole> ocrList = new  List<OpportunityContactRole>();
        for(Opportunity newOp : newOppsList) {
            Boolean isPersonAcc = personAccsIds.contains(newOp.AccountId);
            if(isPersonAcc) {
                OpportunityContactRole ocr = new OpportunityContactRole(
                    ContactId = accs.get(newOp.AccountId).PersonContactId,
                    OpportunityId = newOp.Id,
                    IsPrimary = true,
                    Role = 'Buyer'
                );
                ocrList.add(ocr);
            }
        }
        insert ocrList;
    }

    public void finish(Database.BatchableContext bc) {
        if(++currentDealerIndex < dealers.size()) {
            Database.executeBatch(new AutoCreateLeaseRetentionOpportunityBatch(currentDealerIndex, dealers, autoCreateLeaseRetentionDaysMap));
        }
        
    }

    private Map<String, Decimal> getAutoCreateLeaseRetentionDaysMap() {
        Map<String, Decimal> dealersDateMap = new Map<String, Decimal>();
        Id recTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        Account[] dealersList = [SELECT Dealer_Code__c, Auto_Create_Lease_Retention_Days__c 
            FROM Account WHERE recordTypeId =:recTypeId AND Dealer_Code__c != null AND Auto_Create_Lease_Retention_Days__c != null];
        for( Account acc : dealersList ) {
            dealersDateMap.put(acc.Dealer_Code__c, acc.Auto_Create_Lease_Retention_Days__c);
        }
        return dealersDateMap;
    }
}