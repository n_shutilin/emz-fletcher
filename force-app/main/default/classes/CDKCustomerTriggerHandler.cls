public with sharing class CDKCustomerTriggerHandler {
    public static void updateSFUniqueIdentifierField(List<CDK_Customer__c> newList) {
        for (CDK_Customer__c customer : newList) {
            if(customer.Dealer_ID__c != null && customer.Customer_Control_Number__c !=null) {
                customer.SF_Unique_Identifier__c = customer.Dealer_ID__c + '_' + customer.Customer_Control_Number__c;
            }
        }
    }
}