public class CDKSalesChildTriggerHandler {
	
    public static void updateSFUniqueIdentifierField(List<WorkOrderLineItem> newList) {
        List<Account> accountList = [
        	SELECT CDK_Dealer_Code__c, Dealer_Code__c
            FROM Account
            WHERE RecordType.DeveloperName = :ServiceSalesClosedBatchable.ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null
        ];
        Map<String, Account> dealerCodeMap = new Map<String, Account>();
        Set<String> roNumberSet = new Set<String>();
        for (Account accountItem : accountList) {
            dealerCodeMap.put(accountItem.CDK_Dealer_Code__c, accountItem);
        }
        for (WorkOrderLineItem salesChild : newList) {
            if (salesChild.DealerId__c != null && dealerCodeMap.containsKey(salesChild.DealerId__c) && salesChild.RONumber__c != null && salesChild.HostItemID__c != null) {
                salesChild.UnuqueRONumber__c = dealerCodeMap.get(salesChild.DealerId__c).CDK_Dealer_Code__c + String.valueOf(salesChild.RONumber__c);
                salesChild.Unique_Identifier__c = salesChild.UnuqueRONumber__c + salesChild.HostItemID__c;
                roNumberSet.add(salesChild.UnuqueRONumber__c);
            }
        }
        List<WorkOrder> workOrders = [
            SELECT UnuqueRONumber__c
            FROM WorkOrder
            WHERE UnuqueRONumber__c IN :roNumberSet
        ];
        Map<String, Id> workOrderMap = new Map<String, Id>();
        for (WorkOrder wo : workOrders) {
            workOrderMap.put(wo.UnuqueRONumber__c, wo.Id);
        }

        for (WorkOrderLineItem salesChild : newList) {
            salesChild.WorkOrderId = workOrderMap.get(salesChild.UnuqueRONumber__c);
        }
    }
}