public class CDKSalesTriggerHandler {

    public static void updateSFUniqueIdentifierField(List<WorkOrder> newList) {
        List<Account> accountList = [
        	SELECT CDK_Dealer_Code__c, Dealer_Code__c
            FROM Account
            WHERE RecordType.DeveloperName = :ServiceSalesClosedBatchable.ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null
        ];
        Map<String, Account> dealerCodeMap = new Map<String, Account>();
        for (Account accountItem : accountList) {
            dealerCodeMap.put(accountItem.CDK_Dealer_Code__c, accountItem);
        }
        Id repOrderRecTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Repair Order').getRecordTypeId();
        List<String> advisorNumbers = new List<String>();
        for (WorkOrder sales : newList) {
            advisorNumbers.add(sales.Service_Advisor__c);

            sales.RecordTypeId= repOrderRecTypeId;
            if (sales.Dealer_ID__c != null && dealerCodeMap.containsKey(sales.Dealer_ID__c)) {
                sales.Dealer_ID__c = dealerCodeMap.get(sales.Dealer_ID__c).Dealer_Code__c;
                if (sales.RO_Number__c != null) {
                	sales.UnuqueRONumber__c = dealerCodeMap.get(sales.Dealer_ID__c).CDK_Dealer_Code__c + String.valueOf(workOrder.RO_Number__c);
                }
            }
        }

        Map<String, Map<String, Service_Advisor__c>> advisorsByAdvisorNumberMap = getAdvisorsByAdvisorNumber(advisorNumbers);

        for (WorkOrder sales : newList) {
            sales.SA_Name__c = advisorsByAdvisorNumberMap.get(sales.Dealer_ID__c)?.get(sales.Service_Advisor__c)?.Name;
        }
    }

    private static Map<String, Map<String, Service_Advisor__c>> getAdvisorsByAdvisorNumber(List<String> advisorNumbers) {
        Map<String, Map<String, Service_Advisor__c>> advisorsByAdvisorNumberMap = new Map<String, Map<String, Service_Advisor__c>>();

        List<Service_Advisor__c> advisors = [
            SELECT Id, Name, Service_Advisor_Number__c, Dealer__c
            FROM Service_Advisor__c
            WHERE Service_Advisor_Number__c IN :advisorNumbers
        ];

        for (Service_Advisor__c advisor : advisors) {
            if (advisorsByAdvisorNumberMap.get(advisor.Dealer__c) == NULL) {
                advisorsByAdvisorNumberMap.put(advisor.Dealer__c, new Map<String, Service_Advisor__c>{advisor.Service_Advisor_Number__c => advisor});
            } else {
                Map<String, Service_Advisor__c> dealerAdvisors = advisorsByAdvisorNumberMap.get(advisor.Dealer__c);
                dealerAdvisors.put(advisor.Service_Advisor_Number__c, advisor);
            }
            System.debug(advisorsByAdvisorNumberMap);
            System.debug('advisorsByAdvisorNumberMap');
            // advisorsByAdvisorNumberMap.put(advisor.Service_Advisor_Number__c, advisor);
        }

        return advisorsByAdvisorNumberMap;
    }
}