public with sharing class CheckNotReplyNeededButtonController {
    @AuraEnabled
    public static string updateEmailMessage(String recordId){
        try {
            EmailMessage email = [SELECT Id, No_Reply_Needed__c FROM EmailMessage WHERE Id = :recordId LIMIT 1];
            email.No_Reply_Needed__c = true;
            update email;
            return null;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        
    }
}