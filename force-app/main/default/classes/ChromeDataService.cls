public with sharing class ChromeDataService {

    public static final String SOAPENV = 'http://schemas.xmlsoap.org/soap/envelope/';
    public static final String URN = 'urn:description7b.services.chrome.com';

    public class VehicleResponseWrapper {
        public String Vin;
        public String Make;
        public String Model;
        public String Year;
        public String[] Images;
    }

    public static VehicleResponseWrapper getVehicleInfo(String vinNumber) {
        Chrome_Data_Integration__c settings = Chrome_Data_Integration__c.getOrgDefaults();

        String textBody =
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:description7b.services.chrome.com">'
                + '<soapenv:Header/>'
                + '<soapenv:Body>'
                + '<urn:VehicleDescriptionRequest>'
                + '<urn:accountInfo number="' + settings.AccountNumberFULL__c
                + '" secret="' + settings.AccountSecretFULL__c
                + '" country="' + settings.Country__c
                + '" language="' + settings.Language__c + '" behalfOf=""/>'
                + '<urn:vin>' + vinNumber + '</urn:vin>'
                + '<urn:includeMediaGallery>Multi-View</urn:includeMediaGallery>'
                + '</urn:VehicleDescriptionRequest>'
                + '</soapenv:Body>'
                + '</soapenv:Envelope>';

        HttpRequest request = new HttpRequest();
        request.setEndpoint(settings.Endpoint__c);
        request.setMethod('POST');
        request.setTimeout(60000);
        request.setHeader('Content-Type', 'text/xml');
        request.setBody(textBody);

        System.debug('~ ' + request);
        System.debug('~ ' + request.getBody());

        System.HttpResponse response;
        response = new System.Http().send(request);

        //System.debug(response.getBody());

        Dom.Document docResult = response.getBodyDocument();

        Dom.XmlNode envelopeResult = docResult.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement('Body', SOAPENV);
        Dom.XmlNode VehicleDescription = bodyResult.getChildElement('VehicleDescription', URN);
        if (VehicleDescription == null) {
            return null;
        }
        Dom.XmlNode responseStatus = VehicleDescription.getChildElement('responseStatus', URN);

        String responseCode = responseStatus.getAttribute('responseCode', null);

        System.debug('responseCode');
        System.debug(responseCode);

        if (responseCode != 'Successful') {
            return null;
        }

        Dom.XmlNode vinDescription = VehicleDescription.getChildElement('vinDescription', URN);
        String vin = vinDescription.getAttribute('vin', null);
        String modelYear = vinDescription.getAttribute('modelYear', null);
        String division = vinDescription.getAttribute('division', null);
        String modelName = vinDescription.getAttribute('modelName', null);

        Dom.XmlNode style = VehicleDescription.getChildElement('style', URN);
        Dom.XmlNode mediaGallery = style.getChildElement('mediaGallery', URN);

        String[] imageUrls = new List<String>();

        if (mediaGallery != null) {
            for (Dom.XmlNode view : mediaGallery.getChildElements()) {
                String backgroundDescription = view.getAttribute('backgroundDescription', null);
                String width = view.getAttribute('width', null);//width="320"
                String height = view.getAttribute('height', null);//height="240"
                String imgUrl = view.getAttribute('url', null);

                if (backgroundDescription == 'Transparent' && width == '320' && height == '240') {
                    imageUrls.add(imgUrl);
                }
            }
        } else {
            Dom.XmlNode stockImage = style.getChildElement('stockImage', URN);
            if(stockImage != null){
                String stockImageUrl = stockImage.getAttribute('url', null);
                imageUrls.add(stockImageUrl);
            }
        }

        VehicleResponseWrapper vehicleInfo = new VehicleResponseWrapper();
        vehicleInfo.Vin = vin;
        vehicleInfo.Make = division;
        vehicleInfo.Model = modelName;
        vehicleInfo.Year = modelYear;
        vehicleInfo.Images = imageUrls;

        return vehicleInfo;
    }
}