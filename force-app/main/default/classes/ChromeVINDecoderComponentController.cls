public class ChromeVINDecoderComponentController {
 public class ControllerResponse {
        @AuraEnabled public Integer code;
        @AuraEnabled public String body;

        public ControllerResponse(Integer code, String body){
            this.code = code;
            this.body = body;
        }
    }


    @AuraEnabled
    public static ControllerResponse getVin(String recordId, String objectType){
        String vinField;
        List<String> allowedObjectTypes = new List<String>{'Trade_In__c', 'CDK_Vehicle__c', 'Vehicle__c'};

        if (allowedObjectTypes.contains(objectType)){
            vinField = 'Vin__c';
        } else if (objectType == 'ServiceAppointment') {
            vinField = 'CDK_Vehicle_VIN__c';
        }

        if(vinField != null) {
            try{
                String query = 'SELECT Id, ' + vinField + ' FROM ' + objectType + ' WHERE Id = :recordId';
                List<SObject> result = Database.query(query);
                if(!result.isEmpty()){
                    return new ControllerResponse(202, String.valueOf(result.get(0).get(vinField)));
                } else {
                    return new ControllerResponse(400, 'Current object wasnt found.');
                }
            } catch (Exception e) {
                return new ControllerResponse(400, e.getMessage());
            }

        } else {
            return new ControllerResponse(400, objectType + ' object type is not allowed to this component.');
        }
    }
    
    @AuraEnabled
    public static ControllerResponse getVehicleInfo(String vin){
        ChromeDataService.VehicleResponseWrapper wrapper = ChromeDataService.getVehicleInfo(vin);
        if(wrapper != null){
            return new ControllerResponse(202, JSON.serialize(wrapper));
        } else {
            return new ControllerResponse(400, 'Something went wrong Probably VIN is incorrect.');
        }
    }
    
    @AuraEnabled
    public static ControllerResponse updateRecord(String recordId, String objectType, String data){
        try{
            ChromeDataService.VehicleResponseWrapper wrapper = (ChromeDataService.VehicleResponseWrapper)JSON.deserialize(data, ChromeDataService.VehicleResponseWrapper.class);
            SObject objectToUpdate;
            switch on objectType {
                when 'Trade_In__c' {
                    objectToUpdate = new Trade_In__c(
                        Id = recordId,
                    	Year__c = wrapper.Year,
                        Model__c = wrapper.Model,
                        Make__c = wrapper.Make);
                }	
                when 'CDK_Vehicle__c'{
                    objectToUpdate = new CDK_Vehicle__c(
                        Id = recordId,
                    	Year__c = wrapper.Year,
                        Model__c = wrapper.Model,
                        Make__c = wrapper.Make);
                }
                when 'Vehicle__c'{
                     objectToUpdate = new Vehicle__c(
                        Id = recordId,
                     	Year__c = wrapper.Year,
                        Model__c = wrapper.Model,
                        Make__c = wrapper.Make);
                }
            }
            update objectToUpdate;
            return new ControllerResponse(202, '');  
        } catch (Exception e){
            return new ControllerResponse(400, e.getMessage());
        }
    }
}