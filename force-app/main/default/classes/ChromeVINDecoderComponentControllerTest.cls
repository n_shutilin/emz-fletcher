@IsTest
public class ChromeVINDecoderComponentControllerTest {
    private static final String TEST_VIN = '19XFC2F74GE085105';

    @TestSetup
    static void init(){
        Chrome_Data_Integration__c settings = new Chrome_Data_Integration__c(
            Endpoint__c = 'test',
            AccountNumberFULL__c = 'test',
            AccountSecretFULL__c = 'test',
            Country__c = 'te',
            Language__c = 'te'
        );
        insert settings;

        Trade_In__c testTradeIn = new Trade_In__c(
            Vin__c = TEST_VIN
        );

        insert testTradeIn;

        CDK_Vehicle__c testCDKVehicle = new CDK_Vehicle__c(
            Vin__c = TEST_VIN
        );
        insert testCDKVehicle;

        Vehicle__c testVehicle = new Vehicle__c(
            Vin__c = TEST_VIN
        );
        insert testVehicle;

    }
    
    @IsTest
    static void getVinTest(){
        Trade_In__c tradeIn = [SELECT Id FROM Trade_In__c LIMIT 1];
        String actual = ChromeVINDecoderComponentController.getVin(tradeIn.Id, 'Trade_In__c').body;
        String expected = TEST_VIN;
        System.assertEquals(expected, actual);
    }

    @IsTest
    static void getVehicleInfoTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ChromeVINDecoderTestMock());
        ChromeVINDecoderComponentController.ControllerResponse response =  ChromeVINDecoderComponentController.getVehicleInfo(TEST_VIN);
        Test.stopTest();
        Integer actual  = response.code;
        Integer expected = 202;
        System.assertEquals(expected, actual);
    }

    @IsTest
    static void updateRecordTest(){
        Trade_In__c tradeIn = [SELECT Id FROM Trade_In__c LIMIT 1];
        CDK_Vehicle__c testCDKVehicle = [SELECT Id FROM CDK_Vehicle__c LIMIT 1];
        Vehicle__c testVehicle = [SELECT Id FROM Vehicle__c LIMIT 1];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ChromeVINDecoderTestMock());
        ChromeVINDecoderComponentController.ControllerResponse response =  ChromeVINDecoderComponentController.getVehicleInfo(TEST_VIN);
        Test.stopTest();
        response = ChromeVINDecoderComponentController.updateRecord(tradeIn.Id, 'Trade_In__c', JSON.serialize(response));
        response = ChromeVINDecoderComponentController.updateRecord(testCDKVehicle.Id, 'CDK_Vehicle__c', JSON.serialize(response));
        response = ChromeVINDecoderComponentController.updateRecord(testVehicle.Id, 'Vehicle__c', JSON.serialize(response));
        Integer actual  = response.code;
        Integer expected = 202;
        System.assertEquals(expected, actual);
    }
}