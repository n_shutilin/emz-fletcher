global class ChromeVINDecoderTestMock implements HttpCalloutMock{
    private static final String RESPONSE_BODY = 
        '<?xml version=\'1.0\' encoding=\'UTF-8\'?>' +
        '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<S:Body>' +
                '<VehicleDescription xmlns="urn:description7b.services.chrome.com" country="US" language="en" modelYear="2016" bestMakeName="Honda" bestModelName="Civic Sedan" bestStyleName="4dr CVT EX" bestTrimName="EX">' +
                    '<responseStatus responseCode="Successful" description="Successful"/>' +
                    '<vinDescription vin="19XFC2F74GE085105" modelYear="2016" division="Honda" modelName="Civic Sedan" styleName="4dr CVT EX" bodyType="Sedan 4 Dr." builddata="no">' +
                    '</vinDescription>' +
                    '<style id="381761" modelYear="2016" name="4dr CVT EX" nameWoTrim="4dr CVT" trim="EX" mfrModelCode="FC2F7GJW" fleetOnly="false" modelFleet="false" passDoors="4" altModelName="Civic" altStyleName="EX CVT Sedan" altBodyType="4dr Car" drivetrain="Front Wheel Drive">' +
                        '<division id="16">Honda</division>' +
                        '<subdivision id="8541">Honda</subdivision>' +
                        '<model id="29036">Civic Sedan</model>' +
                        '<basePrice unknown="false" invoice="19553.83" msrp="21040.0" destination="875.0"/>' +
                        '<bodyType primary="true" id="2">4dr Car</bodyType>' +
                        '<marketClass id="44">4-door Mid-Size Passenger Car</marketClass>' +
                        '<stockImage filename="23556.jpg" url="http://media.chromedata.com/autoBuilderData/stockPhotos/23556.jpg" width="400" height="200"/>' +
                        '<acode>USC60HOC021B0</acode>' +
                    '</style>' +
                '</VehicleDescription>' +
            '</S:Body>' +
        '</S:Envelope>';

    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setBody(RESPONSE_BODY);
        response.setStatusCode(200);
        return response;
    }
}