public with sharing class ClientToAdventCtrl {
    
    public static void createClient(String contactId) {
        List<Contact> contacts = [
            SELECT Birthdate, Name, HomePhone, Phone
            FROM Contact
            WHERE Id = :contactId
        ];
        Contact contactItem;
        if (!contacts.isEmpty()) {
            contactItem = contacts.get(0);
        } else {
            return;
        }
        List<OpportunityContactRole> contactRoles = [
            SELECT Opportunity.Dealer_ID__c
            FROM OpportunityContactRole
            WHERE ContactId = :contactId
        ];
        String dealerId;
        if (!contactRoles.isEmpty()) {
            dealerId = contactRoles.get(0).Opportunity.Dealer_ID__c;
        } else {
            return;
        }
        List<Account> accounts = [
            SELECT Advent_Store_Number__c  
            FROM Account 
            WHERE Dealer_Code__c = :dealerId
            AND Advent_Store_Number__c != NULL
        ];
        String adventNumber;
        if (!accounts.isEmpty()) {
            adventNumber = accounts.get(0).Advent_Store_Number__c;
        } else {
            return;
        }
        Vendor__c vendor = Vendor__c.getOrgDefaults();
        String vendorId = vendor.Id__c;
        AdventApi api = new AdventApi(vendorId, adventNumber);
        AdventClientHandler.ClientResource client = new AdventClientHandler.ClientResource();
        //client.birthdate = contactItem.Birthdate;
        client.home_phone = contactItem.HomePhone;
        client.individual_first_name = contactItem.Name.split(' ').get(0);
        client.individual_last_name = contactItem.Name.split(' ').get(1);
        client.cell_phone = contactItem.Phone;
        System.debug(api.post('/client', JSON.serialize(client)));
    }

}