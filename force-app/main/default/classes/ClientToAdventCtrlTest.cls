@IsTest
public class ClientToAdventCtrlTest {

    @IsTest
    public static void testCreateClient() {
        Opportunity opportunity = new Opportunity(
            Name = 'Test Opportunity',
            StageName = 'New',
            CloseDate = Date.today().addMonths(1),
            Dealer_ID__c = 'AUDFJ'
        );
        insert opportunity;

        Contact contact = new Contact(
            FirstName = 'Test',
            LastName = 'Contact'
        );
        insert contact;

        OpportunityContactRole opportunityContactRole = new OpportunityContactRole(
            OpportunityId = opportunity.Id,
            ContactId = contact.Id
        );
        insert opportunityContactRole;

        Account account = new Account(
            Name = 'Test',
            Dealer_Code__c = 'AUDFJ',
            Advent_Store_Number__c = '123'
        );
        insert account;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new AdventMockHttpResponse());
        ClientToAdventCtrl.createClient(contact.Id);
        Test.stopTest();
    }
}