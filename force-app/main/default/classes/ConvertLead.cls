public class ConvertLead {
        private final Lead lObj;
        
        public ConvertLead (ApexPages.StandardController stdController)  {   
            this.lObj = (Lead)stdController.getRecord();                  
            }
            
      public PageReference DoConvert()  { 
                
                //If the lead has a phone number, strip everything except the numbers
                string LeadPhone;
                if(lObj.Phone != NULL){
                LeadPhone = lObj.Phone.replaceAll('[^0-9]','');
                }
                
                //Check for existing person accounts that match the lead name and phone or email
                List<Account> DupeId = [Select Id, PersonEmail, Phone from Account where IsPersonAccount = True and FirstName = :lObj.FirstName and LastName = :lObj.LastName and (Phone = :LeadPhone OR PersonEmail = :lObj.Email) LIMIT 1];                       
                
                //Get the appropriate converted status for leads
                LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];                                               
                
                //Create the lead conversion method
                Database.LeadConvert lc = new database.LeadConvert();
                lc.setLeadId(lObj.Id);
                // If a duplicate person account was found, convert the lead into that account
                Account HouseholdAcc;
                Account CustomerAcc;
                if( DupeId.size() > 0){
                    lc.setAccountId(DupeId[0].Id);
                } 
                //If a duplicate account was not found, create a dealer level account and a household account.
                else {
                    //Create FJ Account record
                    CustomerAcc = New Account();
                    CustomerAcc.Name = lObj.LastName + ' ' + 'Household';
                    CustomerAcc.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('FJAG Account').RecordTypeId;
                    CustomerAcc.Dealer__c = lObj.Dealer_Id__c;
                    insert CustomerAcc;
                    //Create Household Account Record
                    HouseholdAcc = New Account();
                    HouseholdAcc.Name = lObj.FirstName + ' ' + lObj.LastName;
                    HouseholdAcc.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Household').RecordTypeId;
                    HouseholdAcc.ParentId = CustomerAcc.Id;
                    HouseholdAcc.Dealer__c = lObj.Dealer_Id__c;
                    insert HouseholdAcc;
                }
                
                lc.setConvertedStatus(convertStatus.MasterLabel);
                //Convert the lead               
                Database.LeadConvertResult lcr = Database.convertLead(lc);
                System.assert(lcr.isSuccess());                                 
                
                //If a customer account was created, set the Person Account's parent customer account to that account
                if(HouseholdAcc != NULL){
                    //Get the Account record that was created as a result of the conversion 
                    Account ConvertedAccount = [Select Id, Parent_Customer_Account__c from Account WHERE Id = :lcr.getAccountId()];
                    //Set the parent customer account field
                    ConvertedAccount.Parent_Customer_Account__c = HouseholdAcc.Id;
                    update ConvertedAccount;
                }        
                Id oppId = lcr.getOpportunityId();
                System.debug('OPPID: '+oppId);
                //String sServerName = ApexPages.currentPage().getHeaders().get('Host');
               // sServerName = 'https://' + sServerName + '/';

               // System.debug('REDIRECTING TO:'+sServerName+oppId);
                PageReference newPage =  new pagereference('/'+oppId);
               
                newPage.setRedirect(true);
                return newPage ;
           }           
    
}