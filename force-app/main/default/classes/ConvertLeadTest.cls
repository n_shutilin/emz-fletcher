@IsTest
public class ConvertLeadTest {
	
    @IsTest
    static void testMethod1(){
        
        Lead l = New Lead();
        l.LastName = 'Test';
        l.Phone = string.valueof(1231231223);
        insert l;
    
    PageReference pageRef = Page.CustomLeadConvert;
	Test.setCurrentPage(pageRef);
	pageRef.getParameters().put('Id', String.valueOf(l.Id));
	ApexPages.StandardController sc = new ApexPages.StandardController(l);
    ConvertLead cl = New ConvertLead(sc);
    cl.DoConvert();
    }
}