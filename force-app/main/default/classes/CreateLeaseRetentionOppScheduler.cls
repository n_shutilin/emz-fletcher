public with sharing class CreateLeaseRetentionOppScheduler implements Schedulable {
    public static String sched = '0 00 00 * * ?';

    public static String scheduleLeaseRetentionCreation() {
        CreateLeaseRetentionOppScheduler SC = new CreateLeaseRetentionOppScheduler(); 
        String jobName = 'Auto Create Lease Retention Opportunities ' + Date.today().format();
        return System.schedule(jobName, sched, SC);
    }

    public void execute(SchedulableContext SC) {
        AutoCreateLeaseRetentionOpportunityBatch batch = new AutoCreateLeaseRetentionOpportunityBatch();
        ID batchprocessid = Database.executeBatch(batch);
       
    }
}