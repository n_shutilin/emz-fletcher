@isTest
private class CreditApplicationArchive_BatchableTest
{
    private static testMethod void myTestMethod()
    {

    Account acc = New Account();
    acc.Name = 'Test Account';
    insert acc;
    
    Opportunity opp = New Opportunity();
    opp.Name = 'Test Opp';
    opp.AccountId = acc.Id;
    opp.CloseDate = system.today() ;
    opp.StageName = 'Registered Sale';
    insert opp;
    
    Credit_Application__c ca = new Credit_Application__c();
    ca.opportunity__c = opp.id;
    insert ca;
    
        
       
         Test.startTest();
        
             String Query = 'Select a.opportunity__r.Accountid,a.Active_Military__c,a.Alimony_Income__c,a.Alimony_Income_Annually__c,a.Alimony_Income_Court_Order__c,a.Alimony_Income_Interval__c,a.Alimony_Income_Monthly__c,a.Alimony_Income_Verbal_Agreement__c,a.Alimony_Income_Written_Agreement__c,a.Bank_Reference_1_Account_Number__c,a.Bank_Reference_1_Account_Type__c,a.Bank_Reference_1_Address__c,a.Bank_Reference_1_Balance__c,a.Bank_Reference_1_Branch__c,a.Bank_Reference_1_Name__c,a.Bank_Reference_2_Account_Number__c,a.Bank_Reference_2_Account_Type__c,a.Bank_Reference_2_Address__c,a.Bank_Reference_2_Balance__c,a.Bank_Reference_2_Branch__c,a.Bank_Reference_2_Name__c,a.Bank_Reference_3_Account_Number__c,a.Bank_Reference_3_Account_Type__c,a.Bank_Reference_3_Address__c,a.Bank_Reference_3_Balance__c,a.Bank_Reference_3_Name__c,a.Bankruptcy_Filed__c,a.Contact__c,a.CreatedById,a.CreatedDate,a.Credit_Reference_1_Account_Number__c,a.Credit_Reference_1_Address_1__c,a.Credit_Reference_1_Address_2__c,a.Credit_Reference_1_Closed__c,a.Credit_Reference_1_Closed_Date__c,a.Credit_Reference_1_Company__c,a.Credit_Reference_1_Current_Balance__c,a.Credit_Reference_1_High_Balance__c,a.Credit_Reference_1_Opened_Date__c,a.Credit_Reference_1_Payment__c,a.Credit_Reference_1_Term__c,a.Credit_Reference_1_Type__c,a.Credit_Reference_2_Account_Number__c,a.Credit_Reference_2_Address_1__c,a.Credit_Reference_2_Address_2__c,a.Credit_Reference_2_Closed__c,a.Credit_Reference_2_Closed_Date__c,a.Credit_Reference_2_Company__c,a.Credit_Reference_2_Current_Balance__c,a.Credit_Reference_2_High_Balance__c,a.Credit_Reference_2_Opened_Date__c,a.Credit_Reference_2_Payment__c,a.Credit_Reference_2_Term__c,a.Credit_Reference_2_Type__c,a.Credit_Reference_3_Account_Number__c,a.Credit_Reference_3_Address_1__c,a.Credit_Reference_3_Address_2__c,a.Credit_Reference_3_Closed__c,a.Credit_Reference_3_Closed_Date__c,a.Credit_Reference_3_Company__c,a.Credit_Reference_3_Current_Balance__c,a.Credit_Reference_3_High_Balance__c,a.Credit_Reference_3_Opened_Date__c,a.Credit_Reference_3_Payment__c,a.Credit_Reference_3_Term__c,a.Credit_Reference_3_Type__c,a.Credit_Reference_4_Account_Number__c,a.Credit_Reference_4_Address_1__c,a.Credit_Reference_4_Address_2__c,a.Credit_Reference_4_Closed__c,a.Credit_Reference_4_Closed_Date__c,a.Credit_Reference_4_Company__c,a.Credit_Reference_4_Current_Balance__c,a.Credit_Reference_4_High_Balance__c,a.Credit_Reference_4_Opened_Date__c,a.Credit_Reference_4_Payment__c,a.Credit_Reference_4_Term__c,a.Credit_Reference_4_Type__c,a.Current_Address_City__c,a.Current_Address_Country__c,a.Current_Address_Months__c,a.Current_Address_State__c,a.Current_Address_Street_2__c,a.Current_Address_Street__c,a.Current_Address_Years__c,a.Current_Address_Zipcode__c,a.Current_Employer_Address_1__c,a.Current_Employer_Address_2__c,a.Current_Employer_Badge_Number__c,a.Current_Employer_City__c,a.Current_Employer_Country__c,a.Current_Employer_Months__c,a.Current_Employer_Name__c,a.Current_Employer_Phone__c,a.Current_Employer_Position__c,a.Current_Employer_State__c,a.Current_Employer_Years__c,a.Current_Employer_Zipcode__c,a.Current_Vehicle_1_Account_Number__c,a.Current_Vehicle_1_Address__c,a.Current_Vehicle_1_Closed_Date__c,a.Current_Vehicle_1_Finance_Company__c,a.Current_Vehicle_1_Name__c,a.Current_Vehicle_1_Opened_Date__c,a.Current_Vehicle_1_Payment__c,a.Current_Vehicle_1_Term__c,a.Current_Vehicle_1_Type__c,a.Current_Vehicle_2_Account_Number__c,a.Current_Vehicle_2_Address__c,a.Current_Vehicle_2_Closed_Date__c,a.Current_Vehicle_2_Finance_Company__c,a.Current_Vehicle_2_Name__c,a.Current_Vehicle_2_Opened_Date__c,a.Current_Vehicle_2_Payment__c,a.Current_Vehicle_2_Term__c,a.Current_Vehicle_2_Type__c,a.Date_of_Birth__c,a.Dependent_1_Age__c,a.Dependent_2_Age__c,a.Dependent_3_Age__c,a.Driver_License_Country__c,a.Driver_License_Number__c,a.Driver_Licnese_State__c,a.Education_Level__c,a.Email_Address__c,a.Employment_Income_Annually__c,a.Employment_Income_Monthly__c,a.First_Name__c,a.Gross_Income__c,a.Gross_Income_Interval__c,a.Home_Phone__c,a.Id,a.Inactive_Military__c,a.Insurance_Agent__c,a.Insurance_Cancelled__c,a.Insurance_Cancelled_Reason__c,a.Insurance_City__c,a.Insurance_Company_Name__c,a.Insurance_Garaged_At__c,a.Insurance_Losses_Amount__c,a.Insurance_Losses_Number__c,a.Insurance_Phone_Number__c,a.Insurance_Policy_Number__c,a.Insurance_State__c,a.Insurance_Street__c,a.Insurance_Zip__c,a.IsDeleted,a.Last_Name__c,a.Lawsuit_s_Pending__c,a.Marital_Status__c,a.Middle_Name__c,a.Military_Reserve__c,a.Monthly_Rent__c,a.Mortgage_2nd_Balance__c,a.Mortgage_2nd_Payment__c,a.Mortgage_Account_Number__c,a.Mortgage_Balance__c,a.Mortgage_City__c,a.Mortgage_Company_Name__c,a.Mortgage_Home_Age__c,a.Mortgage_Home_Market_Value__c,a.Mortgage_Home_Price__c,a.Mortgage_Monthly_Payment__c,a.Mortgage_Purchase_Date__c,a.Mortgage_State__c,a.Mortgage_Street__c,a.Mortgage_Zip__c,a.Name,a.Number_of_Dependents__c,a.Opportunity__c,a.Other_Income__c,a.Other_Income_Annually__c,a.Other_Income_Description__c,a.Other_Income_Interval__c,a.Other_Income_Monthly__c,a.Other_Income_Source__c,a.Peresonal_Reference_1_Phone__c,a.Personal_Reference_1_City__c,a.Personal_Reference_1_Country__c,a.Personal_Reference_1_First_Name__c,a.Personal_Reference_1_Last_Name__c,a.Personal_Reference_1_Phone_Number__c,a.Personal_Reference_1_Postal_Code__c,a.Personal_Reference_1_Relationship__c,a.Personal_Reference_1_State__c,a.Personal_Reference_1_Street_2__c,a.Personal_Reference_1_Street__c,a.Personal_Reference_2_City__c,a.Personal_Reference_2_Country__c,a.Personal_Reference_2_First_Name__c,a.Personal_Reference_2_Last_Name__c,a.Personal_Reference_2_Phone_Number__c,a.Personal_Reference_2_Postal_Code__c,a.Personal_Reference_2_Relationship__c,a.Personal_Reference_2_State__c,a.Personal_Reference_2_Street_2__c,a.Personal_Reference_2_Street__c,a.Personal_Reference_3_City__c,a.Personal_Reference_3_Country__c,a.Personal_Reference_3_First_Name__c,a.Personal_Reference_3_Last_Name__c,a.Personal_Reference_3_Phone_Number__c,a.Personal_Reference_3_Postal_Code__c,a.Personal_Reference_3_Relationship__c,a.Personal_Reference_3_State__c,a.Personal_Reference_3_Street_2__c,a.Personal_Reference_3_Street__c,a.Previous_Address_1_City__c,a.Previous_Address_1_Country__c,a.Previous_Address_1_Months__c,a.Previous_Address_1_State__c,a.Previous_Address_1_Street_2__c,a.Previous_Address_1_Street__c,a.Previous_Address_1_Years__c,a.Previous_Address_1_Zipcode__c,a.Previous_Address_2_City__c,a.Previous_Address_2_Country__c,a.Previous_Address_2_Months__c,a.Previous_Address_2_State__c,a.Previous_Address_2_Street_2__c,a.Previous_Address_2_Street__c,a.Previous_Address_2_Years__c,a.Previous_Employer_Address_1__c,a.Previous_Employer_Address_2__c,a.Previous_Employer_Badge_Number__c,a.Previous_Employer_City__c,a.Previous_Employer_Country__c,a.Previous_Employer_Months__c,a.Previous_Employer_Name__c,a.Previous_Employer_Phone__c,a.Previous_Employer_Position__c,a.Previous_Employer_State__c,a.Previous_Employer_Years__c,a.Previous_Employer_Zipcode__c,a.Property_Repossessed__c,a.Residence_Type__c,a.SSN_Number__c '

                +  'from Credit_Application__c a where createddate >= LAST_N_DAYS:180 order by CreatedDate ASC';
        
         CreditApplicationArchive_Batchable ClassObj = new  CreditApplicationArchive_Batchable(Query);
        database.executeBatch(ClassObj, 10);
        Test.stopTest();}
        
            private static testMethod void myAccountFjidHandlerTrigger_BatchableTest()
    {
        Test.startTest();
            Datetime dt = Datetime.now().addMinutes(1);
            String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
            CreditApplicationArchive_Schedulable ClassObj = new CreditApplicationArchive_Schedulable();
            system.schedule('Test Method', CRON_EXP, ClassObj);   
        Test.stopTest();
    }
        
             

    }