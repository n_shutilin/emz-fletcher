public with sharing class CreditApplicationLWCController {
    @AuraEnabled(cacheable=true)
    public static String getPicklistValues(String fieldAPIName) {
        return JSON.serialize(schema.getGlobalDescribe().get('Credit_Application__c')
            .getDescribe().fields.getMap().get(fieldAPIName).
            getDescribe().getSObjectField().getDescribe().getPicklistValues());
    }

    @AuraEnabled
    public static Account getContactByRole(String oppId, String role) {
    // public static Contact getContactByRole(String oppId, String role) {
        List<OpportunityContactRole> ocr = [SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId = :oppId AND Role = :role LIMIT 1];

        if(ocr.isEmpty()) {
            return null;
        } else {
            String contactId = ocr[0].ContactId;

            Account personAccount = [SELECT Id, PersonContactId , FirstName, MiddleName, LastName, PersonHomePhone, PersonEmail, PersonBirthdate,
                BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode 
                FROM account 
                WHERE PersonContactId = :contactId LIMIT 1];

            return personAccount;
            // Contact cont = [SELECT Id, FirstName, MiddleName, LastName, HomePhone, Email, Birthdate,
            //     BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode
            //     FROM Contact WHERE Id = :contactId LIMIT 1];

            // return cont;
        }
    }
        
    @AuraEnabled
    public static void saveCreditApplications(String applicantJSON, String coapplicantJSON){
        List<Credit_Application__c> creditApplList = new List<Credit_Application__c>();

        Credit_Application__c applicant = (Credit_Application__c) JSON.deserialize(applicantJSON, Credit_Application__c.class);
        Credit_Application__c coappicant = (Credit_Application__c) JSON.deserialize(coapplicantJSON, Credit_Application__c.class);

        List<OpportunityContactRole> relApplicantContactList = [SELECT ContactId, Opportunity.AccountId FROM OpportunityContactRole 
            WHERE OpportunityId =: applicant.Opportunity__c AND Role = 'Buyer' LIMIT 1];

        if (relApplicantContactList.size() > 0) {
            if (String.isNotEmpty(applicant.First_Name__c) && String.isNotEmpty(applicant.Last_Name__c)) {
                applicant.Contact__c = relApplicantContactList[0].ContactId;
                //applicant.Account__c = relApplicantContactList[0].Opportunity.AccountId;
                creditApplList.add(applicant);
            } else {
                throw new AuraHandledException('Buyer required fields are Empty!');
            }
        } else {
            throw new AuraHandledException('There is no Buyer on Opportunity');
        }

        List<OpportunityContactRole> relCoApplicantContactList = [SELECT ContactId FROM OpportunityContactRole 
            WHERE OpportunityId =: applicant.Opportunity__c AND Role = 'Cobuyer' LIMIT 1];

        if (relCoApplicantContactList.size() > 0) {
            if (String.isNotEmpty(coappicant.First_Name__c) && String.isNotEmpty(coappicant.Last_Name__c)) {
                coappicant.Contact__c = relCoApplicantContactList[0].ContactId;
                creditApplList.add(coappicant);
                insert creditApplList;
            } else {
                throw new AuraHandledException('Cobuyer required fields are Empty!');
            }
        } else {
            if (String.isEmpty(coappicant.First_Name__c) && String.isEmpty(coappicant.Last_Name__c)) {
                insert creditApplList;
            } else {
                throw new AuraHandledException('There is no Cobuyer on Opportunity');
            }
        }
    }
}