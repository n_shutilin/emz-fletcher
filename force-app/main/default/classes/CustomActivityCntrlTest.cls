@IsTest
public with sharing class CustomActivityCntrlTest {
    @TestSetup
    static void createRecords(){
        String recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Set<Id> recordsIds = new Set<Id>();
        List<Task> tasks = new List<Task>();
        Account acc = new Account(
            FirstName = 'test', LastName = 'Account', RecordTypeId = recordTypeId
        );
        insert acc;
        recordsIds.add(acc.Id);
        Opportunity opp = new Opportunity(
            Name = 'testOpp', AccountId = acc.Id, StageName = 'Working', CloseDate = Date.today().addDays(1)
        );
        insert opp;
        recordsIds.add(opp.Id);
        Lead aLead = new Lead(
            LastName = 'testLead', Dealer_ID__c = 'AUDFJ', Status = 'New'
        );
        insert aLead;
        tasks.add(new Task(
            Subject = 'its a subject', Priority = 'High', Status = 'Completed', 
            WhoId = aLead.Id, OwnerId = UserInfo.getUserId(), Activity_Type__c = 'Email'
        ));
        for (Id recId : recordsIds) {
            tasks.add(new Task(
                Subject = 'its a subject', Priority = 'High', Status = 'Completed', 
                WhatId = recId, OwnerId = UserInfo.getUserId(), Activity_Type__c = 'SMS'
            ));
        }
        for (Id recId : recordsIds) {
            tasks.add(new Task(
                Subject = 'its a subject', Priority = 'High', Status = 'Completed', 
                WhatId = recId, OwnerId = UserInfo.getUserId(), Activity_Type__c = 'Visit'
            ));
        }
        for (Id recId : recordsIds) {
            tasks.add(new Task(
                Subject = 'its a subject', Priority = 'High', Status = 'Completed', 
                WhatId = recId, OwnerId = UserInfo.getUserId(), Activity_Type__c = 'Note'
            ));
        }  
        for (Id recId : recordsIds) {
            tasks.add(new Task(
                Subject = 'its a subject', Priority = 'High', Status = 'Completed', 
                WhatId = recId, OwnerId = UserInfo.getUserId(), Activity_Type__c = 'Call'
            ));
        }
        for (Id recId : recordsIds) {
            tasks.add(new Task(
                Subject = 'its a subject', Priority = 'High', Status = 'Completed', 
                WhatId = recId, OwnerId = UserInfo.getUserId(), Activity_Type__c = 'Appointment'
            ));
        }          
        insert tasks;
    }

    @IsTest
    public static void getTasksTest() {
        Account acc = [
            SELECT Id
            FROM Account
            LIMIT 1
        ];
        List<CustomActivityController.HistoryWrapper> accTasks = CustomActivityController.getTasks(acc.Id);
        System.assertEquals(10, accTasks.size());
        Opportunity opp = [
            SELECT Id
            FROM Opportunity
            LIMIT 1
        ];
        List<CustomActivityController.HistoryWrapper> oppTasks = CustomActivityController.getTasks(opp.Id);
        System.assertEquals(5, oppTasks.size());
        Lead aLead = [
            SELECT Id
            FROM Lead
            LIMIT 1
        ];
        List<CustomActivityController.HistoryWrapper> leadTasks = CustomActivityController.getTasks(aLead.Id);
        System.assertEquals(1, leadTasks.size());
    }
}