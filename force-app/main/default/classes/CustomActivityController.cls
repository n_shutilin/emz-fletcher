public without sharing class CustomActivityController {
    @AuraEnabled
    public static List<HistoryWrapper> getTasks(String recordId) {
        List<ActivityHistory> histories = new List<ActivityHistory>();
        List<HistoryWrapper> historiesWrapper = new List<HistoryWrapper>();
        Map<Id, User> idToUserMap = new Map<Id, User>();
        Set<String> ownerIds = new Set<String>();
        Set<String> activityTypes = new Set<String>{
                'Call', 'SMS', 'Email', 'Visit', 'Note', 'Appointment'
        };
        List<EmailMessage> emailMessages = new List<EmailMessage>();
        if (recordId.startsWith('006')) {
            Opportunity opp = [
                    SELECT
                    (
                            SELECT Activity_Type__c, Description, OwnerId, CreatedDate, Appointment_Type__c,
                                    Owner.Name, LastModifiedDate, Subject, Id, Call_Direction__c, Notified__c
                            FROM ActivityHistories
                            WHERE Activity_Type__c IN :activityTypes
                            ORDER BY CreatedDate DESC
                    )
                    FROM Opportunity
                    WHERE Id = :recordId
            ];
            if (!opp.ActivityHistories.isEmpty()) {
                histories = opp.ActivityHistories;
            }

            emailMessages = [
                    SELECT Subject, CreatedDate, LastModifiedById, LastModifiedBy.Name, MessageDate
                    FROM EmailMessage
                    WHERE RelatedToId = :recordId
                    ORDER BY CreatedDate DESC
            ];
        }
        if (recordId.startsWith('001')) {
            Account acc = [
                    SELECT
                    (
                            SELECT Activity_Type__c, Description, OwnerId, CreatedDate, Appointment_Type__c,
                                    Owner.Name, LastModifiedDate, Subject, Id, Call_Direction__c, Notified__c
                            FROM ActivityHistories
                            WHERE Activity_Type__c IN :activityTypes
                            ORDER BY CreatedDate DESC
                    )
                    FROM Account
                    WHERE Id = :recordId
            ];
            if (!acc.ActivityHistories.isEmpty()) {
                histories = acc.ActivityHistories;
            }
            emailMessages = [
                    SELECT Subject, CreatedDate, LastModifiedById, LastModifiedBy.Name, MessageDate
                    FROM EmailMessage
                    WHERE RelatedToId = :recordId
                    ORDER BY CreatedDate DESC
            ];
        }
        if (recordId.startsWith('00Q')) {
            Lead aLead = [
                    SELECT
                    (
                            SELECT Activity_Type__c, Description, OwnerId, CreatedDate, Appointment_Type__c,
                                    Owner.Name, LastModifiedDate, Subject, Id, Call_Direction__c, Notified__c
                            FROM ActivityHistories
                            WHERE Activity_Type__c IN :activityTypes
                            ORDER BY CreatedDate DESC
                    )
                    FROM Lead
                    WHERE Id = :recordId
            ];
            if (!aLead.ActivityHistories.isEmpty()) {
                histories = aLead.ActivityHistories;
            }
            List<EmailMessageRelation> emailMessageRelations = [
                    SELECT EmailMessage.Subject, RelationId
                    FROM EmailMessageRelation
                    WHERE RelationId = :recordId
            ];

            Set<Id> emailMessagesIds = new Set<Id>();
            if(!emailMessageRelations.isEmpty()) {
                for (EmailMessageRelation emailMessageRelation : emailMessageRelations) {
                    emailMessagesIds.add(emailMessageRelation.EmailMessage.Id);
                }
            }
            emailMessages = [
                    SELECT Subject, CreatedDate, LastModifiedById, LastModifiedBy.Name, MessageDate
                    FROM EmailMessage
                    WHERE Id IN :emailMessagesIds
                    ORDER BY CreatedDate DESC
            ];
        }

        for (ActivityHistory history : histories) {
            ownerIds.add(history.OwnerId);
        }
        for (User usr : [SELECT Department FROM User WHERE Id IN :ownerIds]) {
            idToUserMap.put(usr.Id, usr);
        }
        for (ActivityHistory history : histories) {
            historiesWrapper.add(new HistoryWrapper(history, idToUserMap));
        }
        for(EmailMessage email : emailMessages) {
            historiesWrapper.add(new HistoryWrapper(email));
        }
        return historiesWrapper;
    }

    public class HistoryWrapper {
        @AuraEnabled public string activityType;
        @AuraEnabled public string recordId;
        @AuraEnabled public string description;
        @AuraEnabled public string performedBy;
        @AuraEnabled public string performedById;
        @AuraEnabled public string activityDate;
        @AuraEnabled public string iconType;
        @AuraEnabled public DateTime actualDate;
        @AuraEnabled public string department;
        @AuraEnabled public string emailMessageId;

        public HistoryWrapper(ActivityHistory activityHistory, Map<Id, User> idToUserMap) {
            if (activityHistory.Activity_Type__c == 'Email') {
                this.Description = activityHistory.Subject;
                this.iconType = 'Email_Icon';
                this.activityType = activityHistory.Activity_Type__c;
            } else if (activityHistory.Activity_Type__c == 'Visit') {
                this.Description = activityHistory.Subject;
                this.iconType = 'Visit_Icon';
                this.activityType =
                        activityHistory.Notified__c == True ? activityHistory.Activity_Type__c + ' (NU)' : activityHistory.Activity_Type__c;
            } else if (activityHistory.Activity_Type__c == 'Note') {
                this.Description = activityHistory.Description;
                this.iconType = 'Note_Icon';
                this.activityType =
                        activityHistory.Notified__c == True ? activityHistory.Activity_Type__c + ' (NU)' : activityHistory.Activity_Type__c;
            } else if (activityHistory.Activity_Type__c == 'SMS') {
                this.Description = activityHistory.Description;
                this.iconType = activityHistory.Call_Direction__c == 'Inbound' ? 'SMS_Icon_Red' : 'SMS_Icon';
                this.activityType =
                        activityHistory.Notified__c == True ? activityHistory.Activity_Type__c + ' (NU)' : activityHistory.Activity_Type__c;
            } else if (activityHistory.Activity_Type__c == 'Call') {
                this.iconType = activityHistory.Call_Direction__c == 'Inbound' ? 'Phone_Icon_Red' : 'Phone_Icon';
                this.Description = activityHistory.Description;
                this.activityType =
                        activityHistory.Notified__c == True ? activityHistory.Activity_Type__c + ' (NU)' : activityHistory.Activity_Type__c;
                //String.isNotBlank(activityHistory.Call_Direction__c) ? activityHistory.Call_Direction__c + ' ' +activityHistory.Activity_Type__c : activityHistory.Activity_Type__c;
            } else if (activityHistory.Activity_Type__c == 'Appointment') {
                this.Description = activityHistory.Description;
                this.activityType = activityHistory.Appointment_Type__c;
            }
            this.department = idToUserMap.containsKey(activityHistory.OwnerId) ? idToUserMap.get(activityHistory.OwnerId).Department : '';
            this.activityDate = activityHistory.LastModifiedDate.format();
            this.recordId = '/' + activityHistory.Id;
            this.performedBy = activityHistory.Owner.Name;
            this.performedById = '/' + activityHistory.OwnerId;
            this.actualDate = activityHistory.LastModifiedDate;
        }
        public HistoryWrapper(EmailMessage emailMessage) {
            this.Description = emailMessage.Subject;
            this.activityType = 'Email';
            this.emailMessageId = '/' + emailMessage.Id;
            this.iconType = 'Email_Icon';
            this.performedBy = emailMessage.LastModifiedBy.Name;
            this.performedById = '/' + emailMessage.LastModifiedById;
            this.activityDate = emailMessage.MessageDate != null ? emailMessage.MessageDate.format() : null;
            this.recordId = '/' + emailMessage.Id;
        }
    }
}