public with sharing class CustomLookupCtrl {
    public static final Set<String> SOQL_OBJECT_SET = new Set<String> {'RecordType', 'Contact', 'User'};

    @AuraEnabled
    public static List<SearchResult> search(String searchString, String objectApiName) {
        List<SearchResult> results = new List<SearchResult>();

        if (String.isEmpty(objectApiName)) { return results; }

        if (CustomLookupCtrl.SOQL_OBJECT_SET.contains(objectApiName)) {
            String searchKey = '%' + searchString + '%';

            String query = 'SELECT Id, Name FROM ' + objectApiName;

            if (searchString != '') {
                query += ' WHERE Name LIKE :searchKey';
            }

            query += ' LIMIT 5';

            List<SObject> data = Database.query(String.escapeSingleQuotes(query));

            for (SObject row : data) {
                results.add(new SearchResult(
                    (String)row.get('Id'),
                    (String)row.get('Name')
                ));
            }
        } else {
            searchString += '*';

            String query = 'FIND {' + searchString + '*} ' +
                'IN NAME FIELDS ' +
                'RETURNING ' +
                objectApiName + '(Id, Name) ' +
                'LIMIT 6';

            List<List<SObject>> searchResults = Search.query(query);

            List<SObject> objects = (List<SObject>) searchResults[0];
            for (SObject obj : objects) {
                results.add(
                    new SearchResult(
                        (String)obj.get('Id'),
                        (String)obj.get('Name')
                    )
                );
            }
        }

        return results;
    }

    @AuraEnabled(Cacheable=true)
    public static SearchResult getSObjectById(String objectApiName, String id) {
        SearchResult result;

        if (String.isEmpty(objectApiName) || String.isEmpty(id)) { return result; }

        String query = 'SELECT Id, Name FROM ' + objectApiName + ' WHERE Id = :id';

        List<SObject> data = Database.query(query);

        if (data.isEmpty()) return result;

        result = new SearchResult((String)data[0].get('Id'), (String)data[0].get('Name'));

        return result;
    }

    public class SearchResult {
        @AuraEnabled public String id;
        @AuraEnabled public String name;

        public SearchResult(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}