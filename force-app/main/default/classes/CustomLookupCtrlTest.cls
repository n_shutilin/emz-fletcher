@IsTest
private class CustomLookupCtrlTest {

    @TestSetup
    private static void setup() {
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId();

        insert new Account(
            Name = 'TestAcc',
            RecordTypeId = rtId
        );
    }

    @IsTest
    private static void search_TestAcc() {
        Account acc = new Account(Name = 'Test Account');
        insert acc;

        Id [] fixedSearchResults = new Id[1];
        fixedSearchResults[0] = acc.Id;

        Test.setFixedSearchResults(fixedSearchResults);
        List<CustomLookupCtrl.SearchResult> results = CustomLookupCtrl.search('Test', 'Account');

        System.assertEquals(1, results.size());
        System.assertEquals('Test Account', results.get(0).name);
    }

    @IsTest
    private static void search_TestRT() {
        List<CustomLookupCtrl.SearchResult> results = CustomLookupCtrl.search('Household', 'RecordType');
        System.assert(!results.isEmpty());
    }

    @IsTest
    private static void getSObjectById_Test() {
        Account acc = new Account(Name = 'Test');
        insert acc;

        CustomLookupCtrl.SearchResult result = CustomLookupCtrl.getSObjectById('Account', acc.Id);

        System.assertEquals('Test', result.name);
    }
}