public class CustomMetadataService {
    public CustomMetadataService() {}
    
    public SalesTimeslotsSettings__mdt getOppHoursMDT(String dealer, String appointmentType){
        return [SELECT OperatingHoursId__c FROM SalesTimeslotsSettings__mdt 
                WHERE Dealer__c = :dealer AND SalesAppointmentType__c = :appointmentType LIMIT 1];
    }

}