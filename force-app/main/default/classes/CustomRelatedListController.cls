public without sharing class CustomRelatedListController {
 private static String OBJECT_API_NAME = 'ServiceAppointment';
 private static String SERVICE_APPOINTMENT_FIELD_SET_API_NAME = 'Account_related_list_field_set';
 private static String SALES_SERVICE_APPOINTMENT_FIELD_SET_API_NAME = 'Account_sales_related_list_field_set';
 private static String VEHICLE_SERVICE_APPOINTMENT_FIELD_SET_API_NAME = 'Account_vehicle_related_list_field_set';
 private static String STANDARD_FIELD_TODISPLAY_NUMBER = 'AppointmentNumber';
 private static String SALES_APPOINTMENT_RECORD_TYPE_NAME = 'SalesAppointment';
 private static String VEHICLE_SERVICE_APPOINTMENTNAME = 'VehicleServiceAppointment';

    public class FieldSetProperties{
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public String type;
    }

    @AuraEnabled
    public static List<AppointmentWrapper> getAppointmentsListWithLimit(Integer offset, Integer onPageLimit, String accountId, String sortedFieldName, String directionToSort, String recordTypeName) {
        List<ServiceAppointment> appointmentList = Database.query (formSOQLQuerry(offset, onPageLimit, accountId, sortedFieldName, directionToSort, recordTypeName));
        System.debug(appointmentList);
        List<AppointmentWrapper> wrapperList = new List<AppointmentWrapper>();
        if(!appointmentList.isEmpty()){
            for(ServiceAppointment appointment : appointmentList){
                AppointmentWrapper wrapper = new AppointmentWrapper();
                wrapper.appointment = appointment;
                wrapper.url = '/' + appointment.Id;
                wrapperList.add(wrapper);
        	}
    	}
        return wrapperList;
    }
    
    @AuraEnabled(cacheable=true)
    public static Integer calculateNumberOfPages(Integer onPageLimit, String accountId, String recordTypeName){
        Integer count = getNumberOfAppointments(accountId, recordTypeName);
        Integer numberOfPages = count / onPageLimit;
        if(Math.mod(count, onPageLimit) > 0){
            numberOfPages++;
        }
        return numberOfPages; 
    } 

    @AuraEnabled
    public static Integer getNumberOfAppointments(String accountId, String recordTypeName){
        String recordTypeId = getRecordTypeIdByName(recordTypeName);
        if(recordTypeName == SALES_APPOINTMENT_RECORD_TYPE_NAME){
            List<String> oppIdList = findOppIdList(accountId);
            return Database.countQuery ('SELECT COUNT()' +
                                      + ' FROM ServiceAppointment' + 
                                      + ' WHERE NotCompleted__c = false AND RecordTypeId = ' + recordTypeId + ' AND  ParentRecordId IN :oppIdList');          
        }
        List<String> perosnIdList = formIdList(accountId);
        if(!perosnIdList.isEmpty()){
            return Database.countQuery ('SELECT COUNT()' +
                                      + ' FROM ServiceAppointment' +
                                      + ' WHERE (NotCompleted__c = false AND RecordTypeId =' + recordTypeId + ' AND ParentRecordId  = :accountId)' +
                                      + ' OR (NotCompleted__c = false AND RecordTypeId =' + recordTypeId + ' AND ContactId IN :perosnIdList )');
        }else{
            return Database.countQuery ('SELECT COUNT()' +
                                      + ' FROM ServiceAppointment' + 
                                      + ' WHERE NotCompleted__c = false AND RecordTypeId =' + recordTypeId + ' AND  ParentRecordId  = :accountId');       
        }
    }
    
    @AuraEnabled(cacheable = true)
    public static  List<FieldSetProperties> getFieldSet(String recordTypeName){
        String fieldSetApiName = getFieldSetNameByRecordTypeName(recordTypeName);
        List<FieldSetProperties> wrappersList = new List<FieldSetProperties>();
        Schema.SObjectType sObjType = Schema.getGlobalDescribe().get(OBJECT_API_NAME);
        Schema.DescribeSObjectResult desSObjRslt = sObjType.getDescribe();            
        Schema.FieldSet fieldSetIns = desSObjRslt.FieldSets.getMap().get(fieldSetApiName);
        
        for( Schema.FieldSetMember fieldSetMember : fieldSetIns.getFields() ){
            FieldSetProperties wrapperIns = new FieldSetProperties();
            wrapperIns.label = String.valueOf(fieldSetMember.getLabel()); 
            wrapperIns.fieldName = String.valueOf(fieldSetMember.getFieldPath()); 
            wrapperIns.type = String.valueOf(fieldSetMember.getType()).toLowerCase();
            wrappersList.add(wrapperIns);
        }        
        return wrappersList;
    }
    private static String formSOQLQuerry(Integer offset, Integer onPageLimit, String accountId, String sortedFieldName, String directionToSort, String recordTypeName){
        String finalQuerry = 'SELECT Id, AppointmentNumber ';
        String recordTypeId = getRecordTypeIdByName(recordTypeName);
        List<FieldSetProperties> wrappersList = getFieldSet(recordTypeName);
        for(FieldSetProperties wrapper : wrappersList){
            if(wrapper.fieldName != STANDARD_FIELD_TODISPLAY_NUMBER){
                finalQuerry = finalQuerry + ', ' + wrapper.fieldName;
            }       
        }
        if(recordTypeName == SALES_APPOINTMENT_RECORD_TYPE_NAME){
            List<String> oppIdList = findOppIdListWithDelimiter(accountId);
            if(oppIdList.size() == 1){
                String first = oppIdList[0];
             	finalQuerry = finalQuerry + ' FROM ServiceAppointment' +
                                      	  + ' WHERE NotCompleted__c = false AND RecordTypeId =' + recordTypeId + 
                                      	  + ' AND  ParentRecordId =' + first +                                   
                 						  + ' ORDER BY ' + sortedFieldName + ' ' + directionToSort + ' NULLS LAST LIMIT ' + onPageLimit + ' OFFSET ' + offset;
            }else {
            finalQuerry = finalQuerry + ' FROM ServiceAppointment' +
                                      + ' WHERE NotCompleted__c = false AND RecordTypeId =' + recordTypeId + 
                                      + ' AND  ParentRecordId IN (';
            finalQuerry = finalQuerry + String.join(oppIdList,',');                                       
                finalQuerry = finalQuerry + ') ORDER BY ' + sortedFieldName + ' ' + directionToSort + ' NULLS LAST LIMIT ' + onPageLimit + ' OFFSET ' + offset;}
            System.debug(finalQuerry);
            return finalQuerry;
        }
        List<String> personIdList = formIdListWithDelimiter(accountId);
        if(!personIdList.isEmpty()){
            finalQuerry = finalQuerry + ' FROM ServiceAppointment' +
                                      + ' WHERE (NotCompleted__c = false' +
                                      + ' AND RecordTypeId =' + recordTypeId + ' AND  ParentRecordId = \'' + accountId + '\')' +
                                      + ' OR (NotCompleted__c = false AND RecordTypeId =' + recordTypeId + ' AND  ContactId IN ('; 
            finalQuerry = finalQuerry + String.join(personIdList,',');
            finalQuerry = finalQuerry + ')) ORDER BY ' + sortedFieldName + ' ' + directionToSort + ' NULLS LAST LIMIT ' + onPageLimit + ' OFFSET ' + offset;
        }
        else{
            finalQuerry = finalQuerry + ' FROM ServiceAppointment' +
                                      + ' WHERE NotCompleted__c = false AND RecordTypeId =' + recordTypeId + 
                                      + ' AND  ParentRecordId = \'' + accountId + '\'' +
                                      + ' ORDER BY ' + sortedFieldName + ' ' + directionToSort + ' NULLS LAST LIMIT ' + onPageLimit + ' OFFSET ' + offset;    
        }
        return finalQuerry;
    }

    private static List<String> formIdList(String accountId){
        List<String> perosnIdList = new List<String>();
        List<String> authorizedContactIdList = new List<String>();
        authorizedContactIdList.add(accountId);
        List<Authorized_Contact__c> authorizedContactList = [SELECT Authorized_Contact__r.Id 
                                                             FROM Authorized_Contact__c 
                                                             WHERE Parent_Account__r.Id = :accountId];
        if(!authorizedContactList.isEmpty()){
        for(Authorized_Contact__c contact : authorizedContactList){
            authorizedContactIdList.add(contact.Id);}
        }
        List<Account> personIdInAccountList = [SELECT PersonContactId 
                                               FROM Account 
                                               WHERE Id in :authorizedContactIdList];
        if(!personIdInAccountList.isEmpty()){
        for(Account account : personIdInAccountList){
            perosnIdList.add(account.PersonContactId);}
        }
        return perosnIdList;
    }

    private static List<String> formIdListWithDelimiter(String accountId){
        List<String> perosnIdList = new List<String>();
        List<String> authorizedContactIdList = new List<String>();
        authorizedContactIdList.add(accountId);
        List<Authorized_Contact__c> authorizedContactList = [SELECT Authorized_Contact__r.Id 
                                                             FROM Authorized_Contact__c 
                                                             WHERE Parent_Account__r.Id = :accountId];
        if(!authorizedContactList.isEmpty()){
        for(Authorized_Contact__c contact : authorizedContactList){
            authorizedContactIdList.add(contact.Id);}
        }
        List<Account> personIdInAccountList = [SELECT PersonContactId 
                                               FROM Account 
                                               WHERE Id in :authorizedContactIdList];
        if(!personIdInAccountList.isEmpty()){
        for(Account account : personIdInAccountList){
            perosnIdList.add('\'' + account.PersonContactId + '\'');}
        }
        return perosnIdList;
    }
    
    private static List<String> findOppIdListWithDelimiter(String accountId){
        List<Opportunity> oppList = [SELECT Id 
                                     FROM Opportunity 
                                     WHERE AccountId = :accountId];
        List<String> oppIdList = new List<String>();
        if(!oppList.isEmpty()){
            for(Opportunity opp : oppList){
                oppIdList.add('\'' + opp.Id + '\'');
            }
        } else {
            oppIdList.add('\'\'');
        }
        return oppIdLIst;
    }

    private static List<String> findOppIdList(String accountId){
        List<Opportunity> oppList = [SELECT Id 
                                     FROM Opportunity 
                                     WHERE AccountId = :accountId];
        List<String> oppIdList = new List<String>();
        if(!oppList.isEmpty()){
            for(Opportunity opp : oppList){
                oppIdList.add(opp.Id);
            }
        }
        return oppIdLIst;
    }

    private static String getRecordTypeIdByName(String recordTypeName){
        String recordTypeId = '';
        List<RecordType> recordTypeIdList =  [SELECT Id
                                              FROM RecordType
                                              WHERE DeveloperName = :recordTypeName 
                                              AND sObjectType =  :OBJECT_API_NAME];
        if(recordTypeIdList.size()>0){
            recordTypeId = recordTypeIdList[0].Id;
        }
        return '\'' + recordTypeId + '\'';

    }

    private static String getFieldSetNameByRecordTypeName(String recordTypeName){
      switch on recordTypeName {
          when 'SalesAppointment' {
            return SALES_SERVICE_APPOINTMENT_FIELD_SET_API_NAME;
          }
          when 'VehicleServiceAppointment'{
            return  VEHICLE_SERVICE_APPOINTMENT_FIELD_SET_API_NAME;
          }
          when else {
             return  SERVICE_APPOINTMENT_FIELD_SET_API_NAME;
          }
      }
}
}