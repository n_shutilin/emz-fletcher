public with sharing class CustomRepairOrderController {
    private static String OBJECT_API_NAME = 'WorkOrder';
    private static String SERVICE_APPOINTMENT_FIELD_SET_API_NAME = 'Account_related_list_field_set';
    private static String STANDARD_FIELD_TODISPLAY_NUMBER = 'RO_Number__c';

    public class FieldSetProperties{
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public String type;
    }

    public class WorkOrderWrapper{
        @AuraEnabled
        public WorkOrder appointment;
        @AuraEnabled
        public String url;
    }

    @AuraEnabled
    public static List<WorkOrderWrapper> getAppointmentsListWithLimit(Integer offset, Integer onPageLimit, String accountId, String sortedFieldName, String directionToSort) {
        List<WorkOrder> appointmentList = Database.query (formSOQLQuerry(offset, onPageLimit, accountId, sortedFieldName, directionToSort));
        System.debug(appointmentList);
        List<WorkOrderWrapper> wrapperList = new List<WorkOrderWrapper>();
        if(!appointmentList.isEmpty()){
            for(WorkOrder appointment : appointmentList){
                WorkOrderWrapper wrapper = new WorkOrderWrapper();
                wrapper.appointment = appointment;
                wrapper.url = '/' + appointment.Id;
                wrapperList.add(wrapper);
            }
        }
        return wrapperList;
    }

    @AuraEnabled(cacheable=true)
    public static Integer calculateNumberOfPages(Integer onPageLimit, String accountId){
        Integer count = getNumberOfAppointments(accountId);
        Integer numberOfPages = count / onPageLimit;
        if(Math.mod(count, onPageLimit) > 0){
            numberOfPages++;
        }
        return numberOfPages;
    }

    @AuraEnabled
    public static Integer getNumberOfAppointments(String accountId){
        return Database.countQuery ('SELECT COUNT()' +
            + ' FROM WorkOrder' +
            + ' WHERE AccountId = :accountId');
    }

    @AuraEnabled(cacheable = true)
    public static  List<FieldSetProperties> getFieldSet(){
        String fieldSetApiName = SERVICE_APPOINTMENT_FIELD_SET_API_NAME;
        List<FieldSetProperties> wrappersList = new List<FieldSetProperties>();
        Schema.SObjectType sObjType = Schema.getGlobalDescribe().get(OBJECT_API_NAME);
        Schema.DescribeSObjectResult desSObjRslt = sObjType.getDescribe();
        Schema.FieldSet fieldSetIns = desSObjRslt.FieldSets.getMap().get(fieldSetApiName);

        for( Schema.FieldSetMember fieldSetMember : fieldSetIns.getFields() ){
            FieldSetProperties wrapperIns = new FieldSetProperties();
            wrapperIns.label = String.valueOf(fieldSetMember.getLabel());
            wrapperIns.fieldName = String.valueOf(fieldSetMember.getFieldPath());
            wrapperIns.type = String.valueOf(fieldSetMember.getType()).toLowerCase();
            wrappersList.add(wrapperIns);
        }
        return wrappersList;
    }
    private static String formSOQLQuerry(Integer offset, Integer onPageLimit, String accountId, String sortedFieldName, String directionToSort){
        String finalQuerry = 'SELECT Id, ' + STANDARD_FIELD_TODISPLAY_NUMBER + ' ';
        List<FieldSetProperties> wrappersList = getFieldSet();
        for(FieldSetProperties wrapper : wrappersList){
            if(wrapper.fieldName != STANDARD_FIELD_TODISPLAY_NUMBER){
                finalQuerry = finalQuerry + ', ' + wrapper.fieldName;
            }
        }

        finalQuerry = finalQuerry + ' FROM WorkOrder WHERE AccountId = :accountId';

        if (String.isNotBlank(sortedFieldName)) {
            finalQuerry = finalQuerry + ' ORDER BY ' + sortedFieldName + ' ' + directionToSort + ' NULLS LAST';
        }

        finalQuerry = finalQuerry + ' LIMIT ' + onPageLimit + ' OFFSET ' + offset;

        System.debug(finalQuerry);
        return finalQuerry;

    }

}