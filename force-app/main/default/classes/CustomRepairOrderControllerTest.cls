@IsTest
private class CustomRepairOrderControllerTest {
    private static final Integer INDEX_FIRST = 0;
    private static final Integer STANDARD_LIMIT = 10;
    private static final Integer STANDARD_OFFSET = 0;
    private static final Integer EXPECTED_NUMBER_OF_PAGES = 1;
    private static final String STANDARD_FIELD_TO_SORT_LABEL = 'Id';
    private static final String STANDARD_TYPE_TO_SORT_LABEL = 'asc';
    private static final String TEST_ACCOUNT_NAME = 'testAccount';
    private static final String TEST_LAST_NAME = 'test';
    private static final String FIRST_PEROSN_ACCOUNT_NAME = 'firstTest';
    private static final String SECOND_PEROSN_ACCOUNT_NAME = 'secondTest';
    private static final String PEROSN_ACCOUNT_RECORD_TYPE_NAME = 'Person Account';
    private static final String SOBJECT_ACCOUNT_NAME = 'Account';

    @TestSetup static void testInit() {
        RecordType personAccountRecordType = [
            SELECT Id
            FROM RecordType
            WHERE Name = :PEROSN_ACCOUNT_RECORD_TYPE_NAME
            AND SobjectType = :SOBJECT_ACCOUNT_NAME
        ];

        Account testAccount = new Account();
        testAccount.FirstName = TEST_ACCOUNT_NAME;
        testAccount.LastName = TEST_LAST_NAME;
        testAccount.RecordTypeId = personAccountRecordType.Id;
        insert testAccount;

        Account firstPersonAccount = new Account();
        firstPersonAccount.RecordType = personAccountRecordType;
        firstPersonAccount.FirstName = FIRST_PEROSN_ACCOUNT_NAME;
        firstPersonAccount.LastName = TEST_LAST_NAME;
        insert firstPersonAccount;

        Account secondPersonAccount = new Account();
        secondPersonAccount.RecordType = personAccountRecordType;
        secondPersonAccount.FirstName = SECOND_PEROSN_ACCOUNT_NAME;
        secondPersonAccount.LastName = TEST_LAST_NAME;
        insert secondPersonAccount;

        Contact testContact = [
            SELECT Id
            FROM Contact
            WHERE AccountId = :firstPersonAccount.Id
        ];

        WorkOrder workOrder = new WorkOrder(
            RO_Number__c = '1234',
            Auto_Trigger__c = false,
            AccountId = testAccount.Id,
            ContactId = testContact.Id
        );

        insert workOrder;
    }

    @IsTest
    static void getAppointmentsTest() {
        List<Account> accountIdList = [
            SELECT Id
            FROM Account
            WHERE FirstName = :TEST_ACCOUNT_NAME
        ];

        String accountId = accountIdList[INDEX_FIRST].Id;
        CustomRepairOrderController.WorkOrderWrapper expected = new CustomRepairOrderController.WorkOrderWrapper();
        expected.appointment = [
            SELECT Id, RO_Number__c
            FROM WorkOrder
        ];
        expected.url = '/' + expected.appointment.Id;

        List<CustomRepairOrderController.WorkOrderWrapper> actual = CustomRepairOrderController.getAppointmentsListWithLimit(STANDARD_OFFSET, STANDARD_LIMIT, accountId, STANDARD_FIELD_TO_SORT_LABEL, STANDARD_TYPE_TO_SORT_LABEL);
        System.assertEquals(expected.appointment.Id, actual[INDEX_FIRST].appointment.Id);
    }

    @IsTest
    static void calculateNumberOfPagesTest() {
        List<Account> accountIdList = [
            SELECT Id
            FROM Account
            WHERE FirstName = :TEST_ACCOUNT_NAME
        ];

        String accountId = accountIdList[INDEX_FIRST].Id;

        Integer actual = CustomRepairOrderController.calculateNumberOfPages(STANDARD_LIMIT, accountId);
        System.assert(EXPECTED_NUMBER_OF_PAGES == actual);
    }
}