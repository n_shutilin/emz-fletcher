global with sharing class CustomSenderService implements callable {
    
    global Object call(String action, Map<String, Object> args) {
        switch on action {
            when 'getSenderInfo' {
                return getCustomSenderIds(args);
            }
        }
        smagicinteract__SMS_SenderId__c senderIdObj = new smagicinteract__SMS_SenderId__c();
        return senderIdObj;
    }
    
    private String getCustomSenderIds(Map<String, Object> args) {
        String query = '';
        String DEALER_ID = 'Dealer_ID__c';
        Boolean isDefault = true;
        
        List<Id> recordIds = (List<Id>)args.get('ids');
        List<smagicinteract__SMS_SenderId__c> senderIdList = new List<smagicinteract__SMS_SenderId__c> ();//store all the sms senderId
        List<SenderInfo> senderIdRecList = new List<SenderInfo>();// store all the senderInfo
        
        Map<String,smagicinteract__SMS_SenderId__c> senderIdMap = new Map<String,smagicinteract__SMS_SenderId__c>();
        
        Id recordId = recordIds[0];//store the record Id
        String sObjName = recordId.getSObjectType().getDescribe().getName();//store the object Name
       
        //build dynamic query
        query = 'SELECT Id,';
        if(sObjName == 'Account') {
            DEALER_ID = 'Dealer_ID__pc';
        }
        query += DEALER_ID + ' FROM '+ sObjName;
        query +=' WHERE Id =:recordId';
        
        Sobject obj = Database.query(query);//execute the dynamic query
        
        //iterate all sms senderIds
        for(
            smagicinteract__SMS_SenderId__c sId : 
            [SELECT Id,Name,smagicinteract__senderId__c,smagicinteract__Used_For__c,smagicinteract__Label__c,smagicinteract__Channel_Code__c,
             smagicinteract__Description__c,smagicinteract__Email_Notification_Template__c, smagicinteract__Notification_Recipient__c 
             FROM smagicinteract__SMS_SenderId__c]
        ) {
            senderIdMap.put(sId.smagicinteract__senderId__c, sId);
        }
        
        String dealerValue = String.valueOf(obj.get(DEALER_ID));// store the dealerId value
        
        //condition to check the dealerId value is not null and custom setting dealer senderId is not null
        if(obj != null &&  dealerValue != null && DealerSenderId__c.getValues(dealerValue) != null) {
            if(sObjName == 'Account') {
                senderIdList.add(senderIdMap.get(DealerSenderId__c.getValues(dealerValue).ServiceSenderId__c));//collect the sms senderId
                senderIdList.add(senderIdMap.get(DealerSenderId__c.getValues(dealerValue).SaleSenderId__c));//collect the sms senderId
            } else {
                senderIdList.add(senderIdMap.get(DealerSenderId__c.getValues(dealerValue).SaleSenderId__c));//collect the sms senderId
                senderIdList.add(senderIdMap.get(DealerSenderId__c.getValues(dealerValue).ServiceSenderId__c));//collect the sms senderId
            }
        }
        
        //iterate all the stored sms senderIds
        for(smagicinteract__SMS_SenderId__c senderId : senderIdList) {
            SenderInfo temp = new SenderInfo(senderId);// form the senderInfo based on the senderId
            //condition to check the isDefault,if it is true make the senderId has default
            if(isDefault) {
                temp.isDefault = true;
                isDefault = false;
            }
            else {
                temp.isDefault = false;
            }
            
            senderIdRecList.add(temp); //collect all the senderInfo record 
        }
        return Json.serialize(senderIdRecList);
    }
}