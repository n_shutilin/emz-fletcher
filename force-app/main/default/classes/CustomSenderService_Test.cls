@isTest
public class CustomSenderService_Test {
    @IsTest
    private static void getSenderIdTest() {
        Boolean isDefault = true;
        
        List<Id> leadIds = new List<Id>();
		List<SenderInfo> senderIdRecList = new List<SenderInfo>();// store all the senderInfo
        List<smagicinteract__SMS_SenderId__c> newsenderIdList = new List<smagicinteract__SMS_SenderId__c>();
        
        newsenderIdList.add(new smagicinteract__SMS_SenderId__c(smagicinteract__senderId__c='13122622031',smagicinteract__Used_For__c='Both'));
        newsenderIdList.add(new smagicinteract__SMS_SenderId__c(smagicinteract__senderId__c='13124654623',smagicinteract__Used_For__c='Both'));
        
        insert newsenderIdList;
        
        DealerSenderId__c dealerSenderId = new DealerSenderId__c(Name='CHMBN',SaleSenderId__c = '13122622031',ServiceSenderId__c='13124654623');
        insert dealerSenderId;
        
        Lead lead = new Lead(FirstName='Wilson',LastName='Test ',Phone='9840137234432',Dealer_ID__c ='CHMBN');
        insert lead;
        leadIds.add(lead.Id);
        
        Map<String, Object> smsMap = new  Map<String, Object> ();
        smsMap.put('ids', (Object) leadIds);
        
        for(smagicinteract__SMS_SenderId__c senderId : newsenderIdList) {
            SenderInfo temp = new SenderInfo(senderId);
            if(isDefault) {
                temp.isDefault = true;
                isDefault = false;
            }
            else {
                temp.isDefault = false;
            }
            
            senderIdRecList.add(temp); //collect all the senderInfo record 
        } 
        
        CustomSenderService senderService = new CustomSenderService();
        Test.startTest();
        	Object result = senderService.call('getSenderInfo', smsMap);
        Test.stopTest(); 
		List<SenderInfo> senderIdList = (List<SenderInfo>)JSON.deserialize(String.valueOf(result), List<SenderInfo>.class);
        System.assertEquals(senderIdRecList[0].senderId, senderIdList[0].senderId);
    }
}