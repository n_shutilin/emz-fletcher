global class CustomerExtractBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {

    public Boolean isDelta;
    public CDK_Integration__c settings;
    public String dealerId;
    public String deltaDate;
    private final static String AUTHORIZATION = 'Authorization';
    private final static String CONTENT_TYPE = 'Content-Type';
    private final static String CONTENT_TYPE_VALUE = 'application/x-www-form-urlencoded';
    private final static Integer TIMEOUT = 120000;

    global CustomerExtractBatchable() {
        this.settings = CDK_Integration__c.getOrgDefaults();
        this.isDelta = true;
        this.deltaDate = Date.today().format();
    }

    global CustomerExtractBatchable(String deltaDate) {
        this.settings = CDK_Integration__c.getOrgDefaults();
        this.isDelta = true;
        this.deltaDate = deltaDate;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
            'SELECT Id, Dealer_Code__c, CDK_Dealer_Code__c, Name ' +
                'FROM Account ' +
                'WHERE RecordType.DeveloperName = \'Dealer\' AND CDK_Dealer_Code__c != NULL';

        if (String.isNotBlank(dealerId)) {
            query += ' AND CDK_Dealer_Code__c = \'' + dealerId + '\'';
        }

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        for (Account account : scope) {
            HttpRequest request = new HttpRequest();
            String reqUrl = settings.Customer_Extract_URL__c;
            request.setEndpoint(reqUrl);
            request.setMethod('POST');
            request.setTimeout(TIMEOUT);
            request.setHeader(CONTENT_TYPE, CONTENT_TYPE_VALUE);
            request.setHeader(AUTHORIZATION, settings.Authorization__c);
            String params =
                'dealerId=' + account.CDK_Dealer_Code__c +
                        '&queryId=' + (isDelta ? settings.Customer_Extract_Delta_Query_Id__c + '&deltaDate=' + deltaDate : settings.Customer_Extract_Query_Id__c) +
                    '&TimeoutSeconds=' + String.valueOf(TIMEOUT);
            request.setBody(params);

            Integration_Logs__c log = new Integration_Logs__c(
                Endpoint__c = request.getEndpoint(),
                Process__c = (isDelta ? 'Delta Customer Extract' : 'Bulk Customer Extract'),
                Result__c = ''
            );

            System.HttpResponse response;

            try {
                response = new System.Http().send(request);
            } catch (Exception ex) {
                log.Result__c = 'Failed for ' + account.Name;
                log.Error__c = ex.getMessage();
            }

            if (response != null) {

                if (response.getStatusCode() != 200) {
                    log.Result__c = 'Failed';
                    log.Error_Message__c = response.getHeaderKeys() + '\n' + response.getBody();
                }
            } else {
                log.Result__c = 'Failed';
                log.Error_Message__c = 'Response is NULL';
            }

            List<CDK_Customer__c> newCustomers;

            try {
                newCustomers = CustomerExtractParser.parseCustomers(response.getBodyDocument(), account.Dealer_Code__c);
            } catch (Exception ex) {
                log.Result__c = 'Failed for ' + account.Name;
                log.Error__c = ex.getMessage();
            }

            if (newCustomers != null && !newCustomers.isEmpty()) {
                try {
                    Database.UpsertResult[] results = Database.upsert(newCustomers, CDK_Customer__c.fields.SF_Unique_Identifier__c, false);
                    Integer insertedNum = 0;
                    Integer updatedNum = 0;

                    for (Integer index = 0, size = results.size(); index < size; index++) {
                        if (results[index].isSuccess()) {
                            if (results[index].isCreated()) {
                                insertedNum++;
                            } else {
                                updatedNum++;
                            }
                        }
                    }

                    if (insertedNum > 0) {
                        log.Result__c += String.valueOf(insertedNum) + ' customers were created from CDK ';
                    }
                    if (insertedNum > 0 && updatedNum > 0) {
                        log.Result__c += 'and ';
                    }
                    if (updatedNum > 0) {
                        log.Result__c += String.valueOf(updatedNum) + ' customers were updated from CDK ';
                    }
                    if (insertedNum > 0 || updatedNum > 0) {
                        log.Result__c += 'for ' + account.Name ;
                    } else {
                        log.Result__c += account.Name + ' has no new customers.';
                    }
                } catch (DmlException ex) {
                    log.Result__c = 'Exception for ' + account.Name;
                    log.Error__c = ex.getMessage();
                }
            } else {
                log.Result__c += account.Name + ' has no new customers.';
            }
            IntegrationLogger.addLog(log,
                new List<Attachment>{
                    new Attachment(
                        Name = 'Request',
                        ContentType = 'text/plain',
                        Body = Blob.valueOf(request.getBody())
                    ),
                    new Attachment(
                        Name = 'Response',
                        ContentType = 'text/plain',
                        Body = Blob.valueOf(response.getBody())
                    )
                }
            );
        }
        try {
            IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }

    global void finish(Database.BatchableContext bc) {}
}