public class CustomerExtractParser {

    public static final String EXTRACT_CUSTOMER_NAMESPACE = 'http://www.dmotorworks.com/pip-extract-customer';
    public static final String DEFAULT_NODE_CHILD = 'Customer';

    public static List<CDK_Customer__c> parseCustomers(Dom.Document doc, String dealerCode) {
        Map<String, Schema.SObjectField> customerFields = Schema.SObjectType.CDK_Customer__c.fields.getMap();

        List<CDK_Customer__c> customers = new List<CDK_Customer__c>();
        for (Dom.XmlNode child: doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == DEFAULT_NODE_CHILD) {
                CDK_Customer__c customer = new CDK_Customer__c();
                setFields(customer, customerFields, CUSTOMER_PARSE_MAP, child, EXTRACT_CUSTOMER_NAMESPACE);
                customer.Dealer_ID__c = dealerCode;
                customer.SF_Unique_Identifier__c = customer.Dealer_ID__c + '_' + customer.Customer_Control_Number__c;
                customers.add(customer);
            }
        }

        if (customers.isEmpty()) {
            return null;
        }

        return customers;
    }

    private static void setFields(SObject curObject, Map<String, Schema.SObjectField> objectFieldMap,
        Map<String, String> fieldMap, Dom.XmlNode child, String namescape) {
        for (String field : fieldMap.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, namescape);
            if (childElement != null && String.isNotBlank(childElement.getText().trim()) && fieldMap.get(field) != null) {
                Schema.DisplayType fieldDataType = objectFieldMap.get(fieldMap.get(field)).getDescribe().getType();
                
                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    curObject.put(fieldMap.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    curObject.put(fieldMap.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    curObject.put(fieldMap.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT    ) {
                    curObject.put(fieldMap.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    curObject.put(fieldMap.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    curObject.put(fieldMap.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    curObject.put(fieldMap.get(field), (childElement.getText().trim().toUpperCase() == 'Y' ? true : false));
                }
            }
        }
    }

    public static final Map<String, String> CUSTOMER_PARSE_MAP = new Map<String, String>{
        'Address'                   => 'Address__c',
        'AddressSecondLine'         => 'AddressLine2__c',
        'BlockEMail'                => 'HasOptedOutOfEmail__c',
        'BlockMail'                 => 'Mail_Opt_Out__c',
        'BlockPhone'                => 'Do_Not_Call__c',
        'BusinessPhone'             => 'OtherPhone__c',
        'HostItemID'                => 'HostItemID__c',
        'BusinessPhoneExt'          => 'Work_Phone_Ext__c',
        'Cellular'                  => 'MobilePhone__c',
        'City'                      => 'City__c',
        'Comment'                   => 'Description__c',
        'Country'                   => 'Country__c',
        'County'                    => 'County__c',
        'CustNo'                    => 'Customer_Control_Number__c',
        'DateAdded'                 => 'Date_Added_CDK__c',
        'Email'                     => 'Email__c',
        'Email2'                    => 'Email_2__c',
        'Email3'                    => 'Email_3__c',
        'EmailDesc'                 => 'EmailDesc__c',
        'EmailDesc2'                => 'EmailDesc2__c',
        'EmailDesc3'                => 'EmailDesc3__c',
        'ErrorLevel'                => 'Error_Level__c',
        'ErrorMessage'              => 'Error_Message__c',
        'FirstName'                 => 'FirstName__c',
        'HomeFax'                   => 'Fax__c',
        'HomePhone'                 => 'Home_Phone__c',
        'LastName'                  => 'LastName__c',
        'LastUpdated'               => 'Last_Update_CDK__c',
        'MiddleName'                => 'Middle_Name__c',
        'Name1'                     => 'Name1_CDK__c',
        'Name2'                     => 'Name2_CDK__c',
        'Name2Company'              => 'Name2_Company__c',
        'Name2First'                => 'Name2_First__c',
        'Name2Last'                 => 'Name2_Last__c',
        'Name2Middle'               => 'Name2_Middle__c',
        'NameCompany'               => 'Name_Company__c',
        'NameSuffix'                => 'Suffix__c',
        'NameTitle'                 => 'Title__c',
        'PreferredContactMethod'    => 'CDK_Preferred_Contact_Method__c',
        'PreferredLanguage'         => 'Language__c',
        'SaleType'                  => 'Sale_Type__c',
        'SecondaryHomePhone'        => 'Secondary_Home_Phone__c',
        'ServiceCustomer'           => 'Service_Customer__c',
        'State'                     => 'State__c',
        'Telephone'                 => 'Phone__c',
        'ZipOrPostalCode'           => 'Postal_Code__c'
    };
}