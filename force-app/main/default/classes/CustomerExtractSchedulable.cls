global with sharing class CustomerExtractSchedulable implements Schedulable{
    global void execute(SchedulableContext SC) {
        CustomerExtractBatchable batch = new CustomerExtractBatchable();
        Database.executeBatch(batch, 1);

        Datetime nextScheduleTime = System.now().addMinutes(15);
        String hour = nextScheduleTime.hour() <= 21 ? String.valueOf(nextScheduleTime.hour()) : '6';
        String minutes = String.valueOf(nextScheduleTime.minute());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        String jobName = 'Customer Extract Delta: ' + nextScheduleTime.format('hh:mm');

        CustomerExtractSchedulable nextJob = new CustomerExtractSchedulable();
        System.schedule(jobName, cronValue , nextJob);

        System.abortJob(SC.getTriggerId());
    }
}