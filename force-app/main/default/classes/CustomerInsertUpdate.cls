public without sharing class CustomerInsertUpdate {

    private final static String SOAP_NS = 'http://schemas.xmlsoap.org/soap/envelope/';
    private final static String PIP = 'http://www.dmotorworks.com/pip-customer';
    private final static String XMLNS_SAVE = 'http://save.customer.pip.adp3pa.dmotorworks.com/';
    private final static String PATH_CUSTOMER_INSERT = '/pip-customer/services/CustomerInsertUpdate';
    public static final String QUERY_SEARCH_CUSTOMER_VEHICLE = 'CUST_VEH_Search';
    public static final String PATH_SEARCH_CUSTOMER_VEHICLE = '/pip-extract/customer-vehicle-search/extract';

    public static String getCustomerNumber(String dealerId) {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', SOAP_NS, 'soapenv');
        envelope.setNamespace('save', XMLNS_SAVE);
        envelope.setNamespace('pip', PIP);

        dom.XmlNode body = envelope.addChildElement('Body', SOAP_NS, 's');
        dom.XmlNode insertTag = body.addChildElement('getCustomerNumber', PIP, 'save');

        dom.XmlNode arg0 = insertTag.addChildElement('arg0', null, null);
        arg0.addChildElement('password', null, null)
                .addTextNode(settings.Password__c);
        arg0.addChildElement('username', null, null)
                .addTextNode(settings.Username__c);

        dom.XmlNode arg1 = insertTag.addChildElement('arg1', null, null);
        arg1.addChildElement('dealerId', null, null)
                .addTextNode(dealerId);

        dom.XmlNode arg2 = insertTag.addChildElement('arg2', null, null);
        arg2.addChildElement('userId', null, null)
                .addTextNode('111');

        System.debug('GET CUSTOMER ID');
        System.debug('request');
        System.debug(doc.toXmlString());

        HttpResponse response = DMotorWorksApi.post(PATH_CUSTOMER_INSERT, doc.toXmlString());
        System.debug('response body ' + response.getBody());
        System.debug('response status  ' + response.getStatusCode());
        System.debug('response body document ' + response.getBodyDocument());

        return readCustomerNumberResponse(response.getBodyDocument());

        // String xmlString = response.getBodyDocument().toXmlString();

        // if(String.isEmpty(xmlString) ) {
        //         Dom.Document doc1 = new Dom.Document();
        //         doc1.load(response.getBody());
        //     return readCustomerNumberResponse(doc1);
        // } else {
        //        return readCustomerNumberResponse(response.getBodyDocument());
        // }
    }

    public static String readCustomerNumberResponse(Dom.Document doc) {
        Dom.XmlNode envelopeResult = doc.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement('Body', SOAP_NS);
        Dom.XmlNode getCustomerNumberResponse = bodyResult.getChildElement('getCustomerNumberResponse', PIP);
        Dom.XmlNode returnResult = getCustomerNumberResponse.getChildElement('return', null);
        Dom.XmlNode code = returnResult.getChildElement('code', null);

        if (code != null && code.getText() == 'success') {
            Dom.XmlNode customerNumber = returnResult.getChildElement('customerNumber', null);

            if (customerNumber != null) {
                System.debug('getCustomerNumberResponse');
                System.debug(customerNumber.getText());

                return customerNumber.getText();
            }
        }

        return null;
    }

    public static CustomerInfo insertUpdateCustomer(String dealerId, Id customerId) {
        Account customer = [
                SELECT Id, CDK_Dealer_Code__c, CDK_Customer_Number__c, CDK_Customer_Message__c,
                        CDK_Customer_Checksum__c, PersonMobilePhone, PersonHomePhone,
                        PersonOtherPhone, FirstName, LastName, MiddleName,
                        PersonMailingStreet, PersonMailingCity,
                        PersonMailingCountry, Mailing_County__c, PersonMailingPostalCode,
                        PersonMailingState, PersonEmail, Email_Secondary__pc,
                        Customer_Primary_Phone__c, PersonBirthdate, Employer__pc,
                        Customer_Type__c, Company_Name__c
                FROM Account
                WHERE Id = :customerId
                LIMIT 1
        ];

        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();
        String newCustomerNumber = getCustomerNumber(dealerId);

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', SOAP_NS, 's');
        envelope.setNamespace('pip', PIP);

        dom.XmlNode body = envelope.addChildElement('Body', SOAP_NS, 's');
        dom.XmlNode insertTag = body.addChildElement('insert', PIP, 'ns2');

        dom.XmlNode arg1 = insertTag.addChildElement('arg1', null, null);
        arg1.addChildElement('dealerId', null, null).addTextNode(dealerId);

        dom.XmlNode arg0 = insertTag.addChildElement('arg0', null, null);
        arg0.addChildElement('password', null, null)
                .addTextNode(settings.Password__c);
        arg0.addChildElement('username', null, null)
                .addTextNode(settings.Username__c);

        dom.XmlNode arg2 = insertTag.addChildElement('arg2', null, null);
        arg2.addChildElement('userId', null, null)
                .addTextNode('111');

        dom.XmlNode arg3 = insertTag.addChildElement('arg3', null, null);

        dom.XmlNode idTag = arg3.addChildElement('id', null, null);
        idTag.addChildElement('value', null, null)
                .addTextNode(newCustomerNumber);

        dom.XmlNode address = arg3.addChildElement('address', null, null);
        address.addChildElement('addressLine', null, null)
                .addTextNode(customer.PersonMailingStreet == null ? '' : customer.PersonMailingStreet);
        address.addChildElement('city', null, null)
                .addTextNode(customer.PersonMailingCity == null ? '' : customer.PersonMailingCity);
        address.addChildElement('country', null, null)
                .addTextNode(customer.PersonMailingCountry == null ? '' : customer.PersonMailingCountry);
        address.addChildElement('county', null, null)
                .addTextNode(customer.Mailing_County__c == null ? '' : customer.Mailing_County__c);
        address.addChildElement('postalCode', null, null)
                .addTextNode(customer.PersonMailingPostalCode == null ? '' : customer.PersonMailingPostalCode);
        address.addChildElement('stateOrProvince', null, null)
                .addTextNode(customer.PersonMailingState == null ? '' : customer.PersonMailingState);

        dom.XmlNode contactInfo = arg3.addChildElement('contactInfo', null, null);

        dom.XmlNode contactTime = contactInfo.addChildElement('contactTime', null, null);
        contactTime.addChildElement('description', null, null)
                .addTextNode('');
        contactTime.addChildElement('time', null, null)
                .addTextNode('');

        if (String.isNotBlank(customer.PersonEmail)) {
            dom.XmlNode emailTag1 = contactInfo.addChildElement('email', null, null);
            emailTag1.addChildElement('desc', null, null)
                    .addTextNode('Cellular');
            emailTag1.addChildElement('value', null, null)
                    .addTextNode(customer.PersonEmail);
        }

        if (String.isNotBlank(customer.Email_Secondary__pc)) {
            dom.XmlNode emailTag2 = contactInfo.addChildElement('email', null, null);
            emailTag2.addChildElement('desc', null, null)
                    .addTextNode('Home');
            emailTag2.addChildElement('value', null, null)
                    .addTextNode(customer.Email_Secondary__pc);
        }

//        if (String.isNotBlank(opp.Customer_Email_3__c)) {
//            dom.XmlNode emailTag3 = contactInfo.addChildElement('email', null, null);
//            emailTag3.addChildElement('desc', null, null)
//                    .addTextNode('Work');
//            emailTag3.addChildElement('value', null, null)
//                    .addTextNode(opp.Customer_Email_3__c);
//        }

        contactInfo.addChildElement('preferredContactDay', null, null)
                .addTextNode('');
        contactInfo.addChildElement('preferredContactMethod', null, null)
                .addTextNode('');
        contactInfo.addChildElement('preferredLanguage', null, null)
                .addTextNode('');

        dom.XmlNode telephoneNumber1 = contactInfo.addChildElement('telephoneNumber', null, null);
        if(String.isNotBlank(customer.PersonMobilePhone)) {
                customer.PersonMobilePhone = customer.PersonMobilePhone.replaceAll('[^0-9]', '');
        }
        telephoneNumber1.addChildElement('desc', null, null)
                .addTextNode(customer.PersonMobilePhone == null ? '' : 'Cellular');
        telephoneNumber1.addChildElement('exten', null, null)
                .addTextNode('');
        telephoneNumber1.addChildElement('value', null, null)
                .addTextNode(customer.PersonMobilePhone == null ? '' : customer.PersonMobilePhone);

        dom.XmlNode telephoneNumber2 = contactInfo.addChildElement('telephoneNumber', null, null);
        if(String.isNotBlank(customer.PersonHomePhone)) {
                customer.PersonHomePhone = customer.PersonHomePhone.replaceAll('[^0-9]', '');
        }
        telephoneNumber2.addChildElement('desc', null, null)
                .addTextNode(customer.PersonHomePhone == null ? '' : 'Home');
        telephoneNumber2.addChildElement('exten', null, null)
                .addTextNode('');
        telephoneNumber2.addChildElement('value', null, null)
                .addTextNode(customer.PersonHomePhone == null ? '' : customer.PersonHomePhone);

        dom.XmlNode telephoneNumber3 = contactInfo.addChildElement('telephoneNumber', null, null);
        if(String.isNotBlank(customer.PersonOtherPhone)) {
                customer.PersonOtherPhone = customer.PersonOtherPhone.replaceAll('[^0-9]', '');
        }
        telephoneNumber3.addChildElement('desc', null, null)
                .addTextNode(customer.PersonOtherPhone == null ? '' : 'Work');
        telephoneNumber3.addChildElement('exten', null, null)
                .addTextNode('');
        telephoneNumber3.addChildElement('value', null, null)
                .addTextNode(customer.PersonOtherPhone == null ? '' : customer.PersonOtherPhone);

        dom.XmlNode telephoneNumber4 = contactInfo.addChildElement('telephoneNumber', null, null);
        telephoneNumber4.addChildElement('desc', null, null).addTextNode('');
        telephoneNumber4.addChildElement('exten', null, null).addTextNode('');
        telephoneNumber4.addChildElement('main', null, null).addTextNode('');
        telephoneNumber4.addChildElement('value', null, null).addTextNode('');

        if (customer.Customer_Primary_Phone__c == 'Mobile') {
            telephoneNumber1.addChildElement('main', null, null).addTextNode('Y');
            telephoneNumber2.addChildElement('main', null, null).addTextNode('');
            telephoneNumber3.addChildElement('main', null, null).addTextNode('');
        } else if (customer.Customer_Primary_Phone__c == 'Home') {
            telephoneNumber1.addChildElement('main', null, null).addTextNode('');
            telephoneNumber2.addChildElement('main', null, null).addTextNode('Y');
            telephoneNumber3.addChildElement('main', null, null).addTextNode('');
        } else if (customer.Customer_Primary_Phone__c == 'Work') {
            telephoneNumber1.addChildElement('main', null, null).addTextNode('');
            telephoneNumber2.addChildElement('main', null, null).addTextNode('');
            telephoneNumber3.addChildElement('main', null, null).addTextNode('Y');
        } else {
            telephoneNumber1.addChildElement('main', null, null).addTextNode('');
            telephoneNumber2.addChildElement('main', null, null).addTextNode('');
            telephoneNumber3.addChildElement('main', null, null).addTextNode('');
        }

        dom.XmlNode demographics = arg3.addChildElement('demographics', null, null);
        demographics.addChildElement('birthDate', null, null)
                .addTextNode(customer.PersonBirthdate == null ? '' : customer.PersonBirthdate.format());

        arg3.addChildElement('employerName', null, null)
                .addTextNode(customer.Employer__pc == null ? '' : customer.Employer__pc);
        arg3.addChildElement('dealerLoyaltyIndicator', null, null).addTextNode('');
        arg3.addChildElement('delCdeServiceNames', null, null).addTextNode('');
        arg3.addChildElement('deleteCode', null, null).addTextNode('');

        dom.XmlNode name1 = arg3.addChildElement('name1', null, null);

        if (customer.Customer_Type__c == 'Business') {
            name1.addChildElement('companyName', null, null)
                    .addTextNode(customer.Company_Name__c == null ? '' : customer.Company_Name__c);
            name1.addChildElement('nameType', null, null)
                    .addTextNode(customer.Customer_Type__c == null ? '' : customer.Customer_Type__c);
        } else {
            name1.addChildElement('firstName', null, null)
                    .addTextNode(customer.FirstName == null ? '' : customer.FirstName);
            name1.addChildElement('lastName', null, null)
                    .addTextNode(customer.LastName == null ? '' : customer.LastName);
            name1.addChildElement('middleName', null, null)
                    .addTextNode(customer.MiddleName == null ? '' : customer.MiddleName);
//            name1.addChildElement('fullName', null, null)
////                    .addTextNode(customer.Name == null ? '' : customer.Name);
            name1.addChildElement('nameType', null, null).addTextNode('Person');
            name1.addChildElement('suffix', null, null).addTextNode('');
            name1.addChildElement('title', null, null).addTextNode('');
        }

        System.debug('CUSTOMER INSERT');
        System.debug('request');
        System.debug(doc.toXmlString());

        HttpResponse response = DMotorWorksApi.post(PATH_CUSTOMER_INSERT, doc.toXmlString());
        System.debug('response body ' + response.getBody());
        System.debug('response status  ' + response.getStatusCode());
        System.debug('response body document ' + response.getBodyDocument());

        return parseInsertResponse(response.getBodyDocument());
    }

    public static CustomerInfo parseInsertResponse(DOM.Document doc) {
        Dom.XmlNode envelopeResult = doc.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement('Body', SOAP_NS);
        Dom.XmlNode getCustomerNumberResponse = bodyResult.getChildElement('insertResponse', PIP);
        Dom.XmlNode returnResult = getCustomerNumberResponse.getChildElement('return', null);
        Dom.XmlNode code = returnResult.getChildElement('code', null);

        if (code != null && code.getText() == 'success') {
            Dom.XmlNode customerParty = returnResult.getChildElement('customerParty', null);
            Dom.XmlNode checksum = customerParty.getChildElement('checksum', null);
            Dom.XmlNode custId = customerParty.getChildElement('id', null);
            Dom.XmlNode custIdValue = custId.getChildElement('value', null);

            CustomerInfo custInfo = new CustomerInfo();
            custInfo.checksum = checksum.getText();
            custInfo.custId = custIdValue.getText();

            return custInfo;
        } else {
            Dom.XmlNode message = returnResult.getChildElement('message', null);

            CustomerInfo custInfo = new CustomerInfo();
            custInfo.error = 'Customer Insert error: ' + message.getText();

            System.debug(code.getText());
            System.debug(message.getText());

            return custInfo;
        }
    }

    public static String searchCustomerVehicle(String dealerId, String qparamType, String qparamValue) {
        System.debug('GET CUTOMER NUMBER');

        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put('dealerId', dealerId);
        paramsMap.put('qparamType', qparamType);
        paramsMap.put('qparamValue', qparamValue);
        paramsMap.put('queryId', QUERY_SEARCH_CUSTOMER_VEHICLE);
        System.debug('paramsMap');
        System.debug(paramsMap);

        HttpResponse response = DMotorWorksApi.get(PATH_SEARCH_CUSTOMER_VEHICLE, paramsMap);
        System.debug('response');
        System.debug(response);

        return readSearchCustomerVehicleResponse(response.getBodyDocument());
    }

    public static String readSearchCustomerVehicleResponse(DOM.Document doc) {
        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == 'W2kCustomerVehicleID') {
                Dom.XmlNode custNo = child.getChildElement('CustNo', 'http://www.dmotorworks.com/pip-extract-customer-vehicle-search');

                if (custNo != null) {
                    return custNo.getText();
                }
            }
        }

        return null;
    }

    public class CustomerInfo {
        public String custId;
        public String checksum;
        public String error;
    }

}