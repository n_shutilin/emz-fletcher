public class DMotorWorksApi {

    public static String getAuthHeader() {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();
        String authHeader = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(settings.Username__c + ':' + settings.Password__c));
        return authHeader;
    }

    public static HttpResponse get(String path, Map<String, String> paramsMap) {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();

        List<String> params = new List<String>();
        for (String paramName : paramsMap.keySet()) {
            params.add(paramName + '=' + paramsMap.get(paramName));
        }

        String requestBody = String.join(params, '&');

        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setTimeout(120000);
        req.setHeader('Authorization', DMotorWorksApi.getAuthHeader());
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setEndpoint(settings.DMotorWorks_Url__c + path);
        req.setBody(requestBody);

        System.debug('request ENDPOINT= = ' + req.getEndpoint());
        System.debug('request HEADER= = ' + req.getHeader('Authorization'));
        System.debug('request BODY= = ' + req.getBody());
        System.debug('request FULL= = ' + req);
        System.debug('request body JSON = ' + JSON.serialize(req.getBody()));

        HttpResponse res = new Http().send(req);
        DMotorWorksApi.log(req, res);
        if (res.getStatus() == 'OK') {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'CDK Integration', res.getBody(), req.getBody());
        } else {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'CDK Integration', res.getBody(), req.getBody());
        }
        return res;
    }

    public static HttpResponse post(String path, String body) {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setTimeout(60000);
        req.setHeader('Authorization', DMotorWorksApi.getAuthHeader());
        req.setHeader('Content-Type', 'text/xml');
        req.setEndpoint(settings.DMotorWorks_Url__c + path);
        req.setBody(body);

        HttpResponse res = new Http().send(req);
        DMotorWorksApi.log(req, res);
        if (res.getStatus() == 'OK') {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'CDK Integration', res.getBody(), req.getBody());
        } else {
            createIntegrationLog(res.getStatus(), '', req.getEndpoint(), 'CDK Integration', res.getBody(), req.getBody());
        }
        return res;
    }

    public static void log(HttpRequest req, HttpResponse res) {
        System.debug('📡');
        System.debug(LoggingLevel.INFO, 'endpoint: ' + req.getEndpoint());

        if (res.getStatusCode() != 200 && res.getStatusCode() != 500) {
            System.debug(LoggingLevel.ERROR, 'status code: ' + res.getStatusCode());
            System.debug(LoggingLevel.ERROR, 'status: ' + res.getStatus());
            System.debug(LoggingLevel.ERROR, 'request: ' + req.getBody());

            throw new DMotorWorksApiException(res.getBody());
        }

        System.debug(LoggingLevel.INFO, 'response: ' + res.getBody());
    }

    public class DMotorWorksApiException extends Exception {

    }

    public static void createIntegrationLog(String result, String errorMessage, String endpoint, String process, String resBody, String reqBody) {
        IntegrationLogger.addLog(
            new Integration_Logs__c(
                Result__c = result,
                Error_Message__c = errorMessage,
                Endpoint__c = endpoint,
                Process__c = process
            ),
            new List<Attachment>{
                new Attachment(
                    Name = 'Request',
                    ContentType = 'text/plain',
                    Body = Blob.valueOf(reqBody)
                ),
                new Attachment(
                    Name = 'Response',
                    ContentType = 'text/plain',
                    Body = Blob.valueOf(resBody)
                )
            }
        );
        try {
           // IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }
}