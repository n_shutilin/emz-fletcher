@isTest
global class DMotorWorksApiMock implements HttpCalloutMock{
    
    private static final String DEFAULT_BODY = '<?xml version=\'1.0\' encoding=\'UTF-8\'?>'+
             +'<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">' +
                +'<S:Body>' +
                    +'<ns2:insertResponse xmlns:ns2="http://www.dmotorworks.com/pip-serviceappointment">' +
                        +'<return>' +
                            +'<code>success</code>' +
                            +'<serviceAppointment>' +
                                +'<apptID>1923917474369</apptID>' +
                                +'<apptDate>2020-09-03T00:00:00-05:00</apptDate>' +
                                +'<apptTime>08:00:00</apptTime>' +
                                +'<promiseDate>2020-09-03T00:00:00-05:00</promiseDate>' +
                                +'<promiseTime>08:00:00</promiseTime>' +
                                +'<comments></comments>' +
                                +'<remarks></remarks>' +
                                +'<dispatchMakeCode>C</dispatchMakeCode>' +
                                +'<lineCount>1</lineCount>' +
                                +'<opCount>1</opCount>' +
                                +'<partsCount>0</partsCount>' +
                                +'<checksum>5875182</checksum>' +
                                +'<dealer>' +
                                    +'<reservationist></reservationist>' +
                                    +'<serviceAdvisor>123</serviceAdvisor>' +
                                +'</dealer>' +
                                +'<customer>' +
                                    +'<contactInfo>1234567891</contactInfo>' +
                                    +'<name>' +
                                        +'<fullName></fullName>' +
                                    +'</name>' +
                                    +'<partyID>' +
                                        +'<ID>6488</ID>' +
                                    +'</partyID>' +
                                    +'<transportation></transportation>' +
                                    +'<waiterFlag>N</waiterFlag>' +
                                +'</customer>' +
                                +'<vehicle>' +
                                    +'<model></model>' +
                                    +'<odometerStatus>0</odometerStatus>' +
                                    +'<vehicleID>5A022222</vehicleID>' +
                                +'</vehicle>' +
                                +'<totals />' +
                                +'<lines>' +
                                    +'<key>A</key>' +
                                    +'<complaint></complaint>' +
                                    +'<serviceRequest>Sales Appointment Defult Description</serviceRequest>' +
                                    +'<laborType>C</laborType>' +
                                    +'<comebackFlag>N</comebackFlag>' +
                                    +'<technician></technician>' +
                                    +'<dispatchCode>S110</dispatchCode>' +
                                    +'<estDuration>0.0</estDuration>' +
                                    +'<cause></cause>' +
                                    +'<urgencyCode></urgencyCode>' +
                                    +'<concernCode></concernCode>' +
                                    +'<serviceEstimate></serviceEstimate>' +
                                    +'<laborEstimate></laborEstimate>' +
                                    +'<partsEstimate></partsEstimate>' +
                                    +'<miscEstimate></miscEstimate>' +
                                    +'<lubeEstimate></lubeEstimate>' +
                                    +'<subletEstimate></subletEstimate>' +
                                    +'<taxEstimate></taxEstimate>' +
                                    +'<ops>' +
                                        +'<key>1</key>' +
                                        +'<opCode></opCode>' +
                                        +'<opCodeDesc></opCodeDesc>' +
                                        +'<labor>C</labor>' +
                                        +'<soldHours>0.00</soldHours>' +
                                        +'<saleAmount>0.00</saleAmount>' +
                                        +'<saleOverride>N</saleOverride>' +
                                        +'<forceShopCharge>N</forceShopCharge>' +
                                        +'<shopCharge>0.00</shopCharge>' +
                                    +'</ops>' +
                                +'</lines>' +
                                +'<errorCode>success</errorCode>' +
                            +'</serviceAppointment>' +
                        +'</return>' +
                    +'</ns2:insertResponse>' +
                +'</S:Body>' +
            +'</S:Envelope>';
    
    private String htmlBody;
    
    global DMotorWorksApiMock(){
        this(DEFAULT_BODY);
    }	
    
    global DMotorWorksApiMock(String htmlBody){
        this.htmlBody = htmlBody;
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        System.debug(htmlBody);
        res.setBody(htmlBody);
        res.setStatusCode(200);
        return res;
    }
}