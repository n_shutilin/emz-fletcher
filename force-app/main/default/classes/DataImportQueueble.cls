public without sharing class DataImportQueueble implements Queueable{
    private String xmlBody;
    private String accountId;
    private String objectName;

    public DataImportQueueble(String xmlBody, String accountId, String objectName) {
        this.xmlBody = xmlBody;
        this.accountId = accountId;
        this.objectName = objectName;
    }

    public void execute(QueueableContext context) {
        Dom.Document doc = new Dom.Document();
        doc.load(xmlBody);

        if (objectName == 'ServiceSalesClosed') {
            List<WorkOrder> workOrders = ServiceSalesClosedParser.process(doc, accountId, 'closed');
            Database.UpsertResult[] results = Database.upsert(workOrders, WorkOrder.fields.UnuqueRONumber__c, true);     
        } else if (objectName == 'ServiceSalesDetailsClosed') {
            List<WorkOrderLineItem> lineItems = ServiceSalesClosedChildParser.process(doc, accountId, 'closed');
            Database.UpsertResult[] results = Database.upsert(lineItems, WorkOrderLineItem.fields.Unique_Identifier__c, true);
        }  else if (objectName == 'ServiceSalesClosedHistory') {
            List<RepairOrder__b> repairOrders = ServiceSalesClosedHistoryParser.process(doc, accountId);
            if( !Test.isRunningTest() ){
                Database.insertImmediate(repairOrders);
            }
        } else if (objectName == 'ServiceSalesClosedDetailsHistory') {
            List<RepairOrderDetail__b> roDetails = ServiceSalesClosedDetailsHistoryParser.process(doc, accountId);
            if( !Test.isRunningTest() ){
                List<Database.SaveResult> l = Database.insertImmediate(roDetails);
                System.debug(l);
            }
        }
    }
}