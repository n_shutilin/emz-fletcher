@RestResource(urlMapping='/DataImport')
global without sharing class DataImportWebService {
    @HttpPost
    global static Integer upsertData() {
        RestRequest request = RestContext.request;
        String xmlBody = request.requestBody.toString();
        String dealerId = request.params.get('dealerId');
        String objectName = request.params.get('objectName');
        Id accountId = [
            SELECT Id, Dealer_Code__c
            FROM Account
            WHERE CDK_Dealer_Code__c =: dealerId
        ].Id;
        
        ID jobID = System.enqueueJob(new DataImportQueueble(xmlBody, accountId, objectName));

        return 40 - [SELECT COUNT() FROM AsyncApexJob WHERE JobType='Queueable' AND Status IN ('Processing','Preparing','Queued')] ;
    }
    
}