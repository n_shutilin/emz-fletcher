// Batch Job for Processing the Records

  global class DavidLRB implements Database.Batchable<sObject> {

      
// Start Method

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String query = 'SELECT Id,AccountId,Name,CreatedDate,StageName,Lease_Maturity_Date__c,Dealer_Id__c,CloseDate FROM Opportunity WHERE Name = \'Test David Opp\'';

        return Database.getQueryLocator(query);

    }

   // Execute method

    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
         Id DealerRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealer').getRecordTypeId();
         Id LRRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Lease Retention').getRecordTypeId();
         Map<String,Double> DealerMap = New Map<String,Double>();
         List<Opportunity> newOpps = new List<Opportunity>();
         List<Account> DealerAccounts = [Select Dealer_Code__c, Auto_Create_Lease_Retention_Days__c from Account where RecordTypeId = :DealerRecordType];
         for(Account acc: DealerAccounts){
            DealerMap.put(acc.Dealer_Code__c,acc.Auto_Create_Lease_Retention_Days__c);
             system.debug(acc.id);
        }
         for(Opportunity opp : scope)
         {            
             // Date OppCreatedDate = date.newInstance(opp.createdDate.year(), opp.createdDate.month(), opp.createdDate.day());
             Integer LeaseMaturityDate = opp.Lease_Maturity_Date__c.monthsBetween(system.today());
             system.debug(opp.Id);
             
             Double MaturitySetting = DealerMap.get(opp.Dealer_ID__c);
             system.debug(LeaseMaturityDate +''+ MaturitySetting);
             if(LeaseMaturityDate == MaturitySetting ){
                 Opportunity newOpp = opp.clone();
                 newOpp.RecordTypeId = LRRecordType;
                 newOpp.StageName = 'Initial Interest';
                 newOpp.External_Id__c = '';
                 newOpps.add(newOpp);
             }
         }
        try{
         insert newOpps;
        }catch (Exception e){
                System.debug(e.getMessage());
        }
    }   

    // Finish Method

    global void finish(Database.BatchableContext BC) {

    }

}