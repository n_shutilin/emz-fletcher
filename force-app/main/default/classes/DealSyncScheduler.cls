/**
 * Created by user on 2/4/2020.
 */

global class DealSyncScheduler implements Schedulable {
    global void execute(SchedulableContext SC) {
        DealSyncBatch batchDeal = new DealSyncBatch();
        Id vehBatchId = Database.executeBatch(batchDeal,1);
    }
}