@RestResource(urlMapping='/DealsIntegration')
global class DealsIntegrationWebService {
    @HttpPost
    global static void upsertDeals() {
        RestRequest request = RestContext.request;
        String filename = request.headers.get('description');
        String fileNameMessage = filename != null ? 'File name: ' + filename + ' ': '';
        String requestBody = request.requestBody.toString();
        List<NBMBN_IMPORT__c> deals = (List<NBMBN_IMPORT__c>) JSON.deserialize(requestBody, List<NBMBN_IMPORT__c>.class);
        try {
            Database.upsert(deals, NBMBN_IMPORT__c.SaleUniqueDealNo__c, true);
            Long timeStamp = DateTime.now().getTime();
            Integration_Logs__c successLog = new Integration_Logs__c(
                Endpoint__c = '*/DealsIntegration SIZE: ' + deals.size() + ' records; ' + fileNameMessage,
                Result__c = 'OK',
                Process__c = 'Advent-FTP integration'
            );
            insert successLog;
        } catch (Exception e) {
            Long timeStamp = DateTime.now().getTime();
            Integration_Logs__c errorLog = new Integration_Logs__c(
                Endpoint__c = '*/DealsIntegration',
                Posix_Time__c = timeStamp,
                Error__c = e.getTypeName(),
                Error_Message__c = fileNameMessage + e.getMessage(),
                Process__c = 'Advent-FTP integration'
            );
            insert errorLog;
        }
    }
}