public without sharing class DeleteDataBatchable implements Database.Batchable<SObject> {
    public String dealer;

    public DeleteDataBatchable(String dealer) {
        this.dealer = dealer;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'select id from workorder where Dealer_ID__c = \'' + dealer + '\'';
        System.debug(query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<workorder> scope) {
        try {
           delete scope;
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, e.getStackTraceString());
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
    }

    public void finish(Database.BatchableContext bc) {
        
    }
}