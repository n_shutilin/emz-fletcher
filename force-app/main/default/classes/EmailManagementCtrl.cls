public with sharing class EmailManagementCtrl {
    private static final Integer RECORDS_ON_PAGE = 15;
    private static final String LIGHTNING_EMAIL_MESSAGE_URL = '/lightning/r/EmailMessage/';
    private static final String VIEW_URL_PARAM = '/view';

    @AuraEnabled(Cacheable=true)
    public static List<OptionsWrapper> getUserOptions() {
        List<OptionsWrapper> userOptions = new List<OptionsWrapper>();

        List<User> users = [
                SELECT Name, Email
                FROM User
                WHERE Id = :UserInfo.getUserId()
                OR ManagerId = :UserInfo.getUserId()
        ];

        for (User user : users) {
            userOptions.add(new OptionsWrapper(user.Name, user.Email));
        }

        return userOptions;
    }

    @AuraEnabled
    public static List<MessageWrapper> getMessages(String address, String dateStr, String filterStr, String emailOption, Integer pageNum) {
        Integer offset = (pageNum - 1) * RECORDS_ON_PAGE;
        Date dateFilter;

        if (dateStr!= null) {
            dateFilter = Date.valueOf(dateStr);
        }

        Set<Id> emsIds = getEmsIds(address);
        String query = getQuery(address, dateFilter, filterStr, emailOption, emsIds);

        query += ' ORDER BY CreatedDate DESC LIMIT :RECORDS_ON_PAGE OFFSET :offset';
        System.debug('--- query: ' + query);

        List<EmailMessageRelation> messageRelations = Database.query(query);
        List<MessageWrapper> messages = new List<MessageWrapper>();

        if (!messageRelations.isEmpty()) {
            for (EmailMessageRelation relation : messageRelations) {
                messages.add(new MessageWrapper(relation, emsIds));
            }
        }

        return messages;
    }

    @AuraEnabled
    public static List<MessageWrapper> getInboundEmailNotifications() {
        String currentUserEmail = UserInfo.getUserEmail();
        System.debug(currentUserEmail);

        if(String.isBlank(currentUserEmail)) {
            throw new AuraHandledException('Error');
        }

        List<Email_Message_Opener__c> emsOpeners = [
            SELECT Email_Message_Id__c
            FROM Email_Message_Opener__c
        ];

        Set<Id> emsIds = new Set<Id>();

        for (Email_Message_Opener__c emsOpener : emsOpeners) {
            emsIds.add(emsOpener.Email_Message_Id__c);
        }

        List<EmailMessageRelation> messageRelations  = [
            SELECT EmailMessageId, EmailMessage.FromName, EmailMessage.FromAddress, EmailMessage.ToAddress,
                EmailMessage.Subject, EmailMessage.RelatedToId, Relation.Name, EmailMessage.RelatedTo.Name,
                CreatedDate, RelationId, RelationObjectType, EmailMessage.IsOpened, EmailMessage.Incoming, RelationType
            FROM EmailMessageRelation
            WHERE EmailMessage.ToAddress = :currentUserEmail
            AND EmailMessageId IN :emsIds
            AND (
                RelationType = 'ToAddress'
                OR RelationType = 'CcAddress'
                OR RelationType = 'BccAddress'
            )
            ORDER BY CreatedDate DESC
        ];

        List<MessageWrapper> messages = new List<MessageWrapper>();

        if (!messageRelations.isEmpty()) {
            for (EmailMessageRelation relation : messageRelations) {
                messages.add(new MessageWrapper(relation, emsIds));
            }
        }

        return messages;
    }

    @AuraEnabled
    public static Double getPagesCount(String address, String dateStr, String filterStr, String emailOption) {
        Set<Id> emsIds = getEmsIds(address);
        Date dateFilter;

        if (dateStr!= null) {
            dateFilter = Date.valueOf(dateStr);
        }

        String query = getQuery(address, dateFilter, filterStr, emailOption, emsIds);

        List<EmailMessageRelation> messageRelations = Database.query(query);

        Double pagesCount = Math.ceil(Double.valueOf(messageRelations.size()) / Double.valueOf(RECORDS_ON_PAGE));

        return pagesCount;
    }

    @AuraEnabled
    public static void deleteOpener(Id recId) {
        User user = [
            SELECT Email
            FROM User
            WHERE Id = :UserInfo.getUserId()
            LIMIT 1
        ];

        List<Email_Message_Opener__c> openers = [
            SELECT Id
            FROM Email_Message_Opener__c
            WHERE Email_Message_Id__c = :recId
            AND Email__c = :user.Email
        ];

        if (!openers.isEmpty()) {
            delete openers;

            Email_Opener__e opener = new Email_Opener__e(Email_Message_Id__c = recId);
            EventBus.publish(opener);
        }
    }

    private static Set<Id> getEmsIds(String email) {
        List<Email_Message_Opener__c> emsOpeners = [
            SELECT Email_Message_Id__c
            FROM Email_Message_Opener__c
            WHERE Email__c = :email
        ];

        Set<Id> emsIds = new Set<Id>();

        for (Email_Message_Opener__c emsOpener : emsOpeners) {
            emsIds.add(emsOpener.Email_Message_Id__c);
        }

        return emsIds;
    }

    private static String getQuery(String address, Date dateFilter, String filterStr, String emailOption, Set<Id> emsIds) {
        String query = 'SELECT EmailMessageId, EmailMessage.FromName, EmailMessage.FromAddress, EmailMessage.ToAddress,' +
            ' EmailMessage.Subject, EmailMessage.relatedToId, Relation.Name, EmailMessage.RelatedTo.Name,' +
            ' CreatedDate, RelationId, RelationObjectType, EmailMessage.IsOpened, EmailMessage.Incoming, RelationType' +
            ' FROM EmailMessageRelation' +
            ' WHERE RelationAddress = :address';

        if (dateFilter != null) {
            query += ' AND DAY_ONLY(CreatedDate) = :dateFilter';
        }

        if (filterStr != null) {
            query += ' AND EmailMessage.Subject LIKE \'%' + filterStr + '%\'';
        }

        if (emailOption != null) {
            if (emailOption == 'inbound') {
                query += ' AND (RelationType = \'ToAddress\' OR RelationType = \'CcAddress\' OR RelationType = \'BccAddress\')';
            } else if (emailOption == 'outbound') {
                query += ' AND RelationType = \'FromAddress\'';
            } else if (emailOption == 'Unread') {
                query += ' AND EmailMessageId IN :emsIds';
            }
        }

        return query;
    }

    private class MessageWrapper {
        @AuraEnabled public String id { get; set; }
        @AuraEnabled public String relationId { get; set; }
        @AuraEnabled public String fromAddress { get; set; }
        @AuraEnabled public String toAddress { get; set; }
        @AuraEnabled public String subject { get; set; }
        @AuraEnabled public String messageLink { get; set; }
        @AuraEnabled public String recordLink { get; set; }
        @AuraEnabled public String recordName { get; set; }
        @AuraEnabled public String sendingDateTime { get; set; }
        @AuraEnabled public Boolean isOpened { get; set; }
        @AuraEnabled public Boolean isInbound { get; set; }

        public MessageWrapper(EmailMessageRelation relation, Set<Id> notOpenedMessageIdSet) {
            String orgUrl = Url.getSalesforceBaseUrl().toExternalForm();

            id = relation.EmailMessageId;
            relationId = relation.Id;
            fromAddress = relation.EmailMessage.FromName != null ? relation.EmailMessage.FromName + ' <' + relation.EmailMessage.FromAddress + '>' : relation.EmailMessage.FromAddress;
            toAddress = relation.EmailMessage.ToAddress;
            subject = relation.EmailMessage.Subject;
            messageLink = orgUrl + LIGHTNING_EMAIL_MESSAGE_URL + relation.EmailMessage.Id + VIEW_URL_PARAM;

            if (relation.EmailMessage.RelatedToId != null) {
                recordLink = relation.EmailMessage.RelatedToId;
            } else if (relation.RelationId != null) {
                recordLink = relation.RelationId;
            } else {
                recordLink = '';
            }

            recordName = relation.EmailMessage.RelatedToId != null ? relation.EmailMessage.RelatedTo.Name : relation.Relation.Name;
            sendingDateTime = relation.CreatedDate.format('M/d/yyyy h:mm a');
            isOpened = !notOpenedMessageIdSet.contains(relation.EmailMessageId);
            isInbound = relation.RelationType == 'ToAddress' || relation.RelationType == 'CcAddress' || relation.RelationType == 'BccAddress';
        }
    }

    private class OptionsWrapper {
        @AuraEnabled public String label { get; set; }
        @AuraEnabled public String value { get; set; }

        public OptionsWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}