@IsTest
private class EmailManagementCtrlTest {

    @TestSetup
    private static void setup() {
        User user = [
                SELECT Email
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];

        Account acc = new Account(Name = 'TestAcc');
        insert acc;

        EmailMessage message = new EmailMessage(
                Subject = 'Test',
                HtmlBody = 'Test',
                RelatedToId = acc.Id,
                ToAddress = user.Email
        );
        insert message;

        insert new EmailMessageRelation(
                EmailMessageId = message.Id,
                RelationAddress = user.Email,
                RelationType = 'ToAddress'
        );
    }

    @IsTest
    private static void getUserOptions_Test() {
        Test.startTest();
        EmailManagementCtrl.getUserOptions();

        Test.stopTest();
    }

    @IsTest
    private static void getMessages_Test() {
        User user = [
                SELECT Email
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];

        String todayStr = Datetime.now().format('YYYY-MM-dd');

        Test.startTest();

        EmailManagementCtrl.getMessages(user.Email, todayStr, 'Test', 'inbound', 1);

        Test.stopTest();
    }

    @IsTest
    private static void getPagesCount_Test() {
        User user = [
                SELECT Email
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];

        String todayStr = Datetime.now().format('YYYY-MM-dd');

        Test.startTest();

        Double pagesCount = EmailManagementCtrl.getPagesCount(user.Email, todayStr, 'Test', null);
        System.assertEquals(1, pagesCount);

        Test.stopTest();
    }

    @IsTest
    static void getInboundEmailNotifications_Test() {
        EmailManagementCtrl.getInboundEmailNotifications();
    }

    @IsTest
    static void deleteOpener_Test() {
        EmailMessage ems = [SELECT Id FROM EmailMessage LIMIT 1];

        Test.startTest();
        EmailManagementCtrl.deleteOpener(ems.Id);
        Test.stopTest();
    }
}