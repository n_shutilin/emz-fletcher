public with sharing class EmailMessageTriggerHandler {
    private List<EmailMessage> newList;
    private Map<Id, EmailMessage> newMap;
    private Map<Id, EmailMessage> oldMap;

    public EmailMessageTriggerHandler(List<EmailMessage> newList, Map<Id, EmailMessage> newMap, Map<Id, EmailMessage> oldMap) {
        this.newList = newList;
        this.newMap = newMap;
        this.oldMap = oldMap;
    }

    public void handleAfterInsert() {
        createNotification();
    }

    private void createNotification() {
        Map<Id, Id> messageOppMap = new Map<Id, Id>();
        Map<Id, Set<String>> oppRecipientsMap = new Map<Id, Set<String>>();

        for (EmailMessage message : newList) {
            if (message.RelatedToId != null && String.valueOf(message.RelatedToId.getSobjectType()) == 'Opportunity') {
                messageOppMap.put(message.Id, message.RelatedToId);
                oppRecipientsMap.put(message.RelatedToId, new Set<String>());
            }
        }

        if (!oppRecipientsMap.isEmpty()) {
            List<Opportunity> opps = [
                SELECT OwnerId
                FROM Opportunity
                WHERE Id IN :oppRecipientsMap.keySet()
            ];

            for (Opportunity opp : opps) {
                oppRecipientsMap.get(opp.Id).add(opp.OwnerId);
            }

            List<OpportunityTeamMember> teamMembers = [
                SELECT OpportunityId, UserId
                FROM OpportunityTeamMember
                WHERE OpportunityId IN :oppRecipientsMap.keySet()
            ];

            if (!teamMembers.isEmpty()) {
                for (OpportunityTeamMember member : teamMembers) {
                    oppRecipientsMap.get(member.OpportunityId).add(member.UserId);
                }
            }

            CustomNotificationType notificationType = [
                SELECT Id, DeveloperName
                FROM CustomNotificationType
                WHERE DeveloperName = 'Email_Notifications'
            ];

            for (Id messageId : messageOppMap.keySet()) {
                EmailMessage message = newMap.get(messageId);
                Id targetId = message.RelatedToId;
                Set<String> recipientsIds = oppRecipientsMap.get(targetId);

                Messaging.CustomNotification notification = new Messaging.CustomNotification();

                notification.setTitle('Message From ' + message.FromAddress);
                notification.setBody(message.Subject);
                notification.setNotificationTypeId(notificationType.Id);
                notification.setTargetId(targetId);

                try {
                    notification.send(recipientsIds);
                }
                catch (Exception e) {
                    System.debug('Problem sending notification: ' + e.getMessage());
                }
            }
        }
    }
}