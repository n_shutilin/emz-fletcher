@IsTest
private class EmailMessageTriggerHandlerTest {

    @IsTest
    private static void test() {
        Opportunity opp = new Opportunity(
            Name = 'TestOpp',
            StageName = 'New',
            CloseDate = Date.today()
        );
        insert opp;

        Test.startTest();

        insert new EmailMessage(
            Subject = 'Test Subject',
            FromAddress = 'test@test.test',
            RelatedToId = opp.Id
        );

        Test.stopTest();
    }
}