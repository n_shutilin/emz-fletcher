public class EmailMessageTriggerHelper {
    public static Boolean isInsertForLead = false;
    
    public static void setRelatedToIdField(List<EmailMessage> ems) {   

        Set<String> emails = getEmails(ems);
        Map<String, String> dealers = getDealers(emails);
        List<Opportunity> opps = [
            SELECT Id, Dealer_Id__c, Customer_Email_Primary__c, Customer_Email_Secondary__c 
            FROM Opportunity
            WHERE Dealer_Id__c IN :dealers.values()
            AND ( Customer_Email_Primary__c IN :emails OR Customer_Email_Secondary__c IN :emails )
            AND ( StageName != 'Finalized' OR StageName != 'Closed Lost' )
            LIMIT 50000
        ];

        List<Account> accs = [
            SELECT Id, Dealer_ID__pc, PersonEmail, Email_Secondary__pc 
            FROM Account
            WHERE Dealer_Id__pc IN :dealers.values()
            AND ( PersonEmail IN :emails OR Email_Secondary__pc IN :emails )
            LIMIT 50000
        ];

        for (EmailMessage em : ems) {
            if (em.RelatedToId != null || em.toIds.size() > 0) continue;
            List<String> parsedEmails = parseEmails(em.ToAddress);
            for (String e : parsedEmails) {
                if (dealers.get(e) != null) {
                    String searchId = searchOppId(opps, dealers.get(e), e);
                    if (searchId != null) {
                        em.RelatedToId = searchId;
                        break;
                    }
                }                
            }
            if (em.RelatedToId != null) continue;
            for (String e : parsedEmails) {
                if (dealers.get(e) != null) {
                    String searchId = searchAccId(accs, dealers.get(e), e);
                    if (searchId != null) {
                        em.RelatedToId = searchId;
                        break;
                    }
                }                
            }
        }
    }
    
    public static void insertEmailMessageOpener(Map<Id, EmailMessage> newMap) {
        List<EmailMessageRelation> relations = [
            SELECT EmailMessageId, RelationAddress
            FROM EmailMessageRelation
            WHERE EmailMessageId IN :newMap.keySet()
            AND (
                RelationType = 'ToAddress'
                OR RelationType = 'CcAddress'
                OR RelationType = 'BccAddress'
            )
        ];

        if (!relations.isEmpty()) {
            List<Email_Message_Opener__c> openers = new List<Email_Message_Opener__c>();

            for (EmailMessageRelation rel : relations) {
                Email_Message_Opener__c opener = new Email_Message_Opener__c(
                    Email_Message_Id__c = rel.EmailMessageId,
                    Email__c = rel.RelationAddress
                );

                openers.add(opener);
            }

            insert openers;
        }
    }

    public static void createEmailMessageRelation(List<EmailMessage> ems) {
        Set<String> emails = getEmails(ems);
        Map<String, String> dealers = getDealers(emails);
        List<EmailMessageRelation> emrs = new List<EmailMessageRelation>();
        List<Id> recordForDelete = new List<Id>();

        List<Lead> leads = [
            SELECT Id, Dealer_Id__c, Email, Email_Secondary__c 
            FROM Lead
            WHERE Dealer_Id__c IN :dealers.values()
            AND ( Email IN :emails OR Email_Secondary__c IN :emails )
            LIMIT 50000
        ];

        for (EmailMessage em : ems) {
            if (em.RelatedToId != null || em.toIds.size() > 0) continue;
            List<String> parsedEmails = parseEmails(em.ToAddress);
            for (String e : parsedEmails) {
                if (dealers.get(e) != null) {
                    String searchId = searchLeadId(leads, dealers.get(e), e);
                    if (searchId != null) {
                        EmailMessage cloneEM = em.clone(false,false);
                        isInsertForLead = true;
                        cloneEM.ToIds = new List<Id>{Id.valueOf(searchId)};      
                        insert cloneEM;
                                               
                        EmailMessageRelation emr = new EmailMessageRelation(
                            EmailMessageId = cloneEM.Id,  
                            RelationId = Id.valueOf(searchId),
                            RelationType = 'ToAddress'
                        );
                        emrs.add(emr);
                        recordForDelete.add(em.Id);
                        isInsertForLead = false;
                        break;
                    } 
                }                
            }
        }

        if (Trigger.new != null && !recordForDelete.isEmpty()) {
            delete [SELECT id FROM EmailMessage WHERE Id IN :Trigger.new AND Id IN :recordForDelete];
        }
        insert emrs;
    }

    private static Set<String> getEmails(List<EmailMessage> ems) {
        Set<String> emails = new Set<String>();
        for (EmailMessage em : ems) {
            List<string> listOfEmails = em.ToAddress.split('\\;');
            for(String e : listOfEmails) {
                e.deleteWhitespace();
                emails.add(e);
            }
        }
        return emails;
    }

    private static Map<String, String> getDealers(Set<String> emails){
        Map<String, String> dealers = new Map<String, String>();
        for (User u : [SELECT Dealer_Id__c, Email FROM User WHERE Email IN :emails]) {
            dealers.put(u.Email, u.Dealer_Id__c);
        }
        return dealers;
    }

    private static String searchOppId(List<Opportunity> ops, String  Dealer_Id, String Email){
        for (Opportunity o : ops) {
            if (o.Dealer_Id__c == Dealer_Id && o.Customer_Email_Primary__c == Email || o.Customer_Email_Secondary__c == Email) {
                return o.Id;
            }
        }
        return null;
    }

    private static String searchLeadId(List<Lead> lds, String  Dealer_Id, String Email){
        for (Lead l : lds) {
            if (l.Dealer_Id__c == Dealer_Id && l.Email == Email || l.Email_Secondary__c == Email) {
                return l.Id;
            }
        }
        return null;
    }

    private static String searchAccId(List<Account> accs, String  Dealer_Id, String Email){
        for (Account a : accs) {
            if (a.Dealer_ID__pc == Dealer_Id && a.PersonEmail == Email || a.Email_Secondary__pc == Email) {
                return a.Id;
            }
        }
        return null;
    }

    private static List<String> parseEmails(String emails) {
        List<string> result = new List<string>();
            for(String e : emails.split('\\;')) {
                result.add(e.deleteWhitespace());
            }
        return result;
    }
}