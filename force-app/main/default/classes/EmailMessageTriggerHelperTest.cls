@IsTest
private class EmailMessageTriggerHelperTest {
    @TestSetup
    static void setup() {
        User u = new User(
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
                LastName = 'last',
                Email = 'test135test@test135.com',
                Username = 'test135test@test135.com',
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                Dealer_ID__c = 'CHAUD'
        );
        insert u;

        Lead lead = new Lead(
                LastName = 'TestLead',
                Dealer_ID__c = 'CHAUD',
                Email = u.Email
        );
        insert lead;

        insert new EmailMessage(
                Subject = 'Test Subject',
                FromAddress = 'test@test.test',
                //RelatedToId = opp.Id,
                ToAddress = u.Email
        );


        insert new EmailMessage(
                Subject = 'Test Subject 1',
                FromAddress = 'test@test.test',
                //RelatedToId = opp.Id,
                ToAddress = u.Email
        );


    }

    @IsTest
    static void setRelatedToIdFieldTest() {
        List<EmailMessage> emailMessages = [SELECT Id, ToAddress, RelatedToId, ToIds FROM EmailMessage WHERE Subject = 'Test Subject'];
        EmailMessageTriggerHelper.setRelatedToIdField(emailMessages);
    }

    @IsTest
    static void insertEmailMessageOpenerTest() {
        Map<Id, EmailMessage> messageMap = new Map<Id, EmailMessage>([
            SELECT Id, ToAddress, RelatedToId, ToIds
            FROM EmailMessage
            WHERE Subject = 'Test Subject'
        ]);
        EmailMessageTriggerHelper.insertEmailMessageOpener(messageMap);
    }

    @IsTest
    static void createEmailMessageRelationTest() {
        List<EmailMessage> emailMessages = [SELECT Id, ToAddress, RelatedToId, ToIds FROM EmailMessage WHERE Subject = 'Test Subject 1'];
        EmailMessageTriggerHelper.createEmailMessageRelation(emailMessages);
    }
}