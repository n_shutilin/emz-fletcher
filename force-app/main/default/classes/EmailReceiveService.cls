global class EmailReceiveService implements Messaging.InboundEmailHandler {
    public static final String DEALER_RECORD_TYPE = 'Dealer';

    public static final String AUDFJ = 'Audi Fletcher Jones';
    public static final String AUDFR = 'Audi Fremont';
    public static final String BHAUD = 'Audi Beverly Hills';
    public static final String CHAUD = 'Fletcher Jones Audi';
    public static final String CHMBN = 'Mercedes-Benz of Chicago';
    public static final String FRMBN = 'Fletcher Jones Motorcars of Fremont';
    public static final String FRPOR = 'Porsche Fremont';
    public static final String HEMBN = 'Mercedes-Benz of Henderson';
    public static final String HOMBN = 'Mercedes-Benz of Honolulu';
    public static final String LVMBN = 'Fletcher Jones Imports';
    public static final String MAMDN = 'Mercedes-Benz of Maui';
    public static final String NBMBN = 'Fletcher Jones Motorcars';
    public static final String ONMBN = 'Mercedes-Benz of Ontario';

    public static final String KEY_AUDFJ = 'Audi Fletcher Jones';
    public static final String KEY_AUDFR = 'Audi Fremont';
    public static final String KEY_BHAUD = 'Audi Beverly Hills';
    public static final String KEY_BHAUD2 = 'Audi Beverly Hills Service';
    public static final String KEY_CHAUD = 'Fletcher Jones Audi';
    public static final String KEY_CHMBN = 'Mercedes-Benz of Chicago';
    public static final String KEY_CHMBN2 = 'Fletcher Jones Mercedes Benz Chicago';
    public static final String KEY_FRMBN = 'Fletcher Jones Motorcars of Fremont';
    public static final String KEY_FRPOR = 'Porsche Fremont';
    public static final String KEY_FRPOR2 = 'Porsche of Fremont';
    public static final String KEY_HEMBN = 'Mercedes-Benz of Henderson';
    public static final String KEY_HEMBN2 = 'Fletcher Jones MB Henderson';
    public static final String KEY_HOMBN = 'Mercedes-Benz of Honolulu';
    public static final String KEY_HOMBN2 = 'Fletcher Jones Mercedes-Benz Honolulu';
    public static final String KEY_LVMBN = 'Fletcher Jones Imports';
    public static final String KEY_MAMBN = 'Mercedes-Benz of Maui';
    public static final String KEY_NBMBN = 'Fletcher Jones Motorcars';
    public static final String KEY_NBMBN2 = 'Fletcher Jones MB Newport';
    public static final String KEY_ONMBN = 'Mercedes-Benz of Ontario';

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail inboundEmail, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String myPlainText = '';
        myPlainText = inboundEmail.plainTextBody;
        Task[] newTask = new Task[0];
        
        try {
            newTask.add(new Task(Description = myPlainText,
            Priority = 'Normal',
            Status = 'Completed',
            Subject = 'ADF Email'));
            
            insert newTask;
            System.debug('New Task Object: ' + newTask );
        }
        catch (QueryException e) { 
            System.debug('Query Issue: ' + e); 
        }
        
        try {
            String leadInfo;

            if (String.isNotBlank(inboundEmail.htmlBody)) {
                leadInfo = inboundEmail.htmlBody.stripHtmlTags().trim().replaceAll('\u00A0', '');
            } else {
                leadInfo = inboundEmail.plainTextBody.trim().replaceAll('\u00A0', '');
            }

            leadInfo = leadInfo.replaceAll('<!\\[CDATA\\[', '')
                    .replaceAll('\\]\\]>', '')
                    .replaceAll('&','&amp;');

            System.debug(leadInfo);

            Lead lead = new Lead();

            Dom.Document doc = new Dom.Document();
            doc.load(leadInfo);
            Dom.XmlNode adf = doc.getRootElement();

            lead.ADF_Copy__c = leadInfo;
            lead.Service_Leads_ADF__c = True;

            Dom.XmlNode prospect = adf.getChildElement('prospect', null);

            String sources = '';
            for (Dom.XmlNode child : prospect.getChildElements()) {
                if (child.getName() == 'id') {
                    String source = child.getAttribute('source', null);

                    if (String.isNotBlank(source)) {
                        sources += 'Source Name: ' + source + ' \n';
                    }

                    if (String.isNotBlank(child.getText())) {
                        sources += 'Id: ' + child.getText() + '; \n';
                    }
                }
            }

            lead.Sequence_Sources__c = sources;

            if (prospect != null) {
                String status = prospect.getAttribute('status', null);
                lead.Prospect_Status__c = (status != null ? status : '');

                Dom.XmlNode requestDate = prospect.getChildElement('requestdate', null);
                lead.Request_Date__c = (requestDate != null ? convertStringToDateTime(requestDate.getText()) : null);

                Dom.XmlNode vehicle = prospect.getChildElement('vehicle', null);

                if (vehicle != null) {
                    Dom.XmlNode year = vehicle.getChildElement('year', null);
                    lead.Year__c = (year != null ? year.getText() : '');

                    Dom.XmlNode make = vehicle.getChildElement('make', null);
                    lead.Make__c = (make != null ? make.getText() : '');

                    Dom.XmlNode model = vehicle.getChildElement('model', null);
                    lead.Model__c = (model != null ? model.getText() : '');

                    Dom.XmlNode vin = vehicle.getChildElement('vin', null);
                    lead.VIN__c = (vin != null ? vin.getText() : '');

                    Dom.XmlNode stock = vehicle.getChildElement('stock', null);
                    lead.Stock_Number__c = (stock != null ? stock.getText() : '');

                    Dom.XmlNode trim = vehicle.getChildElement('trim', null);
                    lead.Trim__c = (trim != null ? trim.getText() : '');

                    Dom.XmlNode doors = vehicle.getChildElement('doors', null);
                    lead.Doors__c = (doors != null ? doors.getText() : '');

                    Dom.XmlNode bodyStyle = vehicle.getChildElement('bodystyle', null);
                    lead.Body_Style__c = (bodyStyle != null ? bodyStyle.getText() : '');

                    Dom.XmlNode transmission = vehicle.getChildElement('transmission', null);
                    lead.Transmission__c = (transmission != null ? transmission.getText() : '');

                    Dom.XmlNode odometer = vehicle.getChildElement('odometer', null);
                    lead.Odometer__c = (odometer != null ? odometer.getText() : '');

                    Dom.XmlNode colorCombination = vehicle.getChildElement('colorcombination', null);

                    if (colorCombination != null) {
                        Dom.XmlNode interiorColor = colorCombination.getChildElement('interiorcolor', null);
                        lead.Interior_Color__c = (interiorColor != null ? interiorColor.getText() : '');

                        Dom.XmlNode exteriorColor = colorCombination.getChildElement('exteriorcolor', null);
                        lead.Exterior_Color__c = (exteriorColor != null ? exteriorColor.getText() : '');

                        Dom.XmlNode preference = colorCombination.getChildElement('preference', null);
                        lead.Preference__c = (preference != null ? preference.getText() : '');
                    }

                    Dom.XmlNode imageTag = vehicle.getChildElement('imagetag', null);
                    lead.Image_Tag__c = (imageTag != null ? imageTag.getText() : null);

                    Dom.XmlNode price = vehicle.getChildElement('price', null);
                    lead.Price__c = ((price != null && !String.isBlank(price.getText())) ? Decimal.valueOf(price.getText()) : null);

                    Dom.XmlNode priceComments = vehicle.getChildElement('pricecomments', null);
                    lead.Price_Comments__c = (priceComments != null ? priceComments.getText().unescapeHtml4() : '');

                    Dom.XmlNode comments = vehicle.getChildElement('comments', null);
                    lead.Comments__c = (comments != null ? comments.getText().unescapeHtml4() : '');

                    Dom.XmlNode[] vehicleChilds = vehicle.getChildren();

                    String options = '';
                    for (Dom.XmlNode child : vehicleChilds) {
                        if (child.getName() == 'option') {
                            Dom.XmlNode optionName = child.getChildElement('optionname', null);
                            Dom.XmlNode weighting = child.getChildElement('weighting', null);

                            if (optionName != null) {
                                options += 'Option Name: ' + optionName.getText() + '\n';
                            }

                            if (weighting != null && String.isNotBlank(weighting.getText())) {
                                options += 'Weighting: ' + weighting.getText() + '\n';
                            }
                        }
                    }

                    lead.Options__c = options;
                }

                Dom.XmlNode customer = prospect.getChildElement('customer', null);

                if (customer != null) {
                    Dom.XmlNode contact = customer.getChildElement('contact', null);

                    if (contact != null) {
                        Dom.XmlNode[] contactChilds = contact.getChildren();
                        
                        if (contactChilds != null) {
                            Map<String,String> namesPartsMap = new Map<String,String>();
                            for (Dom.XmlNode child : contactChilds) {
                                if (child.getName() == 'name') {
                                    String partAttribute = child.getAttributeValue('part', null);
                                    if (partAttribute != null) {
                                        namesPartsMap.put(partAttribute,child.getText());
                                    }
                                }

                                if (child.getName() == 'phone') {
                                    if (child.getAttributeValue('type', null) == 'voice') {
                                        lead.Phone = child.getText();
                                    }

                                    if (child.getAttributeValue('type', null) == 'cellphone') {
                                        lead.MobilePhone = child.getText();
                                    }

                                    if (child.getAttributeValue('type', null) == 'fax') {
                                        lead.Fax = child.getText();
                                    }
                                }
                            }

                            lead.FirstName = (namesPartsMap.get('first') != null ? namesPartsMap.get('first') : '');
                            lead.LastName = (namesPartsMap.get('last') != null ? namesPartsMap.get('last') : namesPartsMap.get('full'));
                            //lead.Company = (namesPartsMap.get('full') != null ? namesPartsMap.get('full') : lead.FirstName + ' ' + lead.LastName);

                            if (String.isBlank(lead.LastName) && String.isNotBlank(lead.LastName)) {
                                lead.LastName = lead.FirstName;
                            } 
                            // else if (String.isBlank(lead.LastName) && String.isBlank(lead.LastName)) {
                            //     lead.LastName = lead.Company;
                            // }

                            // if (String.isNotBlank(lead.LastName) && String.isBlank(lead.Company)) {
                            //     lead.Company = lead.LastName;
                            // }
                        }
                        
                        Dom.XmlNode email = contact.getChildElement('email', null);
                        lead.Email = (email != null ? email.getText() : '');

                        Dom.XmlNode phone = contact.getChildElement('phone', null);
                        lead.Phone = (phone != null ? phone.getText() : '');

                        Dom.XmlNode address = contact.getChildElement('address', null);

                        if (address != null) {
                            String street1 = '';
                            String street2 = '';

                            for (Dom.XmlNode child : address.getChildren()) {
                                if (child.getName() == 'street') {
                                    if (child.getAttributeValue('line', null) == '1') {
                                        street1 = (child != null ? child.getText() : '');
                                    }

                                    if (child.getAttributeValue('line', null) == '2') {
                                        street2 = (child != null ? child.getText() : '');
                                    }
                                }
                            }

                            lead.Street = street1 + '\n' + street2;

                            Dom.XmlNode city = address.getChildElement('city', null);
                            lead.City = (city != null ? city.getText() : '');

                            Dom.XmlNode regionCode = address.getChildElement('regioncode', null);
                            lead.State = (regionCode != null ? regionCode.getText() : '');

                            Dom.XmlNode postalCode = address.getChildElement('postalcode', null);
                            lead.PostalCode = (postalCode != null ? postalCode.getText() : '');

                            Dom.XmlNode country = address.getChildElement('country', null);
                            lead.Country = (country != null ? country.getText() : '');
                        }
                    }

                    Dom.XmlNode comments = customer.getChildElement('comments', null);
                    lead.Prospect_Notes__c = (comments != null ? comments.getText() : '');

                    /*if (comments != null) {
                        Dom.XmlNode callType = comments.getChildElement('ls_call_type', null);
                        lead.Call_Type__c = (callType != null ? callType.getText() : '');
                    }*/
                }

                Dom.XmlNode vendor = prospect.getChildElement('vendor', null);

                if (vendor != null) {
                    Dom.XmlNode vendorName = vendor.getChildElement('vendorname', null);

                    if (vendorName != null) {
                        Dom.XmlNode vendorname2 = vendorName.getChildElement('vendorname', null);

                        if (vendorname2 != null) {
                            lead.Dealer_Name__c = (vendorname2 != null ? vendorname2.getText() : '');
                        } else {
                            lead.Dealer_Name__c = (vendorname != null ? vendorname.getText() : '');
                        }

                        setDealer(lead);
                    }

                    Dom.XmlNode vendorContact = vendor.getChildElement('contact', null);

                    if (vendorContact != null) {
                        Dom.XmlNode email = vendorContact.getChildElement('email', null);
                        lead.Dealer_Email__c = (email != null ? email.getText() : '');
                    }
                }

                Dom.XmlNode provider = prospect.getChildElement('provider', null);

                if (provider != null) {
                    Dom.XmlNode providerName = provider.getChildElement('name', null);
                    lead.LeadSource = (providerName != null ? providerName.getText() : '');

                    Dom.XmlNode dealerService = provider.getChildElement('service', null);
                    lead.Dealer_Service__c = (dealerService != null ? dealerService.getText() : '');
                }

                /*if (String.isNotBlank(lead.VIN__c)) {
                    setVehicle(lead);
                }*/
            }

            System.debug('lead');
            System.debug(lead);

            Database.SaveResult insertResult = Database.insert(lead, false);

            System.debug('insertResult');
            System.debug(insertResult);

            if (insertResult.isSuccess()) {
                result.success = true;

                Attachment attach = new Attachment(
                    ParentId = insertResult.getId(),
                    Name = 'Email',
                    ContentType = 'text/plain',
                    Body = Blob.valueOf(leadInfo)
                );

                insert attach;

            } else {
                System.debug(insertResult);
                result.success = false;
                result.message = 'ERROR ';
                for (Database.Error error : insertResult.getErrors()) {
                    result.message += ' ' + error.getMessage();
                }
            }
        } catch (Exception e) {
            result.success = false;
            result.message = 'ERROR ' + e.getMessage() + ' ' + e.getCause() + ' ' + e.getStackTraceString();
            System.debug(e.getMessage());
        }
        return result;
    }

    private void setDealer(Lead lead) {
        /* Map<String, Id> dealerMap = new Map<String, Id>();

        List<Account> dealerList = [
                SELECT Id, Name
                FROM Account
                WHERE RecordType.DeveloperName = :DEALER_RECORD_TYPE
        ];

        for (Account acc : dealerList) {
            dealerMap.put(acc.Name, acc.Id);
        } */

        if (lead.Dealer_Name__c.containsIgnoreCase(KEY_AUDFJ)) {
            lead.Dealer_Id__c = 'AUDFJ';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_AUDFR)) {
            lead.Dealer_Id__c = 'AUDFR';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_BHAUD) || lead.Dealer_Name__c.containsIgnoreCase(KEY_BHAUD2)) {
            lead.Dealer_Id__c = 'BHAUD';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_NBMBN) || lead.Dealer_Name__c.containsIgnoreCase(KEY_NBMBN2)) {
            lead.Dealer_Id__c = 'NBMBN';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_CHAUD)) {
            lead.Dealer_Id__c = 'CHAUD';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_HOMBN) || lead.Dealer_Name__c.containsIgnoreCase(KEY_HOMBN2)) {
            lead.Dealer_Id__c = 'HOMBN';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_CHMBN) || lead.Dealer_Name__c.containsIgnoreCase(KEY_CHMBN2)) {
            lead.Dealer_Id__c = 'CHMBN';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_HEMBN) || lead.Dealer_Name__c.containsIgnoreCase(KEY_HEMBN2)) {
            lead.Dealer_Id__c = 'HEMBN';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_FRMBN)) {
            lead.Dealer_Id__c = 'FRMBN';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_FRPOR)) {
            lead.Dealer_Id__c = 'FRPOR';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_LVMBN)) {
            lead.Dealer_Id__c = 'LVMBN';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_ONMBN)) {
            lead.Dealer_Id__c = 'ONMBN';
        } else if (lead.Dealer_Name__c.containsIgnoreCase(KEY_MAMBN)) {
            lead.Dealer_Id__c = 'MAMBN';
        }
    }

    private Datetime convertStringToDateTime(String dateTimeVal) {
        return Datetime.valueOf(dateTimeVal.replace('T', ' '));
    }

    /*private void setVehicle(Lead lead) {
        List<Vehicle__c> vehicles = new List<Vehicle__c>();
        vehicles = [
                SELECT Id, Dealer_Id__c
                FROM Vehicle__c
                WHERE VIN__c = :lead.VIN__c
        ];

        if (!vehicles.isEmpty()) {
            lead.Vehicle__c = vehicles[0].Id;

            if (lead.Dealer_Id__c == null) {
                lead.Dealer_Id__c = vehicles[0].Dealer_Id__c;
            }
        }
    }*/

}