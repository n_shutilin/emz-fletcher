public without sharing class ExistingAccountsOnLead
{ 

Public lead ld {get; set;}
public List <Account> accs {get; set;}


public string accoutcome {get;set;}

public ExistingAccountsOnLead() {

        ld = [Select Id,Dealer_ID__c,Email,Phone,Lead_Initials__c,mobilephone from lead WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    
       
        accs = [Select Id,Dealer__c,createddate,open_Opportunity_Count__c ,FJID__c,Latest_Open_Opportunitu_Created_Date__c, Name from Account 
        where Customer_Initials__c =:ld.Lead_Initials__c and Dealer_ID__pc = :ld.dealer_ID__c and Dealer_ID__pc != NULL and ((PersonEmail != NULL and PersonEmail =:ld.email) or (Phone_Pool__c != NULL and (Phone_Pool__c Like:ld.phone or Phone_Pool__c Like:ld.mobilephone)))
        order by name asc] ;
        
        if( accs.size() == 0)
        { 
        accoutcome = 'NONE FOUND';
        }
     
        
           }    
  
    }