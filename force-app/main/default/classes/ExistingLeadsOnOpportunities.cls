public without sharing class ExistingLeadsOnOpportunities
{ 

Public opportunity opp {get; set;}
public List <lead> leads {get; set;}
public string leadoutcome {get;set;}

public ExistingLeadsOnOpportunities() {

        opp = [Select Id,Dealer_ID__c,Customer_Email_Primary__c,account.Customer_Initials__c,Customer_Work_Phone_Stripped__c,Customer_Home_Phone_Stripped__c,Customer_Mobile_Phone_Stripped__c 
        from opportunity WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    
       
        leads = [Select Id,Dealer_id__c,status,createddate, Name from Lead 
        where status !='Qualified' and 
        status !='Unqualified' and
        RecordTypeId ='0123i0000001aKUAAY' and
        lead_Initials__c =:opp.account.Customer_Initials__c and 
        Dealer_id__c = :opp.dealer_ID__c and Dealer_id__c != NULL and
        ((email = :opp.Customer_Email_Primary__c and email !=null) OR
        (phone_pool__c != Null and (phone_pool__c like :opp.Customer_Home_Phone_Stripped__c or phone_pool__c like :opp.Customer_Work_Phone_Stripped__c or phone_pool__c like :opp.Customer_Mobile_Phone_Stripped__c)))
        order by name asc] ;
        
        if( leads.size() == 0)
        { 
        leadoutcome = 'NONE FOUND';
        }
     
        
           }    
  
    }