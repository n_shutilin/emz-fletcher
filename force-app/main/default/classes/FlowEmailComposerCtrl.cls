public with sharing class FlowEmailComposerCtrl {
    @AuraEnabled
    public static List<EmailTemplate> getEmailTemplates() {
        if (Schema.SObjectType.EmailTemplate.isAccessible() && Schema.SObjectType.Attachment.isAccessible()) {
            return new List<EmailTemplate>([
                SELECT Subject,Id, Name,DeveloperName, FolderId, Folder.DeveloperName, Folder.Name, (SELECT Id,Name FROM Attachments)
                FROM EmailTemplate
                WHERE TemplateType IN ('custom', 'text', 'html')
                ORDER BY FolderId, DeveloperName
            ]);
        }
        return new EmailTemplate[]{
        };
    }

    @AuraEnabled
    public static AddressFolder getToAddresses(String recordId) {
        AddressFolder toAddressFolder = new AddressFolder();
        List<Opportunity> anOppo = new List<Opportunity>();
        List<Lead> leads = new List<Lead>();
        List<Account> accounts = new List<Account>();

        if (recordId.startsWith('006')) {
            anOppo = [SELECT Customer_Email_Primary__c, Customer_Email_Secondary__c, Account.PersonEmail, Account.Email_Secondary__pc
                      FROM Opportunity 
                      WHERE Id = :recordId];
        }
        List<String> emails = new List<String>();
        for (Opportunity opp : anOppo) {
            if (String.isNotBlank(opp.Customer_Email_Primary__c)) {
                emails.add(opp.Customer_Email_Primary__c);
            }
            if (String.isNotBlank(opp.Customer_Email_Secondary__c)) {
                emails.add(opp.Customer_Email_Secondary__c);
            }
//            if (String.isNotBlank(opp.Account.PersonEmail)) {
//                emails.add(opp.Account.PersonEmail);
//            }
//            if (String.isNotBlank(opp.Account.Email_Secondary__pc)) {
//                emails.add(opp.Account.Email_Secondary__pc);
//            }
        }

        if (recordId.startsWith('00Q')) {
            leads = [
                    SELECT Id, Email, Email_Secondary__c
                    FROM Lead
                    WHERE Id = :recordId
            ];
        }
        for (Lead l : leads) {
            if (String.isNotBlank(l.Email)) {
                emails.add(l.Email);
            }
            if (String.isNotBlank(l.Email_Secondary__c)) {
                emails.add(l.Email_Secondary__c);
            }
        }

        if (recordId.startsWith('001')) {
            accounts = [
                    SELECT PersonEmail, Email_Secondary__pc, Email_Opt_Out__c
                    FROM Account
                    WHERE Id = :recordId
            ];
        }
        for (Account acc : accounts) {
            if (String.isNotBlank(acc.PersonEmail)) {
                emails.add(acc.PersonEmail);
            }
            if (String.isNotBlank(acc.Email_Secondary__pc)) {
                emails.add(acc.Email_Secondary__pc);
            }
            //comented out by Yervand 4/24/2021
            //if (String.isNotBlank(acc.Email_Opt_Out__c)) {
            //    emails.add(acc.Email_Opt_Out__c);
            //}
        }
        toAddressFolder.toAddresses = String.join(emails, ', ');

        Default_Email_Template_Folder__mdt[] defaultFolder = [
            SELECT Folder_Name__c, User__c
            FROM Default_Email_Template_Folder__mdt
            WHERE User__c = :UserInfo.getName()
            LIMIT 1
        ];
        if (!defaultFolder.isEmpty()) {
            toAddressFolder.defaultFolder = defaultFolder[0].Folder_Name__c;
        } else {
            toAddressFolder.defaultFolder = '';
        }

        return toAddressFolder;
    }

    @AuraEnabled
    public static EmailMsg getTemplateDetails(String templateId, String whoId, String whatId) {
        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(templateId, whoId, whatId, Messaging.AttachmentRetrievalOption.METADATA_ONLY);
        EmailMsg msg = new EmailMsg();
        msg.subject = email.getSubject();
        msg.body = email.getHtmlBody();

        List<Messaging.EmailFileAttachment> attachmentList = email.fileAttachments;

        List<FileAttachmentWrapper> fawList = new List<FileAttachmentWrapper>();
        for (Messaging.EmailFileAttachment efa : attachmentList) {
            FileAttachmentWrapper faw = new FileAttachmentWrapper();
            faw.attachId = efa.id;
            faw.fileName = efa.fileName;
            faw.isContentDocument = false;
            fawList.add(faw);
        }
        for (ContentDocumentLink cdl : [
            SELECT ContentDocument.Id, ContentDocument.Title, ContentDocument.FileExtension
            FROM ContentDocumentLink
            WHERE LinkedEntityId = :templateId
        ]) {
            FileAttachmentWrapper faw = new FileAttachmentWrapper();
            faw.attachId = cdl.ContentDocument.Id;
            faw.isContentDocument = true;
            faw.fileName = cdl.ContentDocument.Title + '.' + cdl.ContentDocument.FileExtension;
            fawList.add(faw);
        }
        msg.fileAttachments = fawList;

        if (String.isBlank(msg.body)) {
            msg.body = email.getPlainTextBody();
            if (String.isNotBlank(msg.body)) {
                //might be causing the extra line in templates. - Yervand.
                msg.body = msg.body.replace('\n', '<br/>');
            }
        }
        return msg;
    }

    @AuraEnabled
    public static void deleteFiles(String sdocumentId) {
        if (Schema.SObjectType.ContentDocument.isDeletable()) {
            delete [SELECT Id,Title,FileType FROM ContentDocument WHERE Id = :sdocumentId];
        }
    }

    @AuraEnabled
    public static void sendAnEmailMsg(String fromAddress, String toAddressesStr, String ccAddressesStr, String bccAddressesStr, String subject, String body, String senderDisplayName, List<String> contentDocumentIds,
        List<String> attachmentIds, String recordId) {
        try {
            if (String.isNotBlank(toAddressesStr) && Schema.SObjectType.ContentVersion.isAccessible()) {
                String[] toAddresses = toAddressesStr.split(',');
                String[] fileIds = new String[]{};
                String[] ccAddresses = String.isNotBlank(ccAddressesStr) ? ccAddressesStr.split(',') : new String[]{};
                String[] bccAddresses = String.isNotBlank(bccAddressesStr) ? bccAddressesStr.split(',') : new String[]{};
               // System.debug('contentDocumentIds****' + contentDocumentIds);
                List<String> cvIds = new String[]{
                };
                for (ContentVersion cv : [
                    SELECT Id, Title, FileType, VersionData, IsLatest, ContentDocumentId
                    FROM ContentVersion
                    WHERE IsLatest = TRUE AND ContentDocumentId IN :contentDocumentIds
                ]) {
                    cvIds.add(cv.Id);
                }
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                if (String.isNotBlank(fromAddress)) {
                    OrgWideEmailAddress[] owea = new List<OrgWideEmailAddress>([
                        SELECT Id
                        FROM OrgWideEmailAddress
                        WHERE Address = :fromAddress
                    ]);
                    if (owea.size() > 0) {
                        email.setOrgWideEmailAddressId(owea.get(0).Id);
                        email.setUseSignature(false);
                    }
                }

                email.setToAddresses(toAddresses);
                email.setCcAddresses(ccAddresses);
                email.setBccAddresses(bccAddresses);
                email.setSubject(subject);
                email.setHtmlBody(body);
                email.setUseSignature(false);

                if (recordId.startsWith('006') || recordId.startsWith('001')) {
                    email.setWhatId(recordId);
                } else if (recordId.startsWith('00Q')){
                    email.setTargetObjectId(recordId);
                }
                //email.setTreatBodiesAsTemplate(true);
                if (String.isNotBlank(senderDisplayName)) {
                    email.setSenderDisplayName(senderDisplayName);
                }
               // System.debug('cvIds**' + cvIds);
                if (cvIds != null && !(cvIds.isEmpty())) {
                    fileIds.addAll(cvIds);
                }
                if (attachmentIds != null && !(attachmentIds.isEmpty())) {
                    fileIds.addAll(attachmentIds);
                }
                if (!(fileIds.isEmpty())) {
                    email.setEntityAttachments(fileIds);
                }
                //email.setTargetObjectId(userinfo.getUserId());
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
                    email
                });
            }
        } catch (Exception e) {
            throw new AuraHandledException('Something went wrong: ' + e.getMessage());
            //throw e;
        }
    }

    public class FileAttachmentWrapper {
        @AuraEnabled public String attachId;
        @AuraEnabled public String fileName;
        @AuraEnabled public Boolean isContentDocument;
    }

    public class EmailMsg {
        @AuraEnabled public String subject;
        @AuraEnabled public String body;
        @AuraEnabled public List<FileAttachmentWrapper> fileAttachments;
        // @AuraEnabled publi List<String> attach
    }

    public class AddressFolder {
        @AuraEnabled public String toAddresses;
        @AuraEnabled public String defaultFolder;
    }
}