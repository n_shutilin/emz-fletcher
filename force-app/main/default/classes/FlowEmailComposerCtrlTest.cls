@IsTest
public class FlowEmailComposerCtrlTest {
    @TestSetup
    static void testCreateContentDocuments() {
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Test Document';
        cv.PathOnClient = 'TestDocument.pdf';
        cv.VersionData = Blob.valueOf('Test Content');
        cv.IsMajorVersion = true;
        insert cv;
        Lead lead = new Lead(
                LastName = 'TestLead',
                Dealer_ID__c = 'CHAUD',
                Email = 'test@test.com',
                Email_Secondary__c = 'test@test.com'
        );
        insert lead;
        Opportunity opp = new Opportunity(
                Name = 'TestOpp',
                StageName = 'New',
                CloseDate = Date.today(),
                Customer_Email_Primary__c = 'test@test.com',
                Customer_Email_Secondary__c = 'test@test.com'
        );
        insert opp;

        String recordTypeId = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        Account acc = new Account(
                RecordTypeId = recordTypeId,
                FirstName = 'Test FName',
                LastName = 'Test LName',
                PersonEmail = 'test@test.com',
                Email_Secondary__pc = 'test@test.com'
        );

        insert acc;
    }

    @IsTest
    static void testgetEmailTemplates() {
        EmailTemplate e = new EmailTemplate (DeveloperName = 'test', FolderId = UserInfo.getUserId(), TemplateType = 'Text', Name = 'test'); // plus any other fields that you want to set
        insert e;
        List<EmailTemplate> emailTemplateList = FlowEmailComposerCtrl.getEmailTemplates();
        System.assert(emailTemplateList.size() > 0);
        System.assert(FlowEmailComposerCtrl.getTemplateDetails([SELECT Id FROM EmailTemplate LIMIT 1].Id, null, null) != null);
    }
    @IsTest
    static void testdeleteFiles() {
        List<ContentVersion> cvList = [SELECT Id, Title, ContentDocumentId FROM ContentVersion];
        System.assertEquals(cvList.size(), 1);

        FlowEmailComposerCtrl.deleteFiles(cvList[0].ContentDocumentId);
        System.assertEquals([SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument].size(), 0);
    }
    @IsTest
    static void testSendEmail() {
        List<ContentVersion> cvList = [SELECT Id, Title, ContentDocumentId FROM ContentVersion];
        Lead lead = [SELECT Id, Email FROM Lead LIMIT 1];
        Test.startTest();

        try {
            FlowEmailComposerCtrl.sendAnEmailMsg('test@gmail.com', lead.Email, 'test@gmail.com', 'test@gmail.com', 'test',
                    'body', 'sender', new string[]{
                            cvList[0].ContentDocumentId
                    }, null, lead.Id);
        } catch (Exception e) {
            System.debug('~~~~~~~~~~~~~~~~' + e);
        }

        Test.stopTest();


    }

    @IsTest
    static void getToAddressesTest() {
        Lead lead = [SELECT Id, Email FROM Lead LIMIT 1];
        FlowEmailComposerCtrl.getToAddresses(lead.Id);
        System.assertEquals('test@test.com', lead.Email);

        Opportunity opp = [SELECT Id, Customer_Email_Primary__c FROM Opportunity LIMIT 1];
        FlowEmailComposerCtrl.getToAddresses(opp.Id);
        System.assertEquals('test@test.com', opp.Customer_Email_Primary__c);

        Account acc = [SELECT Id, PersonEmail FROM Account LIMIT 1];
        FlowEmailComposerCtrl.getToAddresses(acc.Id);
        System.assertEquals('test@test.com', acc.PersonEmail);
    }

}