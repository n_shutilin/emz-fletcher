public class FutureAppointmentsController {

    @AuraEnabled
    public static List<FutureAppointment> getAppointmentActivities(String recordId) {
        Id appointmentId = Id.valueOf(recordId);

        Schema.SObjectType sobjectType = appointmentId.getSobjectType();
        String sobjectName = sobjectType.getDescribe().getName();

        String query = 'SELECT Appointment_Type__c, StartDateTime, Owner.Name, Salesperson__r.Name, CreatedBy.Name ' +
            'FROM Event ' +
            'WHERE Show__c = FALSE AND Cancel__c = FALSE AND StartDateTime >= TODAY AND ';

        if (isAccountSobject(sobjectName)) {
            List<Account> accountList = [SELECT PersonContactId FROM Account WHERE Id = :recordId AND PersonContactId != NULL];

            if (!accountList.isEmpty()) {
                appointmentId = accountList[0].PersonContactId;
                query += 'WhoId';
            } else {
                query += 'WhatId';
            }
        } else {
            query += isPersonSobject(sobjectName) ? 'WhoId' : 'WhatId';
        }

        query += ' =\'' + appointmentId + '\'';

        List<FutureAppointment> futureAppointments = new List<FutureAppointment>();
        for (Event event : Database.query(query)) {
            futureAppointments.add(
                new FutureAppointment(
                    event.Id,
                    event.Appointment_Type__c,
                    event.StartDateTime.format(),
                    event.Owner.Name,
                    event.Salesperson__r.Name,
                    event.CreatedBy.Name
                )
            );
        }

        return futureAppointments;
    }

    @AuraEnabled
    public static void markShownAppointment(String appointmentId) {
        if (String.isBlank(appointmentId)) return;

        List<Event> appointmentList = [SELECT Id, Show__c, Appointment_Type__c, WhatId FROM Event WHERE Id = :appointmentId];

        if (appointmentList.isEmpty()) return;

        Event appointment = appointmentList[0];

        if (appointment.Show__c) return;

        Savepoint sp = Database.setSavepoint();

        try {
            appointment.Show__c = true;
            update appointment;
            createShownActivityHistory(appointment);
        } catch (Exception ex) {
            Database.rollback(sp);
            System.debug(ex.getMessage());

            throw new AuraHandledException('An error occurred during appointment update');
        }
    }

    @AuraEnabled
    public static void cancelAppointment(String appointmentId) {
        if (String.isBlank(appointmentId)) return;

        List<Event> appointmentList = [SELECT Id, Subject, Cancel__c, Appointment_Type__c, StartDateTime, WhatId FROM Event WHERE Id = :appointmentId];

        if (appointmentList.isEmpty()) return;

        Event appointment = appointmentList[0];

        if (appointment.Cancel__c) return;

        Savepoint sp = Database.setSavepoint();

        try {
            appointment.Cancel__c = true;
            update appointment;
            createCancelationActivityHistory(appointment);
            createCancelationAction(appointment.WhatId);
        } catch (Exception ex) {
            Database.rollback(sp);
            System.debug(ex.getMessage());

            throw new AuraHandledException('An error occurred during appointment update');
        }
    }

    private static void createCancelationActivityHistory(Event oldEvent) {
        Task cancelationTask = new Task();
        cancelationTask.Subject = oldEvent.Subject;
        cancelationTask.Cancel__c = true;
        cancelationTask.Status = 'Completed';
        cancelationTask.Activity_Type__c = 'Appointment';
        // cancelationTask.Activity_Type__c = oldEvent.Appointment_Type__c;
        cancelationTask.Appointment_Type__c = oldEvent.Appointment_Type__c;
        cancelationTask.WhatId = oldEvent.WhatId;
        String datetimeString = oldEvent.StartDateTime.format();
        List<String> splitedDate = datetimeString.split(' ');
        String dateString = splitedDate[0];
        String timeString = splitedDate[1] + ' ' + splitedDate[2];
        cancelationTask.Description = 'Sales Appointment Status = Cancelled Original Appointment Date = ' + dateString
            + ' Time = ' + timeString;
        insert cancelationTask;

        // publishTaskCreationEvent(oldEvent.WhatId);
    }

    private static void createShownActivityHistory(Event oldEvent) {
        Task shownTask = new Task();
        shownTask.Show__c = true;
        shownTask.Status = 'Completed';
        shownTask.Activity_Type__c = 'Appointment';
        // shownTask.Activity_Type__c = oldEvent.Appointment_Type__c;
        shownTask.Appointment_Type__c = oldEvent.Appointment_Type__c;
        shownTask.Description = 'Sales Appointment Status = Guest Showed';
        shownTask.WhatId = oldEvent.WhatId;
        insert shownTask;

        // publishTaskCreationEvent(oldEvent.WhatId);
    }

    private static void createCancelationAction(Id recordId) {
        Id groupId = [SELECT Id FROM Group WHERE Type = 'Queue'AND Name = 'BDC Sales' LIMIT 1]?.Id;
        Action__c action = new Action__c();
        action.Type__c = 'Call';
        action.Name = 'Sales Appointment Cancellation Follow-up';
        action.Status__c = 'Open';
        action.OwnerId = groupId;
        action.Due_Date__c = Datetime.now().addDays(1).date();

        String objectEntity = String.valueOf(recordId.getSobjectType());
        switch on objectEntity {
            when 'Opportunity' {
                action.Opportunity__c = recordId;
            }
            when 'Lead' {
                action.Lead__c = recordId;
            }
            when 'ServiceAppointment' {
                action.Service_Appointment__c = recordId;
            }
        }

        insert action;

        // publishActionCreationEvent(recordId);
    }

    private static void publishTaskCreationEvent(String relatedId) {
        Activity_History_Update__e ev = new Activity_History_Update__e(Related_To__c = relatedId);
        Database.SaveResult result = EventBus.publish(ev);
    }

    private static void publishActionCreationEvent(String parentRecordId) {
        Action_Event__e event = new Action_Event__e(
            Parent_Record_Id__c = parentRecordId
        );

        EventBus.publish(event);
    }

    private static Boolean isPersonSobject(String sobjectName) {
        return sobjectName == 'Contact' || sobjectName == 'Lead';
    }

    private static Boolean isAccountSobject(String sobjectName) {
        return sobjectName == 'Account';
    }

    public class FutureAppointment {
        @AuraEnabled
        public String appointmentId {get; set;}
        @AuraEnabled
        public String appointmentType {get; set;}
        @AuraEnabled
        public String appointmentDateTime {get; set;}
        @AuraEnabled
        public String salesManager {get; set;}
        @AuraEnabled
        public String salesperson  {get; set;}
        @AuraEnabled
        public String createdBy {get; set;}

        public FutureAppointment(String appointmentId, String appointmentType, String appointmentDateTime, String salesManager, String salesperson, String createdBy) {
            this.appointmentId = appointmentId;
            this.appointmentType = appointmentType;
            this.appointmentDateTime = appointmentDateTime;
            this.salesManager = salesManager;
            this.salesperson = salesperson;
            this.createdBy = createdBy;
        }
    }
}