@IsTest
public class FutureAppointmentsControllerTest {
    private final static String DEALER_ID = 'AUDFJ';

    @TestSetup
    private static void testSetup() {
        RecordType personAccountRecordType = [
            SELECT Id
            FROM RecordType
            WHERE Name = 'Person Account'
            AND SobjectType = 'Account'
        ];

        Account testAccount = new Account(
            FirstName = 'Test',
            LastName = 'Account',
            RecordTypeId = personAccountRecordType.Id,
            PersonEmail = 'testAccount@email.com'
        );
        insert testAccount;

        String personContactId = [SELECT PersonContactId FROM Account WHERE Id = :testAccount.Id].PersonContactId;

        Opportunity opportunity = new Opportunity(
            AccountId = testAccount.Id,
            Name = 'Test Opportunity',
            StageName = 'Initial Interest',
            CloseDate = Date.today().addDays(10),
            Dealer_ID__c = DEALER_ID
        );
        insert opportunity;

        User saleManager = createUser('saleManager');
        Date eventDate = Date.today().addDays(5);
        Time eventStartTime = Time.newInstance(7, 30, 0, 0);

        List<Event> events = new List<Event>{
            new Event(Subject = 'First Event', WhatId = opportunity.Id, StartDateTime = Datetime.newInstance(eventDate, eventStartTime), OwnerId = saleManager.Id, DurationInMinutes = 15),
            new Event(Subject = 'Second Event', WhoId = personContactId, StartDateTime = Datetime.newInstance(eventDate, eventStartTime), OwnerId = saleManager.Id, DurationInMinutes = 15)
        };

        insert events;
    }

    @IsTest
    public static void testGetAppointmentActivitiesByOpportunity() {
        Opportunity opportunity = [SELECT Id FROM Opportunity WHERE Dealer_ID__c = :DEALER_ID LIMIT 1];

        Test.startTest();
        List<FutureAppointmentsController.FutureAppointment> futureAppointments = FutureAppointmentsController.getAppointmentActivities(opportunity.Id);
        Test.stopTest();

        System.assertEquals(false, futureAppointments.isEmpty());
    }

    @IsTest
    public static void testGetAppointmentActivitiesByAccount() {
        Account account = [SELECT Name FROM Account WHERE PersonEmail = 'testAccount@email.com' LIMIT 1];

        Test.startTest();
        List<FutureAppointmentsController.FutureAppointment> futureAppointments = FutureAppointmentsController.getAppointmentActivities(account.Id);
        Test.stopTest();

        System.assertEquals(false, futureAppointments.isEmpty());
    }

    @IsTest
    public static void testMarkShownAppointmentAction() {
        Opportunity opportunity = [SELECT Id FROM Opportunity WHERE Dealer_ID__c = :DEALER_ID LIMIT 1];
        Event appointment = [SELECT Show__c FROM Event WHERE WhatId = :opportunity.Id LIMIT 1];

        Test.startTest();
        FutureAppointmentsController.markShownAppointment(appointment.Id);
        Test.stopTest();

        Event updatedAppointment = [SELECT Show__c FROM Event WHERE WhatId = :opportunity.Id LIMIT 1];

        System.assertEquals(true, updatedAppointment.Show__c);
    }

    @IsTest
    public static void testCancelAppointmentAction() {
        Opportunity opportunity = [SELECT Id FROM Opportunity WHERE Dealer_ID__c = :DEALER_ID LIMIT 1];
        Event appointment = [SELECT Cancel__c FROM Event WHERE WhatId = :opportunity.Id LIMIT 1];

        Test.startTest();
        FutureAppointmentsController.cancelAppointment(appointment.Id);
        Test.stopTest();

        Event updatedAppointment = [SELECT Cancel__c FROM Event WHERE WhatId = :opportunity.Id LIMIT 1];

        System.assertEquals(true, updatedAppointment.Cancel__c);
    }

    private static User createUserSalesPerson(String userName, Id managerId) {
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];

        User userOrg = new User(
            Alias = 'standt',
            Email = userName + '@test31.com',
            EmailEncodingKey = 'UTF-8',
            LastName = userName + '@test31.com',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = profile.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            Username = userName + '@test31.com',
            ManagerId = managerId
        );

        insert userOrg;
        return userOrg;
    }

    private static User createUser(String userName) {
        return createUserSalesPerson(userName, null);
    }

}