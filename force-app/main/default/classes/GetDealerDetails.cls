public class GetDealerDetails {  
    private final Due_Bill__c DueBill ;
    private List<Account> DealerAccount;
    public string DealerStreet {get;set;}
    public string DealerCity {get;set;}
    public string DealerState {get;set;}
    public string DealerPostalCode {get;set;}
    public string DealerCountry {get;set;}
    public string DealerPhone {get;set;}
    public string DealerName {get;set;}
        
    public GetDealerDetails (ApexPages.StandardController stdController)  {   
        this.DueBill = (Due_Bill__c)stdController.getRecord();                        
        
        Id DealerRecordType = Schema.SObjectType.Account.RecordTypeInfosByName.get('Dealer').RecordTypeId;
        DealerAccount = [Select Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Phone from Account where Dealer_Code__c = :Duebill.Dealer_Id__c and RecordTypeId = :DealerRecordType limit 1];
        
        if(DealerAccount.size() == 1){
            for(Account dl: DealerAccount){
                DealerStreet = DealerAccount[0].BillingStreet;
                DealerCity = DealerAccount[0].BillingCity;
                DealerState = DealerAccount[0].BillingState;
                DealerPostalCode = DealerAccount[0].BillingPostalCode;
                DealerCountry = DealerAccount[0].BillingCountry;
                DealerPhone = DealerAccount[0].Phone;
                DealerName = DealerAccount[0].Name;
            }
        }
        
}
    
}