public with sharing class GetMessageBoardPosts {
    @AuraEnabled(Cacheable = true)
    public static List<Message_Board_Post__c> getRecords() {
        Date today = system.today();
        List<Message_Board_Post__c> MessageBoardPosts = [SELECT Id, Body__c, CreatedById, Start_Date__c, End_Date__c
        FROM Message_Board_Post__c
        WHERE End_Date__c >= :today
        ORDER BY Start_Date__c];
        return MessageBoardPosts;
    }
}