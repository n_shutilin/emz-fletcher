public with sharing class GetUsedVehcileManagerNotes {
    @AuraEnabled(Cacheable = true)
    public static List<FJ_Note__c> getRecords(String recordId, String ObjectApiName) {
        string relatedId = recordId;
        List<FJ_Note__c> UsedVehcileManagerNotes = [SELECT Name, Id, Body__c, CreatedById, CreatedDate, Created_By_Name__c
        FROM FJ_Note__c
        WHERE Related_Record_Id__c = :relatedId ORDER BY CreatedDate DESC];
        return UsedVehcileManagerNotes;
    }
}