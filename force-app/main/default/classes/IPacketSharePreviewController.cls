public without sharing class IPacketSharePreviewController {

    @AuraEnabled
    public static IPacketParametersWrapper getVehicleVIN(String recordId){
        String recordObjectName = Id.valueOf(recordId).getSObjectType().getDescribe().getName();

        String query = 'SELECT Vehicle__c, iPacket_Shared_Link__c FROM ' + recordObjectName + ' WHERE Id =: recordId LIMIT 1';
        SObject record = Database.query(query);

        String vehicleId = String.valueOf(record.get('Vehicle__c'));
        String vinCode = null;
        if(String.isNotBlank(vehicleId)) {
            vinCode = [SELECT VIN__c FROM Vehicle__c WHERE Id = :vehicleId LIMIT 1]?.VIN__c;
        } else {
            System.debug('No Vehicle found on record');
            // throw prepareError('No Vehicle found on record');
        }

        if(String.isBlank(vinCode)) {
            System.debug('There is neither VIN on related Vehicle');
            // throw prepareError('There is neither VIN on related Vehicle');
        }
        
        String sharedLink = String.valueOf(record.get('iPacket_Shared_Link__c'));

        return new IPacketParametersWrapper(sharedLink, vinCode, vehicleId);
    }

    @AuraEnabled
    public static string packetSharePreview(String vinCode, String recordId) {
        IPacketAuthData authData = formAuthData();
        HttpRequest request = new HttpRequest();
		request.setEndpoint(authData.getUri() + 'v1/3pt/packets/' + authData.getStoreAccessKey() + '/' + vinCode + '/preview');
		request.setMethod('GET');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Authorization','Token ' + authData.getApiToken());
		Http httpHandler = new Http();
        HTTPResponse response = httpHandler.send(request);
                
        String responseString;
        if(response.getStatus() == 'OK' && response.getStatusCode() == 200) {
            IPacketResponseWrapper responseWrapper = (IPacketResponseWrapper) JSON.deserialize(response.getBody(), IPacketResponseWrapper.class);
            if(String.isNotBlank(responseWrapper.packet_share_preview_url)) {
                responseString = responseWrapper.packet_share_preview_url;
                updateRecord(recordId, responseString);
            } else {
                throw prepareError('There is no Vehicle with such VIN');
            }
        } else if (response.getStatusCode() == 404) {
            throw prepareError('Vehicle not found');
        } else {
            throw prepareError('Bad Response');
        }

        return responseString;
    }

    private static AuraHandledException prepareError(String message){
        AuraHandledException auraEx = new AuraHandledException(message);
        auraEx.setMessage(message);
        return auraEx;
    }

    private static IPacketAuthData formAuthData(){
        IPacket_Integration_Settings__c autnData = IPacket_Integration_Settings__c.getOrgDefaults();
        return new IPacketAuthData(autnData.Api_Token__c, autnData.Uri__c, autnData.Store_Access_Key__c);
    }

    private static void updateRecord(String recordId, String link) {
        Id objId = recordId;
        SObject record = objId.getSObjectType().newSObject(objId);
        record.put('Id', objId);
        record.put('iPacket_Shared_Link__c', link);
        update record;
    }

    private class IPacketAuthData {
        private String apiToken;
        private String uri;
        private String storeAccessKey;

        IPacketAuthData(String apiToken, String uri, String storeAccessKey){
            this.apiToken = apiToken;
            this.uri = uri;
            this.storeAccessKey = storeAccessKey;
        }

        public String getUri(){
            return this.uri;
        }

        public String getApiToken(){
            return this.apiToken;
        }

        public String getStoreAccessKey(){
            return this.storeAccessKey;
        }
    }

    public class IPacketParametersWrapper {
        @AuraEnabled public String link { get; set; }
        @AuraEnabled public String vin { get; set; }
        @AuraEnabled public String vehicle { get; set; }

        public IPacketParametersWrapper() {
        
        }

        public IPacketParametersWrapper(String link, String vin, String vehicle) {
            this.link = link;
            this.vin = vin;
            this.vehicle = vehicle;
        }
    }

    public class IPacketResponseWrapper {
        @AuraEnabled public String packet_share_preview_url { get; set; }
    }
}