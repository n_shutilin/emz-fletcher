@isTest
private class ImportFlowLauncher_BatchableTest
{
    private static testMethod void myTestMethod()
    {
        NBMBN_IMPORT__c rep = new NBMBN_IMPORT__c( Auto_Trigger__c = False, storeId__c='FRMBN');
        insert rep;
        
         Test.startTest();
        
            String Query = 'Select imp.Id, imp.Auto_Trigger__c '
                 +   'From NBMBN_IMPORT__c imp where Auto_Trigger__c=False and StoreId__c != NULL and Account__c = NULL and CreatedDate=Today';
        
        ImportFlowLauncher_Batchable ClassObj = new ImportFlowLauncher_Batchable(Query);
        database.executeBatch(ClassObj, 1);
        Test.stopTest();}
        
            private static testMethod void myRepOrderFlowLauncher_BatchableTest()
    {
        Test.startTest();
            Datetime dt = Datetime.now().addMinutes(1);
            String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
            ImportFlowLauncher_Schedulable ClassObj = new ImportFlowLauncher_Schedulable();
            system.schedule('Test Method', CRON_EXP, ClassObj);   
        Test.stopTest();
    }
        
             

    }