global class ImportFlowLauncher_Schedulable implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
            String Query = 'Select imp.Id, imp.Auto_Trigger__c '
                 +  'From NBMBN_IMPORT__c imp where Auto_Trigger__c=False and StoreId__c != NULL and Account__c = NULL and CreatedDate=Today';
        
        ImportFlowLauncher_Batchable ClassObj = new ImportFlowLauncher_Batchable(Query);
        database.executeBatch(ClassObj, 1);
    }
    
    }