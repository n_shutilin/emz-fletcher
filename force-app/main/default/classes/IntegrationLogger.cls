public with sharing class IntegrationLogger {

    private static List<Integration_Logs__c> logs;
    private static List<Attachment> logFiles;
    private static Map<Integer, List<Integer>> relations;

    static {
        initialize();
    }

    public static void addLog(Integration_Logs__c log, List<Attachment> files) {
        System.debug('~~~~~~~~~~~~ ' + log);
        logs.add(log);
        Integer currentLogIndex = logs.size() - 1;
        Integer currentLogFilesSize = logFiles.size();
        relations.put(currentLogIndex, new List<Integer>());
        for (Integer i = 0; i < files.size(); i++) {
            relations.get(currentLogIndex).add(currentLogFilesSize + i);
        }
        logFiles.addAll(files);
    }

    public static void insertLogs() {
        try {
            System.debug(Log_Configuration__c.getOrgDefaults().Enable_Logs__c);
            if (Log_Configuration__c.getOrgDefaults().Enable_Logs__c) {
                System.debug('~~~~~~~~~ ' + logs);
                insert logs;
                populateIds(); 
                System.debug('log inserted');
                if (Log_Configuration__c.getOrgDefaults().Enable_Attachements__c) {
                    insert logFiles;
                    initialize();
                }
            }
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }

    private static void populateIds() {
        for (Integer logIndex : relations.keySet()) {
            for (Integer fileIndex : relations.get(logIndex)) {
                logFiles[fileIndex].ParentId = logs[logIndex].Id;
            }
        }
    }

    private static void initialize() {
        logs = new List<Integration_Logs__c>();
        logFiles = new List<Attachment>();
        relations = new Map<Integer, List<Integer>>();
    }
}