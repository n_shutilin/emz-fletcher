public with sharing class InventorySearchController {

    @AuraEnabled
    public static String getSearchParams() {
        String searchParams = '';
        try {
            searchParams = JSON.serialize(InventoryService.getSearchParams());
        } catch (Exception ex) {
            throw new AuraHandledException('An error occurred during model list preparation');
        }
        return searchParams;
    }

    @AuraEnabled
    public static List<InventoryService.SelectOptionDto> getModelOptionsByMake(Id makeId) {
        List<InventoryService.SelectOptionDto> selectOptions = new List<InventoryService.SelectOptionDto>();
        try {
            selectOptions = InventoryService.buildModelSelectOptions(makeId);
        } catch (Exception ex) {
            throw new AuraHandledException('An error occurred during model list preparation');
        }
        return selectOptions;
    }

    @AuraEnabled
    public static String search(String searchParamsJson, String recordId) {
        InventoryService.SearchParams searchParams = (InventoryService.SearchParams) JSON.deserialize(searchParamsJson, InventoryService.SearchParams.class);
        String searchResults = '';
//        try {
            searchResults = JSON.serialize(InventoryService.search(searchParams, recordId));
//        } catch (Exception ex) {
//            throw new AuraHandledException('An error occurred during search');
//        }
        return searchResults;
    }
}