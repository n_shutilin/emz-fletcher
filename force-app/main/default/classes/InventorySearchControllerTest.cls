@IsTest
public with sharing class InventorySearchControllerTest {

    @TestSetup
    static void init(){
        Opportunity testOpportunity = new Opportunity(
                Name = 'Test',
                StageName = 'Test',
                CloseDate = Date.newInstance(2021, 12, 12),
                Dealer_ID__c = 'CHAUD'
        );
        insert testOpportunity;
        Lead testLead = new Lead(
                Dealer_ID__c = 'CHAUD',
                LastName = 'Test'
        );
        insert  testLead;
        Vehicle_Make__c testMake = new Vehicle_Make__c(
            Make__c = 'Audi'
        );
        insert testMake;

        Vehicle_Model__c testModel = new Vehicle_Model__c(
            Model__c = 'TEST',
            Vehicle_Make__c = testMake.Id
        ); 
        insert testModel;
        Vehicle__c testVehicle = new Vehicle__c(
                Exterior_Color__c = 'Black',
                Year__c = '2021',
                Make__c = 'Audi',
                Interior_Color__c = 'Black',
                Body__c = 'Sedan',
                Trim__c = 'Test',
                Model__c = 'TEST'
        );
        insert  testVehicle;
        Inventory__c testInventory = new Inventory__c(
                Vehicle__c = testVehicle.Id,
                TypeNUName__c = 'Test',
                Stock_Number__c = '7488329'
        );
        insert testInventory;
    }

    @IsTest
    static void getSearchParamsTest(){
        String actual = InventorySearchController.getSearchParams();
    }

    @IsTest
    static void getModelOptionsByMakeTest(){
        Vehicle_Make__c testMake = [SELECT Id FROM Vehicle_Make__c LIMIT 1];
        List<InventoryService.SelectOptionDto> actual = InventorySearchController.getModelOptionsByMake(testMake.Id);
        List<InventoryService.SelectOptionDto> expected = new List<InventoryService.SelectOptionDto>{
            new InventoryService.SelectOptionDto('TEST', 'TEST')
        };
        System.assert(actual[0].label == expected[0].label && actual[0].value == expected[0].value);
    }

    @IsTest
    static void search() {
        Opportunity testOpportunity = [
                SELECT Dealer_ID__c
                FROM Opportunity
                LIMIT 1
        ];
        Lead testLead = [
                SELECT Dealer_ID__c
                FROM Lead
                LIMIT 1
        ];
        Inventory__c testInventory = [
                SELECT Vehicle__r.Exterior_Color__c, Vehicle__r.Interior_Color__c, Vehicle__r.Body__c, Vehicle__r.Trim__c, TypeNUName__c, Vehicle__r.Year__c, Vehicle__r.Make__c , Vehicle__r.Model__c, Stock_Number__c
                FROM Inventory__c
                LIMIT 1
        ];
        InventoryService.SearchParams searchParams = InventoryService.getSearchParams();
        searchParams.trim = testInventory.Vehicle__r.Trim__c;
        searchParams.interiorColor = testInventory.Vehicle__r.Interior_Color__c;
        searchParams.selectedType = testInventory.TypeNUName__c;
        searchParams.exteriorColor1 = testInventory.Vehicle__r.Exterior_Color__c;
        searchParams.exteriorColor2 = 'Red';
        searchParams.selectedBodyType = testInventory.Vehicle__r.Body__c;
        searchParams.selectedYearTo = '2021';
        searchParams.selectedYear = '2018';
        InventorySearchController.search(JSON.serialize(searchParams), testOpportunity.Id);
        InventorySearchController.search(JSON.serialize(searchParams), testLead.Id);
    }

}