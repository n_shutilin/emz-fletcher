public with sharing class InventoryService {

    private static final String OPPORTUNITY_PREFIX = '006';
    private static final String LEAD_PREFIX = '00Q';

    public static List<SelectOptionDto> buildMakeSelectOptions() {
        List<SelectOptionDto> makeSelectOptions = new List<SelectOptionDto>();
        for (Vehicle_Make__c make : selectAllMakes()) {
            makeSelectOptions.add(new SelectOptionDto(make.Id, make.Make__c));
        }
        return makeSelectOptions;
    }

    public static  List<SelectOptionDto> getTypeOptions() {
        return new List<SelectOptionDto>{
            new SelectOptionDto('', 'None'),
            new SelectOptionDto('NEW', 'New'),
            new SelectOptionDto('USED', 'Pre-Owned')
        };
    }

    public static List<SelectOptionDto> getBodyTypes() {
        return getSelectItems('Vehicle__c', 'Body__c');
    }

    public static List<SelectOptionDto> buildModelSelectOptions(Id makeId) {
        List<Vehicle_Model__c> models = selectModels(makeId);
        List<SelectOptionDto> modelSelectOptions = new List<SelectOptionDto>();
        for (Vehicle_Model__c model : models) {
            modelSelectOptions.add(new SelectOptionDto(model.Model__c, model.Model__c));
        }
        return modelSelectOptions;
    }

    public class SelectOptionDto {
        @AuraEnabled
        public String value;
        @AuraEnabled
        public String label;

        public SelectOptionDto(String name, String value) {
            this.value = name;
            this.label = value;
        }
    }

    @AuraEnabled
    public static void updateInventoryInfo(String recordId, Inventory inventory) {
        SObject leadOrOpp;
        if (recordId.startsWith(OPPORTUNITY_PREFIX)) {
            leadOrOpp = new Opportunity(Id = recordId);
        } else if (recordId.startsWith(LEAD_PREFIX)) {
            leadOrOpp = new Lead(Id = recordId);
        }
        setIfNotBlank(leadOrOpp, 'year__c', inventory.year);
        setIfNotBlank(leadOrOpp, 'make__c', inventory.make);
        setIfNotBlank(leadOrOpp, 'model__c', inventory.model);
        setIfNotBlank(leadOrOpp, 'interior_color__c', inventory.interiorColor);
        setIfNotBlank(leadOrOpp, 'exterior_color__c', inventory.exteriorColor);
        leadOrOpp.put('inventory__c', null);
        leadOrOpp.put('vehicle__c', null);
        update leadOrOpp;
    }

    public static void updateInventoryInfo(Map<Id, Id> inventoryToObjIds) {
        Map<Id, Inventory__c> idToInventory = new Map<Id, Inventory__c>(getInventories(inventoryToObjIds.keySet()));
        List<SObject> recordToUpdate = new List<SObject>();
        String objId;
        for (String inventoryId : inventoryToObjIds.keySet()) {
            objId = inventoryToObjIds.get(inventoryId);
            if (objId.startsWith(OPPORTUNITY_PREFIX)) {
                Opportunity opportunity = new Opportunity(Id = objId);
                populateInventoryInfo(opportunity, idToInventory.get(inventoryId));
                recordToUpdate.add(opportunity);
            } else if (objId.startsWith(LEAD_PREFIX)) {
                Lead lead = new Lead(Id = objId);
                populateLeadInventoryInfo(lead, idToInventory.get(inventoryId));
                recordToUpdate.add(lead);
            }
        }
        update recordToUpdate;
    }

    public static List<InventoryService.SelectOptionDto> getYearOptions(Integer maxYear, Integer numberOfYears) {
        List<InventoryService.SelectOptionDto> selectOptions = new List<InventoryService.SelectOptionDto>();
        Integer currentYear = maxYear;
        String year = '';
        for(Integer i = 0; i < numberOfYears; i++) {
            year = String.valueOf(currentYear - i);
            selectOptions.add(new InventoryService.SelectOptionDto(year, year));
        }
        return selectOptions;
    }

    private static void populateLeadInventoryInfo(SObject obj, Inventory__c inventory) {
        Inventory__c inv = [SELECT Id, Vehicle__c FROM Inventory__c WHERE Id = :inventory.Id];
        setIfNotBlank(obj, 'year__c', inventory.Vehicle__r.Year__c);
        setIfNotBlank(obj, 'make__c', inventory.Vehicle__r.Make__c);
        setIfNotBlank(obj, 'model__c', inventory.Vehicle__r.Model__c);
        //setIfNotBlank(obj, 'inventory__c', inventory.Id);
        setIfNotBlank(obj, 'vehicle__c', inv.Vehicle__c);
    }

    private static void populateInventoryInfo(SObject obj, Inventory__c inventory) {
        Inventory__c inv = [SELECT Id, Vehicle__c FROM Inventory__c WHERE Id = :inventory.Id];
        setIfNotBlank(obj, 'year__c', inventory.Vehicle__r.Year__c);
        setIfNotBlank(obj, 'make__c', inventory.Vehicle__r.Make__c);
        setIfNotBlank(obj, 'model__c', inventory.Vehicle__r.Model__c);
        setIfNotBlank(obj, 'inventory__c', inventory.Id);
        setIfNotBlank(obj, 'vehicle__c', inv.Vehicle__c);
    }

    private static List<Inventory__c> getInventories(Set<Id> inventoryIds) {
        return [
            SELECT Id, Vehicle__r.Model__c, Vehicle__r.Make__c, Vehicle__r.Year__c
            FROM Inventory__c
            WHERE Id IN :inventoryIds
            ORDER BY CreatedDate DESC
        ];
    }

    private static void setIfNotBlank(SObject obj, String fieldName, String value) {
        if (String.isNotBlank(value)) {
            obj.put(fieldName, value);
        }
    }

    private static List<InventoryService.SelectOptionDto> getSelectItems(String objectName, String fieldName) {
        SObjectType sobjectValue = Schema.getGlobalDescribe().get(objectName);
        Map<String, Schema.SObjectField> fieldMap = sobjectValue.getDescribe().fields.getMap();
        Schema.DescribeFieldResult fieldResult = fieldMap.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();

        List<InventoryService.SelectOptionDto> selectItems = new List<InventoryService.SelectOptionDto>();
        for( Schema.PicklistEntry picklistEntry : picklistEntries){
            selectItems.add(new InventoryService.SelectOptionDto(picklistEntry.getValue(), picklistEntry.getLabel()));
        }
        return selectItems;
    }

    public static List<String> getInventoryFields() {
        List<String> fields = new List<String>();
        Map<String, SObjectField> fieldMap = Schema.SObjectType.Inventory__c.fields.getMap();
        for (String key : fieldMap.keySet()) {
            fields.add(fieldMap.get(key).getDescribe().name);
        }
        return fields;
    }

    public static SearchParams getSearchParams() {
        SearchParams searchParams = new SearchParams();
        searchParams.yearOptions = InventoryService.getYearOptions(Datetime.now().year() + 1, 20);
        searchParams.makeOptions = InventoryService.buildMakeSelectOptions();
        searchParams.typeOptions = InventoryService.getTypeOptions();
        searchParams.bodyOptions = InventoryService.getBodyTypes();
        return searchParams;
    }

    public static SearchResult search(SearchParams searchParams, String recordId) {
        SObject obj;
        if (recordId.startsWith(OPPORTUNITY_PREFIX)) {
            obj = [
                SELECT Id, Dealer_ID__c
                FROM Opportunity
                WHERE Id =:recordId
            ];
        } else if (recordId.startsWith(LEAD_PREFIX)) {
            obj = [
                SELECT Id, Dealer_ID__c
                FROM Lead
                WHERE Id =:recordId
            ];
        }
        String dealer = String.valueOf(obj.get('Dealer_ID__c'));
        InventoryQueryBuilder builder = new InventoryQueryBuilder();
        Integer totalNumberOfRecords = Database.countQuery(builder.buildCountQuery(searchParams,dealer));
        List<Inventory> invenotryList =  buildInventoryDtos(Database.query(builder.buildSearchQuery(searchParams, dealer)));
        return new SearchResult(totalNumberOfRecords, invenotryList);
    }

    private static List<Vehicle_Make__c> selectAllMakes() {
        return [SELECT Id, Make__c FROM Vehicle_Make__c];
    }

    private static List<Vehicle_Model__c> selectModels(Id id) {
        return [SELECT Id, Model__c FROM Vehicle_Model__c WHERE Vehicle_Make__c = :id];
    }

    private static List<Inventory> buildInventoryDtos( List<Inventory__c> inventories) {
        List<Inventory> inventoryDtos = new List<Inventory>();
        for (Inventory__c inventory : inventories) {
            Inventory inventoryDto = new Inventory();
            inventoryDto.inventoryId = inventory.Id;
            inventoryDto.stockNumber = inventory.Stock_Number__c;
            inventoryDto.model = inventory.Vehicle__r.Model__c;
            inventoryDto.make = inventory.Vehicle__r.Make__c;
            inventoryDto.year = inventory.Vehicle__r.Year__c;
            inventoryDto.type = inventory.TypeNUName__c == 'NEW' ? 'New' : 'Pre-Owned';
            inventoryDto.type = String.isBlank(inventory.TypeNUName__c) ? '' : inventoryDto.type;
            inventoryDto.trim = inventory.Vehicle__r.Trim__c;
            inventoryDto.body = inventory.Vehicle__r.Body__c;
            inventoryDto.drive = inventory.WheelDrive__c;
            inventoryDto.color = getColor(inventory.Vehicle__r.Exterior_Color__c) + ' / ' + getColor(inventory.Vehicle__r.Interior_Color__c);
            inventoryDto.vin = inventory.Vehicle__r.VIN__c;
            inventoryDto.mileage = inventory.Vehicle__r.Mileage__c;
            inventoryDto.price = inventory.Price__c;
            inventoryDto.daysInStock = inventory.Age_in_Inventory_Days__c;
            inventoryDto.status = inventory.StatusName__c;
            inventoryDtos.add(inventoryDto);
        }
        return inventoryDtos;
    }

    private static String getColor(String color) {
        return String.isNotBlank(color) ? color : 'NA';
    }

    public class InventoryQueryBuilder {

        public String buildCountQuery(SearchParams searchParams, String delaer){
            String query = 'SELECT COUNT() FROM Inventory__c WHERE Dealer__c =' + '\'' + delaer + '\' ';

            if (String.isNotBlank(searchParams.stockNumber)) {
                query += 'AND Stock_Number__c = \'' + searchParams.stockNumber + '\'';
                return query;
            }

            Map<Id, Vehicle_Make__c> vehicleMakeMap = new Map<Id, Vehicle_Make__c>(InventoryService.selectAllMakes());
            if (String.isNotBlank(searchParams.selectedMake) && String.isNotBlank(searchParams.selectedModel)) {
                query += prepareCondition(query, '(Vehicle__r.Make__c = \'' + vehicleMakeMap.get(searchParams.selectedMake).Make__c + '\'' + ' AND Vehicle__r.Model__c = \'' + searchParams.selectedModel + '\')');
            } else if (String.isNotBlank(searchParams.selectedMake)) {
                query += prepareCondition(query, 'Vehicle__r.Make__c = \'' + vehicleMakeMap.get(searchParams.selectedMake).Make__c + '\'');
            } else {
                String condition = 'Vehicle__r.Make__c IN (';
                for (Vehicle_Make__c make : vehicleMakeMap.values()) {
                    condition += '\'' + make.Make__c + '\',';
                }
                condition = condition.removeEnd(',');
                condition += ')';
                query += prepareCondition(query, condition);
            }

            List<String> exteriorColors = new List<String>();
            addIfNotBlank(exteriorColors, searchParams.exteriorColor1);
            addIfNotBlank(exteriorColors, searchParams.exteriorColor2);
            addIfNotBlank(exteriorColors, searchParams.exteriorColor3);
            if (exteriorColors.size() == 1) {
                query += prepareCondition(query, 'Vehicle__r.Exterior_Color__c = \'' + exteriorColors.get(0) + '\'');
            } else if (!exteriorColors.isEmpty()) {
                String condition = '(';
                for (String color : exteriorColors) {
                    condition += 'Vehicle__r.Exterior_Color__c = \'' + color + '\' OR ';
                }
                condition = condition.trim().removeEnd('OR') + ')';
                query += prepareCondition(query, condition);
            }

            if (String.isNotBlank(searchParams.interiorColor)) {
                query += prepareCondition(query, 'Vehicle__r.Interior_Color__c = \'' + searchParams.interiorColor + '\'');
            }

            if (String.isNotBlank(searchParams.selectedBodyType)) {
                query += prepareCondition(query, 'Vehicle__r.Body__c = \'' + searchParams.selectedBodyType + '\'');
            }

            if (String.isNotBlank(searchParams.trim)) {
                query += prepareCondition(query, 'Vehicle__r.Trim__c = \'' +searchParams.trim + '\'');
            }

            if (String.isNotBlank(searchParams.selectedType)) {
                query += prepareCondition(query, 'TypeNUName__c = \'' +searchParams.selectedType + '\'');
            }

            if (String.isNotBlank(searchParams.selectedYear) && String.isNotBlank(searchParams.selectedYearTo)) {
                String condition = ' Vehicle__r.Year__c IN (';
                for (Integer i = Integer.valueOf(searchParams.selectedYear); i <=  Integer.valueOf(searchParams.selectedYearTo); i++) {
                    condition += '\'' + i + '\',';
                }
                condition = condition.removeEnd(',');
                condition += ')';
                query += prepareCondition(query, condition);
            }
            System.debug('~ ' + query);

            return query;
        }

        public String buildSearchQuery(SearchParams searchParams, String delaer) {
            String query = 'SELECT Id, Vehicle__r.Year__c, Vehicle__r.Make__c, Vehicle__r.Model__c, Vehicle__r.Body__c, WheelDrive__c, Vehicle__r.Mileage__c, Price__c, Age_in_Inventory_Days__c, StatusName__c, Vehicle__r.Exterior_Color__c, Vehicle__r.Interior_Color__c,' +
                'Stock_Number__c, Vehicle__r.Trim__c, TypeNUName__c, ModelYear__c, Vehicle__r.VIN__c FROM Inventory__c WHERE Dealer__c =' + '\'' + delaer + '\' ';

            if (String.isNotBlank(searchParams.stockNumber)) {
                query += 'AND Stock_Number__c = \'' + searchParams.stockNumber + '\'';
                return addLimit(query, 200);
            }

            Map<Id, Vehicle_Make__c> vehicleMakeMap = new Map<Id, Vehicle_Make__c>(InventoryService.selectAllMakes());
            if (String.isNotBlank(searchParams.selectedMake) && String.isNotBlank(searchParams.selectedModel)) {
                query += prepareCondition(query, '(Vehicle__r.Make__c = \'' + vehicleMakeMap.get(searchParams.selectedMake).Make__c + '\'' + ' AND Vehicle__r.Model__c = \'' + searchParams.selectedModel + '\')');
            } else if (String.isNotBlank(searchParams.selectedMake)) {
                query += prepareCondition(query, 'Vehicle__r.Make__c = \'' + vehicleMakeMap.get(searchParams.selectedMake).Make__c + '\'');
            } else {
                String condition = 'Vehicle__r.Make__c IN (';
                for (Vehicle_Make__c make : vehicleMakeMap.values()) {
                    condition += '\'' + make.Make__c + '\',';
                }
                condition = condition.removeEnd(',');
                condition += ')';
                query += prepareCondition(query, condition);
            }

            List<String> exteriorColors = new List<String>();
            addIfNotBlank(exteriorColors, searchParams.exteriorColor1);
            addIfNotBlank(exteriorColors, searchParams.exteriorColor2);
            addIfNotBlank(exteriorColors, searchParams.exteriorColor3);
            if (exteriorColors.size() == 1) {
                query += prepareCondition(query, 'Vehicle__r.Exterior_Color__c = \'' + exteriorColors.get(0) + '\'');
            } else if (!exteriorColors.isEmpty()) {
                String condition = '(';
                for (String color : exteriorColors) {
                    condition += 'Vehicle__r.Exterior_Color__c = \'' + color + '\' OR ';
                }
                condition = condition.trim().removeEnd('OR') + ')';
                query += prepareCondition(query, condition);
            }

            if (String.isNotBlank(searchParams.interiorColor)) {
                query += prepareCondition(query, 'Vehicle__r.Interior_Color__c = \'' + searchParams.interiorColor + '\'');
            }

            if (String.isNotBlank(searchParams.selectedBodyType)) {
                query += prepareCondition(query, 'Vehicle__r.Body__c = \'' + searchParams.selectedBodyType + '\'');
            }

            if (String.isNotBlank(searchParams.trim)) {
                query += prepareCondition(query, 'Vehicle__r.Trim__c = \'' +searchParams.trim + '\'');
            }

            if (String.isNotBlank(searchParams.selectedType)) {
                query += prepareCondition(query, 'TypeNUName__c = \'' +searchParams.selectedType + '\'');
            }

            if (String.isNotBlank(searchParams.selectedYear) && String.isNotBlank(searchParams.selectedYearTo)) {
                String condition = ' Vehicle__r.Year__c IN (';
                for (Integer i = Integer.valueOf(searchParams.selectedYear); i <=  Integer.valueOf(searchParams.selectedYearTo); i++) {
                    condition += '\'' + i + '\',';
                }
                condition = condition.removeEnd(',');
                condition += ')';
                query += prepareCondition(query, condition);
            }
            System.debug('~ ' + query);

            return addLimit(query, 100);
        }

        private void addIfNotBlank(List<String> values, String value) {
            if (String.isNotBlank(value)) {
                values.add(value);
            }
        }

        private String prepareCondition(String query, String condition) {
            if (query.trim().endsWith('WHERE')) {
                return condition;
            } else {
                return ' AND ' + condition;
            }
        }

        private String addLimit(String query, Integer size) {
            if (query.trim().endsWith('WHERE')) {
                return query.trim().removeEnd('WHERE') + ' LIMIT ' + size;
            } else {
                return query + ' LIMIT ' + size;
            }
        }
    }

    public class Inventory {
        public String inventoryId;
        public String make;
        public String model;
        public String year;
        public String type;
        public String stockNumber;
        public String trim;
        public String body;
        public String drive;
        public String color;
        public String vin;
        public Decimal mileage;
        public Decimal price;
        public Decimal daysInStock;
        public String status;
        public String interiorColor;
        public String exteriorColor;
    }

    public class SearchResult {
        public List<Inventory> invenotyList;
        public Integer numberOfRecords;

        public SearchResult(Integer numberOfRecords, List<Inventory> invenotyList) {
            this.numberOfRecords = numberOfRecords;
            this.invenotyList = invenotyList;
        }
    }

    public class SearchParams {
        public String selectedType;
        public String selectedYear;
        public String selectedYearTo;
        public String selectedMake;
        public String selectedModel;
        public String trim;
        public String selectedBodyType;
        public String exteriorColor1;
        public String exteriorColor2;
        public String exteriorColor3;
        public String interiorColor;
        public String stockNumber;
        public List<InventoryService.SelectOptionDto> typeOptions;
        public List<InventoryService.SelectOptionDto> yearOptions;
        public List<InventoryService.SelectOptionDto> makeOptions;
        public List<InventoryService.SelectOptionDto> modelOptions;
        public List<InventoryService.SelectOptionDto> bodyOptions;
    }
}