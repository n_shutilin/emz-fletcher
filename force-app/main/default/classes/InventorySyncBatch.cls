global class InventorySyncBatch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts {


    global Database.QueryLocator start(Database.BatchableContext BC) {

        Id recTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        String query = 'SELECT Id, Name, Advent_Store_Number__c FROM Account WHERE recordTypeId =:recTypeId AND Advent_Store_Number__c != null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> dealerList) {
        if (dealerList.size() > 1) {
            throw new ApplicationException('Scope is too large. Please set the scope size equal to 1 item');
        }

        System.debug(dealerList);
        Account currentDealer = dealerList.get(0);
        System.debug('~~~~ ' + currentDealer.Advent_Store_Number__c);
        Vendor__c vendor = Vendor__c.getOrgDefaults();
        String vendorId = vendor.Id__c;
        AdventApi advApi = new AdventApi(vendorId, currentDealer.Advent_Store_Number__c);

        System.debug('header = ' + advApi.authHeader);

        Map<String, String> params = new Map<String, String>();

        //params.put('start_date', AdventApi.generateDealDate(System.today()));
        //params.put('end_date', AdventApi.generateDealDate(System.today().addDays(-1)));

        //params.put('start_date','08/15/2012');
        //params.put('end_date','08/20/2012');

        HttpResponse response = advApi.get('/inventory', params);
        System.debug('response' + response.getBody());

        System.debug('~~~~~~~~~~~~ ' + response.getBody());
        //DealHandler newDeals = (DealHandler) System.JSON.deserialize(response.getBody(), DealHandler.class);

        //System.debug('data ==========   ' + newDeals.data.get(0).Sale);
    }

    global void finish(Database.BatchableContext bc) {
        try {
            IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }
}