global class InventoryVehicleBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    public Integer recordsProcessed = 0;
    public Integer recordsTotal = 0;
    public static final String ACCOUNT_DEALER_RT = 'Dealer';
    public Integer deltaDays;
    public Boolean isBulk;

    global InventoryVehicleBatchable() {
        this.deltaDays = 1;
        this.isBulk = false;
    }

    global InventoryVehicleBatchable(Integer deltaDays) {
        this.deltaDays = deltaDays;
        this.isBulk = false;
    }

    global InventoryVehicleBatchable(Boolean isBulk) {
        this.isBulk = isBulk;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
            'SELECT CDK_Dealer_Code__c, CDK_Inventory_Company_Number__c ' +
            'FROM Account ' +
            'WHERE RecordType.DeveloperName = :ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null';

        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        for (Account acc : scope) {
            List<CDK_Inventory_Vehicle__c> vehicles = InventoryVehicleHandler.getVehicles(acc.CDK_Dealer_Code__c, deltaDays, acc.Id, acc.CDK_Inventory_Company_Number__c, isBulk);
            try {
                Database.UpsertResult[] results = Database.upsert(vehicles, CDK_Inventory_Vehicle__c.fields.UniqueVehicleId__c, true);
                recordsProcessed += scope.size();
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, e.getStackTraceString());
                System.debug(LoggingLevel.ERROR, e.getMessage());
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        try {
            IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }  
    }

}