public class InventoryVehicleHandler {

    public static final String PATH_VEHICLES = '/pip-extract/inventoryvehicleext/extract';
    public static final String QUERY_VEHICLES_ID = 'IVEH_Delta';
    public static final String QUERY_BULK_VEHICLES_ID = 'IVEH_Bulk';
    public static final String INV_COMPANY_PARAM = 'qparamInvCompany';
    public static final String DEALERID_PARAM = 'dealerId';
    public static final String QUERYID_PARAM = 'queryId';
    public static final String DELTADATE_PARAM = 'deltaDate';
    public static final String DELTADATE_FORMAT = 'MM/dd/yyyy';

    public static List<CDK_Inventory_Vehicle__c> getVehicles(String dealerId, Integer deltaDays, String accountId, Decimal invCompany, Boolean isBulk) {
        Map<String, String> paramsMap = new Map<String, String>();
        if (!isBulk) {
            Datetime deltaDate = Datetime.now().addDays(-deltaDays);
            String deltaDaysStr = deltaDate.format(DELTADATE_FORMAT);
            paramsMap.put(DELTADATE_PARAM, deltaDaysStr);
        }
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(INV_COMPANY_PARAM, String.valueOf(Integer.valueOf(invCompany)));
        paramsMap.put(QUERYID_PARAM, isBulk ? QUERY_BULK_VEHICLES_ID : QUERY_VEHICLES_ID);
        HttpResponse res = DMotorWorksApi.get(PATH_VEHICLES, paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<CDK_Inventory_Vehicle__c> vehicles = InventoryVehicleParser.process(res.getBodyDocument(), accountId);
        System.debug('Vehicles ' + vehicles);
        return vehicles;
    }
}