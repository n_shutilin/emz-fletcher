public class InventoryVehicleParser {

    public static final String NAMESPACE = 'http://www.dmotorworks.com/pip-extract-inventory-vehicle';
    public static final String CHILD_NAME = 'InventoryVehicle';

    public static List<CDK_Inventory_Vehicle__c> process(Dom.Document doc, String accountId) {
        List<CDK_Inventory_Vehicle__c> vehicles = new List<CDK_Inventory_Vehicle__c>();
        String dealerCode = [SELECT Id, CDK_Dealer_Code__c FROM Account WHERE Id = :accountId].CDK_Dealer_Code__c;
        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == CHILD_NAME) {
                System.debug('### ' + child);
                CDK_Inventory_Vehicle__c vehicle = new CDK_Inventory_Vehicle__c();  
                mapFields(vehicle, child);
                vehicle.UniqueVehicleId__c = dealerCode + '_' + vehicle.VehID__c + '_' + vehicle.HostItemID__c;
                vehicles.add(vehicle);
            }
        }

        return vehicles;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'AccountingAccount' => CDK_Inventory_Vehicle__c.AccountingAccount__c,
            'EntryDate' => CDK_Inventory_Vehicle__c.EntryDate__c,
            'ErrorLevel' => CDK_Inventory_Vehicle__c.ErrorLevel__c,
            'Model' => CDK_Inventory_Vehicle__c.Model__c,
            'ModelName' => CDK_Inventory_Vehicle__c.ModelName__c,
            'ModelNo' => CDK_Inventory_Vehicle__c.ModelNo__c,
            'ModelType' => CDK_Inventory_Vehicle__c.ModelType__c,
            'TotalOptionsCost' => CDK_Inventory_Vehicle__c.TotalOptionsCost__c,
            'TotalOptionsInvoice' => CDK_Inventory_Vehicle__c.TotalOptionsInvoice__c,
            'TotalOptionsRetail' => CDK_Inventory_Vehicle__c.TotalOptionsRetail__c,
            'TransmissionNo' => CDK_Inventory_Vehicle__c.TransmissionNo__c,
            'RetailPriceUpdateDate' => CDK_Inventory_Vehicle__c.RetailPriceUpdateDate__c,
            'Color' => CDK_Inventory_Vehicle__c.Color__c,
            'InventoryAcct' => CDK_Inventory_Vehicle__c.InventoryAcct__c,
            'InventoryCompany' => CDK_Inventory_Vehicle__c.InventoryCompany__c,
            'LastActivityDate' => CDK_Inventory_Vehicle__c.LastActivityDate__c,
            'FleetNo' => CDK_Inventory_Vehicle__c.FleetNo__c,
            'HostItemID' => CDK_Inventory_Vehicle__c.HostItemID__c,
            'Balance' => CDK_Inventory_Vehicle__c.Balance__c,
            'BodyStyle' => CDK_Inventory_Vehicle__c.BodyStyle__c,
            'Certified' => CDK_Inventory_Vehicle__c.Certified__c,
            'VIN' => CDK_Inventory_Vehicle__c.VIN__c,
            'VehID' => CDK_Inventory_Vehicle__c.VehID__c,
            'Wholesale' => CDK_Inventory_Vehicle__c.Wholesale__c,
            'Year' => CDK_Inventory_Vehicle__c.Year__c,
            'SoldMileage' => CDK_Inventory_Vehicle__c.SoldMileage__c,
            'Status' => CDK_Inventory_Vehicle__c.Status__c,
            'StockNo' => CDK_Inventory_Vehicle__c.StockNo__c,
            'StockType' => CDK_Inventory_Vehicle__c.StockType__c,
            'Make' => CDK_Inventory_Vehicle__c.Make__c,
            'MakeName' => CDK_Inventory_Vehicle__c.MakeName__c,
            'Mileage' => CDK_Inventory_Vehicle__c.Mileage__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, NAMESPACE);

            System.debug(field + '~~ ' + childElement);
            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}