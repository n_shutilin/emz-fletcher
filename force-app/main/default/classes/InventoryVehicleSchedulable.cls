global class InventoryVehicleSchedulable implements Schedulable{

    global void execute(SchedulableContext SC) {
        InventoryVehicleBatchable batch = new InventoryVehicleBatchable(1);
        Database.executeBatch(batch, 1);

        Datetime nextScheduleTime = System.now().addMinutes(120);
        String hour = String.valueOf(nextScheduleTime.hour());
        String minutes = String.valueOf(nextScheduleTime.minute());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        String jobName = 'Inventory Vehicle Extract ' + nextScheduleTime.format('hh:mm');
        InventoryVehicleSchedulable p = new InventoryVehicleSchedulable();
        System.schedule(jobName, cronValue , p);

        System.abortJob(SC.getTriggerId());
    }

}