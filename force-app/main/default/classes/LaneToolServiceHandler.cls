global class LaneToolServiceHandler implements Messaging.InboundEmailHandler
{
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.Inboundenvelope envelope)
    {
      Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
      try
      {
        Case mycase = new Case();
        mycase.Subject= email.subject;
        mycase.Description = String.isNotBlank(email.htmlBody) ? email.htmlBody : email.plainTextBody;
        mycase.SuppliedEmail= email.FromAddress;
        insert mycase;
      // Save attachments, if any
      List<Attachment> attachments = new List<Attachment>();
      if(email.textAttachments != null)
      {
        for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
          Attachment attachment = new Attachment();
          attachment.Name = tAttachment.fileName;
          attachment.Body = Blob.valueOf(tAttachment.body);
          attachment.ParentId = mycase.Id;
          attachments.add(attachment);
        }
      }
      if(email.binaryAttachments != null)
      {
        for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
          Attachment attachment = new Attachment();
        
          attachment.Name = bAttachment.fileName;
          attachment.Body = bAttachment.body;
          attachment.ParentId = mycase.Id;
          attachments.add(attachment);
        }
      }
      if(attachments.size() > 0)
      {
        insert attachments;
      }
      result.success = true;
      }catch(Exception e)
      {
        result.success = false;
           result.message = e.getMessage() + e.getStackTraceString();
      }
      return result;                                                      
    }
}