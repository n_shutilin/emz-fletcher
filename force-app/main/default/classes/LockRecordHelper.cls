public class LockRecordHelper {
	@AuraEnabled
	public static sObject getRecordData(String Id, String objName) {
		String query = 'SELECT Id, UserWorkingWithRecord__c, LastModifiedDate FROM ';
		query += objName + ' WHERE Id = ' + '\'' + Id + '\' LIMIT 1';
		sObject result = Database.query(query)[0];
		return result;
	}

	@AuraEnabled
	public static Boolean updateRecordData(String Id, String objName, String data) {
		try {
			if (objName == 'Opportunity') {
				Opportunity o = [SELECT UserWorkingWithRecord__c FROM Opportunity WHERE Id = :Id LIMIT 1];
				o.UserWorkingWithRecord__c = data;
				update o;
			} else {
				Lead l = [SELECT UserWorkingWithRecord__c FROM Lead WHERE Id = :Id LIMIT 1];
				l.UserWorkingWithRecord__c = data;
				update l;
			}
			return true;
		} catch (Exception e) {
			System.debug('Error in updating field');
			System.debug(e);
			return false;
		}
	}

	public static void preventUpdateLockOpportunityRecord(List<Opportunity> opps) {
		String uid = Userinfo.getUserId();
		if (opps.size() == 1 && opps[0].UserWorkingWithRecord__c != uid && opps[0].UserWorkingWithRecord__c != null) {
			opps[0].addError('Cannot save changes! The record is blocked!');
		}
	}

	public static void preventUpdateLockLeadRecord(List<Lead> leads) {
		String uid = Userinfo.getUserId();
		if (leads.size() == 1 && leads[0].UserWorkingWithRecord__c != uid && leads[0].UserWorkingWithRecord__c != null) {
			leads[0].addError('Cannot save changes! The record is blocked!');
		}
	}

	public static void prepopulateOpportunityField(List<Opportunity> opps) {
		String uid = Userinfo.getUserId();
		for (Opportunity o : opps) {
			o.UserWorkingWithRecord__c = uid;
		}
	}

	public static void prepopulateLeadField(List<Lead> leads) {
		String uid = Userinfo.getUserId();
		for (Lead l : leads) {
			l.UserWorkingWithRecord__c = uid;
		}
	}

	@AuraEnabled
	public static void recordRequest(String recordId, String userIdBlockedRecord, String userIdMadeRequest, Boolean isAnswer) {
		String userNameMadeRequest = '';
		if (userIdMadeRequest != '') {
			User u = [SELECT Name FROM User WHERE Id = :userIdMadeRequest LIMIT 1];
			userNameMadeRequest = u.Name;
		}		
		RequestRecordUnlock__e newsEvent = new RequestRecordUnlock__e(
			RecordId__c = recordId,
			UserIdBlockedRecord__c = userIdBlockedRecord,
			UserIdMadeRequest__c = userIdMadeRequest,
			NameOfUserMadeRquest__c = userNameMadeRequest,
			IsAnswer__c = isAnswer
		);
		EventBus.publish(newsEvent);
	}
}