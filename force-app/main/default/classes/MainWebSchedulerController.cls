@RestResource(urlMapping='/scheduler/*')
global without sharing class MainWebSchedulerController {
    private static Map<String, WebSchedulerCommand> commandMap = new Map <String, WebSchedulerCommand> {
        'getPersonContactInfo' => new WebSchedulerRequestHandler.getCurrentSObjectTypeCommand(),
        'getDealerInfo' => new WebSchedulerRequestHandler.getDealerInfoCommand(),
        'getServieAppointmentInfo' => new WebSchedulerRequestHandler.getServieAppointmentInfoCommand(),
        'getAuthContacts' => new WebSchedulerRequestHandler.getAuthContactsCommand(),
        'getVehicles' => new WebSchedulerRequestHandler.getVehiclesCommand(),
        'getServiceResourcesByDealer' => new WebSchedulerRequestHandler.getServiceResourcesByDealerCommand(),
        'insertNewVehicle' => new WebSchedulerRequestHandler.insertNewVehicleCommand(),
        'getMostCommonlyUsedWorkTypes' => new WebSchedulerRequestHandler.getMostCommonlyUsedWorkTypesCommand(),
        'getDeclinedServices' => new WebSchedulerRequestHandler.getDeclinedServicesCommand(),
        'removeDeclinedServices' => new WebSchedulerRequestHandler.removeDeclinedServicesCommand(),
        'createServiceAppointment' => new WebSchedulerRequestHandler.createServiceAppointmentCommand(),
        'getAdvisorTimeSlots' => new WebSchedulerRequestHandler.getAdvisorTimeSlotsCommand(),
        'getSchedTimeSlots' => new WebSchedulerRequestHandler.getSchedTimeSlotsCommand(),
        'getDealerTimezoneShift' => new WebSchedulerRequestHandler.getDealerTimezoneShiftCommand(),
        'getAdvisorsAvailability' => new WebSchedulerRequestHandler.getAdvisorsAvailabilityCommand(),
        'scheduleAppointment' => new WebSchedulerRequestHandler.scheduleAppointmentCommand(),
        'getTransportationNotesCommand' => new WebSchedulerRequestHandler.getTransportationNotesCommand(),
        'confirmationServiceAppointmentUpdate' => new WebSchedulerRequestHandler.confirmationServiceAppointmentUpdateCommand(),
        'deleteAppointment' => new WebSchedulerRequestHandler.deleteAppointmentCommand(),
        'sendAppointmentIntoCDK' => new WebSchedulerRequestHandler.sendAppointmentIntoCDKCommand(),
        'executeCodeSend' => new WebSchedulerRequestHandler.executeCodeSend(),
        'authorize' =>  new WebSchedulerRequestHandler.authorize(),
        'createAccount' => new WebSchedulerRequestHandler.createAccount()
    };

    public class SchedulerRequest {
        private String type;
        private String query;
        private List<SObject> objectsToProcess;
        private Map<String, String> arguments;
        private List<String> toAddresses;
        private List<String> ccAddresses;

        public SchedulerRequest() {

        }

        public SchedulerRequest(String type, List<String> toAddresses, List<String> ccAddresses) {
            this.type = type;
            this.toAddresses = toAddresses;
            this.ccAddresses = ccAddresses;
            this.arguments = new Map<String, String>();
        }

        public SchedulerRequest(String type, List<SObject> objectsToProcess) {
            this.type = type;
            this.objectsToProcess = objectsToProcess;
            this.arguments = new Map<String, String>();
        }

        public SchedulerRequest(String type, String query) {
            this.type = type;
            this.query = query;
            this.arguments = new Map<String, String>();
        }

        public SchedulerRequest(String type) {
            this.type = type;
            this.arguments = new Map<String, String>();
        }

        public String getType() {
            return this.type;
        
        }
        public String getQuery() {
            return this.query;
        }

        public List<SObject> getObjectsToProcess() {
            return this.objectsToProcess;
        }

        public void setArgument(String key, String argument) {
            this.arguments.put(key, argument);
        }
        
        public String getArgument(String key) {
            return this.arguments.get(key);
        }

        public List<String> getToAddresses() {
            return this.toAddresses;
        }

        public List<String> getCcAddresses() {
            return this.ccAddresses;
        }
    }

    public class SchedulerResponse {
        private String code;
        private List<SObject> objectList;
        private List<AppointmentBookingController.SchedulingOptionWrapper> apexObjectList;
        private AppointmentBookingController.ServiceAppointmentWrapper apexObject;
        private String body;
        private String errorMessage;
        private Map<String, Map<String, Timeslot[]>> timeSlots;
        private Map<String, Map<String, AssignedResource[]>> advisors;

        public SchedulerResponse(Integer code, String errorMessage) {
            this.code = String.valueOf(code);
            this.errorMessage = errorMessage;
        }

        public SchedulerResponse(Integer code, String errorMessage, Map<String, Map<String, Timeslot[]>> timeSlots) {
            this.code = String.valueOf(code);
            this.errorMessage = errorMessage;
            this.timeSlots = timeSlots;
        }

        public SchedulerResponse(Integer code, String errorMessage, Map<String, Map<String, AssignedResource[]>> advisors) {
            this.code = String.valueOf(code);
            this.errorMessage = errorMessage;
            this.advisors = advisors;
        }

        public SchedulerResponse(Integer code, String errorMessage, String body) {
            this.code = String.valueOf(code);
            this.errorMessage = errorMessage;
            this.body = body;
        }

        public SchedulerResponse(Integer code, String errorMessage, List<SObject> objectList) {
            this.code = String.valueOf(code);
            this.objectList = objectList;
            this.errorMessage = errorMessage;
        }

        public SchedulerResponse(Integer code, String errorMessage, List<AppointmentBookingController.SchedulingOptionWrapper> apexObjectList) {
            this.code = String.valueOf(code);
            this.apexObjectList = apexObjectList;
            this.errorMessage = errorMessage;
        }

        public SchedulerResponse(Integer code, String errorMessage, AppointmentBookingController.ServiceAppointmentWrapper apexObject) {
            this.code = String.valueOf(code);
            this.apexObject = apexObject;
            this.errorMessage = errorMessage;
        }
        
        public Map<String, Map<String, AssignedResource[]>> getAdvisors() {
            return this.advisors;
        }

        public String getCode() {
            return this.code;
        }

        public String getErrorMessage() {
            return this.errorMessage;
        }

        public List<SObject> getObjectList() {
            return this.objectList;
        }

        public Map<String, Map<String, Timeslot[]>> getTimeSlots() {
            return this.timeSlots;
        }

        public List<AppointmentBookingController.SchedulingOptionWrapper> getapexObjectList() {
            return this.apexObjectList;
        }

        public AppointmentBookingController.ServiceAppointmentWrapper getapexObject() {
            return this.apexObject;
        }
        
        public String getBody() {
            return this.body;
        }
    }

    @HttpGet
    global static String doGet() {
        try {
            SchedulerRequest request = (SchedulerRequest) JSON.deserialize(RestContext.request.requestBody.toString(), SchedulerRequest.class);
            if(validateRequest(request)){
                SchedulerResponse response = processRequest(request);
                System.debug('APEX GOVNO response => ' + response);
                System.debug('APEX GOVNO response => ' + JSON.serialize(response));
                return JSON.serialize(response);
            } else {
                return JSON.serialize(new SchedulerResponse(400, 'Bad Request'));
            }
        } catch (Exception e) {
            return JSON.serialize(new SchedulerResponse(400, 'Bad Request ' + e.getMessage()));
        }
    }

    @HttpPost
    global static String doPost() {
        return doGet();
    }


    /*  */
    private static SchedulerResponse processRequest (SchedulerRequest request) {
        return commandMap.get(request.getType()).execute(request);
    }

    private static Boolean validateRequest(SchedulerRequest request) {
        if(request.getType() != null) {
            String type = request.getType();
            return commandMap.keySet().contains(type);
        } else {
            return false;
        }
    }
}