public without sharing class MessageBoardPosts_Controller
{ 

public List <Message_Board_Post__c> msg {get; set;}
public list <User> usr;
public string dep;
public string tempInputDep;
public string dealer;
public string tempInputDealer;


public MessageBoardPosts_Controller( ) {

        usr=[Select Id,Department,Dealer_ID__c from user where Id=:userinfo.getUserId()];
        dep= usr[0].Department;
        string tempInputDep = '%' + dep + '%';
        dealer= usr[0].Dealer_ID__c;
        string tempInputDealer = '%' + dealer + '%';
        
        msg = [Select Id, Body__c from Message_Board_Post__c where Is_Expired__c = False and Start_Date__c <= TODAY and Dealerships_Selected__c like :tempInputDealer 
        and (Departments_Selected__c like :tempInputDep or Departments_Selected__c = NULL) Order by CreatedDate Desc];
  
  
    }}