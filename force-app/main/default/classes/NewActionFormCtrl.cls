public with sharing class NewActionFormCtrl {

    @AuraEnabled(Cacheable=true)
    public static Map<String, List<OptionsWrapper>> getPicklistValues() {
        Map<String, List<OptionsWrapper>> picklistMap = new Map<String, List<OptionsWrapper>>();

        List<New_Action_Setting__mdt> settings = [
            SELECT Field_API_Name__c, Picklist_Items__c
            FROM New_Action_Setting__mdt
        ];

        if (!settings.isEmpty()) {
            for (New_Action_Setting__mdt mdt : settings) {
                picklistMap.put(mdt.Field_API_Name__c, getItemsByString(mdt.Picklist_Items__c));
            }
        }

        return picklistMap;
    }

    private static List<OptionsWrapper> getItemsByString(String itemsStr) {
        List<OptionsWrapper> picklistValues = new List<OptionsWrapper>();
        List<String> items = itemsStr.split(';');

        for (String item : items) {
            picklistValues.add(new OptionsWrapper(item, item));
        }

        return picklistValues;
    }

    @TestVisible
    private class OptionsWrapper {
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String value {get;set;}

        public OptionsWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}