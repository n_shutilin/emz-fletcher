public class OEMLeadsIntegrationActivityBuilder {

    public static final String PLACEHOLDER_CRMID = 'CRM_ID';
    public static final String PLACEHOLDER_SECURITY_CODE = 'SECURITY_CODE';
    public static final String PLACEHOLDER_LEAD_ID = 'LEAD_ID';
    public static final String PLACEHOLDER_VENDOR_DISPOSITION_CODE = 'VENDOR_DISPOSITION_CODE';
    public static final String PLACEHOLDER_TYPE_ID = 'TYPE_ID';
    public static final String PLACEHOLDER_DETAILS = 'DETAILS';
    public static final String PLACEHOLDER_SALES_REPRESENTATIVE = 'SALES_REPRESENTATIVE';
    public static final String PLACEHOLDER_ACTIVITY_DATE = 'ACTIVITY_DATE';
    public static final String PLACEHOLDER_ASSOCIATED_LEAD_ID = 'ASSOCIATED_LEAD_ID';

    public String formHeader() {
        return '<DispositionMessage>' +
                    + '<CrmId>'+ PLACEHOLDER_CRMID +'</CrmId>' +
                        + '<SecurityCode>'+ PLACEHOLDER_SECURITY_CODE +'</SecurityCode>' +
                            + '<Activities>';
    }

    public String formActivityBody() {
        return '<Activity>' +
                    + '<LeadId>'+ PLACEHOLDER_LEAD_ID +'</LeadId>' +
                    + '<VendorDispositionCode>'+ PLACEHOLDER_VENDOR_DISPOSITION_CODE +'</VendorDispositionCode>' +
                    + '<TypeId>'+ PLACEHOLDER_TYPE_ID +'</TypeId>' +
                    + '<Details>'+ PLACEHOLDER_DETAILS +'</Details>' +
                    + '<SalesRepresentative>'+ PLACEHOLDER_SALES_REPRESENTATIVE +'</SalesRepresentative>' +
                    + '<ActivityDate>'+ PLACEHOLDER_ACTIVITY_DATE +'</ActivityDate>' +
                    + '<AssociatedLeadID>'+ PLACEHOLDER_ASSOCIATED_LEAD_ID +'</AssociatedLeadID>' +
                + '</Activity>';
    }

    public String formFooter() {
        return '</Activities>' +
                    + '</DispositionMessage>';
    }
}