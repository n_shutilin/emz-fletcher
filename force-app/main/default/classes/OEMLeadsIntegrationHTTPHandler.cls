public with sharing class OEMLeadsIntegrationHTTPHandler {
    public OEMLeadsIntegrationHTTPHandler() {

    }

    public void sendActivity(String requestBody, String endpoint) {
        HttpRequest request = new HttpRequest();
        request.setBody(requestBody);
        request.setHeader('Content-type', 'text/xml');
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        Http http = new Http();
        HTTPResponse response = http.send(request);
    }
}