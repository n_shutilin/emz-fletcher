public with sharing class OEMLeadsIntegrationHandler {

    private static final String CRM_ID = '35';
    private static final String SECURITY_CODE = 'Sal3sForc3Y3llow1!';
    private static final String ENPOINT_AUDI = 'https://leadservices-staging.audiusaleads.com/dispositions/';
    private static final String ENPOINT_MB = 'http://leadservices-staging.mbusaleads.com/Dispositions';

    private OEMLeadsIntegrationActivityBuilder builder;
    private OEMLeadsIntegrationHTTPHandler httpHandler;

    public OEMLeadsIntegrationHandler() {
        this.builder = new OEMLeadsIntegrationActivityBuilder();
        this.httpHandler = new OEMLeadsIntegrationHTTPHandler();
    }

    public void processLeadActivity(String leadId, String salesRepresentative, Integer typeId) {
        Lead leadToMap = [SELECT Id, Sequence_Sources__c, Make__c
                          FROM Lead 
                          WHERE Id = :leadId];
        String endpoint;
        switch on leadToMap.Make__c {
            when 'Audi' { endpoint = ENPOINT_AUDI; }
            when 'Mercedes-Benz' { endpoint = ENPOINT_MB; }
            when else { return; }
        }
        String requestBody = formRequestBody(leadToMap, salesRepresentative, typeId);
        
        if(requestBody != null) {
            httpHandler.sendActivity(requestBody, endpoint);
        }
    }

    private String formRequestBody(Lead lead, String salesRepresentative, Integer typeId) {
        if(lead.Sequence_Sources__c != null) {
            String header = mapHeader(CRM_ID, SECURITY_CODE, builder.formHeader());
            String footer = builder.formFooter();
            String activityBody = mapBody(lead, String.valueOf(typeId), salesRepresentative, builder.formActivityBody());
            return header + activityBody + footer;
        } else {
            return null;
        }
    }

    private String mapHeader(String crmId, String securityCode, String header) {
        header = header.replace(OEMLeadsIntegrationActivityBuilder.PLACEHOLDER_CRMID, crmId);
        header = header.replace(OEMLeadsIntegrationActivityBuilder.PLACEHOLDER_SECURITY_CODE, securityCode);
        return header;
    }

    private String mapBody(Lead leadToMap, String typeId, String salesRepresentative, String body) {
        String leadId = extractLeadId(leadToMap.Sequence_Sources__c);
        body = body.replace(OEMLeadsIntegrationActivityBuilder.PLACEHOLDER_LEAD_ID, leadId);
        body = body.replace(OEMLeadsIntegrationActivityBuilder.PLACEHOLDER_TYPE_ID, typeId);
        body = body.replace(OEMLeadsIntegrationActivityBuilder.PLACEHOLDER_DETAILS, '');
        body = body.replace(OEMLeadsIntegrationActivityBuilder.PLACEHOLDER_SALES_REPRESENTATIVE, salesRepresentative);
        body = body.replace(OEMLeadsIntegrationActivityBuilder.PLACEHOLDER_ACTIVITY_DATE, getFormatedTime());
        body = body.replace(OEMLeadsIntegrationActivityBuilder.PLACEHOLDER_ASSOCIATED_LEAD_ID, '');
        return body;
    }

    public String extractLeadId(String sequenceSources) {
        String leadId;
        String [] sequenceList =  sequenceSources.split(';');
        if(sequenceList.size() > 0) {
            String firstSequence = sequenceList[0];
            String [] splitedSequence = firstSequence.split('Id: ');
            if(splitedSequence.size() == 2) {
                leadId = splitedSequence[1].trim();
            }
        }
        return leadId;
    }

    private String getFormatedTime() {
        return String.valueOfGmt(DateTime.now()).replace(' ', 'T') + 'Z';
    }
}