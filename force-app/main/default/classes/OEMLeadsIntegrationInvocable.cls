public class OEMLeadsIntegrationInvocable {
    public class Request {

        @InvocableVariable(label = 'Lead Record Id' description = 'Record Id that\'s need to be processed' required = true)
        public Id leadId;
        
        @InvocableVariable(label = 'Activity Type' description = 'Type of activity' required = true)
        public Integer typeId;

        @InvocableVariable(label = 'Sales Representative' description = 'Sales Representative' required = true)
        public String salesRepresentative;
    }

    @InvocableMethod(label='Post Lead Activity')
    public static void postUpdateLeadData(List<Request> requestList) {
        OEMLeadsIntegrationHandler handler = new OEMLeadsIntegrationHandler();
        for(Request request : requestList) {
            handler.processLeadActivity(request.leadId, request.salesRepresentative, request.typeId);
        }
    }
}