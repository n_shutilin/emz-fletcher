public class OppToAdventCtrl {

    public static String BUYER_ROLE = 'Buyer';
    public static String COBUYER_ROLE = 'Cobuyer';
    public static String ErrorMsg = '';

    @AuraEnabled
    public static String createAdventOpp(Id recordId) {
        Opportunity opportunity = OppToAdventCtrl.getOppData(recordId);
        if (opportunity.Vehicle__c == null) {
            throw new AuraHandledException('A vehicle must be selected');
        }
        List<Account> accounts = [SELECT Id, Advent_Store_Number__c FROM Account WHERE Dealer_Code__c = :opportunity.Dealer_ID__c AND Advent_Store_Number__c != NULL];
        if (accounts.isEmpty()) {
            throw new AuraHandledException('Please select a valid Dealer ID');
        }
        Vendor__c vendor = Vendor__c.getOrgDefaults();
        String vendorId = vendor.Id__c;
        AdventApi api = new AdventApi(vendorId, accounts[0].Advent_Store_Number__c);
        System.debug(api);

        String errorMessage = '';
        Map<String, Contact> contactsByRole = getContactsByRole(opportunity.Id);
        if (!contactsByRole.containsKey(BUYER_ROLE)) {
            throw new AuraHandledException('Buyer role is required for sending');
        }
        try {
            String buyerClientId = getClientId(api, opportunity, contactsByRole.get(BUYER_ROLE));
            String cobuyerClientId = null;
            if (!opportunity.Is_Business__c && contactsByRole.keySet().contains(COBUYER_ROLE)) {
                cobuyerClientId = getClientId(api, opportunity, contactsByRole.get(BUYER_ROLE));
            }

            String adventOpportunityId = createAdventOpportunity(api, recordId, buyerClientId);

            update new Opportunity(Id = recordId, Advent_sync_status__c = true, Last_sync_time__c = Datetime.now(), Sync_error_msg__c = '');
            return '';
        } catch (Exception e) {
            errorMessage = e.getMessage();
            System.debug('Error ' + e.getMessage());
            throw new AuraHandledException(e.getMessage());
        } finally {
            try {
                update new Opportunity(Id = recordId, Advent_sync_status__c = false, Last_sync_time__c = Datetime.now(), Sync_error_msg__c = errorMessage);
                IntegrationLogger.insertLogs();
            } catch (Exception ex) {
                System.debug(ex.getMessage());
            }
        }
    }

    @AuraEnabled
    public static String createTradeInAndCreditApp(Id recordId) {
        Opportunity opportunity = OppToAdventCtrl.getOppData(recordId);
        List<Account> accounts = [SELECT Id, Advent_Store_Number__c FROM Account WHERE Dealer_Code__c = :opportunity.Dealer_ID__c AND Advent_Store_Number__c != NULL];
        if (accounts.isEmpty()) {
            throw new AuraHandledException('Please select a valid Dealer ID');
        }
        Vendor__c vendor = Vendor__c.getOrgDefaults();
        String vendorId = vendor.Id__c;
        AdventApi api = new AdventApi(vendorId, accounts[0].Advent_Store_Number__c);
        system.debug(api);

        try {
            List<Opportunity> opps = [SELECT Id, External_Id__c FROM Opportunity WHERE Id = :recordId];
            String adventOpportunityId = opps.isEmpty() ? null : opps[0].External_Id__c;
            Map<String, Contact> contactsByRole = getContactsByRole(opportunity.Id);
            System.debug('~ contactsByRole ' + contactsByRole);
            String buyerClientId = getClientId(api, opportunity, contactsByRole.get(BUYER_ROLE));
            System.debug('--- buyerClientId1: ' + buyerClientId);
            if (String.isBlank(buyerClientId)) {
                buyerClientId = createClient(api, contactsByRole.get(BUYER_ROLE));
            }
            System.debug('--- buyerClientId2: ' + buyerClientId);
            String cobuyerClientId = null;
            if (!opportunity.Is_Business__c && contactsByRole.keySet().contains(COBUYER_ROLE)) {
                cobuyerClientId = getClientId(api, opportunity, contactsByRole.get(COBUYER_ROLE));
                if (String.isBlank(cobuyerClientId)) {
                    cobuyerClientId = createClient(api, contactsByRole.get(COBUYER_ROLE));
                }
                AdventOpportunityHandler oppImpl = new AdventOpportunityHandler(api);
                oppImpl.addCobyerToOpportunity(adventOpportunityId, cobuyerClientId);
            }
            createTradeIn(api, recordId, adventOpportunityId);

            Map<Id, String> contactIdToClientId = new Map<Id, String> {contactsByRole.get(BUYER_ROLE).Id => buyerClientId};
            if (cobuyerClientId != null && contactsByRole.get(COBUYER_ROLE) != null) {
                contactIdToClientId.put(contactsByRole.get(COBUYER_ROLE).Id, cobuyerClientId);
            }
            createCreditApplication(api, recordId, contactIdToClientId);


            update new Opportunity(Id = recordId, Advent_sync_status__c = true, Last_sync_time__c = Datetime.now(), Sync_error_msg__c = '');
            return '';
        } catch (Exception e) {
            System.debug('CATCH BLOCK IN METHOD ' + e.getMessage());
            update new Opportunity(Id = recordId, Advent_sync_status__c = false, Last_sync_time__c = Datetime.now(), Sync_error_msg__c = e.getMessage());
            return e.getMessage();
        } finally {
            try {
                IntegrationLogger.insertLogs();
            } catch (Exception ex) {
                System.debug(ex.getMessage());
            }
        }

    }

    @AuraEnabled
    public static String updateAdventOpp(Id recordId) {
        Opportunity opportunity = OppToAdventCtrl.getOppData(recordId);
        if (opportunity.Vehicle__c == null) {
            throw new AuraHandledException('A vehicle must be selected');
        }
        List<Account> accounts = [SELECT Id, Advent_Store_Number__c FROM Account WHERE Dealer_Code__c = :opportunity.Dealer_ID__c AND Advent_Store_Number__c != NULL];
        if (accounts.isEmpty()) {
            throw new AuraHandledException('Please select a valid Dealer ID');
        }
        Vendor__c vendor = Vendor__c.getOrgDefaults();
        String vendorId = vendor.Id__c;
        AdventApi api = new AdventApi(vendorId, accounts[0].Advent_Store_Number__c);
        AdventOpportunityHandler.OpportunityResource opr = AdventOpportunityHandler.buildOpportunityResource(recordId, null);
        Map<String, String> paramsMap = new Map<String, String>{
            AdventOpportunityHandler.ID_PARAM => opportunity.External_Id__c
        };
        AdventOpportunityHandler oppImpl = new AdventOpportunityHandler(api);
        Map<String, Contact> contactsByRole = getContactsByRole(opportunity.Id);
        if (!contactsByRole.containsKey(BUYER_ROLE)) {
            throw new AuraHandledException('Buyer role is required for sending');
        }
        try {
            AdventOpportunityHandler.PostOpportunityResponse resp = oppImpl.updateOpp(opr, paramsMap);

            if (resp.success) {
                update new Opportunity(Id = recordId, Advent_sync_status__c = true, Last_sync_time__c = Datetime.now(), Sync_error_msg__c = '');
                updateDeal(recordId);
                return '';
            } else {
                String errorMsg = '';
                errorMsg += resp.message;
                if (resp.errors != null) {
                    errorMsg = errorMsg + ' ' + resp.errors[0].msg;
                }
                update new Opportunity(Id = recordId, Advent_sync_status__c = false, Last_sync_time__c = Datetime.now(), Sync_error_msg__c = errorMsg);
                return errorMsg;
            }

        } catch (Exception e) {
            update new Opportunity(Id = recordId, Advent_sync_status__c = false, Last_sync_time__c = Datetime.now(), Sync_error_msg__c = e.getMessage());
            return e.getMessage();
        } finally {
            try {
                IntegrationLogger.insertLogs();
            } catch (Exception ex) {
                System.debug(ex.getMessage());
            }
        }
    }

    private static void updateDeal(String recordId) {
        Opportunity opp = [
            SELECT Id, Dealer_ID__c, Opportunity_Number__c
            FROM Opportunity
            WHERE Id = :recordId
        ];

        DealSyncBatch batchDeal = new DealSyncBatch(opp.Dealer_ID__c, opp.Opportunity_Number__c);
        Id vehBatchId = Database.executeBatch(batchDeal,1);
    }

    public static void createCreditApplication(AdventApi api, String opportunityId, Map<Id, String> contactIdToClientId) {
        List<Credit_Application__c> creditApplications = getCreditApplicationData(opportunityId, contactIdToClientId.keySet());
        System.debug('--- creditApplications: ' + creditApplications);
        if (creditApplications.isEmpty()) {
            return;
        }

        AdventCreditApplicationHandler applicationHandler = new AdventCreditApplicationHandler(api);
        Map<Id, AdventCreditApplicationHandler.CreditApplicationResource> resources = applicationHandler.createCreditApplicationWrappers(creditApplications);
        for (Id contactId : resources.keySet()) {
            Map<String, String> paramsMap = new Map<String, String>{
                    AdventCreditApplicationHandler.ID_PARAM => contactIdToClientId.get(contactId)
            };
            AdventCreditApplicationHandler.PostCreditApplicationResponse resp = applicationHandler.updateCreditApplication(resources.get(contactId), paramsMap);
            System.debug('response ' + resp);
        }
    }

    public static void createTradeIn(AdventApi api, String opportunityId, String adventOpportunityId) {
        Trade_In__c tradeIn = getTradeInData(opportunityId);
        System.debug('--- tradeIn: ' + tradeIn);
        if (tradeIn == null) {
            // no trade-in to send
            return;
        }
        AdventOpportunityTradeHandler.TradeResource tr = new AdventOpportunityTradeHandler.TradeResource();
        tr.opportunity_id = adventOpportunityId;
        tr.TradeNumber = '1'; // todo remove hardcode, confirm trade number type
        tr.InvtVIN = tradeIn.VIN__c;
        //TODO: change to a picklist of 1=>‘Car’, 2=>‘Truck’, 3=>‘Van’, 4=>‘RV’, 5=>‘HDTrk’, 6=>‘Other’, 7=>‘SpUtl’
        //tr.InvtTypeVehicle = tradeIn.Model__c;
        tr.InvtModelName = tradeIn.Model__c;
        // ptr.InvtTypeNU = ;
        if (String.isNotBlank(tradeIn.Year__c)) {
            tr.InvtYYYY = Integer.valueOf(tradeIn.Year__c);
        }
        tr.InvtMakeCode = 'HO'; // todo remove hardcode
        tr.InvtMakeName = tradeIn.Make__c;

        AdventOpportunityTradeHandler tradeInImpl = new AdventOpportunityTradeHandler(api);
        AdventOpportunityTradeHandler.PostTradeResponse resp = tradeInImpl.postTrade(tr);

    }

    public static String createAdventOpportunity(AdventApi api, String opportunityId, String clientId) {
        AdventOpportunityHandler.OpportunityResource opr = AdventOpportunityHandler.buildOpportunityResource(opportunityId, clientId);

        AdventOpportunityHandler oppImpl = new AdventOpportunityHandler(api);
        AdventOpportunityHandler.PostOpportunityResponse resp = oppImpl.createOpp(opr);
        System.debug('response = = ' + resp);
        System.debug('response SUCCESS = = ' + resp.success);

        if (resp.success) {
            update new Opportunity(
                Id = opportunityId,
                Advent_sync_status__c = true,
                Last_sync_time__c = Datetime.now(),
                Sync_error_msg__c = '',
                External_Id__c = resp.data.opportunity_id,
                Client_Number__c = resp.data.client_number,
                Cobuyer_Client_Number__c = resp.data.cobuyer_client_number,
                Opportunity_Number__c = resp.data.opportunity_number,
                StageName = 'Desking'
            );
        } else {
            String errorMsg = '';
            errorMsg += resp.message;
            if (resp.errors != null) {
                errorMsg = errorMsg + ' ' + resp.errors[0].msg;
            }
            update new Opportunity(Id = opportunityId, Advent_sync_status__c = false, Last_sync_time__c = Datetime.now(), Sync_error_msg__c = errorMsg);
            throw new ApplicationException(errorMsg);
        }
        return resp.data.opportunity_id;
    }

    @AuraEnabled
    public static Opportunity getOppData(String recordId) {
        List<Opportunity> opps = [
            SELECT Id, Name, Is_Business__c, Customer_Email_Primary__c, Customer_Primary_Phone__c,
                Customer_Mobile_Phone__c, Customer_Home_Phone__c, Customer_Work_Phone__c,
                Customer_First_Name__c, Customer_Last_Name__c, Vehicle__c, DMS_Opportunity_Type__c,
                External_Id__c, Advent_sync_status__c, Last_sync_time__c, Sync_error_msg__c, Dealer_ID__c
            FROM Opportunity
            WHERE Id = :recordId
        ];
       // System.debug('opp = ' + opp);

        return !opps.isEmpty() ? opps[0] : null;
    }

    public static Account getAccountData(Id oppId) {
        Account acc = [
            SELECT Id, Name
            FROM Account
            WHERE Id = :oppId
        ];
        return acc;
    }

    //TODO: Move to selector
    public static List<Credit_Application__c> getCreditApplicationData(Id oppId, Set<Id> contactIds) {
        System.debug('--- oppId: ' + oppId);
        System.debug('--- contactIds: ' + contactIds);
        return [
            SELECT Id, Name, Contact__c, Gross_Income__c, Gross_Income_Interval__c, Alimony_Income__c, Alimony_Income_Interval__c,
                Alimony_Income_Court_Order__c, Alimony_Income_Written_Agreement__c, Alimony_Income_Verbal_Agreement__c,
                Other_Income__c, Other_Income_Description__c, Other_Income_Interval__c, Education_Level__c, Marital_Status__c,
                Number_of_Dependents__c, Dependent_1_Age__c, Dependent_2_Age__c, Dependent_3_Age__c,
                Personal_Reference_1_First_Name__c, Personal_Reference_1_Last_Name__c, Personal_Reference_1_Street__c, Personal_Reference_1_City__c, Personal_Reference_1_State__c,
                Personal_Reference_1_Postal_Code__c, Personal_Reference_1_Phone_Number__c, Personal_Reference_1_Relationship__c,
                Personal_Reference_2_First_Name__c, Personal_Reference_2_Last_Name__c, Personal_Reference_2_Street__c,
                Personal_Reference_2_City__c, Personal_Reference_2_State__c, Personal_Reference_2_Postal_Code__c,
                Personal_Reference_2_Phone_Number__c, Personal_Reference_2_Relationship__c, Personal_Reference_3_First_Name__c,
                Personal_Reference_3_Last_Name__c, Personal_Reference_3_Street__c, Personal_Reference_3_City__c,
                Personal_Reference_3_State__c, Personal_Reference_3_Postal_Code__c, Personal_Reference_3_Phone_Number__c,
                Personal_Reference_3_Relationship__c, Credit_Reference_1_Type__c, Credit_Reference_1_Closed__c,
                Credit_Reference_1_Company__c, Credit_Reference_1_Account_Number__c, Credit_Reference_1_Address_1__c,
                Credit_Reference_1_Address_2__c, Credit_Reference_1_Current_Balance__c, Credit_Reference_1_High_Balance__c,
                Credit_Reference_1_Payment__c, Credit_Reference_1_Closed_Date__c, Credit_Reference_2_Type__c, Credit_Reference_2_Closed__c,
                Credit_Reference_2_Company__c, Credit_Reference_2_Account_Number__c, Credit_Reference_2_Address_1__c,
                Credit_Reference_2_Address_2__c, Credit_Reference_2_Current_Balance__c, Credit_Reference_2_High_Balance__c,
                Credit_Reference_2_Payment__c, Credit_Reference_2_Closed_Date__c, Credit_Reference_3_Type__c, Credit_Reference_3_Closed__c,
                Credit_Reference_3_Company__c, Credit_Reference_3_Account_Number__c, Credit_Reference_3_Address_1__c,
                Credit_Reference_3_Address_2__c, Credit_Reference_3_Current_Balance__c, Credit_Reference_3_High_Balance__c,
                Credit_Reference_3_Payment__c, Credit_Reference_3_Closed_Date__c, Credit_Reference_4_Type__c,
                Credit_Reference_4_Closed__c, Credit_Reference_4_Company__c, Credit_Reference_4_Account_Number__c,
                Credit_Reference_4_Address_1__c, Credit_Reference_4_Address_2__c, Credit_Reference_4_Current_Balance__c,
                Credit_Reference_4_High_Balance__c, Credit_Reference_4_Payment__c, Credit_Reference_4_Closed_Date__c,
                Bank_Reference_1_Name__c, Bank_Reference_1_Account_Number__c, Bank_Reference_1_Account_Type__c,
                Bank_Reference_1_Branch__c, Bank_Reference_1_Address__c, Bank_Reference_1_Balance__c, Bank_Reference_2_Name__c,
                Bank_Reference_2_Account_Number__c, Bank_Reference_2_Account_Type__c, Bank_Reference_2_Branch__c, Bank_Reference_2_Address__c,
                Bank_Reference_2_Balance__c, Residence_Type__c, Mortgage_Company_Name__c, Mortgage_Account_Number__c, Mortgage_Street__c,
                Mortgage_City__c, Mortgage_State__c, Mortgage_Zip__c, Mortgage_Purchase_Date__c, Mortgage_Home_Age__c,
                Mortgage_Home_Price__c, Mortgage_Home_Market_Value__c, Mortgage_Balance__c, Mortgage_Monthly_Payment__c,
                Mortgage_2nd_Balance__c, Mortgage_2nd_Payment__c, Monthly_Rent__c, Current_Vehicle_1_Finance_Company__c,
                Current_Vehicle_1_Account_Number__c, Current_Vehicle_1_Address__c, Current_Vehicle_1_Payment__c,
                Current_Vehicle_2_Finance_Company__c, Current_Vehicle_2_Account_Number__c, Current_Vehicle_2_Address__c,
                Current_Vehicle_2_Payment__c, Insurance_Company_Name__c, Insurance_Agent__c, Insurance_Street__c, Insurance_City__c,
                Insurance_State__c, Insurance_Zip__c, Insurance_Phone_Number__c, Insurance_Policy_Number__c, Insurance_Garaged_At__c,
                Insurance_Cancelled__c, Insurance_Cancelled_Reason__c, Insurance_Losses_Number__c, Insurance_Losses_Amount__c,
                Property_Repossessed__c, Bankruptcy_Filed__c, Lawsuit_s_Pending__c, Active_Military__c, Inactive_Military__c, Military_Reserve__c
            FROM Credit_Application__c
            WHERE Opportunity__c = :oppId AND Contact__c = :contactIds
        ];
    }

    public static Trade_In__c getTradeInData(Id oppId) {
        List<Trade_In__c> tradeIn = [
            SELECT Id, Name, VIN__c, Model__c, Year__c, Make__c
            FROM Trade_In__c
            WHERE Opportunity__c = :oppId
        ];

        if (tradeIn.isEmpty()) {
            return null;
        } else {
            // todo handle if more than 1 trade-in
            return tradeIn.get(0);
        }

    }

    public static Vehicle__c getVehicleData(Id vehicleId) {
        if (String.isBlank(vehicleId)) {
            return null;
        }

        Vehicle__c vehicle = [
            SELECT Id, Name, Stock_Number__c, New_Used__c, Make__c, Model__c,
                Year__c, Exterior_Color__c
            FROM Vehicle__c
            WHERE Id = :vehicleId
        ];
        return vehicle;
    }


    public static Map<String, Contact> getContactsByRole(Id opportunityId) {
        Map<Id, String> buyerIds = new Map<Id, String>();
        for (OpportunityContactRole role : [
            SELECT Id, ContactId, Role FROM OpportunityContactRole
            WHERE OpportunityId = :opportunityId AND (Role = :BUYER_ROLE OR Role = :COBUYER_ROLE)
        ]) {
            buyerIds.put(role.ContactId, role.Role);
        }

        List<Contact> contacts = [
            SELECT Id, LastName, MiddleName, FirstName, Name, Email,
                Phone, MobilePhone, Fax, HomePhone, OtherPhone
            FROM Contact
            WHERE Id = :buyerIds.keySet()
        ];
        Map<String, Contact> contactsByRole = new Map<String, Contact>();
        for (Contact contact : contacts) {
            if (buyerIds.containsKey(contact.Id)) {
                contactsByRole.put(buyerIds.get(contact.Id), contact);
            }
        }
        return contactsByRole;
    }

    //TODO: Refactor
    public static Contact getBuyerData(Id oppId) {
        List<OpportunityContactRole> ocrs = [
            SELECT Id, ContactId, OpportunityId, Role
            FROM OpportunityContactRole
            WHERE OpportunityId = :oppId
            AND Role = :BUYER_ROLE
        ];
        if (ocrs.isEmpty()) {
            String msg = 'Could not find contact with role: ' + BUYER_ROLE;
            ErrorMsg = msg;
            System.debug(LoggingLevel.ERROR, msg);
            AuraHandledException ex = new AuraHandledException(msg);
            ex.setMessage(msg);
            throw ex;
        }
        Contact cnt = [
            SELECT Id, LastName, MiddleName, FirstName, Name, Email,
                Phone, MobilePhone, Fax, HomePhone, OtherPhone
            FROM Contact
            WHERE Id = :ocrs.get(0).ContactId
        ];

        return cnt;
    }

    public static Contact getCobuyerData(Id oppId) {
        List<OpportunityContactRole> ocrs = [
            SELECT Id, ContactId, OpportunityId, Role
            FROM OpportunityContactRole
            WHERE OpportunityId = :oppId
            AND Role = :COBUYER_ROLE
        ];
        if (ocrs.isEmpty()) {
            String msg = 'Could not find contact with role: ' + COBUYER_ROLE;
            ErrorMsg = msg;
            System.debug(LoggingLevel.ERROR, msg);
            AuraHandledException ex = new AuraHandledException(msg);
            ex.setMessage(msg);
            throw ex;
        }
        Contact cnt = [
            SELECT Id, LastName, FirstName, Name, Phone, Email, MiddleName
            FROM Contact
            WHERE Id = :ocrs.get(0).ContactId
        ];

        return cnt;
    }

    public static String getClientId(AdventApi api, Opportunity opportunity, Contact contact) {
        Map<String, String> clientParams = new Map<String, String>();
        if (opportunity.Is_Business__c) {
            clientParams.put(AdventClientHandler.BUSINESS_F_PARAM, AdventApi.Flag.Y.name());
            Account acc = getAccountData(opportunity.Id);
            clientParams.put(AdventClientHandler.BUSINESS_NAME_PARAM, acc.Name);
        } else {
            clientParams.put(AdventClientHandler.BUSINESS_F_PARAM, AdventApi.Flag.N.name());
            clientParams.put(AdventClientHandler.EMAIL_PARAM, contact.Email);
            clientParams.put(AdventClientHandler.PHONE_PARAM, contact.Phone);
            clientParams.put(AdventClientHandler.INDIVIDUAL_FIRST_NAME_PARAM, contact.FirstName);
            clientParams.put(AdventClientHandler.INDIVIDUAL_LAST_NAME_PARAM, contact.LastName);
        }
        AdventClientHandler clientImpl = new AdventClientHandler(api);
        AdventClientHandler.GetClientResponse gcr = clientImpl.getClients(clientParams);
        String clientId = null;
        if (gcr.totalProperty > 0) {
            clientId = gcr.data[0].client_id;
        }
        return clientId;
    }

    public static String createClient(AdventApi api, Contact contactItem) {
        AdventClientHandler.ClientResource client = new AdventClientHandler.ClientResource();
        client.home_phone = contactItem.HomePhone;
        client.individual_first_name = contactItem.FirstName;
        client.individual_last_name = contactItem.LastName;
        client.cell_phone = contactItem.Phone;
        AdventClientHandler clientImpl = new AdventClientHandler(api);
        AdventClientHandler.InsertClientResponse response = clientImpl.insertClient(client);
        String clientId = null;
        if (response.success) {
            clientId = response.data.client_id;
        }
        return clientId;
    }

}