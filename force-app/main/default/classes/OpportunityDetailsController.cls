public class OpportunityDetailsController {
    @AuraEnabled
    public static String getOriginalOpportunityID(String oppId) {
        String originalOppId;
        Opportunity opp = [SELECT Original_Opportunity_Id__c FROM Opportunity WHERE Id =: oppId LIMIT 1];
        if(opp != null && String.isNotEmpty(opp.Original_Opportunity_Id__c)) {
            originalOppId = opp.Original_Opportunity_Id__c;
        } 
        return originalOppId;
    }

    @AuraEnabled
    public static String getOpportunityFields() {
        List<Schema.FieldSetMember> fieldSetFields = SObjectType.Opportunity.FieldSets.OriginalOpportunityDetails.getFields();
        List<String> fields = new List<String>();
        for (Schema.FieldSetMember field : fieldSetFields) {
            fields.add(field.getFieldPath());
        }
        return JSON.serialize(fields);
    }
}