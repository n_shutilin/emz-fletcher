public with sharing class OpportunityEventLogController {
    public static final String ERROR_MESSAGE_CURRENT_SALES_MANAGER_IS_NOT_AVAILABLE_IN_THAT_TIME =
            'Current Sales Manager is not available in that time, please choose another time slot.';

    @AuraEnabled(Cacheable = true)
    public static List<PicklistValues> getAppointmentTypes() {
        List<Schema.PicklistEntry> picklistEntries = Event.Appointment_Type__c.getDescribe().getPicklistValues();

        List<PicklistValues> appointmentTypes = new List<PicklistValues>();
        for (Schema.PicklistEntry picklistEntry : picklistEntries) {
            appointmentTypes.add(new PicklistValues(picklistEntry.getLabel(), picklistEntry.getValue()));
        }
        return appointmentTypes;
    }

    @AuraEnabled
    public static String getPersonAccountEmail(String opportunityId) {
        return [SELECT Id, Dealer_ID__c, Account.PersonEmail FROM Opportunity WHERE Id = :opportunityId]?.Account.PersonEmail;
    }

    @AuraEnabled(Cacheable = true)
    public static List<PicklistValues> getSalesManagerNames(Id opportunityId) {
        String dealerId = [SELECT Id, Dealer_ID__c FROM Opportunity WHERE Id = :opportunityId]?.Dealer_ID__c;
        Set<Id> saleManagerIds = getServiceResourceIds(dealerId, 'Sale Manager');

        List<User> saleManagers = [SELECT Id, Name FROM User WHERE Id IN :saleManagerIds];

        List<PicklistValues> saleManagersNames = new List<PicklistValues>();
        for (User user : saleManagers) {
            saleManagersNames.add(new PicklistValues(user.Name, user.Id));
        }
        return saleManagersNames;
    }

    @AuraEnabled(Cacheable = true)
    public static List<PicklistValues> getSalesPersonNames(Id opportunityId, String saleManagerId) {
        String dealerId = [SELECT Id, Dealer_ID__c FROM Opportunity WHERE Id = :opportunityId]?.Dealer_ID__c;

        Set<Id> salesPersonIds = getServiceResourceIds(dealerId, 'Salesperson');

        List<User> salesPersons = [
                        SELECT Id, Name
                        FROM User
                        WHERE Id IN :salesPersonIds
                ];
        // List<User> salesPersons = new List<User>();
        // if (String.isNotBlank(saleManagerId)) {
        //     salesPersons = [
        //             SELECT Id, Name, ManagerId
        //             FROM User
        //             WHERE Id IN :salesPersonIds AND ManagerId = :saleManagerId
        //     ];
        // }
        // if (salesPersons.isEmpty()) {
        //     salesPersons = [
        //             SELECT Id, Name
        //             FROM User
        //             WHERE Id IN :salesPersonIds
        //     ];
        // }

        List<PicklistValues> salesPersonsNames = new List<PicklistValues>();
        for (User user : salesPersons) {
            salesPersonsNames.add(new PicklistValues(user.Name, user.Id));
        }
        return salesPersonsNames;
    }

    @AuraEnabled
    public static Boolean isAvailableDayOfWeekForAppointment(Id opportunityId, String selectedDateString) {
        return String.isNotBlank(selectedDateString) &&
                Date.today() <= Date.valueOf(selectedDateString) &&
                !getAvailableAppointmentTimeSlots(opportunityId, selectedDateString).isEmpty();
    }

    @AuraEnabled(Cacheable = true)
    public static List<PicklistValues> getAvailableTimePerDayAppointment(Id opportunityId, String selectedDateString) {
        List<PicklistValues> availableTimeIntervals = new List<PicklistValues>();
        if (String.isNotBlank(selectedDateString)) {
            List<TimeSlot> availableTimeSlots = getAvailableAppointmentTimeSlots(opportunityId, selectedDateString);
            Time lastAvailableTime;
            for (TimeSlot timeSlot : availableTimeSlots) {
                System.debug('timeSlot.DayOfWeek: ' + timeSlot.DayOfWeek);
                Time startTimeInterval = timeSlot.StartTime;

                for (; startTimeInterval <= timeSlot.EndTime;) {
                    String timeString = getFormattedTimeString(startTimeInterval);
                    availableTimeIntervals.add(new PicklistValues(timeString, String.valueOf(startTimeInterval)));

                    if (lastAvailableTime == null || startTimeInterval > lastAvailableTime) {
                        lastAvailableTime = startTimeInterval;
                    }
                    startTimeInterval = startTimeInterval.addMinutes(15);
                }
            }
            if (!availableTimeIntervals.isEmpty()) {
                availableTimeIntervals.remove(availableTimeIntervals.size() - 1);
            }
        }

        return availableTimeIntervals;
    }

    @AuraEnabled
    public static EventCreatedInfo createEvent(EventInfo eventInfo) {
        Datetime startDatetime = Datetime.valueOf(eventInfo.eventDate + ' ' + eventInfo.eventStartTime);
        Datetime endDatetime = startDatetime.addMinutes(15);
        List<Event> saleManagerEvents = [
                SELECT Id, Busy__c, StartDateTime, EndDateTime
                FROM Event
                WHERE OwnerId = :eventInfo.saleManagerId AND
                ((StartDateTime >= :startDatetime AND StartDateTime < :endDatetime) OR
                (EndDateTime > :startDatetime AND EndDateTime <= :endDatetime) OR
                (StartDateTime <= :startDatetime AND EndDateTime >= :endDatetime))
        ];
//        System.debug('***************');
//        System.debug('startDatetime: ' + startDatetime);
//        System.debug('endDatetime: ' + endDatetime);
        Boolean isBusy = false;
        for (Event event : saleManagerEvents) {
            if (event.Busy__c) {
                isBusy = true;
            }
//            System.debug('StartDateTime <= :startDatetime AND EndDateTime > :startDatetime: ' + (event.StartDateTime <= startDatetime && event.EndDateTime > startDatetime));
//            System.debug('StartDateTime < :endDatetime AND EndDateTime > :endDatetime: ' + (event.StartDateTime < endDatetime && event.EndDateTime > endDatetime));
//            System.debug('StartDateTime <= :startDatetime AND EndDateTime <= :endDatetime: ' + (event.StartDateTime <= startDatetime && event.EndDateTime <= endDatetime));
//            System.debug('StartDateTime <= :startDatetime: ' + (event.StartDateTime <= startDatetime));
//            System.debug('EndDateTime <= :endDatetime: ' + (event.EndDateTime <= endDatetime));
//            System.debug('isBusy: ' + isBusy);
        }

        if (saleManagerEvents.size() >= 4 || isBusy) {
            return new EventCreatedInfo(false, false, ERROR_MESSAGE_CURRENT_SALES_MANAGER_IS_NOT_AVAILABLE_IN_THAT_TIME);
        }

        Opportunity relatedOpportunity = [SELECT Id, Dealer_ID__c, Account.PersonContactId FROM Opportunity WHERE Id = :eventInfo.opportunityId];
        String relatedContactId = relatedOpportunity?.Account.PersonContactId;
        String dealerId = relatedOpportunity?.Dealer_ID__c;
        String salesPersonName = [SELECT Id, Name FROM User WHERE Id = :eventInfo.salesPersonId]?.Name;

        Event event = new Event();
        event.Id = eventInfo.appointmentId;
        event.DealerId__c = dealerId;
        event.Subject = eventInfo.eventType + (String.isNotBlank(salesPersonName) ? ' with (' + salesPersonName + ')' : '');
        event.Appointment_Type__c = eventInfo.eventType;
        event.StartDateTime = startDatetime;
        event.EndDateTime = endDatetime;
        event.OwnerId = eventInfo.saleManagerId;
        event.Salesperson__c = eventInfo.salesPersonId;
        event.Description = eventInfo.eventNotes;
        event.WhatId = eventInfo.opportunityId;
        event.WhoId = relatedContactId;
        event.DoNotSendEmail__c = eventInfo.doNotSendEmail;
        event.TO_Email__c = eventInfo.toEmail;
        event.CC_Email__c = eventInfo.ccEmail;

        Database.UpsertResult saveResult = Database.upsert(event, false);

        if (saveResult.isSuccess()) {
            tossPlatformEvent(eventInfo.opportunityId);
        }

        String errorMessage = '';
        if (saveResult.isSuccess() && saveResult.isCreated()) {
            // EventRelation salespersonAttendees = new EventRelation(EventId = event.Id, RelationId = eventInfo.salesPersonId);
            // insert salespersonAttendees;
        } else if (!saveResult.getErrors().isEmpty()) {
            errorMessage = saveResult.getErrors()[0].getMessage();
        }

        Boolean isEventCreated = saveResult.isSuccess() && saveResult.isCreated();
        Boolean isEventUpdated = saveResult.isSuccess() && !saveResult.isCreated();

        return new EventCreatedInfo(isEventCreated, isEventUpdated, errorMessage);
    }

    @AuraEnabled
    public static EventInfo getAppointmentInfo(String appointmentId) {
        if (String.isBlank(appointmentId)) return null;

        List<Event> appointmentList = [
                SELECT Appointment_Type__c, StartDateTime, OwnerId, Salesperson__c, Description, DoNotSendEmail__c, TO_Email__c, CC_Email__c
                FROM Event
                WHERE Id = :appointmentId
        ];

        if (appointmentList.isEmpty()) return null;

        Event appointment = appointmentList[0];

        EventInfo eventInfo = new EventInfo(
                appointmentId,
                appointment.Appointment_Type__c,
                String.valueOf(appointment.StartDateTime.date()),
                String.valueOf(appointment.StartDateTime.time()),
                appointment.OwnerId,
                appointment.Salesperson__c,
                appointment.Description,
                appointment.DoNotSendEmail__c,
                appointment.TO_Email__c,
                appointment.CC_Email__c
        );

        return eventInfo;
    }

    private static Set<Id> getServiceResourceIds(String dealerId, String salesResourceType) {
        List<ServiceTerritoryMember> territoryMembers = [
                SELECT Id, ServiceResourceId, ServiceResource.Name, EffectiveStartDate, EffectiveEndDate
                FROM ServiceTerritoryMember
                WHERE ServiceTerritory.Dealer__c = :dealerId
                AND EffectiveStartDate <= TODAY
                AND (EffectiveEndDate = NULL OR EffectiveEndDate >= TODAY)
        ];

        Set<Id> serviceResourceId = new Set<Id>();
        for (ServiceTerritoryMember serviceTerritoryMember : territoryMembers) {
            serviceResourceId.add(serviceTerritoryMember.ServiceResourceId);
        }
        List<ServiceResource> serviceResources = [
                SELECT Id, RelatedRecordId, RelatedRecord.Name, Name, IsActive
                FROM ServiceResource
                WHERE Id IN :serviceResourceId
                AND Sales_Resource_Type__c = :salesResourceType
                AND IsActive = TRUE
        ];
        Set<Id> salesPersonIds = new Set<Id>();
        for (ServiceResource serviceResource : serviceResources) {
            salesPersonIds.add(serviceResource.RelatedRecordId);
        }
        return salesPersonIds;
    }

    private static String getDayOfWeekForThisDate(String dateString) {
        Date selectedDate = Date.valueOf(dateString);
        List<String> dayOfWeek = new List<String>{
                'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
        };
        return dayOfWeek.get(selectedDate.toStartOfWeek().daysBetween(selectedDate));
    }

    @TestVisible
    private static String getFormattedTimeString(Time timeValue) {
        String timeString = '';
        if (timeValue.hour() == 0) {
            timeString += '12';
        } else if (timeValue.hour() > 12) {
            timeString += '' + (timeValue.hour() - 12);
        } else {
            timeString += timeValue.hour();
        }
        timeString += ':';
        if (timeValue.minute() < 10) {
            timeString += '0' + timeValue.minute();
        } else {
            timeString += timeValue.minute();
        }
        timeString += ' ';
        timeString += timeValue.hour() < 12 ? 'AM' : 'PM';
        return timeString;
    }

    public static List<TimeSlot> getAvailableAppointmentTimeSlots(Id opportunityId, String selectedDateString) {
        String dayOfWeek = getDayOfWeekForThisDate(selectedDateString);
        List<TimeSlot> availableAppointmentTimeSlots = new List<TimeSlot>();
        if (String.isNotBlank(dayOfWeek)) {
            String dealerId = [SELECT Id, Dealer_ID__c FROM Opportunity WHERE Id = :opportunityId]?.Dealer_ID__c;

            Id operatingHoursId = [
                    SELECT Id, OperatingHoursId
                    FROM ServiceTerritory
                    WHERE Dealer__c = :dealerId AND IsSalesTerritory__c = TRUE AND IsActive = TRUE
            ]?.OperatingHoursId;

            availableAppointmentTimeSlots = [
                    SELECT Id, DayOfWeek, StartTime, EndTime
                    FROM TimeSlot
                    WHERE OperatingHoursId = :operatingHoursId AND DayOfWeek = :dayOfWeek
                    ORDER BY StartTime
            ];
        }
        return availableAppointmentTimeSlots;
    }

    private static void tossPlatformEvent(String parentRecordId) {
        Future_Appointment_Event__e event = new Future_Appointment_Event__e(
            Parent_Record_Id__c = parentRecordId
        );

        EventBus.publish(event);
    }

    public class PicklistValues {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }

        public PicklistValues(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

    public class EventInfo {
        @AuraEnabled
        public String appointmentId { get; set; }
        @AuraEnabled
        public String opportunityId { get; set; }
        @AuraEnabled
        public String eventType { get; set; }
        @AuraEnabled
        public String eventDate { get; set; }
        @AuraEnabled
        public String eventStartTime { get; set; }
        @AuraEnabled
        public String saleManagerId { get; set; }
        @AuraEnabled
        public String salesPersonId { get; set; }
        @AuraEnabled
        public String eventNotes { get; set; }
        @AuraEnabled
        public Boolean doNotSendEmail { get; set; }
        @AuraEnabled
        public String toEmail { get; set; }
        @AuraEnabled
        public String ccEmail { get; set; }

        public EventInfo() {
        }

        public EventInfo(String opportunityId, String eventType, String eventDate, String eventStartTime, String saleManagerId,
                String salesPersonId, String eventNotes, Boolean doNotSendEmail, String toEmail, String ccEmail) {
            this.opportunityId = opportunityId;
            this.eventType = eventType;
            this.eventDate = eventDate;
            this.eventStartTime = eventStartTime;
            this.saleManagerId = saleManagerId;
            this.salesPersonId = salesPersonId;
            this.eventNotes = eventNotes;
            this.doNotSendEmail = doNotSendEmail;
            this.toEmail = toEmail;
            this.ccEmail = ccEmail;
        }
    }

    public class EventCreatedInfo {
        @AuraEnabled
        public Boolean isEventCreated { get; set; }
        @AuraEnabled
        public Boolean isEventUpdated { get; set; }
        @AuraEnabled
        public String errorMessage { get; set; }

        public EventCreatedInfo(Boolean isEventCreated, Boolean isEventUpdated, String errorMessage) {
            this.isEventCreated = isEventCreated;
            this.isEventUpdated = isEventUpdated;
            this.errorMessage = errorMessage;
        }
    }
}