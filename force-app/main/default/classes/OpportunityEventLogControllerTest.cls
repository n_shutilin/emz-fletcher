@IsTest
private class OpportunityEventLogControllerTest {
    private final static String DEALER_ID = 'AUDFJ';

    @TestSetup
    private static void testSetup() {
        OperatingHours operatingHours = new OperatingHours();
        operatingHours.Name = 'Test Sales Operating Hours';
        operatingHours.TimeZone = 'America/Los_Angeles';
        operatingHours.Dealer_Code__c = DEALER_ID;
        insert operatingHours;

        TimeSlot mondayTimeSlot = new TimeSlot();
        mondayTimeSlot.DayOfWeek = 'Monday';
        mondayTimeSlot.StartTime = Time.newInstance(7, 0, 0, 0);
        mondayTimeSlot.EndTime = Time.newInstance(8, 0, 0, 0);
        mondayTimeSlot.OperatingHoursId = operatingHours.Id;
        insert mondayTimeSlot;

        TimeSlot tuesdayTimeSlot = new TimeSlot();
        tuesdayTimeSlot.DayOfWeek = 'Tuesday';
        tuesdayTimeSlot.StartTime = Time.newInstance(10, 0, 0, 0);
        tuesdayTimeSlot.EndTime = Time.newInstance(12, 0, 0, 0);
        tuesdayTimeSlot.OperatingHoursId = operatingHours.Id;
        insert tuesdayTimeSlot;

        Id testDealerId = createServiceTerritory('Test dealer', operatingHours.Id, true, true);
        Id testDealerNoSalesId = createServiceTerritory('Test dealer No Sales', operatingHours.Id, false, true);
        Id testDealerNoActiveId = createServiceTerritory('Test dealer No Active', operatingHours.Id, true, false);

        RecordType personAccountRecordType = [
                SELECT Id
                FROM RecordType
                WHERE Name = 'Person Account'
                AND SobjectType = 'Account'
        ];

        Account testAccount = new Account();
        testAccount.FirstName = 'test Account firstName';
        testAccount.LastName = 'test Account lastName';
        testAccount.RecordTypeId = personAccountRecordType.Id;
        testAccount.PersonEmail = 'testAccount@email.com';
        insert testAccount;

        Opportunity opportunity = new Opportunity();
        opportunity.AccountId = testAccount.Id;
        opportunity.Name = 'Test Opportunity';
        opportunity.StageName = 'Initial Interest';
        opportunity.CloseDate = Date.today().addDays(10);
        opportunity.Dealer_ID__c = DEALER_ID;
        insert opportunity;

        // create Sale Managers
        User saleManager = createUser('saleManager');
        Id saleManagerResourceId = createServiceResource(saleManager, 'Sale Manager', true);
        createServiceTerritoryMember(testDealerId, saleManagerResourceId, Datetime.now().addDays(-1), null);

        User saleManagerNotWorking = createUser('saleManagerNotWorking');
        Id saleManagerResourceNotWorkingId = createServiceResource(saleManagerNotWorking, 'Sale Manager', true);
        createServiceTerritoryMember(testDealerId, saleManagerResourceNotWorkingId, Datetime.now().addDays(-10), Datetime.now().addDays(-1));

        // create Sales Persons
        User salesPerson = createUser('salesPerson');
        Id salesPersonResource = createServiceResource(salesPerson, 'Salesperson', true);
        createServiceTerritoryMember(testDealerId, salesPersonResource, Datetime.now().addDays(-1), null);

        User salesPersonWithManagerId = createUserSalesPerson('salesPersonManagerId', saleManager.Id);
        Id salesPersonWithManagerIdResource = createServiceResource(salesPersonWithManagerId, 'Salesperson', true);
        createServiceTerritoryMember(testDealerId, salesPersonWithManagerIdResource, Datetime.now().addDays(-1), null);

        User salesPersonNotWorking = createUserSalesPerson('salesPersonNotWorking', saleManager.Id);
        Id salesPersonResourceNotWorkingId = createServiceResource(salesPersonNotWorking, 'Salesperson', true);
        createServiceTerritoryMember(testDealerId, salesPersonResourceNotWorkingId, Datetime.now().addDays(-10), Datetime.now().addDays(-1));

    }

    @IsTest
    private static void getPersonAccountEmailTest() {
        Id opportunityId = [SELECT Id, Name FROM Opportunity LIMIT 1].Id;
        Test.startTest();
        String personEmail = OpportunityEventLogController.getPersonAccountEmail(opportunityId);
        Test.stopTest();

        System.assertEquals('testAccount@email.com'.toLowerCase(), personEmail);
    }

    @IsTest
    private static void getPersonAccountEmailIfOpportunityIdIsNullTest() {
        Test.startTest();
        String personEmail = OpportunityEventLogController.getPersonAccountEmail(null);
        Test.stopTest();

        System.assertEquals(null, personEmail);
    }

    @IsTest
    private static void getAppointmentTypesTest() {
        List<OpportunityEventLogController.PicklistValues> picklistValues = OpportunityEventLogController.getAppointmentTypes();
        System.assertNotEquals(picklistValues.size(), 0);
    }

    @IsTest
    private static void getSalesManagerNamesTest() {
        Id opportunityId = [SELECT Id, Name FROM Opportunity LIMIT 1].Id;

        Test.startTest();
        List<OpportunityEventLogController.PicklistValues> salesManagerNames = OpportunityEventLogController.getSalesManagerNames(opportunityId);
        Test.stopTest();

        User saleManager = [SELECT Id, Username FROM User WHERE Username = 'saleManager@test31.com' LIMIT 1];
        System.assertEquals(1, salesManagerNames.size());
        System.assertEquals(saleManager.Username, salesManagerNames[0].label.toLowerCase());
        System.assertEquals(saleManager.Id, salesManagerNames[0].value);

    }

    @IsTest
    private static void getSalesManagerNamesIfOpportunityIdIsNullTest() {
        Test.startTest();
        List<OpportunityEventLogController.PicklistValues> salesManagerNames = OpportunityEventLogController.getSalesManagerNames(null);
        Test.stopTest();

        System.assertEquals(true, salesManagerNames.isEmpty());
    }

    @IsTest
    private static void getSalesPersonNamesTest() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id;
        User saleManager = [SELECT Id, Username FROM User WHERE Username = 'saleManager@test31.com' LIMIT 1];
        Test.startTest();
        List<OpportunityEventLogController.PicklistValues> salesPersonNames = OpportunityEventLogController.getSalesPersonNames(
                opportunityId,
                saleManager.Id
        );
        Test.stopTest();

        User salesPerson = [SELECT Id, Username FROM User WHERE Username = 'salespersonmanagerid@test31.com' LIMIT 1];
        System.assertNotEquals(0, salesPersonNames.size());
        //System.assertEquals(salesPerson.Username, salesPersonNames[0].label.toLowerCase());
        //System.assertEquals(salesPerson.Id, salesPersonNames[0].value);
    }

    @IsTest
    private static void getSalesPersonNamesIfOpportunityIdIsNullTest() {
        Test.startTest();
        List<OpportunityEventLogController.PicklistValues> salesPersonNames = OpportunityEventLogController.getSalesPersonNames(
                null,
                null
        );
        Test.stopTest();
        System.assertEquals(true, salesPersonNames.isEmpty());
    }

    @IsTest
    private static void getSalesPersonNamesIfSaleManagerIsNullTest() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id;
        Test.startTest();
        List<OpportunityEventLogController.PicklistValues> salesPersonNames = OpportunityEventLogController.getSalesPersonNames(
                opportunityId,
                null
        );
        Test.stopTest();

        List<User> expectedSalesPersons = [
                SELECT Id, Username
                FROM User
                WHERE Username IN ('salesPerson@test31.com', 'salesPersonManagerId@test31.com')
        ];
        System.assertEquals(2, salesPersonNames.size());
        System.assertEquals(expectedSalesPersons[0].Username, salesPersonNames[0].label.toLowerCase());
        System.assertEquals(expectedSalesPersons[0].Id, salesPersonNames[0].value);
        System.assertEquals(expectedSalesPersons[1].Username, salesPersonNames[1].label.toLowerCase());
        System.assertEquals(expectedSalesPersons[1].Id, salesPersonNames[1].value);
    }

    @IsTest
    private static void isAvailableDayOfWeekForAppointmentTest() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id;

        Boolean isDayAvailableForAppointment;

        String mondayDate = String.valueOf(Date.today().toStartOfWeek().addDays(8));
        isDayAvailableForAppointment = OpportunityEventLogController.isAvailableDayOfWeekForAppointment(
                opportunityId,
                mondayDate
        );
        System.assertEquals(true, isDayAvailableForAppointment);

        String tuesdayDate = String.valueOf(Date.today().toStartOfWeek().addDays(9));
        isDayAvailableForAppointment = OpportunityEventLogController.isAvailableDayOfWeekForAppointment(
                opportunityId,
                tuesdayDate
        );
        System.assertEquals(true, isDayAvailableForAppointment);

        String sundayDate = String.valueOf(Date.today().toStartOfWeek());
        isDayAvailableForAppointment = OpportunityEventLogController.isAvailableDayOfWeekForAppointment(
                opportunityId,
                sundayDate
        );
        System.assertEquals(false, isDayAvailableForAppointment);

        String olderDate = String.valueOf(Date.today().addDays(-1));
        isDayAvailableForAppointment = OpportunityEventLogController.isAvailableDayOfWeekForAppointment(
                opportunityId,
                olderDate
        );
        System.assertEquals(false, isDayAvailableForAppointment);

        Boolean checkWithNulls = OpportunityEventLogController.isAvailableDayOfWeekForAppointment(
                null,
                null
        );
        System.assertEquals(false, checkWithNulls);

        Boolean checkIfOpportunityIdIsNull = OpportunityEventLogController.isAvailableDayOfWeekForAppointment(
                null,
                String.valueOf(Date.today().toStartOfWeek().addDays(3))
        );
        System.assertEquals(false, checkIfOpportunityIdIsNull);

        Boolean checkIfDateIsNull = OpportunityEventLogController.isAvailableDayOfWeekForAppointment(
                opportunityId,
                null
        );
        System.assertEquals(false, checkIfDateIsNull);
    }

    @IsTest
    private static void getAvailableTimePerDayAppointmentTest() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id;

        List<OpportunityEventLogController.PicklistValues> availableTimeSlots;

        availableTimeSlots = OpportunityEventLogController.getAvailableTimePerDayAppointment(
                null,
                null
        );
        System.assertEquals(true, availableTimeSlots.isEmpty());

        availableTimeSlots = OpportunityEventLogController.getAvailableTimePerDayAppointment(
                opportunityId,
                null
        );
        System.assertEquals(true, availableTimeSlots.isEmpty());

        availableTimeSlots = OpportunityEventLogController.getAvailableTimePerDayAppointment(
                null,
                String.valueOf(Date.today().toStartOfWeek().addDays(3))
        );
        System.assertEquals(true, availableTimeSlots.isEmpty());

        String mondayDate = String.valueOf(Date.today().toStartOfWeek().addDays(1));
        availableTimeSlots = OpportunityEventLogController.getAvailableTimePerDayAppointment(
                opportunityId,
                mondayDate
        );
        System.assertEquals(4, availableTimeSlots.size());
        System.assertEquals('7:00 AM', availableTimeSlots[0].label);
        System.assertEquals('07:00:00.000Z', availableTimeSlots[0].value);

        String tuesdayDate = String.valueOf(Date.today().toStartOfWeek().addDays(1));
        availableTimeSlots = OpportunityEventLogController.getAvailableTimePerDayAppointment(
                opportunityId,
                tuesdayDate
        );
        System.assertEquals(4, availableTimeSlots.size());

        String sundayDate = String.valueOf(Date.today().toStartOfWeek());
        availableTimeSlots = OpportunityEventLogController.getAvailableTimePerDayAppointment(
                opportunityId,
                sundayDate
        );
        System.assertEquals(true, availableTimeSlots.isEmpty());

        String olderDate = String.valueOf(Date.today().addDays(-1));
        availableTimeSlots = OpportunityEventLogController.getAvailableTimePerDayAppointment(
                opportunityId,
                olderDate
        );
        System.assertEquals(false, availableTimeSlots.isEmpty());
    }

    @IsTest
    private static void createEventTest() {
        Opportunity opportunity = [SELECT Id, Dealer_ID__c, Account.PersonContactId FROM Opportunity LIMIT 1];
        User saleManager = [SELECT Id, Username FROM User WHERE Username = 'saleManager@test31.com' LIMIT 1];
        User salesPerson = [SELECT Id, Username FROM User WHERE Username = 'salespersonmanagerid@test31.com' LIMIT 1];
        final String testAppointmentType = 'In Showroom';
        final String testNoticeString = 'Test Notice text';
        final String testToEmail = 'exampleTO@email.com';
        final String testCCEmail = 'exampleCC@email.com';
        final Date eventDate = Date.today().addDays(5);
        final Time eventStartTime = Time.newInstance(7, 30, 0, 0);

        OpportunityEventLogController.EventInfo eventInfo = new OpportunityEventLogController.EventInfo(
        opportunity.Id, testAppointmentType, String.valueOf(eventDate), String.valueOf(eventStartTime), saleManager.Id,
                salesPerson.Id, testNoticeString, true, testToEmail, testCCEmail);

        Test.startTest();
        OpportunityEventLogController.EventCreatedInfo eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        Test.stopTest();

        System.assertEquals(true, eventCreatedInfo.isEventCreated);
        Event createdEvent = [
                SELECT Id, Subject, DealerId__c, Appointment_Type__c, StartDateTime, EndDateTime, OwnerId,
                        Salesperson__c, Description, WhatId, WhoId, DoNotSendEmail__c, TO_Email__c, CC_Email__c
                FROM Event
                LIMIT 1
        ];
        System.assertEquals('In Showroom with (salesPersonManagerId@test31.com)', createdEvent.Subject);
        System.assertEquals(opportunity.Dealer_ID__c, createdEvent.DealerId__c);
        System.assertEquals(eventDate, createdEvent.StartDateTime.date());
        System.assertEquals(eventStartTime, createdEvent.StartDateTime.time());
        System.assertEquals(eventStartTime.addMinutes(15), createdEvent.EndDateTime.time());
        System.assertEquals(saleManager.Id, createdEvent.OwnerId);
        System.assertEquals(salesPerson.Id, createdEvent.Salesperson__c);
        System.assertEquals(testNoticeString, createdEvent.Description);
        System.assertEquals(opportunity.Id, createdEvent.WhatId);
        System.assertEquals(opportunity.Account.PersonContactId, createdEvent.WhoId);
        System.assertEquals(true, createdEvent.DoNotSendEmail__c);
        System.assertEquals(testToEmail.toLowerCase(), createdEvent.TO_Email__c);
        System.assertEquals(testCCEmail.toLowerCase(), createdEvent.CC_Email__c);
    }

    @IsTest
    private static void givenSaleManagerHasForthEventWithTheSameStartTimeWhenNewEventIsCreatedThanErrorShouldBeAppeared() {
        Opportunity opportunity = [SELECT Id, Dealer_ID__c, Account.PersonContactId FROM Opportunity LIMIT 1];
        User saleManager = [SELECT Id, Username FROM User WHERE Username = 'saleManager@test31.com' LIMIT 1];
        User salesPerson = [SELECT Id, Username FROM User WHERE Username = 'salespersonmanagerid@test31.com' LIMIT 1];
        final String testAppointmentType = 'In Showroom';
        final String testNoticeString = 'Test Notice text';
        final String testToEmail = 'exampleTO@email.com';
        final String testCCEmail = 'exampleCC@email.com';
        final Date eventDate = Date.today().addDays(5);
        final Time eventStartTime = Time.newInstance(7, 30, 0, 0);

        List<Event> events = new List<Event>{
                new Event(Subject = 'First Event', StartDateTime = Datetime.newInstance(eventDate, eventStartTime), OwnerId = saleManager.Id, DurationInMinutes = 15),
                new Event(Subject = 'Second Event', StartDateTime = Datetime.newInstance(eventDate, eventStartTime), OwnerId = saleManager.Id, DurationInMinutes = 15),
                new Event(Subject = 'Third Event', StartDateTime = Datetime.newInstance(eventDate, eventStartTime), OwnerId = saleManager.Id, DurationInMinutes = 15),
                new Event(Subject = 'Forth Event', StartDateTime = Datetime.newInstance(eventDate, eventStartTime), OwnerId = saleManager.Id, DurationInMinutes = 15)
        };
        insert events;

        OpportunityEventLogController.EventInfo eventInfo = new OpportunityEventLogController.EventInfo(
        opportunity.Id, testAppointmentType, String.valueOf(eventDate), String.valueOf(eventStartTime), saleManager.Id,
                salesPerson.Id, testNoticeString, true, testToEmail, testCCEmail);

        Test.startTest();
        OpportunityEventLogController.EventCreatedInfo eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        Test.stopTest();

        System.assertEquals(false, eventCreatedInfo.isEventCreated);
        System.assertEquals(OpportunityEventLogController.ERROR_MESSAGE_CURRENT_SALES_MANAGER_IS_NOT_AVAILABLE_IN_THAT_TIME,
                eventCreatedInfo.errorMessage);
        List<Event> createdEvents = [SELECT Id FROM Event];
        System.assertEquals(4, createdEvents.size());
    }

    @IsTest
    private static void givenSaleManagerHasEventWithStatusBusyInThatTimeWhenNewEventIsCreatedThanErrorShouldBeAppeared() {
        Opportunity opportunity = [SELECT Id, Dealer_ID__c, Account.PersonContactId FROM Opportunity LIMIT 1];
        User saleManager = [SELECT Id, Username FROM User WHERE Username = 'saleManager@test31.com' LIMIT 1];
        User salesPerson = [SELECT Id, Username FROM User WHERE Username = 'salespersonmanagerid@test31.com' LIMIT 1];
        final String testAppointmentType = 'In Showroom';
        final String testNoticeString = 'Test Notice text';
        final String testToEmail = 'exampleTO@email.com';
        final String testCCEmail = 'exampleCC@email.com';
        Date eventDate = Date.today().addDays(5);

        Time newEventStartTime = Time.newInstance(7, 15, 0, 0);
        Time busyEventStartTime = Time.newInstance(7, 20, 0, 0);
        Time busyEventEndTime = Time.newInstance(10, 0, 0, 0);
        Event event = new Event(
                Subject = 'Event with busy status',
                StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime),
                EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime),
                OwnerId = saleManager.Id,
                Busy__c = true
        );
        insert event;

        OpportunityEventLogController.EventInfo eventInfo = new OpportunityEventLogController.EventInfo(
        opportunity.Id, testAppointmentType, String.valueOf(eventDate), String.valueOf(newEventStartTime),
                saleManager.Id, salesPerson.Id, testNoticeString, true, testToEmail, testCCEmail);

        OpportunityEventLogController.EventCreatedInfo eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);

        // new Event time (7:15-7:30) overlaps with 'busy' Event time (7:20-10:00) - new Event is NOT allowed
        System.assertEquals(false, eventCreatedInfo.isEventCreated);
        System.assertEquals(OpportunityEventLogController.ERROR_MESSAGE_CURRENT_SALES_MANAGER_IS_NOT_AVAILABLE_IN_THAT_TIME,
                eventCreatedInfo.errorMessage);
        List<Event> createdEvents = [SELECT Id FROM Event];
        System.assertEquals(1, createdEvents.size());

//         new Event time (7:15-7:30) overlaps with 'busy' Event time (7:05-7:10) - new Event is NOT allowed
        busyEventStartTime = Time.newInstance(7, 5, 0, 0);
        busyEventEndTime = Time.newInstance(7, 10, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(7, 0, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.assertEquals(false, eventCreatedInfo.isEventCreated);

        //         new Event time (7:15-7:30) overlaps with 'busy' Event time (7:05-7:15) - new Event is NOT allowed
        busyEventStartTime = Time.newInstance(7, 5, 0, 0);
        busyEventEndTime = Time.newInstance(7, 15, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(7, 0, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.assertEquals(false, eventCreatedInfo.isEventCreated);

        // new Event time (8:00-8:15) overlaps with 'busy' Event time (7:20-10:00) - new Event is NOT allowed
        busyEventStartTime = Time.newInstance(7, 20, 0, 0);
        busyEventEndTime = Time.newInstance(10, 0, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(8, 0, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.assertEquals(false, eventCreatedInfo.isEventCreated);

        // new Event time (9:45-10:00) overlaps with 'busy' Event time (7:20-10:00) - new Event is NOT allowed
        busyEventStartTime = Time.newInstance(7, 20, 0, 0);
        busyEventEndTime = Time.newInstance(10, 0, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(9, 45, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.assertEquals(false, eventCreatedInfo.isEventCreated);

        // new Event time (8:00-8:15) overlaps with 'busy' Event time (7:20-8:10) - new Event is NOT allowed
        busyEventStartTime = Time.newInstance(7, 20, 0, 0);
        busyEventEndTime = Time.newInstance(8, 10, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(8, 0, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.assertEquals(false, eventCreatedInfo.isEventCreated);

        // new Event time (7:00-7:15) overlaps with 'busy' Event time (7:00-7:10) - new Event is NOT allowed
        busyEventStartTime = Time.newInstance(7, 0, 0, 0);
        busyEventEndTime = Time.newInstance(7, 10, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(7, 0, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.assertEquals(false, eventCreatedInfo.isEventCreated);

        // new Event time (7:00-7:15) overlaps with 'busy' Event time (7:00-8:00) - new Event is NOT allowed
        busyEventStartTime = Time.newInstance(7, 0, 0, 0);
        busyEventEndTime = Time.newInstance(8, 0, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(7, 0, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.assertEquals(false, eventCreatedInfo.isEventCreated);

        // new Event time (7:00-7:15) overlaps with 'busy' Event time (7:15-10:00) - new Event is allowed
        busyEventStartTime = Time.newInstance(7, 15, 0, 0);
        busyEventEndTime = Time.newInstance(10, 0, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(7, 0, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.assertEquals(true, eventCreatedInfo.isEventCreated);

        // new Event time (10:00-10:15) overlaps with 'busy' Event time (7:15-10:00) - new Event is allowed
        busyEventStartTime = Time.newInstance(7, 15, 0, 0);
        busyEventEndTime = Time.newInstance(10, 0, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(10, 0, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.debug(eventCreatedInfo.errorMessage);
        System.assertEquals(true, eventCreatedInfo.isEventCreated);

        // new Event time (10:00-10:15) overlaps with 'busy' Event time (7:15-9:00) - new Event is allowed
        busyEventStartTime = Time.newInstance(7, 15, 0, 0);
        busyEventEndTime = Time.newInstance(9, 0, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(10, 0, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.debug(eventCreatedInfo.errorMessage);
        System.assertEquals(true, eventCreatedInfo.isEventCreated);

        // new Event time (10:00-10:15) overlaps with 'busy' Event time (11:00-12:00) - new Event is allowed
        busyEventStartTime = Time.newInstance(11, 0, 0, 0);
        busyEventEndTime = Time.newInstance(12, 0, 0, 0);
        event.StartDateTime = Datetime.newInstance(eventDate, busyEventStartTime);
        event.EndDateTime = Datetime.newInstance(eventDate, busyEventEndTime);
        update event;
        newEventStartTime = Time.newInstance(10, 0, 0, 0);
        eventInfo.eventStartTime = String.valueOf(newEventStartTime);
        eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        System.debug(eventCreatedInfo.errorMessage);
        System.assertEquals(true, eventCreatedInfo.isEventCreated);
    }

    @IsTest
    private static void createEventWithDMLErrorTest() {
        Opportunity opportunity = [SELECT Id, Dealer_ID__c, Account.PersonContactId FROM Opportunity LIMIT 1];
        final Date eventDate = Date.today().addDays(5);
        final Time eventStartTime = Time.newInstance(7, 30, 0, 0);

        OpportunityEventLogController.EventInfo eventInfo = new OpportunityEventLogController.EventInfo(
        opportunity.Id, null, String.valueOf(eventDate), String.valueOf(eventStartTime), null,
                null, null, null, null, null);

        Test.startTest();
        OpportunityEventLogController.EventCreatedInfo eventCreatedInfo = OpportunityEventLogController.createEvent(eventInfo);
        Test.stopTest();

        System.assertEquals(false, eventCreatedInfo.isEventCreated);
        System.assert(String.isNotBlank(eventCreatedInfo.errorMessage));
        System.assertEquals(true, [SELECT Id FROM Event].isEmpty());
    }

    @IsTest
    private static void getFormattedTimeStringTest() {
        String formattedTimeString;

        formattedTimeString = OpportunityEventLogController.getFormattedTimeString(Time.newInstance(0, 5, 0, 0));
        System.assertEquals('12:05 AM', formattedTimeString);

        formattedTimeString = OpportunityEventLogController.getFormattedTimeString(Time.newInstance(7, 15, 30, 40));
        System.assertEquals('7:15 AM', formattedTimeString);

        formattedTimeString = OpportunityEventLogController.getFormattedTimeString(Time.newInstance(12, 30, 0, 0));
        System.assertEquals('12:30 PM', formattedTimeString);

        formattedTimeString = OpportunityEventLogController.getFormattedTimeString(Time.newInstance(17, 45, 0, 0));
        System.assertEquals('5:45 PM', formattedTimeString);

        formattedTimeString = OpportunityEventLogController.getFormattedTimeString(Time.newInstance(23, 59, 0, 0));
        System.assertEquals('11:59 PM', formattedTimeString);
    }

    private static Id createServiceTerritory(String dealerName, Id operatingHoursId, Boolean isSalesTerritory, Boolean isActive) {
        ServiceTerritory testDealer = new ServiceTerritory();
        testDealer.Name = dealerName;
        testDealer.Dealer__c = DEALER_ID;
        testDealer.IsSalesTerritory__c = isSalesTerritory;
        testDealer.IsActive = isActive;
        testDealer.OperatingHoursId = operatingHoursId;
        insert testDealer;
        return testDealer.Id;
    }

    private static Id createServiceResource(User user, String salesResourceType, Boolean isActive) {
        ServiceResource serviceResource = new ServiceResource();
        serviceResource.Name = user.Username;
        serviceResource.RelatedRecordId = user.Id;
        serviceResource.Sales_Resource_Type__c = salesResourceType;
        serviceResource.IsActive = isActive;
        insert serviceResource;
        return serviceResource.Id;
    }

    private static void createServiceTerritoryMember(Id dealerId, Id serviceResourceId, Datetime startDate, Datetime endDate) {
        ServiceTerritoryMember saleManagerTerritoryMember = new ServiceTerritoryMember();
        saleManagerTerritoryMember.ServiceTerritoryId = dealerId;
        saleManagerTerritoryMember.EffectiveStartDate = startDate;
        saleManagerTerritoryMember.EffectiveEndDate = endDate;
        saleManagerTerritoryMember.ServiceResourceId = serviceResourceId;
        insert saleManagerTerritoryMember;
    }

    private static User createUserSalesPerson(String userName, Id managerId) {
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User userOrg = new User(Alias = 'standt', Email = userName + '@test31.com', EmailEncodingKey = 'UTF-8', LastName = userName + '@test31.com', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = profile.Id, TimeZoneSidKey = 'America/Los_Angeles', Username = userName + '@test31.com', ManagerId = managerId);
        insert userOrg;
        return userOrg;
    }

    private static User createUser(String userName) {
        return createUserSalesPerson(userName, null);
    }
}