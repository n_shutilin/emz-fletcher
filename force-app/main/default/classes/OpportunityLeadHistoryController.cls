public with sharing class OpportunityLeadHistoryController {

  // the Opportunity record being viewed
  public Opportunity opportunity { get; set; }
	
  // the Lead record that created this Opportunity record
  public Lead convertedLead {
		get {
			if(convertedLead == null) {
				List<Lead> convertedLeads = [select Id from Lead where ConvertedOpportunityId = :opportunity.Id];
				if(!convertedLeads.isEmpty()) convertedLead = convertedLeads[0];
			}
			return convertedLead;
		}
		set;
	}

  // the Lead field history data
	public List<LeadHistory> leadHistory {
		get {
			if(leadHistory == null && convertedLead != null) {
				leadHistory = [select OldValue, NewValue, LeadId, IsDeleted, Id, Field, CreatedDate, CreatedById From LeadHistory where LeadId = :convertedLead.Id order by CreatedDate asc];
			}
			return leadHistory;
		}
		set;
	}
	
  // the constructor code
	public OpportunityLeadHistoryController(ApexPages.StandardController controller) {
		opportunity = (Opportunity)controller.getRecord();
	}
	
}