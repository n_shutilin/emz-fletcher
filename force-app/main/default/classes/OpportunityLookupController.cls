public with sharing class OpportunityLookupController {
    public class SearchResult {
        @AuraEnabled public String id;
        @AuraEnabled public String name;

        public SearchResult(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    @AuraEnabled
    public static List<SearchResult> search(String searchString, String dealer){

        List<SearchResult> resultList = new List<SearchResult>();
        searchString += '*';

        String query = 'FIND {' + searchString + '*} ' +
            'IN ALL FIELDS ' +
            'RETURNING ' +
            'Opportunity (Id, Name  WHERE Dealer_ID__c = \''+ dealer + '\') ' +
            'LIMIT 6';
        List<List<SObject>> searchResults = Search.query(query);

        List<SObject> objects = (List<SObject>) searchResults[0];
        for (SObject obj : objects) {
            resultList.add(
                new SearchResult(
                    (String)obj.get('Id'),
                    (String)obj.get('Name')
                )
            );
        }
        return resultList;
    }
}