@IsTest
public class OpportunityLookupControllerTest {

    @TestSetup
    static void init(){
        
    }

    @IsTest
    static void searchTest(){
        OpportunityLookupController.SearchResult testResult = new OpportunityLookupController.SearchResult(null, null);
        List<OpportunityLookupController.SearchResult> result = OpportunityLookupController.search('test', 'AUDFJ');
        System.assert(result.isEmpty());
    }
}