public class PartsSpecialOrderParser {
    public static final String NAMESPACE = 'http://www.dmotorworks.com/pip-extract-parts-special-order';
    public static final String CHILD_NAME = 'SpecialOrder';

    public static List<PartsSpecialOrder__c> process(Dom.Document doc, String accountId) {
        List<PartsSpecialOrder__c> orders = new List<PartsSpecialOrder__c>();
        String dealerCode = [SELECT Id, CDK_Dealer_Code__c FROM Account WHERE Id = :accountId].CDK_Dealer_Code__c;
        System.debug('dealerCode ' + dealerCode);
        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == CHILD_NAME) {
                System.debug('child ' + child);
                PartsSpecialOrder__c order = new PartsSpecialOrder__c();
                mapFields(order, child);
                order.UniqueOrderId__c = dealerCode + '_' + order.SpecialOrderNo__c;  
                System.debug('order ' + order);
                orders.add(order);
            }
        }

        return orders;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'HostItemID' => PartsSpecialOrder__c.HostItemID__c,
            'SpecialOrderNo' => PartsSpecialOrder__c.SpecialOrderNo__c,
            'CustNo' => PartsSpecialOrder__c.CustNo__c,
            'CustomerName' => PartsSpecialOrder__c.CustomerName__c,
            'CustomerName2' => PartsSpecialOrder__c.CustomerName2__c,
            'CustomerAddress' => PartsSpecialOrder__c.CustomerAddress__c,
            'CustomerCSZ' => PartsSpecialOrder__c.CustomerCSZ__c,
            'HomePhone' => PartsSpecialOrder__c.HomePhone__c,
            'BusinessPhone' => PartsSpecialOrder__c.BusinessPhone__c,
            'OpenDate' => PartsSpecialOrder__c.OpenDate__c,
            'Comment1' => PartsSpecialOrder__c.Comment1__c,
            'Comment2' => PartsSpecialOrder__c.Comment2__c,
            'PONumber' => PartsSpecialOrder__c.PONumber__c,
            'ClosedDate' => PartsSpecialOrder__c.ClosedDate__c,
            'Origin' => PartsSpecialOrder__c.Origin__c,
            'VehID' => PartsSpecialOrder__c.VehID__c,
            'ServiceAdvisor' => PartsSpecialOrder__c.ServiceAdvisor__c,
            'CustomerAddress2' => PartsSpecialOrder__c.CustomerAddress2__c,
            'CellPhone' => PartsSpecialOrder__c.CellPhone__c,
            'ErrorLevel' => PartsSpecialOrder__c.ErrorLevel__c,
            'ErrorMessage' => PartsSpecialOrder__c.ErrorMessage__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, NAMESPACE);

            System.debug('~~ ' + childElement);
            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}