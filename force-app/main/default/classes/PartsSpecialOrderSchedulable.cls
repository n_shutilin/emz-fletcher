global class PartsSpecialOrderSchedulable implements Schedulable{
    global void execute(SchedulableContext SC) {
        PartsSpecialOrderBatchable batch = new PartsSpecialOrderBatchable();
        Database.executeBatch(batch, 1);

        // Datetime nextScheduleTime = System.now().addHours(12);
        // String hour = String.valueOf(nextScheduleTime.hour());
        // String minutes = String.valueOf(nextScheduleTime.minute());
        // String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        // String jobName = 'Parts Special Order Extract ' + nextScheduleTime.format('hh:mm');

        // PartsSpecialOrderSchedulable p = new PartsSpecialOrderSchedulable();
        // System.schedule(jobName, cronValue , p);

        // System.abortJob(SC.getTriggerId());
    }
}