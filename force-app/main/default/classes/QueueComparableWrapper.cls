public with sharing class QueueComparableWrapper implements Comparable{
    public String Id;
    public Decimal position;
    public Decimal upTime;
    public Decimal totalOnPositionTime;
    public Decimal totalTime;
    public Boolean isActualState;
    public Decimal inQueueUpTime;
    public String serviceResourceId;
    public String queueId;
    public Time startTime;
    public Decimal elapsedTime;
    public Decimal beforeBreakElapsedTime = 0;
    public Decimal beforeBreakPosition = 0;
    public Decimal beforeBreakUpTime = 0;
    public String resourceName;

    public QueueComparableWrapper(){

    }

    public QueueComparableWrapper(Resource_State_In_Queue__c state) {
        this.Id = state.Id;
        this.position = state.Position__c;
        this.upTime = state.Up_time__c;
        this.totalOnPositionTime = state.Total_On_Position_Time__c;
        this.totalTime = state.Total_Time__c;
        this.isActualState = state.Is_Actual_State__c;
        this.inQueueUpTime = state.In_Queue_Up_Time__c;
        this.serviceResourceId = state.Service_Resource_Id__c;
        this.queueId = state.Queue_Id__c;
        this.startTime = state.Start_Time__c;
        this.elapsedTime = state.Elapsed_time__c;
        this.beforeBreakElapsedTime = state.Before_Break_Elapsed_Time__c;
        this.beforeBreakPosition = state.Before_Break_Position__c;
        this.beforeBreakUpTime = state.Before_Break_UpTime__c;
        this.resourceName = state.Service_Resource_Id__r.Name;
    }

    public Integer compareTo (Object compareTo) {
        QueueComparableWrapper temp = (QueueComparableWrapper) compareTo;
        if(this.position == temp.position){
            return 0;
        } 
        if(this.position > 0 && temp.position > 0){
            return Integer.valueOf(this.position - temp.position);
        }
        if(this.position < 0 && temp.position > 0){
            return 1;
        }
        if(this.position > 0 && temp.position < 0){
            return -1;
        }
        else {
            return 0;
        }
    }
}