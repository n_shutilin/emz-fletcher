global with sharing class QueueContainer implements Schedulable {
    private static final Integer SECOND_MS = 1000;

    global QueueContainer() {
    }

    global void execute(SchedulableContext context) {
        List<Queue__c> queueList = [SELECT Id, isActive__c, Last_Rows_Data_Update__c, Name, Creation_Date__c, Start_Time__c, End_Time__c 
                                    FROM Queue__c
                                    WHERE isActive__c = true];
        List<Queue__c> expiredQueueList = [SELECT Id, isActive__c, Last_Rows_Data_Update__c, Name, Creation_Date__c, Start_Time__c, End_Time__c 
                                           FROM Queue__c
                                           WHERE isActive__c = false];
        if(!queueList.isEmpty() || !expiredQueueList.isEmpty()){
            if(!queueList.isEmpty()){
                processActiveQueues(queueList);
            } 
            if(!expiredQueueList.isEmpty()) {
                processExpiredQueues(expiredQueueList);
            }
            reschedule(context);
        } else {
            System.abortJob(context.getTriggerId());
        }
    }

    private void processActiveQueues(List<Queue__c> activeQueueList){
        for(Queue__c queue : activeQueueList){
            if(DateTime.now() >= defineQueueEndTime(queue)) {
                QueueManagerController.closeQueue(queue.Id);
            } else if(DateTime.now() >= buildDateTimeFromTime(queue.Start_Time__c)){
                runBatch(queue);
            }
        }
    }

    private void processExpiredQueues(List<Queue__c> expiredQueueList){
        for(Queue__c queue : expiredQueueList){
            QueueManagerController.deleteQueueWithData(queue.Id);
        }
    }


    private void reschedule(SchedulableContext context){
        Datetime nextScheduleTime = System.now().addMinutes(1);
        String hour = String.valueOf(nextScheduleTime.hour());
        String minutes = String.valueOf(nextScheduleTime.minute());
        String seconds = String.valueOf(nextScheduleTime.second());
        String cronExpression = seconds + ' ' 
                              + minutes + ' ' 
                              + hour + ' * * ?' ;
        String jobName = 'Up System\'s container. Id:' + context.getTriggerId();
        QueueContainer container = new QueueContainer();
        System.abortJob(context.getTriggerId());
        System.schedule(jobName, cronExpression , container);
    }

    private void runBatch(Queue__c queue){
        Database.executeBatch(new QueueContainerBatch(queue)); 
    }

    public static DateTime buildDateTimeFromTime(Time timeToBuild){
        Date today = Date.today();
        return DateTime.newInstanceGmt(today, timeToBuild);
    }

    public static DateTime defineQueueEndTime(Queue__c queue){
        DateTime startDateTime = buildDateTimeFromTime(queue.Start_Time__c);
        DateTime endDateTime = buildDateTimeFromTime(queue.End_Time__c);
        if(endDateTime < startDateTime){
            endDateTime = endDateTime.addDays(1);
        }
        return endDateTime;
    }
}