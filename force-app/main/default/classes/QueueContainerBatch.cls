public with sharing class QueueContainerBatch implements Database.batchable<Resource_State_In_Queue__c>, Database.RaisesPlatformEvents{

    private Queue__c queue;

    public QueueContainerBatch(Queue__c queue) {
        this.queue = queue;
    }

    public List<Resource_State_In_Queue__c> start(Database.BatchableContext BC){
        Up_System_Platform_Event__e event = new Up_System_Platform_Event__e(
            Type__c = 'Batch started.'
        );
        EventBus.publish(event);
        return [SELECT Id, Position__c, Up_time__c,Total_On_Position_Time__c, Total_Time__c, Is_Actual_State__c, 
                       In_Queue_Up_Time__c, Service_Resource_Id__c, Queue_Id__c, Start_Time__c, Elapsed_time__c, Status__c,
                       Before_Break_Elapsed_Time__c, Before_Break_Position__c, Before_Break_UpTime__c, Service_Resource_Id__r.Name, Entered_Queue__c
                FROM Resource_State_In_Queue__c
                WHERE Queue_Id__c = :this.queue.Id ORDER BY Position__c ASC];
     }
   
    public void execute(Database.BatchableContext BC, List<Resource_State_In_Queue__c> stateList){
        if(!stateList.isEmpty()){
            QueueTimeLogic timeLogic = new QueueTimeLogic();
            stateList = timeLogic.incrementInQueueTime(this.queue, stateList);
            upsert stateList;
            update this.queue;
        }
    }

    public void finish(Database.BatchableContext BC){
        String id = this.queue.Id;
        Up_System_Platform_Event__e event = new Up_System_Platform_Event__e(
            Type__c = 'Update.',
            Queue_Id__c = id
        );
        EventBus.publish(event);
    }
}