@IsTest
public class QueueContainerTest {
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    @TestSetup
    static void init() {
        UpSystemTestDataProvider provider = new UpSystemTestDataProvider('FRMBN');
    }

    @IsTest
    static void queueContainerTest() {
        Test.startTest();
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP,
            new QueueContainer());
        Test.stopTest();
    }

    @IsTest
    static void upSystemAutomationJobTest() {
        Test.startTest();
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP,
            new UpSystemAutomationJob());
        Test.stopTest();
    }

    @IsTest
    static void queueCreationBatchTest() {
        QueueCreationBatch batch = new QueueCreationBatch('FRMBN');
        Test.startTest();
        ServiceTerritory territory = [SELECT Id, IsSalesTerritory__c FROM ServiceTerritory LIMIT 1];
        territory.IsSalesTerritory__c = true;
        update territory;
        Database.executeBatch(batch);
        Test.stopTest();
        batch.validateTime('11:22 AM');
    }
}