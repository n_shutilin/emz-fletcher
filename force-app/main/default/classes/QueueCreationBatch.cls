public class QueueCreationBatch implements  Database.Batchable<sObject>{
    private static Integer HOUR_IN_MS = 3600000;
    private static final String SALES_TYPE = 'Sales';
    private static final String SERVICE_TYPE = 'Service';
    private static final String FINANCE_TYPE = 'Finance';
    private String dealer;

    public QueueCreationBatch(String dealer) {
        this.dealer = dealer;
    }

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
              'SELECT Id, Dealer__c, IsSalesTerritory__c, OperatingHours.Timezone ' +
            + 'FROM ServiceTerritory ' + 
            + 'WHERE Dealer__c =\''+ this.dealer +'\'');
    }

    public void execute(Database.BatchableContext bc, List<ServiceTerritory> territoryList){
        Up_System_Automation_Settings__mdt salesSettings = getSettings(SALES_TYPE);
        Up_System_Automation_Settings__mdt serviceSettings = getSettings(SERVICE_TYPE);
        Up_System_Automation_Settings__mdt financeSettings = getSettings(FINANCE_TYPE);
        for(ServiceTerritory territory : territoryList) {
            String name = getQueueName(territory);
            switch on name {
                when  'Sales Territory'{
                    Time startTimeGMT = formTimeToGMT(QueueTimeUtils.parseTime(salesSettings.Start_Time__c), territory.OperatingHours.Timezone);
                    Time endTimeGMT = formTimeToGMT(QueueTimeUtils.parseTime(salesSettings.End_Time__c), territory.OperatingHours.Timezone);
                    QueueManagerController.insertQueue(
                        null, territory.Id, getQueueName(territory), this.dealer, 
                        startTimeGMT, endTimeGMT, salesSettings.Up_Time__c,
                        salesSettings.Revert_Time__c, salesSettings.Revert_To_UpNext_Time__c);
                }
                when else {
                    Time startTimeGMT = formTimeToGMT(QueueTimeUtils.parseTime(serviceSettings.Start_Time__c), territory.OperatingHours.Timezone);
                    Time endTimeGMT = formTimeToGMT(QueueTimeUtils.parseTime(serviceSettings.End_Time__c), territory.OperatingHours.Timezone);
                    QueueManagerController.insertQueue(
                        null, territory.Id, getQueueName(territory), this.dealer, 
                        startTimeGMT, endTimeGMT, serviceSettings.Up_Time__c,
                        serviceSettings.Revert_Time__c, serviceSettings.Revert_To_UpNext_Time__c);
                }
            }

        }
    }

    public void finish(Database.BatchableContext bc){
        
    }

    public Time formTimeToGMT(Time actualTime, String timeZone) {
        Integer hoursShift = getTimeZoneDifferenceWithGMTInHours(timeZone);
        return actualTime.addHours(-1 * hoursShift);
    }

    public Integer getTimeZoneDifferenceWithGMTInHours(String timezone) {
        Timezone zone = System.TimeZone.getTimeZone(timezone);
        Datetime currentTimeInGmt = Datetime.now();
        return zone.getOffset(currentTimeInGmt)/HOUR_IN_MS;
    }

    public String getQueueName(ServiceTerritory territory) {
        return territory.IsSalesTerritory__c ? 'Sales Territory' : 'Service Territory';
    }

    public Up_System_Automation_Settings__mdt getSettings(String type) {
        String name = this.dealer + '_' + type;
        List<Up_System_Automation_Settings__mdt> dealerSettingList = [
            SELECT Start_Time__c, End_Time__c, Revert_Time__c, Revert_To_UpNext_Time__c, Up_Time__c
            FROM Up_System_Automation_Settings__mdt
            WHERE DeveloperName = :name
        ];
        if(dealerSettingList.isEmpty()) {
            return getDefaultSettings().get(0);
        } 
        if(!isSettingsValid(dealerSettingList.get(0))) {
            return getDefaultSettings().get(0);
        }
        return dealerSettingList.get(0);
    }

    public List<Up_System_Automation_Settings__mdt> getDefaultSettings() {
        return [
            SELECT Start_Time__c, End_Time__c, Revert_Time__c, Revert_To_UpNext_Time__c, Up_Time__c
            FROM Up_System_Automation_Settings__mdt
            WHERE DeveloperName = 'default'
        ];
    }

    public Boolean isSettingsValid(Up_System_Automation_Settings__mdt settings) {
        return validateTime(settings.Start_Time__c) && validateTime(settings.End_Time__c);
    }

    public Boolean validateTime(String timeString) {
        String[] parsedString = timeString.split(' ');
        if(parsedString.size() != 2) {
            return false;
        } else {
            parsedString = parsedString[0].split(':');
            if(parsedString.size() != 2) {
                return false;
            } else {
                try {
                    Integer hours = Integer.valueOf(parsedString[0]);
                    Integer minutes = Integer.valueOf(parsedString[1]);
                    return (hours >= 0 && hours <= 12) && (minutes >= 0 && minutes < 60);
                } catch (Exception e) {
                    return false;
                }
            }
        }
    }
}