@IsTest
public class QueueManagerControllerTest {

    @TestSetup
    static void init(){
        UpSystemTestDataProvider provider = new UpSystemTestDataProvider('FRMBN');
    }

    @isTest
    static void getUserInfoByIdTest() {
        QueueManagerController.getUserInfoById(UserInfo.getUserId());
    }

    @isTest
    static void pairRecordTest() {
        ServiceResource resource = [SELECT Id 
                                    FROM ServiceResource LIMIT 1];
        Queue__c queue = [SELECT Id 
                          FROM Queue__c LIMIT 1];
        QueueManagerController.pairRecords(resource.Id, resource.Id, queue.Id, null);
        QueueManagerController.unPairRecords(resource.Id, resource.Id, queue.Id, null);
    }
    
    @isTest
    static void pairRecordListTest() {
        ServiceResource resource = [SELECT Id 
                                    FROM ServiceResource LIMIT 1];
        Queue__c queue = [SELECT Id 
                          FROM Queue__c LIMIT 1];
        QueueManagerController.pairRecords(resource.Id, resource.Id, queue.Id, null);
        QueueManagerController.unPairRecords(resource.Id, new List<String>{resource.Id}, queue.Id, null);
    }
    
    @isTest
    static void getDealerTest(){
        QueueManagerController.getDealer(UserInfo.getUserId());
    }

    @IsTest
    static void newQueueTest(){
        Test.startTest();
        QueueManagerController.insertQueue(
            'queueName', 
            'FRMBN', 
            '11:15:54.000Z', 
            '15:00:00.000Z',
            UserInfo.getUserId(), 
            '180', 
            '3', 
            '3');
        Test.stopTest();
    }

    @IsTest
    static void insertNewQueue(){
        ServiceResource resource = [SELECT Id 
                                    FROM ServiceResource LIMIT 1];   
        ServiceTerritory territory = [SELECT Id
                                      FROM ServiceTerritory LIMIT 1];     
        Test.startTest();
        QueueManagerController.insertQueue(
            resource.Id,
            territory.Id,
            'queueName', 
            'FRMBN', 
            Time.newInstance(0, 0, 0, 0),
            Time.newInstance(12, 0, 0, 0), 
            18, 
            3, 
            3);
        Test.stopTest();
    }

    @IsTest
    static void getTimeSlotTest() {
        QueueManagerController.getTimeSlotByUserId(UserInfo.getUserId(), 'Monday');
    }

    @IsTest
    static void isUserInSalesTerrytoryTest() {
        QueueManagerController.isUserInSalesTerrytory(UserInfo.getUserId());
    }

    @IsTest
    static void updateQueueDataTest() {
        Queue__c queue = [SELECT Id 
                          FROM Queue__c LIMIT 1];
        QueueManagerController.updateQueueData(queue.Id, '10', '10', '10', '10');
    }

    @IsTest
    static void closeQueueTest(){
        Queue__c queue = [SELECT Id 
                          FROM Queue__c LIMIT 1];
        QueueManagerController.closeQueue(queue.Id);
    }

    @IsTest
    static void deleteQueueWithDataTest(){
        Queue__c queue = [SELECT Id 
                          FROM Queue__c LIMIT 1];
        QueueManagerController.deleteQueueWithData(queue.Id);
    }

    @IsTest
    static void createInQueueActivity(){
        ServiceResource resource = [SELECT Id 
                                    FROM ServiceResource LIMIT 1];
        QueueManagerController.createInQueueActivity(resource.Id, 'lead', null, null);
        QueueManagerController.createInQueueActivity(resource.Id, 'test', null, null);
        QueueManagerController.createInQueueActivity(resource.Id, 'opportunity', null, null);
        QueueManagerController.createInQueueActivity(resource.Id, 'account', null, null);
    }

    @IsTest
    static void getResourcedByUserIdTest(){
        QueueManagerController.getResourcedByUserId(UserInfo.getUserId(), 'Daily');
    }

    @IsTest
    static void getQueueDataByIdTest(){
        Queue__c queue = [SELECT Id 
                          FROM Queue__c LIMIT 1];
        QueueManagerController.getQueueDataById(queue.Id);
    }

    @IsTest
    static void getQueuesByUserIdTest() {
        Queue__c queue = [SELECT Id 
                          FROM Queue__c LIMIT 1];
        QueueManagerController.getQueuesByUserId(UserInfo.getUserId());        
    }

    @IsTest
    static void createShoowroomRecordTest() {
        ServiceResource resource = [SELECT Id 
                                    FROM ServiceResource LIMIT 1];
        QueueManagerController.createShoowroomRecord(resource.Id); 
        Opportunity opportunity = new Opportunity(
            Name = 'Test',
            StageName = 'Working',
            CloseDate = Date.today());
        insert opportunity;
        QueueManagerController.linkOpportunityToShowroom(resource.Id, opportunity.Id, 'FRMBN'); 
    }
}