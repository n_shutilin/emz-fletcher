public with sharing class QueueTimeLogic {

    public QueueTimeLogic() {

    }

    public void executeRowsUpdateLogic(String serializedRows, String queueId){
        updateLastQueueUpdateTime(queueId);
        List<ResourceWrapper.ResourceDataWrapper> rows = (List<ResourceWrapper.ResourceDataWrapper>)JSON.deserialize(serializedRows, List<ResourceWrapper.ResourceDataWrapper>.class);
        List<String> rowsIdList = formRowsIdList(rows);
        List<Resource_State_In_Queue__c> stateList = [SELECT Service_Resource_Id__c, In_Queue_Up_Time__c, Is_actual_state__c,Total_On_Position_Time__c, Elapsed_time__c, 
                                                                 Position__c, Up_time__c, Status__c, Total_Time__c, Start_Time__c,
                                                                 Before_Break_Elapsed_Time__c,Before_Break_Position__c, Before_Break_UpTime__c
                                                      FROM Resource_State_In_Queue__c
                                                      WHERE Queue_Id__c = :queueId AND Service_Resource_Id__c IN :rowsIdList];
        List<Resource_State_In_Queue__c> stateToRemoveList = [SELECT Service_Resource_Id__c, In_Queue_Up_Time__c, Is_actual_state__c,Total_On_Position_Time__c, Elapsed_time__c, 
                                                                 Position__c, Up_time__c, Status__c, Total_Time__c, Start_Time__c,
                                                                 Before_Break_Elapsed_Time__c,Before_Break_Position__c, Before_Break_UpTime__c
                                                             FROM Resource_State_In_Queue__c
                                                             WHERE Queue_Id__c = :queueId AND Service_Resource_Id__c NOT IN :rowsIdList AND Is_actual_state__c = true];
        for(ResourceWrapper.ResourceDataWrapper row : rows){
            if(!findState(stateList, row)){
                disableState(row.newPosition, row.resourceId, stateList); //try to disable exists this state or not
                Resource_State_In_Queue__c state = new Resource_State_In_Queue__c();
                state.Service_Resource_Id__c = row.resourceId;
                state.Queue_Id__c = row.queueId;
                state.Is_actual_state__c = true;
                state.Elapsed_time__c = 0;
                state.Position__c = row.newPosition;
                state.Up_time__c = row.newPosition > 0 ? row.inQueueUpTime : -1;
                state.Status__c = row.status;
                state.Total_Time__c = row.totalTime;
                state.Start_Time__c = row.startTime;
                state.Entered_Queue__c = row.enteredQueue;
                state.Before_Break_Elapsed_Time__c = row.beforeBreakStatement.elapsedTime;
                state.Before_Break_Position__c = row.beforeBreakStatement.position;
                state.Before_Break_UpTime__c = row.beforeBreakStatement.upTime;
                state.In_Queue_Up_Time__c = row.inQueueUpTime;
                stateList.add(state);
            }
        }
        upsert stateList;
        if(!stateToRemoveList.isEmpty()){
            for(Resource_State_In_Queue__c stateToRemove : stateToRemoveList){
                stateToRemove.Is_actual_state__c = false;
            }
            upsert stateToRemoveList;
        }
    }

    public void splitRows(List<ResourceWrapper.ResourceDataWrapper> mainRows, 
                                  List<ResourceWrapper.ResourceDataWrapper> changedPositionRows, 
                                  List<ResourceWrapper.ResourceDataWrapper> samePositionRows){
        for(ResourceWrapper.ResourceDataWrapper row : mainRows){
            if(row.newPosition == row.oldPosition){
                samePositionRows.add(row);
            } else {
                changedPositionRows.add(row);
            }
        }
    }

    public List<String> formRowsIdList(List<ResourceWrapper.ResourceDataWrapper> rowList){
        List<String> idList = new List<String>();
        for(ResourceWrapper.ResourceDataWrapper row : rowList){
            idLIst.add(row.resourceId);
        }
        return idList;
    }   

    public void updateLastQueueUpdateTime(String queueId){
        Queue__c queue = [SELECT Id, Last_Rows_Data_Update__c 
                          FROM Queue__c
                          WHERE Id = :queueId];
        queue.Last_Rows_Data_Update__c = DateTime.now();
        update queue;
    }

    public  Resource_State_In_Queue__c formNewState(ResourceWrapper.ResourceDataWrapper row, Boolean isActual){
        Resource_State_In_Queue__c state = new Resource_State_In_Queue__c();
        state.Service_Resource_Id__c = row.resourceId;
        state.Queue_Id__c = row.queueId;
        state.Is_actual_state__c = isActual;
        state.Elapsed_time__c = row.newOnPositionTime;
        state.Position__c = row.newPosition;
        state.Up_time__c = row.upTime;
        state.Status__c = row.status;
        state.Total_Time__c = row.totalTime;
        state.Start_Time__c = row.startTime;
        state.Before_Break_Elapsed_Time__c = row.beforeBreakStatement.elapsedTime;
        state.Before_Break_Position__c = row.beforeBreakStatement.position;
        state.Before_Break_UpTime__c = row.beforeBreakStatement.upTime;
        state.In_Queue_Up_Time__c = row.inQueueUpTime;
        state.Entered_Queue__c = row.enteredQueue;
        return state;
    }

    public List<Resource_State_In_Queue__c> incrementInQueueTime(Queue__c queue, List<Resource_State_In_Queue__c> resourceList){
        if(isFirstPositionUpTimeExpired(resourceList)){
            recalculatePositions(queue.Id, resourceList);
            queue.Last_Rows_Data_Update__c = DateTime.now();
        } else {
            for(Resource_State_In_Queue__c resource : resourceList){
                if(resource.Is_Actual_State__c){
                    if(resource.Position__c == 1){
                        resource.Up_time__c--;
                    }
                    incrementTime(resource);
                }
            }
        }
        return resourceList;
    }

    public Resource_State_In_Queue__c formNewState(Resource_State_In_Queue__c resource, Decimal position){
        Resource_State_In_Queue__c state = new Resource_State_In_Queue__c();
        state.Service_Resource_Id__c = resource.Service_Resource_Id__c;
        state.Queue_Id__c = resource.Queue_Id__c;
        state.Is_Actual_State__c = true;
        state.Elapsed_time__c = 0;
        state.Position__c = position;
        state.Up_time__c = resource.In_Queue_Up_Time__c;
        state.Total_Time__c = resource.Total_Time__c;
        state.Start_Time__c = getStartTime();
        state.Total_On_Position_Time__c = 0;
        state.Status__c = defineStatusByPosition(position);
        state.Before_Break_Elapsed_Time__c = 0;
        state.Before_Break_Position__c = 0;
        state.Before_Break_UpTime__c = 0;
        state.In_Queue_Up_Time__c = resource.In_Queue_Up_Time__c;
        state.Entered_Queue__c = resource.Entered_Queue__c;
        return state;
    }

    private Boolean findState(List<Resource_State_In_Queue__c> stateList, ResourceWrapper.ResourceDataWrapper row){
        Boolean isJunctionExists = false;
        for(Resource_State_In_Queue__c state : stateList){
            if(state.Service_Resource_Id__c == row.resourceId && state.Position__c == row.newPosition){
                if(!state.Is_actual_state__c){
                    state.Is_Actual_State__c = true;
                    state.Up_time__c = row.newPosition > 0 ? row.inQueueUpTime : -1;
                    state.In_Queue_Up_Time__c = row.inQueueUpTime;
                    state.Status__c = row.status;
                    state.Elapsed_time__c = 0;
                    state.Start_Time__c = row.startTime;
                    state.Entered_Queue__c = row.enteredQueue;
                    disableState(row.newPosition, row.resourceId, stateList);
                } else {
                    state.Elapsed_time__c = row.newOnPositionTime;
                    state.Status__c = row.status;
                    state.Start_Time__c = row.startTime;
                    state.Up_time__c = row.newPosition > 0 ? row.upTime : -1;
                    state.Entered_Queue__c = row.enteredQueue;
                    state.In_Queue_Up_Time__c = row.inQueueUpTime;
                }
                isJunctionExists = true;
                break;
            }
        }
        return isJunctionExists;
    }

    private void disableState(Decimal position, String id, List<Resource_State_In_Queue__c> stateList){
        for(Resource_State_In_Queue__c state : stateList){
            if(state.Position__c != position && state.Service_Resource_Id__c == id){
                state.Is_actual_state__c = false;
            }
        }
    }

    private void recalculatePositions (String queueId, List<Resource_State_In_Queue__c> resourceList) {
        List<Resource_State_In_Queue__c> tempList = new List<Resource_State_In_Queue__c>();
        if(getMaxPosition(queueId) == 1){
            for(Resource_State_In_Queue__c resource : resourceList){
                if(resource.Is_Actual_State__c){
                    if(resource.Position__c == 1){
                        incrementTime(resource);
                        resource.Up_time__c = resource.In_Queue_Up_Time__c;
                    } else if(resource.Position__c < 0){
                        incrementTime(resource);
                    }
                }
            }
        } else {
            String oldFirstPosition;
            for(Resource_State_In_Queue__c resource : resourceList){
                if(resource.Is_Actual_State__c){
                    if(resource.Position__c == 1){
                        resource.Up_time__c--;
                        incrementTime(resource);
                        resource.Is_Actual_State__c = false;
                        oldFirstPosition = resource.Service_Resource_Id__c;
                        Decimal lastPosition = getMaxPosition(queueId);
                        if(isStateExists(resourceList, lastPosition, resource.Service_Resource_Id__c)){
                            for(Resource_State_In_Queue__c innerResource : resourceList){
                                if((innerResource.Position__c == lastPosition) && (innerResource.Service_Resource_Id__c == resource.Service_Resource_Id__c)){
                                    activateState(innerResource, resource);
                                }
                            }
                        } else {
                            tempList.add(formNewState(resource, lastPosition));
                        }
                    } else if(resource.Position__c > 1 && resource.Service_Resource_Id__c != oldFirstPosition){
                        incrementTime(resource);
                        resource.Is_Actual_State__c = false;
                        Decimal newPosition = resource.Position__c - 1;
                        if(isStateExists(resourceList, newPosition, resource.Service_Resource_Id__c)){
                            for(Resource_State_In_Queue__c innerResource : resourceList){
                                if(innerResource.Position__c == newPosition && innerResource.Service_Resource_Id__c == resource.Service_Resource_Id__c){
                                    activateState(innerResource, resource);
                                }
                            }
                        } else {
                            tempList.add(formNewState(resource, newPosition));
                        }
                    } else if(resource.Position__c < 0){
                        incrementTime(resource);
                    }
                }
            }
            if(!tempList.isEmpty()){
                resourceList.addAll(tempList);
            }
        }
    }

    private void activateState(Resource_State_In_Queue__c newResource, Resource_State_In_Queue__c oldResource){
        newResource.Is_Actual_State__c = true;
        newResource.Up_time__c = oldResource.In_Queue_Up_Time__c;
        newResource.Elapsed_time__c = 0;
        newResource.Start_Time__c = getStartTime();
    }

    private void incrementTime(Resource_State_In_Queue__c resource){
        resource.Total_On_Position_Time__c++;
        resource.Elapsed_time__c++;
        resource.Total_Time__c++;
    }

    private String defineStatusByPosition(Decimal position){
        if(position == 1){
            return 'Up Next';
        } else {
            return 'In Queue';
        }
    }

    private Time getStartTime(){
        DateTime now = DateTime.now();
        return Time.newInstance(now.hourGmt(), now.minuteGmt(), now.secondGmt(), 0000);
    }

    private Boolean isFirstPositionUpTimeExpired(List<Resource_State_In_Queue__c> resourceList){
        Boolean isExpired = false;
        for(Resource_State_In_Queue__c resource : resourceList){
            if(resource.Is_Actual_State__c && resource.Position__c == 1 && resource.Up_time__c <=1){
                isExpired = true;
            }
        }
        return isExpired;
    }

    private Decimal getMaxPosition(String queueId){
        List<AggregateResult> aggResults  = [SELECT MAX(Position__c)maxPosition 
                                             FROM Resource_State_In_Queue__c
                                             WHERE Queue_Id__c = :queueId AND Is_actual_state__c = true];
        return Decimal.valueOf(Integer.valueOf(aggResults[0].get('maxPosition')));
    }

    private Boolean isStateExists(List<Resource_State_In_Queue__c> resourceList, Decimal newPosition, String resourceId){
        Boolean isExists = false;
        for(Resource_State_In_Queue__c resource : resourceList){
            if((resource.Service_Resource_Id__c == resourceId) && (resource.Position__c == newPosition)){
                isExists = true;
            }
        }
        return isExists;
    }
}