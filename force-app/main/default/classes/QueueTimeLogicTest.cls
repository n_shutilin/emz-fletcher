@IsTest
public class QueueTimeLogicTest {

    @TestSetup
    static void init(){
        UpSystemTestDataProvider provider = new UpSystemTestDataProvider('FRMBN');
    }

    @IsTest
    static void executeRowsUpdateLogicTest() {
        ServiceResource resource = [SELECT Id 
                                    FROM ServiceResource LIMIT 1];
        Queue__c queue = [SELECT Id 
                          FROM Queue__c LIMIT 1];

        String rowStateJSON = '[{' +
                            + '"resourceId": "RESOURCE_ID",' +
                            + '"name": "Test",' +
                            + '"pictureUrl": "https://fletcherjones--data--c.documentforce.com/profilephoto/005/F",' +
                            + '"queueId": "QUEUE_ID",' +
                            + '"oldPosition": 2,' +
                            + '"newPosition": 1,' +
                            + '"newOnPositionTime": 0,' +
                            + '"totalTime": 0,' +
                            + '"startTime": "12:31:10.000Z",' +
                            + '"enteredQueue": "12:31:05.000Z",' +
                            + '"status": "Up Next",' +
                            + '"upTime": 180,' +
                            + '"inQueueUpTime": 180,' +
                            + '"beforeBreakStatement": {' +
                                + '"upTime": 0,' +
                                + '"position": 0,' +
                                + '"elapsedTime": 0' +
                                + '}' +
                            + '},{' +
                            + '"resourceId": "RESOURCE_ID",' +
                            + '"name": "Test",' +
                            + '"pictureUrl": "https://fletcherjones--data--c.documentforce.com/profilephoto/005/F",' +
                            + '"queueId": "QUEUE_ID",' +
                            + '"oldPosition": 3,' +
                            + '"newPosition": 3,' +
                            + '"newOnPositionTime": 0,' +
                            + '"totalTime": 0,' +
                            + '"startTime": "12:31:10.000Z",' +
                            + '"enteredQueue": "12:31:05.000Z",' +
                            + '"status": "Up Next",' +
                            + '"upTime": 180,' +
                            + '"inQueueUpTime": 180,' +
                            + '"beforeBreakStatement": {' +
                                + '"upTime": 0,' +
                                + '"position": 0,' +
                                + '"elapsedTime": 0' +
                                + '}' +
                            + '}]';
        rowStateJSON = rowStateJSON.replaceAll('RESOURCE_ID', resource.Id);
        rowStateJSON = rowStateJSON.replaceAll('QUEUE_ID', queue.Id);
        System.debug(rowStateJSON);
        QueueTimeLogic handler = new QueueTimeLogic();
        handler.executeRowsUpdateLogic(rowStateJSON, queue.Id);
    }

    @IsTest
    static void formNewStateTest(){
        Resource_State_In_Queue__c resourceState = [SELECT Id, Service_Resource_Id__c, Queue_Id__c, Is_Actual_State__c, Elapsed_time__c,
                                                           Position__c, Up_time__c, Total_Time__c, Start_Time__c,Total_On_Position_Time__c, 
                                                           Before_Break_Elapsed_Time__c, Before_Break_Position__c, Before_Break_UpTime__c,
                                                           In_Queue_Up_Time__c, Entered_Queue__c
                                                    FROM Resource_State_In_Queue__c LIMIT 1];
        ResourceWrapper.ResourceDataWrapper row = new ResourceWrapper.ResourceDataWrapper();
        QueueWrapper.State state = new QueueWrapper.State();
        row.beforeBreakStatement = state;
        QueueTimeLogic handler = new QueueTimeLogic();
        handler.formNewState(row, true);
        handler.formNewState(resourceState, 2);
    }

    @IsTest
    static void recalculateSingleTest(){
        Resource_State_In_Queue__c state = [SELECT Id 
                                            FROM Resource_State_In_Queue__c WHERE Position__c <> 1 LIMIT 1];
        delete state;
        Queue__c queue = [SELECT Id 
                          FROM Queue__c LIMIT 1];
        QueueTimeLogic handler = new QueueTimeLogic();                  
        handler.incrementInQueueTime(queue, [SELECT Id, Position__c, Up_time__c,Total_On_Position_Time__c, Total_Time__c, Is_Actual_State__c, 
                                                       In_Queue_Up_Time__c, Service_Resource_Id__c, Queue_Id__c, Start_Time__c, Elapsed_time__c, Status__c,
                                                       Before_Break_Elapsed_Time__c, Before_Break_Position__c, Before_Break_UpTime__c, Service_Resource_Id__r.Name, Entered_Queue__c
                                                FROM Resource_State_In_Queue__c]);
    }
}