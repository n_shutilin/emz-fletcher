public with sharing class QueueTimeUtils {

    private static final Integer INDEX_FIRST = 0;
    private static final Integer INDEX_SECOND = 1;
    private static final Integer INDEX_THIRD = 2;

    private QueueTimeUtils() {
    }

    public static Time parseTime(String value){
        String [] temp = value.split(' ');
        String [] hoursMinutes = temp[INDEX_FIRST].split(':');
        String period = temp[INDEX_SECOND];
        Integer hours = Integer.valueOf(period == 'PM' ? Integer.valueOf(hoursMinutes[INDEX_FIRST]) + 12 : Integer.valueOf(hoursMinutes[INDEX_FIRST]));
        Integer minutes = Integer.valueOf(hoursMinutes[INDEX_SECOND]);
        return Time.newInstance(hours, minutes, 0, 0);
    }

    public static String formTime(Time value){
        Datetime myDateTime = Datetime.newInstance(Date.today(), value);
        return myDateTime.format('h:mm a');
    }

    public static Time parseFormattedTime(String value){
        String[] strTimeSplit = value.split(':');
        String seconds = strTimeSplit[INDEX_THIRD].replace('.000Z','');
        return Time.newInstance(
            Integer.valueOf(strTimeSplit[INDEX_FIRST]),
            Integer.valueOf(strTimeSplit[INDEX_SECOND]), 
            Integer.valueOf(seconds),0); 
    }
}