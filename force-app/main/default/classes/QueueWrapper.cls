public with sharing class QueueWrapper {
    public String name;
    public String id;
    public String dealer;
    public Time endTime;
    public Time startTime;
    public DateTime lastRowsDataUpdateTime;
    public Decimal upTime;
    public Decimal upNextTime;
    public Decimal revertTime;
    private List<InQueueRowWrapper> rows;
    public Boolean isExpired;

    public QueueWrapper(){
        this.rows = new List<InQueueRowWrapper>();
    }

    public void addRow(ResourceWrapper.ResourceDataWrapper row){
        InQueueRowWrapper wrapper = new InQueueRowWrapper();
        wrapper.id = row.resourceId;
        wrapper.position = row.oldPosition;
        wrapper.prevPosition = row.oldPosition;
        wrapper.name = row.name;
        wrapper.pictureUrl = row.pictureUrl;
        wrapper.status = row.status;
        wrapper.upTime = row.upTime;
        wrapper.gmtStartTime = row.startTime;
        wrapper.gmtEnteredQueue = row.enteredQueue;
        wrapper.endTime = calculateEndTime(row.startTime, row.upTime, row.newOnPositionTime);
        wrapper.elapsedTime = row.newOnPositionTime;
        wrapper.totalTime = row.totalTime;
        wrapper.inQueueUpTime = row.inQueueUpTime;
        wrapper.paired = row.paired; //TODO change
        wrapper.havePairedRecords = row.havePairedRecords; //TODO change
        fillByStatus(wrapper);
        if(row.beforeBreakStatement != null){
           wrapper.beforeBreakStatement = row.beforeBreakStatement;
        }
        this.rows.add(wrapper);
    }

    private Time calculateEndTime(Time startTime, Decimal upTime, Decimal elapsedTime){
        return startTime.addMinutes(Integer.valueOf(upTime + elapsedTime));
    }

    private void fillByStatus(InQueueRowWrapper wrapper){
        switch on wrapper.status {
            when 'Lead Suspend' {
                wrapper.withGuest = false;
                wrapper.onBreak = false;  
                wrapper.leadSuspend = true;  
            }
            when 'With Guest' {
                wrapper.withGuest = true;
                wrapper.onBreak = false;
                wrapper.leadSuspend = false;
            }
            when 'On Break' {
                wrapper.withGuest = false;
                wrapper.onBreak = true;                    
            }
            when else {
                wrapper.withGuest = false;
                wrapper.onBreak = false;  
                wrapper.leadSuspend = false;                  
            }
        }
    }

    public class InQueueRowWrapper {
        public String id;
        public Decimal position;
        public Decimal prevPosition;
        public String name;
        public String pictureUrl;
        public String status;
        public Decimal upTime;
        public Decimal inQueueUpTime;
        public Time gmtEnteredQueue;
        public String enteredQueue;
        public Time gmtStartTime;
        public String startTime;
        public Time endTime;
        public Decimal elapsedTime;
        public Decimal totalTime;
        public boolean onBreak;
        public boolean withGuest;
        public boolean leadSuspend;
        public State beforeBreakStatement;
        public List<String> paired;
        public Boolean havePairedRecords;
        public Boolean inPairing;
    }

    public class State {
        public Decimal position;
        public Decimal elapsedTime;
        public Decimal upTime;
    }
}