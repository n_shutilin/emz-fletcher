global class RepOrderFlowLauncher_Batchable implements Database.Batchable<sObject>
{
    global final String Query;
    
  global RepOrderFlowLauncher_Batchable (String q)
  {
    Query = q;
  }
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<WorkOrder> scope)
   {
       List<WorkOrder> ros = new List<WorkOrder>();
       for(WorkOrder rObj : scope)
       {       

               rObj.Auto_Trigger__c = TRUE;
               ros.add(rObj);       
             
       
       }
    if(ros.size() > 0)
      update ros;
    }
  global void finish(Database.BatchableContext BC){
  }
}