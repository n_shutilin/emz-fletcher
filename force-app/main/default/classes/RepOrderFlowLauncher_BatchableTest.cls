@isTest
private class RepOrderFlowLauncher_BatchableTest
{
    private static testMethod void myTestMethod()
    {
        WorkOrder rep = new WorkOrder(RO_Number__c = '1234', Auto_Trigger__c = False);
        insert rep;
        
         Test.startTest();
         String Query = 'Select ro.Id, ro.Auto_Trigger__c '
                 +  'From WorkOrder ro where Auto_Trigger__c=False and CreatedDate=Today';
        RepOrderFlowLauncher_Batchable ClassObj = new RepOrderFlowLauncher_Batchable(Query);
        database.executeBatch(ClassObj, 1);
        Test.stopTest();}
        
            private static testMethod void myRepOrderFlowLauncher_BatchableTest()
    {
        Test.startTest();
            Datetime dt = Datetime.now().addMinutes(1);
            String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
            RepOrderFlowLauncher_Schedulable ClassObj = new RepOrderFlowLauncher_Schedulable();
            system.schedule('Test Method', CRON_EXP, ClassObj);   
        Test.stopTest();
    }
        
             

    }