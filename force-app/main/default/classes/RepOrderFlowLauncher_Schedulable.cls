global class RepOrderFlowLauncher_Schedulable implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
            String Query = 'Select ro.Id, ro.Auto_Trigger__c '
                 +  'From WorkOrder ro where Auto_Trigger__c=False and CreatedDate=Today';
        RepOrderFlowLauncher_Batchable ClassObj = new RepOrderFlowLauncher_Batchable(Query);
        database.executeBatch(ClassObj, 1);
    }
    
    }