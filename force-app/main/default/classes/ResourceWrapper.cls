public with sharing class ResourceWrapper {
    public String name;
    public String id;
    private String dealer;
    public String pictureUrl;
    public Boolean notInUse;
    public Decimal totalInQueueTime;
    public Decimal totalOnBreakTime;
    public Decimal totalUpNextTime;
    public Decimal totalWithGuestTime;

    public ResourceWrapper(String dealer){
        this.dealer = dealer;
        this.totalInQueueTime = 0;
        this.totalOnBreakTime = 0;
        this.totalUpNextTime = 0;
        this.totalWithGuestTime = 0;
        this.notInUse = true;
    }

    public class ResourceDataWrapper {
        public String resourceId;
        public String name;
        public String pictureUrl;
        public String queueId;
        public Decimal oldPosition;
        public Decimal newPosition;
        public Decimal newOnPositionTime;
        public Decimal totalTime;
        public Time startTime;
        public Time enteredQueue;
        public String status;
        public Decimal upTime;
        public Decimal inQueueUpTime;
        public QueueWrapper.State beforeBreakStatement;
        public List<String> paired;
        public Boolean havePairedRecords;
    }
}