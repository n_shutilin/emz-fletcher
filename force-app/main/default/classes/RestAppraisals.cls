public with sharing class RestAppraisals {
    
    public static final String HEADER_ACCESS = 'Accept';
    public static final String HEADER_KEY = 'X-DT-API-KEY';
    public static final String HEADER_ACCESS_JSON = 'application/json';
    public static final String PARAM_INVENTORY = 'inv_id';
    public static final String PARAM_USER_EID = 'user_eid';
    public static final String PARAM_VIN = 'vin';
    public static final String METHOD_GET = 'GET';
    public static final String METHOD_POST = 'POST';
    public static final String URI_APPRAISALS = '/api/v1/appraisals';

    public static CDK_Integration__c IUTEGRATION_SETTING = CDK_Integration__c.getOrgDefaults();

    public static String appraisalRetrieve(String tradeId) {
        List<Trade_In__c> appraisals = [
            SELECT VIN__c, Year__c, Make__c, Model__c, Interior_Color__c, Chrome_Style_Id__c, Notes__c, Transaction_Code__c, Inventory_Plus_Id__c, Opportunity__c,
                Last_Connection_Try_Time__c, Last_Connection_Error_Message__c
            FROM Trade_In__c
            WHERE Id = :tradeId
        ];
        Trade_In__c appraisal = appraisals.get(0);
        List<Opportunity> opportunityList = [
            SELECT Type, Initial_Cost__c, Vehicle__c, AccountId, Dealer_ID__c
            FROM Opportunity 
            WHERE Id = :appraisal.Opportunity__c
            AND Dealer_ID__c != null
        ];
        Opportunity opportunityItem = opportunityList.isEmpty() ? null : opportunityList.get(0);
        if (opportunityItem == null) {
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = 'Invalid dealer id for related opportunity.';
            update appraisal;
            return 'Invalid dealer id for related opportunity.';
        }
        List<Account> dealerAccounts = [
            SELECT Inventory_Plus_Id__c
            FROM Account 
            WHERE Dealer_ID__pc = :opportunityItem.Dealer_ID__c
            AND Inventory_Plus_Id__c != null
        ];
        if (dealerAccounts.isEmpty()) {
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = 'Inventory Id not filled for this dealer.';
            update appraisal;
            return 'Inventory Id not filled for this dealer.';
        }
        
        Vehicle__c vehicleItem;
        Contact contactItem;
        if (opportunityItem != null) {
            List<Vehicle__c> vehicleList = [
                SELECT Trim__c, Mileage__c, Generic_Exterior_Color__c, Exterior_Color_Code__c, Transmission__c, Interior_Type__c
                FROM Vehicle__c 
                WHERE Id = :opportunityItem.Vehicle__c
            ];
            vehicleItem = vehicleList.isEmpty() ? null : vehicleList.get(0);
            List<Contact> contactList = [
                SELECT Email, Phone 
                FROM Contact 
                WHERE AccountId = :opportunityItem.AccountId
            ];
            contactItem = contactList.isEmpty() ? null : contactList.get(0);
        }
        HttpRequest request = new HttpRequest();
        request.setMethod(METHOD_GET);
        request.setEndpoint(
            IUTEGRATION_SETTING.Inventory_Plus_Endpoint__c + URI_APPRAISALS + '?' + PARAM_INVENTORY + '=' + dealerAccounts.get(0).Inventory_Plus_Id__c + 
            '&' + PARAM_VIN + '=' + appraisal.VIN__c
        );
        request.setHeader(HEADER_ACCESS, HEADER_ACCESS_JSON);
        request.setHeader(HEADER_KEY, IUTEGRATION_SETTING.Inventory_Plus_Api_Key__c);
        Http http = new Http();
        HttpResponse response = http.send(request);
        if (response.getStatusCode() == 200) {
            InventaryRetrieveResponseWrapper responseBody;
            try {
                responseBody = (InventaryRetrieveResponseWrapper)JSON.deserialize(response.getBody(), InventaryRetrieveResponseWrapper.class);
            } catch (Exception e) {
                appraisal.Last_Connection_Try_Time__c = Datetime.now();
                appraisal.Last_Connection_Error_Message__c = 'Apex error: ' + e.getMessage();
                update appraisal;
                return 'Apex error: ' + e.getMessage();
            }
            if (responseBody.content.isEmpty()) {
                appraisal.Last_Connection_Try_Time__c = Datetime.now();
                appraisal.Last_Connection_Error_Message__c = 'Appraisal not found on the Inventory service';
                update appraisal;
                return 'Appraisal not found on the Inventory service';
            }
            responseBody.content.get(0).parsToSObjects(appraisal, contactItem, opportunityItem, vehicleItem);
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = null;
            update appraisal;
            update contactItem;
            update opportunityItem;
            update vehicleItem;
            return 'Success';
        } else if (response.getStatusCode() == 401) {
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = 'Inventory Plus API Key expired';
            update appraisal;
            return 'Inventory Plus API Key expired';
        } else {
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = 'Geting failed. Status code: ' + response.getStatusCode();
            update appraisal;
            return 'Geting failed. Status code: ' + response.getStatusCode();
        }
    }

    public static String appraisalPost(String tradeId) {
        List<Trade_In__c> appraisals = [
            SELECT VIN__c, Year__c, Make__c, Model__c, Opportunity__r.Vehicle__r.Trim__c, Opportunity__r.Vehicle__r.Mileage__c, 
                Opportunity__r.Vehicle__r.Generic_Exterior_Color__c, Opportunity__r.Vehicle__r.Exterior_Color_Code__c, Interior_Color__c, 
                Opportunity__r.Vehicle__r.Transmission__c, Opportunity__r.Vehicle__r.Interior_Type__c, Opportunity__r.Type, Chrome_Style_Id__c, 
                CreatedBy.Name, CreatedDate, Opportunity__r.Initial_Cost__c, Notes__c, Transaction_Code__c,	Opportunity__r.AccountId, 
                Inventory_Plus_Id__c, Last_Connection_Try_Time__c, Last_Connection_Error_Message__c,
                Evaluation__c, Condition_Report__c, Recondition_Estimate__c, Opportunity__c
            FROM Trade_In__c
            WHERE Id = :tradeId
        ];
        Trade_In__c appraisal = appraisals.get(0);
        List<Contact> contactList = [
            SELECT Name, Email, Phone 
            FROM Contact 
            WHERE AccountId = :appraisal.Opportunity__r.AccountId
        ];
        List<Recondition_Estimate__c> reconditionEstimates = [
            SELECT Notes__c, Additions__c, Deductions__c
            FROM Recondition_Estimate__c
            WHERE Id = :appraisal.Recondition_Estimate__c
        ];
        Recondition_Estimate__c reconditionEstimate;
        if (!reconditionEstimates.isEmpty()) {
            reconditionEstimate = reconditionEstimates.get(0);
        }
        List<Condition_Report__c> conditionReports = [
            SELECT Mechanical_Damage__c, Theft_Recovery__c, Num_Panels_Painted__c, Police_Use__c, Recon_Exterior__c, No_Other_Issues__c, Recon_Mechanical__c, Odor__c, 
                Tires_Match__c, Title_Country__c, Replaced_Panels__c, Title_Status__c, Airbag_Light_On__c, User_Eid__c, Num_Panels_Replaced__c, Mechanical_Recon_Needed__c, 
                Factory_Buyback__c, Flood_Damaged__c, Keyless_Entry__c, Undercarriage_Rust__c, Lf_Cond__c, Lf_Dmg__c, Lf_Mfg__c, Lf_Tread__c, Lf_Wsize__c, Lr_Cond__c, 
                Lr_Dmg__c, Lr_Mfg__c, Lr_Tread__c, Lr_Wsize__c, Lri_Cond__c, Lri_Dmg__c, Lri_Mfg__c, Lri_Tread__c, Lri_Wsize__c, Rf_Cond__c, Rf_Dmg__c, Rf_Mfg__c, 
                Rf_Tread__c, Rf_Wsize__c, Rr_Cond__c, Rr_Dmg__c, Rr_Mfg__c, Rr_Tread__c, Rr_Wsize__c, Rri_Cond__c, Rri_Dmg__c, Rri_Mfg__c, Rri_Tread__c, Rri_Wsize__c, 
                Sp_Cond__c, Sp_Dmg__c, Sp_Mfg__c, Sp_Tread__c, Sp_Wsize__c
            FROM Condition_Report__c
            WHERE Id = :appraisal.Condition_Report__c
        ];
        Condition_Report__c conditionReport;
        if (!conditionReports.isEmpty()) {
            conditionReport = conditionReports.get(0);
        }
        List<Evaluation__c> evaluations = [
            SELECT Appraiser__c, Date_Updated__c, Evaluated_Value__c, Notes__c, Recondition_Value__c, Selling_Price__c
            FROM Evaluation__c
            WHERE Id = :appraisal.Evaluation__c
        ];
        Evaluation__c evaluation;
        if (!evaluations.isEmpty()) {
            evaluation = evaluations.get(0);
        }
        List<Opportunity> opportunityList = [
            SELECT Type, Initial_Cost__c, Vehicle__c, AccountId, Dealer_ID__c
            FROM Opportunity 
            WHERE Id = :appraisal.Opportunity__c
            AND Dealer_ID__c != null
        ];
        Opportunity opportunityItem = opportunityList.isEmpty() ? null : opportunityList.get(0);
        if (opportunityItem == null) {
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = 'Invalid dealer id for related opportunity.';
            update appraisal;
            return 'Invalid dealer id for related opportunity.';
        }
        List<Account> dealerAccounts = [
            SELECT Inventory_Plus_Id__c
            FROM Account 
            WHERE Dealer_ID__pc = :opportunityItem.Dealer_ID__c
            AND Inventory_Plus_Id__c != null
        ];
        if (dealerAccounts.isEmpty()) {
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = 'Inventory Id not filled for this dealer.';
            update appraisal;
            return 'Inventory Id not filled for this dealer.';
        }
        Contact contactItem = contactList.isEmpty() ? null : contactList.get(0);
        List<User> users = [SELECT Inventory_Plus_Id__c FROM User WHERE Id = :UserInfo.getUserId()];
        String userEid;
        if (!users.isEmpty() && users.get(0).Inventory_Plus_Id__c != null) {
            userEid = users.get(0).Inventory_Plus_Id__c;
        } else {
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = 'Current salesforce user is unauthorized in the Inventory Plus service';
            update appraisal;
            return 'Current salesforce user is unauthorized in the Inventory Plus service';
        }
        HttpRequest request = new HttpRequest();
        request.setMethod(METHOD_POST);
        request.setEndpoint(
            IUTEGRATION_SETTING.Inventory_Plus_Endpoint__c + URI_APPRAISALS + '?' + PARAM_INVENTORY + '=' + dealerAccounts.get(0).Inventory_Plus_Id__c + '&' + 
            PARAM_USER_EID + '=' + userEid + '&' + PARAM_VIN + '=' + appraisal.VIN__c
        );
        System.debug('Endpoint: ' + (IUTEGRATION_SETTING.Inventory_Plus_Endpoint__c + URI_APPRAISALS + '?' + PARAM_INVENTORY + '=' + dealerAccounts.get(0).Inventory_Plus_Id__c + '&' + 
            PARAM_USER_EID + '=' + userEid + '&' + PARAM_VIN + '=' + appraisal.VIN__c));
        request.setHeader(HEADER_ACCESS, HEADER_ACCESS_JSON);
        request.setHeader(HEADER_KEY, IUTEGRATION_SETTING.Inventory_Plus_Api_Key__c);
        request.setBody(JSON.serialize(new AppraisalWrapper(appraisal, contactItem, evaluation, reconditionEstimate, conditionReport)));
        Http http = new Http();
        HttpResponse response = http.send(request);
        System.debug('Status: ' + response.getStatusCode());
        System.debug('Body: ' + response.getBody());
        if (response.getStatusCode() == 200) {
            InventaryPostResponseWrapper responseBody;
            try {
                responseBody = (InventaryPostResponseWrapper)JSON.deserialize(response.getBody(), InventaryPostResponseWrapper.class);
            } catch (Exception e) {
                appraisal.Last_Connection_Try_Time__c = Datetime.now();
                appraisal.Last_Connection_Error_Message__c = 'Current salesforce user is unauthorized in the Inventory Plus service';
                update appraisal;
                return 'Apex error: ' + e.getMessage();
            }
            appraisal.Transaction_Code__c = dealerAccounts.get(0).Inventory_Plus_Id__c + responseBody.content.eid;
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = null;
            appraisal.Inventory_Plus_Id__c = responseBody.content.eid;
            update appraisal;
            return 'Success';
        } else if (response.getStatusCode() == 401) {
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = 'Inventory Plus API Key expired';
            update appraisal;
            return 'Inventory Plus API Key expired';
        } else {
            appraisal.Last_Connection_Try_Time__c = Datetime.now();
            appraisal.Last_Connection_Error_Message__c = 'Posting failed. Status code: ' + response.getStatusCode();
            update appraisal;
            return 'Posting failed. Status code: ' + response.getStatusCode();
        }
    }

    public class AppraisalWrapper {
        public String vin;
        public Integer year;
        public String make;
        public String model;
        public String trim;
        public Integer mileage;
        public String ext_generic_color;
        public String ext_oem_color;
        public String interior_color;
        public String interior_type;
        public String odometer_type;
        public String transmission;
        public String trade_type;
        public Integer chrome_style_id;
        public String created_by;
        public String date_created;
        public Integer cost_initial;
        public String notes;
        public String transaction_code;
        public ContactWrapper contact;
        public EvaluationWrapper evaluation;
        public ReconditionEstimateWrapper recondition_estimate;
        public ConditionReportWrapper condition_report;
        //public String AddressWrapper;
        //public String EvaluationWrapper;
        //public String ConditionReportWrapper;

        public AppraisalWrapper(Trade_In__c appraisal, Contact contactItem, Evaluation__c evaluation, Recondition_Estimate__c reconditionEstimate, Condition_Report__c conditionReport) {
            this.vin = appraisal.VIN__c;
            try{ this.year = appraisal.Year__c != null ? Integer.valueOf(appraisal.Year__c) : null; } catch(Exception e) {}
            this.make = appraisal.Make__c;
            this.model = appraisal.Model__c;
            this.trim = appraisal.Opportunity__r.Vehicle__r.Trim__c;
            this.mileage = appraisal.Opportunity__r.Vehicle__r.Mileage__c != null ? Integer.valueOf(appraisal.Opportunity__r.Vehicle__r.Mileage__c) : null;
            this.ext_generic_color = appraisal.Opportunity__r.Vehicle__r.Generic_Exterior_Color__c;
            this.ext_oem_color = appraisal.Opportunity__r.Vehicle__r.Exterior_Color_Code__c;
            this.interior_color = appraisal.Interior_Color__c;
            this.interior_type = appraisal.Opportunity__r.Vehicle__r.Interior_Type__c;
            this.transmission = appraisal.Opportunity__r.Vehicle__r.Transmission__c;
            this.trade_type = appraisal.Opportunity__r.Type;
            this.chrome_style_id = appraisal.Chrome_Style_Id__c != null ? Integer.valueOf(appraisal.Chrome_Style_Id__c) : null;
            this.created_by = appraisal.CreatedBy.Name;
            this.date_created = String.valueOf(appraisal.CreatedDate);
            this.cost_initial = appraisal.Opportunity__r.Initial_Cost__c != null ? Integer.valueOf(appraisal.Opportunity__r.Initial_Cost__c) : null;
            this.notes = appraisal.Notes__c;
            this.transaction_code = appraisal.Transaction_Code__c;
            if (contactItem != null) {
                this.contact = new ContactWrapper(contactItem);
            }
            if (evaluation != null) {
                this.evaluation = new EvaluationWrapper(evaluation);
            }
            if (reconditionEstimate != null) {
                this.recondition_estimate = new ReconditionEstimateWrapper(reconditionEstimate);
            }
            if (conditionReport != null) {
                this.condition_report = new ConditionReportWrapper(conditionReport);
            }

        }

        public AppraisalWrapper() {

        }

        public void parsToSObjects(Trade_In__c appraisal, Contact contactItem, Opportunity opportunityItem, Vehicle__c vehicle) {
            if (appraisal != null) {
                appraisal.VIN__c = this.vin;
                appraisal.Year__c = String.valueOf(this.year);
                appraisal.Make__c = this.make;
                appraisal.Model__c = this.model;
                appraisal.Interior_Color__c = this.interior_color;
                appraisal.Chrome_Style_Id__c = String.valueOf(this.chrome_style_id);
                appraisal.Notes__c = this.notes;
                appraisal.Transaction_Code__c = this.transaction_code;
            }
            if (contactItem != null && this.contact != null) {
                contactItem.Email = this.contact.email;
                contactItem.Phone = this.contact.phone;
            }
            if (opportunityItem != null) {
                opportunityItem.Type = this.trade_type;
                opportunityItem.Initial_Cost__c = this.cost_initial;
            }
            if (vehicle != null) {
                vehicle.Trim__c = this.trim;
                vehicle.Mileage__c = this.mileage;
                vehicle.Generic_Exterior_Color__c = this.ext_generic_color;
                vehicle.Exterior_Color_Code__c = this.ext_oem_color;
                vehicle.Interior_Type__c = this.interior_type;
                vehicle.Transmission__c = this.transmission;
            }
        }

    }

    public class ContactWrapper {
        public String name_last;
        public String name_first;
        public String email;
        public String phone;
        //public AddressWrapper address;

        public ContactWrapper(Contact contactItem) {
            this.name_last = contactItem.Name.substringBefore(' ');
            this.name_last = contactItem.Name.substringAfter(' ');
            this.email = contactItem.Email;
            this.phone = contactItem.Phone;
        }

        public ContactWrapper() {

        }
    }

    public class InventaryPostResponseWrapper {
        public List<String> messages;
        public String location;
        EidContainerWrapper content;
    }

    public class InventaryRetrieveResponseWrapper {
        public List<String> messages;
        public String location;
        List<AppraisalWrapper> content;
    }

    public class EidContainerWrapper {
        public String eid;
    }

    /*
    public class AddressWrapper {
        public String address;
        public String city;
        public String state;
        public String email;
        public String country;
        public String zip_code;
    }*/

    public class EvaluationWrapper {
        public String appraiser;
        public Integer recondition_value;
        public Integer evaluated_value;
        public Integer selling_price;
        public String date_updated;
        public String notes;

        public EvaluationWrapper(Evaluation__c evaluation) {
            this.appraiser = evaluation.Appraiser__c;
            this.recondition_value = Integer.valueOf(evaluation.Recondition_Value__c);
            this.evaluated_value = Integer.valueOf(evaluation.Evaluated_Value__c);
            this.selling_price = Integer.valueOf(evaluation.Selling_Price__c);
            this.date_updated = evaluation.Date_Updated__c;
            this.notes = evaluation.Notes__c;
        }
    }
    
    public class ReconditionEstimateWrapper {
        public String notes;
        public String additions;
        public String deductions;

        public ReconditionEstimateWrapper(Recondition_Estimate__c reconditionEstimate) {
            this.notes = reconditionEstimate.Notes__c;
            this.additions = reconditionEstimate.Additions__c;
            this.deductions = reconditionEstimate.Deductions__c;
        }
    }

    public class ConditionReportWrapper {
        public Boolean mechanical_damage;
        public Boolean theft_recovery;
        public Integer num_panels_painted;
        public Boolean police_use;
        public Double recon_exterior;
        public Boolean no_other_issues;
        public Double recon_mechanical;
        public Boolean odor;
        public Boolean tires_match;
        public String title_country;
        public Boolean replaced_panels;
        public String title_status;
        public Boolean airbag_light_on;
        public String user_eid;
        public Integer num_panels_replaced;
        public Boolean mechanical_recon_needed;
        public Boolean factory_buyback;
        public Boolean flood_damaged;
        public Boolean keyless_entry;
        public Boolean undercarriage_rust;
        public String lf_cond;
        public String lf_dmg;
        public String lf_mfg;
        public Integer lf_tread;
        public Integer lf_wsize;
        public String lr_cond;
        public String lr_dmg;
        public String lr_mfg;
        public Integer lr_tread;
        public Integer lr_wsize;
        public String lri_cond;
        public String lri_dmg;
        public String lri_mfg;
        public Integer lri_tread;
        public Integer lri_wsize;
        public String rf_cond;
        public String rf_dmg;
        public String rf_mfg;
        public Integer rf_tread;
        public Integer rf_wsize;
        public String rr_cond;
        public String rr_dmg;
        public String rr_mfg;
        public Integer rr_tread;
        public Integer rr_wsize;
        public String rri_cond;
        public String rri_dmg;
        public String rri_mfg;
        public Integer rri_tread;
        public Integer rri_wsize;
        public String sp_cond;
        public String sp_dmg;
        public String sp_mfg;
        public Integer sp_tread;
        public Integer sp_wsize;

        public ConditionReportWrapper(Condition_Report__c conditionReport) {
            this.mechanical_damage = conditionReport.Mechanical_Damage__c;
            this.theft_recovery = conditionReport.Theft_Recovery__c;
            this.num_panels_painted = Integer.valueOf(conditionReport.Num_Panels_Painted__c);
            this.police_use = conditionReport.Police_Use__c;
            this.recon_exterior = conditionReport.Recon_Exterior__c;
            this.no_other_issues = conditionReport.No_Other_Issues__c;
            this.recon_mechanical = conditionReport.Recon_Mechanical__c;
            this.odor = conditionReport.Odor__c;
            this.tires_match = conditionReport.Tires_Match__c;
            this.title_country = conditionReport.Title_Country__c;
            this.replaced_panels = conditionReport.Replaced_Panels__c;
            this.title_status = conditionReport.Title_Status__c;
            this.airbag_light_on = conditionReport.Airbag_Light_On__c;
            this.user_eid = conditionReport.User_Eid__c;
            this.num_panels_replaced = Integer.valueOf(conditionReport.Num_Panels_Replaced__c);
            this.mechanical_recon_needed = conditionReport.Mechanical_Recon_Needed__c;
            this.factory_buyback = conditionReport.Factory_Buyback__c;
            this.flood_damaged = conditionReport.Flood_Damaged__c;
            this.keyless_entry = conditionReport.Keyless_Entry__c;
            this.undercarriage_rust = conditionReport.Undercarriage_Rust__c;
            this.lf_cond = conditionReport.Lf_Cond__c;
            this.lf_dmg = conditionReport.Lf_Dmg__c;
            this.lf_mfg = conditionReport.Lf_Mfg__c;
            this.lf_tread = Integer.valueOf(conditionReport.Lf_Tread__c);
            this.lf_wsize = Integer.valueOf(conditionReport.Lf_Wsize__c);
            this.lr_cond = conditionReport.Lr_Cond__c;
            this.lr_dmg = conditionReport.Lr_Dmg__c;
            this.lr_mfg = conditionReport.Lr_Mfg__c;
            this.lr_tread = Integer.valueOf(conditionReport.Lr_Tread__c);
            this.lr_wsize = Integer.valueOf(conditionReport.Lr_Wsize__c);
            this.lri_cond = conditionReport.Lri_Cond__c;
            this.lri_dmg = conditionReport.Lri_Dmg__c;
            this.lri_mfg = conditionReport.Lri_Mfg__c;
            this.lri_tread = Integer.valueOf(conditionReport.Lri_Tread__c);
            this.lri_wsize = Integer.valueOf(conditionReport.Lri_Wsize__c);
            this.rf_cond = conditionReport.Rf_Cond__c;
            this.rf_dmg = conditionReport.Rf_Dmg__c;
            this.rf_mfg = conditionReport.Rf_Mfg__c;
            this.rf_tread = Integer.valueOf(conditionReport.Rf_Tread__c);
            this.rf_wsize = Integer.valueOf(conditionReport.Rf_Wsize__c);
            this.rr_cond = conditionReport.Rr_Cond__c;
            this.rr_dmg = conditionReport.Rr_Dmg__c;
            this.rr_mfg = conditionReport.Rr_Mfg__c;
            this.rr_tread = Integer.valueOf(conditionReport.Rr_Tread__c);
            this.rr_wsize = Integer.valueOf(conditionReport.Rr_Wsize__c);
            this.rri_cond = conditionReport.Rri_Cond__c;
            this.rri_dmg = conditionReport.Rri_Dmg__c;
            this.rri_mfg = conditionReport.Rri_Mfg__c;
            this.rri_tread = Integer.valueOf(conditionReport.Rri_Tread__c);
            this.rri_wsize = Integer.valueOf(conditionReport.Rri_Wsize__c);
            this.sp_cond = conditionReport.Sp_Cond__c;
            this.sp_dmg = conditionReport.Sp_Dmg__c;
            this.sp_mfg = conditionReport.Sp_Mfg__c;
            this.sp_tread = Integer.valueOf(conditionReport.Sp_Tread__c);
            this.sp_wsize = Integer.valueOf(conditionReport.Sp_Wsize__c);
        }
    }
}