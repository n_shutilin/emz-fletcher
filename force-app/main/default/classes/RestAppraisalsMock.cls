@isTest
global class RestAppraisalsMock implements HttpCalloutMock{
    
    private static final String DEFAULT_BODY = '{'+
  												  +'"messages": [],'+
  												  +'"location": null,'+
        										  +'"content": [{"year":null,"vin":"test","trim":null,"transmission":null,"transaction_code":null,"trade_type":null,"recondition_estimate":null,"odometer_type":null,"notes":null,"model":null,"mileage":null,"make":null,"interior_type":null,"interior_color":null,"ext_oem_color":null,"ext_generic_color":null,"evaluation":null,"date_created":null,"created_by":null,"cost_initial":null,"contact":null,"condition_report":null,"chrome_style_id":null}]'+
											  +'}';
    private static final Integer DEFAULT_CODE =  401;
    
    private String htmlBody;
    private Integer code;
    
    global RestAppraisalsMock(){
        this(DEFAULT_BODY, DEFAULT_CODE);
    }	

     global RestAppraisalsMock(Integer code){
 		this(DEFAULT_BODY, code);
    }
    
    global RestAppraisalsMock(String htmlBody){
 		this(htmlBody, DEFAULT_CODE);
    }
    
    global RestAppraisalsMock(String htmlBody, Integer code){
        this.htmlBody = htmlBody;
        this.code = code;
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(this.htmlBody);
        res.setStatusCode(this.code);
        return res;
    }
}