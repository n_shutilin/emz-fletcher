public class RoundRobinController {
	//Passing a list variable so that it can be invocable. Originally had it as Lead MyLead.
	@InvocableMethod(label='Lead Round Robin')
	public static void RoundRobinController(List<Lead> MyLeadList) {
		//Get A List of All Time Slot Records
		List<TimeSlot> TimeSlots = [SELECT Id, StartTime, EndTime, DayOfWeek, OperatingHours.Dealer_Code__c FROM TimeSlot];
		//Get a list of all Round Robin Group Records alond with Group Members
		List<Round_Robin_Group__c> RoundRobinGroup = [
			SELECT
				id,
				Dealer_Account__r.Dealer_Code__c,
				Round_Robin_Iteration__c,
				(SELECT Lead_Source__c, Round_Robin_Group__c FROM Round_Robin_Lead_Sources__r),
				(SELECT id, Service_Resource__r.RelatedRecordId, Round_Robin_Number__c, Round_Robin_Group__r.Id FROM Round_Robin_Group_Members__r)
			FROM Round_Robin_Group__c
			WHERE Active__c = TRUE
		];
		// Get a list of all Shift records that start today
		List<Shift> shifts = [SELECT id, StartTime, EndTime, Status, ServiceResource.RelatedRecordId, ServiceTerritoryId FROM Shift WHERE StartTime >= TODAY];

        System.debug('shifts');
        System.debug(shifts);
        System.debug('RoundRobinGroup');
        System.debug(RoundRobinGroup);

		// Collection of all leads where the owner will be updated
		List<Lead> LeadToUpdate = new List<Lead>();

		// Number to add to the round robin calculation
		integer iteration = 0;

		//Loop over the leads being passed in
		for (Lead MyLead : MyLeadList) {
			//Add one to the iteration variable to be used in the round robin calculation
			iteration += 1;

			//If no time slots are not found, exit.
			if (TimeSlots.size() <= 0) {
				return;
			}

			//Loop Over Time Slot Records
			for (TimeSlot ts : TimeSlots) {
				//Get The Time from the CreatedDate field on the current lead
				Time CreatedTime = MyLead.CreatedDate.time();

				/*Check if the Time Slot Record Matches The Lead's Day of Week And Dealer ID
				 and that it falls within business hours and had some activity performed on it*/
				if ( ((CreatedTime <= ts.StartTime || CreatedTime >= ts.EndTime) || ((CreatedTime >= ts.StartTime && CreatedTime <= ts.EndTime) && MyLead.LastActivityDate != null)) && ts.DayOfWeek == MyLead.Lead_Created_Day_of_Week__c && ts.OperatingHours.Dealer_Code__c == MyLead.Dealer_ID__c ) {
					return;
				}
			}
			//Collect a list of round robin user ids
			List<Round_Robin_Group__c> SelectedRoundRobinGroupId = new List<Round_Robin_Group__c>();
			//Collect a map of valid Round Robin Users with an assigned round robin number
			Map<id, Integer> validRoundRobinUsers = new Map<id, Integer>();
			if (RoundRobinGroup.size() > 0) {
				for (Round_Robin_Group__c rrg : RoundRobinGroup) {
					//Check the Round Robin Groups to find one with a matching Dealer ID
					if (rrg.Dealer_Account__r.Dealer_Code__c == MyLead.Dealer_ID__c) {
						//Check the Round RObin Group Lead Sources to find one with a matching lead source
						for (Round_Robin_Lead_Source__c rrls : rrg.Round_Robin_Lead_Sources__r) {
							//Add the matching Round Robin group to a list
							if (rrls.Lead_Source__c == MyLead.FJ_Lead_Source__c) {
								SelectedRoundRobinGroupId.add(rrg);
							}
						}
						//Check to see if any matching Round Robin Groups were found. There should be only one matching group per lead.
						if (SelectedRoundRobinGroupId.size() > 0) {
							Integer i = 0;
							//Check all Round Robin Group Memebrs to find the ones with a matching round robin group
							for (Round_Robin_Group_Member__c rrgm : rrg.Round_Robin_Group_Members__r) {
								i += 1;
								//For all round robin group members that are part of the matching round robin group
								if (SelectedRoundRobinGroupId[0].id == rrgm.Round_Robin_Group__c) {
									for (Shift sft : shifts) {
										//Check to see if their shift falls within the lead's created Date/Time and add them to a list
										if ( MyLead.CreatedDate >= sft.StartTime && MyLead.CreatedDate <= sft.EndTime && sft.ServiceResource.RelatedRecordId == rrgm.Service_Resource__r.RelatedRecordId ) {
											validRoundRobinUsers.put(sft.ServiceResource.RelatedRecordId, i);
										}
									}
								}
							}
							//If the Round Robin group does not match, move on to the next round robin group.
						} else {
							continue;
						}
					}
				}
				//If no Round Robin Groups were found, exit.
			} else {
				return;
			}

			system.debug(validRoundRobinUsers);
			//If no valid round robin users were found, continue to the next lead.
			if (validRoundRobinUsers.size() <= 0) {
				continue;
			}

			Integer RoundRobinAssignmentId;
			//Assign a round robin Id to the lead based on the size of valid round robin member users plus the iteration.
			RoundRobinAssignmentId = math.mod((integer.valueof(SelectedRoundRobinGroupId[0].Round_Robin_Iteration__c) + iteration), validRoundRobinUsers.size()) + 1;

			//Loop all valid round robin users
			for (id svgm : validRoundRobinUsers.keyset()) {
				//Find the Round robin User who's round robin number matches the Round Robin Assignment Id
				if (validRoundRobinUsers.get(svgm) == RoundRobinAssignmentId) {
					//Assign a new owner
					Lead UpdatedLead = new Lead(id = MyLead.Id, OwnerId = svgm);
					//Add the lead to the Update List
					LeadToUpdate.add(UpdatedLead);
				}
			}
		}

		try {
			update LeadToUpdate;
		} catch (DmlException e) {
			system.debug(e.getmessage());
		}
	}
}