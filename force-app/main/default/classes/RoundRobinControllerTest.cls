@IsTest
public class RoundRobinControllerTest {

    @TestSetup
    static void setup(){
        Datetime yesterday = Datetime.now().addDays(-1);
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testemz.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testemz.com.com');
        insert u;

        Lead_Source__c ls = New Lead_Source__c();
		ls.Name = 'Test Lead Source';
        ls.Dealer__c = 'NBMBN';
        insert ls;
        
        Lead l1 = New Lead();
        l1.LastName = 'Test';
        l1.Dealer_ID__c = 'NBMBN';
        l1.FJ_Lead_Source__c = ls.Id;
        Insert l1;
		test.setCreatedDate(l1.Id, yesterday);

        Lead l2 = New Lead();
        l2.LastName = 'Test2';
        l2.Dealer_ID__c = 'NBMBN';
        l2.FJ_Lead_Source__c = ls.Id;
        Insert l2;
        test.setCreatedDate(l2.Id, Datetime.now().addMinutes(1));
        
        OperatingHours op = New OperatingHours();
        op.Name = 'Test Operating Hours';
        op.TimeZone = 'America/Los_Angeles';
        op.Dealer_Code__c = 'NBMBN';
        insert op;
        
        TimeSlot ts = New TimeSlot();
        ts.DayOfWeek = 'Sunday';
        ts.OperatingHoursId = op.Id;
        ts.StartTime = Time.newInstance(14, 30, 2, 20);
        ts.EndTime = Time.newInstance(18, 30, 2, 20);
        insert ts;
    	
        Account Dealer = New Account();
        Dealer.Name = 'Test Dealer';
        dealer.Dealer_Code__c = 'NBMBN';
        insert Dealer;
        
        Round_Robin_Group__c rrg = New Round_Robin_Group__c();
        rrg.Dealer_Account__c = Dealer.Id;
        rrg.Round_Robin_Iteration__c = 2;
        rrg.Active__c = true;
        insert rrg;
		
        Round_Robin_Lead_Source__c rrls = New Round_Robin_Lead_Source__c();
        rrls.Lead_Source__c = ls.Id;
        rrls.Round_Robin_Group__c = rrg.Id;
        insert rrls;
        
        ServiceResource svr = New ServiceResource();
        svr.Name = 'Test Resource';
        svr.RelatedRecordId = u.id;
        svr.IsActive = true;
        insert svr;
        
        Round_Robin_Group_Member__c rrgm = New Round_Robin_Group_Member__c();
        rrgm.Round_Robin_Group__c = rrg.Id;
        rrgm.Round_Robin_Number__c = 5;
        rrgm.Service_Resource__c = svr.Id;
        insert rrgm;

        OperatingHours operatingHours = new OperatingHours(
            Name = 'Test Sales Operating Hours',
            TimeZone = 'America/Los_Angeles',
            Dealer_Code__c = 'NBMBN');
        insert operatingHours;

        ServiceTerritory testST =  new ServiceTerritory(
            Name = 'TestTerritory',
            Dealer__c = 'NBMBN',
            IsSalesTerritory__c = true,
            OperatingHoursId = operatingHours.Id,
            IsActive = true);
        insert testST;

        ServiceTerritoryMember serviceTM = new ServiceTerritoryMember(
            EffectiveStartDate = Date.today().addDays(-5),
            EffectiveEndDate = Date.today().addDays(5),
            ServiceResourceId = svr.Id,
            ServiceTerritoryId = testST.Id);
        insert serviceTM;

        Shift testShift = new Shift(
            StartTime = Datetime.now(),
            EndTime = Datetime.now().addDays(1),
            Status = 'Tentative',
            ServiceTerritoryId = testST.Id,
            ServiceResourceId = svr.Id
        );
        insert testShift;
    }
	
    @IsTest
    static void RoundRobinControllerTest(){
        Test.startTest();
        Id expected = [SELECT Id FROM User WHERE Email='standarduser@testemz.com' LIMIT 1].Id;

        List<Lead> MyLeadList = [
            SELECT id, CreatedDate, LastActivityDate, Lead_Created_Day_of_Week__c, Dealer_ID__c, FJ_Lead_Source__c, OwnerId
            FROM Lead 
            WHERE LastName = 'Test' OR LastName = 'Test2'
        ];
        
        RoundRobinController.RoundRobinController(MyLeadList);
        
        Id result = [ SELECT OwnerId FROM Lead WHERE LastName = 'Test2' LIMIT 1].OwnerId;
        Test.stopTest();
        System.assertEquals(expected, result);
    }

    @IsTest
    static void RoundRobinIteratorTest(){
        Test.startTest();
        List<Lead> MyLeadList = [
            SELECT id, CreatedDate, LastActivityDate, Lead_Created_Day_of_Week__c, Dealer_ID__c, FJ_Lead_Source__c 
            FROM Lead 
            WHERE LastName = 'Test' OR LastName = 'Test2'
        ];
        
        RoundRobinIterator.RoundRobinIterator(MyLeadList);
        Decimal result = [ SELECT Round_Robin_Iteration__c FROM Round_Robin_Group__c WHERE Active__c = true LIMIT 1].Round_Robin_Iteration__c;
        Test.stopTest();
        System.assertEquals(4, result);
    }
    
}