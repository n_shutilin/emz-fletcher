public class RoundRobinIterator{

    @InvocableMethod(label='Round Robin Iterator')
    public static void RoundRobinIterator(List<Lead> MyLeads){
        
        List<Round_Robin_Group__c> RoundRobinGroup = [Select id, Dealer_Account__r.Dealer_code__c, Round_Robin_Iteration__c,Active__c, (Select id, Lead_Source__c from Round_Robin_Lead_Sources__r) FROM Round_Robin_Group__c];
        for(Lead ld: MyLeads){
            for(Round_Robin_Group__c rrg: RoundRobinGroup){
                if(rrg.Dealer_Account__r.Dealer_code__c == ld.Dealer_id__c && rrg.Active__c == True){
                    for(Round_Robin_Lead_Source__c rrls: rrg.Round_Robin_Lead_Sources__r){
                        if(rrls.Lead_Source__c == ld.FJ_Lead_Source__c){
                            rrg.Round_Robin_Iteration__c += 1;
                        }
                    }
                }
            }
        }
     update RoundRobinGroup;
    }

}