public with sharing class SalesAppointmentBookingController {

    @AuraEnabled(cacheable=true)
    public static User getUserDetails(String recId) {
        return  [SELECT Id , Name  FROM User WHERE Id =:recId];
    }

    @AuraEnabled(cacheable=true)
    public static String getSalesAppointmentTypes() {
        return JSON.serialize(ServiceAppointment.fields.SalesAppointmentType__c.getDescribe().getSObjectField().getDescribe().getPicklistValues());
    }

    @AuraEnabled
    public static String getCurrentSObjectType(String recordId){
        return Id.valueOf(recordId).getSObjectType().getDescribe().getName();
    }

    @AuraEnabled
    public static Opportunity getCurrentRecordInfo(String recordId){
        return [SELECT Id, Dealer_Id__c, OwnerId,  Account.Name, Account.FirstName, Account.PersonEmail, Account.PersonContactId 
            FROM Opportunity WHERE Id = :recordId LIMIT 1];
    }

    @AuraEnabled
    public static Account getDealerInfo(String dealer){
        Id recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Dealer' AND sObjectType = 'Account'].Id;
        return [SELECT Id, BufferTime__c FROM Account WHERE Dealer_Code__c = :dealer AND RecordTypeId =: recordTypeId LIMIT 1];
    }

    @AuraEnabled
    public static Integer getDealerTimezoneShift(String dealer){
        String tzString = [SELECT OperatingHours.Timezone FROM ServiceTerritory WHERE IsActive = true AND Dealer__c = :dealer AND IsSalesTerritory__c = true].OperatingHours.Timezone;
        Timezone tz = System.TimeZone.getTimeZone(tzString);
        Datetime dttz = Datetime.now();
        return tz.getOffset(dttz);
    }

    @AuraEnabled
    public static String getSalesAppointmentInfo(String recordId) {
        ServiceAppointment sa = [SELECT Id, ParentRecordId, DealerId__c,  EarliestStartTime, PickupStreet__c, 
            PickupCity__c, PickupState__c, PickupZIP__c, SchedStartTime, SchedEndTime, DueDate, PickupAddressComments__c,
            SalesAppointmentType__c, PrimaryResource__c, SecondaryResource__c
            FROM ServiceAppointment WHERE Id = :recordId LIMIT 1];

        SalesAppointmentWrapper saw = new SalesAppointmentWrapper();

        saw.Id = sa.Id;
        saw.ParentId = sa.ParentRecordId;
        saw.Dealer = sa.DealerId__c;
        saw.SalesAppointmentType = sa.SalesAppointmentType__c;
        saw.PrimaryResource = sa.PrimaryResource__c;
        saw.SecondaryResource = sa.SecondaryResource__c;
        saw.PreferredDate = sa.SchedStartTime != null ? sa.SchedStartTime.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString()) : sa.EarliestStartTime.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
        saw.BookingStartTime = sa.SchedStartTime != null ? sa.SchedStartTime.format('HH:mm',  UserInfo.getTimeZone().toString()) : null;
        saw.BookingEndTime = sa.SchedEndTime != null ? sa.SchedEndTime.format('HH:mm',  UserInfo.getTimeZone().toString()) : null;
        saw.PickupStreet = sa.PickupStreet__c;
        saw.PickupCity = sa.PickupCity__c;
        saw.PickupState = sa.PickupState__c;
        saw.PickupZIP = sa.PickupZIP__c;
        saw.PickupAddressComments = sa.PickupAddressComments__c;

        return JSON.serialize(saw);
    }

    @AuraEnabled
    public static List<ServiceResource> getDefaultServiceResource(String userId){
        return [SELECT Id FROM ServiceResource WHERE RelatedRecordId = :userId LIMIT 1];
    }

    @AuraEnabled
    public static List<ServiceResource> getAvailableServiceResource(String dealer) {
        List<ServiceResource> serviceResources = new List<ServiceResource>();

        List<ServiceTerritory> servTerritory = [SELECT Id FROM ServiceTerritory 
            WHERE Dealer__c =: dealer AND IsSalesTerritory__c = true LIMIT 1];

        if(servTerritory.size() > 0) {
            String servTerritoryId = servTerritory[0].Id;

            List<ServiceTerritoryMember> servTerMembers = [SELECT Id, ServiceResourceId FROM ServiceTerritoryMember 
                WHERE ServiceTerritoryId = :servTerritoryId AND (EffectiveEndDate > TODAY OR EffectiveEndDate = NULL)];

            List<String> serviceResourcesIds = new List<String>();
            for(ServiceTerritoryMember stm : servTerMembers) {
                serviceResourcesIds.add(stm.ServiceResourceId);
            }

            serviceResources = [SELECT Id, Name, DefaultAdvisorTime__c, Sales_Resource_Type__c, Dealer__c
                FROM ServiceResource WHERE Dealer__c = :dealer AND Id in :serviceResourcesIds ORDER BY Name ASC];
        }

        return serviceResources;
    }

    @AuraEnabled
    public static String createServiceAppointment(String recordId, String dealer) {
        ServiceTerritory servTerritory = [SELECT Id, Dealer__c FROM ServiceTerritory WHERE Dealer__c = :dealer AND IsSalesTerritory__c = true LIMIT 1];

        Id recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'SalesAppointment' AND sObjectType = 'ServiceAppointment'].Id;
        
        ServiceAppointment newAppointment    = new ServiceAppointment();
        newAppointment.RecordTypeId          = recordTypeId;
        newAppointment.ServiceTerritoryId    = servTerritory.Id;
        newAppointment.DealerId__c           = servTerritory.Dealer__c;
        newAppointment.ParentRecordId        = Id.valueOf(recordId);
        newAppointment.DurationType          = 'Minutes';
        newAppointment.Duration              = 30;
        newAppointment.Status                = 'None';
        newAppointment.NotCompleted__c       = true;
        
        Time startTime = Time.newInstance(0, 0, 0, 0);
        Time endTime = Time.newInstance(23, 59, 59, 0);
        newAppointment.EarliestStartTime = DateTime.newInstance(Date.today(), startTime);
        newAppointment.DueDate = DateTime.newInstance(Date.today().addDays(4), endTime);

        insert newAppointment;
    
        return newAppointment.Id;
    }

    @AuraEnabled
    public static string getAppointmentTypeTimeslots(String appointmentType, String dealer){

        List<SalesTimeslotsSettings__mdt> sts = [SELECT OperatingHoursId__c FROM SalesTimeslotsSettings__mdt 
            WHERE Dealer__c = :dealer AND SalesAppointmentType__c = :appointmentType LIMIT 1];

        if(sts.isEmpty()) {
            string errorMessage = 'Metadata for ' + appointmentType + ' Sales Appointment is not configured';
            AuraHandledException auraEx = new AuraHandledException(errorMessage);
            auraEx.setMessage(errorMessage);
            throw auraEx;
        } else {
            List<Timeslot> tsList = [SELECT DayOfWeek, StartTime, EndTime FROM Timeslot WHERE OperatingHoursId =: sts[0].OperatingHoursId__c];

            Map<String, Timeslot[]> appointmentTypeTimeslotMap = new Map<String, Timeslot[]>();
        
            for(TimeSlot ts : tsList) {
                if (appointmentTypeTimeslotMap.get(ts.DayOfWeek) == NULL) {
                    appointmentTypeTimeslotMap.put(ts.DayOfWeek, new TimeSlot[]{ts});
                } else {
                    Timeslot[] slotByDayList = appointmentTypeTimeslotMap.get(ts.DayOfWeek);
                    slotByDayList.add(ts);
                }
            }

            if(appointmentTypeTimeslotMap.isEmpty()) {
                string errorMessage = 'Timeslots for selected Sales Appointment Type not created';
                AuraHandledException auraEx = new AuraHandledException(errorMessage);
                auraEx.setMessage(errorMessage);
                throw auraEx;
            }

            return JSON.serialize(appointmentTypeTimeslotMap);
        }
    }

    @AuraEnabled
    public static Map<String, Timeslot[]> getAppointmentTypeTimeslots_v2(String appointmentType, String dealer){

        List<SalesTimeslotsSettings__mdt> sts = [SELECT OperatingHoursId__c FROM SalesTimeslotsSettings__mdt 
            WHERE Dealer__c = :dealer AND SalesAppointmentType__c = :appointmentType LIMIT 1];

        if(sts.isEmpty()) {
            string errorMessage = 'Metadata for ' + appointmentType + ' Sales Appointment is not configured';
            AuraHandledException auraEx = new AuraHandledException(errorMessage);
            auraEx.setMessage(errorMessage);
            throw auraEx;
        } else {
            List<Timeslot> tsList = [SELECT DayOfWeek, StartTime, EndTime FROM Timeslot WHERE OperatingHoursId =: sts[0].OperatingHoursId__c];

            Map<String, Timeslot[]> appointmentTypeTimeslotMap = new Map<String, Timeslot[]>();
        
            for(TimeSlot ts : tsList) {
                if (appointmentTypeTimeslotMap.get(ts.DayOfWeek) == NULL) {
                    appointmentTypeTimeslotMap.put(ts.DayOfWeek, new TimeSlot[]{ts});
                } else {
                    Timeslot[] slotByDayList = appointmentTypeTimeslotMap.get(ts.DayOfWeek);
                    slotByDayList.add(ts);
                }
            }

            if(appointmentTypeTimeslotMap.isEmpty()) {
                string errorMessage = 'Timeslots for selected Sales Appointment Type not created';
                AuraHandledException auraEx = new AuraHandledException(errorMessage);
                auraEx.setMessage(errorMessage);
                throw auraEx;
            }

            return appointmentTypeTimeslotMap;
        }
    }

    private static String getAppointmentTypeOpHours(String appointmentType, String dealer) {
        return [SELECT OperatingHoursId__c FROM SalesTimeslotsSettings__mdt 
        WHERE Dealer__c = :dealer AND SalesAppointmentType__c = :appointmentType LIMIT 1].OperatingHoursId__c;
    }

    @AuraEnabled
    public static String getAvailableTimeSlots(String appointmentId, String resourceId, String dealer, String appointmentType) {
        Id schedulingPolicyId = [SELECT Id FROM FSL__Scheduling_Policy__c WHERE Name = 'Customer First' LIMIT 1].Id;
        FSL.GradeSlotsService slotService = new FSL.GradeSlotsService(schedulingPolicyId, appointmentId);
        FSL.AdvancedGapMatrix resultMatrix = slotService.getGradedMatrix(true);
        Map<Id, FSL.ResourceScheduleData> mySRGradedTimeSlotMap = resultMatrix.ResourceIDToScheduleData;
        List<SchedulingOptionWrapper> schedOptionsList = new List<SchedulingOptionWrapper>();
        String opHoursId = getAppointmentTypeOpHours(appointmentType, dealer );
        Integer hoursShift = AppointmentSchedulerUtility.calculateTimezoneDifferenceMinutes(opHoursId, true);
        for (Id thisresourceid : mySRGradedTimeSlotMap.keySet()) {
            for (FSL.SchedulingOption thisso : mySRGradedTimeSlotMap.get(thisresourceid).SchedulingOptions) {
                if(thisso.Interval.Start.dateGMT() == thisso.Interval.Finish.dateGMT()){
                    schedOptionsList.add(new SchedulingOptionWrapper (thisresourceid, thisso.Grade, thisso.Interval.Start, thisso.Interval.Finish, hoursShift));
                } else {
                    schedOptionsList.add(new SchedulingOptionWrapper (thisresourceid, thisso.Grade, thisso.Interval.Start, thisso.Interval.Finish.dateGMT(), hoursShift));
                    schedOptionsList.add(new SchedulingOptionWrapper (thisresourceid, thisso.Grade, thisso.Interval.Finish.dateGMT(), thisso.Interval.Finish, hoursShift));
                }
            }
        }

        List<SchedulingOptionWrapper> finalSchedOptionsList = new List<SchedulingOptionWrapper>();

        for(SchedulingOptionWrapper sow : schedOptionsList) {
            if(sow.ResourceID == resourceId) {
                finalSchedOptionsList.add(sow);
            }
        }
        
        return JSON.serialize(finalSchedOptionsList);
    }

    @AuraEnabled
    public static String getAvailableTimeSlots_v2(String appointmentId, String dealer, String appointmentType) {
        Id schedulingPolicyId = [SELECT Id FROM FSL__Scheduling_Policy__c WHERE Name = 'Customer First' LIMIT 1].Id;
        FSL.GradeSlotsService slotService = new FSL.GradeSlotsService(schedulingPolicyId, appointmentId);
        FSL.AdvancedGapMatrix resultMatrix = slotService.getGradedMatrix(true);
        Map<Id, FSL.ResourceScheduleData> mySRGradedTimeSlotMap = resultMatrix.ResourceIDToScheduleData;
        List<SchedulingOptionWrapper> schedOptionsList = new List<SchedulingOptionWrapper>();
        String opHoursId = getAppointmentTypeOpHours(appointmentType, dealer );
        Integer hoursShift = AppointmentSchedulerUtility.calculateTimezoneDifferenceMinutes(opHoursId, true);
        for (Id thisresourceid : mySRGradedTimeSlotMap.keySet()) {
            for (FSL.SchedulingOption thisso : mySRGradedTimeSlotMap.get(thisresourceid).SchedulingOptions) {
                if(thisso.Interval.Start.dateGMT() == thisso.Interval.Finish.dateGMT()){
                    schedOptionsList.add(new SchedulingOptionWrapper (thisresourceid, thisso.Grade, thisso.Interval.Start, thisso.Interval.Finish, hoursShift));
                } else {
                    schedOptionsList.add(new SchedulingOptionWrapper (thisresourceid, thisso.Grade, thisso.Interval.Start, thisso.Interval.Finish.dateGMT(), hoursShift));
                    schedOptionsList.add(new SchedulingOptionWrapper (thisresourceid, thisso.Grade, thisso.Interval.Finish.dateGMT(), thisso.Interval.Finish, hoursShift));
                }
            }
        }
        System.debug(schedOptionsList);
        
        return JSON.serialize(schedOptionsList);
    }

    @AuraEnabled
    public static void updateServiceAppointmentDate(String appointmentId, String startDate, String endDate) {
        ServiceAppointment sa = new ServiceAppointment();
        sa.Id = appointmentId;
        Time startTime = Time.newInstance(0, 0, 0, 0);
        Time endTime = Time.newInstance(23, 59, 59, 0);
        sa.EarliestStartTime = DateTime.newInstance(Date.parse(startDate), startTime);
        sa.DueDate = DateTime.newInstance(Date.parse(endDate), endTime);

        update sa;
    }

    @AuraEnabled
    public static void updateServiceAppointmentDate_v2(String appointmentId, String preferredDate) {
        ServiceAppointment sa = new ServiceAppointment();
        sa.Id = appointmentId;
        sa.EarliestStartTime = AppointmentSchedulerUtility.getStartDate(preferredDate);
        sa.DueDate = AppointmentSchedulerUtility.getEndDate(preferredDate);

        update sa;
    }

    @AuraEnabled
    public static String scheduleServiceAppointment(String salesAppointment, Boolean isRestoring) {
        SalesAppointmentWrapper appointmentWrapper =
            (SalesAppointmentWrapper) JSON.deserialize(salesAppointment, SalesAppointmentWrapper.class);

        System.debug(appointmentWrapper);
        System.debug(isRestoring);
        List<AssignedResource> oldARList = [SELECT Id FROM AssignedResource WHERE ServiceAppointmentId = :appointmentWrapper.Id];
        delete oldARList;

        String opHoursId = getAppointmentTypeOpHours(appointmentWrapper.SalesAppointmentType, appointmentWrapper.Dealer );
        Integer hoursShift = AppointmentSchedulerUtility.calculateTimezoneDifferenceMinutes(opHoursId, true);

        ServiceAppointment sa = [SELECT Id, ParentRecordId, DealerId__c, EarliestStartTime, DueDate FROM ServiceAppointment WHERE Id = :appointmentWrapper.Id LIMIT 1];

        sa.SchedStartTime = Datetime.valueOf(appointmentWrapper.PreferredDate + ' ' + appointmentWrapper.BookingStartTime + ':00').addMinutes(-hoursShift);
        //sa.SchedEndTime = Datetime.valueOf(appointmentWrapper.PreferredDate + ' ' + appointmentWrapper.BookingEndTime + ':00').addMinutes(-hoursShift);

        sa.SchedEndTime = appointmentWrapper.BookingEndTime == '00:00'
            ? Datetime.valueOf(appointmentWrapper.PreferredDate + ' ' + appointmentWrapper.BookingEndTime + ':00').addMinutes(-hoursShift + 1440)
            : Datetime.valueOf(appointmentWrapper.PreferredDate + ' ' + appointmentWrapper.BookingEndTime + ':00').addMinutes(-hoursShift);
        Long duration = sa.SchedEndTime.getTime() - sa.SchedStartTime.getTime();
        duration /= 60000;
        sa.Duration = duration;
        sa.Status = 'Scheduled';
        sa.NotCompleted__c = false;
        sa.SalesAppointmentType__c = appointmentWrapper.SalesAppointmentType;
        sa.PrimaryResource__c = appointmentWrapper.PrimaryResource;
        sa.SecondaryResource__c = appointmentWrapper.SecondaryResource;
        sa.PickupStreet__c = appointmentWrapper.PickupStreet;
        sa.PickupCity__c = appointmentWrapper.PickupCity;
        sa.PickupState__c = appointmentWrapper.PickupState;
        sa.PickupZIP__c = appointmentWrapper.PickupZIP;
        sa.PickupAddressComments__c = appointmentWrapper.PickupAddressComments;
        sa.Email_Template_Formatted_Data__c = sa.SchedStartTime.format('E, d MMM yyyy hh:mm aaa');
        System.debug(sa.SchedStartTime);
        System.debug(sa.Email_Template_Formatted_Data__c);
        sa.Email_Template_Recipient_Names__c = appointmentWrapper.EmailRecepientsName;

        sa.EarliestStartTime = sa.SchedStartTime;
        sa.DueDate           = sa.SchedEndTime;
        
            update sa;
        
        AssignedResource asr = new AssignedResource(ServiceAppointmentId = sa.Id , ServiceResourceId = sa.PrimaryResource__c);
        insert asr;

        List<Event> eventList = [SELECT id FROM Event WHERE WhatId =: sa.ParentRecordId AND Appointment__c = :sa.Id];
        delete eventList;

        if(appointmentWrapper.SecondaryResource != null) {
            ServiceResource manager = [SELECT RelatedRecordId FROM ServiceResource WHERE Id = :appointmentWrapper.SecondaryResource LIMIT 1];

            Opportunity opp = [SELECT Account.Name FROM Opportunity WHERE Id =: sa.ParentRecordId LIMIT 1];

            Event ev = new Event();
            ev.Subject = appointmentWrapper.SalesAppointmentType;
            ev.OwnerId = manager.RelatedRecordId;
            ev.StartDateTime = sa.SchedStartTime;
            ev.EndDateTime = sa.SchedEndTime;
            ev.WhatId = sa.ParentRecordId;
            ev.DealerId__c = sa.DealerId__c;
            ev.Description = appointmentWrapper.SalesAppointmentType + ' - ' + opp.Account.Name;
            ev.ShowAs = 'Free';
            ev.Appointment__c = sa.Id;

            insert ev;
        }

        return sa.Id;
    }

    @AuraEnabled
    public static void confirmationSalesAppointmentUpdate(String appointmentId, Boolean doNotSendEmail){
        ServiceAppointment sa = [SELECT Id FROM ServiceAppointment WHERE Id = :appointmentId LIMIT 1];
        sa.OptOutOfConfirmation__c = doNotSendEmail;
        update sa;
    }

    @AuraEnabled
    public static void deleteAppointment(String appointmentId) {
        ServiceAppointment sa = [SELECT Id FROM ServiceAppointment WHERE Id = :appointmentId LIMIT 1];
        delete sa;
    }

    // @AuraEnabled
    // public static String getEmailTemplate(String appointmentId, Boolean isUpdate){
    //     return AppointmentSchedulerUtility.getParsedEmailTemplateBody(appointmentId, isUpdate);
    // }

    // @AuraEnabled
    // public static void sendConfirmationEmail(List<String> ccAddresses, String appointmentId, String contactId, Boolean isUpdate){
    //     AppointmentSchedulerUtility.sendConfirmationEmail(ccAddresses, appointmentId, contactId, isUpdate);
    // }

    public class SalesAppointmentWrapper {
        public String Id;
        public String ParentId;
        public String Dealer;
        public String SalesAppointmentType;
        public String PrimaryResource;
        public String SecondaryResource;

        public String PreferredDate;
        public String BookingStartTime;
        public String BookingEndTime;

        public String PickupStreet;
        public String PickupCity;
        public String PickupState;
        public String PickupZIP;
        public String PickupAddressComments;

        public String EmailRecepientsName;
        public String EmailAdditionalComment;
        public Boolean OptOutOfConfirmation;
    }

    public class SchedulingOptionWrapper {
        public String ResourceID { get; set; }
        public Decimal Grade { get; set; }
        public Datetime StartDate { get; set; }
        public Datetime EndDate { get; set; }

        public SchedulingOptionWrapper() {
        }

        public SchedulingOptionWrapper(String id, Decimal curGrade, Datetime startDate, Datetime endDate, Integer shift) {
            this.ResourceID = id;
            this.Grade = curGrade;
            // this.StartDate  = startDate;
            // this.EndDate    = endDate;
            this.StartDate  = startDate.addMinutes(shift);
            this.EndDate    = endDate.addMinutes(shift);
        }
    }
}