public with sharing class SaveAttachmentController {

    @AuraEnabled
    public static void saveVideo(String documentId, String appointmentId) {
        try {
            insert new VehicleImage__c(Document_Id__c = documentId, Service_Appointment__c = appointmentId, Media_Type__c = 'Video');
        } catch(Exception ex) {
            throw new AuraHandledException('Video uploading error');
        }
    }

    @AuraEnabled
    public static List<String> retrieveVideos(String appointmentId) {
        List<String> videoIds = new List<String>();
        if(!String.isBlank(appointmentId)){
            List<VehicleImage__c> videos = [
                SELECT Id, Document_Id__c
                FROM VehicleImage__c
                WHERE Service_Appointment__c = :appointmentId AND Media_Type__c = 'Video'
            ];
            for (VehicleImage__c video : videos) {
                if (video != null) {
                    videoIds.add(video.Document_Id__c);
                }
            }
        }
        return videoIds;
    }

    @AuraEnabled
    public static void saveAnnotation(List<AnnotationWrapper> notes, String appointmentId) {
        List<VehicleImage__c> images = new List<VehicleImage__c>();
        List<Attachment> attachments = new List<Attachment>();
        Map<String, String> bodyMap = new Map<String, String>();
        System.debug('notes ' + notes);
        for (AnnotationWrapper item : notes) {
            images.add(new VehicleImage__c(
                Comment__c = item.text,
                Type__c = item.note,
                X_Coordinate__c = item.x,
                Y_Coordinate__c = item.y,
                PhotoHeight__c = item.photoHeight,
                PhotoWidth__c = item.photoWidth,
                Service_Appointment__c = appointmentId
            ));
            bodyMap.put((item.x + '_' + item.y), item.photo);
            System.debug('bodyMap ' + bodyMap);
        }
        try {
            insert images;
        } catch (Exception e) {
            throw new AuraHandledException('Record Saving Error');        
        }
        for (VehicleImage__c image : images) {
            System.debug('key ' + (image.X_Coordinate__c + '_' + image.Y_Coordinate__c));
            String imageBody = bodyMap.get(image.X_Coordinate__c + '_' + image.Y_Coordinate__c);
            System.debug('imageBody ' + imageBody);
            System.debug(imageBody.substringBefore(';'));
            //imageBody.remove(imageBody.substringBefore(';') + ';');
            System.debug(imageBody);
            String base64Part = imageBody.substringAfter(',');
            System.debug(base64Part);
            if (base64Part != '' && base64Part != null) {
                attachments.add(new Attachment(
                    ParentId = image.Id,
                    Name = 'Vehicle Image',
                    ContentType = 'image/png',
                    Body = EncodingUtil.base64Decode(base64Part)
                ));
            }
        }
        try {
            insert attachments;
        } catch (Exception e) {
            throw new AuraHandledException('Attachment Saving Error');        
        }
    }

    @AuraEnabled
    public static List<AnnotationWrapper> retrieveAnnotation(String vehicleImageId) {
        List<VehicleImage__c> images = [
            SELECT Comment__c, Type__c, X_Coordinate__c, Y_Coordinate__c, PhotoHeight__c,
                PhotoWidth__c, Service_Appointment__c
            FROM VehicleImage__c
            WHERE Id = :vehicleImageId AND Media_Type__c != 'Video'
        ];
        List<String> imageIds = new List<String>();
        List<AnnotationWrapper> notes = new List<AnnotationWrapper>();
        for (VehicleImage__c image : images) {
            notes.add(new AnnotationWrapper(image));
            imageIds.add(image.Id);
        }
        List<Attachment> attachments = [
            SELECT Id
            FROM Attachment
            WHERE ParentId IN :imageIds
            LIMIT 1
        ];
        for (AnnotationWrapper note : notes) {
            if (!attachments.isEmpty()) {
                note.photo = '/servlet/servlet.FileDownload?file=' + attachments[0].Id;
            }
        }
        return notes;
    }

    @AuraEnabled
    public static List<String> getImageIds(String appointmentId) {
        List<String> imageIds = new List<String>();
        if(!String.isBlank(appointmentId)){
            List<VehicleImage__c> images = [
                SELECT Id
                FROM VehicleImage__c
                WHERE Service_Appointment__c = :appointmentId AND Media_Type__c != 'Video'
            ];
            if(!images.isEmpty()){
                for (VehicleImage__c image : images) {
                    imageIds.add(image.Id);
                }
                System.debug('appointment id: ' + appointmentId + ' images id list: ' + imageIds);
            }
        }
        return imageIds;
    }

    @AuraEnabled
    public static String getSignatureImage(String appointmentId) {
        List<Attachment> signatures = [
            SELECT Id, Body FROM Attachment WHERE ParentId = :appointmentId AND Name = 'Signature' ORDER BY CreatedDate DESC LIMIT 1
        ];
        if (!signatures.isEmpty()) {
            return EncodingUtil.base64Encode(signatures[0].Body);
        }
        return '';
    }

    @AuraEnabled
    public static void saveSignature(String imageBody, String appointmentId) {
        List<String> base64Parts = imageBody.split(',');
        imageBody = String.isBlank(imageBody) ? '' : imageBody;
        if (base64Parts.size() > 1) {
            System.debug(appointmentId);
            insert new Attachment(
                ParentId = appointmentId,
                Name = 'Signature',
                ContentType = 'image/png',
                Body = EncodingUtil.base64Decode(base64Parts[1])
            );
        }
    }
    
    public class AnnotationWrapper {
        @AuraEnabled
        public String note {get;set;}
        @AuraEnabled
        public String text {get;set;}
        @AuraEnabled
        public Double x {get;set;}
        @AuraEnabled
        public Double y {get;set;}
        @AuraEnabled
        public Double photoHeight {get;set;}
        @AuraEnabled
        public Double photoWidth {get;set;}
        @AuraEnabled
        public String photo {get;set;}

        public AnnotationWrapper() {}

        public AnnotationWrapper(VehicleImage__c image) {
            this.note = image.Type__c;
            this.text = image.Comment__c;
            this.x = image.X_Coordinate__c;
            this.y = image.Y_Coordinate__c;
            this.photoHeight = image.PhotoHeight__c;
            this.photoWidth = image.PhotoWidth__c;
        }
    }
}