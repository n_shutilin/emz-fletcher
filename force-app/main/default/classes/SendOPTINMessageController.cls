public class SendOPTINMessageController {
    
    @InvocableMethod
    public static void sendOPTINMessage(List<sendSMSDetail> sendSMSDetails) {
        String query = '';
        String SENDER_ID = '';
        String DEALER_ID = 'Dealer_ID__c';
        
        Id recordId = sendSMSDetails[0].recordId;//store the recordId
        String sObjName = recordId.getSObjectType().getDescribe().getName();//store the objName
        
        //build dynamic query
        query = 'SELECT Id,';
        if(sObjName == 'Account') {
            DEALER_ID = 'Dealer_ID__pc';
        }
        query += DEALER_ID + ' FROM '+sObjName ;
        query +=' WHERE Id =:recordId';
        
        Sobject obj = Database.query(query);//excute dynamic query
        
        String dealerValue = String.valueOf(obj.get(DEALER_ID));// store the dealer Id value
        
        //condition to check the dealerId value is not null and custom setting dealer senderId is not null
        if(obj != null &&  dealerValue != null && DealerSenderId__c.getValues(dealerValue) != null){
            if(sObjName == 'Account') {
                SENDER_ID= DealerSenderId__c.getValues(dealerValue).ServiceSenderId__c;
            } else if(sObjName == 'Opportunity' || sObjName == 'Lead') {
                SENDER_ID= DealerSenderId__c.getValues(dealerValue).SaleSenderId__c;
            }
        }
        // condition to check senderId is null,if it is true return
        if(SENDER_ID == '') {
            return;
        }
        
        //create the outgoing sms
        smagicinteract__smsMagic__c smsHistory = new smagicinteract__smsMagic__c();
        smsHistory.smagicinteract__SenderId__c = SENDER_ID;
        smsHistory.smagicinteract__PhoneNumber__c = sendSMSDetails[0].phoneNumber;
        smsHistory.smagicinteract__disableSMSOnTrigger__c = 0; // this field either be 0 or 1, if you specify the value as 1 then sms will not get send but entry of sms will get create under SMS History object
        smsHistory.smagicinteract__external_field__c = smagicinteract.ApexAPI.generateUniqueKey();
        smsHistory.smagicinteract__Direction__c ='OUT';
        if(sObjName == 'Account') {
            smsHistory.smagicinteract__Account__c = recordId;
        } else if(sObjName == 'Opportunity') {
            smsHistory.smagicinteract__Opportunity__c = recordId;
        } else if(sObjName == 'Lead') {
            smsHistory.smagicinteract__Lead__c =  recordId;
        }
        smsHistory.smagicinteract__ObjectType__c = sObjName;
        smsHistory.smagicinteract__SMS_Template__c = sendSMSDetails[0].templateId;
        smsHistory.smagicinteract__Source__c = '1495';
        smsHistory.smagicinteract__Type__c = 'SMS';
        
        Insert smsHistory;
    }
    
    public class sendSMSDetail {
        @InvocableVariable
        public Id  recordId;
        
        @InvocableVariable
        public Id templateId;
        
        @InvocableVariable
        public String phoneNumber;
    }
}