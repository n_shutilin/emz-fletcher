@isTest
public class SendOPTINMessageController_Test {
    
    @isTest
    private static void sendOPTINMessageTest() {
        List<SendOPTINMessageController.sendSMSDetail> sendSMSDetails = new List<SendOPTINMessageController.sendSMSDetail>(); 
        String SENDER_ID = '13238949025';
        
        DealerSenderId__c dealerSenderId = new DealerSenderId__c(Name='CHMBN',SaleSenderId__c = '13122622031',ServiceSenderId__c='13124654623');
        insert dealerSenderId;
        
        Lead lead = new Lead(FirstName='Wilson',LastName='Test ',Phone='9840137234432',Dealer_ID__c ='CHMBN');
        insert lead;
        smagicinteract__SMS_Template__c template = new smagicinteract__SMS_Template__c(smagicinteract__Name__c = 'Send First OPTIN Message',
                                                                                       smagicinteract__Text__c= 'Hi {!Lead.Name} Please reply with the Keyword START or SUBSCRIBE',
                                                                                       smagicinteract__ObjectName__c ='Lead');
        insert template;
        
        SendOPTINMessageController.sendSMSDetail smsDetail = new SendOPTINMessageController.sendSMSDetail();
        smsDetail.recordId =lead.Id;
        smsDetail.phoneNumber=lead.Phone;
        smsDetail.templateId =template.Id;
        sendSMSDetails.add(smsDetail);
        Test.startTest();
        	SendOPTINMessageController.sendOPTINMessage(sendSMSDetails);
        Test.stopTest(); 
        
    }
}