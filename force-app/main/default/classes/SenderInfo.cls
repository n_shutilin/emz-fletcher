global with sharing class SenderInfo implements Comparable {
    @AuraEnabled public smagicinteract__SMS_SenderId__c senderRecord{get;set;}
    @AuraEnabled public String id{get;set;}
    @AuraEnabled public String senderId{get;set;}
    @AuraEnabled public String label{get;set;}
    @AuraEnabled public String description{get;set;}
    @AuraEnabled public String used_for{get;set;}
    @AuraEnabled public String email_Notification_Template{get;set;}
    @AuraEnabled public String notification_Recipient{get;set;}
    @AuraEnabled public Boolean isDefault{get;set;}
    @AuraEnabled public Set<Id> assignedUsers{get;set;}
    @AuraEnabled public Boolean isSenderIdValid{get;set;}
    @AuraEnabled public String failureMessage{get;set;}
    @AuraEnabled public String channelCode{get;set;}
    
    public boolean equals(Object obj) {
    if(obj instanceof SenderInfo && ((SenderInfo)obj).id == this.id && ((SenderInfo)obj).label == this.label) 
        return true;
    else 
        return false;
 	}
    
    global SenderInfo(){}

    public Integer hashCode() {
        Integer result = 0;
        if(String.isNotBlank(senderId)) 
            result = 17*senderId.hashCode();

        return id == null ? result : result + 17*id.hashCode();
    }

    public Integer compareTo(Object otherObject) {
        SenderInfo otherOption = (SenderInfo) otherObject;
        return this.label > otherOption.label ? 1 : this.label < otherOption.label ? -1 : 0;
    }

    global SenderInfo(String senderId, String label) {
        this.senderId = senderId;
        this.label = label;
        this.assignedUsers = new Set<Id> ();
        this.isSenderIdValid = true;
        this.isDefault = false;
    }
    
    public SenderInfo(smagicinteract__SMS_SenderId__c record) {
        if(record == null) {
            this.isSenderIdValid = false;
            this.failureMessage = '';
            return;
        }
        this.channelCode = String.isNotBlank(record.smagicinteract__Channel_Code__c) ? record.smagicinteract__Channel_Code__c : '1';
        this.isSenderIdValid = true;
        this.senderRecord = record;
        this.id = record.Id;
        this.senderId = record.smagicinteract__senderId__c;
        if(String.isNotBlank(record.smagicinteract__Label__c)) {
            this.label = record.smagicinteract__Label__c + ': '+ record.smagicinteract__senderId__c;
        }
        else {
            this.label = record.smagicinteract__senderId__c;
        } 
        this.description = record.smagicinteract__Description__c;
        this.used_for = record.smagicinteract__Used_For__c;
        this.email_Notification_Template = record.smagicinteract__Email_Notification_Template__c;
        this.notification_Recipient = record.smagicinteract__Notification_Recipient__c;
        List<smagicinteract__SenderId_Profile_Map__c> senderMapList = record.smagicinteract__SenderId_Profile_Map__r;
        this.assignedUsers = new Set<Id> ();
        if(senderMapList != null && !senderMapList.isEmpty()) {
            for(smagicinteract__SenderId_Profile_Map__c rec: senderMapList) {
                this.assignedUsers.add(rec.smagicinteract__Profile_Id__c);
                this.assignedUsers.add(rec.smagicinteract__User__c);
            }
            this.assignedUsers.remove(null);
        }
        this.isDefault = false;

    }
    
    public void setSenderIdInvalid(String failureMessage) {
        this.isSenderIdValid = false;
        this.failureMessage = failureMessage;
    }
}