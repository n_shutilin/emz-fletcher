@isTest
public class SenderInfo_Test {
    
    
    @IsTest
    private static void createSenderIdInfoTest() {
        smagicinteract__SMS_SenderId__c senderId =  new smagicinteract__SMS_SenderId__c(smagicinteract__senderId__c='13122622031',
                                                                                        smagicinteract__Channel_Code__c='1',smagicinteract__Used_For__c='Both');
        insert senderId;
        smagicinteract__SenderId_Profile_Map__c profileMap = new smagicinteract__SenderId_Profile_Map__c(
            smagicinteract__SenderId_Lookup__c=senderId.Id,smagicinteract__User__c=UserInfo.getUserId(),
            smagicinteract__Profile_Id__c =UserInfo.getProfileId()
        );
        insert profileMap;
        smagicinteract__SMS_SenderId__c sendId = [SELECT Id,smagicinteract__senderId__c,smagicinteract__Channel_Code__c,smagicinteract__Label__c,
                                                  smagicinteract__Description__c,smagicinteract__Used_For__c,smagicinteract__Email_Notification_Template__c,
                                                  smagicinteract__Notification_Recipient__c,
                                                  (SELECT Id,smagicinteract__User__c,smagicinteract__Profile_Id__c 
                                                   FROM smagicinteract__SenderId_Profile_Map__r)
                                                  FROM smagicinteract__SMS_SenderId__c WHERE Id =:senderId.Id];
        
        Test.startTest();
        	SenderInfo temp = new SenderInfo(sendId);
        Test.stopTest();
        
    }
    @IsTest
    private static void compareToTest() {
        SenderInfo senderId = new SenderInfo();
        
        SenderInfo temp = new SenderInfo('13122622031','INCOMING');
        temp.hashCode();
        
        senderId.equals((Object)temp);
        Test.startTest();
        	Integer sd = senderId.compareTo((Object)temp);
        Test.stopTest();
        System.assertEquals(-1, sd); 
        
    }
    @IsTest
    private static void setSenderIdInvalidTest() {
        SenderInfo senderId = new SenderInfo();
        Test.startTest();
        	senderId.setSenderIdInvalid('FAILED');
        Test.stopTest();
    }
    
}