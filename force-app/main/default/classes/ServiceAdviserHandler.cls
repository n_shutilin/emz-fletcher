public class ServiceAdviserHandler {
    public static final String PATH_APPOINTMENTS = '/pip-serviceappointment/services/ServiceAppointmentInsertUpdate';
    public static final String NAMESPACE = 'http://www.dmotorworks.com/pip-serviceappointment';

    public static List<Service_Advisor__c> getResources(String dealerId, String accountId) {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();

        String body =
            '<soapenv:Envelope ' +
                'xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
                'xmlns:pip="http://www.dmotorworks.com/pip-serviceappointment">' +
                '<soapenv:Header/>' +
                '<soapenv:Body>' +
                    '<pip:getHelpServiceAdvisor>' +
                        '<arg0>' +
                            '<username>' + settings.Username__c + '</username>' +
                            '<password>' + settings.Password__c + '</password>' +
                        '</arg0>' +
                        '<arg1>' +
                            '<dealerId>' + dealerId + '</dealerId>' +
                        '</arg1>' +
                    '</pip:getHelpServiceAdvisor>' +
                '</soapenv:Body>' +
            '</soapenv:Envelope>';

        HttpResponse response = DMotorWorksApi.post(PATH_APPOINTMENTS, body);
        List<Service_Advisor__c> resources = process(response.getBodyDocument(), accountId);

        return resources;
    }

    private static List<Service_Advisor__c> process(Dom.Document doc, String accountId) {
        Account dealerAccount = [SELECT Id, Dealer_Code__c, CDK_Dealer_Code__c  FROM Account WHERE Id = :accountId];
        String dealerCode = dealerAccount?.Dealer_Code__c;
        String dealerId = dealerAccount?.CDK_Dealer_Code__c;

        List<Service_Advisor__c> resources = new List<Service_Advisor__c>();

        Dom.XmlNode envelopeResult = doc.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/');
        Dom.XmlNode responseResult = bodyResult.getChildElement('getHelpServiceAdvisorResponse', NAMESPACE);

        if (responseResult != null) {
            for (Dom.XmlNode child : responseResult.getChildElements()) {
                if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == 'return') {
                    Service_Advisor__c advisor = new Service_Advisor__c(
                            Dealer__c = dealerCode
                    );

                    mapFields(advisor, child);
                    String name = processName(advisor.Name);
                    advisor.Name = name;
                    advisor.Unique_Name__c = dealerId + advisor.Service_Advisor_Number__c + name.deleteWhitespace();
                    resources.add(advisor);
                }
            }
        }

        return resources;
    }

    private static String processName(String rawName) {
        String name = rawName;
        if(rawName != NULL) {
            String[] splittedName = rawName.split(',');
            if(splittedName.size() > 1){
                name = splittedName[1] + ' ' + splittedName[0];
            }
        } else {
            return '';
        }
        return name;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
                'name' => Service_Advisor__c.Name,
                'serviceAdvisor' => Service_Advisor__c.Service_Advisor_Number__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, null);

            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                        fieldDataType == Schema.DisplayType.EMAIL ||
                        fieldDataType == Schema.DisplayType.LONG ||
                        fieldDataType == Schema.DisplayType.PICKLIST ||
                        fieldDataType == Schema.DisplayType.PHONE ||
                        fieldDataType == Schema.DisplayType.ADDRESS ||
                        fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                        fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }

}