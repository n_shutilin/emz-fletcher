global class ServiceAdviserRetrieverBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    public static final String ACCOUNT_DEALER_RT = 'Dealer';
    public Integer recordsProcessed = 0;
    public Integer recordsTotal = 0;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
            'SELECT Id, Dealer_Code__c, Name, CDK_Dealer_Code__c ' +
            'FROM Account ' +
            'WHERE RecordType.DeveloperName = :ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null';

        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        for (Account acc : scope) {
            List<Service_Advisor__c> serviceResources = ServiceAdviserHandler.getResources(acc.CDK_Dealer_Code__c, acc.Id);
            System.debug('serviceResources');
            System.debug(serviceResources);
            try {
               Database.UpsertResult[] results = Database.upsert(serviceResources, Service_Advisor__c.fields.Unique_Name__c, true);
               recordsProcessed += scope.size();
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, e.getStackTraceString());
                System.debug(LoggingLevel.ERROR, e.getMessage());
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        try {
            IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }

}