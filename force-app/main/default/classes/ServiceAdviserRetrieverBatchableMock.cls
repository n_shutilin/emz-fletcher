global class ServiceAdviserRetrieverBatchableMock implements HttpCalloutMock{
    private static String RESPONSE_BODY = '';

    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(RESPONSE_BODY);
        response.setStatusCode(200);
        return response;
    }
}