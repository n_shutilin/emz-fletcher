global class ServiceAdviserRetrieverSchedulable implements Schedulable{

    global void execute(SchedulableContext SC) {
        ServiceAdviserRetrieverBatchable batch = new ServiceAdviserRetrieverBatchable();
        Database.executeBatch(batch, 1);
    }

}