global class ServiceAppointmentBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    public Integer recordsProcessed = 0;
    public Integer recordsTotal = 0;
    public static final String ACCOUNT_DEALER_RT = 'Dealer';
    public Integer deltaDays;
    public Boolean isBulk;
    public String dealerId;

    global ServiceAppointmentBatchable() {
        this.deltaDays = 1;
        this.isBulk = false;
    }

    global ServiceAppointmentBatchable(Integer deltaDays) {
        this.deltaDays = deltaDays;
        this.isBulk = false;
    }

    global ServiceAppointmentBatchable(Boolean isBulk) {
        this.isBulk = isBulk;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
            'SELECT Id, Dealer_Code__c, Name, CDK_Dealer_Code__c ' +
            'FROM Account ' +
            'WHERE RecordType.DeveloperName = :ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null AND Dealer_Code__c != null';

        if (String.isNotBlank(dealerId)) {
            query += ' AND Dealer_Code__c = :dealerId';
        }

        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        for (Account acc : scope) {
            try {
                List<ServiceAppointment> appointments = ServiceAppointmentHandler.getAppointments(acc.CDK_Dealer_Code__c, deltaDays, acc.Id, isBulk);
                ServiceAppointmentTriggerHandler.dealerCode = acc.CDK_Dealer_Code__c;
                Database.UpsertResult[] results = Database.upsert(appointments, ServiceAppointment.fields.UniqueApptId__c, false);

                Database.UpsertResult[] successedRecords = new Database.UpsertResult[]{};
                Database.UpsertResult[] exeptionRecords = new Database.UpsertResult[]{};
                for ( Database.UpsertResult result : results) {
                    if (result.isSuccess()) {
                        successedRecords.add(result);
                    } else {
                        exeptionRecords.add(result);
                        System.debug(result);
                    }
                }
                logSucess(successedRecords.size(), acc.Dealer_Code__c);
                logError('', 'Not upserted records: SIZE ' + exeptionRecords.size(), acc.Dealer_Code__c);
            } catch (Exception e) {
                String errorMessage = e.getMessage();
                System.debug(LoggingLevel.ERROR, e.getStackTraceString());
                System.debug(LoggingLevel.ERROR, errorMessage);
                logError(e.getTypeName(), errorMessage, acc.Dealer_Code__c);
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        ServiceAppointmentDetailsBatchable roBatch = new ServiceAppointmentDetailsBatchable(deltaDays, isBulk);
        if (String.isNotBlank(dealerId)) {
            roBatch.dealerId = dealerId;
        } 
        Id roBatchId = Database.executeBatch(roBatch,1);
    }

    global static void logSucess(Integer size, String dealer){
        Long timeStamp = DateTime.now().getTime();
        Integration_Logs__c sucessLog = new Integration_Logs__c(
            Endpoint__c = '*/CDK Appointment Integration DEALER:' +  dealer +  ' SIZE:' + size,
            Process__c = 'CDK Appointment Integration',
            Result__c = 'OK',
            Posix_Time__c = timeStamp
        );
        insert sucessLog;
    }

    global static void logError(String errorType, String errorMessage, String dealer){
        Long timeStamp = DateTime.now().getTime();
        Integration_Logs__c errorLog = new Integration_Logs__c(
            Endpoint__c = '*/CDK Appointment Integration DEALER:' + dealer,
            Process__c = 'CDK Appointment Integration',
            Result__c = '',
            Error__c = errorType,
            Error_Message__c = errorMessage.length() > 255 ? errorMessage.substring(0,255) : errorMessage,
            Posix_Time__c = timeStamp
        );
        insert errorLog;
    }

}