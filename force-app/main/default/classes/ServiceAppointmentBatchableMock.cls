@isTest
global with sharing class ServiceAppointmentBatchableMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        System.debug('Endpoint ' + req.getEndpoint());
        System.debug('Method ' + req.getMethod());
        HttpResponse res = new HttpResponse();
        StaticResource staticResourceTest = [
            SELECT body 
            FROM StaticResource 
            WHERE Name = :req.getEndpoint().split('/').get(4) == 'serviceappointments' ? 'Test_Service_Appointment_XML' : 'Test_Service_Appointment_Detail_XML'
        ];
        res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        res.setBody(staticResourceTest.body.toString());
        res.setStatusCode(200);
        return res;
    }
}