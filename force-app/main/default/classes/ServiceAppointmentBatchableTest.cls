@IsTest
public with sharing class ServiceAppointmentBatchableTest {
    
    @TestSetup
    private static void setup() {
        insert new CDK_Integration__c(
            Username__c = 'Test Username',
            Password__c = 'Test Password',
            DMotorWorks_Url__c = 'https://test.dmotorworks.com'
        );

        String dealerRecordTypeId = [
            SELECT Id
            FROM RecordType
            WHERE sobjecttype = 'Account'
            AND Name = 'Dealer'
        ].Id;

        Account acc = new Account(
            Dealer_Code__c = 'test',
            CDK_Dealer_Code__c = 'testCode',
            Name = 'Test Dealer',
            RecordTypeId = dealerRecordTypeId
        );
        insert acc;

        insert new TriggerSettings__c(
            DefaultCDKAppointmentAccount__c = acc.Id
        );
    }

    @IsTest
    private static void serviceAppointmentSchedulableTest() { 
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new ServiceAppointmentBatchableMock());
        ServiceAppointmentSchedulable sh1 = new ServiceAppointmentSchedulable();
        System.schedule('Service Appointment Retrieve Test', '0 0 23 * * ?', sh1);
        Test.stopTest();
    }

    @IsTest
    private static void serviceAppointmentBatchTest() { 
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new ServiceAppointmentBatchableMock());
        ServiceAppointmentBatchable batch = new ServiceAppointmentBatchable();
        Database.executeBatch(batch, 1);
        Test.stopTest();
    }

    @IsTest
    private static void serviceAppointmentDetailsBatchTest() { 
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new ServiceAppointmentBatchableMock());
        ServiceAppointmentDetailsBatchable roBatch = new ServiceAppointmentDetailsBatchable();
        Id roBatchId = Database.executeBatch(roBatch,1);
        Test.stopTest();
    }
    
        @IsTest
    public static void loggerErrorDetailsTest(){
        ServiceAppointmentDetailsBatchable.logError('TEST', 'TEST', 'TEST');
    }
    
    @IsTest
    public static void loggerErrorAppointmentsTest(){
        ServiceAppointmentBatchable.logSucess(1, 'TEST');
    }
}