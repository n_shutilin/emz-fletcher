public with sharing class ServiceAppointmentButtonController {
    @AuraEnabled
    public static String sendAppointmentIntoCDK(String recordId, String actionType) {
        return ServiceAppointmentService.sendAppointmentIntoCDK(recordId, actionType);
    }

    @AuraEnabled
    public static ServiceAppointment retrieveServiceAppointment(String recordId) {
        return ServiceAppointmentService.retrieveServiceAppointment(recordId);
    }
}