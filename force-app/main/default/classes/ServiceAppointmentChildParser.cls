public class ServiceAppointmentChildParser {

    public static final String NAMESPACE = 'http://www.dmotorworks.com/pip-extract-service-appointments';
    public static final String CHILD_NAME = 'AppointmentsDetail';

    public static List<ServiceAppointmentDetail__c> process(Dom.Document doc, String accountId) {
        List<ServiceAppointmentDetail__c> appointmentDetails = new List<ServiceAppointmentDetail__c>();
        List<String> ApptIds = new List<String>();
        String dealerCode = [SELECT Id, CDK_Dealer_Code__c FROM Account WHERE Id = :accountId].CDK_Dealer_Code__c;
        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == CHILD_NAME) {
                ServiceAppointmentDetail__c appointmentDetail = new ServiceAppointmentDetail__c();
                mapFields(appointmentDetail, child);
                appointmentDetail.UniqueApptId__c = dealerCode + '_' + appointmentDetail.ApptID__c;
                appointmentDetail.UniqueApptDetailedId__c = dealerCode + '_' + appointmentDetail.ApptID__c + '_' + appointmentDetail.HostItemID__c;
                ApptIds.add(appointmentDetail.UniqueApptId__c);
                appointmentDetails.add(appointmentDetail);
            }
        }

        List<ServiceAppointment> appointments = [
                SELECT Id, UniqueApptId__c
                FROM ServiceAppointment
                WHERE UniqueApptId__c IN :ApptIds
        ];

        Map<String, String> appointmentsIdMap = new Map<String, String>();
        for (ServiceAppointment appointment : appointments) {
            appointmentsIdMap.put(appointment.UniqueApptId__c, appointment.Id);
        }

        List<ServiceAppointmentDetail__c> finalAppointmentDetails = new List<ServiceAppointmentDetail__c>();

        for (ServiceAppointmentDetail__c appointmentDetail : appointmentDetails) {
            String parrentId = appointmentsIdMap.get(appointmentDetail.UniqueApptId__c);
            if(String.isNotBlank(parrentId)) {
                appointmentDetail.ServiceAppointment__c = parrentId;
                finalAppointmentDetails.add(appointmentDetail);
            }
        }

        system.debug(finalAppointmentDetails);

        return finalAppointmentDetails;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'ComplaintCode' => ServiceAppointmentDetail__c.ComplaintCode__c,
            'EstimatedDuration' => ServiceAppointmentDetail__c.EstimatedDuration__c,
            'HostItemID' => ServiceAppointmentDetail__c.HostItemID__c,
            'ApptID' => ServiceAppointmentDetail__c.ApptID__c,
            'ComeBack' => ServiceAppointmentDetail__c.ComeBack__c,
            'Cost' => ServiceAppointmentDetail__c.Cost__c,
            'Date' => ServiceAppointmentDetail__c.Date__c,
            'ErrorLevel' => ServiceAppointmentDetail__c.ErrorLevel__c,
            'LaborType' => ServiceAppointmentDetail__c.LaborType__c,
            'LineCode' => ServiceAppointmentDetail__c.LineCode__c,
            'LineDispatchCode' => ServiceAppointmentDetail__c.LineDispatchCode__c,
            'OpCodeDesc' => ServiceAppointmentDetail__c.OpCodeDesc__c,
            'Sale' => ServiceAppointmentDetail__c.Sale__c,
            'SeqNo' => ServiceAppointmentDetail__c.SeqNo__c,
            'ServiceDesc' => ServiceAppointmentDetail__c.ServiceDesc__c,
            'ServiceRequest' => ServiceAppointmentDetail__c.ServiceRequest__c,
            'SoldHours' => ServiceAppointmentDetail__c.SoldHours__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, NAMESPACE);

            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}