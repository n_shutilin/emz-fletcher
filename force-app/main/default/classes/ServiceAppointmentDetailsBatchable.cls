global class ServiceAppointmentDetailsBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    public Integer recordsProcessed = 0;
    public Integer recordsTotal = 0;
    public static final String ACCOUNT_DEALER_RT = 'Dealer';
    public Integer deltaDays;
    public Boolean isBulk;
    public String dealerId;

    global ServiceAppointmentDetailsBatchable() {
        this.deltaDays = 1;
        this.isBulk = false;
    }

    global ServiceAppointmentDetailsBatchable(Integer deltaDays, Boolean isBulk) {
        this.deltaDays = deltaDays;
        this.isBulk = isBulk;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
            'SELECT Id, Dealer_Code__c, Name, CDK_Dealer_Code__c ' +
            'FROM Account ' +
            'WHERE RecordType.DeveloperName = :ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null AND Dealer_Code__c != null';

            if (String.isNotBlank(dealerId)) {
                query += ' AND Dealer_Code__c = :dealerId';
            }

        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        for (Account acc : scope) {
            try {
                List<ServiceAppointmentDetail__c> appointmentDetails = ServiceAppointmentHandler.getAppointmentDetails(acc.CDK_Dealer_Code__c, deltaDays, acc.Id, isBulk);
                Database.UpsertResult[] results = Database.upsert(appointmentDetails, ServiceAppointmentDetail__c.fields.UniqueApptDetailedId__c, true);
                recordsProcessed += scope.size();
                logSucess(appointmentDetails.size(), acc.Dealer_Code__c);
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, e.getStackTraceString());
                System.debug(LoggingLevel.ERROR, e.getMessage());
                logError(e.getTypeName(), e.getMessage(), acc.Dealer_Code__c);
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
    }

    global static void logSucess(Integer size, String dealer){
        Long timeStamp = DateTime.now().getTime();
        Integration_Logs__c sucessLog = new Integration_Logs__c(
            Endpoint__c = '*/CDK Appointment Integration Details DEALER:' +  dealer +  ' SIZE:' + size,
            Process__c = 'CDK Appointment Integration',
            Result__c = 'OK',
            Posix_Time__c = timeStamp
        );
        insert sucessLog;
    }

    global static void logError(String errorType, String errorMessage, String dealer){
        Long timeStamp = DateTime.now().getTime();
        Integration_Logs__c errorLog = new Integration_Logs__c(
            Endpoint__c = '*/CDK Appointment Integration Details DEALER:' + dealer,
            Process__c = 'CDK Appointment Integration',
            Result__c = '',
            Error__c = errorType,
            Error_Message__c = errorMessage.length() > 255 ? errorMessage.substring(0,255) : errorMessage,
            Posix_Time__c = timeStamp
        );
        insert errorLog;
    }

}