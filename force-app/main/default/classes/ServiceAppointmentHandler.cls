public class ServiceAppointmentHandler {

    public static final String PATH_APPOINTMENTS = '/pip-extract/serviceappointments/extract';
    public static final String PATH_APPOINTMENTS_DETAILS = '/pip-extract/serviceappointmentsdetail/extract';
    public static final String QUERY_APPOINTMENTS_ID = 'APPT_Delta_H';
    public static final String QUERY_APPOINTMENTS_DETAILS_ID = 'APPT_Delta_D';
    public static final String QUERY_BULK_APPOINTMENTS_ID = 'APPT_Bulk_H';
    public static final String QUERY_BULK_APPOINTMENTS_DETAILS_ID = 'APPT_Bulk_D';
    public static final String DEALERID_PARAM = 'dealerId';
    public static final String QUERYID_PARAM = 'queryId';
    public static final String DELTADATE_PARAM = 'deltaDate';
    public static final String DELTADATE_FORMAT = 'MM/dd/yyyy';

    public static List<ServiceAppointment> getAppointments(String dealerId, Integer deltaDays, String accountId, Boolean isBulk) {
        Map<String, String> paramsMap = new Map<String, String>();
        if (!isBulk) {
            Datetime deltaDate = Datetime.now().addDays(-deltaDays);
            String deltaDaysStr = deltaDate.format(DELTADATE_FORMAT);
            paramsMap.put(DELTADATE_PARAM, deltaDaysStr);
        }
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, isBulk ? QUERY_BULK_APPOINTMENTS_ID : QUERY_APPOINTMENTS_ID);
        HttpResponse res = DMotorWorksApi.get(PATH_APPOINTMENTS, paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<ServiceAppointment> appointments = ServiceAppointmentParser.process(res.getBodyDocument(), accountId);
        System.debug('Appointments ' + appointments);
        return appointments;
    }
    
    public static List<ServiceAppointmentDetail__c> getAppointmentDetails(String dealerId, Integer deltaDays, Id accountId, Boolean isBulk) {
        Map<String, String> paramsMap = new Map<String, String>();
        if (!isBulk) {
            Datetime deltaDate = Datetime.now().addDays(-deltaDays);
            String deltaDaysStr = deltaDate.format(DELTADATE_FORMAT);
            paramsMap.put(DELTADATE_PARAM, deltaDaysStr);
        }
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, isBulk ? QUERY_BULK_APPOINTMENTS_DETAILS_ID : QUERY_APPOINTMENTS_DETAILS_ID);
        HttpResponse res = DMotorWorksApi.get(PATH_APPOINTMENTS_DETAILS, paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<ServiceAppointmentDetail__c> appointmentDetails = ServiceAppointmentChildParser.process(res.getBodyDocument(), accountId);
        System.debug('Appointment details ' + appointmentDetails);
        return appointmentDetails;
    }

}