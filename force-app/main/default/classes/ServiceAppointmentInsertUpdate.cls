public with sharing class ServiceAppointmentInsertUpdate {

    public static final String PIP_NAMESPACE = 'http://www.dmotorworks.com/pip-serviceappointment';
    public static final String XMLNS_NAMESPACE = 'http://schemas.xmlsoap.org/soap/envelope/';
    public static final String PATH_INSER_UPDATE = '/pip-serviceappointment/services/ServiceAppointmentInsertUpdate';
    public static final String PATH_SEARCH_CUSTOMER_VEHICLE = '/pip-extract/customer-vehicle-search/extract';
    public static final String QUERY_SEARCH_CUSTOMER_VEHICLE = 'CUST_VEH_Search';
    public static final String CODE_FAILURE = 'failure';
    public static final String CODE_SUCCESS = 'success';

    public static ServiceAppointment sendRequestToCDK(ServiceAppointment appointment, String customerNumber, String vehicleId, 
                String cdkDealerCode, String actionType, CDK_Vehicle__c relatedVehicle, Account customerRecord) {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();

        List<ServiceAppointmentDetail__c> lines = new List<ServiceAppointmentDetail__c>();
        lines = [
                SELECT Id, OpCode__c, ComplaintCode__c, ServiceRequest__c, LaborType__c, ComeBack__c, LineDispatchCode__c,
                        OpCodeDesc__c, SoldHours__c, EstimatedDuration__c, Shop_Charge__c, Force_Shop_Charge__c, Sale_Override__c,
                        LineCode__c
                FROM ServiceAppointmentDetail__c
                WHERE ServiceAppointment__c = :appointment.Id
        ];


        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', XMLNS_NAMESPACE, 's');

        dom.XmlNode body = envelope.addChildElement('Body', XMLNS_NAMESPACE, 's');
        dom.XmlNode insertTag = body.addChildElement(actionType, PIP_NAMESPACE, 'ns2');

        dom.XmlNode arg0 = insertTag.addChildElement('arg0', null, null);
        arg0.addChildElement('password', null, null)
                .addTextNode(settings.Password__c);
        arg0.addChildElement('username', null, null)
                .addTextNode(settings.Username__c);

        dom.XmlNode arg1 = insertTag.addChildElement('arg1', null, null);
        arg1.addChildElement('dealerId', null, null)
                .addTextNode(cdkDealerCode);

        dom.XmlNode arg2 = insertTag.addChildElement('arg2', null, null);
//        arg2.addChildElement('apptDate', null, null)
//                .addTextNode(appointment.EarliestStartTime.date().format());
//        arg2.addChildElement('apptTime', null, null)
//                .addTextNode(appointment.EarliestStartTime.time().hour() + ':' +
//                             appointment.EarliestStartTime.time().minute() + ':' +
//                             appointment.EarliestStartTime.time().second());
        if (actionType == 'update' || actionType == 'delete') {
            arg2.addChildElement('apptID', null, null).addTextNode( appointment.ApptID__c);
            arg2.addChildElement('checksum', null, null).addTextNode( appointment.CDK_Checksum__c);
        }
        arg2.addChildElement('apptDate', null, null)
                .addTextNode((appointment.ApptDate__c == null ? '' : appointment.ApptDate__c)); //( == null ? '' : )
        arg2.addChildElement('apptTime', null, null)
                .addTextNode((appointment.ApptTime__c == null ? '' : appointment.ApptTime__c));
        arg2.addChildElement('promiseDate', null, null)
                .addTextNode((appointment.PromiseDate__c == null ? '' : appointment.PromiseDate__c));
        arg2.addChildElement('promiseTime', null, null)
                .addTextNode((appointment.PromiseTime__c == null ? '' : appointment.PromiseTime__c));
        arg2.addChildElement('comments', null, null)
                .addTextNode((appointment.Comments__c == null ? '' : appointment.Comments__c + ' ') + 
                        (appointment.Contact.Name == null ? '' : appointment.Contact.Name) + 
                        (appointment.ServiceSpecials__c == null ? '' : appointment.ServiceSpecials__c));
        arg2.addChildElement('remarks', null, null)
                .addTextNode((appointment.Comments__c == null ? '' : appointment.Comments__c + ' ') + 
                (appointment.Contact.Name == null ? '' : appointment.Contact.Name) + 
                (appointment.ServiceSpecials__c == null ? '' : appointment.ServiceSpecials__c));
//        arg2.addChildElement('dispatchMakeCode', null, null)
//                .addTextNode('');

        arg2.addChildElement('lineCount', null, null)
                .addTextNode(String.valueOf(lines.size()));
        arg2.addChildElement('opCount', null, null)
                .addTextNode(String.valueOf(lines.size()));
        arg2.addChildElement('partsCount', null, null)
                .addTextNode('0');//HARDCODE

        dom.XmlNode dealer = arg2.addChildElement('dealer', null, null);
        dealer.addChildElement('reservationist', null, null)
                .addTextNode((appointment.SANumber__c == null ? '' : appointment.SANumber__c));
        dealer.addChildElement('serviceAdvisor', null, null)
                .addTextNode((appointment.SANumber__c == null ? '' : appointment.SANumber__c));
        //TODO: will be new object

        dom.XmlNode customer = arg2.addChildElement('customer', null, null);
        if(String.isBlank(vehicleId)) {
                dom.XmlNode customerName = customer.addChildElement('name', null, null);
                customerName.addChildElement('firstName', null, null)
                        .addTextNode((customerRecord.FirstName == null ? '' : customerRecord.FirstName));
                customerName.addChildElement('lastName', null, null)
                        .addTextNode(customerRecord.LastName);
                customerName.addChildElement('fullName', null, null)
                        .addTextNode((customerRecord.FirstName == null ? '' : customerRecord.FirstName + ' ') + customerRecord.LastName);
        } else {
                customer.addChildElement('contactInfo', null, null)
                .addTextNode((appointment.Email__c == null ? '' : appointment.Email__c));
                

                //        dom.XmlNode name = customer.addChildElement('name', null, null);
                //        name.addChildElement('fullName', null, null)
                //                .addTextNode('');

                dom.XmlNode partyID = customer.addChildElement('partyID', null, null);
                partyID.addChildElement('ID', null, null)
                        .addTextNode(customerNumber);
        }

        customer.addChildElement('waiterFlag', null, null)
                .addTextNode(appointment.WaiterFlag__c == 1 ? 'Y' : 'N');
        customer.addChildElement('transportation', null, null)
                .addTextNode((appointment.TransType__c == null || appointment.WaiterFlag__c == 1 ? '' : appointment.TransType__c));
        

        dom.XmlNode vehicle = arg2.addChildElement('vehicle', null, null);
        if(String.isBlank(vehicleId)) {
                vehicle.addChildElement('odometerStatus', null, null)
                        .addTextNode( relatedVehicle.Vehicle_Mileage__c != null ? String.valueOf(relatedVehicle.Vehicle_Mileage__c) : '');
                vehicle.addChildElement('model', null, null)
                        .addTextNode(relatedVehicle.Model__c);
                vehicle.addChildElement('modelYear', null, null)
                        .addTextNode(relatedVehicle.Year__c);
                vehicle.addChildElement('make', null, null)
                        .addTextNode(relatedVehicle.Make__c);
        } else {
                vehicle.addChildElement('vehicleID', null, null) //Service Vehicle ID OR Inventory Vehicle ID
                .addTextNode(vehicleId);
        }
        
        vehicle.addChildElement('warrantyExpirationDate', null, null)
                .addTextNode('');

        for (Integer i = 0; i < lines.size(); i++) {
            ServiceAppointmentDetail__c line = lines[i];
            dom.XmlNode appLine = arg2.addChildElement('lines', null, null);
            appLine.addChildElement('key', null, null)
                    .addTextNode(line.LineCode__c);
            appLine.addChildElement('complaint', null, null)
                    .addTextNode((line.ComplaintCode__c == null ? '' : line.ComplaintCode__c));
            appLine.addChildElement('serviceRequest', null, null)
                    .addTextNode((line.ServiceRequest__c == null ? '' : line.ServiceRequest__c));
            appLine.addChildElement('laborType', null, null)
                    .addTextNode((line.LaborType__c == null ? '' : line.LaborType__c));
            appLine.addChildElement('comebackFlag', null, null)
                    .addTextNode((line.ComeBack__c == null ? '' : line.ComeBack__c));
//            appLine.addChildElement('technician', null, null)
//                    .addTextNode('');
            appLine.addChildElement('dispatchCode', null, null)
                    .addTextNode((line.LineDispatchCode__c == null ? '' : line.LineDispatchCode__c));
            appLine.addChildElement('estDuration', null, null)
                    .addTextNode((line.EstimatedDuration__c == null ? '' : String.valueOf(line.EstimatedDuration__c)));
//            appLine.addChildElement('cause', null, null)
//                    .addTextNode('');
//            appLine.addChildElement('urgencyCode', null, null)
//                    .addTextNode('');
            appLine.addChildElement('concernCode', null, null)
                    .addTextNode((line.ComplaintCode__c == null ? '' : line.ComplaintCode__c));
//            appLine.addChildElement('serviceEstimate', null, null)
//                    .addTextNode('');
//            appLine.addChildElement('laborEstimate', null, null)
//                    .addTextNode('');
//            appLine.addChildElement('partsEstimate', null, null)
//                    .addTextNode('');
//            appLine.addChildElement('miscEstimate', null, null)
//                    .addTextNode('');
//            appLine.addChildElement('lubeEstimate', null, null)
//                    .addTextNode('');
//            appLine.addChildElement('subletEstimate', null, null)
//                    .addTextNode('');
//            appLine.addChildElement('taxEstimate', null, null)
//                    .addTextNode('');

            dom.XmlNode ops = appLine.addChildElement('ops', null, null);
            ops.addChildElement('key', null, null)
                    .addTextNode(String.valueOf(i));
            ops.addChildElement('opCode', null, null)
                    .addTextNode((line.OpCodeDesc__c == null ? '' : line.OpCodeDesc__c));
//            ops.addChildElement('opCodeDesc', null, null)
//                    .addTextNode('');
//            ops.addChildElement('labor', null, null)
//                    .addTextNode('');
            ops.addChildElement('soldHours', null, null)
                    .addTextNode((line.SoldHours__c == null ? '' : String.valueOf(line.SoldHours__c)));
            ops.addChildElement('saleOverride', null, null)
                    .addTextNode((line.Sale_Override__c == null ? '' : String.valueOf(line.Sale_Override__c)));
            ops.addChildElement('forceShopCharge', null, null)
                    .addTextNode((line.Force_Shop_Charge__c == null ? '' : String.valueOf(line.Force_Shop_Charge__c)));
            ops.addChildElement('shopCharge', null, null)
                    .addTextNode((line.Shop_Charge__c == null ? '' : String.valueOf(line.Shop_Charge__c)));

//            dom.XmlNode parts = ops.addChildElement('parts', null, null);
//            parts.addChildElement('key', null, null)
//                    .addTextNode('');
//            parts.addChildElement('partNo', null, null)
//                    .addTextNode('');
//            parts.addChildElement('partDesc', null, null)
//                    .addTextNode('');
//            parts.addChildElement('partQty', null, null)
//                    .addTextNode('');
//            parts.addChildElement('saleAmount', null, null)
//                    .addTextNode('');
//            parts.addChildElement('fixedSale', null, null)
//                    .addTextNode('');
//            parts.addChildElement('mfrCode', null, null)
//                    .addTextNode('');
//            parts.addChildElement('coreFlag', null, null)
//                    .addTextNode('');
//            parts.addChildElement('priceOrigin', null, null)
//                    .addTextNode('');
        }

        System.debug('INSERT SA');
        System.debug('request');
        System.debug(doc.toXmlString());

        HttpResponse response = DMotorWorksApi.post(PATH_INSER_UPDATE, doc.toXmlString());
        System.debug('response body ' + response.getBody());
        System.debug('response status  ' + response.getStatusCode());
        System.debug('response body document ' + response.getBodyDocument());

        return readInsertServiceAppointmentResponse(response, appointment, actionType, cdkDealerCode);
    }

    public static ServiceAppointment readInsertServiceAppointmentResponse(HttpResponse response, ServiceAppointment appointment, String actionType, String dealerCode) {
        if (response.getStatusCode() == 500) {
            Dom.XmlNode envelopeResult = response.getBodyDocument().getRootElement();
            Dom.XmlNode bodyResult = envelopeResult.getChildElement('Body', XMLNS_NAMESPACE);
            Dom.XmlNode fault = bodyResult.getChildElement('Fault', XMLNS_NAMESPACE);
            Dom.XmlNode detail = fault.getChildElement('detail', null);

            if (detail != null) {
                Dom.XmlNode ServiceAppointmentPIPException = detail.getChildElement('ServiceAppointmentPIPException', PIP_NAMESPACE);
                Dom.XmlNode message = ServiceAppointmentPIPException.getChildElement('message', null);
                appointment.CDK_Message__c = message.getText();
            } else {
                Dom.XmlNode faultstring = fault.getChildElement('faultstring', null);
                appointment.CDK_Message__c = faultstring.getText();
            }
        } else {
            Dom.XmlNode envelopeResult = response.getBodyDocument().getRootElement();
            Dom.XmlNode bodyResult = envelopeResult.getChildElement('Body', XMLNS_NAMESPACE);
            String responseNodeName = actionType + 'Response';
            Dom.XmlNode responseNode = bodyResult.getChildElement(responseNodeName, PIP_NAMESPACE);
            Dom.XmlNode returnResult = responseNode.getChildElement('return', null);
            Dom.XmlNode code = returnResult.getChildElement('code', null);

            if (code != null) {
                    system.debug(code.getText());
                if (code.getText() == CODE_FAILURE) {
                    Dom.XmlNode message = returnResult.getChildElement('message', null);
                    appointment.CDK_Message__c = 'Service Appointment Insert error: ' + (message.getText().length() >= 10000 ? message.getText().substring(0, 10000) : message.getText());
                    system.debug(appointment.CDK_Message__c);

                } else if (code.getText() == CODE_SUCCESS) {
                    Dom.XmlNode serviceAppointment = returnResult.getChildElement('serviceAppointment', null);
                    Dom.XmlNode apptID = serviceAppointment.getChildElement('apptID', null);
                    appointment.ApptID__c = apptID.getText();
                    appointment.UniqueApptId__c = dealerCode + '_' + apptID.getText();
                //     appointment.Linked_RO__c = apptID.getText();
                    Dom.XmlNode checksum = serviceAppointment.getChildElement('checksum', null);
                    appointment.CDK_Checksum__c = checksum.getText();
                    appointment.CDK_Message__c = '';
                }
            }
        }

        return appointment;
    }

    public static Dom.Document searchCustomerVehicle(String dealerId, String qparamType, String qparamValue) {
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put('dealerId', dealerId);
        paramsMap.put('qparamType', qparamType);
        paramsMap.put('qparamValue', qparamValue);
        paramsMap.put('queryId', QUERY_SEARCH_CUSTOMER_VEHICLE);
        HttpResponse response = DMotorWorksApi.get(PATH_SEARCH_CUSTOMER_VEHICLE, paramsMap);

        return response.getBodyDocument();
    }

}