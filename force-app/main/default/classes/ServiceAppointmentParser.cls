public class ServiceAppointmentParser {

    public static final String NAMESPACE = 'http://www.dmotorworks.com/pip-extract-service-appointments';
    public static final String CHILD_NAME = 'Appointments';

    public static List<ServiceAppointment> process(Dom.Document doc, String accountId) {
        List<ServiceAppointment> appointments = new List<ServiceAppointment>();
        Account dealerAccount = [SELECT Id, CDK_Dealer_Code__c, Dealer_Code__c FROM Account WHERE Id = :accountId LIMIT 1];
        String appointmentRecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('VehicleServiceAppointment').getRecordTypeId();
        String cdkDealerCode = dealerAccount?.CDK_Dealer_Code__c;
        String dealerCode = dealerAccount?.Dealer_Code__c;

        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == CHILD_NAME) {
                ServiceAppointment appointment = new ServiceAppointment();
                appointment.RecordTypeId = appointmentRecordTypeId;
                appointment.Received_from_CDK__c = true;
                mapFields(appointment, child);
                appointment.UniqueApptId__c = cdkDealerCode + '_' + appointment.ApptID__c;
                appointment.DealerId__c = dealerCode;
                appointment.Linked_RO__c = appointment.ApptID__c;
                if (appointment.AppointmentDate__c != null && appointment.AppointmentTime__c != null && appointment.PromiseDate__c != null && appointment.PromiseTime__c != null) {
                    appointment.EarliestStartTime = Datetime.valueOf(appointment.AppointmentDate__c + ' ' + appointment.AppointmentTime__c);
                    appointment.DueDate = Datetime.valueOf(appointment.PromiseDate__c + ' ' + appointment.PromiseTime__c);
                } else {
                    appointment.EarliestStartTime = Datetime.now();
                    appointment.DueDate = Datetime.now().addDays(1);
                }
                appointment.SchedStartTime = appointment.EarliestStartTime;
                appointment.SchedEndTime = appointment.EarliestStartTime.addMinutes(10);
                // appointment.Status = 'Scheduled';
                appointments.add(appointment);
            }
        }

        processAccountSearch(appointments, dealerCode);
        system.debug(appointments);
        defineRelatedContact(appointments);
        
        return appointments;
    }

    private static void defineRelatedContact(List<ServiceAppointment> appointmentList) {
        List<String> accIds = new List<String>();
        for (ServiceAppointment app : appointmentList) {
            accIds.add(app.ParentRecordId);
        }
        Map<Id, Account> relatedAccountsList = new Map<Id, Account>([SELECT Id, PersonContactId FROM Account WHERE Id IN :accIds]);
        for (ServiceAppointment app : appointmentList) {
            app.ContactId = relatedAccountsList.get(app.ParentRecordId).PersonContactId;
        }
    }

    private static void processAccountSearch(List<ServiceAppointment> appointmentList, String expectedDealer) {
        TriggerSettings__c settings = TriggerSettings__c.getOrgDefaults();
        String defaultParentRecordId = settings.DefaultCDKAppointmentAccount__c;
        List<String> expectedCDKCustomerNumberList = formExpectedCustomNumberList(appointmentList);
        Map<String,String> appointmentToAccountMap = formEmptyAppointmentToAccountMap(appointmentList);
        List<Account> accountList = recieveAccountList(expectedDealer, expectedCDKCustomerNumberList);
        if(!accountList.isEmpty()){
            for(Account account : accountList){
                appointmentToAccountMap.put(account.CDK_Customer_Number__c, account.Id);
            }
        }
        if(appointmentToAccountMap.values().contains('empty')){
            expectedCDKCustomerNumberList = filterCustomerNumberList(expectedCDKCustomerNumberList, appointmentToAccountMap);
            List<CDK_Customer__c> customerList = recieveCustomerList(expectedDealer, expectedCDKCustomerNumberList);
            List<CDK_Customer__c> businessCustomerList = new List<CDK_Customer__c>();
            List<CDK_Customer__c> individualCustomerList = new List<CDK_Customer__c>();
            splitCustomerList(customerList, businessCustomerList, individualCustomerList);
            if(businessCustomerList.isEmpty() && individualCustomerList.isEmpty()){
                fillAppointmentToAccountMap(appointmentToAccountMap, defaultParentRecordId);
            } else {
                if(!businessCustomerList.isEmpty()){
                    appointmentToAccountMap = processBusinessCustomerLogic(appointmentToAccountMap, businessCustomerList, expectedDealer);
                }
                if(!individualCustomerList.isEmpty()){
                    appointmentToAccountMap = processIndividualCustomerLogic(appointmentToAccountMap, individualCustomerList, expectedDealer);
                }
            }
            if(appointmentToAccountMap.values().contains('empty')){
                fillAppointmentToAccountMap(appointmentToAccountMap, defaultParentRecordId);
            }
        }
        for(ServiceAppointment appointment : appointmentList){
            appointment.ParentRecordId = appointmentToAccountMap.get(appointment.CustNo__c);
        }
    }

    private static Map<String,String> formEmptyAppointmentToAccountMap(List<ServiceAppointment> appointmentList) {
        Map<String,String> appointmentToAccountMap = new Map<String,String>();
        for(ServiceAppointment appointment : appointmentList){
            appointmentToAccountMap.put(appointment.CustNo__c, 'empty');
        }
        return appointmentToAccountMap;
    }

    private static List<String> formExpectedCustomNumberList(List<ServiceAppointment> appointmentList) {
        List<String> numberList = new List<String>();
        for(ServiceAppointment appointment : appointmentList){
            numberList.add(appointment.CustNo__c);
        }
        return numberList;
    }

    private static List<String> filterCustomerNumberList(List<String> listToFilter, Map<String,String> appointmentToAccountMap){
        for(String key : appointmentToAccountMap.keySet()){
            if(appointmentToAccountMap.get(key) != 'empty'){
                for(Integer i = 0; i<listToFilter.size(); i++){
                    if(listToFilter.get(i) == key){
                        listToFilter.remove(i);
                        break;
                    }
                }
            }
        }
        return listToFilter;
    }

    private static List<Account> recieveAccountList(String expectedDealer, List<String> expectedCDKCustomerNumberList) {
        List<Account> targetAccountList = [
            SELECT Id, CDK_Customer_Number__c
            FROM Account 
            WHERE (Dealer__c != null AND Dealer__c = :expectedDealer) 
              AND (CDK_Customer_Number__c != null AND CDK_Customer_Number__c IN :expectedCDKCustomerNumberList)];
        return targetAccountList;
    }

    private static List<CDK_Customer__c> recieveCustomerList(String expectedDealer, List<String> expectedCDKCustomerNumberList) {
        List<CDK_Customer__c> customerList = [
            SELECT Id, FirstName__c, LastName__c, Name1_CDK__c, Home_Phone__c, OtherPhone__c, Phone__c, MobilePhone__c, Address__c, Email__c,
                   City__c, Postal_Code__c, State__c, Dealer_ID__c, Customer_Control_Number__c
            FROM CDK_Customer__c
            WHERE Dealer_ID__c = :expectedDealer AND Customer_Control_Number__c IN :expectedCDKCustomerNumberList];
        return customerList;
    }

    private static void splitCustomerList(List<CDK_Customer__c> listToSplit, List<CDK_Customer__c> businessCustomerList, List<CDK_Customer__c> individualCustomerList) {
        for(CDK_Customer__c customer : listToSplit){
            if(customer.FirstName__c == null && (customer.LastName__c != null && customer.Name1_CDK__c != null)){
                businessCustomerList.add(customer);
            } else if(customer.FirstName__c != null && customer.LastName__c != null) {
                individualCustomerList.add(customer);
            }
        }
    }

    private static void fillAppointmentToAccountMap(Map<String,String> appointmentToAccountMap, String accountId){
        for(String appointmentNumber : appointmentToAccountMap.keySet()){
            if(appointmentToAccountMap.get(appointmentNumber) == 'empty'){
                appointmentToAccountMap.put(appointmentNumber, accountId);
            }
        }
    }

    private static List<String> collectSObjectFieldByName(List<SObject> objectList, String fieldName){
        List<String> fieldValueList = new List<String>();
        for(SObject obj : objectList){
            fieldValueList.add(String.valueOf(obj.get(fieldName.toLowerCase())));
        }
        return fieldValueList;
    }

    private static String getPersonAccountRecordTypeId() {
        // RecordType personAccountRecordType = [
        //     SELECT Id 
        //     FROM RecordType 
        //     WHERE Name = 'Person Account' 
        //     AND SObjectType = 'Account'];
        // return personAccountRecordType.Id;
        return Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
    }

    private static String formPhone(String rawPhone){
        List<String> symbolsToReplace = new List<String>{'.',' ','-','(',')','1'};
        String formedPhone = '';
        if(rawPhone != null){
            formedPhone = rawPhone;
            for(String toReplace : symbolsToReplace){
                formedPhone = formedPhone.replace(toReplace, '');
            }
        }
        return formedPhone;
    }

    private static String formAddress(String rawAdress){
        String formedAddress = '';
        if(rawAdress != null){
            formedAddress = rawAdress.replaceAll(' ', '');
            formedAddress = formedAddress.length() > 5 ? formedAddress.substring(0, 5) : formedAddress;
        }
        return formedAddress;
    }

    private static Boolean validateByPhone(Account account, List<String> phoneList){
        Boolean isValid = false;
        if(account.Phone_Pool__c != null){
            for(String phone : phoneList){
                if(account.Phone_Pool__c.contains(phone)){
                    isValid = true;
                    break;
                }
            }
        }
        return isValid;
    }

    private static List<Account> filterAccountsByPhoneOrEmail(List<Account> accountList, List<String> phoneList, String email) {
        List<Account> validAccountList = new List<Account>();
        for(Account account : accountList){
            if((account.PersonEmail != null && account.PersonEmail == email) || validateByPhone(account, phoneList)){
                validAccountList.add(account);
            }
        }
        return validAccountList;
    }

    private static List<Account> filterAccountsByAdress(List<Account> accountList, String formedAddress) {
        List<Account> validAccountList = new List<Account>();
        for(Account account : accountList){
            if(account.Street_Left_5_String__c == formedAddress){
                validAccountList.add(account);
            }
        }
        return validAccountList;
    }

    private static Account createNewBusinessAccount(CDK_Customer__c customer, String recordTypeId){
        Account account = new Account(
            BillingCity = customer.City__c,
            BillingPostalCode = customer.Postal_Code__c,
            BillingState = customer.State__c,
            BillingStreet = customer.Address__c,
            Business_Account__c = true,
            Business_Name__c = String.isBlank(customer.Name1_CDK__c) ? customer.LastName__c : customer.Name1_CDK__c,
            CDK_Customer_Number__c = customer.Customer_Control_Number__c,
            Dealer_ID__pc = customer.Dealer_ID__c,
            Dealer__c = customer.Dealer_ID__c,
            LastName = String.isBlank(customer.Name1_CDK__c) ? customer.LastName__c : customer.Name1_CDK__c,
            PersonEmail = customer.Email__c,
            PersonHomePhone = customer.Home_Phone__c,
            PersonMobilePhone = customer.MobilePhone__c,
            PersonOtherPhone = customer.OtherPhone__c,
            Phone = customer.Phone__c,
            RecordTypeId = recordTypeId
        );
        return account;
    }

    private static Map<String,String> processBusinessCustomerLogic(Map<String,String> appointmentToAccountMap, List<CDK_Customer__c> businessCustomerList, String expectedDealer){
        String recordTypeId = getPersonAccountRecordTypeId();
        List<String> nameList = collectSObjectFieldByName(businessCustomerList, 'LastName__c');
        List<String> name1CDKList = collectSObjectFieldByName(businessCustomerList, 'Name1_CDK__c');
        List<Account> accountToInsertList = new List<Account>();

        List<Account> accountList = [
            SELECT Id, Phone_Pool__c, Street_Left_5_String__c, PersonEmail
            FROM Account
            WHERE RecordTypeId = :recordTypeId
              AND Name != null AND (Name IN :name1CDKList OR Name IN :nameList)
              AND Dealer__c != null AND (Dealer__c = :expectedDealer)
              AND ((Phone_Pool__c !=null OR PersonEmail !=null) OR (BillingStreet != null AND Street_Left_5_String__c !=null))];
        
        if(accountList.isEmpty()){
            for(CDK_Customer__c customer : businessCustomerList){
                accountToInsertList.add(createNewBusinessAccount(customer, recordTypeId));
            }
        } else {
            for(CDK_Customer__c customer : businessCustomerList){ 
                String formedHomePhone = formPhone(String.valueOf(customer.Home_Phone__c));
                String formedOtherPhone = formPhone(String.valueOf(customer.OtherPhone__c));
                String formedPhone = formPhone(String.valueOf(customer.Phone__c));
                String formedMobilePhone = formPhone(String.valueOf(customer.MobilePhone__c));
                String formedAddress = formAddress(customer.Address__c);
                List<Account> validAccountList = filterAccountsByPhoneOrEmail(
                    accountList, new List<String>{formedHomePhone, formedOtherPhone, formedPhone, formedMobilePhone}, String.valueOf(customer.Email__c));
                if(!validAccountList.isEmpty()){
                    appointmentToAccountMap.put(customer.Customer_Control_Number__c, validAccountList.get(0).Id);
                } else {
                    validAccountList = filterAccountsByAdress(accountList, formedAddress);
                    if(!validAccountList.isEmpty()){
                        appointmentToAccountMap.put(customer.Customer_Control_Number__c, validAccountList.get(0).Id);
                    } else {
                        accountToInsertList.add(createNewBusinessAccount(customer, recordTypeId));
                    }
                }
            }
        }
        if(!accountToInsertList.isEmpty()){
            insert accountToInsertList;
            for(Account account : accountToInsertList){
                appointmentToAccountMap.put(account.CDK_Customer_Number__c, account.Id);
            }
        }
        return appointmentToAccountMap;
    }

    private static Account createNewIndividualAccount(CDK_Customer__c customer, String recordTypeId){
        Account account = new Account(
            BillingCity = customer.City__c,
            BillingPostalCode = customer.Postal_Code__c,
            BillingState = customer.State__c,
            BillingStreet = customer.Address__c,
            CDK_Customer_Number__c = customer.Customer_Control_Number__c,
            Dealer_ID__pc = customer.Dealer_ID__c,
            Dealer__c = customer.Dealer_ID__c,
            FirstName = customer.FirstName__c,
            LastName = customer.LastName__c,
            PersonEmail = customer.Email__c,
            PersonHomePhone = customer.Home_Phone__c,
            PersonMobilePhone = customer.MobilePhone__c,
            PersonOtherPhone = customer.OtherPhone__c,
            Phone = customer.Phone__c,
            RecordTypeId = recordTypeId
        );
        return account;
    }

    private static Map<String,String> processIndividualCustomerLogic(Map<String,String> appointmentToAccountMap, List<CDK_Customer__c> individualCustomerList, String expectedDealer){
        String recordTypeId = getPersonAccountRecordTypeId();
        List<String> firstNameList = collectSObjectFieldByName(individualCustomerList, 'FirstName__c');
        List<String> lastNameList = collectSObjectFieldByName(individualCustomerList, 'LastName__c');
        List<Account> accountToInsertList = new List<Account>();

        List<Account> accountList = [
            SELECT Id, Phone_Pool__c, Street_Left_5_String__c, PersonEmail
            FROM Account
            WHERE RecordTypeId = :recordTypeId 
              AND FirstName IN :firstNameList
              AND LastName IN :lastNameList
              AND Dealer__c != null AND (Dealer__c = :expectedDealer)
              AND ((Phone_Pool__c !=null OR PersonEmail !=null))]; 
              
        if(accountList.isEmpty()){
            for(CDK_Customer__c customer : individualCustomerList){
                accountToInsertList.add(createNewIndividualAccount(customer, recordTypeId));
            }
        } else {
            for(CDK_Customer__c customer : individualCustomerList){
                String formedHomePhone = formPhone(String.valueOf(customer.Home_Phone__c));
                String formedOtherPhone = formPhone(String.valueOf(customer.OtherPhone__c));
                String formedPhone = formPhone(String.valueOf(customer.Phone__c));
                String formedMobilePhone = formPhone(String.valueOf(customer.MobilePhone__c));

                List<Account> validAccountList = filterAccountsByPhoneOrEmail(
                    accountList, new List<String>{formedHomePhone, formedOtherPhone, formedPhone, formedMobilePhone}, String.valueOf(customer.Email__c));
                if(!validAccountList.isEmpty()){
                    appointmentToAccountMap.put(customer.Customer_Control_Number__c, validAccountList.get(0).Id);
                } else {
                    accountToInsertList.add(createNewIndividualAccount(customer, recordTypeId));
                }
            }
        }
        if(!accountToInsertList.isEmpty()){
            insert accountToInsertList;
            for(Account account : accountToInsertList){
                appointmentToAccountMap.put(account.CDK_Customer_Number__c, account.Id);
            }
        }
        return appointmentToAccountMap;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'HostItemID' => ServiceAppointment.HostItemID__c,
            'AppointmentDate' => ServiceAppointment.AppointmentDate__c,
            'AppointmentTime' => ServiceAppointment.AppointmentTime__c,
            'ApptAddress' => ServiceAppointment.ApptAddress__c,
            'ApptCityStateZip' => ServiceAppointment.ApptCityStateZip__c,
            'ApptContactPhone' => ServiceAppointment.ApptContactPhone__c,
            'ApptCustName' => ServiceAppointment.ApptCustName__c,
            'ApptDate' => ServiceAppointment.ApptDate__c,
            'ApptName' => ServiceAppointment.ApptName__c,
            'ApptID' => ServiceAppointment.ApptID__c,
            'ApptMileage' => ServiceAppointment.ApptMileage__c,
            'ApptOpenDate' => ServiceAppointment.ApptOpenDate__c,
            'ApptOpenTime' => ServiceAppointment.ApptOpenTime__c,
            'ApptTime' => ServiceAppointment.ApptTime__c,
            'BusinessPhone' => ServiceAppointment.BusinessPhone__c,
            'Cellular' => ServiceAppointment.Cellular__c,
            'Comments' => ServiceAppointment.CDK_Comments__c,
            'ComplaintSummary' => ServiceAppointment.ComplaintSummary__c,
            'CustNo' => ServiceAppointment.CustNo__c,
            'EMail' => ServiceAppointment.EMail__c,
            'ErrorLevel' => ServiceAppointment.ErrorLevel__c,
            'FirstName' => ServiceAppointment.FirstName__c,
            'HomePhone' => ServiceAppointment.HomePhone__c,
            'LastName' => ServiceAppointment.LastName__c,
            'Make' => ServiceAppointment.Make__c,
            'MakeAbbr' => ServiceAppointment.MakeAbbr__c,
            'MiddleName' => ServiceAppointment.MiddleName__c,
            'Model' => ServiceAppointment.Model__c,
            'nofModel' => ServiceAppointment.NofModel__c,
            'nofYear' => ServiceAppointment.NofYear__c,
            'NoShowFlag' => ServiceAppointment.NoShowFlag__c,
            'PromiseDate' => ServiceAppointment.PromiseDate__c,
            'PromiseTime' => ServiceAppointment.PromiseTime__c,
            // 'Remarks' => ServiceAppointment.Remarks__c,
            'Reservationist' => ServiceAppointment.Reservationist__c,
            'SalesPerson' => ServiceAppointment.SalesPerson__c,
            'SalesPersonID' => ServiceAppointment.SalesPersonId__c,
            'SAName' => ServiceAppointment.SAName__c,
            'SANumber' => ServiceAppointment.SANumber__c,
            'TransType' => ServiceAppointment.TransType__c,
            'TotalDuration' => ServiceAppointment.TotalDuration__c,
            'Vehid' => ServiceAppointment.Vehid__c,
            'VehidOrCust' => ServiceAppointment.VehidOrCust__c,
            'VIN' => ServiceAppointment.VIN__c,
            'WaiterFlag' => ServiceAppointment.WaiterFlag__c,
            'DELETEPIIDATA' => ServiceAppointment.DELETEPIIDATA__c,
            'OPTOUTSHARINGPIIDATA' => ServiceAppointment.OPTOUTSHARINGPIIDATA__c,
            'OPTOUTSELLINGPIIDATA' => ServiceAppointment.OPTOUTSELLINGPIIDATA__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, NAMESPACE);

            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}