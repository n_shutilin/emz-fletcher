@IsTest
public class ServiceAppointmentParserTest {

    private static Dom.Document getXML(){
        StaticResource staticResourceTest = [
            SELECT body 
            FROM StaticResource 
            WHERE Name = 'APPOINTMENT_PARSER_TEST'
        ];

        Dom.Document XML = new Dom.Document();
        XML.load(staticResourceTest.body.toString());
        return XML;
    }

    private static String getPersonAccountRecordTypeId() {
        RecordType personAccountRecordType = [
            SELECT Id 
            FROM RecordType 
            WHERE Name = 'Person Account' 
            AND SObjectType = 'Account'];
        return personAccountRecordType.Id;
    }

    @TestSetup
    static void init(){
        Account defaultAccount = new Account(
            LastName = 'defaultAccount',
            RecordTypeId = getPersonAccountRecordTypeId()
        );
        insert defaultAccount;

        TriggerSettings__c testSettings = new TriggerSettings__c(
            DefaultCDKAppointmentAccount__c = defaultAccount.Id
        );
        insert testSettings;

        Account dealerAccount = new Account(
            LastName = 'testDealer',
            CDK_Dealer_Code__c = 'testCode',
            Dealer_Code__c = 'AUDFJ'
        );
        insert dealerAccount;
    }

    @IsTest
    static void defaultAccountTest(){
        Dom.Document XML = getXML();
        Account dealerAccount = [SELECT Id FROM Account WHERE LastName = 'testDealer' LIMIT 1];
        List<ServiceAppointment> appointmentList = ServiceAppointmentParser.process(XML, dealerAccount.Id);
    }

    @IsTest
    static void defaultCustomerTest(){
        CDK_Customer__c testCustomer = new CDK_Customer__c(
            Dealer_ID__c = 'AUDFJ',
            Customer_Control_Number__c = '121699'
        );
        insert testCustomer;
        Dom.Document XML = getXML();
        Account dealerAccount = [SELECT Id FROM Account WHERE LastName = 'testDealer' LIMIT 1];
        List<ServiceAppointment> appointmentList = ServiceAppointmentParser.process(XML, dealerAccount.Id);
    }

    @IsTest
    static void businessCustomerTest(){
        CDK_Customer__c testCustomer = new CDK_Customer__c(
            Dealer_ID__c = 'AUDFJ',
            Customer_Control_Number__c = '121699',
            LastName__c = 'businessCustomer',
            Name1_CDK__c = 'testCDKName'
        );
        insert testCustomer;
        Dom.Document XML = getXML();
        Account dealerAccount = [SELECT Id FROM Account WHERE LastName = 'testDealer' LIMIT 1];
        Account defaultAccount = [SELECT Id FROM Account WHERE LastName = 'defaultAccount' LIMIT 1];
        List<ServiceAppointment> appointmentList = ServiceAppointmentParser.process(XML, dealerAccount.Id);
        System.assert(appointmentList.get(0).ParentRecordId != defaultAccount.Id);
    }

    @IsTest
    static void individualCustomerTest(){
        CDK_Customer__c testCustomer = new CDK_Customer__c(
            Dealer_ID__c = 'AUDFJ',
            Customer_Control_Number__c = '121699',
            LastName__c = 'individualCustomer LastName',
            FirstName__c = 'individualCustomer FirstName'
        );
        insert testCustomer;
        Dom.Document XML = getXML();
        Account dealerAccount = [SELECT Id FROM Account WHERE LastName = 'testDealer' LIMIT 1];
        Account defaultAccount = [SELECT Id FROM Account WHERE LastName = 'defaultAccount' LIMIT 1];
        List<ServiceAppointment> appointmentList = ServiceAppointmentParser.process(XML, dealerAccount.Id);
        System.assert(appointmentList.get(0).ParentRecordId != defaultAccount.Id);
    }
}