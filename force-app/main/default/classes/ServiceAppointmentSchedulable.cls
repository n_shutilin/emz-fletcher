global class ServiceAppointmentSchedulable implements Schedulable{

    global void execute(SchedulableContext SC) {
        ServiceAppointmentBatchable batch = new ServiceAppointmentBatchable(0);
        Database.executeBatch(batch, 1);

        Datetime nextScheduleTime = System.now().addMinutes(120);
        String hour = String.valueOf(nextScheduleTime.hour());
        String minutes = String.valueOf(nextScheduleTime.minute());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        String jobName = 'Appointment Extract ' + nextScheduleTime.format('hh:mm');

        ServiceAppointmentSchedulable p = new ServiceAppointmentSchedulable();
        System.schedule(jobName, cronValue , p);

        System.abortJob(SC.getTriggerId());
    }

}