public with sharing class ServiceAppointmentService {

    public static final String PREFIX_ID = '4*';

    public static String sendAppointmentIntoCDK(String recordId, String actionType) {
        ServiceAppointment appointment = (ServiceAppointment)retrieveServiceAppointment(recordId);

        ServiceAppointment updatedAppointment;
        String customerNumber;
        String vehicleId;

        Account customer = retrieveCustomer(appointment.ParentRecordId);
        customerNumber = getCustomerNumber(appointment, customer);


        if (String.isNotBlank(customer.CDK_Customer_Message__c)) {
            return customer.CDK_Customer_Message__c;
        }

        if (String.isBlank(customerNumber)) {
            return 'Please check Customer phone and email';
        }

        CDK_Vehicle__c vehicle;

        if (String.isNotBlank(appointment.CDK_Vehicle__c)) {
            vehicle = (CDK_Vehicle__c)retrieveServiceVehicle(appointment.CDK_Vehicle__c);
        } else {
            return 'CDK Vehicle field should be fill out';
        }

        if (String.isNotBlank(appointment.DealerId__c)) {
            vehicleId = getVehicleId(appointment, vehicle, customerNumber);
        } else {
            return 'Dealer field should be fill out';
        }

        if (String.isNotBlank(vehicle.CDK_Message__c)) {
            return vehicle.CDK_Message__c;
        }

        vehicle.VehID__c = vehicleId;
        String cdkDealerCode = getCDKDealerCode(appointment.DealerId__c);

        System.debug('appointment');
        System.debug(appointment);
        updatedAppointment = ServiceAppointmentInsertUpdate.sendRequestToCDK(appointment, customerNumber, vehicleId, cdkDealerCode, actionType, vehicle, customer);
        updatedAppointment.CDK_Integration_Date__c = System.now();
        if (actionType == 'delete' && String.isBlank(updatedAppointment.CDK_Message__c)) {
            updatedAppointment.Status = 'Canceled';
            ServiceAppointmentTriggerHandler.cdkDeleteProcessed = true;
        }

        system.debug(updatedAppointment.CDK_Message__c);

        update customer;
        update vehicle;
        update updatedAppointment;

        if (String.isNotBlank(updatedAppointment.CDK_Message__c)) {
        system.debug(updatedAppointment.CDK_Message__c);
            
            return updatedAppointment.CDK_Message__c;
        }

        return 'OK';
    }

    @AuraEnabled
    public static String sendVehicleReownershipRequest(String recordId) {
        ServiceAppointment appointment = retrieveServiceAppointment(recordId);

        String cdkDealerCode = getCDKDealerCode(appointment.DealerId__c);
        if (String.isBlank(cdkDealerCode)) {
            return 'CDK Dealer Number is blank. Check Dealer Account.';
        }

        Account customer = retrieveCustomer(appointment.ParentRecordId);
        if (String.isBlank(customer.CDK_Customer_Number__c)) {
            return 'CDK Customer Number is blank. Check Parent Account';
        }

        CDK_Vehicle__c vehicle;
        if (String.isNotBlank(appointment.CDK_Vehicle__c)) {
            vehicle = (CDK_Vehicle__c) retrieveServiceVehicle(appointment.CDK_Vehicle__c);
        } else {
            return 'CDK Vehicle field should be fill out';
        }

        if (String.isBlank(vehicle.VIN__c)) {
            return 'VIN Code is required.';
        }

        ServiceVehicleInsertUpdate.VehicleIdWrapper vehicleIdWrapper = ServiceVehicleInsertUpdate.getVehicleId(cdkDealerCode, vehicle.VIN__c);
        String vehicleId = vehicleIdWrapper.vehiclesVehId; 
        if(String.isNotBlank(vehicleId)) {
            vehicle.VehID__c = vehicleId;
        } else {
            return 'Failed to get CDK Vehicle ID. Please contact the administrator';
        }

        vehicle = ServiceVehicleInsertUpdate.updateServiceVehicle(vehicle, cdkDealerCode, vehicleId, customer.CDK_Customer_Number__c);
        update vehicle;
        if(String.isBlank(vehicle.CDK_Message__c)) {
            return 'OK';
        } else {
            return vehicle.CDK_Message__c;
        }
    }

    public static ServiceAppointment retrieveServiceAppointment(String recordId) {
        ServiceAppointment appointment = [
            SELECT Id, HostItemID__c, AppointmentDate__c, AppointmentTime__c, ApptAddress__c, ApptCityStateZip__c,
                ApptContactPhone__c, ApptCustName__c, ApptDate__c, ApptName__c, ApptID__c, ApptMileage__c,
                ApptOpenDate__c, ApptOpenTime__c, ApptTime__c, BusinessPhone__c, Cellular__c, Comments__c,
                ComplaintSummary__c, CustNo__c, Email__c, ErrorLevel__c, FirstName__c, HomePhone__c,
                LastName__c, Make__c, MakeAbbr__c, MiddleName__c, Model__c, NofModel__c, NofYear__c,
                NoShowFlag__c, PromiseDate__c, PromiseTime__c, Reservationist__c, SalesPerson__c, Remarks__c,
                SalesPersonID__c, SAName__c, SANumber__c, TransType__c, TotalDuration__c, Vehid__c,
                VehidOrCust__c, VIN__c, WaiterFlag__c, DELETEPIIDATA__c, OPTOUTSHARINGPIIDATA__c, CDK_Vehicle__c,
                OPTOUTSELLINGPIIDATA__c, CDK_Checksum__c, CDK_Message__c, CDK_Integration_Date__c,
                CDK_Vehicle__r.VehID__c, CDK_Vehicle__r.VIN__c, ParentRecordId, DealerId__c, Status,
                Contact.Name, ServiceSpecials__c
            FROM ServiceAppointment
            WHERE Id = :recordId
            LIMIT 1
        ];

        return appointment;
    }

    public static String getCDKDealerCode(String dealerCode) {
        return [
            SELECT CDK_Dealer_Code__c
            FROM Account
            WHERE Dealer_Code__c = :dealerCode
            LIMIT 1
        ].CDK_Dealer_Code__c;
    }

    @TestVisible
    private static SObject retrieveServiceVehicle(String recordId) {
        CDK_Vehicle__c vehicle = [
            SELECT Id, AccountingAccount__c, ErrorLevel__c, Model__c, ModelName__c, TransmissionNo__c, Color__c,
                DealerCode__c, HostItemID__c, UnitNo__c, VIN__c, VehID__c, Year__c, StockNo__c, StockType__c,
                LastServiceDate__c, Make__c, MakeName__c, Mileage__c, DeliveryDate__c, CDK_Message__c, InServiceDate__c,
                In_Service_Date_Text__c, Delivery_Date_Text__c, Vehicle_Mileage__c, CreatedDate
            FROM CDK_Vehicle__c
            WHERE Id = :recordId
            LIMIT 1
        ];

        return vehicle;
    }

    @TestVisible
    private static Account retrieveCustomer(String customerId) {
        Account customer = [
            SELECT Id, CDK_Dealer_Code__c, CDK_Customer_Number__c, CDK_Customer_Message__c, CDK_Customer_Checksum__c,
                PersonMobilePhone, PersonHomePhone, PersonOtherPhone, FirstName, LastName, MiddleName, Company_Name__c,
                PersonMailingStreet, PersonMailingCity, PersonMailingCountry, Mailing_County__c, Customer_Type__c,
                PersonMailingPostalCode, PersonMailingState, PersonEmail, Email_Secondary__pc, PersonBirthdate,
                Customer_Primary_Phone__c, Employer__pc, Phone , Dealer__c
            FROM Account
            WHERE Id = :customerId
            LIMIT 1
        ];

        return customer;
    }

    @TestVisible
    private static String getVehicleId(ServiceAppointment appointment, CDK_Vehicle__c vehicle, String customerNumber) {
        if (String.isNotBlank(appointment.CDK_Vehicle__c)) {
            if (String.isNotBlank(appointment.CDK_Vehicle__r.VehID__c)) {
                return appointment.CDK_Vehicle__r.VehID__c;
            }

            ServiceVehicleInsertUpdate.VehicleIdWrapper vehicleId = new ServiceVehicleInsertUpdate.VehicleIdWrapper();

            String vehicleVIN = (String.isNotBlank(vehicle.VIN__c) ? vehicle.VIN__c : appointment.VIN__c);
            String cdkDealerCode = getCDKDealerCode(appointment.DealerId__c);

            if (String.isNotBlank(vehicleVIN)) {
                vehicleId = ServiceVehicleInsertUpdate.getVehicleId(cdkDealerCode, vehicleVIN);
            }

            if (vehicleId.newId != 'Y') {
                return vehicleId.vehiclesVehId;
            } else {
                CDK_Vehicle__c updatedVehicle = ServiceVehicleInsertUpdate.insertServiceVehicle(vehicle, cdkDealerCode, vehicleId.vehiclesVehId, customerNumber);

                return vehicleId.vehiclesVehId;
            }
        }

        return null;
    }

    @TestVisible
    private static String getSearchPhoneString(Account customer) {
        String searchPhone = '';

        switch on customer.Customer_Primary_Phone__c {
            when 'Mobile' {
                searchPhone = customer.PersonMobilePhone;
            }
            when 'Home' {
                searchPhone = customer.PersonHomePhone;
            }
            when 'Phone' {
                searchPhone = customer.Phone;
            }
            when else {
                if (String.isNotBlank(customer.Phone)) {
                    searchPhone = customer.Phone;
                } else if (String.isNotBlank(customer.PersonHomePhone)) {
                    searchPhone = customer.PersonHomePhone;
                } else if (String.isNotBlank(customer.PersonOtherPhone)) {
                    searchPhone = customer.PersonOtherPhone;
                }
            }
        }

        return searchPhone;
    }

    private static String getSearchKeyWordString(Account customer) {
        String searchKeyWord = '';

//        searchKeyWord =
//            (String.isNotBlank(appointment.Account.LastName) ? appointment.Account.LastName : '') +
//            (String.isNotBlank(appointment.Account.LastName) && String.isNotBlank(appointment.Account.FirstName) ? ',' : '') +
//            (String.isNotBlank(appointment.Account.FirstName) ? appointment.Account.FirstName : '') +
//            (String.isNotBlank(appointment.Account.MiddleName) ? ' ' + appointment.Account.MiddleName : '');

        if (String.isNotBlank(customer.PersonEmail)) {
            searchKeyWord = customer.PersonEmail;
        } else if (String.isNotBlank(customer.Email_Secondary__pc)) {
            searchKeyWord = customer.PersonEmail;
        }

        return searchKeyWord;
    }
    
	@TestVisible
    private static String getCustomerNumber(ServiceAppointment appointment, Account customer) {
        if (String.isNotBlank(customer.CDK_Customer_Number__c)) {
            return customer.CDK_Customer_Number__c;
        }

        String cdkDealerCode = getCDKDealerCode(appointment.DealerId__c);
        String searchKeyWord = getSearchKeyWordString(customer);
        String searchPhone = getSearchPhoneString(customer);

        if (String.isNotBlank(searchKeyWord) || String.isNotBlank(searchPhone)) {
            String customerNumber;

            if (String.isNotBlank(searchKeyWord)) {
                customerNumber = CustomerInsertUpdate.searchCustomerVehicle(cdkDealerCode, 'EMAIL', searchKeyWord);
            }

            if (String.isBlank(customerNumber) && String.isNotBlank(searchPhone)) {
                customerNumber = CustomerInsertUpdate.searchCustomerVehicle(cdkDealerCode, 'PHONE', searchPhone);
            }

            if (String.isNotBlank(customerNumber)) {
                customer.CDK_Customer_Number__c = customerNumber;
            } else {
                CustomerInsertUpdate.CustomerInfo info = CustomerInsertUpdate.insertUpdateCustomer(cdkDealerCode, appointment.ParentRecordId);
                if (info != null) {
                    if (String.isNotBlank(info.error)) {
                        customer.CDK_Customer_Message__c = info.error;
                    } else {
                        customer.CDK_Customer_Number__c = info.custId.remove(PREFIX_ID);
                        customer.CDK_Customer_Checksum__c = info.checksum;
                        customer.CDK_Customer_Message__c = '';
                    }
                }
            }

            return customer.CDK_Customer_Number__c;
        }

        return null;
    }
}