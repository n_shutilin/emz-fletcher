public with sharing class ServiceAppointmentTriggerHandler {
    public static final String DEFAULT_PARENT_ACCOUNT_ID = '001c000002RUmOJAA1';
    public static Boolean cdkDeleteProcessed = false;
    public static String dealerCode;

     // public static void updateRequiredFields(List<ServiceAppointment> newList) {
    //     for (ServiceAppointment appointment : newList) {
    //         system.debug('~~~~~~~~~~~~~~~~appointment');
    //         system.debug(appointment);
    //         //appointment.Status = 'None';
    //         if (String.isBlank(appointment.Status)) {
    //             appointment.Status = 'None';
    //         }
    //         appointment.DurationType = String.isNotBlank(appointment.DurationType) ? appointment.DurationType : 'Hours';

    //         if (appointment.Duration == NULL || appointment.Duration == 0) {
    //             appointment.Duration = appointment.TotalDuration__c != NULL || appointment.TotalDuration__c != 0 ? appointment.TotalDuration__c : 24;
    //         }

    //         // appointment.Duration = appointment.Duration != NULL || appointment.Duration != 0 ? appointment.Duration :  appointment.TotalDuration__c : 24;
    //         // appointment.Duration = appointment.TotalDuration__c != NULL || appointment.TotalDuration__c != 0 ? appointment.TotalDuration__c : 24;
    //         //appointment.UniqueApptId__c = appointment.DealerId__c + '_' + appointment.ApptID__c;
    //         if(String.isBlank(appointment.UniqueApptId__c)) {
    //             appointment.UniqueApptId__c = appointment.DealerId__c + '_' + appointment.ApptID__c;
    //         }
    //         //appointment.UniqueApptId__c = appointment.UniqueApptId__c != null 
    //         //    ? appointment.UniqueApptId__c
    //         //    : appointment.DealerId__c + '_' + appointment.ApptID__c;
    //         // appointment.ParentRecordId = String.isNotBlank(appointment.ParentRecordId) ? appointment.ParentRecordId : DEFAULT_PARENT_ACCOUNT_ID;
    //         if (String.isBlank(appointment.ParentRecordId)) {
    //             appointment.ParentRecordId = DEFAULT_PARENT_ACCOUNT_ID;
    //         }
    //         if (appointment.AppointmentDate__c != null && appointment.AppointmentTime__c != null && appointment.PromiseDate__c != null && appointment.PromiseTime__c != null) {
    //             appointment.EarliestStartTime = Datetime.valueOf(appointment.PromiseDate__c + ' ' + appointment.PromiseTime__c);
    //             //appointment.EarliestStartTime = Datetime.valueOf(appointment.AppointmentDate__c + ' ' + appointment.AppointmentTime__c);
    //             //appointment.DueDate = Datetime.valueOf(appointment.PromiseDate__c + ' ' + appointment.PromiseTime__c);
    //         } else {
    //             appointment.EarliestStartTime = Datetime.now();
    //             //appointment.DueDate = Datetime.now().addDays(1);
    //         }
    //         if(appointment.DueDate == null) {
    //             appointment.DueDate =  appointment.EarliestStartTime.addDays(1);
    //         }
    //         //appointment.DueDate =  appointment.EarliestStartTime.addDays(1);

    //         system.debug(appointment);
    //     }
    // }

    public static void processCDKDelete(Map<Id, ServiceAppointment> oldMap, Map<Id, ServiceAppointment> newMap) {
        if(!cdkDeleteProcessed){
            List<Id> idList = filterCDKAppointments(oldMap, newMap);
            if(!idList.isEmpty()){
                for(Id id : idList){
                    dispatchDelete(id);
                }
            }
        }
    }

    private static List<Id> filterCDKAppointments(Map<Id, ServiceAppointment> oldMap, Map<Id, ServiceAppointment> newMap) {
        List<Id> idList = new List<Id>();
        for(Id appointmentId : newMap.keySet()){
            if(newMap.get(appointmentId).ApptID__c != null && (newMap.get(appointmentId).Status == 'Canceled' && oldMap.get(appointmentId).Status != 'Canceled')){
                idList.add(appointmentId);
            }
        }
        return idList;
    }

    @Future(callout=true)
    private static void dispatchDelete(Id id) {
        ServiceAppointmentService.sendAppointmentIntoCDK(String.valueOf(id), 'delete');
    }

    public static void afterInsertHandler(List<ServiceAppointment> newList) {
        System.debug(newList);
        System.debug(dealerCode);
        List<ServiceAppointment> appointmentsForScheduling = new List<ServiceAppointment>();
        Set<String> advisorNumbers = new Set<String>();
        for (ServiceAppointment appointment : newList){
            if (appointment.Received_from_CDK__c) {
                ServiceAppointment updatedAppointment = new ServiceAppointment(
                    Id = appointment.Id,
                    Status = 'Scheduled',
                    Received_from_CDK__c = false,
                    SANumber__c = appointment.SANumber__c
                );
                appointmentsForScheduling.add(updatedAppointment);
                advisorNumbers.add(appointment.SANumber__c);
                
                
            }
        }
        System.debug(advisorNumbers);
        System.debug(appointmentsForScheduling);

        if (!appointmentsForScheduling.isEmpty() && String.isNotBlank(dealerCode)) {
            String dealerId = [SELECT Id, Dealer_Code__c FROM Account 
                WHERE RecordType.DeveloperName = 'Dealer' AND CDK_Dealer_Code__c = :dealerCode AND Dealer_Code__c != null LIMIT 1].Dealer_Code__c;
                System.debug(dealerId);

            List<ServiceResource> resources = [SELECT Id, ServiceAdvisorNumber__c FROM ServiceResource 
                WHERE ServiceAdvisorNumber__c IN :advisorNumbers AND Dealer__c = :dealerId];
                System.debug(resources);

            Map<String, String> serviceAdvisorsMap = new Map<String,String>();
            if (!resources.isEmpty()) {
                for (ServiceResource res : resources) {
                    serviceAdvisorsMap.put(res.ServiceAdvisorNumber__c, res.Id);
                }
            }
            System.debug(serviceAdvisorsMap);


            List<AssignedResource> assignedResources = new List<AssignedResource>();
            for (ServiceAppointment appointment : appointmentsForScheduling) {
                if ( String.isNotBlank(serviceAdvisorsMap.get(appointment.SANumber__c))) {
                    assignedResources.add(new AssignedResource(
                        ServiceAppointmentId = appointment.Id,
                        ServiceResourceId = serviceAdvisorsMap.get(appointment.SANumber__c)
                    ));
                }
            }

            System.debug('assignedResources');
            System.debug(assignedResources);

            insert assignedResources;
        }

        update appointmentsForScheduling;
    }
}