@IsTest
public with sharing class ServiceAppointmentTriggerHandlerTest {
    @TestSetup
    static void init(){
        insert new CDK_Integration__c(
            Password__c = 'TEST',
            Username__c = 'TEST');

        RecordType personAccountRecordType = [
            SELECT Id 
            FROM RecordType 
            WHERE Name = 'Person Account' 
            AND SObjectType = 'Account'];

        Account firstTestAccount = new Account();
            firstTestAccount.FirstName = 'TEST';
            firstTestAccount.LastName = 'TEST';
            firstTestAccount.RecordTypeId = personAccountRecordType.Id;
            firstTestAccount.PersonEmail = 'test@test.com';
        	firstTestAccount.Dealer_Code__c = 'AUDFJ';
        	firstTestAccount.CDK_Dealer_Code__c = 'TEST';
        insert firstTestAccount;
        
        String firstTestAccountPersonContactId = [
            SELECT PersonContactId 
            FROM Account 
            WHERE Id = :firstTestAccount.Id].PersonContactId;

        ServiceAppointment serviceAppointment = new ServiceAppointment(
           ParentRecordId = firstTestAccount.Id, 
           ContactId = firstTestAccountPersonContactId, 
           Status = 'none',
           ApptID__c = '1923917474369',
           DealerId__c = 'AUDFJ',
           Transportation_Notes_New__c = 'Test'
        );
        insert serviceAppointment; 
    }
    
    @IsTest
    public static void statusUpdateTest() {
        ServiceAppointment serviceAppointment = [SELECT Id, Status FROM ServiceAppointment LIMIT 1];
        Test.startTest();
        String responseBody = '<?xml version=\'1.0\' encoding=\'UTF-8\'?>'+
            +'<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">'+
                +'<S:Body>'+
                    +'<ns2:getCustomerNumberResponse xmlns:ns2="http://www.dmotorworks.com/pip-customer">'+
                        +'<return>'+
                            +'<code>success</code>'+
                            +'<customerNumber>6500</customerNumber>'+
                        +'</return>'+
                    +'</ns2:getCustomerNumberResponse>'+
                    +'<ns2:insertResponse xmlns:ns2="http://www.dmotorworks.com/pip-customer">'+
                        +'<return>'+
                            +'<code>success</code>'+
                            +'<message></message>'+
                            +'<customerParty>'+
                                +'<checksum>11406622</checksum>'+
                                +'<id>'+
                                    +'<value>6500</value>'+
                                +'</id>'+
                                +'<arStatus />'+
                                +'<address>'+
                                    +'<addressLine>Tamarack Ave 22</addressLine>'+
                                    +'<city>Carlsbad</city>'+
                                    +'<country>USA</country>'+
                                    +'<county>San Diego</county>'+
                                    +'<postalCode>92008</postalCode>'+
                                    +'<stateOrProvince>CA</stateOrProvince>'+
                                +'</address>'+
                                +'<contactInfo>'+
                                    +'<email>'+
                                        +'<desc>Cellular</desc>'+
                                        +'<value>v_zhartun@twistellar.com</value>'+
                                    +'</email>'+
                                    +'<mainTelephoneNumber>'+
                                        +'<desc>Home</desc>'+
                                        +'<main>true</main>'+
                                        +'<value>1234567891</value>'+
                                    +'</mainTelephoneNumber>'+
                                    +'<telephoneNumber>'+
                                        +'<desc>Cellular</desc>'+
                                        +'<main>false</main>'+
                                        +'<value>1234000000</value>'+
                                    +'</telephoneNumber>'+
                                    +'<telephoneNumber>'+
                                        +'<desc>Home</desc>'+
                                        +'<main>true</main>'+
                                        +'<value>1234567891</value>'+
                                    +'</telephoneNumber>'+
                                +'</contactInfo>'+
                                +'<demographics />'+
                                +'<fleetFlag>N</fleetFlag>'+
                                +'<name1>'+
                                    +'<companyName></companyName>'+
                                    +'<firstName>Test 11</firstName>'+
                                    +'<fullName>qwerty 111 06-23,Test 11</fullName>'+
                                    +'<lastName>qwerty 111 06-23</lastName>'+
                                    +'<middleName></middleName>'+
                                    +'<nameType>Person</nameType>'+
                                    +'<suffix></suffix>'+
                                    +'<title></title>'+
                                +'</name1>'+
                                +'<name2>'+
                                    +'<companyName></companyName>'+
                                    +'<firstName></firstName>'+
                                    +'<fullName></fullName>'+
                                    +'<lastName></lastName>'+
                                    +'<middleName></middleName>'+
                                    +'<suffix></suffix>'+
                                    +'<title></title>'+
                                +'</name2>'+
                                +'<salesStatus>'+
                                    +'<prospect></prospect>'+
                                    +'<type></type>'+
                                +'</salesStatus>'+
                                +'<privacy>'+
                                    +'<privacyInd>OFF</privacyInd>'+
                                    +'<privacyType>EMAIL</privacyType>'+
                                +'</privacy>'+
                                +'<privacy>'+
                                    +'<privacyInd>OFF</privacyInd>'+
                                    +'<privacyType>MAIL</privacyType>'+
                                +'</privacy>'+
                                +'<privacy>'+
                                    +'<privacyInd>OFF</privacyInd>'+
                                    +'<privacyType>PHONE</privacyType>'+
                                +'</privacy>'+
                                +'<insurance />'+
                            +'</customerParty>'+
                        +'</return>'+
                    +'</ns2:insertResponse>'+
                +'</S:Body>'+
            +'</S:Envelope>';
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(responseBody));
        serviceAppointment.Status = 'Canceled';
        update serviceAppointment;
        Test.stopTest();
    }
}