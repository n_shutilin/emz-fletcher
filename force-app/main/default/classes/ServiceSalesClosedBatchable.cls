global class ServiceSalesClosedBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    public Integer recordsProcessed = 0;
    public Integer recordsTotal = 0;
    public static final String ACCOUNT_DEALER_RT = 'Dealer';
    public Integer deltaDays;
    public String orderType;
    public String startDate;
    public String endDate;
    public String dealerId;
    
    global ServiceSalesClosedBatchable() {
        this.orderType = 'open';
    }

    global ServiceSalesClosedBatchable(Integer deltaDays, String orderType) {
        this.deltaDays = deltaDays;
        this.orderType = orderType;
    }

    global ServiceSalesClosedBatchable(String startDate, String endDate, String dealerId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.orderType = 'closed';
        this.dealerId = dealerId;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
            'SELECT Id, Dealer_Code__c, Name, CDK_Dealer_Code__c ' +
            'FROM Account ' +
            'WHERE RecordType.DeveloperName = :ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null';

        if(String.isNotEmpty(dealerId)) {
            query += ' AND Id = :dealerId';
        }

        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        if (scope.size() > 1) {
            throw new ApplicationException('The batch size must be populated as 1');
        }
        Account acc = scope[0];
        List<WorkOrder> wordOrders = new List<WorkOrder>();
        if (orderType == 'open' && String.isBlank(startDate) && String.isBlank(endDate) && deltaDays == NULL) {
            wordOrders = ServiceSalesClosedHandler.getOpenedOrdersBulk(acc.CDK_Dealer_Code__c, acc.Id);
        } else if (orderType == 'closed' && String.isNotBlank(startDate) && String.isNotBlank(endDate)) {
            wordOrders = ServiceSalesClosedHandler.getOrdersByDateRange(acc.CDK_Dealer_Code__c, acc.Id, orderType, startDate, endDate);
        } else {
            wordOrders = ServiceSalesClosedHandler.getOrders(acc.CDK_Dealer_Code__c, deltaDays, acc.Id, orderType);
        }
        
        try {
            Database.UpsertResult[] results = Database.upsert(wordOrders, WorkOrder.fields.UnuqueRONumber__c, true);
            recordsProcessed += scope.size();
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, e.getStackTraceString());
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
    }

    global void finish(Database.BatchableContext bc) {
        if (orderType == 'open' && String.isBlank(startDate) && String.isBlank(endDate) && deltaDays == NULL) {
            ServiceSalesClosedChildBatchable roBatch = new ServiceSalesClosedChildBatchable();
            Id roBatchId = Database.executeBatch(roBatch,1);
        } else if (orderType == 'closed' && String.isNotBlank(startDate) && String.isNotBlank(endDate)) {
            ServiceSalesClosedChildBatchable roBatch = new ServiceSalesClosedChildBatchable(startDate, endDate, dealerId);
            Id roBatchId = Database.executeBatch(roBatch,1);
        } else {
            ServiceSalesClosedChildBatchable roBatch = new ServiceSalesClosedChildBatchable(deltaDays, orderType);
            Id roBatchId = Database.executeBatch(roBatch,1);
        }
        try {
            IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }

}