global class ServiceSalesClosedChildBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    public Integer recordsProcessed = 0;
    public Integer recordsTotal = 0;
    public static final String ACCOUNT_DEALER_RT = 'Dealer';
    public Integer deltaDays;
    public String orderType;
    public String startDate;
    public String endDate;
    public String dealerId;

    global ServiceSalesClosedChildBatchable() {
        this.orderType = 'open';
    }

    global ServiceSalesClosedChildBatchable(Integer deltaDays, String orderType) {
        this.deltaDays = deltaDays;
        this.orderType = orderType;
    }

    global ServiceSalesClosedChildBatchable(String startDate, String endDate, String dealerId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.orderType = 'closed';
        this.dealerId = dealerId;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
            'SELECT Id, Dealer_Code__c, Name, CDK_Dealer_Code__c ' +
            'FROM Account ' +
            'WHERE RecordType.DeveloperName = :ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null';

        if(String.isNotEmpty(dealerId)) {
            query += ' AND Id = :dealerId';
        }

        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        for (Account acc : scope) {
            List<WorkOrderLineItem> lineItems = new List<WorkOrderLineItem>();
            if (orderType == 'open' && String.isBlank(startDate) && String.isBlank(endDate) && deltaDays == NULL) {
                lineItems = ServiceSalesClosedHandler.getOpenedOrderDetailsBulk(acc.CDK_Dealer_Code__c, acc.Id);
            } else if (orderType == 'closed' && String.isNotBlank(startDate) && String.isNotBlank(endDate)) {
                lineItems = ServiceSalesClosedHandler.getOrdersDetailsByDateRange(acc.CDK_Dealer_Code__c, acc.Id, orderType, startDate, endDate);
            } else {
                lineItems = ServiceSalesClosedHandler.getOrdersDetails(acc.CDK_Dealer_Code__c, deltaDays, acc.Id, orderType);
            }
            try {
                Database.UpsertResult[] results = Database.upsert(lineItems, WorkOrderLineItem.fields.Unique_Identifier__c, true);
                recordsProcessed += scope.size();
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, e.getStackTraceString());
                System.debug(LoggingLevel.ERROR, e.getMessage());
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        //TODO: add logging functionality
        try { 
            IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }

}