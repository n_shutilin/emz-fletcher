global class ServiceSalesClosedChildHistoryBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    public Integer recordsProcessed = 0;
    public String startDate;
    public String endDate;
    public String dealerId;

    global ServiceSalesClosedChildHistoryBatchable(String startDate, String endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    global ServiceSalesClosedChildHistoryBatchable(String startDate, String endDate, String dealerId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.dealerId = dealerId;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
            'SELECT Id, Dealer_Code__c, Name, CDK_Dealer_Code__c ' +
            'FROM Account ' +
            'WHERE RecordType.DeveloperName = :ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null';

        if(String.isNotEmpty(dealerId)) {
            query += ' AND Id = :dealerId';
        }

        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        for (Account acc : scope) {
            List<WorkOrderLineItem> lineItems = ServiceSalesClosedHandler.getHistoryOrdersDetailsByDateRange(acc.CDK_Dealer_Code__c, acc.Id, startDate, endDate);
            
            try {
                Database.UpsertResult[] results = Database.upsert(lineItems, WorkOrderLineItem.fields.Unique_Identifier__c, true);
                recordsProcessed += scope.size();
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, e.getStackTraceString());
                System.debug(LoggingLevel.ERROR, e.getMessage());
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        //TODO: add logging functionality
        try { 
            IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }
}