public class ServiceSalesClosedChildParser {

    public static final String NAMESPACE_CLOSED = 'http://www.dmotorworks.com/pip-extract-service-sales-closed';
    public static final String NAMESPACE_OPEN = 'http://www.dmotorworks.com/pip-extract-servicesalesopen';

    public static List<WorkOrderLineItem> process(Dom.Document doc, Id accountId, String orderType) {
        Set<String> roNumberSet = new Set<String>();
        List<WorkOrderLineItem> lineItems = new List<WorkOrderLineItem>();
        String dealerId = [SELECT Id, Dealer_Code__c FROM Account WHERE Id = :accountId].Dealer_Code__c;
        
        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT
                    && (child.getName() == 'ServiceSalesDetailsClosed') || child.getName() == 'ServiceSalesDetailsOpen') {
                WorkOrderLineItem lineItem = new WorkOrderLineItem();

                mapFields(lineItem, child, (orderType.toLowerCase() == 'open' ? NAMESPACE_OPEN : NAMESPACE_CLOSED));

                lineItem.UnuqueRONumber__c = accountId + lineItem.RONumber__c;
                lineItem.Unique_Identifier__c = lineItem.UnuqueRONumber__c + lineItem.HostItemID__c;
                lineItem.DealerId__c = dealerId;

                roNumberSet.add(lineItem.UnuqueRONumber__c);
                lineItems.add(lineItem);
            }
        }

        List<WorkOrder> workOrders = new List<WorkOrder>();
        workOrders = [
                SELECT Id, UnuqueRONumber__c
                FROM WorkOrder
                WHERE UnuqueRONumber__c IN :roNumberSet
        ];

        Map<String, Id> workOrderMap = new Map<String, Id>();
        for (WorkOrder wo : workOrders) {
            workOrderMap.put(wo.UnuqueRONumber__c, wo.Id);
        }

        for (WorkOrderLineItem lineItem : lineItems) {
            lineItem.WorkOrderId = workOrderMap.get(lineItem.UnuqueRONumber__c);
        }

        return lineItems;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child, String namespace) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'ServiceRequest' => WorkOrderLineItem.ServiceRequest__c,
            'LineCode' => WorkOrderLineItem.LineCode__c,
            'LopSeqNo' => WorkOrderLineItem.LopSeqNo__c,
            'LaborSale' => WorkOrderLineItem.LaborSale__c,
            'PartsSale' => WorkOrderLineItem.PartsSale__c,
            'ActualHours' => WorkOrderLineItem.ActualHours__c,
            'RONumber' => WorkOrderLineItem.RONumber__c,
            'HostItemID' => WorkOrderLineItem.HostItemID__c,
            'OpCode' => WorkOrderLineItem.OpCode__c,
            'OpCodeDescription' => WorkOrderLineItem.OpCodeDescription__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, namespace);

            // System.debug('~~ ' + childElement);
            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}