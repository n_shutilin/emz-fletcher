public class ServiceSalesClosedDetailsHistoryParser {
    public static final String NAMESPACE_CLOSED = 'http://www.dmotorworks.com/pip-extract-service-sales-closed';
    public static final String ACCOUNT_DEALER_RT = 'Dealer';
    
    public static List<RepairOrderDetail__b> process(Dom.Document doc, Id accountId) {
        List<RepairOrderDetail__b> roDetails = new List<RepairOrderDetail__b>();
        String dealerId = [SELECT Id, Dealer_Code__c FROM Account WHERE Id = :accountId AND RecordType.DeveloperName = :ACCOUNT_DEALER_RT].Dealer_Code__c;
        
        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == 'ServiceSalesDetailsClosed') {
                RepairOrderDetail__b roDetail = new RepairOrderDetail__b();

                mapFields(roDetail, child);

                roDetail.UniqueRONumber__c = accountId + roDetail.RONumber__c;
                roDetail.Unique_Identifier__c = roDetail.UniqueRONumber__c + roDetail.Host_Item_ID__c;
                roDetail.DealerId__c = dealerId;

                roDetails.add(roDetail);
            }
        }

        return roDetails;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'ServiceRequest' => RepairOrderDetail__b.ServiceRequest__c,
            'LineCode' => RepairOrderDetail__b.LineCode__c,
            'LopSeqNo' => RepairOrderDetail__b.LopSeqNo__c,
            'LaborSale' => RepairOrderDetail__b.LaborSale__c,
            'PartsSale' => RepairOrderDetail__b.PartsSale__c,
            'ActualHours' => RepairOrderDetail__b.ActualHours__c,
            'RONumber' => RepairOrderDetail__b.RONumber__c,
            'HostItemID' => RepairOrderDetail__b.Host_Item_ID__c,
            'OpCode' => RepairOrderDetail__b.OpCode__c,
            'OpCodeDescription' => RepairOrderDetail__b.OpCodeDescription__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, NAMESPACE_CLOSED);

            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}