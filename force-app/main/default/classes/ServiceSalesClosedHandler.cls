public class ServiceSalesClosedHandler {
    public static final String PATH_CLOSED = '/pip-extract/servicesalesclosedext/extract';
    public static final String PATH_CLOSED_DETAILS = '/pip-extract/servicesalesdetailsclosedext/extract';
    public static final String PATH_OPEN = '/pip-extract/servicesalesopenext/extract';
    public static final String PATH_OPEN_DETAILS = '/pip-extract/servicesalesdetailsopenext/extract';
    public static final String PATH_WORK_TYPES = '/pip-extract/opcode/extract';
    public static final String QUERY_CLOSED_ID = 'SSC_DeltaWIP_H';
    public static final String QUERY_CLOSED_ID_WITH_DATERANGE = 'SSC_DateRange_H';
    public static final String QUERY_CLOSED_DETAILS_ID_WITH_DATERANGE = 'SSC_DateRange_D';
    public static final String QUERY_CLOSED_DETAILS_ID = 'SSC_DeltaWIP_D';
    public static final String QUERY_OPEN_ID = 'SSO_Delta_H';
    public static final String QUERY_OPEN_BULK = 'SSO_Bulk_H';
    public static final String QUERY_OPEN_DETAILS_ID = 'SSO_Delta_D';
    public static final String QUERY_OPEN_DETAILS_BULK = 'SSO_Bulk_D';
    public static final String QUERY_HISTORY_CLOSED = 'SSCH_DateRange_History';
    public static final String QUERY_HISTORY_CLOSED_DETAILS = 'SSCD_DateRange_History';
    public static final String DEALERID_PARAM = 'dealerId';
    public static final String QUERYID_PARAM = 'queryId';
    public static final String DELTADATE_PARAM = 'deltaDate';
    public static final String DELTADATE_FORMAT = 'MM/dd/yyyy';
    public static final String STARTDATE_PARAM = 'qparamStartDate';
    public static final String ENDDATE_PARAM = 'qparamEndDate';


    public static List<WorkOrder> getOrders(String dealerId, Integer deltaDays, Id accountId, String orderType) {
        Datetime deltaDate = Datetime.now().addDays(-deltaDays);
        String deltaDaysStr = deltaDate.format(DELTADATE_FORMAT);
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, (orderType.toLowerCase() == 'open' ? QUERY_OPEN_ID : QUERY_CLOSED_ID));
        paramsMap.put(DELTADATE_PARAM, deltaDaysStr);
        HttpResponse res = DMotorWorksApi.get((orderType.toLowerCase() == 'open' ? PATH_OPEN : PATH_CLOSED), paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<WorkOrder> workOrders = ServiceSalesClosedParser.process(res.getBodyDocument(), accountId, orderType);
        System.debug('work orders ' + workOrders);
        return workOrders;
    }

    public static List<WorkOrder> getOrdersByDateRange(String dealerId, Id accountId, String orderType, String startDate, String endDate) {
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, QUERY_CLOSED_ID_WITH_DATERANGE);
        paramsMap.put(STARTDATE_PARAM, startDate);
        paramsMap.put(ENDDATE_PARAM, endDate);
        HttpResponse res = DMotorWorksApi.get(PATH_CLOSED, paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<WorkOrder> workOrders = ServiceSalesClosedParser.process(res.getBodyDocument(), accountId, orderType);
        System.debug('work orders ' + workOrders);
        return workOrders;
    }

    public static List<WorkOrderLineItem> getOrdersDetails(String dealerId, Integer deltaDays, Id accountId, String orderType) {
        Datetime deltaDate = Datetime.now().addDays(-deltaDays);
        String deltaDaysStr = deltaDate.format(DELTADATE_FORMAT);
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, (orderType.toLowerCase() == 'open' ? QUERY_OPEN_DETAILS_ID : QUERY_CLOSED_DETAILS_ID));
        paramsMap.put(DELTADATE_PARAM, deltaDaysStr); 
        HttpResponse res = DMotorWorksApi.get((orderType.toLowerCase() == 'open' ? PATH_OPEN_DETAILS : PATH_CLOSED_DETAILS), paramsMap);
        System.debug('response body ' + res.getBody()); 
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<WorkOrderLineItem> lineItems = ServiceSalesClosedChildParser.process(res.getBodyDocument(), accountId, orderType);
        System.debug('work orders ' + lineItems);
        return lineItems;
    }

    public static List<WorkOrderLineItem> getOrdersDetailsByDateRange(String dealerId, Id accountId, String orderType, String startDate, String endDate) {
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, QUERY_CLOSED_DETAILS_ID_WITH_DATERANGE);
        paramsMap.put(STARTDATE_PARAM, startDate);
        paramsMap.put(ENDDATE_PARAM, endDate);
        HttpResponse res = DMotorWorksApi.get(PATH_CLOSED_DETAILS, paramsMap);
        System.debug('response body ' + res.getBody()); 
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<WorkOrderLineItem> lineItems = ServiceSalesClosedChildParser.process(res.getBodyDocument(), accountId, orderType);
        System.debug('work orders ' + lineItems);
        return lineItems;
    }

    public static List<WorkOrder> getOpenedOrdersBulk(String dealerId, Id accountId) {
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, QUERY_OPEN_BULK);
        HttpResponse res = DMotorWorksApi.get(PATH_OPEN, paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<WorkOrder> workOrders = ServiceSalesClosedParser.process(res.getBodyDocument(), accountId, 'open');
        System.debug('work orders ' + workOrders);
        return workOrders;
    }

    public static List<WorkOrderLineItem> getOpenedOrderDetailsBulk(String dealerId, Id accountId) {
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, QUERY_OPEN_DETAILS_BULK);
        HttpResponse res = DMotorWorksApi.get(PATH_OPEN_DETAILS, paramsMap);
        System.debug('response body ' + res.getBody()); 
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<WorkOrderLineItem> lineItems = ServiceSalesClosedChildParser.process(res.getBodyDocument(), accountId, 'open');
        System.debug('work orders ' + lineItems);
        return lineItems;
    }

    public static List<WorkOrder> getHistoryOrdersByDateRange(String dealerId, Id accountId, String startDate, String endDate) {
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, QUERY_HISTORY_CLOSED);
        paramsMap.put(STARTDATE_PARAM, startDate);
        paramsMap.put(ENDDATE_PARAM, endDate);
        HttpResponse res = DMotorWorksApi.get(PATH_CLOSED, paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<WorkOrder> workOrders = ServiceSalesClosedParser.process(res.getBodyDocument(), accountId, 'closed');
        System.debug('work orders ' + workOrders);
        return workOrders;
    }

    public static List<WorkOrderLineItem> getHistoryOrdersDetailsByDateRange(String dealerId, Id accountId, String startDate, String endDate) {
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, QUERY_HISTORY_CLOSED_DETAILS);
        paramsMap.put(STARTDATE_PARAM, startDate);
        paramsMap.put(ENDDATE_PARAM, endDate);
        HttpResponse res = DMotorWorksApi.get(PATH_CLOSED_DETAILS, paramsMap);
        System.debug('response body ' + res.getBody()); 
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<WorkOrderLineItem> lineItems = ServiceSalesClosedChildParser.process(res.getBodyDocument(), accountId, 'closed');
        System.debug('work orders ' + lineItems);
        return lineItems;
    }

}