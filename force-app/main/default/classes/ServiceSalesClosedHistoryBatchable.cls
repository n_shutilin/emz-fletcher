global class ServiceSalesClosedHistoryBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    public Integer recordsProcessed = 0;
    public String startDate;
    public String endDate;
    public String dealerId;

    global ServiceSalesClosedHistoryBatchable(String startDate, String endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    global ServiceSalesClosedHistoryBatchable(String startDate, String endDate, String dealerId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.dealerId = dealerId;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
            'SELECT Id, Dealer_Code__c, Name, CDK_Dealer_Code__c ' +
            'FROM Account ' +
            'WHERE RecordType.DeveloperName = :ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null';

        if(String.isNotEmpty(dealerId)) {
            query += ' AND Id = :dealerId';
        }

        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        if (scope.size() > 1) {
            throw new ApplicationException('The batch size must be populated as 1');
        }
        Account acc = scope[0];
        List<WorkOrder> wordOrders = ServiceSalesClosedHandler.getHistoryOrdersByDateRange(acc.CDK_Dealer_Code__c, acc.Id, startDate, endDate);
        
        try {
            Database.UpsertResult[] results = Database.upsert(wordOrders, WorkOrder.fields.UnuqueRONumber__c, true);
            recordsProcessed += scope.size();
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, e.getStackTraceString());
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
    }

    global void finish(Database.BatchableContext bc) {
        if(String.isNotEmpty(dealerId)) {
            ServiceSalesClosedChildHistoryBatchable roBatch = new ServiceSalesClosedChildHistoryBatchable(startDate, endDate, dealerId);
            Id roBatchId = Database.executeBatch(roBatch,1);
        } else {
            ServiceSalesClosedChildHistoryBatchable roBatch = new ServiceSalesClosedChildHistoryBatchable(startDate, endDate);
            Id roBatchId = Database.executeBatch(roBatch,1);
        }

        try {
            IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }
}