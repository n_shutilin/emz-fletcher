public class ServiceSalesClosedHistoryParser {
    public static final String NAMESPACE_CLOSED = 'http://www.dmotorworks.com/pip-extract-service-sales-closed';
    public static final String ACCOUNT_DEALER_RT = 'Dealer';

    public static List<RepairOrder__b> process(Dom.Document doc, Id accountId) {
        List<RepairOrder__b> repairOrders = new List<RepairOrder__b>();
        String dealerId = [SELECT Id, Dealer_Code__c FROM Account WHERE Id =: accountId AND RecordType.DeveloperName = :ACCOUNT_DEALER_RT].Dealer_Code__c;

        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == 'ServiceSalesClosed') {
                RepairOrder__b newRepairOrder = new RepairOrder__b();

                mapFields(newRepairOrder, child);

                //newRepairOrder.Status__c = 'Closed';
                newRepairOrder.DealerID__c = dealerId;
                newRepairOrder.UniqueRONumber__c = accountId + String.valueOf(newRepairOrder.RO_Number__c);

                repairOrders.add(newRepairOrder);
            }
        }

        return repairOrders;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'Model' => RepairOrder__b.Model__c,
            'ROComment1' => RepairOrder__b.RO_Comment_1__c,
            'ROComment2' => RepairOrder__b.RO_Comment_2__c,
            'ROComment3' => RepairOrder__b.RO_Comment_3__c,
            'ROComment4' => RepairOrder__b.RO_Comment_4__c,
            'ROComment5' => RepairOrder__b.RO_Comment_5__c,
            'ROComment6' => RepairOrder__b.RO_Comment_6__c,
            'ROComment7' => RepairOrder__b.RO_Comment_7__c,
            'ROComment8' => RepairOrder__b.RO_Comment_8__c,
            'ROComment9' => RepairOrder__b.RO_Comment_9__c,
            'OpenDate' => RepairOrder__b.Open_Date__c,
            'OpenedTime' => RepairOrder__b.Opened_Time__c,
            'PromisedDate' => RepairOrder__b.Promised_Date__c,
            'PromisedTime' => RepairOrder__b.Promised_Time__c,
            'RONumber' => RepairOrder__b.RO_Number__c,
            'ROStatusCode' => RepairOrder__b.RO_Status_Code__c,
            'ROStatusCodeDesc' => RepairOrder__b.RO_Status_Code_Desc__c,
            'CustNo' => RepairOrder__b.Custno__c,
            'VIN' => RepairOrder__b.Vin__c,
            'CloseDate' => RepairOrder__b.Close_Date__c,
            'VehID' => RepairOrder__b.Veh_ID__c,
            'Year' => RepairOrder__b.Year__c,
            'ServiceAdvisor' => RepairOrder__b.Service_Advisor__c,
            'Make' => RepairOrder__b.Make__c,
            'Mileage' => RepairOrder__b.Mileage__c,
            'MileageOut' => RepairOrder__b.Mileage_Out__c,
            'Address' => RepairOrder__b.Address__c,
            'LaborSaleCustomerPay' => RepairOrder__b.Labor_Sale_Customer_pay__c,
            'LaborSaleInternal' => RepairOrder__b.Labor_Sale_Internal__c,
            'LaborSaleWarranty' => RepairOrder__b.Labor_Sale_Warranty__c,
            'PartsSaleCustomerPay' => RepairOrder__b.Parts_Sale_Customer_Pay__c,
            'PartsSaleInternal' => RepairOrder__b.Parts_Sale_Internal__c,
            'PartsSaleWarranty' => RepairOrder__b.Parts_Sale_Warranty__c,
            'PriorityValue' => RepairOrder__b.Priority_Value__c,
            'Name1' => RepairOrder__b.Name1__c,
            'Name2' => RepairOrder__b.Name2__c,
            'ContactEmailAddress' => RepairOrder__b.ContactEmailAddress__c,
            'ContactPhoneNumber' => RepairOrder__b.ContactPhoneNumber__c,
            'HostItemID' => RepairOrder__b.HostItemId__c,
            'CityStateZip' => RepairOrder__b.CityStateZip__c,
            'EstCompletionDate' => RepairOrder__b.EstCompletionDate__c,
            'EstCompletionTime' => RepairOrder__b.EstCompletionTime__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, NAMESPACE_CLOSED);

            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}