public class ServiceSalesClosedParser {
    public static final String NAMESPACE_CLOSED = 'http://www.dmotorworks.com/pip-extract-service-sales-closed';
    public static final String NAMESPACE_OPEN = 'http://www.dmotorworks.com/pip-extract-servicesalesopen';

    public static List<WorkOrder> process(Dom.Document doc, Id accountId, String orderType) {
        // Map<String, Id> resourceMap = new Map<String, Id>();
        // for (ServiceResource sr : [SELECT Id, ServiceAdvisorNumber__c FROM ServiceResource WHERE AccountId = :accountId]) {
        //     resourceMap.put(sr.ServiceAdvisorNumber__c, sr.Id);
        // }
        Map<String, Service_Advisor__c> resourceMap = getDealerAdvisorsMap(accountId);

        List<WorkOrder> workOrders = new List<WorkOrder>();
        Map<Id, Account> idToAccount = new Map<Id, Account>([
            SELECT Id, Dealer_Code__c
            FROM Account
            WHERE Dealer_Code__c != NULL AND RecordType.DeveloperName = :ServiceSalesClosedBatchable.ACCOUNT_DEALER_RT
        ]);
        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT
                && (child.getName() == 'ServiceSalesClosed' || child.getName() == 'ServiceSalesOpen')) {
                WorkOrder workOrder = new WorkOrder();

                mapFields(workOrder, child, (orderType.toLowerCase() == 'open' ? NAMESPACE_OPEN : NAMESPACE_CLOSED));

                if (child.getName() == 'ServiceSalesClosed') {
                    workOrder.Status = 'Closed';
                }

                Id repOrderRecTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Repair Order').getRecordTypeId();

                if (repOrderRecTypeId != null) {
                    workOrder.RecordTypeId= repOrderRecTypeId;
                }
                if (idToAccount.containsKey(accountId)) {
                    workOrder.Dealer_ID__c = idToAccount.get(accountId).Dealer_Code__c;
                }
                workOrder.UnuqueRONumber__c = accountId + String.valueOf(workOrder.RO_Number__c);

                if (resourceMap.containsKey(workOrder.Service_Advisor__c)) {
                    workOrder.SA_Name__c = resourceMap.get(workOrder.Service_Advisor__c)?.Name;
                }

                List<String> nameComponents = splitName(workOrder.Name1__c);
                workOrder.Last_Name__c = nameComponents.get(0);
                workOrder.First_Name__c = nameComponents.get(1);
                workOrder.Middle_Name__c = nameComponents.get(2);

                nameComponents = splitName(workOrder.Name2__c);
                workOrder.Name_2_Last__c = nameComponents.get(0);
                workOrder.Name2_First__c = nameComponents.get(1);
                workOrder.Name_2_Middle__c = nameComponents.get(2);
                workOrders.add(workOrder);
            }
        }

        workOrders = updateCustomerData(workOrders);

        return workOrders;
    }

    private static List<WorkOrder> updateCustomerData(List<WorkOrder> workOrders) {
        List<String> customerIds = new List<String>();
        for (WorkOrder repairOrder : workOrders) {
            customerIds.add(repairOrder.Dealer_ID__c + '_' + repairOrder.Custno__c);
        }

        List<CDK_Customer__c> customers = [SELECT Id, Email__c, Customer_Control_Number__c FROM CDK_Customer__c WHERE SF_Unique_Identifier__c IN :customerIds];
        Map<String, CDK_Customer__c> customersMap = new Map<String, CDK_Customer__c>();
        for (CDK_Customer__c customer : customers) {
            customersMap.put(customer.Customer_Control_Number__c, customer);
        }

        for (WorkOrder repairOrder : workOrders) {
            if (String.isBlank(repairOrder.ContactEmailAddress__c)) {
                repairOrder.ContactEmailAddress__c = customersMap.get(repairOrder.Custno__c)?.Email__c;
            }
        }

        return workOrders;
    }

    private static Map<String, Service_Advisor__c> getDealerAdvisorsMap(String accountId) {
        Map<String, Service_Advisor__c> advisorsByAdvisorNumberMap = new Map<String, Service_Advisor__c>();
        
        Account dealer = [
        	SELECT Dealer_Code__c
            FROM Account
            WHERE RecordType.DeveloperName = :ServiceSalesClosedBatchable.ACCOUNT_DEALER_RT AND Id = :accountId
            LIMIT 1
        ];

        String dealerCode = dealer?.Dealer_Code__c;

        if(dealerCode != null) {
            List<Service_Advisor__c> advisors = [
                SELECT Id, Name, Service_Advisor_Number__c, Dealer__c
                FROM Service_Advisor__c
                WHERE Dealer__c = :dealerCode
            ];

            for (Service_Advisor__c advisor : advisors) {
                advisorsByAdvisorNumberMap.put(advisor.Service_Advisor_Number__c, advisor);
            }
        }

        return advisorsByAdvisorNumberMap;
    }

    private static List<String> splitName(String name) {
        name = String.isBlank(name) ? '' : name;
        List<String> nameComponents = name.split(',');
        nameComponents.addAll(new List<String> {'', '', ''});
        return nameComponents;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child, String namespace) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'Model' => WorkOrder.Model__c,
            'ROComment1' => WorkOrder.RO_Comment_1__c,
            'ROComment2' => WorkOrder.RO_Comment_2__c,
            'ROComment3' => WorkOrder.RO_Comment_3__c,
            'ROComment4' => WorkOrder.RO_Comment_4__c,
            'ROComment5' => WorkOrder.RO_Comment_5__c,
            'ROComment6' => WorkOrder.RO_Comment_6__c,
            'ROComment7' => WorkOrder.RO_Comment_7__c,
            'ROComment8' => WorkOrder.RO_Comment_8__c,
            'ROComment9' => WorkOrder.RO_Comment_9__c,
            'OpenDate' => WorkOrder.Open_Date__c,
            'OpenedTime' => WorkOrder.Opened_Time__c,
            'PromisedDate' => WorkOrder.Promised_Date__c,
            'PromisedTime' => WorkOrder.Promised_Time__c,
            'RONumber' => WorkOrder.RO_Number__c,
            'ROStatusCode' => WorkOrder.RO_Status_Code__c,
            'ROStatusCodeDesc' => WorkOrder.RO_Status_Code_Desc__c,
            'CustNo' => WorkOrder.Custno__c,
            'VIN' => WorkOrder.Vin__c,
            'CloseDate' => WorkOrder.Close_Date__c,
            'VehID' => WorkOrder.Veh_ID__c,
            'Year' => WorkOrder.Year__c,
            'ServiceAdvisor' => WorkOrder.Service_Advisor__c,
            'Make' => WorkOrder.Make__c,
            'Mileage' => WorkOrder.Mileage__c,
            'MileageOut' => WorkOrder.Mileage_Out__c,
            'Address' => WorkOrder.Address_Second_Line__c,
            'LaborSaleCustomerPay' => WorkOrder.Labor_Sale_Customer_pay__c,
            'LaborSaleInternal' => WorkOrder.Labor_Sale_Internal__c,
            'LaborSaleWarranty' => WorkOrder.Labor_Sale_Warranty__c,
            'PartsSaleCustomerPay' => WorkOrder.Parts_Sale_Customer_Pay__c,
            'PartsSaleInternal' => WorkOrder.Parts_Sale_Internal__c,
            'PartsSaleWarranty' => WorkOrder.Parts_Sale_Warranty__c,
            'PriorityValue' => WorkOrder.Priority_Value__c,
            'Name1' => WorkOrder.Name1__c,
            'Name2' => WorkOrder.Name2__c,
            'ContactEmailAddress' => WorkOrder.ContactEmailAddress__c,
            'ContactPhoneNumber' => WorkOrder.ContactPhoneNumber__c,
            'HostItemID' => WorkOrder.HostItemId__c,
            'CityStateZip' => WorkOrder.CityStateZip__c,
            'EstCompletionDate' => WorkOrder.Est_Completion_Date__c,
            'EstCompletionTime' => WorkOrder.Est_Completion_Time__c,
            'ApptIDs' => WorkOrder.Appt_Id__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, namespace);

            // System.debug('~~ ' + childElement);
            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}