global class ServiceSalesExtractSchedulable implements Schedulable{

    global void execute(SchedulableContext SC) {
        ServiceSalesClosedBatchable batch = new ServiceSalesClosedBatchable(1, 'closed');
        Database.executeBatch(batch, 1);

        ServiceSalesClosedBatchable roBatch2 = new ServiceSalesClosedBatchable(1, 'open');
        Database.executeBatch(roBatch2,1);

        Datetime nextScheduleTime = System.now().addMinutes(15);
        String hour = nextScheduleTime.hour() <= 21 ? String.valueOf(nextScheduleTime.hour()) : '6';
        String minutes = String.valueOf(nextScheduleTime.minute());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        String jobName = 'Order Extract ' + nextScheduleTime.format('hh:mm');

        ServiceSalesExtractSchedulable p = new ServiceSalesExtractSchedulable();
        System.schedule(jobName, cronValue , p);

        System.abortJob(SC.getTriggerId());
    }

}