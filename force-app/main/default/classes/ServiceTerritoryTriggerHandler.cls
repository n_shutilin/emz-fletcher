public with sharing class ServiceTerritoryTriggerHandler {
    public List<ServiceTerritory> newList;
    public Map<Id, ServiceTerritory> oldMap;

    public ServiceTerritoryTriggerHandler(List<ServiceTerritory> newList, Map<Id, ServiceTerritory> oldMap) {
        this.newList = newList;
        this.oldMap = oldMap;
    }

    public void handleBeforeInsert() {
        checkDuplicates();
    }

    public void handleBeforeUpdate() {
        checkDuplicates();
    }

    private void checkDuplicates() {
        Set<Id> territoryIdSet = new Set<Id>();
        Set<String> dealerSet = new Set<String>();

        for (ServiceTerritory territory : newList) {
            territoryIdSet.add(territory.Id);
            dealerSet.add(territory.Dealer__c);
        }

        List<ServiceTerritory> existingTerritories = [
            SELECT Dealer__c, IsSalesTerritory__c
            FROM ServiceTerritory
            WHERE Id NOT IN :territoryIdSet
            AND Dealer__c IN :dealerSet
            AND IsActive = TRUE
        ];

        if (!existingTerritories.isEmpty()) {
            Map<String, Map<Boolean, ServiceTerritory>> territoryMap = new Map<String, Map<Boolean, ServiceTerritory>>();

            for (ServiceTerritory territory : existingTerritories) {
                if (!territoryMap.containsKey(territory.Dealer__c)) {
                    territoryMap.put(territory.Dealer__c, new Map<Boolean, ServiceTerritory>());
                }

                territoryMap.get(territory.Dealer__c).put(territory.IsSalesTerritory__c, territory);
            }

            for (ServiceTerritory territory : newList) {
                if (territory.IsActive && territoryMap.containsKey(territory.Dealer__c) && territoryMap.get(territory.Dealer__c).containsKey(territory.IsSalesTerritory__c)) {
                    territory.addError('Territory with the same Dealer and Is Sales Territory already exists');
                }
            }
        }
    }
}