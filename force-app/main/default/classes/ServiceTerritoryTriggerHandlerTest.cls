@IsTest
private class ServiceTerritoryTriggerHandlerTest {

    @TestSetup
    private static void setup() {
        OperatingHours hours = new OperatingHours(Name = 'TestHours');
        insert hours;

        insert new ServiceTerritory(
            Name = 'TestTerritory',
            Dealer__c = 'BHAUD',
            IsSalesTerritory__c = true,
            OperatingHoursId = hours.Id,
            IsActive = true
        );
    }

    @IsTest
    private static void testDuplicates() {
        Boolean expectedExceptionThrown = false;

        OperatingHours hours = new OperatingHours(Name = 'Test');
        insert hours;

        try {
            insert new ServiceTerritory(
                Name = 'Test',
                Dealer__c = 'BHAUD',
                IsSalesTerritory__c = true,
                OperatingHoursId = hours.Id,
                IsActive = true
            );
        } catch (Exception e) {
            expectedExceptionThrown = e.getMessage().contains('Territory with the same Dealer and Is Sales Territory already exists');
        }

        System.assertEquals(true, expectedExceptionThrown);
    }

    @IsTest
    private static void testNoDuplicates() {
        OperatingHours hours = new OperatingHours(Name = 'Test');
        insert hours;

        ServiceTerritory territory = new ServiceTerritory(
            Name = 'Test',
            Dealer__c = 'BHAUD',
            IsSalesTerritory__c = false,
            OperatingHoursId = hours.Id,
            IsActive = true
        );
        insert territory;

        territory.IsSalesTerritory__c = true;
        territory.Dealer__c = 'AUDFJ';
        update territory;

        Integer territoryCount = [
            SELECT COUNT()
            FROM ServiceTerritory
        ];

        System.assertEquals(2, territoryCount);
    }
}