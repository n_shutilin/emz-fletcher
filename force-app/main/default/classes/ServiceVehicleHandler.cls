public class ServiceVehicleHandler {

    public static final String PATH_VEHICLES = '/pip-extract/servicevehicleext/extract';
    public static final String QUERY_VEHICLES_ID = 'SVEH_Delta';
    public static final String QUERY_BULK_VEHICLES_ID = 'SVEH_Bulk';
    public static final String DEALERID_PARAM = 'dealerId';
    public static final String QUERYID_PARAM = 'queryId';
    public static final String DELTADATE_PARAM = 'deltaDate';
    public static final String DELTADATE_FORMAT = 'MM/dd/yyyy';

    public static List<Vehicle__c> getVehicles(String dealerId, Integer deltaDays, String accountId, Boolean isBulk) {
        Map<String, String> paramsMap = new Map<String, String>();
        if (!isBulk) {
            Datetime deltaDate = Datetime.now().addDays(-deltaDays);
            String deltaDaysStr = deltaDate.format(DELTADATE_FORMAT);
            paramsMap.put(DELTADATE_PARAM, deltaDaysStr);
        }
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, isBulk ? QUERY_BULK_VEHICLES_ID : QUERY_VEHICLES_ID);
        HttpResponse res = DMotorWorksApi.get(PATH_VEHICLES, paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<Vehicle__c> vehicles = ServiceVehicleParser.process(res.getBodyDocument(), accountId);
        System.debug('Vehicles ' + vehicles);
        return vehicles;
    }
}