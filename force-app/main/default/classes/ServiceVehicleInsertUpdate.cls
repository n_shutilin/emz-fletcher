public with sharing class ServiceVehicleInsertUpdate {

    public static final String PIP_NAMESPACE = 'http://www.dmotorworks.com/pip-vehicle';
    public static final String XMLNS_NAMESPACE = 'http://schemas.xmlsoap.org/soap/envelope/';
    public static final String PATH_INSERT_UPDATE = '/pip-vehicle/services/VehicleInsertUpdate';
    public static final String CODE_FAILURE = 'failure';
    public static final String CODE_SUCCESS = 'success';

    public static CDK_Vehicle__c insertServiceVehicle(CDK_Vehicle__c processedVehicle, String dealerId, String vehicleId, String customerNumber) {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();

        VehicleInfoWrapper vehicleInfo = explodeVIN(dealerId, processedVehicle.VIN__c);

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', XMLNS_NAMESPACE, 'soapenv');
        envelope.setNamespace('pip', PIP_NAMESPACE);

        dom.XmlNode body = envelope.addChildElement('Body', XMLNS_NAMESPACE, 'soapenv');
        dom.XmlNode insertTag = body.addChildElement('insert', PIP_NAMESPACE, 'pip');

        dom.XmlNode arg0 = insertTag.addChildElement('arg0', null, null);
        arg0.addChildElement('password', null, null)
                .addTextNode(settings.Password__c);
        arg0.addChildElement('username', null, null)
                .addTextNode(settings.Username__c);

        dom.XmlNode arg1 = insertTag.addChildElement('arg1', null, null);
        arg1.addChildElement('id', null, null)
                .addTextNode(dealerId);

        dom.XmlNode arg2 = insertTag.addChildElement('arg2', null, null);

        dom.XmlNode dealer = arg2.addChildElement('dealer', null, null);
        //        dealer.addChildElement('company', null, null)
        //                .addTextNode('');
        //        dealer.addChildElement('dealNumber', null, null)
        //                .addTextNode('');
        //        dealer.addChildElement('dealerNumber', null, null)
        //                .addTextNode('');
        dealer.addChildElement('inServiceDate', null, null)
                .addTextNode((processedVehicle.In_Service_Date_Text__c == null ? 
                        processedVehicle.CreatedDate.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') : 
                        processedVehicle.In_Service_Date_Text__c));
        //        dealer.addChildElement('lastActivityDate', null, null)
        //                .addTextNode('');
        //        dealer.addChildElement('lastServiceDate', null, null)
        //                .addTextNode('');
        dealer.addChildElement('vehicleId', null, null)
                .addTextNode(vehicleId);
        //        dealer.addChildElement('vehicleSoldDate', null, null)
        //                .addTextNode('');
        //        dealer.addChildElement('wholesaleVehicleInd', null, null)
        //                .addTextNode('');

        //        dom.XmlNode manufacturer = arg2.addChildElement('manufacturer', null, null);
        //        dom.XmlNode address = manufacturer.addChildElement('address', null, null);
        //        address.addChildElement('addressLine', null, null)
        //                .addTextNode('');
        //        address.addChildElement('city', null, null)
        //                .addTextNode('');
        //        address.addChildElement('country', null, null)
        //                .addTextNode('');
        //        address.addChildElement('county', null, null)
        //                .addTextNode('');
        //        address.addChildElement('postalCode', null, null)
        //                .addTextNode('');
        //        address.addChildElement('stateOrProvince', null, null)
        //                .addTextNode('');

        //        manufacturer.addChildElement('name', null, null)
        //                .addTextNode('');
        //        manufacturer.addChildElement('plant', null, null)
        //                .addTextNode('');
        //        manufacturer.addChildElement('productionNumber', null, null)
        //                .addTextNode('');
        //        manufacturer.addChildElement('vehicleProductionDate', null, null)
        //                .addTextNode('');

        //        dom.XmlNode invoice = arg2.addChildElement('invoice', null, null);
        //        invoice.addChildElement('entryDate', null, null)
        //                .addTextNode('');
        //        invoice.addChildElement('freight', null, null)
        //                .addTextNode('');

        dom.XmlNode vehicle = arg2.addChildElement('vehicle', null, null);
        //        vehicle.addChildElement('axleCode', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('axleCount', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('bodyStyle', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('brakeSystem', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('cabType', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('certifiedPreownedInd', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('certifiedPreownedNumber', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('chassis', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('color', null, null)
        //                .addTextNode('');
        vehicle.addChildElement('deliveryDate', null, null)
                .addTextNode((processedVehicle.Delivery_Date_Text__c == null ? 
                        processedVehicle.CreatedDate.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') : 
                        processedVehicle.Delivery_Date_Text__c));
        //        vehicle.addChildElement('deliveryMileage', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('doorsQuantity', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('engineNumber', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('engineType', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('exteriorColor', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('fleetVehicleId', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('frontTireCode', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('GMRPOCode', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('ignitionKeyNumber', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('interiorColor', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('licensePlateNo', null, null)
        //                .addTextNode('');
        vehicle.addChildElement('make', null, null)
                .addTextNode((vehicleInfo.make == null ? '' : vehicleInfo.make));
        vehicle.addChildElement('model', null, null)
                .addTextNode((vehicleInfo.model == null ? '' : vehicleInfo.model));
        vehicle.addChildElement('modelAbrev', null, null)
                .addTextNode((vehicleInfo.modelAbrev == null ? '' : vehicleInfo.modelAbrev));
        vehicle.addChildElement('modelYear', null, null)
                .addTextNode((vehicleInfo.modelYear == null ? '' : vehicleInfo.modelYear));
        vehicle.addChildElement('odometerStatus', null, null)
                .addTextNode((processedVehicle.Mileage__c == null ? '' : String.valueOf(processedVehicle.Mileage__c)));
        //        vehicle.addChildElement('rearTireCode', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('restraintSystem', null, null)
        //                .addTextNode('');
        vehicle.addChildElement('saleClassValue', null, null)
                .addTextNode('USED');
        //        vehicle.addChildElement('stickerNumber', null, null)
        //                .addTextNode('');
        vehicle.addChildElement('VIN', null, null)
                .addTextNode(processedVehicle.VIN__c);
        //        vehicle.addChildElement('vehiclePrice', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('vehicleStock', null, null)
        //                .addTextNode('');

        //        //TODO: In the loop
        //        dom.XmlNode options = manufacturer.addChildElement('options', null, null);
        //        vehicle.addChildElement('optionCode', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('optionCost', null, null)
        //                .addTextNode('');
        //        vehicle.addChildElement('optionDescription', null, null)
        //                .addTextNode('');
        //
        //        //TODO: In the loop
        //        dom.XmlNode optionPrice = options.addChildElement('optionPrice', null, null);
        //        optionPrice.addChildElement('optionPricingType', null, null)
        //                .addTextNode('');
        //        optionPrice.addChildElement('price', null, null)
        //                .addTextNode('');
        //
        //TODO: In the loop
        dom.XmlNode owners = arg2.addChildElement('owners', null, null);
        dom.XmlNode id = owners.addChildElement('id', null, null);
        id.addChildElement('assigningPartyId', null, null)
                .addTextNode('CURRENT');
        id.addChildElement('value', null, null)
                .addTextNode(customerNumber);

        dom.XmlNode arg3 = insertTag.addChildElement('arg3', null, null)
            .addTextNode('VEHICLES');

        processedVehicle.VehID__c = vehicleId;

        System.debug('INSERT VEHICLE');
        System.debug('request');
        System.debug(doc.toXmlString());

        HttpResponse response = DMotorWorksApi.post(PATH_INSERT_UPDATE, doc.toXmlString());
        System.debug('response body ' + response.getBody());
        System.debug('response status  ' + response.getStatusCode());
        System.debug('response body document ' + response.getBodyDocument());

        return readInsertUpdateServiceVehicleResponse(response.getBodyDocument(), processedVehicle, 'insertResponse');
    }

    public static CDK_Vehicle__c updateServiceVehicle(CDK_Vehicle__c processedVehicle, String dealerId, String vehicleId, String customerNumber) {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();

        dom.XmlNode vehicleResponse = getVehicleXMLNode(dealerId, vehicleId);
        Dom.XmlNode code = vehicleResponse.getChildElement('code', null);
        Dom.XmlNode vehicle;
        System.debug('request');
        if (code != null && code.getText() == CODE_SUCCESS) {
                System.debug('request1');
                vehicle = vehicleResponse.getChildElement('vehicle', null);
                    dom.XmlNode owners = vehicle.addChildElement('owners', null, null);
                        dom.XmlNode id = owners.addChildElement('id', null, null);
                        id.addChildElement('assigningPartyId', null, null)
                                .addTextNode('CURRENT');
                        id.addChildElement('value', null, null)
                                .addTextNode(customerNumber);    
        } else { 
                System.debug('request2');
                Dom.XmlNode message = vehicleResponse.getChildElement('message', null);
                processedVehicle.CDK_Message__c = 'Vehicle Insert error: ' + message.getText();
                return processedVehicle;
        }

        System.debug('request3');

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', XMLNS_NAMESPACE, 'soapenv');
        envelope.setNamespace('pip', PIP_NAMESPACE);

        dom.XmlNode body = envelope.addChildElement('Body', XMLNS_NAMESPACE, 'soapenv');
        dom.XmlNode insertTag = body.addChildElement('update', PIP_NAMESPACE, 'pip');

        dom.XmlNode arg0 = insertTag.addChildElement('arg0', null, null);
        arg0.addChildElement('password', null, null)
                .addTextNode(settings.Password__c);
        arg0.addChildElement('username', null, null)
                .addTextNode(settings.Username__c);

        dom.XmlNode arg1 = insertTag.addChildElement('arg1', null, null);
        arg1.addChildElement('id', null, null)
                .addTextNode(dealerId);

        dom.XmlNode arg2 = insertTag.addChildElement('arg2', null, null);
        for (Dom.XmlNode vehicleChild : vehicle.getChildElements()) {
                Dom.XmlNode tempNode = arg2.insertBefore(vehicleChild, null);
        }
        
        dom.XmlNode arg3 = insertTag.addChildElement('arg3', null, null)
            .addTextNode('VEHICLES');

        System.debug('UPDATE VEHICLE');
        System.debug('request');
        System.debug(doc.toXmlString());

        HttpResponse response = DMotorWorksApi.post(PATH_INSERT_UPDATE, doc.toXmlString());
        System.debug('response body ' + response.getBody());
        System.debug('response status  ' + response.getStatusCode());
        System.debug('response body document ' + response.getBodyDocument());

        return readInsertUpdateServiceVehicleResponse(response.getBodyDocument(), processedVehicle, 'updateResponse');
        // return processedVehicle;
    }

    public static CDK_Vehicle__c readInsertUpdateServiceVehicleResponse(Dom.Document doc, CDK_Vehicle__c processedVehicle, String responseType) {
        Dom.XmlNode envelopeResult = doc.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement('Body', XMLNS_NAMESPACE);
        Dom.XmlNode response = bodyResult.getChildElement(responseType, PIP_NAMESPACE);
        Dom.XmlNode returnResult = response.getChildElement('return', null);
        Dom.XmlNode code = returnResult.getChildElement('code', null);

        if (code != null) {
            if (code.getText() == CODE_FAILURE) {
                Dom.XmlNode message = returnResult.getChildElement('message', null);
                processedVehicle.CDK_Message__c = 'Vehicle Insert error: ' + message.getText();
            } else if (code.getText() == CODE_SUCCESS) {
                Dom.XmlNode vehicle = returnResult.getChildElement('vehicle', null);
                Dom.XmlNode checksum = vehicle.getChildElement('checksum', null);
                processedVehicle.CDK_Checksum__c = checksum.getText();
                processedVehicle.CDK_Message__c = '';
            }
        }

        return processedVehicle;
    }

    public static Dom.XmlNode getVehicleXMLNode(String dealerId, String vehicleId) {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', XMLNS_NAMESPACE, 'soapenv');
        envelope.setNamespace('pip', PIP_NAMESPACE);

        dom.XmlNode body = envelope.addChildElement('Body', XMLNS_NAMESPACE, 'soapenv');
        dom.XmlNode insertTag = body.addChildElement('read', PIP_NAMESPACE, 'pip');

        dom.XmlNode arg0 = insertTag.addChildElement('arg0', null, null);
        arg0.addChildElement('password', null, null)
                .addTextNode(settings.Password__c);
        arg0.addChildElement('username', null, null)
                .addTextNode(settings.Username__c);

        dom.XmlNode arg1 = insertTag.addChildElement('arg1', null, null);
        arg1.addChildElement('id', null, null)
                .addTextNode(dealerId);

        dom.XmlNode arg2 = insertTag.addChildElement('arg2', null, null);
        arg2.addChildElement('fileType', null, null)
                .addTextNode('VEHICLES');
        arg2.addChildElement('vehiclesVehicleId', null, null)
                .addTextNode(vehicleId);

        HttpResponse response = DMotorWorksApi.post(PATH_INSERT_UPDATE, doc.toXmlString());

        System.debug('GET VEHICLE');
        System.debug('request');
        System.debug(doc.toXmlString());

        System.debug('response body ' + response.getBody());
        System.debug('response status  ' + response.getStatusCode());
        System.debug('response body document ' + response.getBodyDocument());

        return readGetVehicleResponse(response.getBodyDocument());
    }

    public static Dom.XmlNode readGetVehicleResponse(Dom.Document doc) {
        Dom.XmlNode envelopeResult = doc.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement('Body', XMLNS_NAMESPACE);
        Dom.XmlNode response = bodyResult.getChildElement('readResponse', PIP_NAMESPACE);

        Dom.XmlNode returnResult = response.getChildElement('return', null);
        Dom.XmlNode code = returnResult.getChildElement('code', null);

        if (code != null) {
                if (code.getText() == CODE_SUCCESS) {
                    Dom.XmlNode vehicle = returnResult.getChildElement('vehicle', null);
                        for (Dom.XmlNode child : vehicle.getChildElements()) {
                                if (child.getName() == 'owners') {
                                        vehicle.removeChild(child);
                                }
                        }
                }
        }

        // DOM.Document doc1 = new DOM.Document();
        // dom.XmlNode envelope = doc1.createRootElement('Envelope', XMLNS_NAMESPACE, 'soapenv');
        // returnResult = envelope.insertBefore(returnResult, null);
        // System.debug(doc1.toXmlString());

        return returnResult;
    }

    public static VehicleIdWrapper getVehicleId(String dealerId, String vinNumber) {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', XMLNS_NAMESPACE, 'soapenv');
        envelope.setNamespace('pip', PIP_NAMESPACE);

        dom.XmlNode body = envelope.addChildElement('Body', XMLNS_NAMESPACE, 'soapenv');
        dom.XmlNode insertTag = body.addChildElement('getVehIds', PIP_NAMESPACE, 'pip');

        dom.XmlNode arg0 = insertTag.addChildElement('arg0', null, null);
        arg0.addChildElement('password', null, null)
                .addTextNode(settings.Password__c);
        arg0.addChildElement('username', null, null)
                .addTextNode(settings.Username__c);

        dom.XmlNode arg1 = insertTag.addChildElement('arg1', null, null);
        arg1.addChildElement('id', null, null)
                .addTextNode(dealerId);

        dom.XmlNode arg2 = insertTag.addChildElement('arg2', null, null);
        arg2.addChildElement('VIN', null, null)
                .addTextNode(vinNumber);

        HttpResponse response = DMotorWorksApi.post(PATH_INSERT_UPDATE, doc.toXmlString());

        System.debug('GET VEHICLE ID');
        System.debug('request');
        System.debug(doc.toXmlString());

        System.debug('response body ' + response.getBody());
        System.debug('response status  ' + response.getStatusCode());
        System.debug('response body document ' + response.getBodyDocument());

        return readGetVehicleIdResponse(response.getBodyDocument());
    }

    public static VehicleIdWrapper readGetVehicleIdResponse(Dom.Document doc) {
        Dom.XmlNode envelopeResult = doc.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement('Body', XMLNS_NAMESPACE);
        Dom.XmlNode response = bodyResult.getChildElement('getVehIdsResponse', PIP_NAMESPACE);

        List<VehicleIdWrapper> vehicles = new List<VehicleIdWrapper>();
        for (Dom.XmlNode child : response.getChildElements()) {
                System.debug(child);
                // Dom.XmlNode returnResult = response.getChildElement('return', null);
                if (child.getChildElement('code', null).getText() == 'success') {
                        VehicleIdWrapper vehicleId = new VehicleIdWrapper();
                        vehicleId.newId = child.getChildElement('newId', null).getText();
                        vehicleId.vehiclesVehId = child.getChildElement('vehiclesVehId', null).getText();
                        System.debug(vehicleId);
                        vehicles.add(vehicleId);
                }
        }

        System.debug(vehicles);

        if(!vehicles.isEmpty()) {
                for (VehicleIdWrapper vehicle : vehicles) {
                        System.debug(vehicle);
                        if(String.isNotBlank(vehicle.vehiclesVehId)) {
                                return vehicle;
                        }
                }
                return null;
                
        } else {
                return null;
        }
    // return vehicleId;
    }

    public static VehicleInfoWrapper explodeVIN(String dealerId, String vinNumber) {
        CDK_Integration__c settings = CDK_Integration__c.getOrgDefaults();

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', XMLNS_NAMESPACE, 's');
        envelope.setNamespace('pip', PIP_NAMESPACE);

        dom.XmlNode body = envelope.addChildElement('Body', XMLNS_NAMESPACE, 's');
        dom.XmlNode insertTag = body.addChildElement('explodeVIN', PIP_NAMESPACE, 'ns2');

        dom.XmlNode arg0 = insertTag.addChildElement('arg0', null, null);
        arg0.addChildElement('password', null, null)
                .addTextNode(settings.Password__c);
        arg0.addChildElement('username', null, null)
                .addTextNode(settings.Username__c);

        dom.XmlNode arg1 = insertTag.addChildElement('arg1', null, null);
        arg1.addChildElement('id', null, null)
                .addTextNode(dealerId);

        dom.XmlNode arg2 = insertTag.addChildElement('arg2', null, null);
        arg2.addChildElement('VIN', null, null)
                .addTextNode(vinNumber);

        System.debug('EXPLODE VIN');
        System.debug('request');
        System.debug(doc.toXmlString());

        HttpResponse response = DMotorWorksApi.post(PATH_INSERT_UPDATE, doc.toXmlString());
        System.debug('response body ' + response.getBody());
        System.debug('response status  ' + response.getStatusCode());
        System.debug('response body document ' + response.getBodyDocument());

        return readExplodeVINResponse(response.getBodyDocument());
    }

    public static VehicleInfoWrapper readExplodeVINResponse(Dom.Document doc) {
        Dom.XmlNode envelopeResult = doc.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement('Body', XMLNS_NAMESPACE);
        Dom.XmlNode response = bodyResult.getChildElement('explodeVINResponse', PIP_NAMESPACE);
        Dom.XmlNode returnResult = response.getChildElement('return', null);

        Dom.XmlNode vehicle = returnResult.getChildElement('vehicle', null);
        Dom.XmlNode vehicleInner = vehicle.getChildElement('vehicle', null);

        VehicleInfoWrapper vehicleInfo = new VehicleInfoWrapper();
        vehicleInfo.make = vehicleInner.getChildElement('make', null).getText();
        vehicleInfo.model = vehicleInner.getChildElement('model', null).getText();
        vehicleInfo.modelAbrev = vehicleInner.getChildElement('modelAbrev', null).getText();
        vehicleInfo.modelYear = vehicleInner.getChildElement('modelYear', null).getText();

        System.debug('vehicleInfo');
        System.debug(vehicleInfo);

        return vehicleInfo;
    }

    public class VehicleIdWrapper {
        public String newId;
        public String vehiclesVehId;
    }

    public class VehicleInfoWrapper {
        public String make;
        public String model;
        public String modelAbrev;
        public String modelYear;
    }
}