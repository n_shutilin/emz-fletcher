public class ServiceVehicleParser {

    public static final String NAMESPACE = 'http://www.dmotorworks.com/pip-extract-service-vehicle';
    public static final String CHILD_NAME = 'ServiceVehicle';

    public static List<Vehicle__c> process(Dom.Document doc, String accountId) {
        List<Vehicle__c> vehicles = new List<Vehicle__c>();
        String dealerCode = [SELECT Id, CDK_Dealer_Code__c FROM Account WHERE Id = :accountId].CDK_Dealer_Code__c;
        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == CHILD_NAME) {
                Vehicle__c vehicle = new Vehicle__c();  
                mapFields(vehicle, child);
                vehicle.UniqueVehicleId__c = dealerCode + '_' + vehicle.VehID__c + '_' + vehicle.HostItemID__c;
                vehicles.add(vehicle);
            }
        }

        return vehicles;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'AccountingAccount' => Vehicle__c.AccountingAccount__c,
            'ErrorLevel' => Vehicle__c.ErrorLevel__c,
            'Model' => Vehicle__c.Model__c,
            'ModelName' => Vehicle__c.ModelName__c,
            'TransmissionNo' => Vehicle__c.TransmissionNo__c,
            'Color' => Vehicle__c.Color__c,
            'DealerCode' => Vehicle__c.DealerCode__c,
            'HostItemID' => Vehicle__c.HostItemID__c,
            'UnitNo' => Vehicle__c.UnitNo__c,
            'VIN' => Vehicle__c.VIN__c,
            'VehID' => Vehicle__c.VehID__c,
            'Year' => Vehicle__c.Year__c,
            'StockNo' => Vehicle__c.StockNo__c,
            'StockType' => Vehicle__c.StockType__c,
            'LastServiceDate' => Vehicle__c.LastServiceDate__c,
            'Make' => Vehicle__c.Make__c,
            'MakeName' => Vehicle__c.MakeName__c,
            'Mileage' => Vehicle__c.Mileage__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, NAMESPACE);

            System.debug('~~ ' + childElement);
            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}