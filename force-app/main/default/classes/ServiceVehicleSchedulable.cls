global class ServiceVehicleSchedulable implements Schedulable{

    global void execute(SchedulableContext SC) {
        ServiceVehicleBatchable batch = new ServiceVehicleBatchable(1);
        Database.executeBatch(batch, 1);

        Datetime nextScheduleTime = System.now().addMinutes(120);
        String hour = String.valueOf(nextScheduleTime.hour());
        String minutes = String.valueOf(nextScheduleTime.minute());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        String jobName = 'Service Vehicle Extract ' + nextScheduleTime.format('hh:mm');
        ServiceVehicleSchedulable p = new ServiceVehicleSchedulable();
        System.schedule(jobName, cronValue , p);

        System.abortJob(SC.getTriggerId());
    }

}