public class ServiceWorkTypesHandler {

    public static final String PATH_WORK_TYPES = '/pip-extract/opcode/extract';
    public static final String QUERY_WORK_TYPES_DETAILS_ID = 'OPS_Delta';
    public static final String QUERY_WORK_TYPES_BULK_ID = 'OPS_Bulk';
    public static final String DEALERID_PARAM = 'dealerId';
    public static final String QUERYID_PARAM = 'queryId';
    public static final String DELTADATE_PARAM = 'deltaDate';
    public static final String DELTADATE_FORMAT = 'MM/dd/yyyy';

    public static List<WorkType> getWorkTypes(String dealerId, Integer deltaDays, Id accountId, Boolean isBulk) {
        Map<String, String> paramsMap = new Map<String, String>();
        if (!isBulk) {
            Datetime deltaDate = Datetime.now().addDays(-deltaDays);
            String deltaDaysStr = deltaDate.format(DELTADATE_FORMAT);
            paramsMap.put(DELTADATE_PARAM, deltaDaysStr);
        }
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, isBulk ? QUERY_WORK_TYPES_BULK_ID : QUERY_WORK_TYPES_DETAILS_ID);
        HttpResponse res = DMotorWorksApi.get(PATH_WORK_TYPES, paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<WorkType> workTypes = ServiceWorkTypesParser.process(res.getBodyDocument(), accountId);
        System.debug('WorkType ' + workTypes);
        return workTypes;
    }
}