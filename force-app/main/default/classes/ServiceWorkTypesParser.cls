public class ServiceWorkTypesParser {

    public static final String NAMESPACE = 'http://www.dmotorworks.com/pip-extract-opcode';
    public static final String CHILD_NAME = 'OpCode';
    public static final String DURATION_TYPE_HOURS = 'Hours';

    public static List<WorkType> process(Dom.Document doc, String accountId) {
        List<WorkType> workTypes = new List<WorkType>();
        String dealerCode = [SELECT Id, CDK_Dealer_Code__c FROM Account WHERE Id = :accountId].CDK_Dealer_Code__c;
        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == CHILD_NAME) {
                WorkType workTypeItem = new WorkType();  
                mapFields(workTypeItem, child);
                workTypeItem.UniqueWorkTypetId__c = dealerCode + '_' + workTypeItem.ItemID__c;
                workTypeItem.DurationType = DURATION_TYPE_HOURS;
                workTypeItem.Name = workTypeItem.ItemID__c;
                workTypes.add(workTypeItem);
            }
        }

        return workTypes;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'ShopChargeFlagCP' => WorkType.ShopChargeFlagCP__c,
            'ShopChargeFlagWP' => WorkType.ShopChargeFlagWP__c,
            'ShopChargeFlagIP' => WorkType.ShopChargeFlagIP__c,
            'OperationType' => WorkType.OperationType__c,
            'Watched' => WorkType.Watched__c,
            'ItemID' => WorkType.ItemID__c,
            'ComebackFlag' => WorkType.ComebackFlag__c,
            'CorrectionOpCode' => WorkType.CorrectionOpCode__c,
            'Description' => WorkType.Description,
            'DiscountFlag' => WorkType.DiscountFlag__c,
            'EntryDate' => WorkType.EntryDate__c,
            'FlatHours' => WorkType.EstimatedDuration
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, NAMESPACE);

            System.debug('~~ ' + childElement);
            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}