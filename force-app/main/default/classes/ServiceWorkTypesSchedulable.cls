global class ServiceWorkTypesSchedulable implements Schedulable{

    global void execute(SchedulableContext SC) {
        ServiceWorkTypesBatchable batch = new ServiceWorkTypesBatchable(1, false);
        Database.executeBatch(batch, 1);

        Datetime nextScheduleTime = System.now().addMinutes(120);
        String hour = String.valueOf(nextScheduleTime.hour());
        String minutes = String.valueOf(nextScheduleTime.minute());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        String jobName = 'Work Type Extract ' + nextScheduleTime.format('hh:mm');

        ServiceWorkTypesSchedulable p = new ServiceWorkTypesSchedulable();
        System.schedule(jobName, cronValue , p);

        System.abortJob(SC.getTriggerId());
    }

}