public class ShowDealInfoController {

    @AuraEnabled
    public static String getShownDealInfo(String oppId) {
        if (Id.valueOf(oppId).getSobjectType() == Schema.Opportunity.SObjectType) {
            List<Schema.FieldSetMember> fieldSetFields = SObjectType.Deal__c.FieldSets.Shown_On_Opp.getFields();
            Map<String, String> nameToApiName = new Map<String, String>();
            List<String> fields = new List<String>();
            for (Schema.FieldSetMember field : fieldSetFields) {
                nameToApiName.put(field.getFieldPath(), field.getLabel());
                fields.add(field.getFieldPath());
            }
            Deal__c deal = getFieldValues(fields, oppId);
            List<DealInfoWrapper> dealInfo = new List<DealInfoWrapper>();
            for (String field : nameToApiName.keySet()) {
                DealInfoWrapper newDealInfoWrapper = new DealInfoWrapper(nameToApiName.get(field), deal.get(field.toLowerCase()));
                dealInfo.add(newDealInfoWrapper);
            }
            return JSON.serialize(dealInfo);
        } else {
            return '';
        }

    }

    public static Deal__c getFieldValues(List<String> fields, String oppId) {
        Id dealId = [SELECT Deal__c FROM Opportunity WHERE Id = :oppId LIMIT 1].Deal__c;
        Deal__c deal;
        if (dealId != null) {
            String query = 'SELECT ' + String.join(fields, ', ') + ' FROM Deal__c WHERE Id = \'' + dealId + '\' LIMIT 1';
            deal = Database.query(query);
        }
        return deal;
    }

    public class DealInfoWrapper {
        public String fieldName;
        public Object value;

        public DealInfoWrapper(String fieldName, Object value) {
            this.fieldName = fieldName;
            this.value = value;
        }
    }
}