@IsTest
public class ShowDealInfoControllerTest {

    @TestSetup
    public static void setup() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Lease Retention').getRecordTypeId();
        Account account = new Account(Name = 'TestAccount', RecordTypeId = accRecordTypeId);
        insert account;
        Deal__c deal = new Deal__c(LeasMSRP__c = 111.11, UniqueDealerNumber__c = 'AUDFJ-111');
        insert deal;
        Opportunity opp = new Opportunity(
            Name = 'TestOpportunity',
            AccountId = account.Id,
            StageName = 'New',
            Deal__c = deal.Id,
            CloseDate = System.today(),
            RecordTypeId = oppRecordTypeId
        );
        insert opp;
    }

    @IsTest
    public static void getShownDealInfoTest() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        Test.startTest();
        String result = ShowDealInfoController.getShownDealInfo(opp.Id);
        Test.stopTest();
        System.assertNotEquals(result, '');
    }
}