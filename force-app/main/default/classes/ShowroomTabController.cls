public with sharing class ShowroomTabController {
    private static final String QUEUE_GROUP_TYPE_NAME = 'Queue';
    public static final String TYPE_LOG = 'action:log_event';
    public static final String WORKING_STAGE = '';
    public static final String VEHICLE_ORDERED_STAGE = 'action:record';
    public static final String APPOINTMENT_STAGE = 'action:following';
    public static final String TEST_DRIVE_STAGE = 'action:map';
    public static final String ACTIVE_IN_SHOWROOM_STAGE = 'action:new_contact';
    public static final String DESKING_STAGE = 'action:upload';
    public static final String GUEST_INTERVIEW_STAGE = 'action:question_post_action';
    public static final String DEFERRED_STAGE = 'action:defer';
    public static final String PENDING_SALE_STAGE = 'action:update_status';
    public static final String SOLD_NOT_DELIVERED_STAGE = 'action:update';
    public static final String SOLD_DELIVERED_STAGE = 'action:approval';
    private static final String query_fields = 'Id, Name, Dealer_ID__c, CreatedDate, Opportunity__c, Opportunity__r.Id, Opportunity__r.Name, Opportunity__r.StageName, Showroom_Status_formula__c, Salesperson__c, Salesperson__r.Name, Spotter__c, Spotter__r.Name, Opportunity__r.LastModifiedDate, Interview_Comments__c, Opportunity__r.Stock_Number__c, Opportunity__r.Vehicle_Type__c, Opportunity__r.Vehicle_Year__c, Opportunity__r.Vehicle_Make__c, Opportunity__r.Vehicle_Model__c, Opportunity__r.Vehicle_Trim__c, Opportunity__r.Lead_Source__c, Opportunity__r.Lead_Source__r.Name, Opportunity__r.Owner.Name';

    @AuraEnabled(cacheable=true)
    public static List<ResultDto> getShowroomList() {
        String query = addMainShowroomCondition();
        query += ' AND CreatedDate = TODAY ORDER BY CreatedDate DESC';

        List<In_Showroom__c> showroomList = new List<In_Showroom__c>();
        try {
            showroomList = Database.query(query);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
        List<ResultDto> resultList = mapResult(showroomList);
        return resultList;
    }

    @AuraEnabled(cacheable=true)
    public static List<SelectOptionDto> getTeamsForShowing() {
        List<SelectOptionDto> result = getUsers();

        List<Group> groups = [
            SELECT Id, Name
            FROM Group
            WHERE Type = :QUEUE_GROUP_TYPE_NAME
        ];

        for (Group item : groups) {
            result.add(new SelectOptionDto(
                item.Name,
                item.Id
            ));
        }
        return result;
    }

    @AuraEnabled(cacheable=true)
    public static List<SelectOptionDto> getUsers() {
        List<User> users = [
            SELECT Id, Name
            FROM User
        ];

        List<SelectOptionDto> result = new List<SelectOptionDto>();
        for (User item : users) {
            result.add(new SelectOptionDto(
                item.Name,
                item.Id
            ));
        }

        result.add(0, new SelectOptionDto('All', 'All'));
        return result;
    }

    @AuraEnabled(cacheable=true)
    public static List<SelectOptionDto> getYearList() {
        List<SelectOptionDto> selectOptions = new List<SelectOptionDto>();
        Integer currentYear = Datetime.now().year() + 1;
        String year = '';
        for (Integer i = 0; i < 20; i++) {
            year = String.valueOf(currentYear - i);
            selectOptions.add(new SelectOptionDto(year, year));
        }
        return selectOptions;
    }

    @AuraEnabled
    public static List<ResultDto> sortTable(String arrivalDateFrom, String arrivalDateTo, List<String> showroomStatus, String vehicleType, String vehicleMake, String vehicleDateFrom, String vehicleDateTo, String vehicleModel, String vehicleTrim, String team, String lastActivityBy, String showroomDealer) {
        String arrivalDateFromStr;
        if (arrivalDateFrom != null) {
            arrivalDateFromStr = ' CreatedDate >= ' + arrivalDateFrom + 'T00:00:00Z';
        } else {
            arrivalDateFromStr = null;
        }

        String arrivalDateToStr;
        if (arrivalDateTo != null) {
            arrivalDateToStr = ' CreatedDate <= ' + arrivalDateTo + 'T23:59:59Z';
        } else {
            arrivalDateToStr = null;
        }

        String showroomStatusStr;
        if (!showroomStatus.isEmpty()) {
            showroomStatusStr = ' Showroom_Status_formula__c IN (';
            for (String str : showroomStatus) {
                showroomStatusStr += '\'' + str + '\',';
            }
            showroomStatusStr = showroomStatusStr.removeEnd(',');
            showroomStatusStr += ')';
        } else {
            showroomStatusStr = null;
        }

        String vehicleTypeStr = vehicleType != null && vehicleType != 'All' ? ' Opportunity__r.Vehicle__r.Inventory__r.TypeNUName__c = \'' + vehicleType + '\'' : null;
        String vehicleMakeStr = vehicleMake != null ? ' Opportunity__r.Vehicle__r.Make__c = \'' + vehicleMake + '\'' : null;
        String vehicleDateFromStr = vehicleDateFrom != null ? ' Opportunity__r.Vehicle__r.Year__c >= \'' + vehicleDateFrom + '\'' : null;
        String vehicleDateToStr = vehicleDateTo != null ? ' Opportunity__r.Vehicle__r.Year__c <= \'' + vehicleDateTo + '\'' : null;
        String vehicleModelStr = vehicleModel != null ? ' Opportunity__r.Vehicle__r.Model__c= \'' + vehicleModel + '\'' : null;
        String vehicleTrimStr = vehicleTrim != null ? ' Opportunity__r.Vehicle__r.Trim__c= \'' + vehicleTrim + '\'' : null;
        String teamStr = team != null && team != 'All' ? ' OwnerId = \'' + team + '\'' : null;
        String lastActivityByStr = lastActivityBy != null && lastActivityBy != 'All' ? ' Opportunity__r.LastModifiedById = \'' + lastActivityBy + '\'' : null;
        String showroomDealerStr = showroomDealer != null && showroomDealer != 'All' ? ' Dealer_ID__c = \'' + showroomDealer + '\'' : null;

        String condition = '';
        condition = addCondition(condition, arrivalDateFromStr);
        condition = addCondition(condition, arrivalDateToStr);
        condition = addCondition(condition, showroomStatusStr);
        condition = addCondition(condition, vehicleTypeStr);
        condition = addCondition(condition, vehicleMakeStr);
        condition = addCondition(condition, vehicleDateFromStr);
        condition = addCondition(condition, vehicleDateToStr);
        condition = addCondition(condition, vehicleModelStr);
        condition = addCondition(condition, vehicleTrimStr);
        condition = addCondition(condition, teamStr);
        condition = addCondition(condition, lastActivityByStr);
        condition = addCondition(condition, showroomDealerStr);

        String fullCondition = addMainShowroomCondition();

        if (condition.length() > 0) {
            fullCondition += ' AND' + condition;
        }

        fullCondition += ' ORDER BY CreatedDate DESC';

        List<In_Showroom__c> inShowroomList = new List<In_Showroom__c>();
        try {
            inShowroomList = Database.query(fullCondition);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
        List<ResultDto> resultList = mapResult(inShowroomList);
        return resultList;
    }

    public static String addCondition(String condition, String value) {
        if (value != null) {
            condition += condition.length() > 0 ? ' AND' + value : value;
        }
        return condition;
    }

    public static String addMainShowroomCondition() {
        String query = 'SELECT ' + query_fields + ' FROM In_Showroom__c WHERE';
        String dealerId = getUserDealer();

        if (dealerId != 'All Dealer Access' && dealerId != null) {
            query += ' Dealer_ID__c =\'' + dealerId + '\' AND';
        }

        Set<String> latestShowroomSet = getLatestShowroom();

        if (!latestShowroomSet.isEmpty()) {
            String latestShowroomIds = ' (Opportunity__c = NULL OR Id IN (';

            for (String str : latestShowroomSet) {
                latestShowroomIds += '\'' + str + '\',';
            }
            latestShowroomIds = latestShowroomIds.removeEnd(',');
            latestShowroomIds += '))';

            query += latestShowroomIds;
        } else {
            query += ' AND Opportunity__c = NULL';
        }
        return query;
    }

    public static String getUserDealer() {
        String dealerId = '';

        if ([SELECT All_Dealer_Access__c FROM User WHERE Id = :UserInfo.getUserId()].All_Dealer_Access__c) {
            dealerId = 'All Dealer Access';
        } else {
            dealerId = [
                SELECT ServiceTerritory.Dealer__c
                FROM ServiceTerritoryMember
                WHERE ServiceResource.RelatedRecordId = :UserInfo.getUserId()
                AND EffectiveStartDate <= TODAY
                AND (EffectiveEndDate = NULL OR EffectiveEndDate >= TODAY)
            ].ServiceTerritory.Dealer__c;
        }
        return dealerId;
    }

    public static Set<String> getLatestShowroom() {
        List<Opportunity> opportunityList = [
            SELECT Id, (
                SELECT Id
                FROM In_Showrooms__r
                ORDER BY CreatedDate DESC
                LIMIT 1
            )
            FROM Opportunity
            WHERE Id IN (
                SELECT Opportunity__c
                FROM In_Showroom__c
            )
        ];

        Set<String> showroomIds = new Set<String>();
        for (Opportunity opportunity : opportunityList) {
            showroomIds.add(opportunity.In_Showrooms__r[0].Id);
        }

        return showroomIds;
    }

    public static List<ResultDto> mapResult(List<In_Showroom__c> showroomList) {
        Set<String> opportunityIds = new Set<String>();
        for (In_Showroom__c showroom : showroomList) {
            opportunityIds.add(showroom.Opportunity__r.Id);
        }

        List<Opportunity> opportunityList = new List<Opportunity>();
        if (!opportunityIds.isEmpty()) {
            opportunityList = [
                SELECT Id, (
                    SELECT Opportunity__c
                    FROM Sales_Appointments__r
                    LIMIT 1
                )
                FROM Opportunity
                WHERE Id IN :opportunityIds
            ];
        }

        // Map fields for DTO
        List<ResultDto> resultList = new List<ResultDto>();
        for (In_Showroom__c showroom : showroomList) {
            Boolean appt = false;
            for (Opportunity opportunity : opportunityList) {
                if (!opportunity.Sales_Appointments__r.isEmpty()) {
                    appt = showroom.Opportunity__c == opportunity.Sales_Appointments__r[0].Opportunity__c ? true : false;
                }
            }

            String salesperson = showroom.Salesperson__c != null ? showroom.Salesperson__r.Name : '';
            String showroomLink = '/' + showroom.Id;
            String opportunityLink = showroom.Opportunity__c != null ? '/' + showroom.Opportunity__r.Id : '';
            String opportunityName = showroom.Opportunity__c != null ? showroom.Opportunity__r.Name : '';
            String opportunityOwnerName = showroom.Opportunity__c != null ? 'Interviewed By: ' + showroom.Opportunity__r.Owner.Name + '; ' : '';
            String comments = showroom.Interview_Comments__c != null ? showroom.Interview_Comments__c : '';
            String description = opportunityOwnerName + ' ' + comments;
            Datetime lastActivity = showroom.Opportunity__c != null ? showroom.Opportunity__r.LastModifiedDate : null;
            String stockNumber = showroom.Opportunity__c != null && showroom.Opportunity__r.Stock_Number__c != null ? 'Stock Number#:' + showroom.Opportunity__r.Stock_Number__c : '';
            String vehicleType = showroom.Opportunity__c != null && showroom.Opportunity__r.Vehicle_Type__c != null ? ' ' + showroom.Opportunity__r.Vehicle_Type__c : '';
            String vehicleYear = showroom.Opportunity__c != null && showroom.Opportunity__r.Vehicle_Year__c != null ? ' ' + showroom.Opportunity__r.Vehicle_Year__c : '';
            String vehicleMake = showroom.Opportunity__c != null && showroom.Opportunity__r.Vehicle_Make__c != null ? ' ' + showroom.Opportunity__r.Vehicle_Make__c : '';
            String vehicleModel = showroom.Opportunity__c != null && showroom.Opportunity__r.Vehicle_Model__c != null ? ' ' + showroom.Opportunity__r.Vehicle_Model__c : '';
            String vehicleTrim = showroom.Opportunity__c != null && showroom.Opportunity__r.Vehicle_Trim__c != null ? ' ' + showroom.Opportunity__r.Vehicle_Trim__c : '';
            String vehicle = stockNumber + vehicleType + vehicleYear + vehicleMake + vehicleModel + vehicleTrim;
            String vehicleLeadSource = showroom.Opportunity__c != null ? showroom.Opportunity__r.Lead_Source__c != null ? showroom.Opportunity__r.Lead_Source__r.Name : '' : '';
            String spotterName = showroom.Spotter__c != null ? showroom.Spotter__r.Name : 'Up System';
            String type = checkType(showroom.Showroom_Status_formula__c);

            resultList.add(new ResultDto(
                showroom.Id,
                showroomLink,
                showroom.Name,
                showroom.Dealer_ID__c,
                type,
                showroom.CreatedDate,
                opportunityLink,
                opportunityName,
                showroom.Showroom_Status_formula__c,
                appt,
                salesperson,
                spotterName,
                lastActivity,
                description,
                vehicle,
                vehicleLeadSource
            ));
        }
        return resultList;
    }

    public static String checkType(String status) {
        String type = status == 'Working' ? WORKING_STAGE : status == 'Vehicle Ordered/PO' ? VEHICLE_ORDERED_STAGE :
            status == 'Appointment Scheduled' ? APPOINTMENT_STAGE : status == 'Test Drive' ? TEST_DRIVE_STAGE :
                status == 'Active In-Showroom' ? ACTIVE_IN_SHOWROOM_STAGE : status == 'Desking' ? DESKING_STAGE :
                    status == 'Guest Interview' ? GUEST_INTERVIEW_STAGE : status == 'Deferred' ? DEFERRED_STAGE :
                        status == 'Pending Sale' ? PENDING_SALE_STAGE : status == 'Sold/Not Delivered' ? SOLD_NOT_DELIVERED_STAGE :
                            status == 'Sold/Delivered' ? SOLD_DELIVERED_STAGE : status == null ? TYPE_LOG : '';
        return type;
    }

    @AuraEnabled
    public static void addLinkOpportunity(String showroomId, String opportunityId) {
        In_Showroom__c showroom = new In_Showroom__c();
        if (showroomId != null && opportunityId != null) {
            showroom = [
                SELECT Opportunity__c
                FROM In_Showroom__c
                WHERE Id = :showroomId
            ];

            showroom.Opportunity__c = opportunityId;
            showroom.In_Showroom_Time__c = Datetime.now();
            showroom.Showroom_Protection_Expire_Date__c = Date.today();
            update showroom;
        }
    }

    public class SelectOptionDto {
        @AuraEnabled
        public String value;
        @AuraEnabled
        public String label;

        public SelectOptionDto(String name, String value) {
            this.label = name;
            this.value = value;
        }
    }

    public class ResultDto {
        @AuraEnabled public String id;
        @AuraEnabled public String showroomLink;
        @AuraEnabled public String name;
        @AuraEnabled public String dealerId;
        @AuraEnabled public String type;
        @AuraEnabled public Datetime arrivalTime;
        @AuraEnabled public String opportunityLink;
        @AuraEnabled public String opportunityName;
        @AuraEnabled public String showroomStatus;
        @AuraEnabled public Boolean appt;
        @AuraEnabled public String salesperson;
        @AuraEnabled public String spotter;
        @AuraEnabled public Datetime lastActivity;
        @AuraEnabled public String descriptionStr;
        @AuraEnabled public String vehicle;
        @AuraEnabled public String opportunityLeadSource;

        public ResultDto(String id, String showroomLink, String name, String dealerId, String type, Datetime arrivalTime, String opportunityLink, String opportunityName, String showroomStatus, Boolean appt, String salesperson, String spotter, Datetime lastActivity, String descriptionStr, String vehicle, String opportunityLeadSource) {
            this.id = id;
            this.showroomLink = showroomLink;
            this.name = name;
            this.dealerId = dealerId;
            this.type = type;
            this.arrivalTime = arrivalTime;
            this.opportunityLink = opportunityLink;
            this.opportunityName = opportunityName;
            this.showroomStatus = showroomStatus;
            this.appt = appt;
            this.salesperson = salesperson;
            this.spotter = spotter;
            this.lastActivity = lastActivity;
            this.descriptionStr = descriptionStr;
            this.vehicle = vehicle;
            this.opportunityLeadSource = opportunityLeadSource;
        }
    }
}