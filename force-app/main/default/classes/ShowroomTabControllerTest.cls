@IsTest
public class ShowroomTabControllerTest {
    private final static String DEALER_ID = 'NBMBN';

    @TestSetup
    public static void setup() {
        Profile profile = [
            SELECT Id
            FROM Profile
            WHERE Name = 'Standard User'
        ];
        User user = new User(
            Alias = 'standt',
            Email = 'standarduser@testemz.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'Testing',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = profile.Id,
            TimeZoneSidKey = 'America/Los_Angeles',
            Username = 'standarduser@testemz.com.com');
        insert user;

        ServiceResource serviceResource = new ServiceResource(
            Name = 'Test Resource',
            RelatedRecordId = user.Id,
            IsActive = true);
        insert serviceResource;

        OperatingHours operatingHours = new OperatingHours(
            Name = 'Test Sales Operating Hours',
            TimeZone = 'America/Los_Angeles',
            Dealer_Code__c = DEALER_ID);
        insert operatingHours;

        ServiceTerritory serviceTerritory = new ServiceTerritory(
            Name = 'Test Dealer',
            Dealer__c = DEALER_ID,
            IsSalesTerritory__c = true,
            IsActive = true,
            OperatingHoursId = operatingHours.Id);
        insert serviceTerritory;

        ServiceTerritoryMember serviceTerritoryMember = new ServiceTerritoryMember(
            EffectiveStartDate = Date.today().addDays(-5),
            EffectiveEndDate = Date.today().addDays(5),
            ServiceResourceId = serviceResource.Id,
            ServiceTerritoryId = serviceTerritory.Id);
        insert serviceTerritoryMember;

        Account account = new Account(
            Name = 'TestAccount',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId());
        insert account;

        Deal__c deal = new Deal__c(
            LeasMSRP__c = 111.11,
            UniqueDealerNumber__c = DEALER_ID + '_01');
        insert deal;

        Opportunity opportunity = new Opportunity(
            Name = 'TestOpportunity',
            AccountId = account.Id,
            StageName = 'New',
            Deal__c = deal.Id,
            Dealer_ID__c = DEALER_ID,
            CloseDate = System.today(),
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Lease Retention').getRecordTypeId()
        );
        insert opportunity;
        opportunity.OwnerId = user.Id;
        update opportunity;
    }

    @IsTest
    public static void getShowroomListTest() {
        User user = [
            SELECT Id
            FROM User
            WHERE Email = 'standarduser@testemz.com'
            LIMIT 1
        ];

        System.runAs(user) {
            Opportunity opportunity = [
                SELECT Id, Dealer_ID__c
                FROM Opportunity
                LIMIT 1
            ];

            In_Showroom__c showroom = new In_Showroom__c(
                Opportunity__c = opportunity.Id,
                Dealer_ID__c = opportunity.Dealer_ID__c
            );
            insert showroom;

            System.assertNotEquals(0, ShowroomTabController.getShowroomList().size());
        }
    }

    @IsTest
    public static void getTeamsForShowingTest() {
        System.assertNotEquals(0, ShowroomTabController.getTeamsForShowing().size());
    }

    @IsTest
    public static void getYearListTest() {
        System.assertNotEquals(0, ShowroomTabController.getYearList().size());
    }

    @IsTest
    public static void sortTableTest() {
        User user = [
            SELECT Id
            FROM User
            WHERE Email = 'standarduser@testemz.com'
            LIMIT 1
        ];

        System.runAs(user) {
            Opportunity opportunity = [
                SELECT Id, Dealer_ID__c
                FROM Opportunity
                LIMIT 1
            ];

            In_Showroom__c showroom = new In_Showroom__c(
                Opportunity__c = opportunity.Id,
                Dealer_ID__c = opportunity.Dealer_ID__c
            );
            insert showroom;

            System.assertEquals(0, ShowroomTabController.sortTable(Date.today().format(), Date.today().addDays(5).format(), new List<String> {'Active'}, null, null, null, null, null, null, null, null, null).size());
        }
    }

    @IsTest
    public static void addLinkOpportunityTest() {
        User user = [
            SELECT Id
            FROM User
            WHERE Email = 'standarduser@testemz.com'
            LIMIT 1
        ];

        System.runAs(user) {
            Opportunity opportunity = [
                SELECT Id, Dealer_ID__c
                FROM Opportunity
                LIMIT 1
            ];

            In_Showroom__c showroom = new In_Showroom__c();
            insert showroom;

            ShowroomTabController.addLinkOpportunity(showroom.Id, opportunity.Id);

            System.assertNotEquals(0, [SELECT Id FROM In_Showroom__c WHERE Opportunity__c = :opportunity.Id].size());
        }
    }
}