public with sharing class SidebarForActionParentsController {

    @AuraEnabled
    public static List<ResultDto> getParentsInfo(Id recordId, String processName, String subProcessName, String teamId, String dateForSearch, String endDateString) {
        Date actionDate = Date.valueOf(dateForSearch);
        Date endDate = Date.valueOf(endDateString);
        recordId = escape(recordId);
        processName = escape(processName);
        subProcessName = escape(subProcessName);
        teamId = escape(teamId);
        dateForSearch = escape(dateForSearch);
        endDateString = escape(endDateString);

        String query = 'SELECT Id, Opportunity__c, Lead__c, Service_Appointment__c,' +
            '                Opportunity__r.Name, Lead__r.Name, Lead__r.isConverted, Lead__r.ConvertedContactId , Lead__r.ConvertedAccountId, Service_Appointment__r.AppointmentNumber' +
            '            FROM Action__c' +
            '            WHERE Name = :subProcessName AND Process_Name__c = :processName  AND Status__c = \'Open\'' +
            '                AND Due_Date__c >= :actionDate AND Due_Date__c <= :endDate ';
        if (String.isNotBlank(teamId)) {
            query += 'AND OwnerId = :teamId';
        }
        List<Action__c> actions = Database.query(query);
        Map<Id, ResultDto> result = new Map<Id, ResultDto>();
        for (Action__c item : actions) {
            if (item.Opportunity__c != null) {
                result.put(item.Opportunity__c, new ResultDto(
                    item.Opportunity__r.Name,
                    generateLink(item.Opportunity__c, processName, subProcessName, teamId, actionDate, endDate),
                    'Opportunity',
                    item.Opportunity__c == recordId
                ));
            }
            if (item.Lead__c != null) {
                if(item.Lead__r.isConverted){
                    result.put(item.Lead__c, new ResultDto(
                        item.Lead__r.Name,
                        new List<String>{item.Lead__r.ConvertedContactId, item.Lead__r.ConvertedAccountId},
                        generateLink(item.Lead__c, processName, subProcessName, teamId, actionDate, endDate),
                        'Lead',
                        item.Lead__c == recordId
                    ));
                } else {
                    result.put(item.Lead__c, new ResultDto(
                        item.Lead__r.Name,
                        generateLink(item.Lead__c, processName, subProcessName, teamId, actionDate, endDate),
                        'Lead',
                        item.Lead__c == recordId
                    ));
                }
            }
            if (item.Service_Appointment__c != null) {
                result.put(item.Service_Appointment__c, new ResultDto(
                    item.Service_Appointment__r.AppointmentNumber,
                    generateLink(item.Service_Appointment__c, processName, subProcessName, teamId, actionDate, endDate),
                    'Service Appointment',
                    item.Service_Appointment__c == recordId
                ));
            }
        }

        return result.values();
    }

    private static String generateLink(Id idToOpen, String processName, String subProcessName, String teamId, Date actionDate, Date endDate) {
        return '/lightning/r/' + idToOpen.getSobjectType() 
                + '/' + idToOpen 
                + '/view?c__action_process_name=' + processName 
                + '&c__action_sub_process_name=' + subProcessName 
                + '&c__team_id=' + teamId 
                + '&c__date=' + String.valueOf(actionDate)
                + '&c__end_date=' + String.valueOf(endDate);
    }

    private static String escape(String value) {
        if (String.isBlank(value)) {
            return value;
        } else {
            return String.escapeSingleQuotes(value);
        }
    }

    public class ResultDto {
        @AuraEnabled
        public String name;
        @AuraEnabled
        public String link;
        @AuraEnabled
        public String typeLabel;
        @AuraEnabled
        public Boolean isCurrentRecord;
        @AuraEnabled
        public List<String> convertedIdList;

        public ResultDto(String name, String link, String typeLabel, Boolean isCurrentRecord) {
            this.name = name;
            this.link = link;
            this.typeLabel = typeLabel;
            this.isCurrentRecord = isCurrentRecord;
            this.convertedIdList = new List<String>();
        }

        
        public ResultDto(String name, List<String> convertedIdList, String link, String typeLabel, Boolean isCurrentRecord) {
            this.name = name;
            this.link = link;
            this.typeLabel = typeLabel;
            this.isCurrentRecord = isCurrentRecord;
            this.convertedIdList = convertedIdList;
        }
    }
}