@IsTest
private class SidebarForActionParentsControllerTest {
    @TestSetup
    static void setup() {
        Account account = new Account(
                Name = 'Test'
        );
        insert account;
        Contact contact = new Contact(
                LastName = 'Test'
        );
        insert contact;
        Opportunity testOpportunity = new Opportunity(
                Name = 'Test',
                StageName = 'Test',
                CloseDate = Date.newInstance(2021, 12, 12),
                Dealer_ID__c = 'CHAUD'
        );

        insert testOpportunity;
        Lead testLead = new Lead(
                Dealer_ID__c = 'CHAUD',
                LastName = 'Test',
                Status = 'Converted'
        );
        insert testLead;

        ServiceAppointment serviceAppointment = new ServiceAppointment(
                ParentRecordId = testOpportunity.Id
        );
        insert serviceAppointment;

        Action__c testAction = new Action__c(
                Name = 'Test_Name',
                Process_Name__c = 'Test_Process',
                Status__c = 'Open',
                Opportunity__c = testOpportunity.Id,
                Lead__c = testLead.Id,
                Due_Date__c = Date.newInstance(2019, 09, 13),
                Service_Appointment__c = serviceAppointment.Id
        );
        insert testAction;

    }
    @IsTest
    static void getParentsInfo() {
        Action__c action = [SELECT Id, Process_Name__c, Name, OwnerId, Opportunity__c, Lead__c FROM Action__c LIMIT 1];
        SidebarForActionParentsController.getParentsInfo(action.Opportunity__c,'Test_Process' ,'Test_Name',  action.OwnerId, '2019-03-02', '2020-03-02');
        SidebarForActionParentsController.getParentsInfo(action.Lead__c,'Test_Process' ,'Test_Name',  action.OwnerId, '2019-03-02', '2020-03-02');
    }
}