global class SpecialOrderDetailBatchable implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {
    public Integer recordsProcessed = 0;
    public Integer recordsTotal = 0;
    public static final String ACCOUNT_DEALER_RT = 'Dealer';
    public Integer deltaDays;
    public Boolean isBulk;

    global SpecialOrderDetailBatchable() {
        this.deltaDays = 1;
        this.isBulk = false;
    }

    global SpecialOrderDetailBatchable(Integer deltaDays, Boolean isBulk) {
        this.deltaDays = deltaDays;
        this.isBulk = isBulk;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query =
            'SELECT Id, Dealer_Code__c, Name, CDK_Dealer_Code__c ' +
            'FROM Account ' +
            'WHERE RecordType.DeveloperName = :ACCOUNT_DEALER_RT AND CDK_Dealer_Code__c != null';

        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> scope) {
        for (Account acc : scope) {
            List<SpecialOrderDetail__c> specialOrderDetails = SpecialOrderHandler.getSpecialOrderDetails(acc.CDK_Dealer_Code__c, deltaDays, acc.Id, isBulk);
            try {
                Database.UpsertResult[] results = Database.upsert(specialOrderDetails, SpecialOrderDetail__c.fields.UniqueOrderDetailId__c, true);
                recordsProcessed += scope.size();
            } catch (Exception e) {
                System.debug(LoggingLevel.ERROR, e.getStackTraceString());
                System.debug(LoggingLevel.ERROR, e.getMessage());
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        try {
            IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }  
    }

}