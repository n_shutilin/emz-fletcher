public class SpecialOrderDetailParser {
    public static final String NAMESPACE = 'http://www.dmotorworks.com/pip-extract-parts-special-order';
    public static final String CHILD_NAME = 'SpecialOrderDetail';

    public static List<SpecialOrderDetail__c> process(Dom.Document doc, String accountId) {
        List<SpecialOrderDetail__c> specialOrderDetails = new List<SpecialOrderDetail__c>();
        List<String> orderIds = new List<String>();
        String dealerCode = [SELECT Id, CDK_Dealer_Code__c FROM Account WHERE Id = :accountId].CDK_Dealer_Code__c;
        for (Dom.XmlNode child : doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == CHILD_NAME) {
                SpecialOrderDetail__c specialOrderDetail = new SpecialOrderDetail__c();
                mapFields(specialOrderDetail, child);
                specialOrderDetail.UniqueOrderId__c = dealerCode + '_' + specialOrderDetail.SpecialOrderNo__c;
                specialOrderDetail.UniqueOrderDetailId__c = dealerCode + '_' + specialOrderDetail.SpecialOrderNo__c + '_' + specialOrderDetail.HostItemID__c;
                orderIds.add(specialOrderDetail.UniqueOrderId__c);
                specialOrderDetails.add(specialOrderDetail);
            }
        }

        List<PartsSpecialOrder__c> specialOrders = [
                SELECT UniqueOrderId__c
                FROM PartsSpecialOrder__c
                WHERE UniqueOrderId__c IN :orderIds
        ];

        Map<String, String> ordersIdMap = new Map<String, String>();
        for (PartsSpecialOrder__c order : specialOrders) {
            ordersIdMap.put(order.UniqueOrderId__c, order.Id);
        }

        for (SpecialOrderDetail__c specialOrderDetail : specialOrderDetails) {
            specialOrderDetail.PartsSpecialOrder__c = ordersIdMap.get(specialOrderDetail.UniqueOrderId__c);
        }

        return specialOrderDetails;
    }

    @TestVisible
    private static void mapFields(SObject record, Dom.XmlNode child) {
        Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
            'HostItemID' => SpecialOrderDetail__c.HostItemID__c,
            'SpecialOrderNo' => SpecialOrderDetail__c.SpecialOrderNo__c,
            'DetailLineNo' => SpecialOrderDetail__c.DetailLineNo__c,
            'PartNumber' => SpecialOrderDetail__c.PartNumber__c,
            'Description' => SpecialOrderDetail__c.Description__c,
            'QuantityOrdered' => SpecialOrderDetail__c.QuantityOrdered__c,
            'Priority' => SpecialOrderDetail__c.Priority__c,
            'Bin1' => SpecialOrderDetail__c.Bin1__c,
            'Employee' => SpecialOrderDetail__c.Employee__c,
            'SalesPerson' => SpecialOrderDetail__c.SalesPerson__c,
            'PartOrderDate' => SpecialOrderDetail__c.PartOrderDate__c,
            'PartOrderType' => SpecialOrderDetail__c.PartOrderType__c,
            'PartOrderNo' => SpecialOrderDetail__c.PartOrderNo__c,
            'LastReceivedDate' => SpecialOrderDetail__c.LastReceivedDate__c,
            'QuantityFilled' => SpecialOrderDetail__c.QuantityFilled__c,
            'ClosedDate' => SpecialOrderDetail__c.ClosedDate__c,
            'CancelDate' => SpecialOrderDetail__c.CancelDate__c,
            'AccountingAccount' => SpecialOrderDetail__c.AccountingAccount__c,
            'InvoiceNumber' => SpecialOrderDetail__c.InvoiceNumber__c,
            'TechNo' => SpecialOrderDetail__c.TechNo__c,
            'RoPrepaidAmount' => SpecialOrderDetail__c.RoPrepaidAmount__c,
            'RoPrepaidCoreAmount' => SpecialOrderDetail__c.RoPrepaidCoreAmount__c,
            'ErrorLevel' => SpecialOrderDetail__c.ErrorLevel__c,
            'ErrorMessage' => SpecialOrderDetail__c.ErrorMessage__c,
            'CurrentOHQuantity' => SpecialOrderDetail__c.CurrentOHQuantity__c,
            'PreSoldQuantity' => SpecialOrderDetail__c.PreSoldQuantity__c,
            'RequestedQuantity' => SpecialOrderDetail__c.RequestedQuantity__c,
            'AvailableOHQuantity' => SpecialOrderDetail__c.AvailableOHQuantity__c,
            'QuantityFilledDetail' => SpecialOrderDetail__c.QuantityFilledDetail__c
        };

        for (String field : fieldMapping.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, NAMESPACE);

            //System.debug('~~ ' + childElement);
            if (childElement != null && String.isNotBlank(childElement.getText().trim())) {
                Schema.DisplayType fieldDataType = fieldMapping.get(field).getDescribe().getType();

                if (fieldDataType == Schema.DisplayType.STRING ||
                    fieldDataType == Schema.DisplayType.EMAIL ||
                    fieldDataType == Schema.DisplayType.LONG ||
                    fieldDataType == Schema.DisplayType.PICKLIST ||
                    fieldDataType == Schema.DisplayType.PHONE ||
                    fieldDataType == Schema.DisplayType.ADDRESS ||
                    fieldDataType == Schema.DisplayType.TEXTAREA) {
                    record.put(fieldMapping.get(field), childElement.getText().trim());
                }
                if (fieldDataType == Schema.DisplayType.DATE) {
                    record.put(fieldMapping.get(field), Date.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.INTEGER) {
                    record.put(fieldMapping.get(field), Integer.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DOUBLE ||
                    fieldDataType == Schema.DisplayType.PERCENT) {
                    record.put(fieldMapping.get(field), Double.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.DATETIME) {
                    record.put(fieldMapping.get(field), Datetime.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.CURRENCY) {
                    record.put(fieldMapping.get(field), Decimal.valueOf(childElement.getText().trim()));
                }
                if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                    record.put(fieldMapping.get(field), (childElement.getText().trim().toUpperCase() == 'Y'));
                }
            }
        }
    }
}