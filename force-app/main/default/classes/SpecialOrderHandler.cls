public with sharing class SpecialOrderHandler {
    public static final String PATH_ORDERS = '/pip-extract/parts-special-order/extract';
    public static final String PATH_ORDERS_DETAILS = '/pip-extract/parts-special-order-detail/extract';
    public static final String QUERY_ORDERS_ID = 'SORHeaderDelta';
    public static final String QUERY_ORDERS_DETAILS_ID = 'SORDetailDelta';
    public static final String QUERY_BULK_ORDERS_ID = 'SORHeaderBulk';
    public static final String QUERY_BULK_ORDERS_DETAILS_ID = 'SORDetailBulk';
    public static final String DEALERID_PARAM = 'dealerId';
    public static final String QUERYID_PARAM = 'queryId';
    public static final String DELTADATE_PARAM = 'deltaDate';
    public static final String DELTADATE_FORMAT = 'MM/dd/yyyy';

    public static List<PartsSpecialOrder__c> getPartsSpecialOrder(String dealerId, Integer deltaDays, String accountId, Boolean isBulk) {
        Map<String, String> paramsMap = new Map<String, String>();
        if (!isBulk) {
            Datetime deltaDate = Datetime.now().addDays(-deltaDays);
            String deltaDaysStr = deltaDate.format(DELTADATE_FORMAT);
            paramsMap.put(DELTADATE_PARAM, deltaDaysStr);
        }
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, isBulk ? QUERY_BULK_ORDERS_ID : QUERY_ORDERS_ID);
        HttpResponse res = DMotorWorksApi.get(PATH_ORDERS, paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<PartsSpecialOrder__c> orders = PartsSpecialOrderParser.process(res.getBodyDocument(), accountId);
        System.debug('Orders ' + orders);
        return orders;
    }
    
    public static List<SpecialOrderDetail__c> getSpecialOrderDetails(String dealerId, Integer deltaDays, Id accountId, Boolean isBulk) {
        Map<String, String> paramsMap = new Map<String, String>();
        if (!isBulk) {
            Datetime deltaDate = Datetime.now().addDays(-deltaDays);
            String deltaDaysStr = deltaDate.format(DELTADATE_FORMAT);
            paramsMap.put(DELTADATE_PARAM, deltaDaysStr);
        }
        paramsMap.put(DEALERID_PARAM, dealerId);
        paramsMap.put(QUERYID_PARAM, isBulk ? QUERY_BULK_ORDERS_DETAILS_ID : QUERY_ORDERS_DETAILS_ID);
        HttpResponse res = DMotorWorksApi.get(PATH_ORDERS_DETAILS, paramsMap);
        System.debug('response body ' + res.getBody());
        System.debug('response status  ' + res.getStatusCode());
        System.debug('response body document ' + res.getBodyDocument());
        List<SpecialOrderDetail__c> specialOrderDetails = SpecialOrderDetailParser.process(res.getBodyDocument(), accountId);
        System.debug('order details ' + specialOrderDetails);
        return specialOrderDetails;
    }
}