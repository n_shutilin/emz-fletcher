public with sharing class TaskTriggerHandler {
    public static final Set<String> activityTypes = new Set<String>{'Call', 'SMS', 'Email', 'Visit', 'Note', 'Appointment'};
    public static final Set<String> relatedSobjects = new Set<String>{'Opportunity', 'Account', 'Lead'};

    public static void publishEvent(List<Task> newTasks, Map<Id, Task> oldMap) {
        Set<String> relatedIds = new Set<String>();
        Set<String> relatedOpportunityIds = new Set<String>();
        Boolean oldMapExist = !oldMap.isEmpty();
        for (Task aTask : newTasks) {
            if ((String.isNotBlank(aTask.Whoid) || String.isNotBlank(aTask.WhatId)) && activityTypes.contains(aTask.Activity_Type__c)) {
                Id relatedSObjectId = aTask.WhatId != null ? aTask.WhatId : aTask.WhoId;
                Id oldRelatedSObjectId;

                if (oldMapExist) {
                    Task oldTask = oldMap.get(aTask.Id);
                    oldRelatedSObjectId = oldTask.WhatId != null ? oldTask.WhatId : oldTask.WhoId;
                }
                
                // if ((aTask.Status == 'Completed' || (oldMapExist ? oldMap.get(aTask.Id).Status == 'Completed' : false)) && 
                if ((aTask.IsClosed || (oldMapExist ? oldMap.get(aTask.Id).IsClosed : false)) && 
                  relatedSobjects.contains(relatedSObjectId.getSObjectType().getDescribe().getName())) {

                    relatedIds.add(relatedSObjectId);
                    if (String.isNotBlank(oldRelatedSObjectId)) {
                        relatedIds.add(oldRelatedSObjectId);
                    }

                    if (relatedSObjectId.getSObjectType().getDescribe().getName() == 'Opportunity') {
                        relatedOpportunityIds.add(relatedSObjectId);
                    }

                    if (String.isNotBlank(oldRelatedSObjectId) && oldRelatedSObjectId.getSObjectType().getDescribe().getName() == 'Opportunity') {
                        relatedOpportunityIds.add(oldRelatedSObjectId);
                    }
                }
            }
        }

        if (!relatedOpportunityIds.isEmpty()) {
            List<Opportunity> relatedOpportunities = [SELECT AccountId FROM Opportunity WHERE Id IN :relatedOpportunityIds];
            if (!relatedOpportunities.isEmpty()) {
                for (Opportunity opp : relatedOpportunities) {
                    relatedIds.add(opp.AccountId);
                }
            }
        }
        
        if (!relatedIds.isEmpty()) {
            List<String> recordIdsToRefresh = new List<String>(relatedIds);
            Activity_History_Update__e ev = new Activity_History_Update__e(Related_To__c = String.join(recordIdsToRefresh, ', '));
            Database.SaveResult result = EventBus.publish(ev);
        }
    }
}