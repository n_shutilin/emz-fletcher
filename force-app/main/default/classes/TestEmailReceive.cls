@IsTest
private class TestEmailReceive {

    @IsTest
    static void testEmailService() {
        EmailReceive emailService = new EmailReceive();
        
        String currTime = String.valueOf(Datetime.now().getTime());

        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        envelope.fromAddress = 'test@test.test';

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.subject = 'Test';
        email.plainTextBody =
            '<?xml version="1.0"?><?ADF version="1.0"?><adf>' +
            '<prospect>' +
            '<requestdate>2020-02-04 19:43:26</requestdate>' +
            '<vehicle>' +
            '<year>2020</year>' +
            '<make>Audi</make>' +
            '<model></model>' +
            '<vin>1234567890ABCDEFG</vin>' +
            '<stock>A12345</stock>' +
            '<bodystyle></bodystyle>' +
            '<odometer units="miles">5176</odometer>' +
            '<colorcombination>' +
            '<interiorcolor><![CDATA[Black]]></interiorcolor>' +
            '<exteriorcolor><![CDATA[Black]]></exteriorcolor>' +
            '</colorcombination>' +
            '<price>40000.00</price>' +
            '</vehicle>' +
            '<customer>' +
            '<contact>' +
            '<name part="first">Adam</name>' +
            '<name part="last">Adam' + currTime + '</name>' +
            '<name part="full">Yezeryan' + currTime + '</name>' +
            '<phone>8184332407</phone>' +
            '<cellphone>8184332401</cellphone>' +
            '<email>yervand' + currTime + '@emzcloud.com</email>' +
            '<address>' +
            '<city>Los Angeles</city>' +
            '<regioncode>CA, United States</regioncode>' +
            '</address>' +
            '</contact>' +
            '<comments><![CDATA[20019 Audi for Sale -- Stock No. A12345: ignore, this is a test.]]></comments>' +
            '</customer>' +
            '<vendor>' +
            '<vendorname><![CDATA[Audi Fletcher Jones]]></vendorname>' +
            '<contact primarycontact="1">' +
            '<name part="full">James Peacock</name>' +
            '<email>magleads@x-30jbxdv32npskywd9xohm5jultu1qynx55y18r9piepv3ci218.3i-qknjeaq.na112.apex.salesforce.com</email>' +
            '<phone type="voice">888-555-5656</phone>' +
            '<phone type="fax"></phone>' +
            '<address>' +
            '<street line="1">123 Test Ave</street>' +
            '<street line="2"></street>' +
            '<city>Van Nuys</city>' +
            '<regioncode>CA</regioncode>' +
            '<postalcode>91406</postalcode>' +
            '<country>United States</country>' +
            '<url>https://audifletcherjones.com/en_us/</url>' +
            '</address>' +
            '</contact>' +
            '</vendor>' +
            '<provider>' +
            '<name part="full">Hemmings Motor News</name>' +
            '<service>Collector-Car Classifieds</service>' +
            '<url>https://www.hemmings.com</url>' +
            '<email>adsales@hemmings.com</email>' +
            '<phone>802-447-9630</phone>' +
            '<contact primarycontact="1">' +
            '<name part="full">Collins Sennett</name>' +
            '<email>csennett@hemmings.com</email>' +
            '<phone type="voice" time="day">802-447-9662</phone>' +
            '<phone type="fax" time="day">802-447-9566</phone>' +
            '<address>' +
            '<street line="1">222 Main St</street>' +
            '<city>Bennington</city>' +
            '<regioncode>VT</regioncode>' +
            '<postalcode>05262</postalcode>' +
            '<country>US</country>' +
            '</address>' +
            '</contact>' +
            '</provider>' +
            '</prospect>' +
            '</adf>';
            
             Messaging.InboundEmail email2 = new Messaging.InboundEmail();
             email2.subject = 'Test';
             email2.plainTextBody =
            '<?xml version="1.0"?><?ADF version="1.0"?><adf>' +
            '<prospect>' +
            '<requestdate>2020-02-04 19:43:26</requestdate>' +
            '<vehicle>' +
            '<year>2020</year>' +
            '<make>Audi</make>' +
            '<model></model>' +
            '<vin>1234567890ABCDEFG</vin>' +
            '<stock>A12345</stock>' +
            '<bodystyle></bodystyle>' +
            '<odometer units="miles">5176</odometer>' +
            '<colorcombination>' +
            '<interiorcolor><![CDATA[Black]]></interiorcolor>' +
            '<exteriorcolor><![CDATA[Black]]></exteriorcolor>' +
            '</colorcombination>' +
            '<price>40000.00</price>' +
            '</vehicle>' +
            '<customer>' +
            '<contact>' +
            '<name part="first">Adam</name>' +
            '<name part="last">Garcia' + currTime + '</name>' +
            '<name part="full">Yezeryan' + currTime + '</name>' +
            '<phone>8184332407</phone>' +
            '<cellphone>8184332401</cellphone>' +
            '<email>yervand' + currTime + '@emzcloud.com</email>' +
            '<address>' +
            '<city>Los Angeles</city>' +
            '<regioncode>CA, United States</regioncode>' +
            '</address>' +
            '</contact>' +
            '<comments><![CDATA[20019 Audi for Sale -- Stock No. A12345: ignore, this is a test.]]></comments>' +
            '</customer>' +
            '<vendor>' +
            '<vendorname><![CDATA[Audi Fremont]]></vendorname>' +
            '<contact primarycontact="1">' +
            '<name part="full">James Peacock</name>' +
            '<email>magleads@x-30jbxdv32npskywd9xohm5jultu1qynx55y18r9piepv3ci218.3i-qknjeaq.na112.apex.salesforce.com</email>' +
            '<phone type="voice">888-555-5656</phone>' +
            '<phone type="fax"></phone>' +
            '<address>' +
            '<street line="1">123 Test Ave</street>' +
            '<street line="2"></street>' +
            '<city>Van Nuys</city>' +
            '<regioncode>CA</regioncode>' +
            '<postalcode>91406</postalcode>' +
            '<country>United States</country>' +
            '<url>https://audifletcherjones.com/en_us/</url>' +
            '</address>' +
            '</contact>' +
            '</vendor>' +
            '<provider>' +
            '<name part="full">Hemmings Motor News</name>' +
            '<service>Collector-Car Classifieds</service>' +
            '<url>https://www.hemmings.com</url>' +
            '<email>adsales@hemmings.com</email>' +
            '<phone>802-447-9630</phone>' +
            '<contact primarycontact="1">' +
            '<name part="full">Collins Sennett</name>' +
            '<email>csennett@hemmings.com</email>' +
            '<phone type="voice" time="day">802-447-9662</phone>' +
            '<phone type="fax" time="day">802-447-9566</phone>' +
            '<address>' +
            '<street line="1">222 Main St</street>' +
            '<city>Bennington</city>' +
            '<regioncode>VT</regioncode>' +
            '<postalcode>05262</postalcode>' +
            '<country>US</country>' +
            '</address>' +
            '</contact>' +
            '</provider>' +
            '</prospect>' +
            '</adf>';
            
             Messaging.InboundEmail email3 = new Messaging.InboundEmail();
             email2.subject = 'Test';
             email2.plainTextBody =
            '<?xml version="1.0"?><?ADF version="1.0"?><adf>' +
            '<prospect>' +
            '<requestdate>2020-02-04 19:43:26</requestdate>' +
            '<vehicle>' +
            '<year>2020</year>' +
            '<make>Audi</make>' +
            '<model></model>' +
            '<vin>1234567890ABCDEFG</vin>' +
            '<stock>A12345</stock>' +
            '<bodystyle></bodystyle>' +
            '<odometer units="miles">5176</odometer>' +
            '<colorcombination>' +
            '<interiorcolor><![CDATA[Black]]></interiorcolor>' +
            '<exteriorcolor><![CDATA[Black]]></exteriorcolor>' +
            '</colorcombination>' +
            '<price>40000.00</price>' +
            '</vehicle>' +
            '<customer>' +
            '<contact>' +
            '<name part="first">Adam</name>' +
            '<name part="last">Garcia' + currTime + '</name>' +
            '<name part="full">Yezeryan' + currTime + '</name>' +
            '<phone>8184332407</phone>' +
            '<cellphone>8184332401</cellphone>' +
            '<email>yervand' + currTime + '@emzcloud.com</email>' +
            '<address>' +
            '<city>Los Angeles</city>' +
            '<regioncode>CA, United States</regioncode>' +
            '</address>' +
            '</contact>' +
            '<comments><![CDATA[20019 Audi for Sale -- Stock No. A12345: ignore, this is a test.]]></comments>' +
            '</customer>' +
            '<vendor>' +
            '<vendorname><![CDATA[Audi Beverly Hills]]></vendorname>' +
            '<contact primarycontact="1">' +
            '<name part="full">James Peacock</name>' +
            '<email>magleads@x-30jbxdv32npskywd9xohm5jultu1qynx55y18r9piepv3ci218.3i-qknjeaq.na112.apex.salesforce.com</email>' +
            '<phone type="voice">888-555-5656</phone>' +
            '<phone type="fax"></phone>' +
            '<address>' +
            '<street line="1">123 Test Ave</street>' +
            '<street line="2"></street>' +
            '<city>Van Nuys</city>' +
            '<regioncode>CA</regioncode>' +
            '<postalcode>91406</postalcode>' +
            '<country>United States</country>' +
            '<url>https://audifletcherjones.com/en_us/</url>' +
            '</address>' +
            '</contact>' +
            '</vendor>' +
            '<provider>' +
            '<name part="full">Hemmings Motor News</name>' +
            '<service>Collector-Car Classifieds</service>' +
            '<url>https://www.hemmings.com</url>' +
            '<email>adsales@hemmings.com</email>' +
            '<phone>802-447-9630</phone>' +
            '<contact primarycontact="1">' +
            '<name part="full">Collins Sennett</name>' +
            '<email>csennett@hemmings.com</email>' +
            '<phone type="voice" time="day">802-447-9662</phone>' +
            '<phone type="fax" time="day">802-447-9566</phone>' +
            '<address>' +
            '<street line="1">222 Main St</street>' +
            '<city>Bennington</city>' +
            '<regioncode>VT</regioncode>' +
            '<postalcode>05262</postalcode>' +
            '<country>US</country>' +
            '</address>' +
            '</contact>' +
            '</provider>' +
            '</prospect>' +
            '</adf>';

        Messaging.InboundEmailResult result = emailService.handleInboundEmail(email, envelope);
        System.assert( result.success );
        Messaging.InboundEmailResult result2 = emailService.handleInboundEmail(email2, envelope);
        System.assert( result.success );
        Messaging.InboundEmailResult result3 = emailService.handleInboundEmail(email3, envelope);
        System.assert( result.success );
    }
}