@isTest
private class TestLeaseRetentionOpportunityBatch {
    @TestSetup
    static void setupTestData(){

        Id dealeRrecTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        List<Account> accList = new List<Account>();
        Account acc1 = new Account(
            Name = 'Test Account 1',
            RecordTypeId = dealeRrecTypeId,
            Dealer_Code__c = 'CDKTS',
            Auto_Create_Lease_Retention_Days__c = 10
        );
        accList.add(acc1);

        Account acc2 = new Account(
            Name = 'Test Account 2',
            RecordTypeId = dealeRrecTypeId,
            Dealer_Code__c = 'LVMBN',
            Auto_Create_Lease_Retention_Days__c = 20
        );
        accList.add(acc2);

        insert accList;

        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity opp1 = new Opportunity(
            Name = 'Test Opp1',
            StageName = 'New',
            CloseDate = Date.today(),
            Dealer_ID__c = 'CDKTS',
            LeaseMaturityDate__c = Date.today().addDays(-10)
        );
        oppList.add(opp1);

        Opportunity opp2 = new Opportunity(
            Name = 'Test Opp2',
            StageName = 'New',
            CloseDate = Date.today(),
            Dealer_ID__c = 'LVMBN',
            LeaseMaturityDate__c = Date.today().addDays(-20)
        );
        oppList.add(opp2);

        insert oppList;
    }

    @isTest
    static void testBatch(){
        Test.startTest();
        Database.executeBatch(new AutoCreateLeaseRetentionOpportunityBatch());
        Test.stopTest();

        Id recTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByDeveloperName().get('Lease_Retention').getRecordTypeId();
        List<Opportunity> newOpps = [SELECT Id FROM Opportunity WHERE RecordTypeId =: recTypeId];
        System.assertEquals(2, newOpps.size());
    }

    @isTest
    static void testScheduler(){
        Test.startTest();
        CreateLeaseRetentionOppScheduler.scheduleLeaseRetentionCreation();
        CronTrigger ct = [SELECT Id,CronJobDetail.Name FROM CronTrigger WHERE CronJobDetail.Name LIKE 'Auto Create Lease Retention Opportunities%'  LIMIT 1]; 
        System.assertEquals('Auto Create Lease Retention Opportunities ' +  + Date.today().format(), ct.CronJobDetail.Name);
        Test.stopTest();
    }
}