@isTest
public class TestRestrictDealerAccountDelete {

   public static testMethod void myTestMethod(){

    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
       User u = new User(Alias = 'standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', 
       LastName='test',FirstName='test', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123456@testorg.com');
       insert u;

       system.runAs(u){

Account a = new Account(Name='Test', OwnerId = u.Id, recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealer').getRecordTypeId());
insert a;

           try
           {
               Delete a;
            }
           catch(Exception er) 
           {
                a.addError('You can\'t delete Dealer Account records.');
            }
        }
    }
}