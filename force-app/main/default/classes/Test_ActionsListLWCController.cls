@isTest(seeAllData=false)
private class Test_ActionsListLWCController {
    @TestSetup
    static void testSetup(){
        Lead newLead = new Lead(
            LastName = 'Test Lead'
        );
        insert newLead;

        Action__c act = new Action__c(
            Type__c = 'Call',
            Name = 'Test Action',
            Due_Date__c = Date.today(),
            Lead__c = newLead.Id,
            Status__c = 'Open'
        );
        insert act;
    }

    @isTest
    static void testGetActionsList() {
        Lead ld = [SELECT Id FROM Lead WHERE LastName = 'Test Lead' LIMIT 1];
        String awJSON = ActionsListLWCController.getActionsList(ld.Id, '');
        System.assertNotEquals(null, awJSON);
    }
    
    /*@isTest 
    static void testCancelAction() {
        Action__c act = [SELECT Id, Status__c FROM Action__c LIMIT 1];
        ActionsListLWCController.cancelAction(act.Id);
    }*/
}