@isTest
public class Test_AdventApi {

    @isTest
    private static void test(){
        AdventApi api = new AdventApi('test', 'test');
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put('business_name','business_name');
        AdventApi.buildParams(paramsMap);
        Test.setMock(HttpCalloutMock.class, new AdventApiMock());
        Test.startTest();
        api.get('path', paramsMap);
        Test.stopTest();
    }
    
    @isTest
    private static void getNoParamsTest(){
        AdventApi api = new AdventApi('test', 'test');
        Test.setMock(HttpCalloutMock.class, new AdventApiMock());
        Test.startTest();
        api.getNoParams('path');
        Test.stopTest();
    }
    
    @isTest
    private static void generateDealDateTest(){
        AdventApi.generateDealDate(Date.today());
    }
    
    @isTest
    private static void postTest(){
        AdventApi api = new AdventApi('test', 'test');
        Test.setMock(HttpCalloutMock.class, new AdventApiMock());
        Test.startTest();
        api.post('path','body');
        Test.stopTest();
    }
    
    @isTest
    private static void putTest(){
        AdventApi api = new AdventApi('test', 'test');
        Test.setMock(HttpCalloutMock.class, new AdventApiMock());
        Map<String, String> paramsMap = new Map<String, String>();
        paramsMap.put('business_name','business_name');
        Test.startTest();
        api.put('path','body',paramsMap);
        Test.stopTest();
    }
}