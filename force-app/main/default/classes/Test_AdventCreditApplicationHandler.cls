@isTest
public class Test_AdventCreditApplicationHandler {
	@testSetup
    private static void init(){
        
    }
    
    @isTest
    private static void updateCreditApplicationTest(){
    	//given
    	AdventCreditApplicationHandler testACA = new AdventCreditApplicationHandler(new AdventApi('test', 'test'));
    	//when
    	Test.setMock(HttpCalloutMock.class, new AdventCreditApplicationHandlerMock());
    	Test.startTest();
    	AdventCreditApplicationHandler.CreditApplicationResource testCAR = new AdventCreditApplicationHandler.CreditApplicationResource();
    	testACA.updateCreditApplication(testCAR, new Map<String,String>());
    	Test.stopTest();
    	//then
    }
    
    @isTest
    private static void createCreditApplicationWrappersTest(){
        //given
        Opportunity opp = new Opportunity();
        opp.Name = 'testName';
        opp.StageName = 'New';
        opp.CloseDate = Date.today();
        insert opp;
        
        AdventCreditApplicationHandler testACA = new AdventCreditApplicationHandler(new AdventApi('test', 'test'));
        Credit_Application__c cApp = new Credit_Application__c();
        cApp.Opportunity__c = opp.Id;
        insert cApp;
        List<Credit_Application__c> cAppList = new List<Credit_Application__c>();
        cAppList.add(cApp);
        //when
        testACA.createCreditApplicationWrappers(cAppList);
        //then
    }
}