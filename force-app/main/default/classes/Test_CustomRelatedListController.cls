@isTest
public class Test_CustomRelatedListController {
    
    private static final Integer INDEX_FIRST = 0;
    private static final Integer STANDARD_LIMIT = 10;
    private static final Integer STANDARD_OFFSET = 0;
    private static final Integer EXPECTED_NUMBER_OF_PAGES = 1;
    private static final Integer EXPECTED_NUMBER_OF_APPOINTMENTS = 1;
    private static final String STANDARD_FIELD_TO_SORT_LABEL = 'Id';
    private static final String STANDARD_TYPE_TO_SORT_LABEL = 'asc';
    private static final String TEST_ACCOUNT_NAME = 'testAccount';
    private static final String TEST_LAST_NAME = 'test';
    private static final String FIRST_PEROSN_ACCOUNT_NAME = 'firstTest';
    private static final String SECOND_PEROSN_ACCOUNT_NAME = 'secondTest';
    private static final String PEROSN_ACCOUNT_RECORD_TYPE_NAME = 'Person Account';
    private static final String SOBJECT_ACCOUNT_NAME = 'Account';
    private static final String SERVICE_APPOINTMENT_RECORD_TYPE_NAME = 'VehicleServiceAppointment';
    private static final String SALES_APPOINTMENT_RECORD_TYPE_NAME = 'SalesAppointment';
    
    @testSetup static void testInit(){
        String appointmentRecordType = [SELECT Id
                                        FROM RecordType
                                        WHERE DeveloperName = :SERVICE_APPOINTMENT_RECORD_TYPE_NAME 
                                        AND sObjectType =  'ServiceAppointment'].Id;
        String salesAppointmentRecordType = [SELECT Id
                                        	FROM RecordType
                                        	WHERE DeveloperName = :SALES_APPOINTMENT_RECORD_TYPE_NAME 
                                        	AND sObjectType =  'ServiceAppointment'].Id;
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = :PEROSN_ACCOUNT_RECORD_TYPE_NAME and SObjectType = :SOBJECT_ACCOUNT_NAME];
        Account testAccount = new Account();
        testAccount.FirstName = TEST_ACCOUNT_NAME;
        testAccount.LastName = TEST_LAST_NAME;
        testAccount.RecordTypeId = personAccountRecordType.Id;
        insert testAccount;
        String accountId = testAccount.Id;
        
        ServiceAppointment testAppointment = new ServiceAppointment(
            ParentRecordId = accountId, 
            RecordTypeId = appointmentRecordType);
        insert testAppointment;
        
        Account firstPersonAccount = new Account();
        firstPersonAccount.RecordType = personAccountRecordType;
        firstPersonAccount.FirstName = FIRST_PEROSN_ACCOUNT_NAME;
        firstPersonAccount.LastName = TEST_LAST_NAME;
        insert firstPersonAccount;
        
        Account secondPersonAccount = new Account();
        secondPersonAccount.RecordType = personAccountRecordType;
        secondPersonAccount.FirstName = SECOND_PEROSN_ACCOUNT_NAME;
        secondPersonAccount.LastName = TEST_LAST_NAME;
        insert secondPersonAccount;
        
        Contact testContact = [SELECT Id FROM Contact WHERE AccountId = :firstPersonAccount.Id];
        ServiceAppointment secondTestAppointment = new ServiceAppointment(
            ParentRecordId = secondPersonAccount.Id, 
            ContactId = testContact.Id, 
            RecordTypeId = appointmentRecordType);
        insert secondTestAppointment;

        
        Opportunity firstTestOpportunity = new Opportunity();
        firstTestOpportunity.AccountId = secondPersonAccount.Id;
        firstTestOpportunity.Name = 'firstOpp';
        firstTestOpportunity.StageName = 'New';
        firstTestOpportunity.CloseDate = Date.today();
        insert firstTestOpportunity;
                
        ServiceAppointment salesTestAppointment = new ServiceAppointment(
            ParentRecordId = firstTestOpportunity.Id, 
            RecordTypeId = salesAppointmentRecordType,
        	NotCompleted__c = false);
        insert salesTestAppointment;
        
        Authorized_Contact__c testAutorizedContact = new Authorized_Contact__c();
        testAutorizedContact.Authorized_Contact__c = firstPersonAccount.Id;
        testAutorizedContact.Parent_Account__c = secondPersonAccount.Id;
        insert testAutorizedContact;
        System.debug([SELECT Id, AppointmentNumber , Status, SchedStartTime, SchedEndTime 
                      FROM ServiceAppointment 
                      WHERE NotCompleted__c = false AND RecordTypeId = :salesAppointmentRecordType 
                      AND  ParentRecordId = :secondPersonAccount.Id ORDER BY Id asc NULLS LAST LIMIT 10 OFFSET 0]);
    }
    
    @isTest
    static void getAppointmentsListWithNoContactsTest(){
        //given
        List<Account> accountIdList = [SELECT Id FROM Account WHERE FirstName = :TEST_ACCOUNT_NAME];
        String accountId = accountIdList[INDEX_FIRST].Id;
        //when
        AppointmentWrapper expected = new AppointmentWrapper();
        expected.appointment = [SELECT Id, AppointmentNumber FROM ServiceAppointment WHERE ParentRecordId = :accountId];
        expected.url = '/' + expected.appointment.Id;
        //then
        List<AppointmentWrapper> actual = CustomRelatedListController.getAppointmentsListWithLimit(STANDARD_OFFSET,STANDARD_LIMIT,accountId, STANDARD_FIELD_TO_SORT_LABEL, STANDARD_TYPE_TO_SORT_LABEL, SERVICE_APPOINTMENT_RECORD_TYPE_NAME);
        System.assertEquals(expected.appointment.Id, actual[INDEX_FIRST].appointment.Id);
    }
    
    @isTest
    static void getSalesAppointmentsListWithContactsTest(){
        //given
        List<Account> firstPerosnAccountList = [SELECT Id FROM Account WHERE FirstName = :FIRST_PEROSN_ACCOUNT_NAME];
        String firstPersonAccountId = firstPerosnAccountList[INDEX_FIRST].Id;
        List<Account> secondPerosnAccountList = [SELECT Id FROM Account WHERE FirstName = :SECOND_PEROSN_ACCOUNT_NAME];
        String secondPersonAccountId = secondPerosnAccountList[INDEX_FIRST].Id;
        String salesAppointmentRecordType = [SELECT Id
                                        	FROM RecordType
                                        	WHERE DeveloperName = :SALES_APPOINTMENT_RECORD_TYPE_NAME 
                                        	AND sObjectType =  'ServiceAppointment'].Id;
        //when
        List<AppointmentWrapper> expected = new List <AppointmentWrapper>();
        AppointmentWrapper wrapper = new AppointmentWrapper();
        wrapper.appointment = [SELECT Id, AppointmentNumber, ContactId FROM ServiceAppointment WHERE RecordTypeId = :salesAppointmentRecordType];
        wrapper.url = '/' +  wrapper.appointment.Id;
        expected.add(wrapper);
        //then
        List<AppointmentWrapper> actual = CustomRelatedListController.getAppointmentsListWithLimit(STANDARD_OFFSET,STANDARD_LIMIT,secondPersonAccountId, STANDARD_FIELD_TO_SORT_LABEL, STANDARD_TYPE_TO_SORT_LABEL, SALES_APPOINTMENT_RECORD_TYPE_NAME);
        System.debug(actual);
        System.assertEquals(expected[INDEX_FIRST].appointment.Id, actual[INDEX_FIRST].appointment.Id);
    }
    
    @isTest
    static void getServiceAppointmentsListWithContactsTest(){
        //given
        List<Account> firstPerosnAccountList = [SELECT Id FROM Account WHERE FirstName = :FIRST_PEROSN_ACCOUNT_NAME];
        String firstPersonAccountId = firstPerosnAccountList[INDEX_FIRST].Id;
        List<Account> secondPerosnAccountList = [SELECT Id FROM Account WHERE FirstName = :SECOND_PEROSN_ACCOUNT_NAME];
        String secondPersonAccountId = secondPerosnAccountList[INDEX_FIRST].Id;
        String appointmentRecordType = [SELECT Id
                                        FROM RecordType
                                        WHERE DeveloperName = :SERVICE_APPOINTMENT_RECORD_TYPE_NAME 
                                        AND sObjectType =  'ServiceAppointment'].Id;
        //when
        List<AppointmentWrapper> expected = new List <AppointmentWrapper>();
        AppointmentWrapper wrapper = new AppointmentWrapper();
        wrapper.appointment = [SELECT Id, AppointmentNumber, ContactId FROM ServiceAppointment WHERE ParentRecordId = :secondPersonAccountId AND RecordTypeId = :appointmentRecordType];
        wrapper.url = '/' +  wrapper.appointment.Id;
        expected.add(wrapper);
        //then
        List<AppointmentWrapper> actual = CustomRelatedListController.getAppointmentsListWithLimit(STANDARD_OFFSET,STANDARD_LIMIT,secondPersonAccountId, STANDARD_FIELD_TO_SORT_LABEL, STANDARD_TYPE_TO_SORT_LABEL, SERVICE_APPOINTMENT_RECORD_TYPE_NAME);
        System.assertEquals(expected[INDEX_FIRST].appointment.Id, actual[INDEX_FIRST].appointment.Id);
    }
    
    @isTest
    static void getNumberOfAppointmentsWithNoContactsTest(){
        //given
        List<Account> accountIdList = [SELECT Id FROM Account WHERE FirstName = :TEST_ACCOUNT_NAME];
        String accountId = accountIdList[INDEX_FIRST].Id;
        //when
        //then
        Integer actual = CustomRelatedListController.getNumberOfAppointments(accountId, SERVICE_APPOINTMENT_RECORD_TYPE_NAME);
        System.assert(EXPECTED_NUMBER_OF_APPOINTMENTS == actual);
    }
    
    @isTest
    static void getNumberOfAppointmentsWithContactsTest(){
        //given
       List<Account> secondPerosnAccountList = [SELECT Id FROM Account WHERE FirstName = :SECOND_PEROSN_ACCOUNT_NAME];
        String secondPersonAccountId = secondPerosnAccountList[INDEX_FIRST].Id;
        //when
        //then
        Integer actual = CustomRelatedListController.getNumberOfAppointments(secondPersonAccountId, SERVICE_APPOINTMENT_RECORD_TYPE_NAME);
        System.assert(EXPECTED_NUMBER_OF_APPOINTMENTS == actual);
    }
    
    @isTest
    static void calculateNumberOfPagesTest(){
        //given
        List<Account> accountIdList = [SELECT Id FROM Account WHERE FirstName = :TEST_ACCOUNT_NAME];
        String accountId = accountIdList[INDEX_FIRST].Id;
        //when
        //then
        Integer actual = CustomRelatedListController.calculateNumberOfPages(STANDARD_LIMIT,accountId, SERVICE_APPOINTMENT_RECORD_TYPE_NAME);
         System.assert(EXPECTED_NUMBER_OF_PAGES == actual);
    }
 }