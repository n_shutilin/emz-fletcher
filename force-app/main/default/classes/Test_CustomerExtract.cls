@isTest
private class Test_CustomerExtract {
    private static final String SUCCESS_RESPONSE_BODY = '<Customers xmlns="http://www.dmotorworks.com/pip-extract-customer">' +
            '<Customer>' +
                '<Comment />' +
                '<CommentDate />' +
                '<Email2 />' +
                '<Email3 />' +
                '<LastUpdated>2018-10-24</LastUpdated>' +
                '<Name2First />' +
                '<Name2Last />' +
                '<Name2Middle />' +
                '<Name2Suffix />' +
                '<Name2Title />' +
                '<NameSuffix />' +
                '<NameTitle />' +
                '<BlockEMail>N</BlockEMail>' +
                '<BlockMail>N</BlockMail>' +
                '<BlockPhone>N</BlockPhone>' +
                '<HostItemID>4*MA5009</HostItemID>' +
                '<Address>1501 NW 49TH STREET STE 100</Address>' +
                '<AddressSecondLine />' +
                '<BusinessPhone>8084989775</BusinessPhone>' +
                '<BusinessPhoneExt />' +
                '<Cellular>9549523934</Cellular>' +
                '<City>FORT LAUDERDALE</City>' +
                '<Country>USA</Country>' +
                '<County />' +
                '<CustNo>MA5009</CustNo>' +
                '<Email>KYLE.ISHIDA@SIXT.COM</Email>' +
                '<EmailDesc>WORK</EmailDesc>' +
                '<EmailDesc2 />' +
                '<EmailDesc3 />' +
                '<ErrorLevel>0</ErrorLevel>' +
                '<ErrorMessage />' +
                '<FirstName />' +
                '<HomeFax />' +
                '<Telephone>9549523934</Telephone>' +
                '<LastName />' +
                '<MiddleName />' +
                '<Name1>SIXT RENT A CAR LLC</Name1>' +
                '<Name2Company />' +
                '<NameCompany>SIXT RENT A CAR LLC</NameCompany>' +
                '<Pager />' +
                '<PreferredContactMethod />' +
                '<SaleType>CHG</SaleType>' +
                '<SecondaryHomePhone />' +
                '<State>FL</State>' +
                '<Title1 />' +
                '<ZipOrPostalCode>33309</ZipOrPostalCode>' +
                '<HomePhone />' +
                '<PreferredLanguage />' +
                '<Name2 />' +
                '<DateAdded>2018-10-24</DateAdded>' +
                '<ServiceCustomer />' +
            '</Customer>' +
            '<ErrorCode>0</ErrorCode>' +
            '<ErrorMessage/>' +
        '</Customers>';

    private static final String BLANK_RESPONSE_BODY = '<Customers xmlns="http://www.dmotorworks.com/pip-extract-customer"></Customers>';

    @TestSetup
    private static void init() {
        CDK_Integration__c settings = new CDK_Integration__c(
            Password__c = '111111111',
            Username__c = 'testUser',
            Customer_Extract_URL__c = 'testURL',
            Authorization__c = 'test'
        );
        insert settings;
        
        Id recTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        Account dealerAcc = new Account(
            Name = 'CDKTS Test Account',
            RecordTypeId = recTypeId,
            CDK_Dealer_Code__c = '3PA17867',
            Dealer_Code__c = 'CDKTS'
        );
        insert dealerAcc;
    }
    
    @isTest
    private static void testScheduledJob() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(SUCCESS_RESPONSE_BODY));
        String jobId = System.schedule('Account Updates', '0 0 0 28 2 ? 2022', new CustomerExtractSchedulable());
        Test.stopTest();
    }

    @isTest
    private static void testScheduledJobBlank() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(BLANK_RESPONSE_BODY));
        String jobId = System.schedule('Account Updates', '0 0 0 28 2 ? 2022', new CustomerExtractSchedulable());
        Test.stopTest();
    }

    @isTest
    private static void testBatch() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(SUCCESS_RESPONSE_BODY));
        CustomerExtractBatchable batch = new CustomerExtractBatchable('03-03-2021');
        batch.dealerId = '3PA0005213';
        Database.executeBatch(batch, 1);
        Test.stopTest();
    }
}