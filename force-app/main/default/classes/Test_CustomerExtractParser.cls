@isTest
public class Test_CustomerExtractParser {
    
    @testSetup
    private static void init(){

    }

    @isTest
    private static void parseCustomersTest(){
        Account testAccount = new Account(Name = 'testName');
        testAccount.Dealer_Code__c = 'test';
		insert testAccount;
        Dom.Document dom = new Dom.Document();
        String xmlTemplate = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
								+'<lead-data >' +
									+'<c:Customer xmlns:c="http://www.dmotorworks.com/pip-extract-customer">' +
										+'<c:Address>Veera</c:Address>' +
										+'<c:City>Vinnu</c:City>' +
										+'<c:BlockPhone>8008997654</c:BlockPhone>' +
            							+'<c:BlockEMail>veerav@gmail.com</c:BlockEMail>' +									
									+'</c:Customer>' +
           							+'<prospect>' +
            							+'<vehicle>test</vehicle>' +
            							+'<Customer>' +
           					   				+'<contact>test</contact>' +
            							+'</Customer>' +
            							+'<vendor>test</vendor>' +
            							+'<provider>test</provider>' +
            			  			+'</prospect>' +
								+'</lead-data>';
        dom.load(xmlTemplate);
        CustomerExtractParser.parseCustomers(dom, null);
    }
}