@isTest
public class Test_CustomerInsertUpdate {
    
    private static final String BODY = 
        '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' +
                   +'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:save="http://save.customer.pip.adp3pa.dmotorworks.com/" xmlns:pip="http://www.dmotorworks.com/pip-customer">' +
							+'<soapenv:Body>' +
                    			+'<pip:getCustomerNumberResponse>' +
            							 +'<arg0>' +
                							+'<password>test</password>' +
                							+'<username>test</username>' +
            							+'</arg0>' +
            							+'<arg1>' + 
                							+'<dealerId>testId</dealerId>' +
            							+'</arg1>' +
            							+'<arg2>' +
                							+'<userId>111</userId>' +
            							+'</arg2>' +
                                		+'<W2kCustomerVehicleID>' +
                							+'<CustNo xmlns:t="http://www.dmotorworks.com/pip-extract-customer-vehicle-search">test</CustNo>' +
            							+'</W2kCustomerVehicleID>' +
                   						+'<return>' + 
                    						+'<code>success</code>' +
                  					  		+'<customerNumber>200</customerNumber>' +
                    					+'</return>' +
                   				 +'</pip:getCustomerNumberResponse>' +
                  			     +'<pip:insertResponse>' +
            							 +'<arg0>' +
                							+'<password>test</password>' +
                							+'<username>test</username>' +
            							+'</arg0>' +
            							+'<arg1>' +
                							+'<dealerId>testId</dealerId>' +
            							+'</arg1>' +
            							+'<arg2>' +
                							+'<userId>111</userId>' +
            							+'</arg2>' +
                   						+'<return>' + 
                    						+'<code>success</code>' +
                  					  		+'<customerParty>'+ 
                                      			+'<checksum>200</checksum>' +
                  					  			+'<id>'+ 
                                      		      +'<value>200</value>' +
                    							+'</id>'+
                    						+'</customerParty>' +
                    					+'</return>' +
                   				 +'</pip:insertResponse>' +
							+'</soapenv:Body>' +
                   +'</soapenv:Envelope>';
	
    @testSetup
    private static void init(){
		CDK_Integration__c settings = new CDK_Integration__c();
		settings.Password__c = 'test';
        settings.Username__c = 'test';
		insert settings;        
    }
    
    @isTest
    private static void getCustomerNumberTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(BODY));
        String xml = CustomerInsertUpdate.getCustomerNumber('testId');
        Test.stopTest();
    }
    
    @isTest
    private static void insertUpdateCustomerTest(){
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        Account testAccount = new Account();
        testAccount.LastName = 'testName';
        testAccount.PersonEmail = 'test@test.com';
        testAccount.Customer_Primary_Phone__c = 'Mobile';    
        testAccount.RecordTypeId = personAccountRecordType.Id;
        insert testAccount;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(BODY));
        CustomerInsertUpdate.CustomerInfo info = CustomerInsertUpdate.insertUpdateCustomer('testId',testAccount.Id);
        Test.stopTest();
    }
    
    @isTest
    private static void searchCustomerVehicleTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(BODY));
        String result = CustomerInsertUpdate.searchCustomerVehicle('testId','qparamType', 'qparamValue');
        Test.stopTest();
    }
}