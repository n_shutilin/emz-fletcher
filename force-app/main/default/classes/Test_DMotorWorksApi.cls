@isTest
public class Test_DMotorWorksApi {
	
    @isTest
    private static void getTest(){
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock());
        Map<String, String> params = new Map<String, String>();
        params.put('test','test');
        HttpResponse res = DMotorWorksApi.get('http://test.com',params);
    }
    
    @isTest
    private static void postTest(){
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock());
        String body = 'test';
        HttpResponse res = DMotorWorksApi.post('http://test.com',body);
    }
}