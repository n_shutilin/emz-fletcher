@isTest
private class Test_DataImportWebService {
    private static final String REPAIR_ORDER_RESPONSE = '<ServiceSalesClosed xmlns="http://www.dmotorworks.com/pip-extract-service-sales-closed">' +
            '<ServiceSalesClosed>' +
                '<CityStateZip>CORONA DEL MAR, CA 92625</CityStateZip>' +
                '<LaborSaleWarranty>0.00</LaborSaleWarranty>' +
                '<PriorityValue>2167</PriorityValue>' +
                '<PromisedTime>17:00:00</PromisedTime>' +
                '<ROComment4 /><ROComment6 />' +
                '<ROStatusCode>C98</ROStatusCode>' +
                '<ROStatusCodeDesc>CLOSED</ROStatusCodeDesc>' +
                '<CustNo>1102919</CustNo>' +
                '<ServiceAdvisor>2082</ServiceAdvisor>' +
                '<PartsSaleWarranty>0.00</PartsSaleWarranty>' +
                '<Name2 />' +
                '<OpenDate>2020-09-24</OpenDate>' +
                '<ROComment1 /><ROComment2 /><ROComment7 />' +
                '<VIN>W1N4N4HB8MJ175900</VIN>' +
                '<LaborSaleInternal>276.12</LaborSaleInternal>' +
                '<ErrorMessage />' +
                '<Mileage>3</Mileage>' +
                '<PartsSaleCustomerPay>0.00</PartsSaleCustomerPay>' +
                '<Model>GLA250W4</Model>' +
                '<HostItemID>MJ175900*970474</HostItemID>' +
                '<LaborSaleCustomerPay>0.00</LaborSaleCustomerPay>' +
                '<ErrorLevel>0</ErrorLevel>' +
                '<Name1>LORITZ,MELISSA</Name1>' +
                '<OpenedTime>08:51:31</OpenedTime>' +
                '<ROComment3 /><ROComment5 />' +
                '<RONumber>970474</RONumber>' +
                '<ContactEmailAddress />' +
                '<CloseDate>2020-09-24</CloseDate>' +
                '<VehID>MJ175900</VehID>' +
                '<Address>242 HELIOTROPE</Address>' +
                '<ROComment9 /><ContactPhoneNumber />' +
                '<Make>MB</Make>' +
                '<PromisedDate>2020-09-24</PromisedDate>' +
                '<ROComment8 />' +
                '<Year>2021</Year>' +
                '<MileageOut>5</MileageOut>' +
                '<PartsSaleInternal>0.00</PartsSaleInternal>' +
            '</ServiceSalesClosed>' +
            '<ErrorCode>0</ErrorCode><ErrorMessage/>' +
        '</ServiceSalesClosed>';
    
    private static final String REPAIR_ORDER_DETAILS_RESPONSE = '<ServiceSalesClosed xmlns="http://www.dmotorworks.com/pip-extract-service-sales-closed">' +
                '<ServiceSalesDetailsClosed>' +
                    '<ServiceRequest>"B" SERVICE-FLEXIBLE SERVICE</ServiceRequest>' +
                    '<LineCode>A</LineCode>' +
                    '<LopSeqNo>1</LopSeqNo>' +
                    '<LaborSale>374.44</LaborSale>' +
                    '<LaborType>CMM</LaborType>' +
                    '<PartsSale>170.40</PartsSale>' +
                    '<ActualHours>0.82</ActualHours>' +
                    '<ErrorLevel>0</ErrorLevel>' +
                    '<ErrorMessage />' +
                    '<OpCode>1002</OpCode>' +
                    '<OpCodeDescription>PERFORM FLETCHER JONES MOTORCARS STANDARD "B" SERVICE</OpCodeDescription>' +
                    '<RONumber>968668</RONumber>' +
                    '<HostItemID>GU165469*968668*1*0</HostItemID>' +
                '</ServiceSalesDetailsClosed>' +
                '<ErrorCode>0</ErrorCode>' +
                '<ErrorMessage />' +
            '</ServiceSalesClosed>';
    
    @TestSetup
    private static void init() {
        Id recTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        Account dealerAcc = new Account(
            Name = 'AUDFJ Test Account',
            RecordTypeId = recTypeId,
            CDK_Dealer_Code__c = '3PA17867',
            Dealer_Code__c = 'AUDFJ'
        );
        insert dealerAcc;
    }
    
    @isTest
    private static void testRepairOrderImport() {
        Test.startTest();
        RestRequest request = new RestRequest();
        request.requestUri = URL.getOrgDomainUrl() + '/services/apexrest/DataImport';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'text/html');
        request.params.put('dealerId', '3PA17867');
        request.params.put('objectName', 'ServiceSalesClosed');
        request.requestBody = Blob.valueOf(REPAIR_ORDER_RESPONSE);
        RestContext.request = request;
        Integer result = DataImportWebService.upsertData();
        Test.stopTest();
        WorkOrder[] woList = [SELECT Id From WorkOrder];
        System.assertNotEquals(0, woList.size());
    }
    
    @isTest
    private static void testRepairOrderDetailsImport() {
        Test.startTest();
        String accountId = [SELECT Id, Dealer_Code__c FROM Account WHERE CDK_Dealer_Code__c = '3PA17867'].Id;
        
        WorkOrder parentWO = new WorkOrder(
            UnuqueRONumber__c = accountId + '968668'
        );
        insert parentWO;
        
        RestRequest request = new RestRequest();
        request.requestUri = URL.getOrgDomainUrl() + '/services/apexrest/DataImport';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'text/html');
        request.params.put('dealerId', '3PA17867');
        request.params.put('objectName', 'ServiceSalesDetailsClosed');
        request.requestBody = Blob.valueOf(REPAIR_ORDER_DETAILS_RESPONSE);
        RestContext.request = request;
        Integer result = DataImportWebService.upsertData();
        Test.stopTest();
        WorkOrderLineItem[] wolList = [SELECT Id From WorkOrderLineItem];
        System.assertNotEquals(0, wolList.size());
    }

    @isTest
    private static void testHistoryRepairOrderImport() {
        Test.startTest();
        RestRequest request = new RestRequest();
        request.requestUri = URL.getOrgDomainUrl() + '/services/apexrest/DataImport';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'text/html');
        request.params.put('dealerId', '3PA17867');
        request.params.put('objectName', 'ServiceSalesClosedHistory');
        request.requestBody = Blob.valueOf(REPAIR_ORDER_RESPONSE);
        RestContext.request = request;
        Integer result = DataImportWebService.upsertData();
        Test.stopTest();
    }
    
    @isTest
    private static void testHistoryRepairOrderDetailsImport() {
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = URL.getOrgDomainUrl() + '/services/apexrest/DataImport';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'text/html');
        request.params.put('dealerId', '3PA17867');
        request.params.put('objectName', 'ServiceSalesClosedDetailsHistory');
        request.requestBody = Blob.valueOf(REPAIR_ORDER_DETAILS_RESPONSE);
        RestContext.request = request;
        Integer result = DataImportWebService.upsertData();
        Test.stopTest();
    }

}