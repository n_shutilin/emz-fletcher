@isTest
public class Test_DealSyncBatch {
    @testSetup
    private static void init(){
        Id recTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        Account acc = new Account(
        	Name = 'Test Dealer Account',
            Advent_Store_Number__c = '28c8bc2a-1869-4def-828a-88b542da2b8b',
            RecordTypeId = recTypeId
        );
        insert acc;
    }

    @isTest
    private static void testDealSyncBatch() {
        Test.setMock(HttpCalloutMock.class, new AdventMockHttpResponse());
        Test.startTest();
        	DealSyncBatch dsb = new DealSyncBatch();
        	Id batchId = Database.executeBatch(dsb);
        Test.stopTest();
    }
    
    @isTest
    private static void testInventorySyncBatch() {
        Test.setMock(HttpCalloutMock.class, new AdventMockHttpResponse());
        Test.startTest();
        	InventorySyncBatch dsb = new InventorySyncBatch();
        	Id batchId = Database.executeBatch(dsb);
        Test.stopTest();
    }
}