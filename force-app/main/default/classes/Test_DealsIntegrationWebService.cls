@isTest
private class Test_DealsIntegrationWebService {
	private static final String DEAL_INTEGRATION_REQUEST = '[{"SaleRSEntryDate__c": "2021-10-07",' +
        + '"SaleUniqueDealNo__c": "FRMBN-206766",' +
        + '"StoreID__c": "FRMBN",' +
        + '"SaleDealNo__c": "206766",' +
        + '"SaleOpenDate__c": "2020-01-11",' +
        + '"PurchaseOrderDate__c": "2020-01-11",' +
        + '"SaleSaleStatusName__c": "RSed",' +
        + '"SaleStockNo__c": "   R10253",' +
        + '"SaleInvtTypeNU__c": "Used",' +
        + '"SaleSaleTypeName__c": "Retail",' +
        + '"SaleSaleTypeCode__c": "1",' +
        + '"SaleAddlCustNo__c": "000065185",' +
        + '"SaleCustIndEmpName__c": "SATELLITE HEALTHCARE",' +
        + '"SaleCustIndEmpPosition__c": "MASTER CCHD",' +
        + '"SaleCustIndLastName_SaleCustIndFirs__c": "BRILLANTES, SANDY",' +
        + '"SaleInvtNewInvoiceAmt__c": "",' +
        + '"SaleInvtNewHoldbackAmt__c": "",' +
        + '"SaleInvtNewOtherDeduct__c": "0.00",' +
        + '"SaleInvtInitialCost__c": "34613.00",' +
        + '"SaleInvtTtlAddsCost__c": "1484.00",' +
        + '"SaleInvtNetCost__c": "36097.00",' +
        + '"SaleInvtGLBalance__c": "34902.00",' +
        + '"Real_Net_Cost__c": "35962.00",' +
        + '"SaleInvtMSRP__c": "36188.00",' +
        + '"SaleSalePriceAmt__c": "34188.00",' +
        + '"Total_Deal_Gross__c": "10381.81",' +
        + '"Lease_Driveoff_Adj__c": "-1999.00",' +
        + '"Trade_UA__c": "0.00",' +
        + '"SaleTrueVehicleProfitAmt__c": "5381.00",' +
        + '"SaleDealerPack__c": "300.00",' +
        + '"SaleTotalCommAmt__c": "1299.16",' +
        + '"SaleCommVehicleProfitAmt__c": "5081.00",' +
        + '"SaleReserveProfitAmt__c": "1127.81",' +
        + '"SaleVESCProfitAmt__c": "2140.00",' +
        + '"SaleFinAddVehicleProfitAmt__c": "1733.00",' +
        + '"SaleLahAmt__c": "0.00",' +
        + '"Back_Gross__c": "5000.81",' +
        + '"SaleSlsmn1Plan__c": "SALESMAN",' +
        + '"SaleSlsmn1CommAmt__c": "1299.16",' +
        + '"SaleSlsmn2Plan__c": "",' +
        + '"SaleSlsmn2CommAmt__c": "0.00",' +
        + '"SaleSlsmn3Plan__c": "",' +
        + '"SaleSlsmn3CommAmt__c": "0.00",' +
        + '"SaleInvtYear__c": "2020",' +
        + '"SaleInvtMakeName__c": "MERCEDES-BENZ",' +
        + '"SaleInvtModelName__c": "GLA250W",' +
        + '"SaleInvtVIN__c": "WDCTG4EB3LU023502",' +
        + '"SaleInvtReceiveDate__c": "2020-11-06",' +
        + '"Age_In_Invt__c": "143",' +
        + '"SaleinvtColor1Name__c": "N. BLACK 6K MI. DP1",' +
        + '"SaleinvtColor2Name__c": "BLACK",' +
        + '"SaleinvtOdometer__c": "6845",' +
        + '"SaleTrd1VIN__c": "2HKRM4H52EH656326",' +
        + '"SaleTrd1Year__c": "2014",' +
        + '"SaleTrd1MakeName__c": "HONDA",' +
        + '"SaleTrd1ModelName__c": "CRV",' +
        + '"SaleTrd1Color1Name__c": "GREY 78K MI.",' +
        + '"SaleTrd1Color1Code__c": "",' +
        + '"SaleTrd1Color2Name__c": "GREY",' +
        + '"SaleTrd1Color2Code__c": "",' +
        + '"SaleTrd1Odometer__c": "78703",' +
        + '"SaleTrd1GrossAmt__c": "9150.00",' +
        + '"SaleTrd1UsedGrossACV__c": "11000.00",' +
        + '"SaleTrd1PayoffAmt__c": "0.00",' +
        + '"SaleTrd2VIN__c": "",' +
        + '"SaleTrd2Year__c": "",' +
        + '"SaleTrd2MakeName__c": "",' +
        + '"SaleTrd2ModelName__c": "",' +
        + '"SaleTrd2Color1Name__c": "",' +
        + '"SaleTrd2Color1Code__c": "",' +
        + '"SaleTrd2Color2Name__c": "",' +
        + '"SaleTrd2Color2Code__c": "",' +
        + '"SaleTrd2Odometer__c": "",' +
        + '"SaleTrd2GrossAmt__c": "0.00",' +
        + '"SaleTrd2UsedGrossACV__c": "0.00",' +
        + '"SaleTrd2PayoffAmt__c": "0.00",' +
        + '"SaleCustVisitDate__c": "2021-10-07",' +
        + '"SaleCustIndFirstName__c": "SANDY",' +
        + '"SaleCustIndMiddleName__c": "",' +
        + '"SaleCustIndLastName__c": "BRILLANTES",' +
        + '"SaleCustIndStreetName__c": "32239 GLENBROOK ST",' +
        + '"SaleCustCity__c": "UNION CITY",' +
        + '"SaleCustState__c": "CA",' +
        + '"SaleCustZipCode__c": "94587",' +
        + '"SaleCustCountyName__c": "ALAMEDA",' +
        + '"PhoneBlock__c": "",' +
        + '"SaleCustHomePhone__c": "7737429310",' +
        + '"SaleCustWorkPhone__c": "7737429310",' +
        + '"SaleCustCellPhone__c": "",' +
        + '"SaleCustEmailAddress__c": "sandybrillantes@yahoo.com",' +
        + '"SaleCustIndBirthdate__c": "1969-06-11",' +
        + '"SaleCoapIndFirstName_SaleCoapIndLastN__c": "",' +
        + '"SaleCoapIndFirstName__c": "",' +
        + '"SaleCoapIndMiddleName__c": "",' +
        + '"SaleCoapIndLastName__c": "",' +
        + '"SaleCoapIndStreetName__c": "",' +
        + '"SaleCoapCity__c": "",' +
        + '"SaleCoapState__c": "",' +
        + '"SaleCoapZipCode__c": "",' +
        + '"SaleCoapCountyName__c": "",' +
        + '"SaleCoapHomePhone__c": "",' +
        + '"SaleCoapWorkPhone__c": "",' +
        + '"SaleCustBusContactName__c": "",' +
        + '"SaleCustBusName__c": "SATELLITE HEALTHCARE",' +
        + '"SaleSlsmn1EmpDetailEmpNo__c": "220818",' +
        + '"SaleSlsmn1EmpDetailFirstName__c": "MUZAMIL",' +
        + '"SaleSlsmn1EmpDetailLastName__c": "KHAN",' +
        + '"SaleSlsmn2EmpDetailEmpNo__c": "0",' +
        + '"SaleSlsmn2EmpDetailFirstName__c": "",' +
        + '"SaleSlsmn2EmpDetailLastName__c": "",' +
        + '"SaleSlsmn3EmpDetailEmpNo__c": "     0",' +
        + '"SaleSlsmn3EmpDetailFirstName__c": "",' +
        + '"SaleSlsmn3EmpDetailLastName__c": "",' +
        + '"SaleCloserEmpDetailEmpNo__c": "0",' +
        + '"SaleCloserEmpDetailFirstName__c": "No Closer",' +
        + '"SaleCloserEmpDetailLastName__c": "No Closer",' +
        + '"SaleFImgrEmpDetailEmpNo__c": "220526",' +
        + '"SaleFImgrEmpDetailFirstName__c": "AJMAL",' +
        + '"SaleFImgrEmpDetailLastName__c": "SARDAR",' +
        + '"SaleFinCmpyDetailCmpyCode__c": "55",' +
        + '"SaleFinCmpyDetailAbbrev__c": "TDA",' +
        + '"SaleFinCmpyDetailLegalName__c": "TD AUTO FINANCE",' +
        + '"SaleFinCmpyDetailStreet__c": "PO BX 675",' +
        + '"SaleFinCmpyDetailCity__c": "WILMINGTON",' +
        + '"SaleFinCmpyDetailState__c": "OH",' +
        + '"SaleFinCmpyDetailZip__c": "45177",' +
        + '"SaleTaxAccAmt__c": "0.00",' +
        + '"SaleTaxDocFeeAmtSaleLeaseCappedDlrDoc__c": "85.00",' +
        + '"SaleSlsTaxAmt__c": "3341.62",' +
        + '"SaleStockTotalAmt__c": "37614.62",' +
        + '"SaleNonTxAccAmt__c": "4580.00",' +
        + '"SaleStateDMVAmt__c": "475.00",' +
        + '"SaleAddlDMVFee1__c": "505.00",' +
        + '"SaleTaxSmogFeeAmt__c": "0.00",' +
        + '"SaleDepositAmt__c": "0.00",' +
        + '"SalePick1Amt__c": "0.00",' +
        + '"SaleRebateAmt__c": "0.00",' +
        + '"SaleNetTrdCappedAmt__c": "9150.00",' +
        + '"SaleFinancedAmt__c": "37447.62",' +
        + '"SaleFinAPR__c": "5.990",' +
        + '"SaleFinAddonRateSaleLeasCustAPR__c": "3.22000",' +
        + '"SaleFinBankAddonRateSaleLeasBankAPR__c": "2.60000",' +
        + '"SaleFinDlrParticipationSaleLeaseDlrPa__c": "80.00",' +
        + '"SaleFinTerm__c": "72",' +
        + '"SaleFinTtlReserveAmt__c": "7335.66",' +
        + '"SaleFinMthlyPymt__c": "621.99",' +
        + '"SaleFinBalloonPymt__c": "0.00",' +
        + '"SaleFinMethod__c": "1",' +
        + '"SaleFinMethodName__c": "Normal",' +
        + '"SaleLeaseResidualValue__c": "",' +
        + '"SaleLeasDOSecDeposit__c": "",' +
        + '"SaleLeasAllowedMiles__c": "0",' +
        + '"SaleLeasExcessMileageRate__c": "0.000000",' +
        + '"SaleLeasDOTotal__c": "",' +
        + '"Last_Pymt__c": "2027-04-11",' +
        + '"SaleVescAmt_1__c": "3890.00",' +
        + '"SaleVescAmt__c": "3890.00",' +
        + '"SaleAddlVESCCost__c": "1750.00",' +
        + '"SaleAddlVESCTermMiles__c": "0",' +
        + '"SC_Miles_No_Limit__c": "N",' +
        + '"SaleAddlVESCTermMths__c": "24",' +
        + '"SC_Months_No_Limit__c": "N",' +
        + '"SaleAddlVESCEffDate__c": "2021-10-07",' +
        + '"SaleAddlVESCCmpyCode__c": "3",' +
        + '"SaleAddlVESCCmpyAbbrev__c": "MBZ CERT",' +
        + '"SaleAddlVESCPolicyNo__c": "",' +
        + '"SaleAddlVESCDeductible__c": "0",' +
        + '"SaleAddlVEHCCmpyCode__c": "0",' +
        + '"SaleAddlVEHCCmpyName__c": "",' +
        + '"SaleAddlVEHCContactName__c": "",' +
        + '"SaleAddlVEHCContactStreet__c": "",' +
        + '"SaleAddlVEHContactCity__c": "",' +
        + '"SaleAddlVEHCContactState__c": "",' +
        + '"SaleAddlVEHCContactZipCode__c": "",' +
        + '"SaleAddlVEHCContactPhone__c": "",' +
        + '"SaleAddlVEHCPolicyNo__c": "",' +
        + '"SaleAddlVEHCInsCode__c": "",' +
        + '"SaleAddlVEHCVerifyEmpNo__c": "220526",' +
        + '"SaleAddlVEHCCompDeduct__c": "0",' +
        + '"SaleInvtLicenseNo__c": "",' +
        + '"SaleRSNo__c": "53990662",' +
        + '"SaleAcctControlNoAcctControlNo__c": "116805",' +
        + '"SaleCtrlAssignTstamp__c": "05:27 PM 07/22/2020",' +
        + '"SaleToBusOfficeTstamp__c": "",' +
        + '"SaleAcctMth__c": "07/20",' +
        + '"SaleSaleAccount__c": "43401",' +
        + '"Sale_Account_Desc__c": "",' +
        + '"SaleFfupDepositReceiptNo__c": "",' +
        + '"SaleAcctVehProfitAmt__c": "0.00",' +
        + '"SaleAcctResProfitAmt__c": "0.00",' +
        + '"SaleAcctVescProfitAmt__c": "0.00",' +
        + '"SaleAcctAccProfitAmt__c": "0.00",' +
        + '"SaleAcctCommAmt__c": "0.00",' +
        + '"BuyerFico__c": "671",' +
        + '"SaleSlsmgrEmpDetailempNo__c": "220676",' +
        + '"SaleSlsMgrEmpDetailFirstName__c": "MOHAMMED",' +
        + '"SaleSlsmgrEmpDetailLastName__c": "KARIMI",' +
        + '"SaleCustTypeName__c": "Individual",' +
        + '"SaleCustBusTaxID__c": "",' +
        + '"SaleCustBusTypeName__c": "",' +
        + '"SaleCustIndIncome__c": "0",' +
        + '"SaleCustBusDMVLicenseNo__c": "",' +
        + '"SaleDepartment__c": "C",' +
        + '"Department_Code_Description__c": "PRE-OWNED SALES",' +
        + '"SaleInvtCertifiedPreOwnedYN__c": "",' +
        + '"SaleLAHProfitAmt__c": "0.00",' +
        + '"SaleNetProfitAmt__c": "9082.65",' +
        + '"SaleSlsmn1Percent__c": "15.00",' +
        + '"SaleSlsmn1SpiffAmt__c": "0.00",' +
        + '"SaleSlsmn2Percent__c": "0.00",' +
        + '"SaleSlsmn2SpiffAmt__c": "0.00",' +
        + '"SaleTrd1TrimLevel__c": "C",' +
        + '"SaleTrd2TrimLevel__c": "",' +
        + '"SaleInvtTrimLevel__c": "L",' +
        + '"SaleInvtWheelDrive__c": "2",' +
        + '"SaleInvtFuel__c": "GAS"}]';
    
    private static final String DEAL_INTEGRATION_INVALID_REQUEST = '[{"SaleRSEntryDate__c": "2021-10-07",' +
        + '"SaleUniqueDealNo__c": "FRMBN-206766",' +
        + '"StoreID__c": "INVALID",' +
        + '"SaleDealNo__c": "206766",' +
        + '"SaleOpenDate__c": "2020-01-11",' +
        + '"PurchaseOrderDate__c": "2020-01-11",' +
        + '"SaleSaleStatusName__c": "RSed",' +
        + '"SaleStockNo__c": "   R10253",' +
        + '"SaleInvtTypeNU__c": "Used",' +
        + '"SaleSaleTypeName__c": "Retail",' +
        + '"SaleSaleTypeCode__c": "1",' +
        + '"SaleAddlCustNo__c": "000065185",' +
        + '"SaleCustIndEmpName__c": "SATELLITE HEALTHCARE",' +
        + '"SaleCustIndEmpPosition__c": "MASTER CCHD",' +
        + '"SaleCustIndLastName_SaleCustIndFirs__c": "BRILLANTES, SANDY",' +
        + '"SaleInvtNewInvoiceAmt__c": "",' +
        + '"SaleInvtNewHoldbackAmt__c": "",' +
        + '"SaleInvtNewOtherDeduct__c": "0.00",' +
        + '"SaleInvtInitialCost__c": "34613.00",' +
        + '"SaleInvtTtlAddsCost__c": "1484.00",' +
        + '"SaleInvtNetCost__c": "36097.00",' +
        + '"SaleInvtGLBalance__c": "34902.00",' +
        + '"Real_Net_Cost__c": "35962.00",' +
        + '"SaleInvtMSRP__c": "36188.00",' +
        + '"SaleSalePriceAmt__c": "34188.00",' +
        + '"Total_Deal_Gross__c": "10381.81",' +
        + '"Lease_Driveoff_Adj__c": "-1999.00",' +
        + '"Trade_UA__c": "0.00",' +
        + '"SaleTrueVehicleProfitAmt__c": "5381.00",' +
        + '"SaleDealerPack__c": "300.00",' +
        + '"SaleTotalCommAmt__c": "1299.16",' +
        + '"SaleCommVehicleProfitAmt__c": "5081.00",' +
        + '"SaleReserveProfitAmt__c": "1127.81",' +
        + '"SaleVESCProfitAmt__c": "2140.00",' +
        + '"SaleFinAddVehicleProfitAmt__c": "1733.00",' +
        + '"SaleLahAmt__c": "0.00",' +
        + '"Back_Gross__c": "5000.81",' +
        + '"SaleSlsmn1Plan__c": "SALESMAN",' +
        + '"SaleSlsmn1CommAmt__c": "1299.16",' +
        + '"SaleSlsmn2Plan__c": "",' +
        + '"SaleSlsmn2CommAmt__c": "0.00",' +
        + '"SaleSlsmn3Plan__c": "",' +
        + '"SaleSlsmn3CommAmt__c": "0.00",' +
        + '"SaleInvtYear__c": "2020",' +
        + '"SaleInvtMakeName__c": "MERCEDES-BENZ",' +
        + '"SaleInvtModelName__c": "GLA250W",' +
        + '"SaleInvtVIN__c": "WDCTG4EB3LU023502",' +
        + '"SaleInvtReceiveDate__c": "2020-11-06",' +
        + '"Age_In_Invt__c": "143",' +
        + '"SaleinvtColor1Name__c": "N. BLACK 6K MI. DP1",' +
        + '"SaleinvtColor2Name__c": "BLACK",' +
        + '"SaleinvtOdometer__c": "6845",' +
        + '"SaleTrd1VIN__c": "2HKRM4H52EH656326",' +
        + '"SaleTrd1Year__c": "2014",' +
        + '"SaleTrd1MakeName__c": "HONDA",' +
        + '"SaleTrd1ModelName__c": "CRV",' +
        + '"SaleTrd1Color1Name__c": "GREY 78K MI.",' +
        + '"SaleTrd1Color1Code__c": "",' +
        + '"SaleTrd1Color2Name__c": "GREY",' +
        + '"SaleTrd1Color2Code__c": "",' +
        + '"SaleTrd1Odometer__c": "78703",' +
        + '"SaleTrd1GrossAmt__c": "9150.00",' +
        + '"SaleTrd1UsedGrossACV__c": "11000.00",' +
        + '"SaleTrd1PayoffAmt__c": "0.00",' +
        + '"SaleTrd2VIN__c": "",' +
        + '"SaleTrd2Year__c": "",' +
        + '"SaleTrd2MakeName__c": "",' +
        + '"SaleTrd2ModelName__c": "",' +
        + '"SaleTrd2Color1Name__c": "",' +
        + '"SaleTrd2Color1Code__c": "",' +
        + '"SaleTrd2Color2Name__c": "",' +
        + '"SaleTrd2Color2Code__c": "",' +
        + '"SaleTrd2Odometer__c": "",' +
        + '"SaleTrd2GrossAmt__c": "0.00",' +
        + '"SaleTrd2UsedGrossACV__c": "0.00",' +
        + '"SaleTrd2PayoffAmt__c": "0.00",' +
        + '"SaleCustVisitDate__c": "2021-10-07",' +
        + '"SaleCustIndFirstName__c": "SANDY",' +
        + '"SaleCustIndMiddleName__c": "",' +
        + '"SaleCustIndLastName__c": "BRILLANTES",' +
        + '"SaleCustIndStreetName__c": "32239 GLENBROOK ST",' +
        + '"SaleCustCity__c": "UNION CITY",' +
        + '"SaleCustState__c": "CA",' +
        + '"SaleCustZipCode__c": "94587",' +
        + '"SaleCustCountyName__c": "ALAMEDA",' +
        + '"PhoneBlock__c": "",' +
        + '"SaleCustHomePhone__c": "7737429310",' +
        + '"SaleCustWorkPhone__c": "7737429310",' +
        + '"SaleCustCellPhone__c": "",' +
        + '"SaleCustEmailAddress__c": "INVALID",' +
        + '"SaleCustIndBirthdate__c": "1969-06-11",' +
        + '"SaleCoapIndFirstName_SaleCoapIndLastN__c": "",' +
        + '"SaleCoapIndFirstName__c": "",' +
        + '"SaleCoapIndMiddleName__c": "",' +
        + '"SaleCoapIndLastName__c": "",' +
        + '"SaleCoapIndStreetName__c": "",' +
        + '"SaleCoapCity__c": "",' +
        + '"SaleCoapState__c": "",' +
        + '"SaleCoapZipCode__c": "",' +
        + '"SaleCoapCountyName__c": "",' +
        + '"SaleCoapHomePhone__c": "",' +
        + '"SaleCoapWorkPhone__c": "",' +
        + '"SaleCustBusContactName__c": "",' +
        + '"SaleCustBusName__c": "SATELLITE HEALTHCARE",' +
        + '"SaleSlsmn1EmpDetailEmpNo__c": "220818",' +
        + '"SaleSlsmn1EmpDetailFirstName__c": "MUZAMIL",' +
        + '"SaleSlsmn1EmpDetailLastName__c": "KHAN",' +
        + '"SaleSlsmn2EmpDetailEmpNo__c": "0",' +
        + '"SaleSlsmn2EmpDetailFirstName__c": "",' +
        + '"SaleSlsmn2EmpDetailLastName__c": "",' +
        + '"SaleSlsmn3EmpDetailEmpNo__c": "     0",' +
        + '"SaleSlsmn3EmpDetailFirstName__c": "",' +
        + '"SaleSlsmn3EmpDetailLastName__c": "",' +
        + '"SaleCloserEmpDetailEmpNo__c": "0",' +
        + '"SaleCloserEmpDetailFirstName__c": "No Closer",' +
        + '"SaleCloserEmpDetailLastName__c": "No Closer",' +
        + '"SaleFImgrEmpDetailEmpNo__c": "220526",' +
        + '"SaleFImgrEmpDetailFirstName__c": "AJMAL",' +
        + '"SaleFImgrEmpDetailLastName__c": "SARDAR",' +
        + '"SaleFinCmpyDetailCmpyCode__c": "55",' +
        + '"SaleFinCmpyDetailAbbrev__c": "TDA",' +
        + '"SaleFinCmpyDetailLegalName__c": "TD AUTO FINANCE",' +
        + '"SaleFinCmpyDetailStreet__c": "PO BX 675",' +
        + '"SaleFinCmpyDetailCity__c": "WILMINGTON",' +
        + '"SaleFinCmpyDetailState__c": "OH",' +
        + '"SaleFinCmpyDetailZip__c": "45177",' +
        + '"SaleTaxAccAmt__c": "0.00",' +
        + '"SaleTaxDocFeeAmtSaleLeaseCappedDlrDoc__c": "85.00",' +
        + '"SaleSlsTaxAmt__c": "3341.62",' +
        + '"SaleStockTotalAmt__c": "37614.62",' +
        + '"SaleNonTxAccAmt__c": "4580.00",' +
        + '"SaleStateDMVAmt__c": "475.00",' +
        + '"SaleAddlDMVFee1__c": "505.00",' +
        + '"SaleTaxSmogFeeAmt__c": "0.00",' +
        + '"SaleDepositAmt__c": "0.00",' +
        + '"SalePick1Amt__c": "0.00",' +
        + '"SaleRebateAmt__c": "0.00",' +
        + '"SaleNetTrdCappedAmt__c": "9150.00",' +
        + '"SaleFinancedAmt__c": "37447.62",' +
        + '"SaleFinAPR__c": "5.990",' +
        + '"SaleFinAddonRateSaleLeasCustAPR__c": "3.22000",' +
        + '"SaleFinBankAddonRateSaleLeasBankAPR__c": "2.60000",' +
        + '"SaleFinDlrParticipationSaleLeaseDlrPa__c": "80.00",' +
        + '"SaleFinTerm__c": "72",' +
        + '"SaleFinTtlReserveAmt__c": "7335.66",' +
        + '"SaleFinMthlyPymt__c": "621.99",' +
        + '"SaleFinBalloonPymt__c": "0.00",' +
        + '"SaleFinMethod__c": "1",' +
        + '"SaleFinMethodName__c": "Normal",' +
        + '"SaleLeaseResidualValue__c": "",' +
        + '"SaleLeasDOSecDeposit__c": "",' +
        + '"SaleLeasAllowedMiles__c": "0",' +
        + '"SaleLeasExcessMileageRate__c": "0.000000",' +
        + '"SaleLeasDOTotal__c": "",' +
        + '"Last_Pymt__c": "2027-04-11",' +
        + '"SaleVescAmt_1__c": "3890.00",' +
        + '"SaleVescAmt__c": "3890.00",' +
        + '"SaleAddlVESCCost__c": "1750.00",' +
        + '"SaleAddlVESCTermMiles__c": "0",' +
        + '"SC_Miles_No_Limit__c": "N",' +
        + '"SaleAddlVESCTermMths__c": "24",' +
        + '"SC_Months_No_Limit__c": "N",' +
        + '"SaleAddlVESCEffDate__c": "2021-10-07",' +
        + '"SaleAddlVESCCmpyCode__c": "3",' +
        + '"SaleAddlVESCCmpyAbbrev__c": "MBZ CERT",' +
        + '"SaleAddlVESCPolicyNo__c": "",' +
        + '"SaleAddlVESCDeductible__c": "0",' +
        + '"SaleAddlVEHCCmpyCode__c": "0",' +
        + '"SaleAddlVEHCCmpyName__c": "",' +
        + '"SaleAddlVEHCContactName__c": "",' +
        + '"SaleAddlVEHCContactStreet__c": "",' +
        + '"SaleAddlVEHContactCity__c": "",' +
        + '"SaleAddlVEHCContactState__c": "",' +
        + '"SaleAddlVEHCContactZipCode__c": "",' +
        + '"SaleAddlVEHCContactPhone__c": "",' +
        + '"SaleAddlVEHCPolicyNo__c": "",' +
        + '"SaleAddlVEHCInsCode__c": "",' +
        + '"SaleAddlVEHCVerifyEmpNo__c": "220526",' +
        + '"SaleAddlVEHCCompDeduct__c": "0",' +
        + '"SaleInvtLicenseNo__c": "",' +
        + '"SaleRSNo__c": "53990662",' +
        + '"SaleAcctControlNoAcctControlNo__c": "116805",' +
        + '"SaleCtrlAssignTstamp__c": "05:27 PM 07/22/2020",' +
        + '"SaleToBusOfficeTstamp__c": "",' +
        + '"SaleAcctMth__c": "07/20",' +
        + '"SaleSaleAccount__c": "43401",' +
        + '"Sale_Account_Desc__c": "",' +
        + '"SaleFfupDepositReceiptNo__c": "",' +
        + '"SaleAcctVehProfitAmt__c": "0.00",' +
        + '"SaleAcctResProfitAmt__c": "0.00",' +
        + '"SaleAcctVescProfitAmt__c": "0.00",' +
        + '"SaleAcctAccProfitAmt__c": "0.00",' +
        + '"SaleAcctCommAmt__c": "0.00",' +
        + '"BuyerFico__c": "671",' +
        + '"SaleSlsmgrEmpDetailempNo__c": "220676",' +
        + '"SaleSlsMgrEmpDetailFirstName__c": "MOHAMMED",' +
        + '"SaleSlsmgrEmpDetailLastName__c": "KARIMI",' +
        + '"SaleCustTypeName__c": "Individual",' +
        + '"SaleCustBusTaxID__c": "",' +
        + '"SaleCustBusTypeName__c": "",' +
        + '"SaleCustIndIncome__c": "0",' +
        + '"SaleCustBusDMVLicenseNo__c": "",' +
        + '"SaleDepartment__c": "C",' +
        + '"Department_Code_Description__c": "PRE-OWNED SALES",' +
        + '"SaleInvtCertifiedPreOwnedYN__c": "",' +
        + '"SaleLAHProfitAmt__c": "0.00",' +
        + '"SaleNetProfitAmt__c": "9082.65",' +
        + '"SaleSlsmn1Percent__c": "15.00",' +
        + '"SaleSlsmn1SpiffAmt__c": "0.00",' +
        + '"SaleSlsmn2Percent__c": "0.00",' +
        + '"SaleSlsmn2SpiffAmt__c": "0.00",' +
        + '"SaleTrd1TrimLevel__c": "C",' +
        + '"SaleTrd2TrimLevel__c": "",' +
        + '"SaleInvtTrimLevel__c": "L",' +
        + '"SaleInvtWheelDrive__c": "2",' +
        + '"SaleInvtFuel__c": "GAS"}]';
    
    @isTest
    private static void testDealUpsert() {
        Test.startTest();
        RestRequest request = new RestRequest();
        request.requestUri = URL.getOrgDomainUrl() + '/services/apexrest/DealsIntegration';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'text/json');
        request.addHeader('description', 'test file');
        request.requestBody = Blob.valueOf(DEAL_INTEGRATION_REQUEST);
        RestContext.request = request;
        DealsIntegrationWebService.upsertDeals();
        Test.stopTest();
        NBMBN_IMPORT__c[] dealList = [SELECT Id From NBMBN_IMPORT__c];
        System.assertNotEquals(0, dealList.size());
    }
    
    @isTest
    private static void testDealUpsertError() {
        Test.startTest();
        RestRequest request = new RestRequest();
        request.requestUri = URL.getOrgDomainUrl() + '/services/apexrest/DealsIntegration';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'text/json');
        request.addHeader('description', 'test file');
        request.requestBody = Blob.valueOf(DEAL_INTEGRATION_INVALID_REQUEST);
        RestContext.request = request;
        DealsIntegrationWebService.upsertDeals();
        Test.stopTest();
        NBMBN_IMPORT__c[] dealList = [SELECT Id From NBMBN_IMPORT__c];
        System.assertEquals(0, dealList.size());
    }
}