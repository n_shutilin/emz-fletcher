@isTest
private class Test_DeleteDataBatchable {
    @TestSetup
    private static void init() {
        WorkOrder wo = new WorkOrder(Dealer_ID__c = 'CDKTS');
        insert wo;
        
        WorkOrderLineItem woli = new WorkOrderLineItem(WorkOrderId = wo.Id);
        insert woli;
    }
    
    @isTest
    private static void testDeleteRecords() {
        Test.startTest();
        DeleteDataBatchable roBatch = new DeleteDataBatchable('CDKTS');
		Id roBatchId = Database.executeBatch(roBatch);
        Test.stopTest();
        
        
    }
}