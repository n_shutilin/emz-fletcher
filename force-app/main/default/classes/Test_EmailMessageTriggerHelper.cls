@isTest
public class Test_EmailMessageTriggerHelper {
    @TestSetup
    static void setup(){
        Account testAcc = new Account(Name = 'Test acc for test');
        insert testAcc;

        Opportunity testOpp = new Opportunity(
            AccountId = testAcc.id,
            Name = 'opportunity for test',
            Dealer_ID__c = 'NBMBN',
            StageName = 'Working',
            Customer_Email_Primary__c = 'hale.brandon@fjag.com.dev1.invalid',
            CloseDate = Date.today().addMonths(1)
        );
        insert testOpp;

        Opportunity testOpp2 = new Opportunity(
            AccountId = testAcc.id,
            Name = 'opportunity for test',
            Dealer_ID__c = 'CHMBN',
            StageName = 'Working',
            Customer_Email_Secondary__c = 'morley.kevin@fjag.com.dev1.invalid',
            CloseDate = Date.today().addMonths(1)
        );
        insert testOpp2;

        Lead testLead = new Lead(
            Salutation = 'Mr.',
            FirstName = 'Test',
            LastName = 'Testovich',
            Status = 'New',
            Dealer_Id__c = 'CHAUD',
            Email = 'krasnow.michael@fjag.com.dev1.invalid'
        );
        insert testLead;

        Account tsAcc = new Account(
            LastName = 'Test',
    		RecordTypeId = '012c00000002JJFAA2',
    		Dealer__c = 'FRPOR',
            Dealer_ID__pc = 'FRPOR',
            PersonEmail = 'merge.data@fjag.com.dev1.invalid'
        );
        insert tsAcc;

        Account tsAcc2 = new Account(
            LastName = 'Test2',
    		RecordTypeId = '012c00000002JJFAA2',
    		Dealer__c = 'AUDFJ',
            Dealer_ID__pc = 'AUDFJ',
            Email_Secondary__pc = 'poleshuk.aleksandr@fjag.com.dev1.invalid'
        );
        insert tsAcc2;
    }

    @isTest
    public static void relatedToOppTest() {
        EmailMessage testMessage = new EmailMessage(
            Subject='SuperTest',
            FromAddress='test@test.com', 
            FromName='Test Testovich', 
            ToAddress='hale.brandon@fjag.com.dev1.invalid'
        );
        Test.startTest();
        insert testMessage;
        EmailMessage result = [SELECT Id, RelatedToId FROM EmailMessage WHERE Subject = 'SuperTest'];
        Test.stopTest();        
        System.assertNotEquals(null, result.RelatedToId, 'Not set related Id to EmailMessage from Opp');
    }

    @isTest
    public static void relatedToLeadTest() {
        EmailMessage testMessage = new EmailMessage(
            Subject = 'SuperTest1',
            FromAddress = 'test1@test.com', 
            FromName = 'Test Testovich1', 
            ToAddress = 'krasnow.michael@fjag.com.dev1.invalid'
        );
        Test.startTest();
        insert testMessage;
        List<EmailMessage> ems = [SELECT Id, ToIds FROM EmailMessage WHERE Subject = 'SuperTest1'];
        Test.stopTest();
        System.assertEquals(1, ems.size(), 'Original record EmailMessage not deleted');
        List<EmailMessageRelation> emrs = [SELECT Id FROM EmailMessageRelation WHERE EmailMessageId = :ems[0].Id];
        System.debug(emrs);
        System.assert(emrs.size() > 0, 'EmailMessageRelation was not created');
    }

    @isTest
    public static void relatedToAccTest() {
        EmailMessage testMessage = new EmailMessage(
            Subject = 'SuperTest2',
            FromAddress = 'test2@test.com', 
            FromName = 'Test Testovich2', 
            ToAddress = 'merge.data@fjag.com.dev1.invalid'
        );
        Test.startTest();
        insert testMessage;
        EmailMessage result = [SELECT Id, RelatedToId FROM EmailMessage WHERE Subject = 'SuperTest2'];
        Test.stopTest();     
        System.assertNotEquals(null, result.RelatedToId, 'Not set related Id to EmailMessage from Account');
    }

    @isTest
    public static void relatedToBatch() {
        List<EmailMessage> testList = new List<EmailMessage>();
        for (Integer i = 0; i < 50; i++) {
            testList.add(new EmailMessage(
                Subject = 'SuperTest' + i,
                FromAddress = 'test' + i + '@test.com', 
                FromName = 'Test Testovich' + i, 
                ToAddress = 'hale.brandon@fjag.com.dev1.invalid'
            ));
        }
        for (Integer i = 50; i < 100; i++) {
            testList.add(new EmailMessage(
                Subject = 'SuperTest' + i,
                FromAddress = 'test' + i + '@test.com', 
                FromName = 'Test Testovich' + i, 
                ToAddress = 'morley.kevin@fjag.com.dev1.invalid'
            ));
        }
        for (Integer i = 100; i < 150; i++) {
            testList.add(new EmailMessage(
                Subject = 'SuperTest' + i,
                FromAddress = 'test' + i + '@test.com', 
                FromName = 'Test Testovich' + i, 
                ToAddress = 'merge.data@fjag.com.dev1.invalid'
            ));
        }
        for (Integer i = 150; i < 200; i++) {
            testList.add(new EmailMessage(
                Subject = 'SuperTest' + i,
                FromAddress = 'test' + i + '@test.com', 
                FromName = 'Test Testovich' + i, 
                ToAddress = 'poleshuk.aleksandr@fjag.com.dev1.invalid'
            ));
        }
        Test.startTest();
        insert testList;
        List<EmailMessage> results = [SELECT Id, RelatedToId FROM EmailMessage WHERE Subject LIKE 'SuperTest%'];
        Test.stopTest();
        Boolean testResult = true;
        for (EmailMessage em : results) {
            if (em.RelatedToId == null) {
                testResult = false;
                break;
            }
        }     
        System.assertEquals(true, testResult, 'Not all EmailMessage record get related Id');
    }

    @isTest
    public static void negativeTest() {
        EmailMessage testMessage = new EmailMessage(
            Subject = 'SuperTest3',
            FromAddress = 'test3@test.com', 
            FromName = 'Test Testovich3', 
            ToAddress = 'yervand@emzcloud.com'
        );
        Test.startTest();
        insert testMessage;
        EmailMessage result = [SELECT Id, RelatedToId FROM EmailMessage WHERE Subject = 'SuperTest3'];
        Test.stopTest();     
        System.assertEquals(null, result.RelatedToId, 'Set not valid Id');
    }
}