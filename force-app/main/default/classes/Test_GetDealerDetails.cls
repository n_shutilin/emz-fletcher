@isTest
public class Test_GetDealerDetails {
	
    @isTest
    private static void test(){
        Id dealerRecordType = Schema.SObjectType.Account.RecordTypeInfosByName.get('Dealer').RecordTypeId;
        Due_Bill__c bill = new Due_Bill__c();
        bill.Dealer_Id__c = 'AUDFJ';
        insert bill;
        Account firstAccount = new Account();
        firstAccount.Name = 'testName';
        firstAccount.Dealer_Code__c = 'AUDFJ';
        firstAccount.RecordTypeId = dealerRecordType;
        insert firstAccount;
        GetDealerDetails controller = new GetDealerDetails(new ApexPages.StandardController(bill));
    }
}