@isTest
public class Test_HIC_EmailMessageControllerOnComm {
    
    @testSetup
    private static void init(){                       
        EmailTemplate em = new EmailTemplate();
        em.FolderId = UserInfo.getUserId();
        em.DeveloperName = 'testTemplate';
        em.Name = 'testTemplate';
        em.TemplateType= 'Text';
        insert em;
    }
	
    @isTest
    private static void sendMailMethodTest(){
        Lead testLead = new Lead();
        testLead.LastName = 'testName';
        testLead.Email='test@test.com';
        insert testLead;
        
        String templateId = [SELECT Id 
                             FROM EmailTemplate 
                             WHERE DeveloperName = 'testTemplate' LIMIT 1].Id;
        HIC_EmailMessageControllerOnComm.sendMailMethod('test@test.com', 'testSubject', 'testBody',testLead.Id, 'folderId', templateId);
    }
    
    @isTest
    private static void getTemplatesTest(){
        HIC_EmailMessageControllerOnComm.getEmailTempaltes();
    }
    
    @isTest
    private static void getLeadRecTest(){
        Lead testLead = new Lead();
        testLead.LastName = 'test';
        testLead.Email='test@test.com';
        insert testLead;
        HIC_EmailMessageControllerOnComm.getLeadRec(testLead.Id);
    }
}