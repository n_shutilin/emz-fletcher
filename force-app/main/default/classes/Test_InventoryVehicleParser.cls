@isTest
public class Test_InventoryVehicleParser {
    private static final String TEST_CDK_DEALER_CODE = 'test';
    
    @testSetup
    private static void init(){
        Account testAccount = new Account();
        testAccount.Name = 'testName';
        testAccount.CDK_Dealer_Code__c = TEST_CDK_DEALER_CODE;
        insert testAccount;
    }
	
    @isTest
    private static void processTest(){
        //give
        String accountId = [SELECT Id
                           	FROM Account
                            WHERE CDK_Dealer_Code__c = :TEST_CDK_DEALER_CODE].Id;
        String xml = '<?xml version="1.0" encoding="UTF-8"?>' +
						+'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' +
							+'<pip:InventoryVehicle xmlns:pip="http://www.dmotorworks.com/pip-extract-inventory-vehicle">' +
								+'<pip:Model> ' +
									+'LFC06940' +
								+'</pip:Model>' +
							+'</pip:InventoryVehicle>' +
 					+'</soapenv:Envelope>';
        Dom.Document document = new Dom.Document();
        document.load(xml);
        //when
        InventoryVehicleParser.process(document, accountId);
        //then
    }
}