@isTest
public class Test_LockRecordHelper {
	@TestSetup
	static void setup() {
        /* String uid = Userinfo.getUserId();
		Opportunity opportunity = new Opportunity(
            Name = 'Test Opportunity', 
            StageName = 'New', 
            CloseDate = Date.today().addMonths(1),
            UserWorkingWithRecord__c =uid);
		insert opportunity;

		Lead aLead = new Lead(
            LastName = 'Test Lead', 
            Dealer_ID__c = 'AUDFJ', 
            Status = 'New',
            UserWorkingWithRecord__c =uid);
		insert aLead; */

		Opportunity opportunity = new Opportunity(
            Name = 'Test Opportunity', 
            StageName = 'New', 
            CloseDate = Date.today().addMonths(1));
		insert opportunity;

		Lead aLead = new Lead(
            LastName = 'Test Lead', 
            Dealer_ID__c = 'AUDFJ', 
            Status = 'New');
		insert aLead;

	}

    @isTest
    public static void getRecordDataTest() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Test Opportunity' LIMIT 1];
        Test.startTest();
        Opportunity result = (Opportunity) LockRecordHelper.getRecordData(opp.Id, 'Opportunity');
        Test.stopTest();
        System.assertEquals(opp.Id, result.Id);
    }

    @isTest
    public static void updateRecordDataTest() {
        String uid = Userinfo.getUserId();
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Test Opportunity' LIMIT 1];
        Lead l = [SELECT Id FROM Lead WHERE Name = 'Test Lead' LIMIT 1];
        Test.startTest();
        Boolean oppResult = LockRecordHelper.updateRecordData(opp.Id, 'Opportunity', uid);
        Boolean lResult = LockRecordHelper.updateRecordData(l.Id, 'Lead', uid);
        Test.stopTest();
        System.assertEquals(oppResult, true);
        System.assertEquals(lResult, true);
    }

    @isTest
    public static void preventUpdateLockOpportunityRecordTest() {
        String uid = Userinfo.getUserId();
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Test Opportunity' LIMIT 1];
        try {
            Test.startTest();
            opp.UserWorkingWithRecord__c = 'test';
            update opp;
            Test.stopTest();
        } catch (Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot save changes! The record is blocked!') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        
    }

    @isTest
    public static void preventUpdateLockLeadRecordTest() {
        String uid = Userinfo.getUserId();
        Lead l = [SELECT Id FROM Lead WHERE Name = 'Test Lead' LIMIT 1];
        try {
            Test.startTest();
            l.UserWorkingWithRecord__c = 'test';
            update l;
            Test.stopTest();
        } catch (Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot save changes! The record is blocked!') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        
    }

    @isTest
    public static void recordRequestTest() {
        String uid = Userinfo.getUserId();
        Test.startTest();
        LockRecordHelper.recordRequest('testRecordId', 'testUserIdBlockedRecord', uid, false);
        Test.stopTest();
    }
}