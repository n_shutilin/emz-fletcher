@isTest
public class Test_LookupSearchResult {
	
    @isTest
    private static void test(){
        Account firstTestAccount = new Account(Name = 'testName');
        insert firstTestAccount;
        Account secondTestAccount = new Account(Name = 'testName2');
        insert secondTestAccount;
        LookupSearchResult firstResult = new LookupSearchResult(firstTestAccount.Id, 'Account', 'test', 'test', null, firstTestAccount);
        LookupSearchResult secondResult = new LookupSearchResult(secondTestAccount.Id, 'Account', 'test', null, 'test', secondTestAccount);
        firstResult.compareTo(secondResult);
        firstResult.getIcon();
        firstResult.getObj();
        firstResult.getId();
        firstResult.getSubtitle();
        firstResult.getId();
        firstResult.getSObjectType();
    }
}