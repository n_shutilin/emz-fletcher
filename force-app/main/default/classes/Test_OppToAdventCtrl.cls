@isTest
public class Test_OppToAdventCtrl {
    private static final String TEST_NAME = 'testName';
    private static final String FIRST_TEST_BODY = '{'+
  					+'"success": true,'+
  					+'"message": "test",'+
  					+'"errors": ['+
  						+'{'+
  							+'"msg": null,'+
  							+'"id": null'+
  						+'}'+
  					+'],'+
  					+'"data": {'+
    					+'"opportunity_number": null,'+
    					+'"opportunity_id": null,'+
    					+'"client_number": null,'+
    					+'"client_id": null'+
  					+'}'+
				+'}';
    private static final String SECOND_TEST_BODY = '{'+
 					 +'"totalProperty": 0,'+
  					 +'"success": true,'+
  					 +'"data": ['+
  					 	+'{'+
  					 	+'"work_phone_ext": null,'+
  					 	+'"work_phone": null,'+
  					 	+'"individual_spouse": null,'+
  					 	+'"individual_middle_name": null,'+
  					 	+'"individual_last_name": null,'+
  					 	+'"individual_gender_c": null,'+
  					 	+'"individual_first_name": null,'+
  					 	+'"home_phone_ext": null,'+
  					 	+'"home_phone": null,'+
  					 	+'"fax_phone": null,'+
  					 	+'"email_address_list": null,'+
  					 	+'"customer_f": null,'+
  					 	+'"current_address_street": null,'+
  					 	+'"current_address_postal_code": null,'+
  					 	+'"current_address_city": null,'+
  					 	+'"current_address_address_state_c": null,'+
  					 	+'"client_number": null,'+
  					 	+'"client_id": null,'+
  					 	+'"client_generation_c": null,'+
  					 	+'"cell_phone": null,'+
  					 	+'"business_name": null,'+
  					 	+'"business_f": null,'+
  					 	+'"business_contact_position": null,'+
  					 	+'"business_contact_name": null,'+
  					 	+'"birthdate": null'+
  					 	+'}'+
  					 +']}';
    
	@testSetup
    private static void init(){
        Account testAccount = new Account();
        testAccount.Name = TEST_NAME;
        testAccount.Dealer_Code__c = 'AUDFJ';
        testAccount.Advent_Store_Number__c = 'testNumber';
        insert testAccount;
        
        Vehicle__c testVehicle = new Vehicle__c();
        testVehicle.VIN__c = 'testVIN';
        insert testVehicle;
        
        Contact testContact = new Contact();
        testContact.AccountId = testAccount.Id;
        testContact.LastName = TEST_NAME;
        testContact.Email = 'test@test.com';
        testContact.FirstName = TEST_NAME;
        testContact.Phone = '123123123';
        testContact.HomePhone = '11111';
        insert testContact;
        
        Opportunity testOpp = new Opportunity();
        testOpp.AccountId = testAccount.Id;
        testOpp.Name = TEST_NAME;
        testOpp.StageName = 'New';
        testOpp.CloseDate = Date.today();
        testOpp.Vehicle__c = testVehicle.Id;
        testOpp.Dealer_ID__c = 'AUDFJ';
        insert testOpp;
        
        OpportunityContactRole role = new OpportunityContactRole();
        role.OpportunityId = testOpp.Id;
        role.ContactId = testContact.Id;
        role.Role = 'Buyer';
        insert role;
        
        Trade_In__c tradeIn = new Trade_In__c();
        tradeIn.Opportunity__c = testOpp.Id;
        tradeIn.VIN__c = '123';
        insert tradeIn;
        
        Account testAccount2 = new Account();
        testAccount2.Name = TEST_NAME + 2;
        testAccount2.Dealer_Code__c = 'AUDFJ';
        testAccount2.Advent_Store_Number__c = 'testNumber';
        insert testAccount2;
        
        Vehicle__c testVehicle2 = new Vehicle__c();
        testVehicle2.VIN__c = 'testVIN2';
        insert testVehicle2;
        
        /*Contact testContact2 = new Contact();
        testContact2.AccountId = testAccount2.Id;
        testContact2.LastName = TEST_NAME + 2;
        testContact2.Email = 'test2@test.com';
        testContact2.FirstName = TEST_NAME + 2;
        testContact2.Phone = '88003253535';
        insert testContact2;*/
        
        Opportunity testOpp2 = new Opportunity();
        testOpp2.AccountId = testAccount2.Id;
        testOpp2.Name = TEST_NAME + 2;
        testOpp2.StageName = 'New';
        testOpp2.CloseDate = Date.today();
        testOpp2.Vehicle__c = testVehicle2.Id;
        testOpp2.Dealer_ID__c = 'AUDFJ';
        insert testOpp2;
        
        OpportunityContactRole roleCobuyer = new OpportunityContactRole();
        roleCobuyer.OpportunityId = testOpp2.Id;
        roleCobuyer.ContactId = testContact.Id;
        roleCobuyer.Role = 'Cobuyer';
        insert roleCobuyer;
    }
    
    @isTest
    private static void createAdventOppTest(){
        String accId = [SELECT Id FROM Account WHERE Name = :TEST_NAME].Id;
        String oppId = [SELECT Id
                        FROM Opportunity
                        WHERE AccountId = :accId].Id;
        Test.setMock(HttpCalloutMock.class, new AdventApiMock(SECOND_TEST_BODY));
    	Test.startTest();
        OppToAdventCtrl.createAdventOpp(oppId);
        Test.stopTest();
        
    }
    @isTest
    private static void createTradeInAndCreditAppTest(){
        String accId = [SELECT Id FROM Account WHERE Name = :TEST_NAME].Id;
        String oppId = [SELECT Id
                        FROM Opportunity
                        WHERE AccountId = :accId].Id;
        Test.setMock(HttpCalloutMock.class, new AdventApiMock(FIRST_TEST_BODY));
    	Test.startTest();
        OppToAdventCtrl.createTradeInAndCreditApp(oppId);
        Test.stopTest();        
    }
    @isTest
    private static void updateAdventOppTest(){
        String accId = [SELECT Id FROM Account WHERE Name = :TEST_NAME].Id;
        String oppId = [SELECT Id
                        FROM Opportunity
                        WHERE AccountId = :accId].Id;
        Test.setMock(HttpCalloutMock.class, new AdventApiMock(FIRST_TEST_BODY));
    	Test.startTest();
        OppToAdventCtrl.updateAdventOpp(oppId);
        Test.stopTest();        
    }
    @isTest
    private static void createCreditApplicationTest(){
        String accId = [SELECT Id FROM Account WHERE Name = :TEST_NAME].Id;
        String oppId = [SELECT Id
                        FROM Opportunity
                        WHERE AccountId = :accId].Id;
        Test.setMock(HttpCalloutMock.class, new AdventApiMock(FIRST_TEST_BODY));
    	Test.startTest();
        OppToAdventCtrl.createCreditApplication(new AdventApi('test','test'), oppId, new Map<Id, String>());
        Test.stopTest();        
    }     
    @isTest
    private static void getCreditApplicationDataTest(){
        String accId = [SELECT Id FROM Account WHERE Name = :TEST_NAME].Id;
        List<Opportunity> oppList = [SELECT Id, ContactId
                        			 FROM Opportunity
                        			 WHERE AccountId = :accId];
        Test.setMock(HttpCalloutMock.class, new AdventApiMock(FIRST_TEST_BODY));
        Set<Id> idSet = new Set<Id>();
        idSet.add(oppList[0].ContactId);
    	Test.startTest();
        OppToAdventCtrl.getCreditApplicationData(oppList[0].Id,idSet);
        Test.stopTest();        
    }
    @isTest
    private static void getCobuyerDataTest(){
        String accId = [SELECT Id FROM Account WHERE Name = 'testName2'].Id;
        String oppId = [SELECT Id
                        FROM Opportunity
                        WHERE AccountId = :accId].Id;
        Test.setMock(HttpCalloutMock.class, new AdventApiMock(FIRST_TEST_BODY));
    	Test.startTest();
        OppToAdventCtrl.getCobuyerData(oppId);
        Test.stopTest();        
    }   
    @isTest
    private static void getTradeInDataTest(){
        String accId = [SELECT Id FROM Account WHERE Name = :TEST_NAME].Id;
        String oppId = [SELECT Id
                        FROM Opportunity
                        WHERE AccountId = :accId].Id;
        Test.setMock(HttpCalloutMock.class, new AdventApiMock(FIRST_TEST_BODY));
    	Test.startTest();
        OppToAdventCtrl.getTradeInData(oppId);
        Test.stopTest();        
    } 
    @isTest
    private static void createTradeInTest(){
        String accId = [SELECT Id FROM Account WHERE Name = :TEST_NAME].Id;
        String oppId = [SELECT Id
                        FROM Opportunity
                        WHERE AccountId = :accId].Id;
        Test.setMock(HttpCalloutMock.class, new AdventApiMock(FIRST_TEST_BODY));
    	Test.startTest();
        OppToAdventCtrl.createTradeIn(new AdventApi('test','test'),oppId,oppId);
        Test.stopTest();        
    } 
    @isTest
    private static void createClientTest(){
        String accId = [SELECT Id FROM Account WHERE Name = :TEST_NAME].Id;
        String oppId = [SELECT Id
                        FROM Opportunity
                        WHERE AccountId = :accId].Id;
        Test.setMock(HttpCalloutMock.class, new AdventApiMock(FIRST_TEST_BODY));
    	Test.startTest();
        OppToAdventCtrl.createClient(new AdventApi('test','test'), new Contact());
        Test.stopTest();        
    }     
}