@isTest(SeeAllData=false)
private class Test_OpportunityDetailsController {
    @TestSetup
    static void testSetup(){
        Opportunity opp1 = new Opportunity(
            Name = 'Test Opp1',
            StageName = 'New',
            CloseDate = Date.today()
        );
        insert opp1;

        Opportunity opp2 = new Opportunity(
            Name = 'Test Opp2',
            StageName = 'New',
            CloseDate = Date.today(),
            Original_Opportunity_Id__c = opp1.Id
        );
        insert opp2;
    }

    @isTest
    static void testGetOpportunityFields() {
        String fieldsJSON = OpportunityDetailsController.getOpportunityFields();
        System.assertNotEquals('[]', fieldsJSON);
    }

    @isTest
    static void testGetOriginalOpportunityID() {
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Test Opp2' LIMIT 1];
        String originalOppId = OpportunityDetailsController.getOriginalOpportunityID(opp.Id);
        Opportunity originOpp = [SELECT Id FROM Opportunity WHERE Name = 'Test Opp1' LIMIT 1];
        System.assertEquals(originOpp.Id, originalOppId);
    }
}