@isTest
private class Test_PartsSpecialOrderExtract {
    private static final String RESPONSE_BODY = '<PartsSpecialOrder xmlns="http://www.dmotorworks.com/pip-extract-parts-special-order">' +
        +'<SpecialOrder>' +
            +'<HostItemID>20955</HostItemID>' +
            +'<SpecialOrderNo>20955</SpecialOrderNo>' +
            +'<CustNo>2419172</CustNo>' +
            +'<CustomerName>BOWEN,LINDA F</CustomerName>' +
            +'<CustomerName2 />' +
            +'<CustomerAddress>23648 STONEHENGE BLVD</CustomerAddress>' +
            +'<CustomerCSZ>WASHOUGAL, WA 98671</CustomerCSZ>' +
            +'<HomePhone>3604777892</HomePhone>' +
            +'<BusinessPhone />' +
            +'<OpenDate>2011-12-21</OpenDate>' +
            +'<Comment1 />' +
            +'<Comment2 />' +
            +'<PONumber />' +
            +'<ClosedDate />' +
            +'<Origin>R</Origin>' +
            +'<VehID>72121502</VehID>' +
            +'<ServiceAdvisor>302</ServiceAdvisor>' +
            +'<CustomerAddress2 />' +
            +'<CellPhone />' +
            +'<ErrorLevel>0</ErrorLevel>' +
            +'<ErrorMessage />' +
        +'</SpecialOrder>' +
        +'<SpecialOrderDetail>' +
            +'<HostItemID>20955*1</HostItemID>' +
            +'<SpecialOrderNo>20955</SpecialOrderNo>' +
            +'<DetailLineNo>1</DetailLineNo>' +
            +'<PartNumber>12479377</PartNumber>' +
            +'<Description>COVER KIT</Description>' +
            +'<QuantityOrdered>1</QuantityOrdered>' +
            +'<Priority>CSO</Priority>' +
            +'<Bin1 />' +
            +'<Employee>997003</Employee>' +
            +'<SalesPerson />' +
            +'<PartOrderDate>2011-12-21</PartOrderDate>' +
            +'<PartOrderType>C</PartOrderType>' +
            +'<PartOrderNo>2221</PartOrderNo>' +
            +'<LastReceivedDate>2011-12-22</LastReceivedDate>' +
            +'<QuantityFilled>0</QuantityFilled>' +
            +'<ClosedDate />' +
            +'<CancelDate />' +
            +'<AccountingAccount>DART-A</AccountingAccount>' +
            +'<InvoiceNumber>20955*5</InvoiceNumber>' +
            +'<TechNo>997365</TechNo>' +
            +'<RoPrepaidAmount />' +
            +'<RoPrepaidCoreAmount />' +
            +'<ErrorLevel>0</ErrorLevel>' +
            +'<ErrorMessage />' +
            +'<CurrentOHQuantity>1</CurrentOHQuantity>' +
            +'<PreSoldQuantity>1</PreSoldQuantity>' +
            +'<RequestedQuantity>0</RequestedQuantity>' +
            +'<AvailableOHQuantity>0</AvailableOHQuantity>' +
        +'</SpecialOrderDetail>' +
        +'<ErrorCode>0</ErrorCode>' +
        +'<ErrorMessage />' +
    +'</PartsSpecialOrder>';
    
    @TestSetup
    private static void init() {
        CDK_Integration__c settings = new CDK_Integration__c(
            Password__c = '111111111',
            Username__c = 'testUser'
        );
        insert settings;
        
        //List<Account> accList = new List<Account>();
        Id recTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        //Id personAccRecTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        Account dealerAcc = new Account(
            Name = 'CDKTS Test Account',
            RecordTypeId = recTypeId,
            CDK_Dealer_Code__c = '3PA17867',
            Dealer_Code__c = 'CDKTS'
        );
        insert dealerAcc;
        //accList.add(dealerAcc);
    }

    @isTest
    private static void testScheduledJob() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(RESPONSE_BODY));
        String jobId = System.schedule('Account Updates', '0 0 0 28 2 ? 2022', new PartsSpecialOrderSchedulable());
        Test.stopTest();
    }
    
    /*@isTest
    private static void testServiceSalesExtractWithDateRange() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(RESPONSE_BODY));
        ServiceSalesClosedBatchable batch = new ServiceSalesClosedBatchable('01/01/2020','01/02/2020');
		Database.executeBatch(batch,1);
        Test.stopTest();
    }*/
}