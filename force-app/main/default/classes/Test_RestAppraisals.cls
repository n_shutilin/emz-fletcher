@isTest
public class Test_RestAppraisals {
    private static final String TEST_VIN = 'testV';
    
	@testSetup
    private static void init(){
        Account testAccount = new Account();
        testAccount.Name = 'testName';
        insert testAccount;
        
        Vehicle__c veh = new Vehicle__c(VIN__c = TEST_VIN);
        insert veh;
        
        Contact testContact = new Contact();
        testContact.LastName = 'testName';
        testContact.AccountId = testAccount.Id;
        insert testContact;
        
        Opportunity testOpp = new Opportunity();
        testOpp.AccountId = testAccount.Id;
        testOpp.Name = 'testName';
        testOpp.CloseDate = Date.today();
        testOpp.StageName = 'New';
        testOpp.Vehicle__c = veh.Id;
        insert testOpp;
        
        Trade_In__c tradeIn = new Trade_In__c(VIN__c = TEST_VIN);
        tradeIn.Opportunity__c = testOpp.Id;
        insert tradeIn;
        
        CDK_Integration__c settings = new CDK_Integration__c();
        settings.Inventory_Plus_Api_Key__c = 'test';
        insert settings;
    }
    
    @isTest
    private static void appraisalRetrieveTest(){
        String tradeId = [SELECT Id 
                          FROM Trade_In__c
                          WHERE VIN__c = :TEST_VIN].Id;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestAppraisalsMock());
        RestAppraisals.appraisalRetrieve(tradeId);
        Test.stopTest();
    }
    
    @isTest
    private static void secondAppraisalRetrieveTest(){
        String tradeId = [SELECT Id 
                          FROM Trade_In__c
                          WHERE VIN__c = :TEST_VIN].Id;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestAppraisalsMock(200));
        RestAppraisals.appraisalRetrieve(tradeId);
        Test.stopTest();
    }
    
    @isTest
    private static void thirdFppraisalRetrieveTest(){
        String tradeId = [SELECT Id 
                          FROM Trade_In__c
                          WHERE VIN__c = :TEST_VIN].Id;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestAppraisalsMock(100));
        RestAppraisals.appraisalRetrieve(tradeId);
        Test.stopTest();
    }
    
    @isTest
    private static void appraisalPostTest(){
        String tradeId = [SELECT Id 
                          FROM Trade_In__c
                          WHERE VIN__c = :TEST_VIN].Id;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestAppraisalsMock());
        RestAppraisals.appraisalPost(tradeId);
        Test.stopTest();
    }

    @isTest
    private static void secondAppraisalPostTest(){
        String tradeId = [SELECT Id 
                          FROM Trade_In__c
                          WHERE VIN__c = :TEST_VIN].Id;
        String testJson = '{'+
  							 +'"messages": [],'+
  							 +'"location": null,'+
        					 +'"content": {"eid":"test"}'+
						 +'}';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestAppraisalsMock(200));
        RestAppraisals.appraisalPost(tradeId);
        Test.stopTest();
    }

	@isTest
    private static void test(){
        RestAppraisals.EvaluationWrapper test = new RestAppraisals.EvaluationWrapper(new Evaluation__c());
        
        RestAppraisals.ReconditionEstimateWrapper test2 = new RestAppraisals.ReconditionEstimateWrapper(new Recondition_Estimate__c());
        
        RestAppraisals.ConditionReportWrapper test3 = new RestAppraisals.ConditionReportWrapper(new Condition_Report__c());
    }    
}