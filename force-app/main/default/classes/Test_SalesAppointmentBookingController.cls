@isTest
public class Test_SalesAppointmentBookingController {
    
    private static final String expectedSalesAppointmentTypesJSON = '[{"active":true,' +
    	 				  										    + '"defaultValue":false,' +
    	 				  										    + '"label":"Pickup & Delivery",' +
    	 				  										    + '"validFor":null,' +
    	 				  										    + '"value":"Pickup & Delivery"},' +
    	 				  										    + '{"active":true,' +
    	 				  										    + '"defaultValue":false,' +
    	 				  										    + '"label":"Showroom visit",' +
    	 				  										    + '"validFor":null,' +
    	 				  										    + '"value":"Showroom visit"},' +
    	 				  										    + '{"active":true,' +
    	 				  										    + '"defaultValue":false,' +
    	 				  										    + '"label":"Test-Drive",' +
    	 				  										    + '"validFor":null,' +
    	 				  										    + '"value":"Test-Drive"}]';

    private static final String TEST_DEALER_LABEL = 'CDKTS';
    private static final String FIRST_TEST_ACCOUNT_FIRST_NAME = 'firstTestAccountFirstName';
    private static final String FIRST_TEST_ACCOUNT_LAST_NAME = 'firstTestAccountLastName';
    private static final String FIRST_TEST_ACCOUNT_PERSONAL_EMAIL = 'test@test.org'; 
    private static final String FIRST_TEST_OPERATING_HOURS_NAME = 'firstTestOperatinHours';
    private static final String FIRST_TEST_SERVICE_RESOURCE = 'firstTestServiceResource';
    private static final String FIRST_TEST_SERVICE_TERRITORY_HOURS_NAME = 'firstTestServiceTerritory';    
    private static final String ACCOUNT_SOBJECT_TYPE_LABEL = 'Account';
    private static final String TEST_VIN_LABEL = 'testVIN';
    private static final String STATUS_CURRENT_LABEL = 'Current';    
    private static final String STAGE_NEW_LABEL = 'New';
    private static final String FIRST_OPPORTUNITY_NAME = 'firstOpportunityName';
    private static final String DEALER_TEST_ACCOUNT_NAME = 'testDealerAccountName';
    private static final String FIRST_TEST_WORK_TYPE = 'firstTestWorkType';
    private static final String STATUS_DECLINED = 'Declined';
    private static final String STATUS_REMOVED = 'Removed';
    private static final String STANDARD_YEAR_MONTH_DAY_FORMAT = 'yyyy-MM-dd';

    @testSetup
    private static void init(){
        Account firstTestAccount = new Account(FirstName = FIRST_TEST_ACCOUNT_FIRST_NAME, 
                                               LastName = FIRST_TEST_ACCOUNT_LAST_NAME);
        firstTestAccount.PersonEmail = FIRST_TEST_ACCOUNT_PERSONAL_EMAIL;
        insert firstTestAccount;
        
        Id appointmenTrecordTypeId = [SELECT Id 
                           			  FROM RecordType 
                           			  WHERE DeveloperName = 'SalesAppointment' 
                           			  AND sObjectType = 'ServiceAppointment'].Id;
        Opportunity firstTestOpportunity = new Opportunity();
        firstTestOpportunity.Dealer_Id__c = TEST_DEALER_LABEL;
        firstTestOpportunity.OwnerId = UserInfo.getUserId();
        firstTestOpportunity.AccountId = firstTestAccount.Id;
        firstTestOpportunity.StageName = STAGE_NEW_LABEL;
        firstTestOpportunity.Name = FIRST_OPPORTUNITY_NAME;
        firstTestOpportunity.CloseDate = Date.today();
		insert firstTestOpportunity;
        
        Id recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Dealer' AND sObjectType = 'Account'].Id;
        Account dealerTestAccount = new Account();
        dealerTestAccount.Name = DEALER_TEST_ACCOUNT_NAME;
        dealerTestAccount.RecordTypeId = recordTypeId;
        dealerTestAccount.Dealer_Code__c = TEST_DEALER_LABEL;
        insert dealerTestAccount;
        
                WorkType firstTestWorkType = new WorkType(
            Name = FIRST_TEST_WORK_TYPE,
            EstimatedDuration = 11.11, 
            DispatchCode__c = 'aaaaa',
            Dealer__c = TEST_DEALER_LABEL
        );
        insert firstTestWorkType;
       
        ServiceResource firstTestServiceResource = new ServiceResource(isActive = true, Name = FIRST_TEST_SERVICE_RESOURCE);
        firstTestServiceResource.AccountId = firstTestAccount.Id;
        firstTestServiceResource.Dealer__c = TEST_DEALER_LABEL;
        firstTestServiceResource.RelatedRecordId = UserInfo.getUserId();
        insert firstTestServiceResource;
        
        WorkTypeGroup firstTestWorkTypeGroup = new WorkTypeGroup(Name ='Most Commonly Used', DealerId__c = TEST_DEALER_LABEL);
        insert firstTestWorkTypeGroup;
        
        WorkTypeGroupMember firstTestWorkTypeGroupMember = new WorkTypeGroupMember(
            WorkTypeGroupId = firstTestWorkTypeGroup.Id,
            WorkTypeId = firstTestWorkType.Id);
        insert firstTestWorkTypeGroupMember;
        
        CDK_Vehicle__c firstTestVehicle = new CDK_Vehicle__c();
        firstTestVehicle.VIN__c = TEST_VIN_LABEL;
        insert firstTestVehicle;
        
        CDK_Vehicle_Ownership__c firstCDKVehicleOwnership = new CDK_Vehicle_Ownership__c();
        firstCDKVehicleOwnership.Status__c = STATUS_CURRENT_LABEL;
        firstCDKVehicleOwnership.Account__c = firstTestAccount.Id;
        firstCDKVehicleOwnership.CDK_Vehicle__c = firstTestVehicle.Id;
        insert firstCDKVehicleOwnership;
        
        DeclinedService__c firstDeclinedService = new DeclinedService__c(
            Account__c = firstTestAccount.Id, 
            Status__c = STATUS_DECLINED, 
            CDK_Vehicle__c = firstTestVehicle.Id);
        insert firstDeclinedService;
        
        
        List<OperatingHours> opHoursList = new List<OperatingHours>();
        OperatingHours firstTestOperatingHours = new OperatingHours(Name = FIRST_TEST_OPERATING_HOURS_NAME);
        firstTestOperatingHours.Timezone = 'America/Los_Angeles';
        CustomMetadataService mdtService = new CustomMetadataService();
        SalesTimeslotsSettings__mdt saslesMDT = mdtService.getOppHoursMDT('CDKTS', 'Showroom visit');
        firstTestOperatingHours.Id = saslesMDT.OperatingHoursId__c;
        opHoursList.add(firstTestOperatingHours);
        
        OperatingHours secondTestOperatingHours = new OperatingHours(Name = 'Second Test OpHours');
        firstTestOperatingHours.Timezone = 'America/Los_Angeles';
        opHoursList.add(secondTestOperatingHours);
        
        upsert opHoursList;
        
        List<Timeslot> tsList = new List<Timeslot>();
        Timeslot ts1 = new Timeslot(
            OperatingHoursId = secondTestOperatingHours.Id,
            StartTime = Time.newInstance(9, 0, 0, 0),
            EndTime = Time.newInstance(10, 0, 0, 0),
            DayOfWeek = 'Monday',
            Type = 'Normal'
        );
        tsList.add(ts1);
        Timeslot ts2 = new Timeslot(
            OperatingHoursId = secondTestOperatingHours.Id,
            StartTime = Time.newInstance(10, 0, 0, 0),
            EndTime = Time.newInstance(11, 0, 0, 0),
            DayOfWeek = 'Friday',
            Type = 'Normal'
        );
        tsList.add(ts2);
        insert tsList;
        
        ServiceTerritory firstTestServiceTerritory = new ServiceTerritory(
            isActive = true, 
            IsSalesTerritory__c = true,
            Name = FIRST_TEST_SERVICE_TERRITORY_HOURS_NAME,
            Dealer__c = TEST_DEALER_LABEL, 
            OperatingHoursId = firstTestOperatingHours.Id);
        insert firstTestServiceTerritory;
        
        ServiceTerritoryMember firstTestServiceTerritoryMember = new ServiceTerritoryMember(
            TerritoryType = 'P' ,
            EffectiveStartDate = Datetime.newInstance(2020, 7, 1),
            ServiceTerritoryId = firstTestServiceTerritory.Id, 
            ServiceResourceId = firstTestServiceResource.Id);
        insert firstTestServiceTerritoryMember;

        FSL__Scheduling_Policy__c firstTestSchedulingPolicy = new FSL__Scheduling_Policy__c(Name = 'Customer First');
        insert firstTestSchedulingPolicy;
        
        List<ServiceAppointment> saList = new List<ServiceAppointment>();
        String firstTestAccountPersonContactId = [SELECT PersonContactId 
                                                  FROM Account 
                                                  WHERE Id = :firstTestAccount.Id].PersonContactId;
        ServiceAppointment firstTestServiceAppointment = new ServiceAppointment(
           ParentRecordId = firstTestAccount.Id, ContactId = firstTestAccountPersonContactId, Status = 'Scheduled');
        DateTime today = DateTime.now();
        firstTestServiceAppointment.DealerId__c = TEST_DEALER_LABEL;
        firstTestServiceAppointment.SchedStartTime = today;
        firstTestServiceAppointment.SchedEndTime = today.addDays(30);
        firstTestServiceAppointment.ServiceTerritoryId = firstTestServiceTerritory.Id;
        firstTestServiceAppointment.RecordTypeId = appointmenTrecordTypeId;
        saList.add(firstTestServiceAppointment);
        
        ServiceAppointment secondTestServiceAppointment = new ServiceAppointment(
           ParentRecordId = firstTestOpportunity.Id, Status = 'Scheduled');
        secondTestServiceAppointment.DealerId__c = TEST_DEALER_LABEL;
        secondTestServiceAppointment.SchedStartTime = today;
        secondTestServiceAppointment.SchedEndTime = today.addDays(30);
        secondTestServiceAppointment.ServiceTerritoryId = firstTestServiceTerritory.Id;
        secondTestServiceAppointment.RecordTypeId = appointmenTrecordTypeId;
        saList.add(secondTestServiceAppointment);
        
        insert saList;
    }

    @isTest
    private static void getUserDetailsTest(){
        //given
        String userId = UserInfo.getUserId();
        //when
        //then
        User expected = [SELECT Id, Name 
                         FROM User 
                         WHERE Id = :userId 
                         LIMIT 1];
        User actual = SalesAppointmentBookingController.getUserDetails(userId);
        System.assertEquals(expected, actual);
    }
    
    @isTest
    private static void getSalesAppointmentTypesTest(){
        //given
        String expected = expectedSalesAppointmentTypesJSON;
        //when
        //then
        String actual = SalesAppointmentBookingController.getSalesAppointmentTypes();
        // System.assertEquals(expected, actual);
    }
    
    @isTest
    private static void getCurrentSObjectTypeTest(){
        //given
		String accountId = [SELECT Id 
                            FROM Account
                            WHERE FirstName = :FIRST_TEST_ACCOUNT_FIRST_NAME].Id;
        String actual = ACCOUNT_SOBJECT_TYPE_LABEL;
        //when
        //then
        String expected = SalesAppointmentBookingController.getCurrentSObjectType(accountId);
        System.assertEquals(expected, actual);
    }
    
    @isTest
    private static void getCurrentRecordInfoTest(){
        //given
        Opportunity expected = [SELECT Id, Dealer_Id__c, OwnerId,  Account.Name, Account.FirstName, Account.PersonEmail, Account.PersonContactId
                          		FROM Opportunity
                           		WHERE Dealer_Id__c = :TEST_DEALER_LABEL];
        String opproturnityId = expected.Id;
        //when
        //then
        Opportunity actual = SalesAppointmentBookingController.getCurrentRecordInfo(opproturnityId);
        System.assertEquals(expected, actual);
    }
    
    @isTest
    private static void getDealerInfoTest(){
        //given
        Account expected = [SELECT Id, BufferTime__c
                            FROM Account
                            WHERE Name = :DEALER_TEST_ACCOUNT_NAME];
        //when
        //then
        Account actual = SalesAppointmentBookingController.getDealerInfo(TEST_DEALER_LABEL);
        System.assertEquals(expected, actual);
    }
    
    @isTest
    private static void getSalesAppointmentInfoTest(){
        //given
        String accountId = [SELECT Id 
                            FROM Account 
                            WHERE FirstName = :FIRST_TEST_ACCOUNT_FIRST_NAME].Id;
        String personContactId = [SELECT PersonContactId 
                                  FROM Account 
                                  WHERE Id = :accountId].PersonContactId;
        SalesAppointmentBookingController.SalesAppointmentWrapper expectedObject = new SalesAppointmentBookingController.SalesAppointmentWrapper();
        ServiceAppointment appointment = [SELECT Id, ParentRecordId, DealerId__c,  EarliestStartTime, PickupStreet__c, 
          							  				PickupCity__c, PickupState__c, PickupZIP__c, SchedStartTime, SchedEndTime, DueDate, PickupAddressComments__c,
            						  				SalesAppointmentType__c, PrimaryResource__c, SecondaryResource__c 
                                			FROM ServiceAppointment 
                                			WHERE ParentRecordId = :accountId];
        String memberId = [SELECT Id 
                           FROM ServiceResource 
                           WHERE Name = :FIRST_TEST_SERVICE_RESOURCE].Id;
        //when
        expectedObject.Id = appointment.Id;
        expectedObject.ParentId = appointment.ParentRecordId;
        expectedObject.Dealer = appointment.DealerId__c;
        expectedObject.SalesAppointmentType = appointment.SalesAppointmentType__c;
        expectedObject.PrimaryResource = appointment.PrimaryResource__c;
        expectedObject.SecondaryResource = appointment.SecondaryResource__c;
        expectedObject.PreferredDate = appointment.SchedStartTime != null ? appointment.SchedStartTime.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString()) : appointment.EarliestStartTime.format('yyyy-MM-dd',  UserInfo.getTimeZone().toString());
        expectedObject.BookingStartTime = appointment.SchedStartTime != null ? appointment.SchedStartTime.format('HH:mm',  UserInfo.getTimeZone().toString()) : null;
        expectedObject.BookingEndTime = appointment.SchedEndTime != null ? appointment.SchedEndTime.format('HH:mm',  UserInfo.getTimeZone().toString()) : null;
        expectedObject.PickupStreet = appointment.PickupStreet__c;
        expectedObject.PickupCity = appointment.PickupCity__c;
        expectedObject.PickupState = appointment.PickupState__c;
        expectedObject.PickupZIP = appointment.PickupZIP__c;
        expectedObject.PickupAddressComments = appointment.PickupAddressComments__c;
        
        //then 
        String actual = SalesAppointmentBookingController.getSalesAppointmentInfo(appointment.Id);
        String expected = JSON.serialize(expectedObject);
        System.assertEquals(expected, actual);
    }
    
    @isTest
    private static void getDefaultServiceResourceTest(){
        //given
        String userId = UserInfo.getUserId();
        List<ServiceResource> expected = [SELECT Id FROM ServiceResource WHERE RelatedRecordId = :userId LIMIT 1];
        //when
        //then
        List<ServiceResource> actual = SalesAppointmentBookingController.getDefaultServiceResource(userId);
        System.assertEquals(expected, actual);
    }
    
    @isTest
    private static void getAvailableServiceResourceTest(){
        //given
        List<ServiceResource> expected = [SELECT Id, Name, DefaultAdvisorTime__c, Sales_Resource_Type__c, Dealer__c
                                          FROM ServiceResource
                                          WHERE Name = :FIRST_TEST_SERVICE_RESOURCE];
        //when
        //then
        List<ServiceResource> actual = SalesAppointmentBookingController.getAvailableServiceResource(TEST_DEALER_LABEL);
        System.assertEquals(expected, actual);
    }
    
    @isTest
    private static void createServiceAppointmentTest(){
        //given
        String parentRecordId = [SELECT Id 
                           		 FROM Account 
                            	 WHERE FirstName = :FIRST_TEST_ACCOUNT_FIRST_NAME].Id;
        //when
		SalesAppointmentBookingController.createServiceAppointment(parentRecordId, TEST_DEALER_LABEL);     
        //then
        Integer actualNumber = [SELECT COUNT()
                               	FROM ServiceAppointment
                               	WHERE ParentRecordId = :parentRecordId];
        System.assertEquals(2, actualNumber);
    }
    
    @isTest
    private static void getAppointmentTypeTimeslotsTest(){
        //given
        String expected = 'Timeslots for selected Sales Appointment Type not created';
        //when
        //then
        try{
          String actual = SalesAppointmentBookingController.getAppointmentTypeTimeslots('Test-Drive', TEST_DEALER_LABEL);  
        } catch(Exception e){
            String actual = e.getMessage();
            // System.assertEquals(expected, actual);
        }     
    }
    
    @isTest
    private static void getAppointmentTypeTimeslotsPositiveTest(){
        //given
        String expected = 'Timeslots for selected Sales Appointment Type not created';
        //when
        //then
        try{
          String actual = SalesAppointmentBookingController.getAppointmentTypeTimeslots('Showroom Visit', TEST_DEALER_LABEL);  
        } catch(Exception e){
            String actual = e.getMessage();
            // System.assertEquals(expected, actual);
        }     
    }
    
    @isTest
    private static void getAvailableTimeSlotsTest(){
    	//given
        String accountId = [SELECT Id 
                            FROM Account 
                            WHERE FirstName = :FIRST_TEST_ACCOUNT_FIRST_NAME].Id;
        String appointmentId = [SELECT Id
                              FROM ServiceAppointment 
                              WHERE ParentRecordId = :accountId].Id;
        String expected = '[]';
    	//when
    	//then
    	String actual = SalesAppointmentBookingController.getAvailableTimeSlots(appointmentId, '', TEST_DEALER_LABEL, 'Showroom Visit');
        System.assertEquals(expected, actual);
    }
    
   	@isTest
    private static void updateServiceAppointmentDateTest(){
        //give
        String accountId = [SELECT Id 
                            FROM Account 
                            WHERE FirstName = :FIRST_TEST_ACCOUNT_FIRST_NAME].Id;
        ServiceAppointment appointment = [SELECT Id, EarliestStartTime, DueDate
                              			  FROM ServiceAppointment 
                              			  WHERE ParentRecordId = :accountId];  
        String appointmentId = appointment.Id;
        //when
        SalesAppointmentBookingController.updateServiceAppointmentDate(appointmentId, '12/27/2222', '12/28/2222');
        //then
        ServiceAppointment actual = [SELECT Id, EarliestStartTime, DueDate
                                     FROM ServiceAppointment
                                     WHERE Id = :appointmentId];
        System.assertNotEquals(appointment, actual);
    }
    
    @isTest
    private static void scheduleServiceAppointmentTest(){
        //given
        String accountId = [SELECT Id 
                            FROM Account 
                            WHERE FirstName = :FIRST_TEST_ACCOUNT_FIRST_NAME].Id;
        String opportunityId = [SELECT Id 
                            FROM Opportunity 
                            WHERE AccountId = :accountId LIMIT 1].Id;
        ServiceAppointment appointment = [SELECT Id, EarliestStartTime, DueDate
                              			  FROM ServiceAppointment 
                              			  WHERE ParentRecordId = :opportunityId];
        String serviceId = [SELECT Id
                            FROM ServiceResource
                            WHERE Name = :FIRST_TEST_SERVICE_RESOURCE].Id;
		String salesAppointmentToScheduleJSON = '{"EmailRecepientsName":"Test 11",' +
        									   + '"Id":"' + appointment.Id + '",' +
        									   + '"Dealer":"CDKTS",' +
        									   + '"OptOutOfConfirmation":false,' +
        									   + '"PrimaryResource":"' + serviceId + '",' +
        									   + '"SecondaryResource":"' + serviceId + '",' +
        									   + '"SalesAppointmentType":"Showroom visit",' +
        									   + '"PreferredDate":"2020-08-11",' +
        									   + '"BookingStartTime":"07:00",' +
        									   + '"BookingEndTime":"08:00"}';        
        //when
        //then
        String expected = appointment.Id;
        String actual = SalesAppointmentBookingController.scheduleServiceAppointment(salesAppointmentToScheduleJSON , false);
        System.assertEquals(expected, actual);
    }
    
    @isTest 
    private static void getEmailTemplateTest(){
        //given
        String accountId = [SELECT Id 
                            FROM Account 
                            WHERE FirstName = :FIRST_TEST_ACCOUNT_FIRST_NAME].Id;
        String appointmentId = [SELECT Id 
                                FROM ServiceAppointment 
                                WHERE ParentRecordId = :accountId].Id;
        //when
        //then
        String actual =  SalesAppointmentBookingController.getEmailTemplate(appointmentId ,false);
        //System.assertEquals(expected, actual);
    }
    
    @isTest 
    private static void sendConfirmationEmailTest(){
        //given
        String accountId = [SELECT Id 
                            FROM Account 
                            WHERE FirstName = :FIRST_TEST_ACCOUNT_FIRST_NAME].Id;
        String appointmentId = [SELECT Id 
                                FROM ServiceAppointment 
                                WHERE ParentRecordId = :accountId].Id;  
        String firstTestAccountPersonContactId = [SELECT PersonContactId 
                                                  FROM Account 
                                                  WHERE Id = :accountId].PersonContactId;
        //when
        Test.StartTest();
        SalesAppointmentBookingController.sendConfirmationEmail(new List<String>{'test@test.com'}, appointmentId, firstTestAccountPersonContactId, false);
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        //then
        System.assertEquals(1, invocations, 'An email has not been sent');
    }
    
    @isTest 
    private static void deleteAppointmentTest(){
        //given
        String accountId = [SELECT Id 
                            FROM Account 
                            WHERE FirstName = :FIRST_TEST_ACCOUNT_FIRST_NAME].Id;
        String firstTestAccountPersonContactId = [SELECT PersonContactId 
                                                  FROM Account 
                                                  WHERE Id = :accountId].PersonContactId;
        String advisorId = [SELECT Id 
                            FROM ServiceResource 
                            WHERE Name = :FIRST_TEST_SERVICE_RESOURCE].Id;      
        ServiceAppointment testAppointment = new ServiceAppointment(
            ParentRecordId = accountId, 
            ContactId = firstTestAccountPersonContactId, 
            Status = 'Scheduled');
        insert testAppointment;
        //when
        SalesAppointmentBookingController.deleteAppointment(testAppointment.Id);
        //then
        List <ServiceAppointment> actual = [SELECT Id FROM ServiceAppointment WHERE Id = :testAppointment.Id];
        System.assert(actual.size() == 0);
    }

	@isTest
	private static void getDealerTimezoneShiftTest() {
        Integer dealerShift = SalesAppointmentBookingController.getDealerTimezoneShift(TEST_DEALER_LABEL);
    }    
}