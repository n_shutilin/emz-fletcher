@isTest
public with sharing class Test_ServiceAdvisorExtract {
    private static final String BODY = 
    '<?xml version=\'1.0\' encoding=\'UTF-8\'?>' +
    '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">' +
        '<S:Body>' +
            '<ns2:getHelpServiceAdvisorResponse xmlns:ns2="http://www.dmotorworks.com/pip-serviceappointment">' +
                '<return>' +
                    '<name>HEYA,GARY</name>' +
                    '<serviceAdvisor>4</serviceAdvisor>' +
                '</return>' +
                '<return>' +
                    '<name>CAR WASH</name>' +
                    '<serviceAdvisor>1202</serviceAdvisor>' +
                '</return>' +
                '<return>' +
                    '<name>MIYAMOTO,KYLE</name>' +
                    '<serviceAdvisor>1213</serviceAdvisor>' +
                '</return>' +
            '</ns2:getHelpServiceAdvisorResponse>' +
        '</S:Body>' +
    '</S:Envelope>';
	
    @testSetup
    private static void init(){
		CDK_Integration__c settings = new CDK_Integration__c();
		settings.Password__c = 'test';
        settings.Username__c = 'test';
		insert settings;   
        
        RecordType dealerRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Dealer' and SObjectType = 'Account'];
        Account testAccount = new Account();
        testAccount.Name = 'testName';
        testAccount.CDK_Dealer_Code__c = 'test';
        testAccount.RecordTypeId = dealerRecordType.Id;
        insert testAccount;
    }
    
    @isTest
    private static void getServiceAdvisorsSchedulerTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(BODY));
        ServiceAdviserRetrieverSchedulable sh1 = new ServiceAdviserRetrieverSchedulable();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Extract', sch, sh1);
        Test.stopTest();
    }
}