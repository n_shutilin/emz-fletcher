@isTest
public class Test_ServiceAppointmentButtonController {
    @testSetup
    private static void setup() {
        Id recTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        Account customerAcc = new Account(
            Name = 'CDKTS Test Account',
            RecordTypeId = recTypeId,
            CDK_Dealer_Code__c = '3PA17867',
            Dealer_Code__c = 'CDKTS'
        );
        insert customerAcc;

        CDK_Vehicle__c vehicle = new CDK_Vehicle__c(
            Model__c = 'xxxxxx',
            Make__c = 'Ford',
            Year__c = '2010',
            VIN__c = '12341231234123',
            VehID__c = '123123'
        );
        insert vehicle;

        ServiceAppointment testSrvAppointment1 = new ServiceAppointment(
            ParentRecordId = customerAcc.Id,
            CDK_Vehicle__c = vehicle.Id,
            AppointmentDate__c = '2020-09-03', 
            AppointmentTime__c = '08:00:00', 
            PromiseDate__c = '2020-09-03',
            PromiseTime__c = '08:00:00',
            SAName__c = 'TestUser1',
            SANumber__c = '123',
            DealerId__c = 'CDKTS',
            Status = 'Scheduled',
            CDK_Checksum__c = '5875182',
            ApptID__c = '1923917474369'
        ); 
        insert testSrvAppointment1;
    }
    @isTest
    private static void sendAppointmentIntoCDK() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock());
        ServiceAppointment testSrvAppointment1 = [SELECT Id FROM ServiceAppointment WHERE SAName__c = 'TestUser1' LIMIT 1];
        ServiceAppointmentService.sendAppointmentIntoCDK(testSrvAppointment1.Id, 'insert');
        Test.stopTest();
    }
}