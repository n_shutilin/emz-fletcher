@isTest
private class Test_ServiceSalesExtract {
    private static final String RESPONSE_BODY = '<ServiceSalesClosed xmlns="http://www.dmotorworks.com/pip-extract-service-sales-closed">' + 
            + '<ServiceSalesClosed>' + 
                + '<ErrorLevel>0</ErrorLevel>' + 
                + '<ErrorMessage />' + 
                + '<Model>C300W</Model>' + 
                + '<Name1>YAO,XI</Name1>' + 
                + '<Name2 />' + 
                + '<OpenDate>2020-09-15</OpenDate>' + 
                + '<OpenedTime>10:03:07</OpenedTime>' + 
                + '<PromisedDate>2020-09-18</PromisedDate>' + 
                + '<PromisedTime>12:30:00</PromisedTime>' + 
                + '<ROComment1 />' + 
                + '<ROComment2 />' + 
                + '<ROComment3 />' + 
                + '<ROComment4 />' + 
                + '<ROComment5 />' + 
                + '<ROComment6 />' + 
                + '<ROComment7 />' + 
                + '<ROComment8 />' + 
                + '<ROComment9 />' + 
                + '<RONumber>968668</RONumber>' + 
                + '<ROStatusCode>C98</ROStatusCode>' + 
                + '<ROStatusCodeDesc>CLOSED</ROStatusCodeDesc>' + 
                + '<ContactEmailAddress />' + 
                + '<ContactPhoneNumber>9497671847</ContactPhoneNumber>' + 
                + '<CustNo>1087739</CustNo>' + 
                + '<HostItemID>GU165469*968668</HostItemID>' + 
                + '<CityStateZip>IRVINE, CA 92602-1847</CityStateZip>' + 
                + '<CloseDate>2020-09-19</CloseDate>' + 
                + '<VIN>55SWF4JB5GU165469</VIN>' + 
                + '<VehID>GU165469</VehID>' + 
                + '<Year>2016</Year>' + 
                + '<ServiceAdvisor>743</ServiceAdvisor>' + 
                + '<Make>MB</Make>' + 
                + '<Mileage>16781</Mileage>' + 
                + '<MileageOut>16876</MileageOut>' + 
                + '<Address>64 TUNDRA</Address>' + 
                + '<LaborSaleCustomerPay>775.44</LaborSaleCustomerPay>' + 
                + '<LaborSaleInternal>0.00</LaborSaleInternal>' + 
                + '<LaborSaleWarranty>0.00</LaborSaleWarranty>' + 
                + '<PartsSaleCustomerPay>616.80</PartsSaleCustomerPay>' + 
                + '<PartsSaleInternal>0.00</PartsSaleInternal>' + 
                + '<PartsSaleWarranty>0.00</PartsSaleWarranty>' + 
                + '<PriorityValue>3676</PriorityValue>' + 
            + '</ServiceSalesClosed>' + 
            + '<ServiceSalesDetailsClosed>' + 
                + '<ServiceRequest>"B" SERVICE-FLEXIBLE SERVICE</ServiceRequest>' + 
                + '<LineCode>A</LineCode>' + 
                + '<LopSeqNo>1</LopSeqNo>' + 
                + '<LaborSale>374.44</LaborSale>' + 
                + '<LaborType>CMM</LaborType>' + 
                + '<PartsSale>170.40</PartsSale>' + 
                + '<ActualHours>0.82</ActualHours>' + 
                + '<ErrorLevel>0</ErrorLevel>' + 
                + '<ErrorMessage />' + 
                + '<OpCode>1002</OpCode>' + 
                + '<OpCodeDescription>PERFORM FLETCHER JONES MOTORCARS STANDARD "B" SERVICE</OpCodeDescription>' + 
                + '<RONumber>968668</RONumber>' + 
                + '<HostItemID>GU165469*968668*1*0</HostItemID>' + 
            + '</ServiceSalesDetailsClosed>' + 
            + '<ServiceSalesDetailsClosed>' + 
                + '<ServiceRequest>PERFORMANCE OIL SERVICE - EFFECTIVE CLEANERS ARE PLACED INTO THE ENGINE TO BREAKDOWN HARMFUL SLUDGE &amp; VARNISH. THEN THE OIL &amp; FILTER IS REPLACED &amp; A FUEL SUPPLEMENT IS ADDED TO THE FUEL TANK FOR ADDITIONAL DEPOSIT CLEAN UP &amp; CONTROL</ServiceRequest>' + 
                + '<LineCode>B</LineCode>' + 
                + '<LopSeqNo>2</LopSeqNo>' + 
                + '<LaborSale>64.50</LaborSale>' + 
                + '<LaborType>CMM</LaborType>' + 
                + '<PartsSale>32.50</PartsSale>' + 
                + '<ActualHours>0.45</ActualHours>' + 
                + '<ErrorLevel>0</ErrorLevel>' + 
                + '<ErrorMessage />' + 
                + '<OpCode>00-440</OpCode>' + 
                + '<OpCodeDescription>PERFORMANCE OIL SERVICE - EFFECTIVE CLEANERS ARE PLACED INTO THE ENGINE TO BREAKDOWN HARMFUL SLUDGE &amp; VARNISH. THEN THE OIL &amp; FILTER IS REPLACED &amp; A FUEL SUPPLEMENT IS ADDED TO THE FUEL TANK FOR ADDITIONAL DEPOSIT CLEAN UP &amp; CONTROL</OpCodeDescription>' + 
                + '<RONumber>968668</RONumber>' + 
                + '<HostItemID>GU165469*968668*2*0</HostItemID>' + 
            + '</ServiceSalesDetailsClosed>' + 
            + '<ErrorCode>0</ErrorCode>' + 
            + '<ErrorMessage />' + 
        + '</ServiceSalesClosed>';
    @TestSetup
    private static void init() {
        CDK_Integration__c settings = new CDK_Integration__c(
            Password__c = '111111111',
            Username__c = 'testUser'
        );
        insert settings;
        
        //List<Account> accList = new List<Account>();
        Id recTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        //Id personAccRecTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        Account dealerAcc = new Account(
            Name = 'CDKTS Test Account',
            RecordTypeId = recTypeId,
            CDK_Dealer_Code__c = '3PA17867',
            Dealer_Code__c = 'CDKTS'
        );
        insert dealerAcc;
        //accList.add(dealerAcc);
    }
    /*
    @isTest
    private static void testScheduledJob() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(RESPONSE_BODY));
        String jobId = System.schedule('Account Updates', '0 0 0 28 2 ? 2022', new ServiceSalesExtractSchedulable());
        Test.stopTest();
    }*/
    
    @isTest
    private static void testServiceSalesExtractWithDateRange() {
        String dealerId = [SELECT Id FROM Account WHERE Name = 'CDKTS Test Account'].Id;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(RESPONSE_BODY));
        ServiceSalesClosedBatchable batch = new ServiceSalesClosedBatchable('01/01/2020','01/02/2020', dealerId);
		Database.executeBatch(batch,1);
        Test.stopTest();
    }
    
    @isTest
    private static void testServiceSalesExtractBulk() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new DMotorWorksApiMock(RESPONSE_BODY));
        ServiceSalesClosedBatchable batch = new ServiceSalesClosedBatchable();
		Database.executeBatch(batch,1);
        Test.stopTest();
    }
}