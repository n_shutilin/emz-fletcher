@isTest
public class Test_TaskTriggerHandler {
    @TestSetup
    static void makeData(){
        RecordType personAccountRecordType = [
            SELECT Id
            FROM RecordType
            WHERE Name = 'Person Account'
            AND SobjectType = 'Account'
        ];

        Account testAccount = new Account(
            FirstName = 'Test000003',
            LastName = 'Account',
            RecordTypeId = personAccountRecordType.Id,
            PersonEmail = 'testAccount@testTaskTrigger.com'
        );
        insert testAccount;

        String personContactId = [SELECT PersonContactId FROM Account WHERE Id = :testAccount.Id].PersonContactId;

        Opportunity opportunity = new Opportunity(
            AccountId = testAccount.Id,
            Name = 'Test Opportunity 99999999999',
            StageName = 'Initial Interest',
            CloseDate = Date.today().addDays(10)
        );
        insert opportunity;
    }
    @isTest
    static void testTaskInsert() {
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        insert new Task(
            Subject = 'its a subject', Priority = 'High', Status = 'Completed', 
            WhatId = opportunity.Id, OwnerId = UserInfo.getUserId(), Activity_Type__c = 'Email'
        );
    }
}