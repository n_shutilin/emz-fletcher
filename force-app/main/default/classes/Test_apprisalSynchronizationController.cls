@isTest
public class Test_apprisalSynchronizationController {
	@testSetup
    private static void init(){
     	Trade_In__c tradeIn = new Trade_In__c();
        tradeIn.VIN__c = 'testV';
        insert tradeIn;
        
        CDK_Integration__c settings = new CDK_Integration__c();
        settings.Inventory_Plus_Api_Key__c = 'test';
        insert settings;
    }
    
    @isTest
    private static void getTradeTest(){
        //given
        String tradeId = [SELECT Id
                          FROM Trade_In__c
                          WHERE VIN__C = 'testV'].Id;
        //when
        Trade_In__c actual = apprisalSynchronizationController.getTrade(tradeId);
        //then
    }
    
    @isTest
    private static void sendTradeTest(){
        //given
        String tradeId = [SELECT Id
                          FROM Trade_In__c
                          WHERE VIN__C = 'testV'].Id;
        //when
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestAppraisalsMock());
        apprisalSynchronizationController.sendTrade(tradeId);
        Test.stopTest();
        //then
    }
    
    @isTest
    private static void retrieveTradeTest(){
        //given
        String tradeId = [SELECT Id
                          FROM Trade_In__c
                          WHERE VIN__C = 'testV'].Id;
        //when
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new RestAppraisalsMock());
        apprisalSynchronizationController.retrieveTrade(tradeId);
        Test.stopTest();
        //then        
    }
}