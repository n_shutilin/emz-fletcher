public with sharing class TimeSlots {
	
    public class RowsWrapperClass {
    public Integer index {get;set;} 
	public String dayOfWeek {get;set;} 
	public String fromTime {get;set;} 
	public String toTime {get;set;}
	public Integer increment {get;set;}
    }
    
	@AuraEnabled
	public static void saveTimeSlots(String json, Id recordId, String startDate, String endDate) {
        
        Integer offset = UserInfo.getTimezone().getOffset(system.now());
        system.debug(offset);
        
        /* Build Start Date/Time */
        list<String> sd = startDate.split('-');
        list<integer> StartDateTime = new list<integer>();
        StartDateTime.add(Integer.valueOf(sd[0]));
    	StartDateTime.add(Integer.valueOf(sd[1]));
    	StartDateTime.add(Integer.valueOf(sd[2].left(2)));
        
        String sta = sd[2].substringBetween('T','Z');
        list<String> time1 = sta.split(':');
        StartDateTime.add(Integer.valueOf(time1[0]));
        StartDateTime.add(Integer.valueOf(time1[1]));
        StartDateTime.add(Integer.valueOf(time1[2]));
        
        DateTime sdt = Datetime.newInstance(StartDateTime[0],
                                        	StartDateTime[1],
                                        	StartDateTime[2],
                                       	 	StartDateTime[3],
                                        	StartDateTime[4],
                                        	StartDateTime[5]);
        
        
        /* Build End Date/Time */
        list<String> ed = endDate.split('-');
        list<integer> EndDateTime = new list<integer>();
        EndDateTime.add(Integer.valueOf(ed[0]));
    	EndDateTime.add(Integer.valueOf(ed[1]));
    	EndDateTime.add(Integer.valueOf(ed[2].left(2)));
        
        String et = ed[2].substringBetween('T','.');
        list<String> time2 = et.split(':');
        EndDateTime.add(Integer.valueOf(time2[0]));
        EndDateTime.add(Integer.valueOf(time2[1]));
        EndDateTime.add(Integer.valueOf(time2[2]));
        
        DateTime edt = Datetime.newInstance(EndDateTime[0],
                                        	EndDateTime[1],
                                        	EndDateTime[2],
                                       	 	EndDateTime[3],
                                        	EndDateTime[4],
                                        	EndDateTime[5]);
        system.debug(edt);
        system.debug(sdt);
        
        /*Get Service Resource*/
        List<ServiceResource> sr = [Select Dealer__c FROM ServiceResource where id = :recordId];
        
        /*Find Existing Service Territory*/
        List<ServiceTerritory> st = [Select id, Name, Dealer__c, IsActive  FROM ServiceTerritory WHERE Dealer__c = :sr[0].Dealer__c and IsActive = True];
        
        /*Check if a service territory was found*/
        if(st.size() == 0){
         return;   
        }
        
        /*Check for existing service territroy member*/
        List<ServiceTerritoryMember> existingStm = [Select ServiceResourceId , ServiceTerritoryId  from ServiceTerritoryMember where ServiceResourceId = :recordId and ServiceTerritoryId  = :st[0].id];
        
        /*Check if service resource already belongs to this territroy*/
        if(existingStm.size() > 0){
         return;   
        }
        
        /*Deserialize Rows*/
		List<RowsWrapperClass> ObjList = new List<RowsWrapperClass>();
        ObjList = (List<RowsWrapperClass>)System.JSON.deserialize(json,List<RowsWrapperClass>.class);
		
        /*Create Operating Hours*/
        OperatingHours oh = new OperatingHours();
        oh.Name = 'Test Operating Hours';
        oh.Dealer_Code__c = sr[0].Dealer__c;
        oh.TimeZone = 'America/Los_Angeles';
        insert oh;
        
        /*Create Service Territory Member*/
        ServiceTerritoryMember stm = new ServiceTerritoryMember();
        stm.ServiceResourceId = sr[0].Id;
        stm.TerritoryType = 'P';
        stm.EffectiveStartDate = DateTime.valueOfGMT(String.valueOf(sdt));
        stm.EffectiveEndDate = DateTime.valueOfGMT(String.valueOf(edt));
        stm.OperatingHoursId = oh.id;
        stm.ServiceTerritoryId = st[0].id;
        insert stm;
        
        /*Loop over rows*/
        List<TimeSlot> t = new List<TimeSlot>();
        for(RowsWrapperClass r: ObjList){
            system.debug(r);
            String[] fromTimeSplit = r.fromTime.split(':');
            String[] toTimeSplit = r.toTime.split(':');
            Time fromTime = Time.newInstance( Integer.valueOf(fromTimeSplit[0])
                                             ,Integer.valueOf(fromTimeSplit[1])
                                             ,0,0);
            Time toTime = Time.newInstance( Integer.valueOf(toTimeSplit[0])
                                             ,Integer.valueOf(toTimeSplit[1])
                                             ,0,0);
            system.debug(fromTime);
            system.debug(toTime);

            Integer loops = ((( toTime.hour() - fromTime.hour())*60)/r.increment);
            /*Build time slots*/
            for(Integer i=0; i < loops; i++){
                TimeSlot ts = new TimeSlot();
                if(i == 0){
                ts.OperatingHoursId = oh.id;
                ts.StartTime = fromTime;
                ts.EndTime = fromTime.addMinutes(r.increment);
                ts.DayOfWeek = r.dayOfWeek;
                ts.type = 'Normal';
                }else{
                ts.OperatingHoursId = oh.id;
                ts.StartTime = fromTime.addMinutes((r.increment*i));
                ts.EndTime = fromTime.addMinutes(((r.increment*i)+r.increment));
                ts.DayOfWeek = r.dayOfWeek;
                ts.type = 'Normal';    
                }
                t.add(ts);
            }
            
        }
        try{
            insert t;
        }catch(DmlException e){
            system.debug(e);
        }
	}

}