global class UpSystemAutomationJob implements Schedulable{

    global void execute(SchedulableContext sc) {
        for(String dealer : getDealerList()) {
            Database.executeBatch(new QueueCreationBatch(dealer));
        }
    }

    private List<String> getDealerList() {
        Schema.DescribeFieldResult fieldDescribe = ServiceTerritory.Dealer__c.getDescribe();
        Schema.sObjectField field = fieldDescribe.getSObjectField();
        List<String> dealerList = new List<String>();
        for(PicklistEntry entry : field.getDescribe().getPicklistValues()) {
            dealerList.add(entry.getValue());
        }
        return dealerList;
    }
}