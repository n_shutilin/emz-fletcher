public with sharing class UpSystemCustomLookupController {

    private static final String PEROSN_ACCOUNT_RECORD_TYPE_NAME = 'Person Account';
    private static final String SOBJECT_TYPE_ACCOUNT_LABEL = 'Account';

    public class SearchResult {
        @AuraEnabled public String id;
        @AuraEnabled public String name;

        public SearchResult(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }


    @AuraEnabled
    public static List<SearchResult> search(String searchString, String dealer, String objectName){
        switch on objectName {
            when 'Account'{
                return accountSearch(searchString, dealer);
            }
            when else {
                return opportunitySearch(searchString, dealer);
            }
        }

    }

    public static List<SearchResult> accountSearch(String searchString, String dealer) {
        String recordTypeId = getPersonAccountRecordTypeId();
        List<SearchResult> resultList = new List<SearchResult>();
        searchString += '*';

        String query = 'FIND {' + searchString + '*} ' +
            'IN ALL FIELDS ' +
            'RETURNING ' +
            'Account (Id, Name WHERE RecordTypeId = \''+ recordTypeId + '\' AND Dealer__c = \''+ dealer + '\') ' +
            'LIMIT 6';
        List<List<SObject>> searchResults = Search.query(query);

        List<SObject> objects = (List<SObject>) searchResults[0];
        for (SObject obj : objects) {
            resultList.add(
                new SearchResult(
                    (String)obj.get('Id'),
                    (String)obj.get('Name')
                )
            );
        }
        return resultList;
    }

    public static List<SearchResult> opportunitySearch(String searchString, String dealer) {
        List<SearchResult> resultList = new List<SearchResult>();
        searchString += '*';

        String query = 'FIND {' + searchString + '*} ' +
            'IN ALL FIELDS ' +
            'RETURNING ' +
            'Opportunity (Id, Name  WHERE Dealer_ID__c = \''+ dealer + '\') ' +
            'LIMIT 6';
        List<List<SObject>> searchResults = Search.query(query);

        List<SObject> objects = (List<SObject>) searchResults[0];
        for (SObject obj : objects) {
            resultList.add(
                new SearchResult(
                    (String)obj.get('Id'),
                    (String)obj.get('Name')
                )
            );
        }
        return resultList;
    }

    private static String getPersonAccountRecordTypeId(){
        RecordType personAccountRecordType = [SELECT Id
                                              FROM RecordType 
                                              WHERE Name = :PEROSN_ACCOUNT_RECORD_TYPE_NAME 
                                              AND SObjectType = :SOBJECT_TYPE_ACCOUNT_LABEL];
        return personAccountRecordType.Id;
    }
}