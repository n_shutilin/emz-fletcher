public with sharing class UpSystemTestDataProvider {

    private static final String SOBJECT_TYPE_ACCOUNT_LABEL = 'Account';
    private static final String TEST_DEALER_CODE = 'test';
    private static final String TEST_LAST_NAME = 'test';
    private static final String FIRST_TEST_ACCOUNT_NAME = 'firstTestAccount';
    private static final String SECOND_TEST_ACCOUNT_NAME = 'secondTestAccount';
    private static final String FIRST_DEALER_TEST_NAME = 'firstTestDealer';
    private static final String DEVELOPER_NAME_LABEL = 'Dealer';
    private static final String PEROSN_ACCOUNT_RECORD_TYPE_NAME = 'Person Account';
    private static final String STATUS_CURRENT_LABEL = 'Current';
    private static final String TEST_VIN_LABEL = 'testVIN';
    private static final String TEST_DEALER_LABEL = 'AUDFJ';
    private static final String FIRST_TEST_SERVICE_RESOURCE = 'firstTestServiceResource';
    private static final String TEST_INPUT_NEW_VEHICLE = '{"Year__c":"1999","Make__c":"Alfa Romeo","Model__c":"testmodel","VIN__c":"123pladpvkspodkfo"}';
    private static final String FIRST_TEST_WORK_TYPE = 'firstTestWorkType';
    private static final String STANDARD_YEAR_MONTH_DAY_FORMAT = 'yyyy-MM-dd';
    private static final String FIRST_TEST_OPERATING_HOURS_NAME = 'firstTestOperatinHours';
    private static final String FIRST_TEST_SERVICE_TERRITORY_HOURS_NAME = 'firstTestServiceTerritory';
    private static final String STATUS_DECLINED = 'Declined';
    private static final String STATUS_REMOVED = 'Removed';
    private final static String CONFIRMATION_EMAIL_TEMPLATE_NAME = 'Service_Appointment_Booking_Confirmation';
    private static final Integer INDEX_FIRST = 0;
    private static final Integer TEST_LOANER_CAPACITY = 1234;
    private static final Integer TEST_DURATION_FOR_LOANER = 1234;
    private static final String TEST_QUEUE_NAME = 'Test Queue';

    private String dealer;

    public UpSystemTestDataProvider(String dealer) {
        this.dealer = dealer;
        init();
    }

    private void init(){
        String accountId = createTestAccount('FIRST');
        String resourceId = createTestResource(accountId, 'FIRST');
        String operatingHoursId = createOpperatinHours();
        createTimeSlots(operatingHoursId);
        String terrytoryId = createServiceTerrytory(operatingHoursId);
        String queueId = createTestQueue(terrytoryId);
        String stateId = createState(queueId, resourceId);
        String secondState = createSecondState(queueId, resourceId);
        String personContactId = getPersonContactId(accountId);
        String appointmentId = createAppointment(terrytoryId, personContactId, accountId);
        String memberId = createMember(resourceId, terrytoryId);
        String assigneResource = createAssignedResource(appointmentId, resourceId);
    }

    private String getPersonContactId(String accountId) {
        String firstTestAccountPersonContactId = [SELECT PersonContactId 
                                                FROM Account 
                                                WHERE Id = :accountId].PersonContactId;
        return firstTestAccountPersonContactId;                        
    }

    private String getAppointmentRecordTypeId() {
        Id appointmenTrecordTypeId = [SELECT Id 
                                      FROM RecordType 
                                      WHERE DeveloperName = 'VehicleServiceAppointment' 
                                      AND sObjectType = 'ServiceAppointment'].Id;   
        return appointmenTrecordTypeId;
    }

    private String getPersonAccountRecordTypeId(){
        RecordType personAccountRecordType = [SELECT Id 
                                              FROM RecordType 
                                              WHERE Name = :PEROSN_ACCOUNT_RECORD_TYPE_NAME 
                                              AND SObjectType = :SOBJECT_TYPE_ACCOUNT_LABEL];
        return personAccountRecordType.Id;
    }

    private String createTestQueue(String testTerrytoryId) {
        Queue__c testQueue = new Queue__c(
            isActive__c = true,
            Name = TEST_QUEUE_NAME,
            Dealer__c = this.dealer,
            Start_Time__c = Time.newInstance(0,0,0,0),
            End_Time__c = Time.newInstance(18, 30, 2, 20),
            Last_Rows_Data_Update__c = DateTime.now(),
            Service_Territory_Id__c = testTerrytoryId);
        insert testQueue;
        return testQueue.Id;
    }

    private String createServiceTerrytory(String operatingHoursId) {
        ServiceTerritory firstTestServiceTerritory = new ServiceTerritory(
            isActive = true, 
            Name = FIRST_TEST_SERVICE_TERRITORY_HOURS_NAME,
            Dealer__c = this.dealer, 
            IsSalesTerritory__c = false,
            OperatingHoursId = operatingHoursId);
        insert firstTestServiceTerritory;
        return firstTestServiceTerritory.Id;
    }

    private String createMember(String resource, String territory) {
        ServiceTerritoryMember firstTestServiceTerritoryMember = new ServiceTerritoryMember(
            TerritoryType = 'P' ,
            EffectiveStartDate = Datetime.newInstance(2020, 7, 1),
            ServiceTerritoryId = territory, 
            ServiceResourceId = resource);
        insert firstTestServiceTerritoryMember;
        return firstTestServiceTerritoryMember.Id;
    }

    private static String createAssignedResource(String appointmentId, String resourceId) {
        AssignedResource assignedResource = new AssignedResource(ServiceAppointmentId = appointmentId , 
                                                                 ServiceResourceId = resourceId);
        insert assignedResource;
        return assignedResource.Id;
    }

    private String createOpperatinHours() {
        OperatingHours firstTestOperatingHours = new OperatingHours(Name = FIRST_TEST_OPERATING_HOURS_NAME, TimeZone='America/Los_Angeles');
        insert firstTestOperatingHours;
        return firstTestOperatingHours.Id; 
    }

    private void createTimeSlots(String OpperatinHoursId) {
        List<Timeslot> timeslots = new List<Timeslot>();
        TimeSlot mondayTestTimeSlot = new TimeSlot(
            DayOfWeek = 'Monday', 
            OperatingHoursId = OpperatinHoursId, 
            StartTime = Time.newInstance(0,0,0,0), 
            EndTime = Time.newInstance(12,0,0,0));
        timeslots.add(mondayTestTimeSlot);
        
        TimeSlot tuesdayTestTimeSlot = new TimeSlot(
            DayOfWeek = 'Tuesday', 
            OperatingHoursId = OpperatinHoursId, 
            StartTime = Time.newInstance(0,0,0,0), 
            EndTime = Time.newInstance(12,0,0,0));
        timeslots.add(tuesdayTestTimeSlot);

        TimeSlot wednesdayTestTimeSlot = new TimeSlot(
            DayOfWeek = 'Wednesday', 
            OperatingHoursId = OpperatinHoursId, 
            StartTime = Time.newInstance(0,0,0,0), 
            EndTime = Time.newInstance(12,0,0,0));
        timeslots.add(wednesdayTestTimeSlot);    
            
        TimeSlot thursdayTestTimeSlot = new TimeSlot(
            DayOfWeek = 'Thursday', 
            OperatingHoursId = OpperatinHoursId, 
            StartTime = Time.newInstance(0,0,0,0), 
            EndTime = Time.newInstance(12,0,0,0));
        timeslots.add(thursdayTestTimeSlot);

        TimeSlot fridayTestTimeSlot = new TimeSlot(
            DayOfWeek = 'Friday', 
            OperatingHoursId = OpperatinHoursId, 
            StartTime = Time.newInstance(0,0,0,0), 
            EndTime = Time.newInstance(12,0,0,0));
        timeslots.add(fridayTestTimeSlot);            

        TimeSlot saturdayTestTimeSlot = new TimeSlot(
            DayOfWeek = 'Saturday', 
            OperatingHoursId = OpperatinHoursId,  
            StartTime = Time.newInstance(0,0,0,0), 
            EndTime = Time.newInstance(12,0,0,0));
        timeslots.add(saturdayTestTimeSlot);            

        TimeSlot sundayTestTimeSlot = new TimeSlot(
            DayOfWeek = 'Sunday', 
            OperatingHoursId = OpperatinHoursId, 
            StartTime = Time.newInstance(0,0,0,0), 
            EndTime = Time.newInstance(12,0,0,0));
        timeslots.add(sundayTestTimeSlot); 
        
        insert timeslots;
    }

    private String createTestResource(String accountId, String name) {
        ServiceResource firstTestServiceResource = new ServiceResource(
            isActive = true, 
            Name = name,
            AccountId = accountId,
            Dealer__c = this.dealer,
            RelatedRecordId = UserInfo.getUserId());
        insert firstTestServiceResource;
        return firstTestServiceResource.Id;
    }

    private String createTestAccount(String name){
        Account firstTestAccount = new Account(
            FirstName = name,
            LastName = name,
            RecordTypeId = getPersonAccountRecordTypeId(),
            PersonEmail = 'test@test.com');
        insert firstTestAccount;
        return firstTestAccount.Id;
    }

    private String createState(String queueId, String resourceId) {
        Resource_State_In_Queue__c resource = new Resource_State_In_Queue__c(
            Service_Resource_Id__c = resourceId,
            Position__c = 1,
            Queue_Id__c = queueId,
            Is_Actual_State__c = true,
            Up_Time__c = 0,
            In_Queue_Up_Time__c = 180,
            Start_Time__c = Time.newInstance(0,0,0,0)
        );
        insert resource;
        return resource.Id;
    }

    private String createSecondState(String queueId, String resourceId) {
        Resource_State_In_Queue__c resource = new Resource_State_In_Queue__c(
            Service_Resource_Id__c = resourceId,
            Position__c = 2,
            Queue_Id__c = queueId,
            Is_Actual_State__c = true,
            Up_Time__c = 180,
            In_Queue_Up_Time__c = 180,
            Start_Time__c = Time.newInstance(0,0,0,0)
        );
        insert resource;
        return resource.Id;
    }

    private String createAppointment(String serviceTerritoryId, String testAccountPersonContactId, String testAccountId) {
        DateTime today = DateTime.now();
        ServiceAppointment firstTestServiceAppointment = new ServiceAppointment(
            ParentRecordId = testAccountId, 
            ContactId = testAccountPersonContactId, 
            Status = 'Scheduled',
            Transportation_Notes_New__c = 'Test',
            SchedStartTime = today,
            SchedEndTime = today.addDays(30),
            ServiceTerritoryId = serviceTerritoryId,
            DealerId__c = this.dealer,
            RecordTypeId = getAppointmentRecordTypeId());
        insert firstTestServiceAppointment;
        return firstTestServiceAppointment.Id;
    }
}