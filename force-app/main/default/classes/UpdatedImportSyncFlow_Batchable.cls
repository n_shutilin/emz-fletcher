global class UpdatedImportSyncFlow_Batchable implements Database.Batchable<sObject>
{
    global final String Query;
    
  global UpdatedImportSyncFlow_Batchable (String q)
  {
    Query = q;
  }
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<NBMBN_IMPORT__c> scope)
   {
       List<NBMBN_IMPORT__c> imp = new List<NBMBN_IMPORT__c>();
       for(NBMBN_IMPORT__c impObj : scope)
       {       

               impObj.Sync_Flow_Launcher__c = TRUE;
               imp.add(impObj);       
             
       
       }
    if(imp.size() > 0)
      update imp;
    }
  global void finish(Database.BatchableContext BC){
  }
}