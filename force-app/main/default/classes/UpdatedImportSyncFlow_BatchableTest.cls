@isTest
private class UpdatedImportSyncFlow_BatchableTest
{
    private static testMethod void myTestMethod(){
    
    Account acc = New Account();
    acc.Name = 'Test Account';
    insert acc;
    
    Opportunity opp = New Opportunity();
    opp.Name = 'Test Opp';
    opp.AccountId = acc.Id;
    opp.CloseDate = system.today() ;
    opp.StageName = 'Registered Sale';
    insert opp;
    
        NBMBN_IMPORT__c rep = new NBMBN_IMPORT__c( Sync_Flow_Launcher__c = False,opportunity__c = opp.id);
        insert rep;
        Datetime yesterday = Datetime.now().addDays(-1);
        Test.setCreatedDate(rep.Id, yesterday); 
        
         Test.startTest();
        
            String Query = 'Select imp.Id, imp.Sync_Flow_Launcher__c '
                 +  'From NBMBN_IMPORT__c imp where Opportunity__c != NULL and CreatedDate !=Today and LastModifiedDate = Today';
        
        UpdatedImportSyncFlow_Batchable ClassObj = new UpdatedImportSyncFlow_Batchable(Query);
        database.executeBatch(ClassObj, 1);
        Test.stopTest();}
        
            private static testMethod void myUpdatedImportSyncFlow_BatchableTest()
    {
        Test.startTest();
            Datetime dt = Datetime.now().addMinutes(1);
            String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
            UpdatedImportSyncFlow_Schedulable ClassObj = new UpdatedImportSyncFlow_Schedulable();
            system.schedule('Test Method', CRON_EXP, ClassObj);   
        Test.stopTest();
    }
        
             

    }