global class UpdatedImportSyncFlow_Schedulable implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
            String Query = 'Select imp.Id, imp.Sync_Flow_Launcher__c '
                 +  'From NBMBN_IMPORT__c imp where Sync_Flow_Launcher__c = false and Opportunity__c != NULL and CreatedDate !=Today and LastModifiedDate = Today';
        
        UpdatedImportSyncFlow_Batchable ClassObj = new UpdatedImportSyncFlow_Batchable(Query);
        database.executeBatch(ClassObj, 1);
    }
    
    }