global class VehDeclineFlowLauncher_Batchable implements Database.Batchable<sObject>
{
    global final String Query;
    
  global VehDeclineFlowLauncher_Batchable (String q)
  {
    Query = q;
  }
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Multi_Point_Inspection_XTMPI__c> scope)
   {
       List<Multi_Point_Inspection_XTMPI__c> ros = new List<Multi_Point_Inspection_XTMPI__c>();
       for(Multi_Point_Inspection_XTMPI__c rObj : scope)
       {       

               rObj.Auto_Trigger__c = TRUE;
               ros.add(rObj);       
             
       
       }
    if(ros.size() > 0)
      update ros;
    }
  global void finish(Database.BatchableContext BC){
  }
}