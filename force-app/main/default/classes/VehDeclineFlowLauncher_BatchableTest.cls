@isTest
private class VehDeclineFlowLauncher_BatchableTest
{
    private static testMethod void myTestMethod()
    {
        Multi_Point_Inspection_XTMPI__c rep = new Multi_Point_Inspection_XTMPI__c(Undecided__c = 'Y', Auto_Trigger__c = False);
        insert rep;
        
         Test.startTest();
            String Query = 'Select xt.Id, xt.Auto_Trigger__c '
                 +  'From Multi_Point_Inspection_XTMPI__c xt where Auto_Trigger__c=False and Account__c=NULL and (Undecided__c = \'Y\' OR Declined__c = \'Y\')';
        VehDeclineFlowLauncher_Batchable ClassObj = new VehDeclineFlowLauncher_Batchable(Query);
        database.executeBatch(ClassObj, 1);
        Test.stopTest();}
        
            private static testMethod void myRepOrderFlowLauncher_BatchableTest()
    {
        Test.startTest();
            Datetime dt = Datetime.now().addMinutes(1);
            String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
            VehDeclineFlowLauncher_Schedulable ClassObj = new VehDeclineFlowLauncher_Schedulable();
            system.schedule('Test Method', CRON_EXP, ClassObj);   
        Test.stopTest();
    }
        
             

    }