global class VehDeclineFlowLauncher_Schedulable implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
            String Query = 'Select xt.Id, xt.Auto_Trigger__c '
                 +  'From Multi_Point_Inspection_XTMPI__c xt where customer_last_name__c != null and Account__c=NULL and (Undecided__c = \'Y\' OR Declined__c = \'Y\')';
        VehDeclineFlowLauncher_Batchable ClassObj = new VehDeclineFlowLauncher_Batchable(Query);
        database.executeBatch(ClassObj, 1);
    }
    
    }