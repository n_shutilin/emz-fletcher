public with sharing class VehicleModelFormController {

    @AuraEnabled
    public static void updateInventoryInfo(Id recordId, Id inventoryId) {
        try {
            InventoryService.updateInventoryInfo(new Map<Id, Id> {inventoryId => recordId});
        } catch (Exception ex) {
            throw new AuraHandledException('An error occurred during vehicle info update ' + ex.getMessage());
        }
    }

    @AuraEnabled
    public static void updateInventoryInfoFromParams(String recordId, String inventoryJson) {
        try {
            InventoryService.Inventory inventory = (InventoryService.Inventory) JSON.deserialize(inventoryJson, InventoryService.Inventory.class);
            InventoryService.updateInventoryInfo(recordId, inventory);
        } catch (Exception ex) {
            throw new AuraHandledException('An error occurred during vehicle info update ' + ex.getMessage());
        }
    }

    @AuraEnabled
    public static List<String> getInventoryFields() {
        return InventoryService.getInventoryFields();
    }

    @AuraEnabled
    public static List<InventoryService.SelectOptionDto> getYearOptions() {
        List<InventoryService.SelectOptionDto> selectOptions = new List<InventoryService.SelectOptionDto>();
        try {
            selectOptions = InventoryService.getYearOptions(Datetime.now().year() + 1, 20);
        } catch (Exception ex) {
            throw new AuraHandledException('An error occurred during year list preparation');
        }
        return selectOptions;
    }

    @AuraEnabled
    public static List<InventoryService.SelectOptionDto> getModelOptionsByMake(Id makeId) {
        List<InventoryService.SelectOptionDto> selectOptions = new List<InventoryService.SelectOptionDto>();
        try {
            selectOptions = InventoryService.buildModelSelectOptions(makeId);
        } catch (Exception ex) {
            throw new AuraHandledException('An error occurred during model list preparation');
        }
        return selectOptions;
    }

    @AuraEnabled
    public static List<InventoryService.SelectOptionDto> getMakeOptions() {
        List<InventoryService.SelectOptionDto> selectOptions = new List<InventoryService.SelectOptionDto>();
        try {
            selectOptions = InventoryService.buildMakeSelectOptions();
        } catch (Exception ex) {
            throw new AuraHandledException('An error occurred during make list preparation');
        }
        return selectOptions;
    }
}