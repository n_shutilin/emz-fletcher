@IsTest
private class VehicleModelFormControllerTest {
    @TestSetup
    static void setup() {
        Opportunity testOpportunity = new Opportunity(
                Name = 'Test',
                StageName = 'Test',
                CloseDate = Date.newInstance(2021, 12, 12),
                Dealer_ID__c = 'CHAUD'
        );
        insert testOpportunity;
        Lead testLead = new Lead(
                Dealer_ID__c = 'CHAUD',
                LastName = 'Test'
        );
        insert testLead;

        Vehicle_Make__c testMake = new Vehicle_Make__c(
                Make__c = 'Audi'
        );
        insert testMake;
        Vehicle_Make__c testMake1 = new Vehicle_Make__c(
        );
        insert testMake1;

        Vehicle_Model__c testModel = new Vehicle_Model__c(
                Model__c = 'TEST',
                Vehicle_Make__c = testMake.Id
        );
        insert testModel;
        Vehicle__c testVehicle = new Vehicle__c(
                Exterior_Color__c = 'Black',
                Year__c = '2021',
                Make__c = 'Audi',
                Interior_Color__c = 'Black',
                Body__c = 'Sedan',
                Trim__c = 'Test',
                Model__c = 'TEST'
        );
        insert testVehicle;
        Inventory__c testInventory = new Inventory__c(
                Vehicle__c = testVehicle.Id,
                TypeNUName__c = 'Test',
                Stock_Number__c = '7488329'
        );
        insert testInventory;
    }
    @IsTest
    static void updateInventoryInfo() {
        Opportunity testOpportunity = [
                SELECT Dealer_ID__c
                FROM Opportunity
                LIMIT 1
        ];
        Lead testLead = [
                SELECT Dealer_ID__c
                FROM Lead
                LIMIT 1
        ];
        Inventory__c testInventory = [
                SELECT Vehicle__r.Exterior_Color__c, Vehicle__r.Interior_Color__c, Vehicle__r.Body__c, Vehicle__r.Trim__c, TypeNUName__c, Vehicle__r.Year__c, Vehicle__r.Make__c, Vehicle__r.Model__c, Stock_Number__c
                FROM Inventory__c
                LIMIT 1
        ];
        VehicleModelFormController.updateInventoryInfo(testOpportunity.Id, testInventory.Id);
        VehicleModelFormController.updateInventoryInfo(testLead.Id, testInventory.Id);
    }

    @IsTest
    static void updateInventoryInfoNegative() {
        try {
            VehicleModelFormController.updateInventoryInfo(NULL, NULL);
        } catch (Exception e) {

        }

    }

    @IsTest
    static void getInventoryFields() {
        VehicleModelFormController.getInventoryFields();
    }

    @IsTest
    static void getMakeOptions() {
        VehicleModelFormController.getMakeOptions();
    }

    @IsTest
    static void updateInventoryInfoFromParams() {
        Opportunity testOpportunity = [
                SELECT Dealer_ID__c
                FROM Opportunity
                LIMIT 1
        ];
        Lead testLead = [
                SELECT Dealer_ID__c
                FROM Lead
                LIMIT 1
        ];
        Inventory__c testInventory = [
                SELECT Vehicle__r.Exterior_Color__c, Vehicle__r.Interior_Color__c, Vehicle__r.Body__c, Vehicle__r.Trim__c, TypeNUName__c, Vehicle__r.Year__c, Vehicle__r.Make__c, Vehicle__r.Model__c, Stock_Number__c
                FROM Inventory__c
                LIMIT 1
        ];
        VehicleModelFormController.updateInventoryInfoFromParams(testOpportunity.Id, JSON.serialize(testInventory));
        VehicleModelFormController.updateInventoryInfoFromParams(testLead.Id, JSON.serialize(testInventory));
    }

    @IsTest
    static void updateInventoryInfoFromParamsNegative() {
        try {
            VehicleModelFormController.updateInventoryInfoFromParams(NULL, NULL);
        } catch (Exception e) {

        }
    }

    @IsTest
    static void getYearOptions() {
        VehicleModelFormController.getYearOptions();
    }

    @IsTest
    static void getModelOptionsByMake() {
        Vehicle_Make__c testMake = [SELECT Id FROM Vehicle_Make__c WHERE Make__c = 'Audi' LIMIT 1];
        VehicleModelFormController.getModelOptionsByMake(testMake.Id);
    }

}