/**
 * Created by user on 1/24/2020.
 */


public class VehicleParcer {

    public Boolean success;
    public List<Vehicle> data;
    public Integer totalProperty;

    public class Vehicle {
        public String InvtAddsCost;
        public String InvtAddsResidual;
        public String InvtAddsRetail;
        public String InvtCertifiedPreOwnedYN;
        public String InvtInitialCost;
        public String InvtKeyStockNumber;
        public String InvtMakeName;
        public String InvtMileage;
        public String InvtModelName;
        public String InvtNetCost;
        public String InvtOdometerStatusName;
        public String InvtRSUsedYN;
        public String InvtReceiveDate;
        public String InvtReceiveFrom;
        public String InvtStatusName;
        public String InvtTypeNUName;
        public String InvtTypeVehicleName;
        public String InvtUsedGrossTrade;
        public String InvtUserStatus;
        public String InvtVIN;
        public String InvtYYYY;
        public String InvtLicExpDate;
        public String InvtLicense;
        public String InvtOriginCodeName;
        public String InvtBodyStyle;
        public String InvtColor1Name;
        public String InvtCylinders;
        public String InvtFuelType;
        public String InvtColor2Name;
        public String InvtEquipmentDesc1;
        public String InvtMSRP;
        public String InvtEquipmentDesc2;
        public String InvtEquipmentDesc3;
        public String InvtUsedActualValue;
    }


    public static VehicleParcer parse(String json) {
        return (VehicleParcer) System.JSON.deserialize(json, VehicleParcer.class);
    }
}