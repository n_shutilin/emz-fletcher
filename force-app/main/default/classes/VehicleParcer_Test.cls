/**
 * Created by Anton Salanovich on 1/24/2020.
 */



@IsTest
public class VehicleParcer_Test {

    static testMethod void testParse() {
        String json = '{'+
                '    \"success\": true,'+
                '    \"data\": ['+
                '        {'+
                '            \"InvtAddsCost\": \"150.00\",'+
                '            \"InvtAddsResidual\": \"0.00\",'+
                '            \"InvtAddsRetail\": \"0.00\",'+
                '            \"InvtCertifiedPreOwnedYN\": \"N\",'+
                '            \"InvtColor1Name\": \"ARCTIC WHITE\",'+
                '            \"InvtColor2Name\": \"BLACK\",'+
                '            \"InvtEquipmentDesc1\": \"C01 C02 X04 EK1 E34 F68 MJ8 ML4\",'+
                '            \"InvtEquipmentDesc2\": \"V43 W54\",'+
                '            \"InvtInitialCost\": \"32705.00\",'+
                '            \"InvtKeyStockNumber\": \"72127\",'+
                '            \"InvtMSRP\": \"34710.00\",'+
                '            \"InvtMakeName\": \"MERCED SPRINTER\",'+
                '            \"InvtMileage\": \"5\",'+
                '            \"InvtModelName\": \"METRIS CARGO VA\",'+
                '            \"InvtNetCost\": \"32855.00\",'+
                '            \"InvtOdometerStatusName\": \"Actual\",'+
                '            \"InvtOriginCodeName\": \"FACTORY ORDER\",'+
                '            \"InvtRSUsedYN\": \"N\",'+
                '            \"InvtReceiveDate\": \"11/19/2019\",'+
                '            \"InvtReceiveFrom\": \"MBUSA\",'+
                '            \"InvtStatusName\": \"Current\",'+
                '            \"InvtTypeNUName\": \"New\",'+
                '            \"InvtTypeVehicleName\": \"Car\",'+
                '            \"InvtUserStatus\": \"R\",'+
                '            \"InvtVIN\": \"WD3PG2EA6L3651153\",'+
                '            \"InvtYYYY\": \"2020\"'+
                '        }'+
                '    ],'+
                '    \"totalProperty\": 1'+
                '}';
        VehicleParcer obj = VehicleParcer.parse(json);
        System.assert(obj != null);
    }
}