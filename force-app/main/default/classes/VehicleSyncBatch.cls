/**
 * Created by Anton Salanovich on 1/24/2020.
 */

global class VehicleSyncBatch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts {


    global Database.QueryLocator start(Database.BatchableContext BC) {

        Id recTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        String query = 'SELECT Id, Name, Advent_Store_Number__c FROM Account WHERE recordTypeId =:recTypeId AND Advent_Store_Number__c != null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Account> dealerList) {
        if (dealerList.size() > 1) {
            throw new ApplicationException('Scope is too large. Please set the scope size equal to 1 item');
        }

        System.debug(dealerList);
        Account currentDealer = dealerList.get(0);

        //remove hardcode and use custom setting
        String vendorId = 'e804ff77-4fd8-48d1-8096-92278d651650';
        AdventApi advApi = new AdventApi(vendorId, currentDealer.Advent_Store_Number__c);

        System.debug('header = ' + advApi.authHeader);

        HttpResponse response = advApi.getNoParams('/inventory');
        System.debug('response' + response);
        VehicleParcer allVehicles = new VehicleParcer();

        allVehicles = VehicleParcer.parse(response.getBody());
        System.debug('allVehicles  = ' + allVehicles);
        System.debug('ID = =   = ' + currentDealer.Id);

        List<Vehicle__c> converted = VehicleSyncBatchHandler.convertToVehicles(allVehicles, currentDealer.Id);

        System.debug('NEW LIST ===  = ' + converted);
        //upsert converted VIN__c;


        System.debug('NEW ITEM ===  = ' + converted.get(0));

    }

    global void finish(Database.BatchableContext bc) {
        try {
            IntegrationLogger.insertLogs();
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }
}