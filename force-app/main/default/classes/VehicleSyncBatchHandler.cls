/**
 * Created by user on 1/24/2020.
 */

public with sharing class VehicleSyncBatchHandler {

    public static List<Vehicle__c> convertToVehicles(VehicleParcer allVehicles, String dealerId) {

        List<Vehicle__c> newVehicles = new List<Vehicle__c>();
        List<Vehicle__c> vehiclesNoVin = new List<Vehicle__c>();
        Map<String, Vehicle__c> myMap = new Map<String, Vehicle__c>();

        for (VehicleParcer.Vehicle oneVehicleData: allVehicles.data) {

            Vehicle__c newVehicle = new Vehicle__c();
            newVehicle.Make__c = oneVehicleData.InvtMakeName;
            if (oneVehicleData.InvtMSRP != null) {
                newVehicle.MSRP__c = Decimal.valueOf(oneVehicleData.InvtMSRP);
            }
            if (oneVehicleData.InvtMileage != null) {
                newVehicle.Mileage__c = Decimal.valueOf(oneVehicleData.InvtMileage);
            }
            if (oneVehicleData.InvtNetCost != null) {
                newVehicle.Net_Cost__c = Decimal.valueOf(oneVehicleData.InvtNetCost);
            }
            if (oneVehicleData.InvtReceiveDate != null) {
                newVehicle.Receive_Date__c = Date.parse(oneVehicleData.InvtReceiveDate);
            }
            String dealerCode = [SELECT Dealer_Code__c FROM Account WHERE Id = :dealerId].Dealer_Code__c;
            newVehicle.Model__c = oneVehicleData.InvtModelName;
            newVehicle.Dealer_ID__c = dealerCode;
            newVehicle.Year__c = oneVehicleData.InvtYYYY;
            newVehicle.VIN__c = oneVehicleData.InvtVIN;
            /*
            NOT MAPPED FIELDS
            "InvtAddsCost"
            "InvtAddsResidual"
            "InvtAddsRetail"
            "InvtCertifiedPreOwnedYN"
            "InvtColor1Name"
            "InvtColor2Name"
            "InvtInitialCost"
            "InvtKeyStockNumber"
            "InvtOdometerStatusName"
            "InvtOriginCodeName"
            "InvtRSUsedYN"
            "InvtReceiveFrom"
            "InvtStatusName"
            "InvtTypeNUName"
            "InvtTypeVehicleName"
            "InvtUserStatus"
            */
            if (newVehicle.VIN__c != null && newVehicle.VIN__c != ''){
                newVehicles.add(newVehicle);
            } else {
                vehiclesNoVin.add(newVehicle);
            }

            if (myMap.get(newVehicle.VIN__c) == null) {
                myMap.put(newVehicle.VIN__c, newVehicle);
            } else {
                if (myMap.get(newVehicle.VIN__c).Receive_Date__c < newVehicle.Receive_Date__c){
                    myMap.put(newVehicle.VIN__c, newVehicle);
                }
                System.debug('duplicate value = ' + newVehicle);
                System.debug('EXISTING value = ' + myMap.get(newVehicle.VIN__c));
            }
        }

        //insert vehiclesNoVin;

        return myMap.values();

    }

}