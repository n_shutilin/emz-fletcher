public without sharing class WebAppointmentBookingController {
    private final static Integer MAX_RESULTS = 10;


    @AuraEnabled
    public static String authorize(String code, String connectType){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('authorize');
        request.setArgument('code', code);
        request.setArgument('connectType', connectType);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return JSON.serialize(response);
    }

    @AuraEnabled
    public static String createAccount(String firstName, String lastName, String zipCode, String phone, String emailAddress, String dealer){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('createAccount');
        request.setArgument('firstName', firstName);
        request.setArgument('lastName', lastName);
        request.setArgument('zipCode', zipCode);
        request.setArgument('phone', phone);
        request.setArgument('emailAddress', emailAddress);
        request.setArgument('dealer', dealer);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return JSON.serialize(response);
    }

    @AuraEnabled
    public static void executeCodeSend(String connectType){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('executeCodeSend');
        request.setArgument('connectType', connectType);
        WebAppointmentBookingControllerHelper.sendRequest(request);
    }

    @AuraEnabled
    public static String getCurrentSObjectType(String recordId){
        return Id.valueOf(recordId).getSObjectType().getDescribe().getName();
    }

    @AuraEnabled
    public static Account getPersonContactInfo(String recordId) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getPersonContactInfo');
        request.setArgument('recordId', recordId);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return (Account) response.getObjectList()[0];
    }

    @AuraEnabled
    public static Account getDealerInfo(String dealer){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getDealerInfo');
        request.setArgument('dealer', dealer);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return (Account) response.getObjectList()[0];
    }

    @AuraEnabled
    public static String getServieAppointmentInfo(String recordId) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getServieAppointmentInfo');
        request.setArgument('recordId', recordId);
        return WebAppointmentBookingControllerHelper.getRawNoFormResponse(request);        
    }

    @AuraEnabled
    public static List<Authorized_Contact__c> getAuthContacts(String recordId) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getAuthContacts');
        request.setArgument('recordId', recordId);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return (List<Authorized_Contact__c>) response.getObjectList();
    }

    @AuraEnabled
    public static List<CDK_Vehicle_Ownership__c> getVehicles(String recordId) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getVehicles');
        request.setArgument('recordId', recordId);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return (List<CDK_Vehicle_Ownership__c>) response.getObjectList();
    }

    @AuraEnabled(cacheable=true)
    public static String getPUDTransportationRequestPicklist() {
        return JSON.serialize(ServiceAppointment.fields.PickupDeliveryTransportationRequest__c.getDescribe().getSObjectField().getDescribe().getPicklistValues());
    }

    @AuraEnabled(cacheable=true)
    public static String getMakePicklist() {
        return JSON.serialize(Vehicle__c.fields.Make__c.getDescribe().getSObjectField().getDescribe().getPicklistValues());
    }

    @AuraEnabled(cacheable=true)
    public static List<ServiceResource> getServiceResourcesByDealer(String dealer) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getServiceResourcesByDealer');
        request.setArgument('dealer', dealer);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return (List<ServiceResource>) response.getObjectList();
    }

    @AuraEnabled
    public static String insertNewVehicle(String newVehicleJSON, String accId) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('insertNewVehicle');
        request.setArgument('newVehicleJSON', newVehicleJSON);
        request.setArgument('accId', accId);
        String response = WebAppointmentBookingControllerHelper.getRawNoFormResponse(request);
        return response;       
    }

    @AuraEnabled(cacheable=true)
    public static List<WorkType> getMostCommonlyUsedWorkTypes(String dealer) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getMostCommonlyUsedWorkTypes');
        request.setArgument('dealer', dealer);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return (List<WorkType>) response.getObjectList();
    }

    @AuraEnabled
    public static string getFile(String name){
        List<StaticResource> staticResource = [SELECT Id, Body FROM StaticResource WHERE Name = :name];
        if(staticResource.size() > 0) {
            return staticResource[0].Body.toString();
        } else {
            return null;
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> search(String searchTerm, List<String> selectedIds, String dealer) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [
            FIND :searchTerm
            IN ALL FIELDS
            RETURNING
                WorkType(Id, Name, DurationType, EstimatedDuration, CorrectionOpCode__c, PartsRequired__c, Loaner_Available__c, Rideshare__c, Horizon__c,
                    ExpressAvailable__c, RentalAvailable__c, ShuttleAvailable__c 
                    WHERE id NOT IN :selectedIds AND Dealer__c = :dealer )
            LIMIT :MAX_RESULTS
        ];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Accounts & convert them into LookupSearchResult
        String worktypeIcon = 'standard:work_type';
        WorkType[] wts = (List<WorkType>) searchResults[0];
        for (WorkType wt : wts) {
            results.add(
                new LookupSearchResult(
                    wt.Id,
                    'WorkType',
                    worktypeIcon,
                    wt.Name,
                    'WorkType • ' + wt.EstimatedDuration,
                    wt
                )
            );
        }

        // Optionnaly sort all results on title
        results.sort();

        return results;
    }

    @AuraEnabled
    public static List<DeclinedService__c> getDeclinedServices(String accountId, String vehicleId){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getDeclinedServices');
        request.setArgument('accountId', accountId);
        request.setArgument('vehicleId', vehicleId);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return (List<DeclinedService__c>) response.getObjectList();
    }

    @AuraEnabled
    public static void removeDeclinedServices(String serviceId){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('removeDeclinedServices');
        request.setArgument('serviceId', serviceId);
        WebAppointmentBookingControllerHelper.sendRequest(request);
    }

    @AuraEnabled
    public static String createServiceAppointment(String serviceAppointmentJSON) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('createServiceAppointment');
        request.setArgument('serviceAppointmentJSON', serviceAppointmentJSON);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return response.getBody();
    }

    @AuraEnabled
    public static String getAdvisorTimeSlots(String advisorIdsJSON, String dealer, String prefDate) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getAdvisorTimeSlots');
        request.setArgument('advisorIdsJSON', advisorIdsJSON);
        request.setArgument('dealer', dealer);
        request.setArgument('prefDate', prefDate);
        return WebAppointmentBookingControllerHelper.getRawResponse(request);
    }

    @AuraEnabled
    public static String getSchedTimeSlots(String appointmentId, String dealer) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getSchedTimeSlots');
        request.setArgument('appointmentId', appointmentId);
        request.setArgument('dealer', dealer);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return JSON.serialize(response.getapexObjectList());
    }

    @AuraEnabled
    public static Integer getDealerTimezoneShift(String dealer){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getDealerTimezoneShift');
        request.setArgument('dealer', dealer);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return Integer.valueOf(response.getBody());
    }

    @AuraEnabled
    public static String getAdvisorsAvailability(String advisorIdsJSON, String prefDate, String transportationType){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getAdvisorsAvailability');
        request.setArgument('advisorIdsJSON', advisorIdsJSON);
        request.setArgument('prefDate', prefDate);
        request.setArgument('transportationType', transportationType);
        return WebAppointmentBookingControllerHelper.getRawResponse(request);
    }

    @AuraEnabled
    public static void scheduleAppointment(String serviceAppointment, Boolean isRestoring) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('scheduleAppointment');
        request.setArgument('serviceAppointment', serviceAppointment);
        request.setArgument('isRestoring', String.valueOf(isRestoring));
        WebAppointmentBookingControllerHelper.sendRequest(request);
    }

    @AuraEnabled
    public static String getTransportationNotes(String appointmentId){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('getTransportationNotes');
        request.setArgument('appointmentId', appointmentId);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        return response.getBody();
    }

    @AuraEnabled
    public static void confirmationServiceAppointmentUpdate(String appointment){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('confirmationServiceAppointmentUpdate');
        request.setArgument('appointment', appointment);
        WebAppointmentBookingControllerHelper.sendRequest(request);
    }

    @AuraEnabled
    public static void deleteAppointment(String appointmentId) {
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('deleteAppointment');
        request.setArgument('appointmentId', appointmentId);
        WebAppointmentBookingControllerHelper.sendRequest(request);
    }

    @AuraEnabled
    public static String sendAppointmentIntoCDK(String recordId, String actionType){
        MainWebSchedulerController.SchedulerRequest request = new MainWebSchedulerController.SchedulerRequest('sendAppointmentIntoCDK');
        request.setArgument('recordId', recordId);
        request.setArgument('actionType', actionType);
        MainWebSchedulerController.SchedulerResponse response = WebAppointmentBookingControllerHelper.sendRequest(request);
        if(Integer.valueOf(response.getCode()) == 202) {
            return response.getBody();
        } else {
            return 'Service Appointment is not synced: ' + response.getErrorMessage();
        } 
    }
}