public with sharing class WebAppointmentBookingControllerHelper {

    private static String URI = 'https://fletcherjones--data.my.salesforce.com/services/apexrest/scheduler';

    public static MainWebSchedulerController.SchedulerResponse sendRequest(MainWebSchedulerController.SchedulerRequest request){
        WebSchedulerAuthorizationTool.Token token = WebSchedulerAuthorizationTool.getToken();
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint(URI);
        httpRequest.setMethod('GET');
        httpRequest.setHeader('Authorization', token.token_type + ' ' + token.access_token);
        httpRequest.setHeader('description', '');
        httpRequest.setHeader('Content-Type', 'text/html');
        httpRequest.setHeader('enabled', 'true');
        httpRequest.setHeader('Content-Length', '0');
        httpRequest.setTimeout(120000);
        httpRequest.setBody(JSON.serialize(request));
        Http http = new Http();
        HTTPResponse response = http.send(httpRequest);
        String body = formBody(response.getBody());
        System.debug(body);
        MainWebSchedulerController.SchedulerResponse schedulerResponse = (MainWebSchedulerController.SchedulerResponse) JSON.deserializeStrict(body, MainWebSchedulerController.SchedulerResponse.class);
        return schedulerResponse;
    }

    public static String getRawResponse(MainWebSchedulerController.SchedulerRequest request){
        WebSchedulerAuthorizationTool.Token token = WebSchedulerAuthorizationTool.getToken();
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint(URI);
        httpRequest.setMethod('GET');
        httpRequest.setHeader('Authorization', token.token_type + ' ' + token.access_token);
        httpRequest.setHeader('description', '');
        httpRequest.setHeader('Content-Type', 'text/html');
        httpRequest.setHeader('enabled', 'true');
        httpRequest.setHeader('Content-Length', '0');
        httpRequest.setTimeout(120000);
        httpRequest.setBody(JSON.serialize(request));
        Http http = new Http();
        HTTPResponse response = http.send(httpRequest);
        return formBody(response.getBody());
    }

    public static String getRawNoFormResponse(MainWebSchedulerController.SchedulerRequest request){
        WebSchedulerAuthorizationTool.Token token = WebSchedulerAuthorizationTool.getToken();
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint(URI);
        httpRequest.setMethod('GET');
        httpRequest.setHeader('Authorization', token.token_type + ' ' + token.access_token);
        httpRequest.setHeader('description', '');
        httpRequest.setHeader('Content-Type', 'text/html');
        httpRequest.setHeader('enabled', 'true');
        httpRequest.setHeader('Content-Length', '0');
        httpRequest.setTimeout(120000);
        httpRequest.setBody(JSON.serialize(request));
        Http http = new Http();
        HTTPResponse response = http.send(httpRequest);
        return response.getBody();
    }

    private static String formBody(String body) {
        return body.remove('\\').removeStart('"').removeEnd('"');
    }
}