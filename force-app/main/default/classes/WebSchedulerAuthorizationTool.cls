public with sharing class WebSchedulerAuthorizationTool {
    private static String CLIENT_ID = '3MVG9eMnfmfDO5NCD6HDuCDWXzZ8sGJu85CudrsxkSxhBvDx_TutJcdHGQa6xv8Fe3NHNmAqYlIQcznluJqrp';
    private static String CLIENT_SECRET = 'DD0233AAC53C258A831ABFE5B98B83232469B942142123B06D124901A47F15C3';
    private static String USERNAME = 'dealsintegrationuser@emzcloud.com';
    private static String PASSWORD = 'EMZdata2021!pQlpeVdKtUUAfX86URDCKyAWG';

    public class Token {
        public String id;
        public String issued_at;
        public String instance_url;
        public String signature;
        public String access_token;
        public String token_type;
    
        public Token(){
        }
    }

    public static Token getToken() {
        HttpRequest request = new HttpRequest();
        request.setMethod('POST');
        request.setHeader('Content-Type','application/x-www-form-urlencoded');
        request.setEndpoint('https://test.salesforce.com/services/oauth2/token');
        request.setBody('grant_type=password' + '&client_id='+CLIENT_ID + 
        '&client_secret='+CLIENT_SECRET + '&username='+USERNAME + '&password='+PASSWORD);
        Http http = new Http();
        HTTPResponse response = http.send(request);
        return (Token) JSON.deserialize(response.getBody(), Token.class);
    }
}