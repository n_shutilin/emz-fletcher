public interface WebSchedulerCommand {
    MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request);
}