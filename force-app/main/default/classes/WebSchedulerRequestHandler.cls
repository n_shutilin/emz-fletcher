public with sharing class WebSchedulerRequestHandler {

    public class getCurrentSObjectTypeCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String recordId = request.getArgument('recordId');
            Account acc = AppointmentBookingController.getPersonContactInfo(recordId);
            return new MainWebSchedulerController.SchedulerResponse(202, null, new List<Account>{acc});
        }
    }

    public class getDealerInfoCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String dealer = request.getArgument('dealer');
            Account acc = AppointmentBookingController.getDealerInfo(dealer);
            return new MainWebSchedulerController.SchedulerResponse(202, null, new List<Account>{acc});
        }
    }

    public class getServieAppointmentInfoCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String recordId = request.getArgument('recordId');
            String saw = AppointmentBookingController.getServieAppointmentInfo(recordId);
            // AppointmentBookingController.ServiceAppointmentWrapper wrapper = (AppointmentBookingController.ServiceAppointmentWrapper) JSON.deserializeStrict(saw, AppointmentBookingController.ServiceAppointmentWrapper.class);
            return new MainWebSchedulerController.SchedulerResponse(202, null, saw);
        }
    }

    public class getAuthContactsCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String recordId = request.getArgument('recordId');
            List<Authorized_Contact__c> contactList = AppointmentBookingController.getAuthContacts(recordId);
            return new MainWebSchedulerController.SchedulerResponse(202, null, contactList);
        }
    }

    public class getVehiclesCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String recordId = request.getArgument('recordId');
            List<CDK_Vehicle_Ownership__c> vehicleList = AppointmentBookingController.getVehicles(recordId);
            return new MainWebSchedulerController.SchedulerResponse(202, null, vehicleList);
        }
    }

    public class getServiceResourcesByDealerCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String dealer = request.getArgument('dealer');
            List<ServiceResource> resourceList = AppointmentBookingController.getServiceResourcesByDealer(dealer);
            return new MainWebSchedulerController.SchedulerResponse(202, null, resourceList);
        }
    }

    public class insertNewVehicleCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String newVehicleJSON = request.getArgument('newVehicleJSON');
            String accId = request.getArgument('accId');
            String body = AppointmentBookingController.insertNewVehicle(newVehicleJSON, accId);
            return new MainWebSchedulerController.SchedulerResponse(202, null, body);
        }
    }

    public class getMostCommonlyUsedWorkTypesCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String dealer = request.getArgument('dealer');
            List<WorkType> workTypeList = AppointmentBookingController.getMostCommonlyUsedWorkTypes(dealer);
            return new MainWebSchedulerController.SchedulerResponse(202, null, workTypeList);
        }
    }
    
    public class getDeclinedServicesCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String accountId = request.getArgument('accountId');
            String vehicleId = request.getArgument('vehicleId');
            List<DeclinedService__c> serviceList = AppointmentBookingController.getDeclinedServices(accountId, vehicleId);
            return new MainWebSchedulerController.SchedulerResponse(202, null, serviceList);
        }
    }

    public class removeDeclinedServicesCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String serviceId = request.getArgument('serviceId');
            AppointmentBookingController.removeDeclinedServices(serviceId);
            return new MainWebSchedulerController.SchedulerResponse(202, null);
        }
    }

    public class createServiceAppointmentCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String serviceAppointmentJSON = request.getArgument('serviceAppointmentJSON');
            String body = AppointmentBookingController.createServiceAppointment(serviceAppointmentJSON);
            return new MainWebSchedulerController.SchedulerResponse(202, null, body);
        }
    }

    public class getAdvisorTimeSlotsCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String advisorIdsJSON = request.getArgument('advisorIdsJSON');
            String dealer = request.getArgument('dealer');
            String prefDate = request.getArgument('prefDate');
            Map<String, Map<String, Timeslot[]>> timeSlots = AppointmentBookingController.getAdvisorTimeSlots(advisorIdsJSON, dealer, prefDate);
            System.debug(timeSlots);
            return new MainWebSchedulerController.SchedulerResponse(202, null, timeSlots);
        }
    }

    public class getSchedTimeSlotsCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String appointmentId = request.getArgument('appointmentId');
            String dealer = request.getArgument('dealer');
            String body = AppointmentBookingController.getSchedTimeSlots(appointmentId, dealer).remove('\\').removeStart('"').removeEnd('"');
            List<AppointmentBookingController.SchedulingOptionWrapper> optionList = (List<AppointmentBookingController.SchedulingOptionWrapper>) JSON.deserializeStrict(body, List<AppointmentBookingController.SchedulingOptionWrapper>.class);
            return new MainWebSchedulerController.SchedulerResponse(202, null, optionList);
        }
    }

    public class getDealerTimezoneShiftCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String dealer = request.getArgument('dealer');
            Integer shift = AppointmentBookingController.getDealerTimezoneShift(dealer);
            return new MainWebSchedulerController.SchedulerResponse(202, null, String.valueOf(shift));
        }
    }

    public class getAdvisorsAvailabilityCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String advisorIdsJSON = request.getArgument('advisorIdsJSON');
            String prefDate = request.getArgument('prefDate');
            String transportationType = request.getArgument('transportationType');
            Map<String, Map<String, AssignedResource[]>> advisors = AppointmentBookingController.getAdvisorsAvailability(advisorIdsJSON, prefDate, transportationType);
            return new MainWebSchedulerController.SchedulerResponse(202, null, advisors);
        }
    }

    public class scheduleAppointmentCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String serviceAppointment = request.getArgument('serviceAppointment');
            Boolean isRestoring = Boolean.valueOf(request.getArgument('isRestoring'));
            AppointmentBookingController.scheduleAppointment(serviceAppointment, isRestoring);
            return new MainWebSchedulerController.SchedulerResponse(202, null);
        }
    }

    public class getTransportationNotesCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String appointmentId = request.getArgument('appointmentId');
            String body = AppointmentBookingController.getTransportationNotes(appointmentId);
            return new MainWebSchedulerController.SchedulerResponse(202, null, body);
        }
    }

    public class confirmationServiceAppointmentUpdateCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String appointment = request.getArgument('appointment');
            Boolean doNotSendEmail = Boolean.valueOf(request.getArgument('doNotSendEmail'));
            AppointmentBookingController.confirmationServiceAppointmentUpdate(appointment);
            return new MainWebSchedulerController.SchedulerResponse(202, null);
        }
    }

    public class deleteAppointmentCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String appointmentId = request.getArgument('appointmentId');
            AppointmentBookingController.deleteAppointment(appointmentId);
            return new MainWebSchedulerController.SchedulerResponse(202, null);
        }
    }

    public class sendAppointmentIntoCDKCommand implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String recordId = request.getArgument('recordId');
            String actionType = request.getArgument('actionType');
            String body = AppointmentBookingController.sendAppointmentIntoCDK(recordId, actionType);
            return new MainWebSchedulerController.SchedulerResponse(202, null, body);
        }
    } 

    public class executeCodeSend implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String connectType = request.getArgument('connectType');
            WebSchedulerRequestHandler.executeCodeSend(connectType);
            return new MainWebSchedulerController.SchedulerResponse(202, null, '');
        }
    }

    public class authorize implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            Decimal code = Decimal.valueOf(request.getArgument('code'));
            String connectType = request.getArgument('connectType');
            String recordTypeId = getPersonAccountRecordTypeId();
            List<Account> targetAccountList = [SELECT Id, Authorization_Code__c, PersonMobilePhone, PersonEmail
                                              FROM Account 
                                              WHERE (RecordTypeId = :recordTypeId) AND (PersonMobilePhone = :connectType OR PersonEmail = :connectType) AND Authorization_Code__c = :code];
            if(!targetAccountList.isEmpty()) {
                return new MainWebSchedulerController.SchedulerResponse(202, null, targetAccountList[0].Id);
            } else {
                return new MainWebSchedulerController.SchedulerResponse(404, 'Authorization failed. Validate email or mobile phone number and code.');
            }

        }
    }

    public class createAccount implements WebSchedulerCommand {
        public MainWebSchedulerController.SchedulerResponse execute(MainWebSchedulerController.SchedulerRequest request) {
            String firstName = request.getArgument('firstName');
            String lastName = request.getArgument('lastName');
            String zipCode = request.getArgument('zipCode');
            String phone = request.getArgument('phone');
            String dealer = request.getArgument('dealer');
            String emailAddress = request.getArgument('emailAddress');
            try{
                List<Account> targetAccountList = [SELECT Id, Authorization_Code__c, PersonMobilePhone, PersonEmail
                                                   FROM Account 
                                                   WHERE (RecordTypeId = :getPersonAccountRecordTypeId()) AND (PersonMobilePhone = :phone OR PersonEmail = :emailAddress)];
                if(targetAccountList.isEmpty()){
                    Account newAccount = new Account(
                        RecordTypeId = getPersonAccountRecordTypeId(),
                        LastName = lastName,
                        FirstName = firstName,
                        PersonMobilePhone = phone,
                        PersonEmail = emailAddress,
                        PersonMailingPostalCode = zipCode,
                        Dealer_ID__pc = dealer
                    );
                    insert newAccount;
                    return new MainWebSchedulerController.SchedulerResponse(202, null, newAccount.Id);
                } else {
                    return new MainWebSchedulerController.SchedulerResponse(404, 'Something went wrong. Possible reason: email or phone already in use.');
                }
            } catch (Exception e) {
                return new MainWebSchedulerController.SchedulerResponse(422, e.getMessage());
            }
        }
    }

    private static final String PEROSN_ACCOUNT_RECORD_TYPE_NAME = 'Person Account';
    private static final String SOBJECT_TYPE_ACCOUNT_LABEL = 'Account';

    private WebSchedulerRequestHandler() {

    }

    public static void executeCodeSend(String connectType) {
        String recordTypeId = getPersonAccountRecordTypeId();
        List<Account> targetAccountList = [SELECT Id, Authorization_Code__c, PersonMobilePhone, PersonEmail, PersonContact.Id 
                                          FROM Account 
                                          WHERE (RecordTypeId = :recordTypeId) AND (PersonMobilePhone = :connectType OR PersonEmail = :connectType)];
        if(!targetAccountList.isEmpty()){
            Account targetAccount = targetAccountList[0];
            Integer code = generaeteAuthorizationCode();
            targetAccount.Authorization_Code__c = code;
            update targetAccount;
            if(targetAccount.PersonMobilePhone == connectType){
                sendSMS(connectType, code);
            } else {
                sendEmail(connectType, code);
            }
        }
    }

    private static String getPersonAccountRecordTypeId(){
        RecordType personAccountRecordType = [SELECT Id
                                              FROM RecordType 
                                              WHERE Name = :PEROSN_ACCOUNT_RECORD_TYPE_NAME 
                                              AND SObjectType = :SOBJECT_TYPE_ACCOUNT_LABEL];
        return personAccountRecordType.Id;
    }

    private static void sendSMS(String connectType, Integer code){

    }

    private static void sendEmail(String connectType, Integer code){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new List<String>{connectType});
        mail.setSenderDisplayName('Web Scheduler');
        mail.setSubject('Authorization code');
        mail.setPlainTextBody(String.valueOf(code));
        Messaging.SendEmailResult[] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

    private static Integer generaeteAuthorizationCode() {
        return Math.round((Math.random() * (900000) + 100000));
    }
}