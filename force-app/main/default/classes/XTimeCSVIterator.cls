public inherited sharing class XTimeCSVIterator implements Iterator<String>, Iterable<String>{
    String data;
    String delimetr;
    Integer currentIndex;

    public XTimeCSVIterator(String data, String delimetr) {
        this.data = data;
        this.delimetr = delimetr;
        this.currentIndex = 0;
    }

    public Boolean hasNext(){
       return currentIndex < data.length();
    }

    public Boolean isConfigured(){
       return this.data != null;
    }

    public String next() {     
       Integer key = this.data.indexOf(this.delimetr, currentIndex);
 
       if (key == -1) {
        key = this.data.length();
       }

       String row = this.data.subString(this.currentIndex, key);
       this.currentIndex = key + 1;
       return row;
    }
    
    public Iterator<String> Iterator() {
       return this;   
    }
}