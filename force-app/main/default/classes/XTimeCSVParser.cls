public inherited sharing class XTimeCSVParser {
    private XTimeErrorHandler errorHandler;
    private XTimeCSVIterator csvIterator;
    private Map<String, SObjectField> fieldMapping = new Map<String, SObjectField> {
        'Store ID' => Multi_Point_Inspection_XTMPI__c.X_Time_Store_ID__c,
        'RO Number' => Multi_Point_Inspection_XTMPI__c.RO_Number__c,
        'DMS Open Date' => Multi_Point_Inspection_XTMPI__c.DMS_Open_Date__c,
        'Inspect Close Date' => Multi_Point_Inspection_XTMPI__c.Inspect_Close_Date__c,
        'DMS Close Date' => Multi_Point_Inspection_XTMPI__c.DMS_Close_Date__c,
        'Closed Reason' => Multi_Point_Inspection_XTMPI__c.Closed_Reason__c,
        'Express' => Multi_Point_Inspection_XTMPI__c.Express__c,
        'Waiter' => Multi_Point_Inspection_XTMPI__c.Waiter__c,
        'Customer First Name' => Multi_Point_Inspection_XTMPI__c.Customer_First_Name__c,
        'Customer Last Name' => Multi_Point_Inspection_XTMPI__c.Customer_Last_Name__c,
        'Address1' => Multi_Point_Inspection_XTMPI__c.Address1__c,
        'Address2' => Multi_Point_Inspection_XTMPI__c.Address2__c,
        'City' => Multi_Point_Inspection_XTMPI__c.City__c,
        'State' => Multi_Point_Inspection_XTMPI__c.State__c,
        'Zip' => Multi_Point_Inspection_XTMPI__c.Zip__c,
        'Home Phone' => Multi_Point_Inspection_XTMPI__c.Home_Phone__c,
        'Work Phone' => Multi_Point_Inspection_XTMPI__c.Work_Phone__c,
        'Cell' => Multi_Point_Inspection_XTMPI__c.Cell__c,
        'Email' => Multi_Point_Inspection_XTMPI__c.Email__c,
        'Veh Year' => Multi_Point_Inspection_XTMPI__c.Veh_Year__c,
        'Veh Make' => Multi_Point_Inspection_XTMPI__c.Veh_Make__c,
        'Veh Model' => Multi_Point_Inspection_XTMPI__c.Veh_Model__c	,
        'Vin' => Multi_Point_Inspection_XTMPI__c.VIN__c,
        'Mileage In' => Multi_Point_Inspection_XTMPI__c.Mileage_In__c,
        'Advisor Number' => Multi_Point_Inspection_XTMPI__c.Advisor_Number__c,
        'Technican Number' => Multi_Point_Inspection_XTMPI__c.Technican_Number__c,
        'Inspection Complete' => Multi_Point_Inspection_XTMPI__c.Inspection_Complete__c,
        'Inspection Complete Time' => Multi_Point_Inspection_XTMPI__c.Inspection_Complete_Time__c,
        'Inspected By' => Multi_Point_Inspection_XTMPI__c.Inspected_By__c,
        'Service Name' => Multi_Point_Inspection_XTMPI__c.Service_Name__c,
        'Op Code' => Multi_Point_Inspection_XTMPI__c.Op_Code__c,
        'Color' => Multi_Point_Inspection_XTMPI__c.Color__c,
        'Labor Types' => Multi_Point_Inspection_XTMPI__c.Labor_Types__c,
        'Line Type' => Multi_Point_Inspection_XTMPI__c.Line_Type__c,
        'Total' => Multi_Point_Inspection_XTMPI__c.Total__c,
        'Parts' => Multi_Point_Inspection_XTMPI__c.Parts__c,
        'Labor' => Multi_Point_Inspection_XTMPI__c.Labor__c,
        'Hours' => Multi_Point_Inspection_XTMPI__c.Hours__c,
        'Approved' => Multi_Point_Inspection_XTMPI__c.Approved__c,
        'Declined' => Multi_Point_Inspection_XTMPI__c.Declined__c,
        'Undecided' => Multi_Point_Inspection_XTMPI__c.Undecided__c,
        'Approved by' => Multi_Point_Inspection_XTMPI__c.Approved_by__c,
        'Recommended By' => Multi_Point_Inspection_XTMPI__c.Recommended_By__c,
        'Quote Sent Via SMS' => Multi_Point_Inspection_XTMPI__c.Quote_Sent_Via_SMS__c,
        'Quote Sent Via Email' => Multi_Point_Inspection_XTMPI__c.Quote_Sent_Via_Email__c,
        'Quote Viewed by Customer' => Multi_Point_Inspection_XTMPI__c.Quote_Viewed_by_Customer__c,
        'Quote viewed by customer time and date' => Multi_Point_Inspection_XTMPI__c.Quote_viewed_by_customer_time_and_date__c,
        'Quote avg approval time via text by consumer' => Multi_Point_Inspection_XTMPI__c.Quote_avg_approval_time_via_text_by_cons__c,
        'Quote avg approval time via email by consumer' => Multi_Point_Inspection_XTMPI__c.Quote_avg_approval_time_via_email_by_con__c,
        'Booklet Printed' => Multi_Point_Inspection_XTMPI__c.Booklet_Printed__c,
        'Booklet Emailed' => Multi_Point_Inspection_XTMPI__c.Booklet_Emailed__c,
        'Time in Dispatch' => Multi_Point_Inspection_XTMPI__c.Time_in_Dispatch__c,
        'Inspection' => Multi_Point_Inspection_XTMPI__c.Inspection__c,
        'Waiting in Parts' => Multi_Point_Inspection_XTMPI__c.Waiting_in_Parts__c,
        'Working on Parts' => Multi_Point_Inspection_XTMPI__c.Working_on_Parts__c,
        'Completed in Parts' => Multi_Point_Inspection_XTMPI__c.Completed_in_Parts__c,
        'to View' => Multi_Point_Inspection_XTMPI__c.to_View__c,
        'Approval' => Multi_Point_Inspection_XTMPI__c.Approval__c,
        'Quote SMS Numbers' => Multi_Point_Inspection_XTMPI__c.Quote_SMS_Numbers__c,
        'Quote Email Addresses' => Multi_Point_Inspection_XTMPI__c.Quote_Email_Addresses__c,
        'Media' => Multi_Point_Inspection_XTMPI__c.Media__c
    };

    private Map<String, String> storeToDealerMap = new Map<String, String> {
        'S107' => 'CHMBN',
        'S109' => 'CHAUD',
        'S1454' => 'AUDFJ',
        'S1821' => 'FRMBN',
        'S1822' => 'FRPOR',
        'S1823' => 'LVMBN',
        'S1824' => 'HEMBN',
        'S1825' => 'NBMBN',
        'S1972' => 'AUDFR',
        'S2157' => 'ONMBN',
        'S2798' => 'BHAUD'
    };

    public XTimeCSVParser(String csv) {
        this.errorHandler = new XTimeErrorHandler();
        this.csvIterator = new XTimeCSVIterator(csv, '\r\n');
    }

    private List<String> parseRow(String row) {
        Pattern pattern = Pattern.compile('(?<=^|,)(\"(?:[^\"]|\"\")*\"|[^,]*)');
        List<String> parsedRow = new List<String>();
        Matcher matcher = pattern.matcher(row);
        while (matcher.find()){
            parsedRow.add(matcher.group()); 
        }
        return parsedRow;
    }

    public List<SObject> parse(){
        List<SObject> objList = new List<SObject>();
        if(this.csvIterator.isConfigured()){
            try {
                List<String> header = this.csvIterator.next().split(',');
                while(this.csvIterator.hasNext()){
                    List<String> splitedRow = parseRow(this.csvIterator.next());
                    Multi_Point_Inspection_XTMPI__c obj = new Multi_Point_Inspection_XTMPI__c();
                    for(Integer i = 0; i < splitedRow.size(); i++) {
                        SObjectField field = fieldMapping.get(header[i]);
                        obj.put(String.valueOf(field), processFieldByType(splitedRow[i],field));
                    }
                    obj.put('Inspection_Id__c', String.valueOf(obj.get('X_Time_Store_ID__c')) + String.valueOf(obj.get('RO_Number__c')));
                    obj.put('Dealer__c',getDealer(obj.get('X_Time_Store_ID__c')));
                    objList.add(obj);
                }
            } catch(Exception e) {
                errorHandler.logError('XTimeCSVParser.parse()', e.getMessage(), e.getTypeName());
            }
        }
        return objList;
    }

    private String getDealer (Object storeId) {
        String id = String.valueOf(storeId);
        return this.storeToDealerMap.get(id);

    }

    private Object processFieldByType(String value, SObjectField field) {
        if(String.isBlank(value)){
            return null;
        }
        Schema.DisplayType type = field.getDescribe().getType();
        if (type == Schema.DisplayType.STRING ||
            type == Schema.DisplayType.EMAIL ||
            type == Schema.DisplayType.LONG ||
            type == Schema.DisplayType.PICKLIST ||
            type == Schema.DisplayType.PHONE ||
            type == Schema.DisplayType.ADDRESS ||
            type == Schema.DisplayType.TEXTAREA) {
            return value.trim();
        }
        if (type == Schema.DisplayType.DATETIME) {
            return DateTime.valueOf(formatDateTime(value));
        }
        if (type == Schema.DisplayType.DATE) {
            return Date.valueOf(formatDate(value.trim()));
        }
        if (type == Schema.DisplayType.INTEGER) { //TODO add normalization
            return Integer.valueOf(value.trim());
        }
        if (type == Schema.DisplayType.DOUBLE || //TODO add normalization
            type == Schema.DisplayType.PERCENT) {
                return Double.valueOf(value.trim());
        }
        if (type == Schema.DisplayType.BOOLEAN) {
            return value.trim().toUpperCase() == 'Y';
        }
        value = formDecimal(value);
        return Decimal.valueOf(value);
    }

    public String formDecimal(String value) {
        String formedValue = value; //TODO rework
        if(formedValue.contains('(') && formedValue.contains(')')){
            formedValue = '-' + formedValue;
        }
        formedValue = formedValue.trim().replace('$', '').replace('(', '').replace(')', '').replace(',', '').replace('"', '');
        return formedValue;
    }

    public String formatDateTime(String value) { //TODO rework
        String [] splitedDateTime = value.split(' ');
        String [] splitedDate = splitedDateTime[0].split('/');
        String formedDate = splitedDate[2] + '-' + splitedDate[0] + '-' + splitedDate[1];
        String [] splitedTime = splitedDateTime[1].split(':');
        String period = splitedDateTime[2];
        Integer hours = Integer.valueOf(period == 'PM' ? Integer.valueOf(splitedTime[0]) + 12 : Integer.valueOf(splitedTime[0]));
        String formedTime = hours + ':' + splitedTime[1] + ':' + splitedTime[2];
        return formedDate + ' ' + formedTime;
    }

    private String formatDate(String value){
        String [] splitedDate = value.split('/');
        return splitedDate[2] + '-' + splitedDate[0] + '-' + splitedDate[1];
    }
}