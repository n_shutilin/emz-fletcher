public without sharing class XTimeErrorHandler {
    public void logError(String endpoint, Integer code, String body) {
        Long timeStamp = DateTime.now().getTime();
        Integration_Logs__c errorLog = new Integration_Logs__c(
            Endpoint__c = endpoint,
            Posix_Time__c = timeStamp,
            Error__c = String.valueOf(code),
            Error_Message__c = body,
            Process__c = 'XTime integration: WEB'
        );
        insert errorLog;
    }

    public void logError(String method, String errorMessage, String error) {
        Long timeStamp = DateTime.now().getTime();
        Integration_Logs__c errorLog = new Integration_Logs__c(
            Endpoint__c = 'CSV processing',
            Posix_Time__c = timeStamp,
            Error__c = error,
            Error_Message__c = errorMessage.length() > 255 ? errorMessage.substring(0,255) : errorMessage,
            Process__c = 'XTime integration: ' + method
        );
        insert errorLog;
    }

    public void logSuccess(String process, String endpoint, String status) {
        Long timeStamp = DateTime.now().getTime();
        Integration_Logs__c successLog = new Integration_Logs__c(
            Endpoint__c = endpoint,
            Result__c = status,
            Process__c = process
        );
        insert successLog;
    }
}