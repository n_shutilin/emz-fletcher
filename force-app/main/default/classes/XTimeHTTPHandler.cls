public class XTimeHTTPHandler {
    private String uri;
    private String password;
    private String login;
    private String dealerGroupId;
    private Date dataDate;
    private XTimeErrorHandler errorHandler;

    public XTimeHTTPHandler(Date dataDate) {
        formAuthData();
        this.dataDate = dataDate;
        this.errorHandler = new XTimeErrorHandler();
    }

    public String getRecords(){
        String endpoint = formEndPoint();
        try {
            HTTPResponse response = getResponse(endpoint);
            if(response.getStatusCode() == 200) {
                return response.getBody();
            } else {
                errorHandler.logError('XTimeHTTPHandler ' + endpoint, response.getStatusCode(), response.getBody());
                return null;
            }
        } catch(Exception e) {
            errorHandler.logError('XTimeHTTPHandler.getRecords()', e.getMessage(), e.getTypeName());
            return null;
        }
    }

    private HTTPResponse getResponse(String endpoint) {
        HTTP httpHandler = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(endpoint);
        Blob headerValue = Blob.valueOf(this.login + ':' + this.password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        request.setHeader('Authorization', authorizationHeader);
        request.setMethod('GET');
        request.setTimeout(120000);
        return  httpHandler.send(request);
    }

    private String formEndPoint(){
        return this.uri +'/Reports?ReportType=NightlyDealerGroupReport&DealerGroupId=' + this.dealerGroupId + '&StartDate=' + formDate();
    }

    private String formDate(){
        return this.dataDate.year() + '-' + this.dataDate.month() + '-' + this.dataDate.day();
    }

    private void formAuthData(){
        XTime_Integration__c autnData = XTime_Integration__c.getOrgDefaults();
        this.uri = autnData.XTime_Url__c;
        this.password = autnData.Password__c;
        this.login = autnData.Username__c;
        this.dealerGroupId = String.valueOf(autnData.Dealer_Group_Id__c).replace('.0','');
    }

}