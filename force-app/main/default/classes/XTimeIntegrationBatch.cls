global without sharing class XTimeIntegrationBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    private Date dataDate;

    public XTimeIntegrationBatch(Date dataDate){
        this.dataDate = dataDate;
    }
    global Iterable<SObject> start(Database.BatchableContext bc) {
        XTimeHTTPHandler httpHandler = new XTimeHTTPHandler(this.dataDate);
        XTimeCSVParser parser = new XTimeCSVParser(httpHandler.getRecords());
        // Map<String, SObject> idToObjectMap = new Map<String, SObject>();
        // for(SObject obj : parser.parse()) { //TODO Remove (was added in test reason)
        //     String objUnicId = String.valueOf(obj.get('Inspection_Id__c'));
        //     if(!idToObjectMap.containsKey(objUnicId)){
        //         idToObjectMap.put(objUnicId, obj);
        //     }
        // }
        return parser.parse();
    }

    global void execute(Database.BatchableContext bc, List<SObject> objectList) {
        XTimeErrorHandler errorHandler = new XTimeErrorHandler();
        try{
            insert objectList;//upsert ?
            insert createDeclinedServices(objectList);
            errorHandler.logSuccess('XTime Integration', 'Multi Point Inspection DATE: ' + this.dataDate + ' SIZE: ' + objectList.size(), 'OK');
        } catch (Exception e) {
            errorHandler.logError('XTimeIntegrationBatch.execute() DATE: ' + this.dataDate + ' SIZE: ' + objectList.size(), e.getMessage(), e.getTypeName());
        }

    }

    global void finish(Database.BatchableContext bc) {

    }

    private List<DeclinedService__c> createDeclinedServices(List<SObject> objectList) {
        List<Multi_Point_Inspection_XTMPI__c> mpObjectList = (List<Multi_Point_Inspection_XTMPI__c>) objectList;

        List<Multi_Point_Inspection_XTMPI__c> declinedServices = new List<Multi_Point_Inspection_XTMPI__c>();
        Set<String> vehicleVINs = new Set<String>(); 
        Set<String> customerEmails = new Set<String>(); 
        Set<String> opCodes = new Set<String>(); 
        Set<String> serviceNames = new Set<String>();

        for (Multi_Point_Inspection_XTMPI__c service : mpObjectList) {
            if(service.Declined__c == 'Y') {
                if(String.isNotBlank(service.VIN__c)) {
                    vehicleVINs.add(service.VIN__c);
                } else {
                    break;
                }
                
                if(String.isNotBlank(service.Email__c)) {
                    customerEmails.add(service.Email__c);
                } else {
                    break;
                }

                if(String.isNotBlank(service.Op_Code__c)) {
                    opCodes.add(service.Op_Code__c);
                } else if(String.isBlank(service.Op_Code__c) && String.isNotBlank(service.Service_Name__c)) {
                    serviceNames.add(service.Service_Name__c);
                } else {
                    break;
                }
                
                declinedServices.add(service);
            }
        }
        System.debug(declinedServices);
        System.debug(vehicleVINs);
        System.debug(customerEmails);
        System.debug(opCodes);
        System.debug(serviceNames);

        List<CDK_Vehicle__c> vehicles = [SELECT Id, VIN__c FROM CDK_Vehicle__c WHERE VIN__c IN :vehicleVINs];
        Map<String, String> vehicleMap = new Map<String,String>();
        for (CDK_Vehicle__c vehicle : vehicles) {
            vehicleMap.put(vehicle.VIN__c, vehicle.Id);
        }
        System.debug(vehicleMap);

        List<Account> customers = [SELECT Id, PersonEmail, Dealer_ID__pc FROM Account WHERE PersonEmail IN :customerEmails];
        Map<String, String> customersMap = new Map<String,String>();
        for (Account customer : customers) {
            customersMap.put(customer.PersonEmail, customer.Id);
        }
        System.debug(customersMap);

        List<WorkType> worktypesByOpCode = [SELECT Id, CorrectionOpCode__c, Dealer__c FROM WorkType WHERE CorrectionOpCode__c IN :opCodes];
        Map<String, String> workTypesByOpCodeMap = new Map<String,String>();
        for (WorkType worktype : worktypesByOpCode) {
            workTypesByOpCodeMap.put(worktype.Dealer__c + worktype.CorrectionOpCode__c, worktype.Id);
        }
        System.debug(workTypesByOpCodeMap);

        List<WorkType> worktypesByName = [SELECT Id, Name, Dealer__c FROM WorkType WHERE Name IN :serviceNames];
        Map<String, String> workTypesByNameMap = new Map<String,String>();
        for (WorkType worktype : worktypesByName) {
            workTypesByNameMap.put(worktype.Dealer__c + worktype.Name, worktype.Id);
        }
        System.debug(workTypesByNameMap);

        List<DeclinedService__c> finalDeclinedServices = new List<DeclinedService__c>();
        for (Multi_Point_Inspection_XTMPI__c service : declinedServices) {
            String vehicleId = vehicleMap.get(service.VIN__c);
            String customerId = customersMap.get(service.Email__c);
            String worktypeId;

            if(String.isNotBlank(service.Op_Code__c)) {
                worktypeId = workTypesByOpCodeMap.get(service.Dealer__c + service.Op_Code__c);
            } else if(String.isBlank(service.Op_Code__c) && String.isNotBlank(service.Service_Name__c)) {
                worktypeId = workTypesByNameMap.get(service.Dealer__c + service.Service_Name__c);
            }
            
            if(String.isNotBlank(vehicleId) && String.isNotBlank(customerId) && String.isNotBlank(worktypeId)) {
                finalDeclinedServices.add(new DeclinedService__c(
                    Account__c = customerId, 
                    WorkType__c = worktypeId,
                    CDK_Vehicle__c = vehicleId,
                    DeclineDate__c = service.DMS_Close_Date__c
                ));
            }
        }

        System.debug(finalDeclinedServices);
        return finalDeclinedServices;
    }
}