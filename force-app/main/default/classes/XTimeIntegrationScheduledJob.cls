global class XTimeIntegrationScheduledJob implements Schedulable{

    global void execute(SchedulableContext SC) {
        Date requestDate = Date.today();
        XTimeIntegrationBatch batch = new XTimeIntegrationBatch(requestDate.addDays(-1));
        XTimeErrorHandler errorHandler = new XTimeErrorHandler();
        try {
            Database.executeBatch(batch);
        } catch (Exception e) {
            errorHandler.logError('XTimeIntegrationScheduledJob DATE: ' + requestDate, e.getMessage(), e.getTypeName());
        }
    }

    global static void startJob(String name){
        String cronExp = '0 0 6 * * ? *';
        System.schedule(name, cronExp, new XTimeIntegrationScheduledJob());
    }

}