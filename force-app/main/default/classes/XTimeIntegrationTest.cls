@IsTest
public class XTimeIntegrationTest {
    @TestSetup
    static void init(){
        insert new XTime_Integration__c(
            XTime_Url__c = 'TEST',
            Password__c = 'TEST',
            Username__c = 'TEST',
            Dealer_Group_Id__c = 11);
    }

    @IsTest
    static void integrationTest() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new XTimeIntegrationTestMock());
        XTimeIntegrationScheduledJob.startJob('TEST');
        Test.stopTest();
    }
    
    @IsTest
    static void logingTest() {
        XTimeErrorHandler logger = new XTimeErrorHandler();
        logger.logError('TEST', 1, 'TEST');
        logger.logError('TEST', 'TEST', 'TEST');
    }
}