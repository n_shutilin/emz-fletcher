global class XTimeIntegrationTestMock implements HttpCalloutMock{

    private static final String RESPONSE_BODY = 
          'Store ID,RO Number,DMS Open Date,Inspect Close Date,DMS Close Date,Closed Reason,Express,Waiter,Customer First Name,Customer Last Name,Address1,Address2,City,State,Zip,Home Phone,Work Phone,Cell,Email,Veh Year,Veh Make,Veh Model,Vin,Mileage In,Advisor Number,Technican Number,Inspection Complete,Inspection Complete Time,Inspected By,Service Name,Op Code,Color,Labor Types,Line Type,Total,Parts,Labor,Hours,Approved,Declined,Undecided,Approved by,Recommended By,Quote Sent Via SMS,Quote Sent Via Email,Quote Viewed by Customer,Quote viewed by customer time and date,Quote avg approval time via text by consumer,Quote avg approval time via email by consumer,Booklet Printed,Booklet Emailed,Time in Dispatch,Inspection,Waiting in Parts,Working on Parts,Completed in Parts,to View,Approval,Quote SMS Numbers,Quote Email Addresses,Media\r\n' +
        + 'S107,847018,1/28/2021 10:51:39 PM,1/28/2021 10:53:27 PM,02/01/2021,Auto: This RO matches filter: Op Code,,N,,,,,,,,,,test@test.com,test@test.com,2021,MERCEDES-BENZ,GLE350,4JGFB4KB5MA407141,5,,,Y,,,[Primary],,Red,I,Primary,($1.00),$0.00,$0.00,0.00,Y,Y,Y,,ASR Pro asrpro,,,N,,0,0,N,N,00:00:00,00:00:00,00:00:00,00:00:00,00:00:00,,00:00:00,,,N';
    
    global HTTPResponse respond(HTTPRequest request) {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(RESPONSE_BODY);
        response.setStatusCode(200);
        return response;
    }
}