public with sharing class apprisalSynchronizationController {

    @AuraEnabled
    public static Trade_In__c getTrade(String recordId) {
        return [
            SELECT Last_Connection_Error_Message__c, Last_Connection_Try_Time__c
            FROM Trade_In__c
            WHERE Id = :recordId
        ];
    }

    @AuraEnabled
    public static String sendTrade(String recordId) {
        return RestAppraisals.appraisalPost(recordId);
    }

    @AuraEnabled
    public static String retrieveTrade(String recordId) {
        return RestAppraisals.appraisalRetrieve(recordId);
    }
}