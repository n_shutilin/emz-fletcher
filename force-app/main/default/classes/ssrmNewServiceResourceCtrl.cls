public with sharing class ssrmNewServiceResourceCtrl {

    @AuraEnabled(Cacheable=true)
    public static String getDealer(Id territoryId) {
        String dealer;

        List<ServiceTerritory> territories = [
            SELECT Dealer__c
            FROM ServiceTerritory
            WHERE Id = :territoryId
            AND Dealer__c != NULL
        ];

        if (!territories.isEmpty()) {
            dealer = territories.get(0).Dealer__c;
        }

        return dealer;
    }

    @AuraEnabled(Cacheable=true)
    public static Map<String, List<OptionsWrapper>> getPicklistValues() {
        Map<String, List<OptionsWrapper>> picklistMap = new Map<String, List<OptionsWrapper>>();
        List<Schema.SObjectField> fields;

        fields = Schema.SObjectType.ServiceTerritory.fields.getMap().values();

        for (Schema.SObjectField field : fields) {
            if (field.getDescribe().getType() == Schema.DisplayType.PICKLIST) {
                List<OptionsWrapper> picklistValues = new List<OptionsWrapper>();

                for (Schema.PicklistEntry ple : field.getDescribe().getPicklistValues()) {
                    picklistValues.add(new OptionsWrapper(ple.getLabel(), ple.getValue()));
                }

                picklistMap.put(field.getDescribe().getName(), picklistValues);
            }
        }

        return picklistMap;
    }

    @AuraEnabled
    public static Id saveServiceResource(ServiceResource resource, Id territoryId) {
        try {
            insert resource;
            Id hoursId = createHours(resource);
            createTerritoryMember(territoryId, resource.Id, hoursId);

            return resource.Id;
        } catch (DmlException e) {
            throw new InsertException(e.getMessage());
        }
    }

    private static Id createHours(ServiceResource resource) {
        OperatingHours hours = new OperatingHours(
            Name = resource.Dealer__c + ' - ' + resource.Name + ' - Resource Operating Hours'
        );

        insert hours;

        return hours.Id;
    }

    private static void createTerritoryMember(Id territoryId, Id resourceId, Id hoursId) {
        insert new ServiceTerritoryMember(
            ServiceTerritoryId = territoryId,
            ServiceResourceId = resourceId,
            OperatingHoursId = hoursId,
            EffectiveStartDate = Date.today()
        );
    }

    @TestVisible
    private class OptionsWrapper {
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String value {get;set;}

        public OptionsWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

    public class InsertException extends Exception {

    }
}