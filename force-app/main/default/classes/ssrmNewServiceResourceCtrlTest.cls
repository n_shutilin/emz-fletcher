@IsTest
private class ssrmNewServiceResourceCtrlTest {

    @TestSetup
    private static void setup() {
        OperatingHours hours = new OperatingHours(Name = 'Test');
        insert hours;

        ServiceTerritory territory = new ServiceTerritory (
            IsActive = true,
            Name = 'Test',
            Dealer__c = 'AUDFJ',
            OperatingHoursId = hours.Id
        );
        insert territory;
    }

    @IsTest
    public static void getDealer_Test() {
        Id territoryId = [
            SELECT Id
            FROM ServiceTerritory
        ].Id;

        Test.startTest();

        String dealer = ssrmNewServiceResourceCtrl.getDealer(territoryId);

        Test.stopTest();

        System.assertEquals('AUDFJ', dealer);
    }

    @IsTest
    public static void getPicklistValues_Test() {
        Test.startTest();

        Map<String, List<ssrmNewServiceResourceCtrl.OptionsWrapper>> optionsMap = ssrmNewServiceResourceCtrl.getPicklistValues();

        Test.stopTest();

        System.assertNotEquals(null, optionsMap);
    }

    @IsTest
    public static void saveServiceResources_Test() {

        Id territoryId = [
            SELECT Id
            FROM ServiceTerritory
        ].Id;

        ServiceResource resource = new ServiceResource(
            IsActive = true,
            Name = 'Test',
            Dealer__c = 'AUDFJ',
            RelatedRecordId = UserInfo.getUserId()
        );

        Test.startTest();

        ssrmNewServiceResourceCtrl.saveServiceResource(resource, territoryId);

        Test.stopTest();

        Integer resourcesCount = [
            SELECT COUNT()
            FROM ServiceResource
        ];

        System.assertEquals(1, resourcesCount);

        Integer hoursCount = [
            SELECT COUNT()
            FROM OperatingHours
        ];

        System.assertEquals(2, hoursCount);

        Integer membersCount = [
            SELECT COUNT()
            FROM ServiceTerritoryMember
        ];

        System.assertEquals(1, membersCount);
    }
}