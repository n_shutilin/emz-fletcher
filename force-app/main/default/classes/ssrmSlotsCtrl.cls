public with sharing class ssrmSlotsCtrl {
    @AuraEnabled
    public static List<List<SlotWrapper>> getNewSlots(Id territoryId, Id resourceId) {
        ServiceResource resource = [
            SELECT Name, Dealer__c, DefaultAdvisorTime__c
            FROM ServiceResource
            WHERE Id = :resourceId
        ];

        List<List<SlotWrapper>> slotsList;

        Map<String, List<Time>> daysRangeMap = getDaysRangeMap(territoryId);

        if (daysRangeMap != null && !daysRangeMap.isEmpty()) {
            slotsList = getSlotWrappers(daysRangeMap, Integer.valueOf(resource.DefaultAdvisorTime__c));
        }

        return slotsList;
    }

    private static Map<String, List<Time>> getDaysRangeMap(Id territoryId) {
        Map<String, List<Time>> daysRangeMap;

        List<ServiceTerritory> territories = [
            SELECT OperatingHoursId
            FROM ServiceTerritory
            WHERE Id = :territoryId
            AND OperatingHoursId != NULL
        ];

        if (!territories.isEmpty()) {
            List<TimeSlot> slots = [
                SELECT DayOfWeek, StartTime, EndTime
                FROM TimeSlot
                WHERE OperatingHoursId = :territories.get(0).OperatingHoursId
                ORDER BY DayOfWeek, StartTime
            ];

            if (!slots.isEmpty()) {
                daysRangeMap = new Map<String, List<Time>>();

                TimeSlot slot = slots.get(0);

                daysRangeMap.put(slot.DayOfWeek, new List<Time>{slot.StartTime});

                for (Integer i = 1; i < slots.size(); i++) {
                    slot = slots.get(i);
                    TimeSlot prevSlot = slots.get(i - 1);

                    if (slot.DayOfWeek != prevSlot.DayOfWeek) {
                        Time endTime = String.valueOf(prevSlot.EndTime) == '00:00:00.000Z' ? prevSlot.EndTime.addMilliseconds(-1) : prevSlot.EndTime;
                        daysRangeMap.get(prevSlot.DayOfWeek).add(endTime);
                        daysRangeMap.put(slot.DayOfWeek, new List<Time>{slot.StartTime});
                    }
                }

                Time endTime = String.valueOf(slot.EndTime) == '00:00:00.000Z' ? slot.EndTime.addMilliseconds(-1) : slot.EndTime;
                daysRangeMap.get(slot.DayOfWeek).add(endTime);
            }
        }

        return daysRangeMap;
    }

    private static List<List<SlotWrapper>> getSlotWrappers(Map<String, List<Time>> daysRangeMap, Integer defaultTime) {
        List<List<SlotWrapper>> slotsList = new List<List<SlotWrapper>>();

        for (String day : daysRangeMap.keySet()) {
            List<SlotWrapper> slots = new List<SlotWrapper>();

            Time startTime = daysRangeMap.get(day).get(0);
            Time endTime = daysRangeMap.get(day).get(1);

            Integer startMinutes = startTime.hour() * 60 + startTime.minute();
            Integer endMinutes = endTime.hour() * 60 + endTime.minute();

            if (endMinutes - startMinutes < defaultTime) {
                TimeSlot slot = new TimeSlot(
                    StartTime = startTime,
                    EndTime = endTime,
                    DayOfWeek = day
                );

                slots.add(new SlotWrapper(slot));
                slotsList.add(slots);
            } else {
                Time slotStartTime = startTime;
                Time slotEndTime = startTime.addMinutes(defaultTime);

                while (slotEndTime <= endTime) {
                    if (slotEndTime < slotStartTime) {
                        if (String.valueOf(slotEndTime) == '00:00:00.000Z') {
                            TimeSlot slot = new TimeSlot(
                                StartTime = slotStartTime,
                                EndTime = slotEndTime,
                                DayOfWeek = day
                            );

                            slots.add(new SlotWrapper(slot));
                        }

                        break;
                    }

                    TimeSlot slot = new TimeSlot(
                        StartTime = slotStartTime,
                        EndTime = slotEndTime,
                        DayOfWeek = day
                    );

                    slots.add(new SlotWrapper(slot));

                    slotStartTime = slotEndTime;
                    slotEndTime = slotStartTime.addMinutes(defaultTime);
                }

                slotsList.add(slots);
            }
        }

        return slotsList;
    }

    @AuraEnabled(Cacheable=true)
    public static List<String> getDays(Id resourceId) {
        List<String> days = new List<String>();

        List<ServiceTerritoryMember> members = [
            SELECT ServiceTerritory.OperatingHoursId
            FROM ServiceTerritoryMember
            WHERE ServiceResourceId = :resourceId
            AND ServiceTerritory.OperatingHoursId != NULL
        ];

        if (!members.isEmpty()) {
            List<AggregateResult> results = [
                SELECT DayOfWeek
                FROM TimeSlot
                WHERE OperatingHoursId = :members.get(0).ServiceTerritory.OperatingHoursId
                GROUP BY DayOfWeek
            ];

            for (AggregateResult result : results) {
                days.add(String.valueOf(result.get('DayOfWeek')));
            }
        }

        return days;
    }

    @AuraEnabled(Cacheable=true)
    public static List<List<SlotWrapper>> getExistingSlots(Id resourceId) {
        List<List<SlotWrapper>> slotsList = new List<List<SlotWrapper>>();

        List<ServiceTerritoryMember> members = [
            SELECT OperatingHoursId
            FROM ServiceTerritoryMember
            WHERE ServiceResourceId = :resourceId
        ];

        if (!members.isEmpty()) {
            List<TimeSlot> slots = [
                SELECT DayOfWeek, StartTime, EndTime
                FROM TimeSlot
                WHERE OperatingHoursId = :members.get(0).OperatingHoursId
                ORDER BY DayOfWeek, StartTime
            ];

            if (!slots.isEmpty()) {
                String dayOfWeek;

                for (TimeSlot slot : slots) {
                    SlotWrapper slotWrapper = new SlotWrapper(slot);

                    if (slotWrapper.day != dayOfWeek) {
                        dayOfWeek = slotWrapper.day;
                        slotsList.add(new List<SlotWrapper>());
                    }

                    slotsList.get(slotsList.size() - 1).add(slotWrapper);
                }
            }
        }

        return slotsList;
    }

    @AuraEnabled
    public static void saveSlots(String slotsStr, String context, Id resourceId) {
        List<SlotWrapper> slotWrappers = (List<SlotWrapper>) JSON.deserialize(slotsStr, List<SlotWrapper>.class);

        List<ServiceTerritoryMember> members = [
            SELECT OperatingHoursId
            FROM ServiceTerritoryMember
            WHERE ServiceResourceId = :resourceId
            AND (EffectiveEndDate = NULL OR EffectiveEndDate > TODAY)
            LIMIT 1
        ];

        if (!members.isEmpty()) {
            Id hourseId = members.get(0).OperatingHoursId;

            if (context == 'edit') {
                deleteOldSlots(hourseId);
            }

            List<TimeSlot> slots = new List<TimeSlot>();

            for (SlotWrapper sw : slotWrappers) {
                TimeSlot slot = getSlotFromWrapper(sw, hourseId);
                System.debug('--- slot: ' + slot);
                slots.add(slot);
            }

            insert slots;
        }
    }

    @AuraEnabled(Cacheable=true)
    public static String getResourceName(Id resourceId) {
        String resourceName = '';

        List<ServiceResource> resources = [
            SELECT Name
            FROM ServiceResource
            WHERE Id = :resourceId
            LIMIT 1
        ];

        if (!resources.isEmpty()) {
            resourceName = resources.get(0).Name;
        }

        return resourceName;
    }

    private static TimeSlot getSlotFromWrapper(SlotWrapper sw, Id hoursId) {
        TimeSlot slot = new TimeSlot(
            OperatingHoursId = hoursId,
            DayOfWeek = sw.day,
            StartTime = Time.newInstance(sw.startTimeHour, sw.startTimeMinute, 0, 0),
            EndTime = Time.newInstance(sw.endTimeHour, sw.endTimeMinute, 0, 0)
        );

        return slot;
    }

    private static void deleteOldSlots(Id hoursId) {
        delete [
            SELECT Id
            FROM TimeSlot
            WHERE OperatingHoursId = :hoursId
        ];
    }

    public class SlotWrapper {
        @AuraEnabled public String day;
        @AuraEnabled public Integer startTimeHour;
        @AuraEnabled public Integer endTimeHour;
        @AuraEnabled public Integer startTimeMinute;
        @AuraEnabled public Integer endTimeMinute;
        @AuraEnabled public String fullStartTime;
        @AuraEnabled public String fullEndTime;

        public SlotWrapper(TimeSlot slot) {
            day = slot.DayOfWeek;

            Integer startMinute = Math.round(Decimal.valueOf(slot.StartTime.minute()) / 5) * 5;

            if (startMinute == 60) {
                startTimeMinute = 0;
                startTimeHour = slot.StartTime.hour() == 23 ? 0 : slot.StartTime.hour() + 1;
            } else {
                startTimeMinute = startMinute;
                startTimeHour = slot.StartTime.hour();
            }

            Integer endMinute = Math.round(Decimal.valueOf(slot.EndTime.minute()) / 5) * 5;

            if (endMinute == 60) {
                endTimeMinute = 0;
                endTimeHour = slot.EndTime.hour() == 23 ? 0 : slot.EndTime.hour() + 1;
            } else {
                endTimeMinute = endMinute;
                endTimeHour = slot.EndTime.hour();
            }


            fullStartTime = getFullTime(this.startTimeHour, this.startTimeMinute);
            fullEndTime = getFullTime(this.endTimeHour, this.endTimeMinute);
        }

        private String getFullTime(Integer hour, Integer minute) {
            String hourStr;

            if (hour <= 12) {
                hourStr = hour == 0 ? '12' : String.valueOf(hour);
            } else {
                hourStr = String.valueOf(hour - 12);
            }

            if (hourStr.length() == 1) {
                hourStr = '0' + hourStr;
            }

            String minuteStr = String.valueOf(minute);

            if (minuteStr.length() == 1) {
                minuteStr = '0' + minuteStr;
            }

            String period = hour < 12 ? 'AM' : 'PM';

            return hourStr + ':' + minuteStr + ' ' + period;
        }
    }
}