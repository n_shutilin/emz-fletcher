@IsTest
private class ssrmSlotsCtrlTest {

    @TestSetup
    private static void setup() {
        OperatingHours territoryHours = new OperatingHours(Name = 'Test');
        insert territoryHours;

        insert new List<TimeSlot>{
            new TimeSlot(
                OperatingHoursId = territoryHours.Id,
                StartTime = Time.newInstance(5, 0, 0, 0),
                EndTime = Time.newInstance(5, 30, 0, 0),
                DayOfWeek = 'Sunday'
            ),

            new TimeSlot(
                OperatingHoursId = territoryHours.Id,
                StartTime = Time.newInstance(5, 30, 0, 0),
                EndTime = Time.newInstance(6, 0, 0, 0),
                DayOfWeek = 'Sunday'
            ),

            new TimeSlot(
                OperatingHoursId = territoryHours.Id,
                StartTime = Time.newInstance(23, 30, 0, 0),
                EndTime = Time.newInstance(0, 00, 0, 0),
                DayOfWeek = 'Monday'
            )
        };

        ServiceTerritory territory = new ServiceTerritory (
            IsActive = true,
            Name = 'Test',
            Dealer__c = 'AUDFJ',
            OperatingHoursId = territoryHours.Id
        );
        insert territory;

        ServiceResource resource = new ServiceResource(
            IsActive = true,
            Name = 'Test',
            Dealer__c = 'AUDFJ',
            RelatedRecordId = UserInfo.getUserId(),
            DefaultAdvisorTime__c = 10
        );
        insert resource;

        OperatingHours resourceHours = new OperatingHours(
            Name = resource.Dealer__c + ' - ' + resource.Name + ' - Resource Operating Hours'
        );
        insert resourceHours;

        insert new ServiceTerritoryMember(
            TerritoryType = 'P',
            ServiceTerritoryId = territory.Id,
            ServiceResourceId = resource.Id,
            OperatingHoursId = resourceHours.Id,
            EffectiveStartDate = Date.today()
        );
    }

    @IsTest
    private static void testSlots() {
        Id territoryId = [SELECT Id FROM ServiceTerritory].Id;
        Id resourceId = [SELECT Id FROM ServiceResource].Id;

        Test.startTest();

        List<List<ssrmSlotsCtrl.SlotWrapper>> slotsList = ssrmSlotsCtrl.getNewSlots(territoryId, resourceId);

        System.assertEquals(2, slotsList.size());
        System.assertEquals(6, slotsList.get(0).size());
        System.assertEquals(3, slotsList.get(1).size());

        List<ssrmSlotsCtrl.SlotWrapper> allSlots = new List<ssrmSlotsCtrl.SlotWrapper>();

        for (List<ssrmSlotsCtrl.SlotWrapper> wrappers : slotsList) {
            allSlots.addAll(wrappers);
        }

        ssrmSlotsCtrl.saveSlots(JSON.serialize(allSlots), 'edit', resourceId);
        slotsList = ssrmSlotsCtrl.getExistingSlots(resourceId);

        System.assertEquals(2, slotsList.size());

        Test.stopTest();
    }

    @IsTest
    private static void testSlots40Mins() {
        Id territoryId = [SELECT Id FROM ServiceTerritory].Id;
        Id resourceId = [SELECT Id FROM ServiceResource].Id;

        update new ServiceResource(
            Id = resourceId,
            DefaultAdvisorTime__c = 40
        );

        Test.startTest();

        List<List<ssrmSlotsCtrl.SlotWrapper>> slotsList = ssrmSlotsCtrl.getNewSlots(territoryId, resourceId);

        System.assertEquals(2, slotsList.size());
        System.assertEquals(1, slotsList.get(0).size());
        System.assertEquals(1, slotsList.get(1).size());

        Test.stopTest();
    }

    @IsTest
    private static void getDays_Test() {
        Id resourceId = [SELECT Id FROM ServiceResource].Id;

        Test.startTest();

        List<String> days = ssrmSlotsCtrl.getDays(resourceId);

        System.assertEquals(2, days.size());

        Test.stopTest();
    }

    @IsTest
    private static void getResourceName_Test() {
        Id resourceId = [SELECT Id FROM ServiceResource].Id;

        Test.startTest();

        String resourceName = ssrmSlotsCtrl.getResourceName(resourceId);

        System.assertEquals('Test', resourceName);

        Test.stopTest();
    }
}