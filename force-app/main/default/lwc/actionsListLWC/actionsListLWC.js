import {LightningElement, api, track} from 'lwc';

import getActionsList from '@salesforce/apex/ActionsListLWCController.getActionsList';
import changeActionStatus from '@salesforce/apex/ActionsListLWCController.changeActionStatus';
import processActionCancel from '@salesforce/apex/ActionsListLWCController.processCancel';
import Id from '@salesforce/user/Id';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { subscribe } from 'lightning/empApi';

const actions = [
    {label: 'Edit', name: 'edit'},
    {label: 'Cancel', name: 'cancel'},
    {label: 'Attempted', name: 'attempted'},
    {label: 'Contacted', name: 'complete'}
];

const columns = [
    {label: 'Subject', fieldName: 'Name', type: 'reference', typeAttributes: {id: {fieldName: 'Id'}}, sortable: true},
    {label: 'Icon', fieldName: 'Icon', type: 'customImage'},
    {label: 'Due Date', fieldName: 'DueDate', type: 'date-local', sortable: true},
    {label: 'Status', fieldName: 'Status'},
    {
        label: 'Owner Alias',
        fieldName: 'OwnerAlias',
        type: 'userReference',
        typeAttributes: {id: {fieldName: 'OwnerId'}},
        sortable: true
    },
    {
        type: 'action',
        typeAttributes: {rowActions: actions},
    },
];

export default class ActionsListLWC extends LightningElement {
    @api recordId;
    @api objectApiName;
    @api channelName = '/event/Action_Event__e';

    @track isDataEmpty;
    @track data = [];
    @track fullData = [];
    @track columns = columns;
    @track rowOffset = 0;
    @track filterValue = 'NEXT_N_DAYS:6';

    @track defaultSortDirection = 'asc';
    @track sortDirection = 'asc';
    @track sortedBy = 'DueDate';

    @track dataSize;
    @track defaultDataSize = 10;
    //@track allDataShown;
    @track showSpinner = false;
    //@track displayShowMoreButton;
    @track editRecordModal;
    @track editRecordName;
    @track selectedRecordToEdit;
    @track isModalOpen

    userId = Id;

    get options() { 
        return [
            {label: 'All Time', value: ''},
            {label: 'Last 7 Days', value: 'LAST_N_DAYS:6'},
            {label: 'Next 7 Days', value: 'NEXT_N_DAYS:6'},
            {label: 'Next 30 Days', value: 'NEXT_N_DAYS:29'},
        ];
    }

    get displayShowMoreButton() {
        return this.defaultDataSize < this.fullData.length;
    }

    get allDataShown() {
        return this.dataSize >= this.fullData.length;
    }

    connectedCallback() {
        this.getActionsList();
        this.subscribe();
    }

    handleNewAction() {
        this.isModalOpen = true;
    }

    handleCloseModal() {
        this.isModalOpen = false;
    }

    handleModalAction(event) {
        let type = event.detail.type
        switch (type) {
            case 'recordCreated':
                let recordId = event.detail.recordId
                let message = recordId ? 'Record with ID: ${recordId} was created.' : 'Record was created.';

                this.showMessage('Sucess!', message, 'success')
                // this.showSpinner = true;
                // setTimeout(() => this.getActionsList(), 1500)
                break;
        }
        this.isModalOpen = false;
    }

    showMessage(title, message, variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        })
        this.dispatchEvent(event)
    }

    handleChange(event) {
        this.filterValue = event.target.value;
        this.sortDirection = 'asc';
        this.getActionsList();
    }

    sortBy(field, reverse, primer) {
        const key = primer
            ? function (x) {
                return primer(x[field]);
            }
            : function (x) {
                return x[field].toLowerCase();
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    onHandleSort(event) {
        const {fieldName: sortedBy, sortDirection} = event.detail;
        const cloneData = [...this.fullData];
        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.fullData = cloneData;
        this.data = cloneData.slice(0, this.dataSize);
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }

    handleShowMore() {
        this.dataSize += 10;
        this.data = this.fullData.slice(0, this.dataSize);
        //this.allDataShown = (this.dataSize >= this.fullData.length);
    }

    handleShowLess() {
        this.dataSize = this.defaultDataSize;
        this.data = this.fullData.slice(0, this.dataSize);
        //this.allDataShown = false;
    }

    getActionsList() {
        this.showSpinner = true;
        getActionsList({
            recId: this.recordId,
            condition: this.filterValue
        })
            .then(result => {
                this.fullData = JSON.parse(result);
                if (this.fullData.length > 0) {
                    this.data = this.fullData.slice(0, this.defaultDataSize);
                    this.dataSize = this.defaultDataSize;
                    //this.displayShowMoreButton = (this.defaultDataSize < this.fullData.length);
                    this.isDataEmpty = false;
                } else {
                    this.isDataEmpty = true;
                }
                this.showSpinner = false;
            })
            .catch(error => {
                this.showSpinner = false;
                console.log('ERROR', JSON.stringify(error));
            });
    }

    handleRowAction(event) {
        if (event.detail.action.name === 'edit') {
            this.editRecordModal = true;
            this.editRecordName = event.detail.row.Name;
            this.selectedRecordToEdit = event.detail.row.Id;
        } else if (event.detail.action.name === 'cancel') {
            this.processCancel(event.detail.row.Id, this.recordId, this.userId)
            //this.changeActionStatus(event.detail.row.Id, event.detail.action.name);
        } else if (event.detail.action.name === 'complete') {
            this.changeActionStatus(event.detail.row.Id, event.detail.action.name);
        } else if (event.detail.action.name === 'attempted') {
            this.changeActionStatus(event.detail.row.Id, event.detail.action.name);
        }
    }

    processCancel = (recordId, parentRecordId, userId) => {
        this.showSpinner = true;
        processActionCancel({
            recordId: recordId,
            parentRecordId: parentRecordId,
            userId: userId
        })
            .then(result => {
                console.log(result)
                this.getActionsList();
                //this.allDataShown = false;
                this.showSpinner = false;
            })
            .catch(error => {
                this.showSpinner = false;
                console.log('ERROR', JSON.stringify(error));
            });
    }

    closeModal() {
        this.editRecordModal = false;
    }

    saveRecord(event) {
        this.getActionsList();
        this.closeModal();
        // if (this.allDataShown) {
        //     this.allDataShown = false; 
        // }
    }

    changeActionStatus(rowId, action) {
        this.showSpinner = true;
        changeActionStatus({
            recId: rowId,
            actionName: action
        })
            .then(result => {
                this.getActionsList();
                //this.allDataShown = false;
                this.showSpinner = false;
            })
            .catch(error => {
                this.showSpinner = false;
                console.log('ERROR', JSON.stringify(error));
            });
    }

    subscribe() {
        const messageCallback = (response) => {
            let event = JSON.parse(JSON.stringify(response));
            if(event.data.payload.Parent_Record_Id__c === this.recordId) {
                this.getActionsList();
            }
        };
        subscribe(this.channelName, -1, messageCallback).then(response => {
            this.subscription = response;
        });
    }
}