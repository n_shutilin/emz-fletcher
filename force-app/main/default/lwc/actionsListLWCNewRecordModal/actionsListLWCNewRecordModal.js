import { LightningElement, api, track } from 'lwc';

export default class ActionsListLWCNewRecordModal extends LightningElement {

    @api recordId
    @api objectApiName

    @track subject = 'CAFU';
     
    get subjectOptions() {
        return [
            { label: 'CAFU', value: 'CAFU' },
            { label: 'Due Bills', value: 'Due Bills' },
            { label: 'Declined', value: 'Declined' },
            { label: 'Due Bills', value: 'Due Bills' },
            { label: 'Internet Leads', value: 'Internet Leads' },
            { label: 'NOFU', value: 'NOFU' },
            { label: 'Pre-Paid Maintenance', value: 'Pre-Paid Maintenance' },
            { label: 'Overdue', value: 'Overdue' },
            { label: 'Recalls', value: 'Recalls' },
            { label: 'SOPs', value: 'SOPs' },
            { label: '3 Day Sales New Vehicle', value: '3 Day Sales New Vehicle' },
            { label: '3 Day Sales Pre-Owned', value: '3 Day Sales Pre-Owned' },
            { label: '3 Day Service', value: '3 Day Service' },
        ];
    }

    get lookupType () {
        switch (this.objectApiName) {
            case 'Opportunity':
                return 'Opportunity__c'
            case 'Lead':
                return 'Lead__c'
            case 'Account':
                return 'Account__c'      
        }
    }

    closeModal = (event) => {
        this.dispatchModalEvent('close')
    }

    handleSubjectChange = (event) => {
        this.subject = event.detail.value;
    }

    handleSuccess = (event) => {
        let newRecordId = event.detail.id;
        this.dispatchModalEvent('recordCreated', newRecordId)
    }

    dispatchModalEvent = (type = null, recordId = null) => {
        this.dispatchEvent(
            new CustomEvent("action", {
                detail: {
                    type: type,
                    recordId: recordId
                }
            })
        );
    }
}