import { LightningElement, api, track } from 'lwc';
// import getEmailTemplate from '@salesforce/apex/AppointmentBookingController.getEmailTemplate';

export default class AppointmentBookingConfirmation extends LightningElement {
    @api 
    get serviceAppointment() {
        return this._serviceAppointment;
    }
    set serviceAppointment(value) {
        if(value) {
            this._serviceAppointment = value;
            this.ownerEmail = value.OwnerEmail
            this.authUserEmail = (value.OwnerId != value.AuthorizeUserId) && value.AuthorizeUserEmail
                ? value.AuthorizeUserEmail 
                : null;
            this.ccEmailList = value.CCAdrresses ? [...value.CCAdrresses] : []
            this.prepareTemplate();
        }
    }
    _serviceAppointment;

    @api isUpdate;
    @api isOpenStore;

    @track ownerEmail
    @track authUserEmail
    @track isReady = false;
    @track isTemplateReady = true;
    
    originalTemplateHTML;
    @track templateHTML;

    tempResepietns;

    @track doNotSendEmail = false;
    @track toEmailList = [];
    @track ccEmailList
    @track ccEmail;
    @track comment = '';

    emailTemplate;

    get toEmails() {
        return this.toEmailList.join(', ');
    }

    get addButtonDisabled() {
        return this.doNotSendEmail || !this.ccEmail || this.ccEmailList.length >= 3
    }

    get isAuthUserEmailVisible() {
        return !!this.authUserEmail
    }

    prepareTemplate() {
        this.toEmailList = this.getEmailRecepient();
        this.prepareEmailData();
        this.isReady = true;
        // getEmailTemplate({
        //     appointmentId: this.serviceAppointment.Id,
        //     isUpdate: this.isUpdate
        // })
        //     .then(result => {
        //         if (result) {
        //             let tempResult            = result;
        //             let cdataOpenIndex        = tempResult.indexOf('<![CDATA[');
        //             let cdataCloseIndex       = tempResult.indexOf(']]>');
        //             tempResult                = tempResult.substring(cdataOpenIndex + 9, cdataCloseIndex);
        //             this.templateHTML = tempResult;

        //             this.toEmailList = this.getEmailRecepient();
        //             this.prepareEmailData();
        //         } else {
        //             this.isTemplateReady = false;
        //         }

        //         this.isReady = true;
        //     })
        // .then(() => {
        //     if (this.isTemplateReady) {
        //         //this.updateTemplate();
                
        //     }
        //     this.isReady = true;
        // })    
        // .catch(error => {
        //     console.log('ERROR', error)
        // });
    }

    getEmailRecepient() {
        return (this.serviceAppointment.OwnerId != this.serviceAppointment.AuthorizeUserId) && this.serviceAppointment.AuthorizeUserEmail
            ? [this.serviceAppointment.OwnerEmail, this.serviceAppointment.AuthorizeUserEmail] 
            : [this.serviceAppointment.OwnerEmail];
    }

    // updateTemplate() {
    //     let tempTemplate = this.originalTemplateHTML;
    //     tempTemplate = tempTemplate.replace('{!ServiceAppointment.Email_Template_Additional_Comments__c}', this.comment);
    //     this.templateHTML = tempTemplate;

    //     this.prepareEmailData();
    // }
    
    prepareEmailData() {
        let doNotSendEmail  = this.doNotSendEmail;
        let ccAddresses     = this.ccEmailList;
        // let toAddresses     = [...this.toEmailList];

        // let index = this.toEmailList.findIndex(selection => selection === this.serviceAppointment.OwnerEmail);
        //     if(index > -1) {
        //         toAddresses.splice(index, 1);
        //     }

        this.dispatchEvent(
            new CustomEvent('emailprepared', {
                detail: {
                    doNotSendEmail : this.doNotSendEmail,
                    ownerAddresses : this.ownerEmail,
                    authContactAddresses : this.authUserEmail,
                    ccAddresses : this.ccEmailList
                    // doNotSendEmail : this.doNotSendEmail
                }
            })
        );
    }

    handleOwnerInput(event) {
        this.ownerEmail = event.detail.value;
        this.prepareEmailData();
    }

    handleAuthUserInput(event) {
        this.authUserEmail = event.detail.value;
        this.prepareEmailData();
    }

    handleCheckboxCheck(event) {
        this.doNotSendEmail = event.target.checked;
        this.prepareEmailData();
    }

    handleCCInput(event) {
        this.ccEmail = event.detail.value;

    }

    handleAddCC() {
        if(this.emailIsValid(this.ccEmail)) {
            this.ccEmailList.push(this.ccEmail)
            this.ccEmail = '';
            this.prepareEmailData();
        }
    }

    handleRemoveCC(event) {
        this.ccEmailList.splice(event.currentTarget.dataset.index , 1);
    }

    // handleCommentInput(event) {
    //     this.comment = event.detail.value;
    //     this.updateTemplate();
    // }

    emailIsValid (email) {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
    }
}