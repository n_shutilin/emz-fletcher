import { LightningElement, api, track } from 'lwc'

import getDealerTimeSlots from '@salesforce/apex/AppointmentBookingController.getDealerTimeSlots'
import getDealerTransportationAvailability from '@salesforce/apex/AppointmentBookingController.getDealerTransportationAvailability'
import getDealerTimezoneShift from '@salesforce/apex/AppointmentBookingController.getDealerTimezoneShift'
import getTranstortationTypeAllowedTime from '@salesforce/apex/AppointmentBookingController.getTranstortationTypeAllowedTime'
import getPickupDeliveryTimeslots from '@salesforce/apex/AppointmentBookingController.getPickupDeliveryTimeslots'
import getTransportationTypeDailyCapacities from '@salesforce/apex/AppointmentBookingController.getTransportationTypeDailyCapacities';

import { ShowToastEvent }       from 'lightning/platformShowToastEvent'

const DAYS_OF_WEEK_MAP = {'Sun':'Sunday', 'Mon':'Monday', 'Tue':'Tuesday', 'Wed':'Wednesday', 'Thu':'Thursday', 'Fri':'Friday', 'Sat':'Saturday'}

export default class AppointmentBookingOpenScheduleTable extends LightningElement {
    @api serviceAppointmentId //++
    @api dealer = {} //++
    @api prefDate //++
    @api earliestStartDate //++
    @api bufferTime
    @api transportationType //++
    @api expressRequested //++
    @api rentalAutoAssigned
    @api requestLoaner
    @api waitCapacity //???
    @api loanerRequested //???
    @api pickupDeliveryResources //???
    

    @api 
    get timeslotsJson() {
        return this._timeslotsJson
    }
    set timeslotsJson(value) {
        this._timeslotsJson = value
        if (value) {
            this.finalTimeSlots = []
            this.availableTimeSlotsMap = {}
            this.dealerTimeSlotsMap = {}

            this.prepareData()
        }
    }
    _timeslotsJson //++

    @api
    get scheduledTimeslot() {
        return this._scheduledTimeslot
    }
    set scheduledTimeslot(value) {
        if(value) {
            this._scheduledTimeslot = value
        }
    }
    _scheduledTimeslot

    @track period = 'All' //++
    @track finalTimeSlots = [] //++

    availableTimeSlotsMap = {} //++
    dealerTimeSlotsMap = {} //++
    datesList = [] //++
    transportationAvailabilityMap //++
    rentalAvailabilityMap //++
    expressAvailabilityMap //++
    waiterAvailabilityMap //++
    dealerTimezoneShift //++
    dealerTimeWithShift //++
    dealerDate //++

    transportationTypesWithDailyCapacity = ['Loaner','Rideshare','Rental', 'Pickup & Delivery', 'Wait']
    transportationTypeDailyCapacities

    get optionsAmPm() {
        return [
            { label: 'All Day', value: 'All' },
            { label: 'AM', value: 'PM' },
            { label: 'PM', value: 'AM' },
        ]
    }

    get isTransportationCapacityRequired() {
        return this.transportationTypesWithDailyCapacity.includes(this.transportationType)
    }

    get isRentalTransportationAvailable() {
        return this.transportationType == 'Loaner' && this.rentalAutoAssigned/**|| this.isLoanerRequestedForPickup;**/
    }

    get isWaitTransportationSelected() {
        return this.transportationType == 'Wait';
    }

    get isTimslotsAvailable() {
        return !!this.finalTimeSlots
    }

    get isLoanerRequestedForPickup() {
        return this.transportationType == 'Pickup & Delivery' && this.loanerRequested;
    }

    get transportationTypeLabel() {
        return this.isLoanerRequestedForPickup ? 'Loaner' : this.transportationType;
    }

    renderedCallback(){
        this._handleAMPM()
        this.template.querySelectorAll('input[name="options"]').forEach(element => {
            element.checked = this.requestLoaner
        })
    }

    async prepareData() {
        this.datesList = this._createDateList();
        this.parseAvailableTimeslotJSON()

        // if(this.transportationType == 'Pickup & Delivery') {
        //     await this.getPickupDeliveryTimeslots()
        // } else {
        //     await this.getDealerTimeSlots()
        // }

        await this.getDealerTimeSlots()
        
        if (this.isTransportationCapacityRequired) {
            this.getTransportationTypeAvailability(this.transportationType, 'transportationAvailabilityMap')
        }
        if (this.isRentalTransportationAvailable) {
            this.getTransportationTypeAvailability('Rental', 'rentalAvailabilityMap')
        }
        if (this.expressRequested) {
            this.getTransportationTypeAvailability('Express', 'expressAvailabilityMap')
        }
        // if (this.isWaitTransportationSelected) {
        //     this.getTransportationTypeAvailability('Wait', 'waiterAvailabilityMap')
        // }
        await this.prepareDealerDateTime()
        await this.getTranstortationTypeAllowedTime() 

        this.transportationTypeDailyCapacities = await getTransportationTypeDailyCapacities({
            dealerId: this.dealer.Dealer_Code__c,
            transType: this.transportationType,
            prefDate: this.prefDate
        })
        console.log(this.transportationTypeDailyCapacities)

        this.prepareFinalSlots();
    }

    isScheduledThrowSevenMonth(date) {
        let today = new Date()
        let lastDate = today.setMonth(today.getMonth() + 7)
        
        return new Date(date) >= lastDate
    }

    parseAvailableTimeslotJSON() {
        let timeslots = JSON.parse(this.timeslotsJson)
        console.log(timeslots)
        let tempTimeSlotMap =  {}
        timeslots.forEach(element => {
            let dateString = element.StartDate.slice(0,10)
            // let resource = this.isScheduledThrowSevenMonth(dateString) ? this.dealer.FirstScheduledResource__c : this.pickupDeliveryResources

            if(this.isScheduledThrowSevenMonth(dateString)) {
                if(this.dealer.FirstScheduledResource__c && element.ResourceID == this.dealer.FirstScheduledResource__c) {
                    
                    tempTimeSlotMap[dateString] = tempTimeSlotMap[dateString] || []
                    tempTimeSlotMap[dateString].push(element)
                }
            } else {
                if((this.transportationType == 'Pickup & Delivery' 
                    && this.pickupDeliveryResources.includes(element.ResourceID) // element.ResourceID == this.pickupDeliveryResources 
                    && element.ResourceID != this.dealer.FirstScheduledResource__c) 
                    || 
                    (this.transportationType != 'Pickup & Delivery' 
                    && !this.pickupDeliveryResources.includes(element.ResourceID) // element.ResourceID != this.pickupDeliveryResources 
                    && element.ResourceID != this.dealer.FirstScheduledResource__c)) {
                    
                        tempTimeSlotMap[dateString] = tempTimeSlotMap[dateString] || []
                        tempTimeSlotMap[dateString].push(element)
                }
            }

            
            
        })
        this.availableTimeSlotsMap = tempTimeSlotMap
        console.log(JSON.stringify(this.availableTimeSlotsMap))
    }

    async getDealerTimeSlots() {
        this.dealerTimeSlotsMap = await getDealerTimeSlots({
            dealer: this.dealer.Dealer_Code__c,
            prefDate: this.prefDate
        })
        // console.log(JSON.stringify(this.dealerTimeSlotsMap))
    }

    async getPickupDeliveryTimeslots() {
        if(this.pickupDeliveryResources) {
            this.dealerTimeSlotsMap = await getPickupDeliveryTimeslots({
                dealer: this.dealer.Dealer_Code__c,
                resourceId: this.pickupDeliveryResources,
                prefDate: this.prefDate
            }) 
        }
    }

    async getTransportationTypeAvailability(transType, property) {
        let transportationAvailability = await getDealerTransportationAvailability({
            dealer: this.dealer.Dealer_Code__c,
            prefDate: this.prefDate,
            transportationType: transType
        })

        let tempTransportationAvailabilityMap = {}
        // if (!this.isWaitTransportationSelected) {
        //     this.datesList.forEach(date => {
        //         let currentTransportationAvailability = transportationAvailability[date.dateLabel] ? transportationAvailability[date.dateLabel].length : 0
        //         tempTransportationAvailabilityMap[date.dateLabel] = currentTransportationAvailability
        //     })
        // } else {
            this.datesList.forEach(date => {
                tempTransportationAvailabilityMap[date.dateLabel] = tempTransportationAvailabilityMap[date.dateLabel] || []
                let tempArray = tempTransportationAvailabilityMap[date.dateLabel]
                tempTransportationAvailabilityMap[date.dateLabel] = tempArray.concat(tempTransportationAvailabilityMap[date.dateLabel])
            })
        // }
        
        this[property] = tempTransportationAvailabilityMap
        console.log(JSON.stringify(tempTransportationAvailabilityMap))
    }

    async prepareDealerDateTime() {
        this.dealerTimezoneShift = await getDealerTimezoneShift({dealer: this.dealer.Dealer_Code__c})
            
        let dealerDatetime = new Date(new Date().getTime() + this.dealerTimezoneShift)
        this.dealerTimeWithShift = dealerDatetime
        this.dealerDate = dealerDatetime.getFullYear() + '-' 
            + (dealerDatetime.getMonth().toString().length === 1 ? '0' + (dealerDatetime.getMonth() + 1) : (dealerDatetime.getMonth() + 1)) + '-' 
            + (dealerDatetime.getDate().toString().length === 1 ? '0' + dealerDatetime.getDate() : dealerDatetime.getDate())
    }

    async getTranstortationTypeAllowedTime() {
        let tempAllowedTimeMap = await getTranstortationTypeAllowedTime({dealer: this.dealer.Dealer_Code__c, transType: this.transportationType})
        this.transTypeAllowedTime = JSON.parse(tempAllowedTimeMap)
    }

    prepareFinalSlots() {
        let tempFinalTimeslots = []
        this.datesList.forEach(date => {
            let dayOfWeek = DAYS_OF_WEEK_MAP[date.date.toUTCString().slice(0,3)]

            let transportationsScheduled = this.isTransportationCapacityRequired ? this.transportationAvailabilityMap[date.dateLabel] : null
            let transportationsAvailable // = this.isTransportationCapacityRequired ? this.getTransportationTypeCapacity() : null

            if (this.transportationTypeDailyCapacities) {
                let daillyCapacity = this.transportationTypeDailyCapacities[dayOfWeek]
                transportationsAvailable = !!daillyCapacity ? daillyCapacity : 0
            } else {
                transportationsAvailable = this.isTransportationCapacityRequired ? this.getTransportationTypeCapacity() : null
            }

            // if (this.dealer.Dealer_Code__c == 'AUDFJ' && date.date.toUTCString().slice(0,3) == 'Sat') {
            //     console.log('I WAS HERE ' + date.date)
            //     switch (this.transportationType) {
            //         case 'Loaner':
            //             transportationsAvailable = 15
            //             console.log('I WAS HERE Loaner ' + transportationsAvailable)
            //             break;

            //         case 'Rental':
            //             transportationsAvailable = 15
            //             console.log('I WAS HERE Loaner ' + transportationsAvailable)
            //             break;
            //     }
            // }   
            // if (this.dealer.Dealer_Code__c == 'AUDFR' && date.date.toUTCString().slice(0,3) == 'Sat') {
            //     switch (this.transportationType) {
            //         case 'Loaner':
            //             transportationsAvailable = 15
            //             console.log('I WAS HERE Loaner ' + transportationsAvailable)
            //             break;

            //         case 'Rental':
            //             transportationsAvailable = 6
            //             console.log('I WAS HERE Loaner ' + transportationsAvailable)
            //             break;
            //     }
            // } 
            let transportationAvailabilityLabel = transportationsAvailable == null
                ? 'No Limit'
                : (transportationsAvailable >= transportationsScheduled ? transportationsAvailable - transportationsScheduled : 0) + ' / ' + transportationsAvailable
        
            let transportationAvailability = transportationAvailabilityLabel
            
            let rentalScheduled = this.isRentalTransportationAvailable ? this.rentalAvailabilityMap[date.dateLabel] : null
            let rentalAvailable = this.isRentalTransportationAvailable ? this.dealer.RentalCapacity__c : null
            let rentalAvailability = (rentalAvailable >= rentalScheduled ? rentalAvailable - rentalScheduled : 0) 
                + ' / ' + rentalAvailable

            let expressScheduled = this.expressRequested ? this.expressAvailabilityMap[date.dateLabel] : null
            let expressAvailable = this.expressRequested ? this.dealer.ExpressCapacity__c : null
            let expressAvailability = (expressAvailable >= expressScheduled ? expressAvailable - expressScheduled : 0) 
                + ' / ' + expressAvailable

            let isDailySlotsAvailable = true
            if (this.isTransportationCapacityRequired) {
                isDailySlotsAvailable = (transportationsAvailable > transportationsScheduled) || (transportationsAvailable == null)
            }
            if (this.isRentalTransportationAvailable && !isDailySlotsAvailable) {
                isDailySlotsAvailable = rentalAvailable > rentalScheduled
            }
            if (isDailySlotsAvailable && this.expressRequested) {
                isDailySlotsAvailable = expressAvailable > expressScheduled
            }

            let utcDate = new Date(date.dateLabel).toUTCString().slice(0,-13)
            let dailySlot = {
                date: utcDate,
                transportationAvailability: transportationAvailability,
                rentalAvailability: rentalAvailability,
                expressAvailability: expressAvailability
            }
            let finalSlots = [];

            if(this.dealerTimeSlotsMap[dayOfWeek]) {
                this.dealerTimeSlotsMap[dayOfWeek].forEach(element => {
                    let tempStartDate = new Date(new Date(date.dateLabel).getTime() + new Date(element.StartTime).getTime())
                    let tempEndDate = new Date(new Date(date.dateLabel).getTime() + new Date(element.EndTime).getTime())

                    let isSlotInTransTypeAllowedTime = !this.transTypeAllowedTime || (this.transTypeAllowedTime && this.transTypeAllowedTime[dayOfWeek] && this.transTypeAllowedTime[dayOfWeek].startTime <= element.StartTime && this.transTypeAllowedTime[dayOfWeek].endTime >= element.EndTime)

                    let isDisabled = true
                    let advisorId = null
                    if(isDailySlotsAvailable && isSlotInTransTypeAllowedTime && this.availableTimeSlotsMap && this.availableTimeSlotsMap[date.dateLabel]) {
                        for (let el of this.availableTimeSlotsMap[date.dateLabel]) {
                            if (tempStartDate >= new Date(el.StartDate) && tempEndDate <= new Date(el.EndDate)) {
                                if ((this.dealerDate !== new Date(el.StartDate).toISOString().slice(0,10)) || (this.dealerDate === new Date(el.StartDate).toISOString().slice(0,10) && tempStartDate >= new Date(new Date(this.dealerTimeWithShift).getTime() + this.bufferTime*60000))) {
                                    isDisabled = false
                                        advisorId = el.ResourceID
                                        break
                                    
                                    
                                    // let waitersScheduled = 0
                                    // if(this.isWaitTransportationSelected && this.waiterAvailabilityMap[date.dateLabel]) {
                                    //     let scheduledWaitAppointmentsByDate = this.waiterAvailabilityMap[date.dateLabel]
                                    //     let slotStartTime = new Date(new Date(tempStartDate).getTime() - this.dealerTimezoneShift)
                                    //     slotStartTime.setMinutes(0)
                                    //     slotStartTime.setSeconds(0)
                                    //     slotStartTime.setMilliseconds(0)
                                    //     let hour = slotStartTime.getHours()
                                    //     let slotEndTime = new Date(slotStartTime.getTime())
                                    //     slotEndTime.setHours(hour + 1)

                                    //     waitersScheduled = scheduledWaitAppointmentsByDate.filter(appointment => 
                                    //         slotStartTime <= new Date(appointment.ServiceAppointment.SchedStartTime) &&
                                    //         slotEndTime > new Date(appointment.ServiceAppointment.SchedStartTime)
                                    //     ).length
                                    // }
                                    
                                    // if(!this.isWaitTransportationSelected || (this.isWaitTransportationSelected && waitersScheduled < this.waitCapacity)) {
                                    //     isDisabled = false
                                    //     advisorId = el.ResourceID
                                    //     break
                                    // }
                                }
                            }
                        }
                    }

                    let transportationType = this.isRentalTransportationAvailable && transportationsAvailable <= transportationsScheduled ? 'Rental': this.transportationType;

                    let start = new Date(element.StartTime).toISOString();
                    start = start.slice(11, 16);
                    let end = new Date(element.EndTime).toISOString();
                    end = end.slice(11, 16);

                    if (this.scheduledTimeslot &&
                        this.scheduledTimeslot.ScheduledDate === date.dateLabel &&
                        this.scheduledTimeslot.StartTime === element.StartTime && isDisabled) {
                            this.scheduledTimeslot = {}
                            this.dispatchEvent(
                                new CustomEvent('timeslotselected', {
                                    detail: {
                                        startDate: null,
                                        endDate: null,
                                        resourceId: null,
                                        prefDate: null,
                                        transportationType: this.transportationType
                                    }
                                })
                            );
                    }

                    finalSlots.push({
                        start: start,
                        end: end,
                        isDisabled: isDisabled,
                        label: this._formatAMPM(start),
                        resId: advisorId,
                        startTime: element.StartTime,
                        date: date.dateLabel,
                        class: (this.scheduledTimeslot &&
                            this.scheduledTimeslot.ScheduledDate === date.dateLabel &&
                            // this.scheduledTimeslot.SelectedAdvisor === advisor &&
                            this.scheduledTimeslot.StartTime === element.StartTime
                        ) ? "slds-button slds-button_brand" : "slds-button",
                        transportationType: transportationType,
                        ampm: this._isAMorPM(start),
                        // dateString: this.prepareFinalDateString(date.dateLabel, this._formatAMPM(start))
                    });
                })
                finalSlots.sort(this._compareSlots)
            }
            dailySlot['slots'] = finalSlots
            tempFinalTimeslots.push(dailySlot)
            // console.log(transportationAvailability + ' || ' + rentalAvailability + ' || ' + expressAvailability);
        })
        this.finalTimeSlots = tempFinalTimeslots

        this.dispatchEvent( new CustomEvent('loaded', { detail: true }) )
    }

    getTransportationTypeCapacity() {
        switch (this.transportationType) {
            case 'Loaner':
            case 'Pickup & Delivery':
                return this.dealer.LoanerCapacity__c
            case 'Rental':
                return this.dealer.RentalCapacity__c
            case 'Rideshare':
                return this.dealer.RideshareCapacity__c
            case 'Wait':
                return this.dealer.WaitCapacity__c
        }
    }


    
    handleDayPeriodChange(event) {
        this.period =  event.detail.value
        this._handleAMPM()
    }

    handleDateChange(event) {
        if(event.detail.value) {
            this.prefDate = event.detail.value;

            let dateAvailable = new Date(this.prefDate) >= new Date(this.earliestStartDate);
            
            if(dateAvailable) {
                this.dispatchEvent(
                    new CustomEvent('gettimeslots', {
                        detail: {
                            date: this.prefDate,
                            // advisor: this.prefAdvisor
                        }
                    })
                );
            } else {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Wrong Date chosen',
                        message: 'message',
                        variant: 'error',
                    })
                );
            }
        } else {
            event.currentTarget.value = this.prefDate;
        }
    }

    handleSchedule(event) {
        this._clearTimeslotSelection();
        console.log("$#@$%@$")

        const resourceId = event.currentTarget.dataset.resId;
        const selectedTimeslot = this.finalTimeSlots[event.currentTarget.dataset.dayindex].slots[event.currentTarget.dataset.index];
        selectedTimeslot.class = "slds-button slds-button_brand";
        
        this.template.querySelectorAll('button').forEach(element => {
            element.className = 'slds-button';
        });

        event.target.className += " slds-button_brand";

        this.scheduledTimeslot = {
            SelectedAdvisor: selectedTimeslot.resId,
            ScheduledDate: selectedTimeslot.date,
            ScheduledStartTime: selectedTimeslot.start,
            ScheduledEndTime: selectedTimeslot.end,
            StartTime: selectedTimeslot.startTime
        };

        // this.selectedTimeslotFormatedDate = selectedTimeslot.dateString
        
        this.dispatchEvent(
            new CustomEvent('timeslotselected', {
                detail: {
                    startDate: selectedTimeslot.start,
                    endDate: selectedTimeslot.end,
                    resourceId: resourceId,
                    prefDate: selectedTimeslot.date,
                    transportationType: selectedTimeslot.transportationType,
                    // requestLoaner: this.requestLoaner
                }
            })
        );

        if (this.transportationType == 'Loaner' && selectedTimeslot.transportationType == 'Rental') {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Rental Auto Assigned',
                    mode: 'sticky'
                    // message: 'message',
                    // variant: 'info'
                })
            )
        }
    }

    handleLoanerRequestChange(event) {
        console.log('TABLE');
        this.requestLoaner = event.target.checked;
        this.dispatchEvent( new CustomEvent('loanerrequested', {detail: this.requestLoaner}) )
    }

    _clearTimeslotSelection() {
        // Object.keys(this.finalTimeSlots).forEach(advisorId => {
        //     let advisor = this.finalTimeSlots[advisorId];
        //     advisor.forEach(day => {
        //         day.slots.forEach(slot => {
        //             slot.class = "slds-button";
        //         });
        //     });
        // });

        this.finalTimeSlots.forEach(day => {
            day.slots.forEach(slot => {
                slot.class = "slds-button"
            })
        })

    }

    _handleAMPM(){
        let selector = "button[data-ampm]"
        this.template.querySelectorAll(selector).forEach(element => {
            if (element.getAttribute('data-ampm') != this.period) {
                element.style.display = null
            } else {
                element.style.display = 'none'
            }
        })
    }

    _createDateList() {
        let dateString = this.prefDate
        let firstDate = new Date(dateString)
        let datesList = [{dateLabel: dateString, date: firstDate}]
        
        for ( let i = 1; i < 5; i++ ) {
            let nextDate = new Date(dateString)
            nextDate = new Date(nextDate.setDate(nextDate.getDate() + i))
            let dateLabel = nextDate.toISOString()
            dateLabel = dateLabel.slice(0,10)
            datesList.push({dateLabel: dateLabel, date: nextDate})
        }

        return datesList
    }

    _formatAMPM(timeString) {
        let splits = timeString.split(':');
        let hours = splits[0];
        let minutes = splits[1];
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 && minutes != '00' ? '0'+minutes : minutes;
        let strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    _isAMorPM(timeString) {
        let splits = timeString.split(':');
        let hours = splits[0];
        let ampm = hours >= 12 ? 'PM' : 'AM';
        return ampm
    }

    _compareSlots(a, b) {
        if (a.startTime < b.startTime) {
            return -1;
        }
        if (a.startTime > b.startTime) {
            return 1;
        }
        return 0;
    }
}