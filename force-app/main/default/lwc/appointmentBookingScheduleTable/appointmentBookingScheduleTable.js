import { LightningElement, api, track } from 'lwc';

import getAdvisorTimeSlots from '@salesforce/apex/AppointmentBookingController.getAdvisorTimeSlots';
import getAdvisorsAvailability from '@salesforce/apex/AppointmentBookingController.getAdvisorsAvailability';
import getDealerTimezoneShift from '@salesforce/apex/AppointmentBookingController.getDealerTimezoneShift';
import getTranstortationTypeAllowedTime from '@salesforce/apex/AppointmentBookingController.getTranstortationTypeAllowedTime';
import getTransportationTypeDailyCapacities from '@salesforce/apex/AppointmentBookingController.getTransportationTypeDailyCapacities';
import { ShowToastEvent }       from 'lightning/platformShowToastEvent';

const DAYS_OF_WEEK_MAP = {'Sun':'Sunday', 'Mon':'Monday', 'Tue':'Tuesday', 'Wed':'Wednesday', 'Thu':'Thursday', 'Fri':'Friday', 'Sat':'Saturday'};
// const MONTHS_MAP = {'Jan':'January', 'Feb':'February', 'Mar':'March', 'Apr':'April', 'May':'May', 'Jun':'June', 'Jul':'July', 
//     'Aug':'August', 'Sep':'September', 'Oct':'October', 'Nov':'November', 'Dec':'December'};

export default class AppointmentBookingScheduleTable extends LightningElement {
    @api 
    get timeslotsJson() {
        return this._timeslotsJson;
    }
    set timeslotsJson(value) {
        this._timeslotsJson = value;
        if (value) {
            this.advisors = [];
            this.finalTimeSlots = {};
            this.availableTimeSlotsMap = {};
            this.advisorTimeSlotsMap = {};

            this.parseAvailableTimeslotJSON();
        }
    }
    _timeslotsJson;
    
    @api
    get scheduledTimeslot() {
        return this._scheduledTimeslot;
    }
    set scheduledTimeslot(value) {
        if(value) {
            this._scheduledTimeslot = value;
            // this.selectedTimeslotFormatedDate = this.prepareFinalDateString(value.ScheduledDate, this._formatAMPM(value.ScheduledStartTime))
        }
    }
    _scheduledTimeslot;

    // selectedTimeslotFormatedDate;

    @api advisorsMap = {};
    @api dealer;
    @api prefAdvisor;
    @api prefDate;
    @api serviceAppointmentId;
    @api earliestStartDate;
    @api bufferTime;
    @api transportationType;
    @api waitCapacity;
    @api loanerRequested;
    @api expressRequested;

    // @api loanerApprovalRequested = false;
    @api 
    get loanerApprovalRequested() {
        return this._loanerApprovalRequested
    }
    set loanerApprovalRequested(value) {
        console.log('loanerApprovalRequested table');
        this._loanerApprovalRequested = value
        this.template.querySelectorAll('input[name="options"]').forEach(element => {
            element.checked = value
        })
    }
    _loanerApprovalRequested
    
    @track advisorPicklistOptions = [];
    @track finalTimeSlots = {};
    @api requestLoaner
    @api rentalAutoAssigned
    @api pickupDeliveryResources
    // @track requestLoaner = this.loanerApprovalRequested;
    
    advisors = [];
    availableTimeSlotsMap = {};
    advisorTimeSlotsMap = {};
    transportationAvailabilityMap;
    rentalAvailabilityMap;
    advisorAvailabilityMap;
    dealerTimezoneShift;
    dealerTimeWithShift;
    dealerDate;
    dealerDatetime;
    waiterAvailabilityMap;
    expressAvailabilityMap;

    // transportationCapacityPerDay;
    transportationTypeDailyCapacities

    // @track availabilityList = [];

    get advisorTimslots() {
        return this.finalTimeSlots[this.prefAdvisor];
    }

    get isAdvisorAvailable() {
        return Object.keys(this.finalTimeSlots).length !== 0;
    }

    get isAdvisorTimslotsAvailable() {
        return !!this.finalTimeSlots[this.prefAdvisor];
    }

    get isTransportationCapacityRequired() {
        // 12-10-2020
        // return ['Loaner','Rideshare','Rental'].includes(this.transportationType) || this.isLoanerRequestedForPickup; 
        return ['Loaner','Rideshare','Rental', 'Pickup & Delivery', 'Wait'].includes(this.transportationType)
    }

    get isRentalTransportationAvailable() {
        // 12-10-2020
        // return this.transportationType == 'Rental' || this.transportationType == 'Loaner' || this.isLoanerRequestedForPickup;
        return (this.transportationType == 'Loaner' || this.isLoanerRequestedForPickup) && this.rentalAutoAssigned;
    }

    get isWaitTransportationSelected() {
        return this.transportationType == 'Wait';
    }

    get isLoanerRequestedForPickup() {
        return this.transportationType == 'Pickup & Delivery' && this.loanerRequested;
    }

    get transportationTypeLabel() {
        return this.isLoanerRequestedForPickup ? 'Loaner' : this.transportationType;
        // return this.isLoanerRequestedForPickup ? 'Loaner' : (this.transportationType == 'Rental' ? 'Waiter' : this.transportationType);
    }

    renderedCallback(){
        console.log('renderedCallback table');
        this.handleAMPM()
        this.template.querySelectorAll('input[name="options"]').forEach(element => {
            element.checked = this.requestLoaner
        })
    }

    parseAvailableTimeslotJSON() {
        console.log('@@@@@@@@@@');
        // Prepare Available Timeslots Map
        const timeslots = JSON.parse(this.timeslotsJson);
        let tempTimeSlotMap =  {};
        timeslots.forEach(element => {
            if((this.transportationType == 'Pickup & Delivery' && this.pickupDeliveryResources.includes(element.ResourceID)/*element.ResourceID == this.pickupDeliveryResources*/ ) || 
                (this.transportationType != 'Pickup & Delivery' && !this.pickupDeliveryResources.includes(element.ResourceID)/*element.ResourceID != this.pickupDeliveryResources*/)) {
                    tempTimeSlotMap[element.ResourceID] = tempTimeSlotMap[element.ResourceID] || {};
                    let dateString = element.StartDate.slice(0,10);
                    tempTimeSlotMap[element.ResourceID][dateString] = tempTimeSlotMap[element.ResourceID][dateString] || [];
                    tempTimeSlotMap[element.ResourceID][dateString].push(element);
            }
        });
        this.availableTimeSlotsMap = tempTimeSlotMap;

        // Prepare List of Available Advisors
        let advList = [];
        Object.keys(this.availableTimeSlotsMap).forEach(element => {
            if(this.advisorsMap[element]) {
                advList.push(element);
            }
        })
        this.advisors = advList;

        this.getAdvisorsTimeSlots();
    }

    getAdvisorsTimeSlots = async () => {
        this.advisorTimeSlotsMap = await getAdvisorTimeSlots({
            advisorIdsJSON: JSON.stringify(this.advisors),
            dealer: this.dealer,
            prefDate: this.prefDate
        });
        // console.log(JSON.stringify(this.advisorTimeSlotsMap));

        // -------------------------------
        let advisorDailyAvailability = await getAdvisorsAvailability({
            advisorIdsJSON: JSON.stringify(this.advisors),
            prefDate: this.prefDate,
            transportationType: null
        });
        this.createAdvisorDailyAvailabilityMap(advisorDailyAvailability);
        
        // ------------------------------
        if(this.isTransportationCapacityRequired){
            // 12-10-2020
            // let trnsType = this.isLoanerRequestedForPickup ? 'Loaner' : this.transportationType;
            let transportationAvailability = await getAdvisorsAvailability({
                advisorIdsJSON: JSON.stringify(this.advisors),
                prefDate: this.prefDate,
                transportationType: this.transportationType
            });

            console.log('~~~~~~')

            
            // console.log('result: ')
            // console.log(transportationAvailability)
            let datesList = this.createDateList();

            let tempTransportationAvailabilityMap = {};

            // if (this.transportationType == 'Loaner') {
            //     this.advisors.forEach(advisor => {
            //         tempTransportationAvailabilityMap[advisor] = {};
            //         datesList.forEach(date => {
            //             let currentTransportationAvailability = transportationAvailability[advisor] && transportationAvailability[advisor][date.dateLabel] ? transportationAvailability[advisor][date.dateLabel].length : 0;
            //             tempTransportationAvailabilityMap[advisor][date.dateLabel] = currentTransportationAvailability;
            //         });
            //     });
            // } else {
                // datesList.forEach(date => {
                //     let daySumuary = 0
                //     this.advisors.forEach(advisor => {
                //         if (transportationAvailability[advisor] && transportationAvailability[advisor][date.dateLabel]) {
                //             daySumuary += transportationAvailability[advisor][date.dateLabel].length
                //         }
                //         tempTransportationAvailabilityMap[advisor] =  tempTransportationAvailabilityMap[advisor] ?  tempTransportationAvailabilityMap[advisor] : {}
                //         tempTransportationAvailabilityMap[advisor][date.dateLabel] = daySumuary;
                //     })
                // });
            // }

            datesList.forEach(date => {
                let daySumuary = 0
                this.advisors.forEach(advisor => {
                    if (transportationAvailability[advisor] && transportationAvailability[advisor][date.dateLabel]) {
                        daySumuary += transportationAvailability[advisor][date.dateLabel].length
                    }
                    tempTransportationAvailabilityMap[advisor] =  tempTransportationAvailabilityMap[advisor] ?  tempTransportationAvailabilityMap[advisor] : {}
                    tempTransportationAvailabilityMap[advisor][date.dateLabel] = daySumuary;
                })
                
                tempTransportationAvailabilityMap[date.dateLabel] = daySumuary;
            });
            
            // console.log('tempTransportationAvailabilityMap')
            // console.log(JSON.stringify(tempTransportationAvailabilityMap))

            this.transportationAvailabilityMap = tempTransportationAvailabilityMap;
            // console.log(this.transportationAvailabilityMap)

            this.transportationTypeDailyCapacities = await getTransportationTypeDailyCapacities({
                dealerId: this.dealer,
                transType: this.transportationType,
                prefDate: this.prefDate
            })
        }

        // ------------------------------
        if(this.transportationType == 'Loaner' || this.isLoanerRequestedForPickup && this.rentalAutoAssigned){
            let rentalAvailability = await getAdvisorsAvailability({
                advisorIdsJSON: JSON.stringify(this.advisors),
                prefDate: this.prefDate,
                transportationType: 'Rental'
            });

            let datesList = this.createDateList();

            let tempRentalAvailabilityMap = {};
            // datesList.forEach(date => {
            //     let daySumuary = 0
            //     this.advisors.forEach(advisor => {
            //         if (rentalAvailability[advisor] && rentalAvailability[advisor][date.dateLabel]) {
            //             daySumuary += rentalAvailability[advisor][date.dateLabel].length
            //         }
            //         tempRentalAvailabilityMap[advisor] =  tempRentalAvailabilityMap[advisor] ?  tempRentalAvailabilityMap[advisor] : {}
            //         tempRentalAvailabilityMap[advisor][date.dateLabel] = daySumuary;
            //     })
            // });
            datesList.forEach(date => {
                let daySumuary = 0
                this.advisors.forEach(advisor => {
                    if (rentalAvailability[advisor] && rentalAvailability[advisor][date.dateLabel]) {
                        daySumuary += rentalAvailability[advisor][date.dateLabel].length
                    }
                })
                tempRentalAvailabilityMap[date.dateLabel] = daySumuary;
            });

            this.rentalAvailabilityMap = tempRentalAvailabilityMap;
        }
        // ------------------------------
        if(this.expressRequested){
            let expressAvailability = await getAdvisorsAvailability({
                advisorIdsJSON: JSON.stringify(this.advisors),
                prefDate: this.prefDate,
                transportationType: 'Express'
            });

            let datesList = this.createDateList();

            let tempExpressAvailabilityMap = {};
            // this.advisors.forEach(advisor => {
            //     tempExpressAvailabilityMap[advisor] = {};
            //     datesList.forEach(date => {
            //         let currentExpressAvailability = expressAvailability[advisor] && expressAvailability[advisor][date.dateLabel] ? expressAvailability[advisor][date.dateLabel].length : 0;
            //         tempExpressAvailabilityMap[advisor][date.dateLabel] = currentExpressAvailability;
            //     });
            // });
            
            // datesList.forEach(date => {
            //     let daySumuary = 0
            //     this.advisors.forEach(advisor => {
            //         if (expressAvailability[advisor] && expressAvailability[advisor][date.dateLabel]) {
            //             daySumuary += expressAvailability[advisor][date.dateLabel].length
            //         }
            //         tempExpressAvailabilityMap[advisor] =  tempExpressAvailabilityMap[advisor] ?  tempExpressAvailabilityMap[advisor] : {}
            //         tempExpressAvailabilityMap[advisor][date.dateLabel] = daySumuary;
            //     })
            // });
            datesList.forEach(date => {
                let daySumuary = 0
                this.advisors.forEach(advisor => {
                    if (expressAvailability[advisor] && expressAvailability[advisor][date.dateLabel]) {
                        daySumuary += expressAvailability[advisor][date.dateLabel].length
                    }
                })
                tempExpressAvailabilityMap[date.dateLabel] = daySumuary;
            });

            this.expressAvailabilityMap = tempExpressAvailabilityMap;
        }
        // ------------------------------
        // WAIT PER HOUR CAPACITY LOGIC 
        // 
        // 
        // if(this.isWaitTransportationSelected){
        //     let tempWaiterAvailabilityMap = await getAdvisorsAvailability({
        //         advisorIdsJSON: JSON.stringify(this.advisors),
        //         prefDate: this.prefDate,
        //         transportationType: this.transportationType
        //     });

        //     // console.log(JSON.stringify(tempWaiterAvailabilityMap));
            
        //     let tempWaitAppointmentsByDate = {};
        //     Object.keys(tempWaiterAvailabilityMap).forEach(advisor => {
        //         Object.keys(tempWaiterAvailabilityMap[advisor]).forEach(date => {
        //             tempWaitAppointmentsByDate[date] = tempWaitAppointmentsByDate[date] || [];
        //             let tempArray = tempWaitAppointmentsByDate[date];
        //             tempWaitAppointmentsByDate[date] = tempArray.concat(tempWaiterAvailabilityMap[advisor][date]);
        //         });
        //     });
            
        //     // console.log(JSON.stringify(tempWaitAppointmentsByDate))
        //     this.waiterAvailabilityMap = tempWaitAppointmentsByDate;
        // }

        // ------------------------------

        this.dealerTimezoneShift = await getDealerTimezoneShift({dealer: this.dealer})
            
        let dealerDatetime = new Date(new Date().getTime() + this.dealerTimezoneShift);
        this.dealerTimeWithShift = dealerDatetime;
        this.dealerDate = dealerDatetime.getFullYear() + '-' 
            + (dealerDatetime.getMonth().toString().length === 1 ? '0' + (dealerDatetime.getMonth() + 1) : (dealerDatetime.getMonth() + 1)) + '-' 
            + (dealerDatetime.getDate().toString().length === 1 ? '0' + dealerDatetime.getDate() : dealerDatetime.getDate());

        // ------------------------------

        this.transTypeAllowedTime = JSON.parse(await getTranstortationTypeAllowedTime({dealer: this.dealer, transType: this.transportationType}))
        console.log(this.transTypeAllowedTime)

        // ------------------------------

        this.prepareFinalSlots();
    }

    transTypeAllowedTime

    createAdvisorDailyAvailabilityMap(availabilityMap) {
        let datesList = this.createDateList();

        let tempAdvisorAvailabilityMap = {};
        let tempAvailabilityList = [];

        this.advisors.forEach(advisor => {
            tempAdvisorAvailabilityMap[advisor] = {};
            datesList.forEach(date => {
                let currentLoanerAvailability = availabilityMap[advisor] && availabilityMap[advisor][date.dateLabel] ? availabilityMap[advisor][date.dateLabel].length : 0;
                tempAdvisorAvailabilityMap[advisor][date.dateLabel] = currentLoanerAvailability;

                if(date.dateLabel == this.prefDate) {
                    let capacity = this.advisorsMap[advisor].DailyCapacity__c;

                    tempAvailabilityList.push({
                        resId: advisor,
                        availability: currentLoanerAvailability / (capacity ? capacity : 100),
                    })
                }
            });
        });

        this.advisorAvailabilityMap = tempAdvisorAvailabilityMap;

        tempAvailabilityList.sort(this._compareAvailabilities);

        let tempOptionsList = [];
        tempAvailabilityList.forEach(advisor => {
            tempOptionsList.push({label: this.advisorsMap[advisor.resId].Name, value: advisor.resId});
        });

        if(!this.advisors.includes(this.prefAdvisor) && tempAvailabilityList.length > 0) {
            this.prefAdvisor = tempAvailabilityList[0].resId;
        }

        this.advisorPicklistOptions = tempOptionsList;
    }

    createDateList() {
        let dateString = this.prefDate;
        let firstDate = new Date(dateString);
        let datesList = [{dateLabel: dateString, date: firstDate}];
        
        for ( let i = 1; i < 5; i++ ) {
            let nextDate = new Date(dateString);
            nextDate = new Date(nextDate.setDate(nextDate.getDate() + i));
            let dateLabel = nextDate.toISOString();
            dateLabel = dateLabel.slice(0,10);
            datesList.push({dateLabel: dateLabel, date: nextDate});
        }

        return datesList;
    }

    prepareFinalSlots() {
        let tempFinalTimeslotsMap = {};
        let datesList = this.createDateList();

        this.advisors.forEach( advisor => {
            const slots = this.advisorTimeSlotsMap[advisor];
            const avSlots = this.availableTimeSlotsMap[advisor];
        
            let advisor5DaysTimeslots = [];
            datesList.forEach( date => {
                let advisorAvailability = this.advisorAvailabilityMap[advisor][date.dateLabel];
                let dailyCapasity = this.advisorsMap[advisor].DailyCapacity__c ? this.advisorsMap[advisor].DailyCapacity__c : 0;
                let dayOfWeek = DAYS_OF_WEEK_MAP[date.date.toUTCString().slice(0,3)] ;

                let transportationsScheduled;
                let transportationsAvailable;
                // console.log(this.advisorsMap[advisor].RentalCapacity);
                if(this.isTransportationCapacityRequired) {
                    // transportationsScheduled = this.transportationAvailabilityMap[advisor][date.dateLabel];
                    transportationsScheduled = this.transportationAvailabilityMap[date.dateLabel];

                    if (this.transportationTypeDailyCapacities) {
                        let daillyCapacity = this.transportationTypeDailyCapacities[dayOfWeek]
                        // transportationsAvailable = !!daillyCapacity ? daillyCapacity : 0
                        transportationsAvailable = daillyCapacity
                    } else {
                        switch (this.transportationType) {
                            case 'Loaner':
                            case 'Pickup & Delivery':
                                transportationsAvailable = this.advisorsMap[advisor].DailyLoanerAvailability__c;
                                break;
                            case 'Rental':
                                transportationsAvailable = this.advisorsMap[advisor].RentalCapacity;
                                break;
                            case 'Rideshare':
                                transportationsAvailable = this.advisorsMap[advisor].RideshareCapacity;
                                break;
                            case 'Wait':
                                transportationsAvailable = this.advisorsMap[advisor].WaitCapacity;
                                break;
                        }
                    }

                    // if (this.dealer == 'BHAUD' && date.date.toUTCString().slice(0,3) == 'Sat') {
                    //     switch (this.transportationType) {
                    //         case 'Loaner':
                    //             transportationsAvailable = 20
                    //             break;

                    //         case 'Rental':
                    //             transportationsAvailable = 9
                    //             break;
                    //     }
                    // }                  

                }

                let rentalScheduled;
                let rentalAvailable;

                if(this.isRentalTransportationAvailable) {
                    // rentalScheduled = this.rentalAvailabilityMap[advisor][date.dateLabel];
                    rentalScheduled = this.rentalAvailabilityMap[date.dateLabel];
                    rentalAvailable = this.advisorsMap[advisor].RentalCapacity;
                }

                let expressScheduled;
                let expressAvailable;

                if(this.expressRequested) {
                    // expressScheduled = this.expressAvailabilityMap[advisor][date.dateLabel];
                    expressScheduled = this.expressAvailabilityMap[date.dateLabel];
                    expressAvailable = this.advisorsMap[advisor].ExpressCapacity;
                }

                // let loanerScheduled = this.transportationAvailabilityMap[advisor][date.dateLabel];
                // let loanerAvailable = this.advisorsMap[advisor].DailyLoanerAvailability__c;

                let transportationAvailabilityLabel = transportationsAvailable == null
                    ? 'No Limit'
                    : (transportationsAvailable >= transportationsScheduled ? transportationsAvailable - transportationsScheduled : 0) + ' / ' + transportationsAvailable
                
                let dailySlot = {
                    date: new Date(date.dateLabel).toUTCString().slice(0,-13),
                    transportationAvailability: transportationAvailabilityLabel,
                    dailyCapasity: (dailyCapasity >= advisorAvailability ? dailyCapasity - advisorAvailability : 0) 
                        + ' / ' + dailyCapasity,
                    rentalAvailability: (rentalAvailable >= rentalScheduled ? rentalAvailable - rentalScheduled : 0) 
                        + ' / ' + rentalAvailable,
                    expressAvailability: (expressAvailable >= expressScheduled ? expressAvailable - expressScheduled : 0) 
                        + ' / ' + expressAvailable
                }
                let finalSlots = [];
                
                if(slots[dayOfWeek]) {
                    let isDailySlotsAvailable = true
                    if (this.isTransportationCapacityRequired) {
                        isDailySlotsAvailable = (transportationsAvailable > transportationsScheduled) || (transportationsAvailable == null)
                    }
                    if (this.isRentalTransportationAvailable && !isDailySlotsAvailable) {
                        isDailySlotsAvailable = rentalAvailable > rentalScheduled
                    }
                    if (isDailySlotsAvailable && this.expressRequested) {
                        isDailySlotsAvailable = expressAvailable > expressScheduled
                    }
                    slots[dayOfWeek].forEach(element => {
                        let tempStartDate = new Date(new Date(date.dateLabel).getTime() + new Date(element.StartTime).getTime())
                        let tempEndDate = new Date(new Date(date.dateLabel).getTime() + new Date(element.EndTime).getTime())
                        
                        let isDisabled = true;
                        if (dailyCapasity > advisorAvailability) { 
                            if ( isDailySlotsAvailable ) {
                                if (!this.transTypeAllowedTime || (this.transTypeAllowedTime && this.transTypeAllowedTime[dayOfWeek] && this.transTypeAllowedTime[dayOfWeek].startTime <= element.StartTime && this.transTypeAllowedTime[dayOfWeek].endTime >= element.EndTime)) {
                                    if(avSlots[date.dateLabel]) {
                                        for (let el of avSlots[date.dateLabel]) {
                                            if (tempStartDate >= new Date(el.StartDate) && tempEndDate <= new Date(el.EndDate)) {
                                                if ((this.dealerDate !== new Date(el.StartDate).toISOString().slice(0,10)) || (this.dealerDate === new Date(el.StartDate).toISOString().slice(0,10) && tempStartDate >= new Date(new Date(this.dealerTimeWithShift).getTime() + this.bufferTime*60000))) {
                                                    isDisabled = false;
                                                    break;
                                                    
                                                    // WAIT PER HOUR CAPACITY LOGIC
                                                    // 
                                                    // 
                                                    // let waitersScheduled = 0;
                                                    // if(this.isWaitTransportationSelected && this.waiterAvailabilityMap[date.dateLabel]) {
                                                    //     let scheduledWaitAppointmentsByDate = this.waiterAvailabilityMap[date.dateLabel];
                                                    //     let slotStartTime = new Date(new Date(tempStartDate).getTime() - this.dealerTimezoneShift);
                                                    //     slotStartTime.setMinutes(0);
                                                    //     slotStartTime.setSeconds(0);
                                                    //     slotStartTime.setMilliseconds(0);
                                                    //     let hour = slotStartTime.getHours();
                                                    //     let slotEndTime = new Date(slotStartTime.getTime());
                                                    //     slotEndTime.setHours(hour + 1);

                                                    //     waitersScheduled = scheduledWaitAppointmentsByDate.filter(appointment => 
                                                    //         slotStartTime <= new Date(appointment.ServiceAppointment.SchedStartTime) &&
                                                    //         slotEndTime > new Date(appointment.ServiceAppointment.SchedStartTime)
                                                    //     ).length;

                                                    //     // console.log('~~~ ' + slotStartTime + ' - ' + slotEndTime);
                                                    //     // console.log('~~~ waitersScheduled ' + waitersScheduled);
                                                    // }
                                                    
                                                    // if(!this.isWaitTransportationSelected || (this.isWaitTransportationSelected && waitersScheduled < this.waitCapacity)) {
                                                    //     isDisabled = false;
                                                    //     break;
                                                    // }
                                                }
                                            }
                                        }
                                    }
                                }
                            } 
                        }

                        let transportationType = this.isRentalTransportationAvailable && transportationsAvailable <= transportationsScheduled ? 'Rental': this.transportationType;

                        let start = new Date(element.StartTime).toISOString();
                        start = start.slice(11, 16);
                        let end = new Date(element.EndTime).toISOString();
                        end = end.slice(11, 16);

                        if (this.scheduledTimeslot &&
                            this.scheduledTimeslot.ScheduledDate === date.dateLabel &&
                            this.scheduledTimeslot.SelectedAdvisor === advisor &&
                            this.scheduledTimeslot.StartTime === element.StartTime && isDisabled) {
                                this.scheduledTimeslot = {}
                                this.dispatchEvent(
                                    new CustomEvent('timeslotselected', {
                                        detail: {
                                            startDate: null,
                                            endDate: null,
                                            resourceId: null,
                                            prefDate: null,
                                            transportationType: this.transportationType
                                        }
                                    })
                                );
                        }

                        finalSlots.push({
                            start: start,
                            end: end,
                            isDisabled: isDisabled,
                            label: this._formatAMPM(start),
                            resId: advisor,
                            startTime: element.StartTime,
                            date: date.dateLabel,
                            class: (this.scheduledTimeslot &&
                                this.scheduledTimeslot.ScheduledDate === date.dateLabel &&
                                this.scheduledTimeslot.SelectedAdvisor === advisor &&
                                this.scheduledTimeslot.StartTime === element.StartTime
                            ) ? "slds-button slds-button_brand" : "slds-button",
                            transportationType: transportationType,
                            ampm: this._isAMorPM(start),
                            // dateString: this.prepareFinalDateString(date.dateLabel, this._formatAMPM(start))
                        });
                    })
                    finalSlots.sort(this._compareSlots);
                }
                dailySlot['slots'] = finalSlots;
                advisor5DaysTimeslots.push(dailySlot);
            })
            tempFinalTimeslotsMap[advisor] = advisor5DaysTimeslots;
        })
        this.finalTimeSlots = tempFinalTimeslotsMap;

        this.dispatchEvent(
            new CustomEvent('loaded', {
                detail: true
            })
        );
    }

    prepareFinalDateString(dateString, timeString) {
        let utcDateString = new Date(dateString).toUTCString().slice(0,-13)
        let dayOfWeek = DAYS_OF_WEEK_MAP[utcDateString.slice(0, 3)]
        let day = utcDateString.slice(5,7)
        let month = MONTHS_MAP[utcDateString.slice(8,11)]
        let year = utcDateString.slice(12)

        return dayOfWeek + ", " + month + " " + day + ", " + year + " " + timeString
    }

    handlePrefAdvisorSelect(event) {
        this.prefAdvisor = event.detail.value;

        this.template.querySelectorAll('button').forEach(element => {
            element.className = 'slds-button';
        });
    }

    handleAMPM(){
        // console.log('~~!!~~');
        let selector = "button[data-ampm]"
        this.template.querySelectorAll(selector).forEach(element => {
            if (element.getAttribute('data-ampm') != this.period) {
                element.style.display = null;
            } else {
                element.style.display = 'none';
            }
        });
    }

    @track period = 'All'
    get optionsAmPm() {
        return [
            { label: 'All Day', value: 'All' },
            { label: 'AM', value: 'PM' },
            { label: 'PM', value: 'AM' },
        ];
    }
    handleDayPeriodChange(event) {
        this.period =  event.detail.value
        this.handleAMPM()
    }

    handleDateChange(event) {
        if(event.detail.value) {
            this.prefDate = event.detail.value;

            let dateAvailable = new Date(this.prefDate) >= new Date(this.earliestStartDate);
            
            if(dateAvailable) {
                this.dispatchEvent(
                    new CustomEvent('gettimeslots', {
                        detail: {
                            date: this.prefDate,
                            // advisor: this.prefAdvisor
                        }
                    })
                );
            } else {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Wrong Date chosen',
                        message: 'message',
                        variant: 'error',
                    })
                );
            }
        } else {
            event.currentTarget.value = this.prefDate;
        }
    }

    handleSchedule(event) {
        this._clearTimeslotSelection();
        // console.log("$#@$%@$")

        const resourceId = event.currentTarget.dataset.resId;
        const selectedTimeslot = this.advisorTimslots[event.currentTarget.dataset.dayindex].slots[event.currentTarget.dataset.index];
        selectedTimeslot.class = "slds-button slds-button_brand";
        
        this.template.querySelectorAll('button').forEach(element => {
            element.className = 'slds-button';
        });

        event.target.className += " slds-button_brand";

        this.scheduledTimeslot = {
            SelectedAdvisor: selectedTimeslot.resId,
            ScheduledDate: selectedTimeslot.date,
            ScheduledStartTime: selectedTimeslot.start,
            ScheduledEndTime: selectedTimeslot.end,
            StartTime: selectedTimeslot.startTime
        };

        // this.selectedTimeslotFormatedDate = selectedTimeslot.dateString
        
        this.dispatchEvent(
            new CustomEvent('timeslotselected', {
                detail: {
                    startDate: selectedTimeslot.start,
                    endDate: selectedTimeslot.end,
                    resourceId: resourceId,
                    prefDate: selectedTimeslot.date,
                    transportationType: selectedTimeslot.transportationType,
                    // requestLoaner: this.requestLoaner
                }
            })
        );

        if (this.transportationType == 'Loaner' && selectedTimeslot.transportationType == 'Rental') {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Rental Auto Assigned',
                    mode: 'sticky'
                    // message: 'message',
                    // variant: 'info'
                })
            )
        }
    }

    handleLoanerRequestChange(event) {
        console.log('TABLE');
        this.requestLoaner = event.target.checked;
        this.dispatchEvent( new CustomEvent('loanerrequested', {detail: this.requestLoaner}) )
    }

    _clearTimeslotSelection() {
        Object.keys(this.finalTimeSlots).forEach(advisorId => {
            let advisor = this.finalTimeSlots[advisorId];
            advisor.forEach(day => {
                day.slots.forEach(slot => {
                    slot.class = "slds-button";
                });
            });
        });
    }

    _compareAvailabilities(a, b) {
        if (a.availability < b.availability) {
            return -1;
        }
        if (a.availability > b.availability) {
            return 1;
        }
        return 0;
    }

    _compareSlots(a, b) {
        if (a.startTime < b.startTime) {
            return -1;
        }
        if (a.startTime > b.startTime) {
            return 1;
        }
        return 0;
    }

    _formatAMPM(timeString) {
        let splits = timeString.split(':');
        let hours = splits[0];
        let minutes = splits[1];
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 && minutes != '00' ? '0'+minutes : minutes;
        let strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    _isAMorPM(timeString) {
        let splits = timeString.split(':');
        let hours = splits[0];
        let ampm = hours >= 12 ? 'PM' : 'AM';
        return ampm
    }

}