import {LightningElement, api, track} from 'lwc';
import getPUDTransportationRequestPicklist from '@salesforce/apex/AppointmentBookingController.getPUDTransportationRequestPicklist';

export default class appointmentBookingTransportationTypeSelection extends LightningElement {
    @api
    get availableTransportations() {
        return this._availableTransportations;
    };
    set availableTransportations(value){
        if (value) {
            console.log('availableTransportations');
            this._availableTransportations = value;
            if (!this._availableTransportations.has(this.selectedTransportation) ) {
                this.uncheckAll()
            }
        }
    }
    _availableTransportations = new Set();

    // @api
    // get isLoanerAvailable() {
    //     return this._isLoanerAvailable;
    // };
    // set isLoanerAvailable(value){
    //     this._isLoanerAvailable = value;
    //     if(!this._isLoanerAvailable && this.selectedTransportation === 'Loaner') this.uncheckAll();
    // }
    // _isLoanerAvailable = false;

    // @api 
    // get isRideshareAvailable() {
    //     return this._isRideshareAvailable;
    // };
    // set isRideshareAvailable(value){
    //     this._isRideshareAvailable = value;
    //     if(!this._isRideshareAvailable && this.selectedTransportation === 'Rideshare') this.uncheckAll();
    // }
    // _isRideshareAvailable = false;

    @api 
    get disableAll() {
        return this._disableAll;
    };
    set disableAll(value){
        this._disableAll = value;
        if(this._disableAll) this.uncheckAll();
    }
    _disableAll = false;

    @api 
    get preselectedTransportation() {
        return this._preselectedTransportation;
    }
    set preselectedTransportation(value) {
        if(value) {
            console.log('preselectedTransportation');
            this._preselectedTransportation = value;
            this.selectedTransportation = value.transportationType;
            let selector = 'input[value="' + value.transportationType +'"]';
            this.template.querySelectorAll(selector).forEach(element => {
                element.checked = true;
            });

            if(this.showPickupSection){
                this.pickupAddress = value.pickupAddress
                // Object.keys(value.pickupAddress).forEach(property => {
                //     this[property] = value.pickupAddress[property];
                // });
            }

            // this.requestLoaner = value.requestLoaner;

            this.sendSelectedTransportation();
        }
    }
    _preselectedTransportation;

    // @api loanerApprovalRequested
    @api
    get loanerApprovalRequested() {
        return this._loanerApprovalRequested
    }
    set loanerApprovalRequested(value) {
        console.log('loanerApprovalRequested trans');
        this._loanerApprovalRequested = value
        this.template.querySelectorAll('input[name="options"]').forEach(element => {
            element.checked = value
        })
    }
    _loanerApprovalRequested = false;

    get shuttleDisabled() {
        return !this.availableTransportations.has('Shuttle') || this.disableAll;
    }
    get rideshareDisabled() {
        // return !this._isRideshareAvailable || this.disableAll;
        return !this.availableTransportations.has('Rideshare') || this.disableAll;
    }
    get dropoffDisabled() {
        // return !this._isRideshareAvailable || this.disableAll;
        return !this.availableTransportations.has('Drop-Off') || this.disableAll;
    }
    get loanerDisabled() {
        // return !this._isLoanerAvailable || this.disableAll;
        return !this.availableTransportations.has('Loaner') || this.disableAll;
    }
    get rentalDisabled() {
        return !this.availableTransportations.has('Rental') || this.disableAll;
    }
    get pickupAndDeliveryDisabled() {
        return !this.availableTransportations.has('Pickup & Delivery') || this.disableAll;
    }
    get expressDisabled() {
        return !this.selectedTransportation || !this.availableTransportations.has('Express') || this.disableAll;
    }

    

    get showPickupSection() {
        return this.selectedTransportation === 'Pickup & Delivery';
    }

    get showLoanerRequestCheckbox() {
        return !this.disableAll && this.loanerDisabled;
    }

    @track selectedTransportation;
    @track isExpressSelected;

    // @track pickupStreet;
    // @track pickupCity;
    // @track pickupState;
    // @track pickupZIP;
    // @track pickupComment;
    // @track pickupTransportationRequest;
    // @track requestLoaner = this.loanerApprovalRequested;
    @api requestLoaner
    @track PUDTransportationRequestOptions;

    pickupAddress = {};

    connectedCallback() {
        getPUDTransportationRequestPicklist()
            .then(result => {
                let tempOptionsList = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    tempOptionsList.push({ label: element.label, value: element.value });
                })
                this.PUDTransportationRequestOptions = tempOptionsList;
            })
    }

    renderedCallback() {
        console.log('renderedCallback trans');
        this.template.querySelectorAll('input[name="options"]').forEach(element => {
            element.checked = this.requestLoaner
        })
    }

    handleRadioClick(event) {
        if(event.target.value === this.selectedTransportation) {
            event.target.checked = false;
            this.selectedTransportation = '';
        } else {
            this.selectedTransportation = event.target.value;
        }
        this.sendSelectedTransportation();
    }

    handleExpressClick(event) {
        event.target.checked = !this.isExpressSelected;
        this.isExpressSelected = !this.isExpressSelected;
        this.sendSelectedTransportation();
    }

    uncheckAll() {
        this.requestLoaner = false;
        this.template.querySelectorAll('input').forEach(element => {
            element.checked = false;
        });
        this.selectedTransportation = '';
        this.isExpressSelected = false;
        this.sendSelectedTransportation();
    }

    handlePickupAddressChange(event) {
        this[event.target.name] = event.target.value;
        const addressFieldAPIName = event.currentTarget.dataset.apiname;
        this.pickupAddress[addressFieldAPIName] = event.target.value;

        this.sendSelectedTransportation();
    }

    handleLoanerRequestChange(event) {
        console.log('TRANS');
        this.requestLoaner = event.target.checked;
        this.dispatchEvent( new CustomEvent('loanerrequested', {detail: this.requestLoaner}) )
        // this.sendSelectedTransportation();
    }
    
    sendSelectedTransportation() {
        let finalDetails = {};

        finalDetails.selectedTransportation = this.selectedTransportation;
        finalDetails.requestLoaner = this.requestLoaner;
        finalDetails.express = this.isExpressSelected;

        // let autoselectedWorktype
        // if(this.selectedTransportation == 'Loaner') {
        //     autoselectedWorktype = 'Loaner'
        // } else if (this.selectedTransportation === 'Rideshare') {
        //         autoselectedWorktype = 'Rideshare'
        // } else if (this.selectedTransportation === 'Pickup & Delivery' 
        //     && this.pickupAddress.PickupTransportationRequest == 'Loaner Requested') {
        //         autoselectedWorktype = 'PUPD Loaner'
        // } else if (this.selectedTransportation === 'Pickup & Delivery' 
        //     && this.pickupAddress.PickupTransportationRequest == 'No Alternate Transportation Requested') {
        //         autoselectedWorktype = 'PUPD No Alternate Transportation Requested'
        // }
        // finalDetails.autoselectedWorktype = autoselectedWorktype
        
        if(this.selectedTransportation === 'Pickup & Delivery') {
            finalDetails.pickupAddress = this.pickupAddress;
        }

        this.dispatchEvent(
            new CustomEvent('transportationselected', { detail: finalDetails })
        );
    }
}