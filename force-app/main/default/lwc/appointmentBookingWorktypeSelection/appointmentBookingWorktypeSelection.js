import {LightningElement, api, track} from 'lwc';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getMostCommonlyUsedWorkTypes from '@salesforce/apex/AppointmentBookingController.getMostCommonlyUsedWorkTypes';
import getDeclinedServices from '@salesforce/apex/AppointmentBookingController.getDeclinedServices';
import removeDeclinedServices from '@salesforce/apex/AppointmentBookingController.removeDeclinedServices';
import apexSearch from '@salesforce/apex/AppointmentBookingController.search';

const columns = [
    {
        type:  'button-icon',
        typeAttributes: 
        {
          iconName: 'utility:close',
          variant: 'bare',
          alternativeText: 'Delete',
          title: 'Delete Row',
          disabled: false, 
        },
        fixedWidth: 70,
      },

    { label: 'Op-Code', fieldName: 'opCode', cellAttributes: { alignment: 'left' } },
    { label: 'Service Name', fieldName: 'title', cellAttributes: { alignment: 'left' } },
    // { label: 'Parts Req.', fieldName: 'partsReq', type: 'boolean', cellAttributes: { alignment: 'left' }},
    // { label: 'Horizon', fieldName: 'horizon', type: 'number', cellAttributes: { alignment: 'left' }},
    // { label: 'Shuttle', fieldName: 'shuttle', type: 'boolean', cellAttributes: { alignment: 'left' }},
    // { label: 'Rideshare', fieldName: 'rideshare', type: 'boolean', cellAttributes: { alignment: 'left' }},
    // { label: 'Loaner', fieldName: 'loaner', type: 'boolean', cellAttributes: { alignment: 'left' }},
    // { label: 'Rental', fieldName: 'rental', type: 'boolean', cellAttributes: { alignment: 'left' }},
    // { label: 'Express', fieldName: 'express', type: 'boolean', cellAttributes: { alignment: 'left' }},
    
];

export default class AppointmentBookingWorktypeSelection extends LightningElement {
    @api 
    get dealerId() {
        return this._dealerId;
    }
    set dealerId(value) {
        this._dealerId = value;
        if(value) {
            this.getWorkTypesOptions(value);
        }
    }
    @track _dealerId;
    
    // @api 
    // get autoselectedWorktype() {
    //     return this._autoselectedWorktype;
    // }
    // set autoselectedWorktype(value) {
    //     this._autoselectedWorktype = value;
    //     if(value) {
    //         this.automateWorktypeSelction(value);
    //     }
    // }
    // @track _autoselectedWorktype;

    @api 
    get selectedVehicle() {
        return this._selectedVehicle;
    }
    set selectedVehicle(value) {
        this._selectedVehicle = value;
        if(value) {
            this.getDeclinedServiceOptions(value);
        }
    }
    @track _selectedVehicle;

    @api
    get preselectedWorktypes(){
        return this._preselectedWorktypes;
    };
    set preselectedWorktypes(value) {
        if(value) {
            console.log('preselectedWorktypes');
            let tempValue = [...value];
            let tempList = [];
            tempValue.forEach(element => {
                element = JSON.parse(element)
                let currentSelection = 
                    {
                        id: element.Id,
                        sObjectType: 'Worktype',
                        icon: 'standard:work_type',
                        title: element.Name,
                        obj: element,
                        opCode: element.CorrectionOpCode__c,
                        loaner: element.Loaner_Available__c,
                        partsReq: element.PartsRequired__c,
                        horizon: element.Horizon__c,
                        rideshare: element.Rideshare__c,
                        dropoff: element.DropOffAvailable__c,
                        shuttle: element.ShuttleAvailable__c,
                        rental: element.RentalAvailable__c,
                        express: element.ExpressAvailable__c,
                        pickupAndDelivery: element.PickUp_Delivery__c
                    }
                ;
                tempList.push(currentSelection);
            });
            this.initialSelection = JSON.parse(JSON.stringify(tempList));
            console.log(JSON.stringify(this.initialSelection));
            this.worktypeSelectionEvent()
        }
    };
    @track _preselectedWorktypes;

    @api customerId;

    @track workTypeOptions = [];
    @track declinedServOptions = [];
    @track initialSelection = [];
    @track columns = columns;

    @track isDeclined = false;

    isMultiEntry = true;
    maxSelectionSize = 20;
    errors = [];

    get isSelected() {
        return this.initialSelection.length > 0;
    }

    get duration() {
        let duration = 0;
        this.initialSelection.forEach(element => {
            let workTypeObj = element.obj;
            if (workTypeObj.DurationType == 'Hours') {
                duration += workTypeObj.EstimatedDuration * 60;
            } else if (workTypeObj.DurationType == 'Minutes') {
                duration += workTypeObj.EstimatedDuration;
            }
        });

        return duration;
    }

    get declinedServOptionsSize() {

        return this.declinedServOptions.length
    }

    getWorkTypesOptions() {
        getMostCommonlyUsedWorkTypes({dealer: this.dealerId})
            .then(result => {
                let tempOptions = [];
                result.forEach(element => {
                    let worktype = {...element};
                    worktype.isSelected = false;
                    if(this.initialSelection) {
                        let wt = this.initialSelection.find(selection => selection.id === worktype.Id);
                        if(wt) {
                            worktype.isSelected = true;
                        }
                    }
                    tempOptions.push(worktype);
                });
                this.workTypeOptions = this.sortWorkorders(tempOptions);
                // this.workTypeOptions = tempOptions
                // this.worktypeSelectionEvent();
        })
        .catch(error => {
            console.log('ERROR', error)
        });
    }

    sortWorkorders(workordersList){
        let a = Math.round(workordersList.length/2)
        let sortedList = []
        for (let i = 0; i < a; i++) {
            if(workordersList[i]) {
                sortedList.push(workordersList[i])
            }
            if(workordersList[i+a]) {
                sortedList.push(workordersList[i+a])
            }
        }

        return sortedList
    }

    getDeclinedServiceOptions() {
        getDeclinedServices({
            accountId: this.customerId,
            vehicleId: this.selectedVehicle
        })
            .then(result => {
                if(result.length == 0) {
                    this.isDeclined = false;
                } else {
                    this.isDeclined = true;

                    let tempOptions = [];
                    result.forEach(element => {
                    let worktype = {...element};
                    worktype.isSelected = false;
                    worktype.Name = worktype.WorkType__r.Name + ' (' + worktype.DeclineDate__c + ')';
                    tempOptions.push(worktype);
                });
                this.declinedServOptions = tempOptions;
                }
            })
            .catch(error => {
                console.log('ERROR', error)
            });
    }

    // automateWorktypeSelction(worktypeName) {
    //     let wt = this.workTypeOptions.find(selection => selection.Name === worktypeName);
    //     if(wt && wt.isSelected == false) {
    //         let tempSelectionEvent = {currentTarget: {dataset: {id: wt.Id}}};
    //         this.handleCheck(tempSelectionEvent);
    //     }
    // }

    handleCheck = (event) => {
        const worktypeId = event.currentTarget.dataset.id;
        this.workTypeOptions.forEach(element => {
            if (element.Id === worktypeId) {
                let tempList = [...this.initialSelection];
                element.isSelected = !element.isSelected;

                if(!element.isSelected) {
                    let index = tempList.findIndex(worktype => worktypeId === worktype.id);
                    if(index > -1) {
                        tempList.splice(index, 1);
                    }

                    let ds = this.declinedServOptions.find(selection => selection.WorkType__c === element.Id);
                    if(ds) {
                        ds.isSelected = false;
                    }
                } else {
                    let currentSelection = 
                        {
                            id: element.Id,
                            sObjectType: 'Worktype',
                            icon: 'standard:work_type',
                            title: element.Name,
                            obj: element,
                            opCode: element.CorrectionOpCode__c,
                            loaner: element.Loaner_Available__c,
                            partsReq: element.PartsRequired__c,
                            horizon: element.Horizon__c,
                            rideshare: element.Rideshare__c,
                            dropoff: element.DropOffAvailable__c,
                            shuttle: element.ShuttleAvailable__c,
                            rental: element.RentalAvailable__c,
                            express: element.ExpressAvailable__c,
                            pickupAndDelivery: element.PickUp_Delivery__c
                        }
                    ;
                    tempList.push(currentSelection);

                    let ds = this.declinedServOptions.find(selection => selection.WorkType__c === element.Id);
                    if(ds) {
                        ds.isSelected = true;
                    }
                }

                this.initialSelection = [...tempList];
            }
        });
        this.worktypeSelectionEvent();
    }

    handleDiclinedServiceCheck = (event) => {
        const declinedServId = event.currentTarget.dataset.id;
        let declinedServ = this.declinedServOptions.find(selection => selection.Id === declinedServId);
        if(declinedServ) {
            declinedServ.isSelected = !declinedServ.isSelected;

            let tempList = [...this.initialSelection];

            if(!declinedServ.isSelected) {
                let index = tempList.findIndex(worktype => declinedServ.WorkType__c === worktype.id);
                if(index > -1) {
                    tempList.splice(index, 1);
                }

                let wt = this.workTypeOptions.find(selection => selection.Id === declinedServ.WorkType__c);
                if(wt) {
                    wt.isSelected = false;
                }
            } else {
                let currentSelection = 
                    {
                        id: declinedServ.WorkType__c,
                        sObjectType: 'Worktype',
                        icon: 'standard:work_type',
                        title: declinedServ.WorkType__r.Name,
                        obj: declinedServ.WorkType__r,
                        opCode: declinedServ.WorkType__r.CorrectionOpCode__c,
                        loaner: declinedServ.WorkType__r.Loaner_Available__c,
                        partsReq: declinedServ.WorkType__r.PartsRequired__c,
                        horizon: declinedServ.WorkType__r.Horizon__c,
                        rideshare: declinedServ.WorkType__r.Rideshare__c,
                        dropoff: declinedServ.WorkType__r.DropOffAvailable__c,
                        shuttle: declinedServ.WorkType__r.ShuttleAvailable__c,
                        rental: declinedServ.WorkType__r.RentalAvailable__c,
                        express: declinedServ.WorkType__r.ExpressAvailable__c,
                        pickupAndDelivery: element.PickUp_Delivery__c
                    }
                ;
                tempList.push(currentSelection);

                let wt = this.workTypeOptions.find(selection => selection.Id === declinedServ.WorkType__c);
                if(wt) {
                    wt.isSelected = true;
                }
            }
            this.initialSelection = [...tempList];
        }
        this.worktypeSelectionEvent();
    }

    handleSearch(event) {
        let params = event.detail;
        params.dealer = this.dealerId;
        apexSearch(params)
            .then((results) => {
                let tempRes = JSON.parse(JSON.stringify(results));
                tempRes.forEach(element => {
                    element.opCode = element.obj.CorrectionOpCode__c;
                    element.loaner = element.obj.Loaner_Available__c;
                    element.partsReq = element.obj.PartsRequired__c;
                    element.horizon = element.obj.Horizon__c;
                    element.rideshare = element.obj.Rideshare__c;
                    element.dropoff = element.obj.DropOffAvailable__c;
                    element.shuttle = element.obj.ShuttleAvailable__c;
                    element.rental = element.obj.RentalAvailable__c;
                    element.express = element.obj.ExpressAvailable__c;
                    element.pickupAndDelivery = element.obj.PickUp_Delivery__c;
                });
                this.template.querySelector('c-lookup').setSearchResults(tempRes);
            })
            .catch((error) => {
                this.notifyUser('Lookup Error', 'An error occured while searching with the lookup field.', 'error');
                console.error('Lookup error', JSON.stringify(error));
                this.errors = [error];
            });
    }

    handleSelectionChange = (event) => {
        this.checkForErrors();
        event.target.getSelection().forEach(element => {
            let wt = this.workTypeOptions.find(selection => selection.Id === element.id);
            if(wt) {
                wt.isSelected = true;
            }
            let ds = this.declinedServOptions.find(selection => selection.WorkType__c === element.id);
            if(ds) {
                ds.isSelected = true;
            }
        });
        this.initialSelection = event.target.getSelection();
        this.worktypeSelectionEvent();
    }

    checkForErrors() {
        this.errors = [];
        const selection = this.template.querySelector('c-lookup').getSelection();
        // Custom validation rule
        // if (this.isMultiEntry && selection.length > this.maxSelectionSize) {
        //     this.errors.push({ message: `You may only select up to ${this.maxSelectionSize} items.` });
        // }
        // Enforcing required field
        if (selection.length === 0) {
            this.errors.push({ message: 'Please make a selection.' });
        }
    }

    notifyUser(title, message, variant) {
        if (this.notifyViaAlerts) {
            // Notify via alert
            // eslint-disable-next-line no-alert
            alert(`${title}\n${message}`);
        } else {
            // Notify via toast
            const toastEvent = new ShowToastEvent({ title, message, variant });
            this.dispatchEvent(toastEvent);
        }
    }

    handleRemove = (event) => {
        const declinedServId = event.currentTarget.dataset.id;

        removeDeclinedServices({serviceId: declinedServId})
            .then(result => {
                let tempList = [...this.declinedServOptions];

                let index = tempList.findIndex(worktype => declinedServId === worktype.Id);
                if(index > -1) {
                    tempList.splice(index, 1);
                }

                this.declinedServOptions = tempList;

                this.isDeclined = this.declinedServOptions.length;
            })
        .catch((error) => {
            console.log('ERROR', error)
        })
    }

    handleRowActions(event) {
        let wt = this.workTypeOptions.find(selection => selection.Id === event.detail.row.id);
        if(wt) {
            wt.isSelected = false;
        }

        let ds = this.declinedServOptions.find(selection => selection.WorkType__c === event.detail.row.id);
        if(ds) {
            ds.isSelected = false;
        }

        let tempList = [...this.initialSelection];
        let index = tempList.findIndex(worktype => event.detail.row.id === worktype.id);
        if(index > -1) {
            tempList.splice(index, 1);
        }
        this.initialSelection = tempList;
        this.worktypeSelectionEvent();
    }

    worktypeSelectionEvent() {
        console.log('~~~~~worktype select~~~~~~');
        let worktypesList = [];
        let worktypeNames = [];
        let horizon = 0;
        let duration = this.duration;
        let loaner = false;
        let rideshare = false;
        let dropoff = false;

        let transportationTypesSet = new Set()
        // transportationTypesSet.add('Pickup & Delivery')
        transportationTypesSet.add('Wait')

        this.initialSelection.forEach(element => {
            let tempHorizon = element.horizon ? element.horizon : 0; 
            worktypesList.push(element.id);
            worktypeNames.push(element.title);
            loaner = loaner ? loaner : element.loaner;
            rideshare = rideshare ? rideshare : element.rideshare;
            dropoff = dropoff ? dropoff : element.dropoff;
            horizon = horizon > tempHorizon ? horizon : tempHorizon;
            if (element.loaner) {
                transportationTypesSet.add('Loaner')
            }
            if (element.shuttle) {
                transportationTypesSet.add('Shuttle')
            }
            if (element.rideshare) {
                transportationTypesSet.add('Rideshare')
            }
            if (element.dropoff) {
                transportationTypesSet.add('Drop-Off')
            }
            if (element.rental) {
                transportationTypesSet.add('Rental')
            }
            if (element.express) {
                transportationTypesSet.add('Express')
            }
            if (element.pickupAndDelivery) {
                transportationTypesSet.add('Pickup & Delivery')
            }
        });

        this.dispatchEvent(
            new CustomEvent('worktypeselected', {
                detail: {
                    worktypes: worktypesList,
                    loaner: loaner,
                    duration: duration,
                    horizon: horizon,
                    rideshare: rideshare,
                    dropoff: dropoff,
                    names: worktypeNames.join(', '),
                    availableTransportations: transportationTypesSet
                }
            })
        );
    }

    @track showStartPopOver = false;
    
    showStartPop() {
        this.showStartPopOver = !this.showStartPopOver;
    }

    closeStartPop() {
        this.showStartPopOver = false;
    }
}