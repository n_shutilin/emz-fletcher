import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import findActiveQueue from '@salesforce/apex/AssignAdvisorController.getResourceByAppointmentId';
import assigneAdvisor from '@salesforce/apex/AssignAdvisorController.assignAdvisor'

export default class AssignAdvisorComponent extends LightningElement {

    @api recordId;

    isAlreadyAssigned = false;
    prevResourceId;
    prevResourceName;
    responeBody;
    resourceId;
    resourceName;

    connectedCallback() {
        this.dispatchInit();
    }

    handleSuccess() {
        assigneAdvisor({ resourceId: this.resourceId, appointmentId: this.recordId }).then((result) => {
            if (result) {
                let response = JSON.parse(result);
                if (response.code != 200) {
                    this.displayMessage('Advisor cannot be assigned', response.errorMessage, 'error');
                } else {
                    this.coleModal();
                    this.displayMessage('Advisor was assigned', 'Advisor with name: ' + this.resourceName + ' was assigned to this appointment', 'success');
                }
            }
        });
    }

    dispatchInit() {
        findActiveQueue({ appointmentId: this.recordId }).then((result) => {
            if (result) {
                let response = JSON.parse(result);
                if (response.code != 200) {
                    this.displayMessage('Advisor cannot be assigned', response.errorMessage, 'error');
                    this.coleModal();
                } else {
                    if (response.body.length) {
                        this.executeLogicForAssignetAppointment(response);
                    } else {
                        this.responeBody = response.body;
                        this.resourceId = response.body.Id;
                        this.resourceName = response.body.Name;
                    }
                }
            }
        });
    }

    executeLogicForAssignetAppointment(response){
        this.isAlreadyAssigned = true;
        this.prevResourceId = response.body[0].Id;
        this.prevResourceName = response.body[0].Name;
        this.resourceId = response.body[1].Id;
        this.resourceName = response.body[1].Name;
    }

    displayMessage(eventTitle, eventMessage, eventVariant) {
        const evt = new ShowToastEvent({
            title: eventTitle,
            message: eventMessage,
            variant: eventVariant,
        });
        this.dispatchEvent(evt);
    }

    coleModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }
}