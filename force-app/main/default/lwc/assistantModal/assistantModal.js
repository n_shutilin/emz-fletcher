import { LightningElement, api } from 'lwc';

export default class AssistantModal extends LightningElement {
        
    @api selectedRow;
    _selectedQueue;

    @api set selectedQueue(value) {
        this._selectedQueue = value;
    }

    get selectedQueue() {
        return this._selectedQueue;
    }

    get rows() {
        return this.selectedQueue.rows.filter(
            row => !row.withGuest 
                && !row.onBreak 
                && !row.leadSuspend 
                && !row.inPairing 
                && row.position !== 1);
    }

    get isEmpty(){
        return !this.selectedQueue.rows.filter(
            row => !row.withGuest 
                && !row.onBreak 
                && !row.leadSuspend 
                && !row.inPairing 
                && row.position !== 1).length;
    }

    handleChoose(event) {
        let rowid = event.currentTarget.dataset.rowid;
        this.dispatchAction(rowid, "selected");
    }

    dispatchAction(selectedRowId=null, type="close") {
        this.dispatchEvent(
            new CustomEvent("assistant", {
                detail: {
                    type: type,
                    rowId: selectedRowId,
                    queueId: this.selectedQueue.id
                }
            })
        );
    }

    closeModal() {
        this.dispatchAction()
    }
}