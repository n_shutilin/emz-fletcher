import { LightningElement, api } from 'lwc';

export default class ChromeDecoderComponentImageModal extends LightningElement {
    @api image

    connectedCallback(){
        console.log('IMAGE ' + this.image)
    }

    closeModal = () => {
        this.dispatchEvent(new CustomEvent('closemodal', {}))
        console.log('modal closed')
    }
}