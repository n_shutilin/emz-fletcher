import { LightningElement, api, track } from 'lwc';
import getVin from '@salesforce/apex/ChromeVINDecoderComponentController.getVin'
import getVehicleInfo from '@salesforce/apex/ChromeVINDecoderComponentController.getVehicleInfo'
import updateRecord from '@salesforce/apex/ChromeVINDecoderComponentController.updateRecord'
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ChromeVINDecoderComponent extends LightningElement {
    @api recordId
    @api objectName

    @track loading = true
    @track bigPicture = false
    @track errorMessage
    @track _vin
    @track vehicleInfo
    @track currentImage

    get isInfoReceived () {
        return this.vehicleInfo
    }

    get isError () {
        return this.errorMessage
    }

    get year() {
        return this.vehicleInfo.Year
    }

    get vin() {
        return this.vehicleInfo.Vin
    }

    get model() {
        return this.vehicleInfo.Model
    }

    get make() {
        return this.vehicleInfo.Make
    }

    get image(){
        return this.vehicleInfo.Images[this.currentImage]
    }

    get haveImages() {
        return this.vehicleInfo.Images.length
    }

    connectedCallback(){
        this.receiveInfo(this.recordId, this.objectName)
        this.currentImage = 0
    }

    openPicture = () => {
        this.bigPicture = true
    }

    closeBitPicture = () => {
        this.bigPicture = false
    }

    moveLeft = () => {
        let currentPosition = this.currentImage
        let nextPosition = currentPosition - 1
        if(nextPosition < 0){
            nextPosition = this.vehicleInfo.Images.length - 1
        }
        this.currentImage = nextPosition
    }

    moveRight = () => {
        let currentPosition = this.currentImage
        let nextPosition = currentPosition + 1
        if(nextPosition > this.vehicleInfo.Images.length - 1){
            nextPosition = 0
        }
        this.currentImage = nextPosition
    }

    receiveInfo = (recordId, objectName) => {
        getVin({recordId: recordId, objectType: objectName}).then((result)=>{
            if(result.code === 202) {
                this._vin = result.body
                this.decodeVin(this._vin);
            } else {
                this.errorMessage = result.body
                this.loading = false
            }
            
        })
    }

    decodeVin = (vin) => {
        getVehicleInfo({vin: vin}).then((result) => {
            if(result.code === 202) {
                this.vehicleInfo = JSON.parse(result.body)
            } else {
                this.errorMessage = result.body
                this.displayMessage('Something went wrong!', result.body, 'error')
                this.coloseModal()
            }
            this.loading = false
        })
    }

    @api
    handleSave(){
        this.loading = true
        updateRecord({recordId: this.recordId, objectType: this.objectName, data: JSON.stringify(this.vehicleInfo)}).then((result) =>{
            if(result.code === 202){
                this.displayMessage('Success!', 'Record was updated!', 'success')
            } else {
                this.displayMessage('Something went wrong!', result.body, 'error')
            }
            this.coloseModal()
            this.loading = false
        });
    }

    displayMessage(eventTitle, eventMessage, eventVariant) {
        const evt = new ShowToastEvent({
            title: eventTitle,
            message: eventMessage,
            variant: eventVariant,
        });
        this.dispatchEvent(evt);
    }

    coloseModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }
}