import { LightningElement, api, track } from 'lwc';

import getPicklistValues from '@salesforce/apex/CreditApplicationLWCController.getPicklistValues';

export default class CreditApplicationAddresses extends LightningElement {
    @api 
    get buyerContact(){
        return this._buyerContact;
    }
    set buyerContact(value){
        this._buyerContact = value;
        
        if(this._buyerContact) {
            this.applicantAddressWrapper.Current_Address_Street__c = this._buyerContact.BillingStreet;
            this.applicantAddressWrapper.Current_Address_City__c = this._buyerContact.BillingCity;
            this.applicantAddressWrapper.Current_Address_State__c = this._buyerContact.BillingState;
            this.applicantAddressWrapper.Current_Address_Country__c = this._buyerContact.BillingCountry;
            this.applicantAddressWrapper.Current_Address_Zipcode__c = this._buyerContact.BillingPostalCode;
        }
    }
    _buyerContact;

    @api 
    get cobuyerContact(){
        return this._cobuyerContact;
    }
    set cobuyerContact(value){
        this._cobuyerContact = value;
        
        if(this._cobuyerContact) {
            this.coApplicantAddressWrapper.Current_Address_Street__c = this._cobuyerContact.BillingStreet;
            this.coApplicantAddressWrapper.Current_Address_City__c = this._cobuyerContact.BillingCity;
            this.coApplicantAddressWrapper.Current_Address_State__c = this._cobuyerContact.BillingState;
            this.coApplicantAddressWrapper.Current_Address_Country__c = this._cobuyerContact.BillingCountry;
            this.coApplicantAddressWrapper.Current_Address_Zipcode__c = this._cobuyerContact.BillingPostalCode;
        }
    }
    _cobuyerContact;

    @track sameAsApplicant = false;

    @track applicantAddressWrapper = {};
    @track coApplicantAddressWrapper = {};

    @track countryPicklistOptions;
    @track statePicklistOptions;

    connectedCallback() {
        getPicklistValues({fieldAPIName : 'Current_Address_Country__c'})
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.countryPicklistOptions = picklistOptions;
            })
        .then(() => getPicklistValues({fieldAPIName : 'Current_Address_State__c'}))
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.statePicklistOptions = picklistOptions;
            })
        .catch(error => {console.log('ERROR', error)});
    }

    handleValueChange(event) {
        if(event.currentTarget.dataset.role === 'Applicant') {
            this.applicantAddressWrapper[event.currentTarget.dataset.fieldname] = event.target.value;

            if(this.sameAsApplicant) {
                this.coApplicantAddressWrapper[event.currentTarget.dataset.fieldname] = event.target.value;

                this.dispatchEvent(
                    new CustomEvent('valuechanged', {
                        detail: {
                            values: [{
                                fieldName: event.currentTarget.dataset.fieldname,
                                fieldValue: event.target.value
                            }],
                            role: 'Co-Applicant'
                        }
                    })
                );
            }
        } else if (event.currentTarget.dataset.role === 'Co-Applicant') {
            this.coApplicantAddressWrapper[event.currentTarget.dataset.fieldname] = event.target.value;
        }

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: {
                    values: [{
                        fieldName: event.currentTarget.dataset.fieldname,
                        fieldValue: event.target.value
                    }],
                    role: event.currentTarget.dataset.role
                }
            })
        );
    }

    handleSameAsApplicantCheck() {
        this.sameAsApplicant = !this.sameAsApplicant;
        if(this.sameAsApplicant) {
            this.coApplicantAddressWrapper = {...this.applicantAddressWrapper};
            let detailList = [];
            Object.keys(this.coApplicantAddressWrapper).forEach(element => {
                detailList.push({
                    fieldName: element,
                    fieldValue: this.coApplicantAddressWrapper[element]
                });
            });

            this.dispatchEvent(
                new CustomEvent('valuechanged', {
                    detail: {
                        values: detailList,
                        role: 'Co-Applicant'
                    }
                })
            );
        }
    }

    handleClearing = (event) => {
        let detailList = [];
        if(event.currentTarget.dataset.role === 'Applicant') {
            let tempWrapper = {...this.applicantAddressWrapper};
            Object.keys(tempWrapper).forEach(element => {
                if(element.startsWith(event.currentTarget.dataset.fieldname)) {
                    tempWrapper[element] = null;
                    detailList.push({
                        fieldName: element,
                        fieldValue: null
                    });
                }
            });
            this.applicantAddressWrapper = {...tempWrapper};
        } else if (event.currentTarget.dataset.role === 'Co-Applicant') {
            Object.keys(this.coApplicantAddressWrapper).forEach(element => {
                if(element.startsWith(event.currentTarget.dataset.fieldname)) {
                    this.coApplicantAddressWrapper[element] = null;
                    detailList.push({
                        fieldName: element,
                        fieldValue: null
                    });
                }
            });
        }

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: {
                    values: detailList,
                    role: event.currentTarget.dataset.role
                }
            })
        );

        if(event.currentTarget.dataset.role === 'Applicant' && this.sameAsApplicant) {
            detailList.forEach(element => {
                this.coApplicantAddressWrapper[element.fieldName] = null;
            });

            this.dispatchEvent(
                new CustomEvent('valuechanged', {
                    detail: {
                        values: detailList,
                        role: 'Co-Applicant'
                    }
                })
            );
        }
    }
}