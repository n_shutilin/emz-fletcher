import { LightningElement, api, track } from 'lwc';

import getPicklistValues from '@salesforce/apex/CreditApplicationLWCController.getPicklistValues';

export default class CreditApplicationAddressesForm extends LightningElement {
    @api addressType;
    @api componentLabel;

    @track countryPicklistOptions;
    @track statePicklistOptions;
    
    @track street1;
    @track street2;
    @track city;
    @track state;
    @track zipcode;
    @track country;
    @track years;
    @track month;

    fieldAPINameList = [
        'Street_1__c',
        'Street_2__c',
        'City__c',
        'State__c',
        'Zipcode__c',
        'Country__c',
        'Years__c',
        'Months__c'
    ]

    connectedCallback() {
        getPicklistValues({fieldAPIName : this.addressType + '_' + 'Country__c'})
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.countryPicklistOptions = picklistOptions;
            })
        .then(() => getPicklistValues({fieldAPIName : this.addressType + '_' + 'State__c'}))
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.statePicklistOptions = picklistOptions;
            })
        .catch(error => {console.log('ERROR', error)});
    }

    handleValueChange(event) {
        this[event.target.name] = event.target.value;

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: [{
                    fieldName: this.addressType + '_' + event.currentTarget.dataset.fieldname,
                    fieldValue: event.target.value
                }]
            })
        );
    }

    handleClearing() {
        let detailList = [];

        this.street1 = null;
        this.street2 = null;
        this.city = null;
        this.state = null;
        this.zipcode = null;
        this.country = null;
        this.years = null;
        this.month = null;

        this.fieldAPINameList.forEach(element => {
            detailList.push({
                fieldName: this.addressType + '_' + element,
                fieldValue: null
            });
        });

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: detailList
            })
        );
    }
}