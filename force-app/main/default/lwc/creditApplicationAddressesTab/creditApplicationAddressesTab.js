import { LightningElement, api, track } from 'lwc';

export default class CreditApplicationAddressesTab extends LightningElement {
    @api role;
    @api saveforboth;

    @track sameAsApplicant = false;

    get isCoApplicant() {
        return this.role == 'Co-Applicant';
    }

    handleCheck() {
        this.sameAsApplicant = !this.sameAsApplicant;
        this.dispatchEvent(
            new CustomEvent('changesaveasapplicant', {
                detail: this.sameAsApplicant
            })
        );
    }

    handleValueChange(event) {
        if(this.saveforboth) {
            this.dispatchEvent(
                new CustomEvent('valuechanged', {
                    detail: {
                        values: event.detail,
                        role: 'Applicant'
                    }
                })
            );
            this.dispatchEvent(
                new CustomEvent('valuechanged', {
                    detail: {
                        values: event.detail,
                        role: 'Co-Applicant'
                    }
                })
            );
        } else {
            this.dispatchEvent(
                new CustomEvent('valuechanged', {
                    detail: {
                        values: event.detail,
                        role: this.role
                    }
                })
            );
        }
    }
}