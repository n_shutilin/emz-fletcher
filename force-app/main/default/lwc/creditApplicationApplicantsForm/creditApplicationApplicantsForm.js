import { LightningElement, api, track } from 'lwc';

import getPicklistValues from '@salesforce/apex/CreditApplicationLWCController.getPicklistValues';

export default class CreditApplicationApplicantsForm extends LightningElement {
    @api role;
    @api 
    get contact(){
        return this._contact;
    }
    set contact(value){
        this._contact = value;
        
        if(this._contact) {
            this.firstName      = this._contact.FirstName;
            this.middleName     = this._contact.MiddleName;
            this.lastName       = this._contact.LastName;
            this.homePhone      = this._contact.HomePhone;
            this.emailAddress   = this._contact.Email;
            this.dateOfBirth    = this._contact.Birthdate;
        }
    }
    _contact;

    @track educationLevelPicklistOptions;
    @track driverLicenseStatePicklistOptions;
    
    @track firstName;
    @track middleName;
    @track lastName;
    @track homePhone;
    @track emailAddress;
    @track educationLevel;
    @track numOfDependents;
    @track dateOfBirth;
    @track driverLicenseNum;
    @track state;
    @track country;
    @track ssnNum;
    
    aplicantFormWrapper;

    connectedCallback() {
        getPicklistValues({fieldAPIName : 'Education_Level__c'})
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.educationLevelPicklistOptions = picklistOptions;
            })
        .then(() => getPicklistValues({fieldAPIName : 'Driver_Licnese_State__c'}))
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.driverLicenseStatePicklistOptions = picklistOptions;
            })
        .catch(error => {console.log('ERROR', error)});
    }

    handleValueChange(event) {
        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: {
                    values: [{
                        fieldName: event.currentTarget.dataset.fieldname,
                        fieldValue: event.target.value
                    }],
                    role: this.role
                }
            })
        );
    }
}