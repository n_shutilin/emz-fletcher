import { LightningElement, api, track } from 'lwc';

import getPicklistValues from '@salesforce/apex/CreditApplicationLWCController.getPicklistValues';

export default class CreditApplicationEmployerForm extends LightningElement {
    @api employerType;
    @api componentLabel;

    @track countryPicklistOptions;
    @track statePicklistOptions;
    
    @track address1;	
    @track address2;	
    @track badgeNum;	
    @track city; 
    @track country;
    @track months;   
    @track name; 
    @track phone;  
    @track position;	
    @track state;  
    @track years;  
    @track zipcode;

    fieldAPINameList = [
        'Address_1__c',
        'Address_2__c',
        'Badge_Number__c',
        'City__c',
        'Country__c',
        'Months__c',
        'Name__c',
        'Phone__c',
        'Position__c',
        'State__c',
        'Years__c',
        'Zipcode__c'
    ]

    connectedCallback() {
        getPicklistValues({fieldAPIName : this.employerType + '_' + 'Country__c'})
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.countryPicklistOptions = picklistOptions;
            })
        .then(() => getPicklistValues({fieldAPIName : this.employerType + '_' + 'State__c'}))
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.statePicklistOptions = picklistOptions;
            })
        .catch(error => {console.log('ERROR', error)});
    }

    handleValueChange(event) {
        this[event.target.name] = event.target.value;
        
        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: [{
                    fieldName: this.employerType + '_' + event.currentTarget.dataset.fieldname,
                    fieldValue: event.target.value
                }]
            })
        );
    }

    handleClearing() {
        let detailList = [];
 
        this.address1 = null;
        this.address2 = null;
        this.badgeNum = null;
        this.city = null;
        this.country = null;
        this.months	 = null;
        this.name = null;
        this.phone = null;
        this.position = null;
        this.state = null;
        this.years = null;
        this.zipcode = null;

        this.fieldAPINameList.forEach(element => {
            detailList.push({
                fieldName: this.employerType + '_' + element,
                fieldValue: null
            });
        });

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: detailList
            })
        );
    }
}