import { LightningElement, api, track } from 'lwc';

export default class CreditApplicationEmploymentTab extends LightningElement {
    @api role;

    handleValueChange(event) {
        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: {
                    values: event.detail,
                    role: this.role
                }
            })
        );
    }
}