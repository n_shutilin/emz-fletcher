import { LightningElement, api, track } from 'lwc';

import getPicklistValues from '@salesforce/apex/CreditApplicationLWCController.getPicklistValues';

export default class CreditApplicationFinancialTab extends LightningElement {
    @api role;

    @track bankReferenceAccTypeOptions;
    
    bankRefName1;
    bankRefName2;
    bankRefName3;
    bankRefAccNum1;
    bankRefAccNum2;
    bankRefAccNum3;
    bankRefAddress1;
    bankRefAddress2;
    bankRefAddress3;
    bankRefBalance1;
    bankRefBalance2;
    bankRefBalance3;
    bankRefAccType1;
    bankRefAccType2;
    bankRefAccType3;

    fieldAPINamesList = [
        'Bank_Reference_1_Name__c',
        'Bank_Reference_1_Account_Number__c',
        'Bank_Reference_1_Address__c',
        'Bank_Reference_1_Balance__c',
        'Bank_Reference_1_Account_Type__c',
        'Bank_Reference_2_Name__c',
        'Bank_Reference_2_Account_Number__c',
        'Bank_Reference_2_Address__c',
        'Bank_Reference_2_Balance__c',
        'Bank_Reference_2_Account_Type__c',
        'Bank_Reference_3_Name__c',
        'Bank_Reference_3_Account_Number__c',
        'Bank_Reference_3_Address__c',
        'Bank_Reference_3_Balance__c',
        'Bank_Reference_3_Account_Type__c'
    ];


    connectedCallback() {
        getPicklistValues({fieldAPIName : 'Bank_Reference_1_Account_Type__c'})
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.bankReferenceAccTypeOptions = picklistOptions;
            })
        .catch(error => {console.log('ERROR', error)});
    }

    handleValueChange(event) {
        let values;

        if (event.currentTarget.dataset.fieldname) {
            values = [{
                fieldName: event.currentTarget.dataset.fieldname,
                fieldValue: event.target.value
            }];
        } else {
            values = event.detail;
        }
        
        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: {
                    values: values,
                    role: this.role
                }
            })
        );
        
    }
}