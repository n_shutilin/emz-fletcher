import { LightningElement, api, track } from 'lwc';

export default class CreditApplicationIncomeForm extends LightningElement {
    @api componentLabel;

    @api inputdisabled;

    @api employment;

    @api alimony;

    @api otherincome;

    @api totalincome;

    fieldAPINameList = [
        'Employment_Income_',
        'Alimony_Income_',
        'Other_Income_'
    ];

    totalincome = 0;

    handleValueChange(event) {
        this[event.target.name] = event.target.value;
        
        this.totalincome = +(this.employment ? this.employment : 0) + +(this.alimony ? this.alimony : 0) +
            +(this.otherincome ? this.otherincome : 0);

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: [{
                    fieldName: event.currentTarget.dataset.fieldname + this.componentLabel + '__c',
                    fieldValue: event.target.value
                }]
            })
        );
    }

    handleEmploymentValueEdit(event) {
        this.dispatchEvent(new CustomEvent("employmentvaluechange", { detail: (parseInt(event.target.value) * 12)}));
    }

    handleAlimonyValueEdit(event) {
        this.dispatchEvent(new CustomEvent("alimonyvaluechange", { detail: (parseInt(event.target.value) * 12)}));
    }

    handleIncomeValueEdit(event) {
        this.dispatchEvent(new CustomEvent("incomevaluechange", { detail: (parseInt(event.target.value) * 12)}));
    }
}