import { LightningElement, api, track } from 'lwc';

import getPicklistValues from '@salesforce/apex/CreditApplicationLWCController.getPicklistValues';

export default class CreditApplicationIncomeTab extends LightningElement {
    @api role;

    @track alimonyIncomeIntervalOptions;

    @track annuallyemployment;

    @track annuallyalimony;

    @track annuallyincome;

    @track totalincome;

    alimonyIncomeInterval;
    incomeSource;

    fieldAPINamesList = [
        'Alimony_Income_Interval__c',
        'Other_Income_Source__c'
    ]

    connectedCallback() {
        getPicklistValues({fieldAPIName: 'Alimony_Income_Interval__c'})
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.alimonyIncomeIntervalOptions = picklistOptions;
            })
        .catch(error => {console.log('ERROR', error)});
    }

    handleValueChange(event) {
        let values;

        if (event.currentTarget.dataset.fieldname) {
            values = [{
                fieldName: event.currentTarget.dataset.fieldname,
                fieldValue: event.target.value
            }];
        } else {
            values = event.detail;
        }
        
        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: {
                    values: values,
                    role: this.role
                }
            })
        );
    }

    handleEmploymentValueEdit(event) {
        this.annuallyemployment = event.detail;
        this.totalincome = (this.annuallyemployment ? this.annuallyemployment : 0) + (this.annuallyalimony ? this.annuallyalimony : 0) + (this.annuallyincome ? this.annuallyincome : 0);
    }

    handleAlimonyValueEdit(event) {
        this.annuallyalimony = event.detail;
        this.totalincome = (this.annuallyemployment ? this.annuallyemployment : 0) + (this.annuallyalimony ? this.annuallyalimony : 0) + (this.annuallyincome ? this.annuallyincome : 0);
    }

    handleIncomeValueEdit(event) {
        this.annuallyincome = event.detail;
        this.totalincome = (this.annuallyemployment ? this.annuallyemployment : 0) + (this.annuallyalimony ? this.annuallyalimony : 0) + (this.annuallyincome ? this.annuallyincome : 0);
    }
}