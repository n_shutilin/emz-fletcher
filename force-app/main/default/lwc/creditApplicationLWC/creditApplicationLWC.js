import { LightningElement, api, track } from 'lwc';

import saveCreditApplications from '@salesforce/apex/CreditApplicationLWCController.saveCreditApplications';
import getContactByRole from '@salesforce/apex/CreditApplicationLWCController.getContactByRole';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

class CreditApplicationWrapper {
    Opportunity__c = null;
        
    First_Name__c = null;
    Middle_Name__c = null;
    Last_Name__c = null;
    Home_Phone__c = null;
    Email_Address__c = null;
    Education_Level__c = null;
    Number_of_Dependents__c = null;
    Date_of_Birth__c = null;
    Driver_License_Number__c = null;
    Driver_Licnese_State__c = null;
    Driver_License_Country__c = null;
    SSN_Number__c = null;

    Current_Address_Street_1__c = null;
    Current_Address_Street_2__c = null;
    Current_Address_City__c = null;
    Current_Address_State__c = null;
    Current_Address_Zipcode__c = null;
    Current_Address_Country__c = null;
    Current_Address_Years__c = null;
    Current_Address_Months__c = null;
    Previous_Address_1_Street_1__c = null;
    Previous_Address_1_Street_2__c = null;
    Previous_Address_1_City__c = null;
    Previous_Address_1_State__c = null;
    Previous_Address_1_Zipcode__c = null;
    Previous_Address_1_Country__c = null;
    Previous_Address_1_Years__c = null;
    Previous_Address_1_Months__c = null;
    Previous_Address_2_Street_1__c = null;
    Previous_Address_2_Street_2__c = null;
    Previous_Address_2_City__c = null;
    Previous_Address_2_State__c = null;
    Previous_Address_2_Zipcode__c = null;
    Previous_Address_2_Country__c = null;
    Previous_Address_2_Years__c = null;
    Previous_Address_2_Months__c = null;

    Current_Employer_Name__c = null;
    Current_Employer_Position__c = null;
    Current_Employer_Phone__c = null;
    Current_Employer_Badge_Number__c = null;
    Current_Employer_Address_1__c = null;
    Current_Employer_Address_2__c = null;
    Current_Employer_City__c = null;
    Current_Employer_State__c = null;
    Current_Employer_Zipcode__c = null;
    Current_Employer_Country__c = null;
    Current_Employer_Years__c = null;
    Current_Employer_Months__c = null;
    Previous_Employer_Name__c = null;
    Previous_Employer_Position__c = null;
    Previous_Employer_Phone__c = null;
    Previous_Employer_Badge_Number__c = null;
    Previous_Employer_Address_1__c = null;
    Previous_Employer_Address_2__c = null;
    Previous_Employer_City__c = null;
    Previous_Employer_State__c = null;
    Previous_Employer_Zipcode__c = null;
    Previous_Employer_Country__c = null;
    Previous_Employer_Years__c = null;
    Previous_Employer_Months__c = null;

    Employment_Income_Monthly__c = null;
    Alimony_Income_Monthly__c = null;
    Other_Income_Monthly__c = null;
    Employment_Income_Annually__c = null;
    Alimony_Income_Annually__c = null;
    Other_Income_Annually__c = null;
    Alimony_Income_Interval__c = null;
    Other_Income_Source__c = null;

    Personal_Reference_1_First_Name__c = null;
    Personal_Reference_1_Last_Name__c = null;
    Personal_Reference_1_Relationship__c = null;
    Personal_Reference_1_Street__c = null;
    Personal_Reference_1_Street_2__c = null;
    Personal_Reference_1_City__c = null;
    Personal_Reference_1_State__c = null;
    Personal_Reference_1_Postal_Code__c = null;
    Personal_Reference_1_Country__c = null;
    Peresonal_Reference_1_Phone_Number__c = null;
    Personal_Reference_2_First_Name__c = null;
    Personal_Reference_2_Last_Name__c = null;
    Personal_Reference_2_Relationship__c = null;
    Personal_Reference_2_Street__c = null;
    Personal_Reference_2_Street_2__c = null;
    Personal_Reference_2_City__c = null;
    Personal_Reference_2_State__c = null;
    Personal_Reference_2_Postal_Code__c = null;
    Personal_Reference_2_Country__c = null;
    Peresonal_Reference_2_Phone_Number__c = null;
    Personal_Reference_3_First_Name__c = null;
    Personal_Reference_3_Last_Name__c = null;
    Personal_Reference_3_Relationship__c = null;
    Personal_Reference_3_Street__c = null;
    Personal_Reference_3_Street_2__c = null;
    Personal_Reference_3_City__c = null;
    Personal_Reference_3_State__c = null;
    Personal_Reference_3_Postal_Code__c = null;
    Personal_Reference_3_Country__c = null;
    Peresonal_Reference_3_Phone_Number__c = null;

    Bank_Reference_1_Name__c = null;
    Bank_Reference_1_Account_Number__c = null;
    Bank_Reference_1_Address__c = null;
    Bank_Reference_1_Balance__c = null;
    Bank_Reference_1_Account_Type__c = null;
    Bank_Reference_2_Name__c = null;
    Bank_Reference_2_Account_Number__c = null;
    Bank_Reference_2_Address__c = null;
    Bank_Reference_2_Balance__c = null;
    Bank_Reference_2_Account_Type__c = null;
    Bank_Reference_3_Name__c = null;
    Bank_Reference_3_Account_Number__c = null;
    Bank_Reference_3_Address__c = null;
    Bank_Reference_3_Balance__c = null;
    Bank_Reference_3_Account_Type__c = null;

    Current_Vehicle_1_Name__c = null;
    Current_Vehicle_1_Type__c = null;
    Current_Vehicle_1_Finance_Company__c = null;
    Current_Vehicle_1_Account_Number__c = null;
    Current_Vehicle_1_Address__c = null;
    Current_Vehicle_1_Term__c = null;
    Current_Vehicle_1_Opened_Date__c = null;
    Current_Vehicle_1_Payment__c = null;
    Current_Vehicle_1_Closed_Date__c = null;
    Current_Vehicle_2_Name__c = null;
    Current_Vehicle_2_Type__c = null;
    Current_Vehicle_2_Finance_Company__c = null;
    Current_Vehicle_2_Account_Number__c = null;
    Current_Vehicle_2_Address__c = null;
    Current_Vehicle_2_Term__c = null;
    Current_Vehicle_2_Opened_Date__c = null;
    Current_Vehicle_2_Payment__c = null;
    Current_Vehicle_2_Closed_Date__c = null;

    Credit_Reference_1_Type__c = null;
    Credit_Reference_1_Company__c = null;
    Credit_Reference_1_Account_Number__c = null;
    Credit_Reference_1_Address_1__c = null;
    Credit_Reference_1_Payment__c = null;
    Credit_Reference_1_Closed_Date__c = null;
    Credit_Reference_1_Opened_Date__c = null;
    Credit_Reference_1_Term__c = null;
    Credit_Reference_2_Type__c = null;
    Credit_Reference_2_Company__c = null;
    Credit_Reference_2_Account_Number__c = null;
    Credit_Reference_2_Address_1__c = null;
    Credit_Reference_2_Payment__c = null;
    Credit_Reference_2_Closed_Date__c = null;
    Credit_Reference_2_Opened_Date__c = null;
    Credit_Reference_2_Term__c = null;
    Credit_Reference_3_Type__c = null;
    Credit_Reference_3_Company__c = null;
    Credit_Reference_3_Account_Number__c = null;
    Credit_Reference_3_Address_1__c = null;
    Credit_Reference_3_Payment__c = null;
    Credit_Reference_3_Closed_Date__c = null;
    Credit_Reference_3_Opened_Date__c = null;
    Credit_Reference_3_Term__c = null;
    Credit_Reference_4_Type__c = null;
    Credit_Reference_4_Company__c = null;
    Credit_Reference_4_Account_Number__c = null;
    Credit_Reference_4_Address_1__c = null;
    Credit_Reference_4_Payment__c = null;
    Credit_Reference_4_Closed_Date__c = null;
    Credit_Reference_4_Opened_Date__c = null;
    Credit_Reference_4_Term__c = null;

    Residence_Type__c = null;
    Property_Repossessed__c = false;
    Lawsuit_s_Pending__c = false;
    Bankruptcy_Filed__c = false;
    Military_Reserve__c = false;
}

export default class CreditApplicationLWC extends LightningElement {
    @api recordId;

    @track buyer;
    @track cobuyer;

    creditApplicationWrapper = new CreditApplicationWrapper();
    creditCoApplicationWrapper = new CreditApplicationWrapper();
    
    addressesFieldsList = [
        'Current_Address_Street_1__c',
        'Current_Address_Street_2__c',
        'Current_Address_City__c',
        'Current_Address_State__c',
        'Current_Address_Zipcode__c',
        'Current_Address_Country__c',
        'Current_Address_Years__c',
        'Current_Address_Months__c',
        'Previous_Address_1_Street_1__c',
        'Previous_Address_1_Street_2__c',
        'Previous_Address_1_City__c',
        'Previous_Address_1_State__c',
        'Previous_Address_1_Zipcode__c',
        'Previous_Address_1_Country__c',
        'Previous_Address_1_Years__c',
        'Previous_Address_1_Months__c',
        'Previous_Address_2_Street_1__c',
        'Previous_Address_2_Street_2__c',
        'Previous_Address_2_City__c',
        'Previous_Address_2_State__c',
        'Previous_Address_2_Zipcode__c',
        'Previous_Address_2_Country__c',
        'Previous_Address_2_Years__c',
        'Previous_Address_2_Months__c'
    ];

    connectedCallback() {
        this.creditApplicationWrapper.Opportunity__c = this.recordId;
        this.creditCoApplicationWrapper.Opportunity__c = this.recordId;

        getContactByRole({
            oppId: this.recordId,
            role: 'Buyer'
        })
            .then(result => {
                if(result){
                    console.log(result);
                    this.buyer = result;

                    if(this.buyer) {
                        this.creditApplicationWrapper.First_Name__c = this.buyer.FirstName;
                        this.creditApplicationWrapper.Middle_Name__c = this.buyer.MiddleName;
                        this.creditApplicationWrapper.Last_Name__c = this.buyer.LastName;
                        this.creditApplicationWrapper.Home_Phone__c = this.buyer.PersonHomePhone;
                        this.creditApplicationWrapper.Email_Address__c = this.buyer.PersonEmail;
                        this.creditApplicationWrapper.Date_of_Birth__c = this.buyer.PersonBirthdate;
                        this.creditApplicationWrapper.Current_Address_Street__c = this.buyer.BillingStreet;
                        this.creditApplicationWrapper.Current_Address_City__c = this.buyer.BillingCity;
                        this.creditApplicationWrapper.Current_Address_State__c = this.buyer.BillingState;
                        //this.creditApplicationWrapper.Current_Address_Country__c = this.buyer.MailingCountry;
                        this.creditApplicationWrapper.Current_Address_Zipcode__c = this.buyer.BillingPostalCode;
                    }
                }
            })
        .then(() =>getContactByRole({
            oppId: this.recordId,
            role: 'Cobuyer'
        }))
            .then(result => {
                if(result){
                    this.cobuyer = result;

                    if(this.cobuyer) {
                        this.creditCoApplicationWrapper.First_Name__c = this.cobuyer.FirstName;
                        this.creditCoApplicationWrapper.Middle_Name__c = this.cobuyer.MiddleName;
                        this.creditCoApplicationWrapper.Last_Name__c = this.cobuyer.LastName;
                        this.creditCoApplicationWrapper.Home_Phone__c = this.cobuyer.PersonHomePhone;
                        this.creditCoApplicationWrapper.Email_Address__c = this.cobuyer.PersonEmail;
                        this.creditCoApplicationWrapper.Date_of_Birth__c = this.cobuyer.PersonBirthdate;
                        this.creditCoApplicationWrapper.Current_Address_Street__c = this.cobuyer.BillingStreet;
                        this.creditCoApplicationWrapper.Current_Address_City__c = this.cobuyer.BillingCity;
                        this.creditCoApplicationWrapper.Current_Address_State__c = this.cobuyer.BillingState;
                        //this.creditCoApplicationWrapper.Current_Address_Country__c = this.cobuyer.MailingCountry;
                        this.creditCoApplicationWrapper.Current_Address_Zipcode__c = this.cobuyer.BillingPostalCode;
                    }
                }
            })
        .catch(error => {
            console.log('ERROR', error);
        })
    }

    handleValueChange(event){
        let values = event.detail.values;
        
        if (event.detail.role == 'Applicant') {
            values.forEach(element => {
                this.creditApplicationWrapper[element.fieldName] = element.fieldValue;
            });
        } else if (event.detail.role == 'Co-Applicant') {
            values.forEach(element => {
                this.creditCoApplicationWrapper[element.fieldName] = element.fieldValue;
            });
        } 
    }

    handleSave() {
        console.log(this.creditApplicationWrapper);
        console.log(this.creditCoApplicationWrapper);

        let applicant = JSON.stringify(this.creditApplicationWrapper);
        let coapplicant = JSON.stringify(this.creditCoApplicationWrapper);

        saveCreditApplications({
            applicantJSON : applicant,
            coapplicantJSON : coapplicant
        })
            .then(result => {
                this.showToast('Success', 'Records created', 'success');
                this.dispatchEvent(new CustomEvent('close'));
            })
        .catch(error => {
            let errorTitle = 'Error';
            let errorMessage;
            if (error.body.message) {
                errorMessage = error.body.message;
            }
            this.showToast(errorTitle, errorMessage, 'error');
        });
    }

    handleCancel() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    showToast(theTitle, theMessage, theVariant) {
        const event = new ShowToastEvent({
            title: theTitle,
            message: theMessage,
            variant: theVariant
        });
        this.dispatchEvent(event);
    }
}