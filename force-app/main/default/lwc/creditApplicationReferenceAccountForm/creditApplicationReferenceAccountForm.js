import { LightningElement, api, track } from 'lwc';

import getPicklistValues from '@salesforce/apex/CreditApplicationLWCController.getPicklistValues';

export default class CreditApplicationReferenceAccountForm extends LightningElement {
    @api formNumber;
    @api accountType;

    @track accountTypeOptions;

    accType;
    bankName;
    accountNumber;
    address;
    term;
    payment;
    openedDate;
    closedDate;

    fieldAPINameList = [
        'Account_Number__c',
        'Closed_Date__c',
        'Company__c',
        'Opened_Date__c',
        'Payment__c',
        'Term__c',
        'Type__c',
        'Address_1__c'
    ]

    connectedCallback() {
        getPicklistValues({fieldAPIName : this.accountType + 'Type__c'})
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.accountTypeOptions = picklistOptions;
            })
        .catch(error => {console.log('ERROR', error)});
    }

    handleValueChange(event) {
        this[event.target.name] = event.target.value;

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: [{
                    fieldName: this.accountType + event.currentTarget.dataset.fieldname,
                    fieldValue: event.target.value
                }]
            })
        );
    }

    handleClearing() {
        let detailList = [];

        this.accType = null;
        this.bankName = null;
        this.accountNumber = null;
        this.address = null;
        this.term = null;
        this.payment = null;
        this.openedDate = null;
        this.closedDate = null;
        
        this.fieldAPINameList.forEach(element => {
            detailList.push({
                fieldName: this.accountType + element,
                fieldValue: null
            });
        });

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: detailList
            })
        );
    }
}