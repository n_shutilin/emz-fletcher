import { LightningElement, api, track } from 'lwc';

import getPicklistValues from '@salesforce/apex/CreditApplicationLWCController.getPicklistValues';

export default class CreditApplicationReferenceForm extends LightningElement {
    @api referenceType;
    @api componentLabel;

    @track countryPicklistOptions;
    @track statePicklistOptions;

    @track city;
    @track country;
    @track firstName;
    @track lastName;
    @track phoneNumber;
    @track postalCode;
    @track relationship;
    @track state;
    @track street;
    @track street2;

    fieldAPINameList = [
        'City__c',
        'Country__c',
        'First_Name__c',
        'Last_Name__c',
        'Phone_Number__c',
        'Postal_Code__c',
        'Relationship__c',
        'State__c',
        'Street__c',
        'Street_2__c'
    ]

    connectedCallback() {
        getPicklistValues({fieldAPIName: this.referenceType + '_' + 'Country__c'})
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.countryPicklistOptions = picklistOptions;
            })
        .then(() => getPicklistValues({fieldAPIName: this.referenceType + '_' + 'State__c'}))
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.statePicklistOptions = picklistOptions;
            })
        .catch(error => {console.log('ERROR', error)});
    }

    handleValueChange(event) {
        this[event.target.name] = event.target.value;

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: [{
                    fieldName: this.referenceType + '_' + event.currentTarget.dataset.fieldname,
                    fieldValue: event.target.value
                }]
            })
        );
    }

    handleClearing() {
        let detailList = [];

        this.city = null;
        this.country = null;
        this.firstName = null;
        this.lastName = null;
        this.phoneNumber = null;
        this.postalCode = null;
        this.relationship = null;
        this.state = null;
        this.street = null;
        this.street2 = null;

        this.fieldAPINameList.forEach(element => {
            detailList.push({
                fieldName: this.referenceType + '_' + element,
                fieldValue: null
            });
        });

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: detailList
            })
        );
    }
}