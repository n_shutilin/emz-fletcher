import { LightningElement, api } from 'lwc';

export default class CreditApplicationReferencesTab extends LightningElement {
    @api role;

    handleValueChange(event) {
        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: {
                    values: event.detail,
                    role: this.role
                }
            })
        );
    }
}