import { LightningElement, api, track } from 'lwc';

import getPicklistValues from '@salesforce/apex/CreditApplicationLWCController.getPicklistValues';

export default class CreditApplicationResidenceForm extends LightningElement {
    @api role;

    @track typePicklistOptions;

   residenceType;
   propertyRepossessed;
   lawsuitsPending;
   bankruptcyFiled;
   militaryReserve;


    get options() {
        return [
            { label: 'Yes', value: 'true' },
            { label: 'No', value: 'false' },
        ];
    }

    connectedCallback() {
        getPicklistValues({fieldAPIName: 'Residence_Type__c'})
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.typePicklistOptions = picklistOptions;
            })
        .catch(error => {console.log('ERROR', error)});
    }

    handleValueChange(event) {
        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: {
                    values: [{
                        fieldName: event.currentTarget.dataset.fieldname,
                        fieldValue: event.target.value
                    }],
                    role: this.role
                }
            })
        );
    }
}