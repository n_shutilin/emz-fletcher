import { LightningElement, api, track } from 'lwc';

import getPicklistValues from '@salesforce/apex/CreditApplicationLWCController.getPicklistValues';

export default class CreditApplicationVehicleForm extends LightningElement {
    @api componentLabel;
    @api vehicleType;

    @track vehicleTypeOptions;

    accountNumber;
    address;
    closedDate;
    financeCompany;
    name;
    openedDate;
    payment;
    term;
    type;

    fieldAPINameList = [
        'Account_Number__c',
        'Address __c',
        'Closed_Date__c',
        'Finance_Company__c',
        'Name__c',
        'Opened_Date__c',
        'Payment__c',
        'Term__c',
        'Type__c'
    ]

    connectedCallback() {
        getPicklistValues({fieldAPIName : this.vehicleType + 'Type__c'})
            .then(result => {
                let picklistOptions = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    if(element.active) {
                        picklistOptions.push({label: element.label, value: element.value});
                    }
                });
                this.vehicleTypeOptions = picklistOptions;
            })
        .catch(error => {console.log('ERROR', error)});
    }

    handleValueChange(event) {
        this[event.target.name] = event.target.value;

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: [{
                    fieldName: this.vehicleType + event.currentTarget.dataset.fieldname,
                    fieldValue: event.target.value
                }]
            })
        );
    }

    handleClearing() {
        let detailList = [];

        this.accountNumber = null;
        this.address = null;
        this.closedDate = null;
        this.financeCompany = null;
        this.name = null;
        this.openedDate = null;
        this.payment = null;
        this.term = null;
        this.type = null;

        this.fieldAPINameList.forEach(element => {
            detailList.push({
                fieldName: this.vehicleType + element,
                fieldValue: null
            });
        });

        this.dispatchEvent(
            new CustomEvent('valuechanged', {
                detail: detailList
            })
        );
    }
}