import {LightningElement} from 'lwc';
import LightningDatatable from 'lightning/datatable';
import referenceTemplate from './emailMessageLink.html';

export default class CustomEmailMessageDataTable extends LightningDatatable {
    static customTypes = {
        reference: {
            template: referenceTemplate,
            typeAttributes: ['id']
        }
    }
}