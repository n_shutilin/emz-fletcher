import LightningDatatable from 'lightning/datatable';
import customImageTemp from './customImageTemp.html';
import referenceTemplate from './referenceTemplate.html';
import userReferenceTemplate from './userReferenceTemplate.html';

export default class CustomLightningDatatable extends LightningDatatable {
    static customTypes = {
        customImage: {
            template: customImageTemp,
        },
        reference: {
            template: referenceTemplate,
            typeAttributes: ['id']
        },
        userReference: {
            template: userReferenceTemplate,
            typeAttributes: ['id']
        }
    };
}