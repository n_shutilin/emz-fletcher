import { LightningElement, track, api, wire } from 'lwc';
import { getObjectIconMap } from './customLookupIcons';
import search from '@salesforce/apex/CustomLookupCtrl.search';
import getSObjectById from '@salesforce/apex/CustomLookupCtrl.getSObjectById';

const SEARCH_DELAY = 300; // Wait 300 ms after user stops typing then, peform search

export default class CustomLookup extends LightningElement {
    @api label;
    @api placeholder = '';
    @api iconName;
    @api isMultiEntry = false;
    @api errors = [];
    @api scrollAfterNItems;
    @api objectApiName;
    @api value;
    @api labelClass;
    @api isEditMode = false;

    @wire(getSObjectById, { objectApiName : '$objectApiName', id : '$value' })
    getSelectedObject(result) {
        if (result.data) {
            this.selection = [result.data];
            if (this.selection.length) {
                this.outputLabel = this.selection[0].name;
                this.outputLink = '/' + this.selection[0].id;
            }
            if (!this.iconName || getObjectIconMap()[this.objectApiName] !== this.iconName) {
                if (getObjectIconMap().hasOwnProperty(this.objectApiName)) {
                    this.iconName = getObjectIconMap()[this.objectApiName];
                } else {
                    this.iconName = 'standard:default';
                }
            }
        } else {
            this.selection = [];
        }
    }

    @track searchTerm = '';
    @track searchResults = [];
    @track hasFocus = false;
    @track selection = [];
    @track outputLabel;
    @track outputLink;

    cleanSearchTerm;
    blurTimeout;
    searchThrottlingTimeout;
    _objectApiName;

    @api
    getSelection() {
        return this.selection;
    }

    updateSearchTerm(newSearchTerm) {
        this.searchTerm = newSearchTerm;
        let MinSearchTermLength = this.objectApiName === 'Contact' ? 2 : 0;

        // Compare clean new search term with current one and abort if identical
        const newCleanSearchTerm = newSearchTerm
            .trim()
            .replace(/\*/g, '')
            .toLowerCase();
        if (this.cleanSearchTerm === newCleanSearchTerm) {
            return;
        }

        // Save clean search term
        this.cleanSearchTerm = newCleanSearchTerm;

        // Ignore search terms that are too small
        if (newCleanSearchTerm.length < MinSearchTermLength) {
            this.searchResults = [];
            return;
        }

        // Apply search throttling (prevents search if user is still typing)
        if (this.searchThrottlingTimeout) {
            clearTimeout(this.searchThrottlingTimeout);
        }
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.searchThrottlingTimeout = setTimeout(() => {
            // Send search event if search term is long enougth

            if (this.cleanSearchTerm.length >= MinSearchTermLength) {
                search({
                    searchString: this.cleanSearchTerm,
                    selectedIds: this.selection.map(element => element.id),
                    objectApiName: this.objectApiName
                }).then(result => {
                    this.searchResults = result;
                });
            }
            this.searchThrottlingTimeout = null;
        }, SEARCH_DELAY);
    }

    isSelectionAllowed() {
        if (this.isMultiEntry) {
            return true;
        }
        return !this.hasSelection();
    }

    hasResults() {
        return this.searchResults.length > 0;
    }

    hasSelection() {
        return this.selection.length > 0;
    }

    handleInput(event) {
        // Prevent action if selection is not allowed
        if (!this.isSelectionAllowed()) {
            return;
        }
        this.updateSearchTerm(event.target.value);
    }

    handleResultClick(event) {
        const recordId = event.currentTarget.dataset.recordid;

        // Save selection
        let selectedItem = this.searchResults.filter(
            result => result.id === recordId
        );
        if (selectedItem.length === 0) {
            return;
        }
        this.template.querySelector('input').classList.remove('has-error');
        this.errors = [];
        selectedItem = selectedItem[0];
        const newSelection = [...this.selection];
        newSelection.push(selectedItem);
        this.selection = newSelection;
        // Reset search
        this.searchTerm = '';
        this.searchResults = [];

        // Notify parent components that selection has changed
        this.dispatchEvent(new CustomEvent('valuechange', { detail: this.selection }));
    }

    handleComboboxClick() {
        // Hide combobox immediatly
        if (this.blurTimeout) {
            window.clearTimeout(this.blurTimeout);
        }
        this.hasFocus = false;
    }

    handleFocus(event) {
        // Prevent action if selection is not allowed
        if (!this.isSelectionAllowed()) {
            return;
        }
        this.hasFocus = true;
        this.handleInput(event);
    }

    handleBlur() {
        // Prevent action if selection is not allowed
        if (!this.isSelectionAllowed()) {
            return;
        }
        // Delay hiding combobox so that we can capture selected result
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.blurTimeout = window.setTimeout(() => {
            this.hasFocus = false;
            this.blurTimeout = null;
        }, 300);
    }

    handleRemoveSelectedItem(event) {
        const recordId = event.currentTarget.name;
        console.log('selection', this.selection);
        this.selection = this.selection.filter(item => item.id !== recordId);
        // Notify parent components that selection has changed
        //this.dispatchEvent(new CustomEvent('valuechange', { detail: this.selection }));
    }

    @api
    handleClearSelection() {
        this.selection = [];
        // Notify parent components that selection has changed
        this.dispatchEvent(new CustomEvent('valuechange', { detail: this.selection }));
    }

    @api
    validate() {
        if (this.value) {
            this.template.querySelector('input').classList.remove('has-error');
            this.errors = [];
        } else {
            this.template.querySelector('input').classList.add('has-error');
            this.errors = [{'id':'error', 'message':'Complete this field.'}];
        }
    }

    // STYLE EXPRESSIONS

    get getContainerClass() {
        let css = 'slds-combobox_container slds-has-inline-listbox ';
        if (this.hasFocus && this.hasResults()) {
            css += 'slds-has-input-focus ';
        }
        if (this.errors.length > 0) {
            css += 'has-custom-error';
        }
        return css;
    }

    get getDropdownClass() {
        let css =
            'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click ';
        if (this.hasFocus && this.hasResults()) {
            css += 'slds-is-open';
        } else {
            css += 'slds-combobox-lookup';
        }
        return css;
    }

    get getInputClass() {
        let css =
            'slds-input slds-combobox__input has-custom-height ' +
            (this.errors.length === 0 ? '' : 'has-custom-error ');
        if (!this.isMultiEntry) {
            css +=
                'slds-combobox__input-value ' +
                (this.hasSelection() ? 'has-custom-border' : '');
        }
        return css;
    }

    get getComboboxClass() {
        let css = 'slds-combobox__form-element slds-input-has-icon ';
        if (this.isMultiEntry) {
            css += 'slds-input-has-icon_right';
        } else {
            css += this.hasSelection()
                ? 'slds-input-has-icon_left-right'
                : 'slds-input-has-icon_right';
        }
        return css;
    }

    get getSearchIconClass() {
        let css = 'slds-input__icon slds-input__icon_right ';
        if (!this.isMultiEntry) {
            css += this.hasSelection() ? 'slds-hide' : '';
        }
        return css;
    }

    get getClearSelectionButtonClass() {
        return (
            'slds-button slds-button_icon slds-input__icon slds-input__icon_right ' +
            (this.hasSelection() ? '' : 'slds-hide')
        );
    }

    get getSelectIconName() {
        return this.hasSelection()
            ? this.selection[0].icon
            : 'standard:default';
    }

    get getSelectIconClass() {
        return (
            'slds-combobox__input-entity-icon ' +
            (this.hasSelection() ? '' : 'slds-hide')
        );
    }

    get getInputValue() {
        if (this.isMultiEntry) {
            return this.searchTerm;
        }
        return this.hasSelection() ? this.selection[0].name : this.searchTerm;
    }

    get getListboxClass() {
        return (
            'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid ' +
            (this.scrollAfterNItems
                ? 'slds-dropdown_length-with-icon-' + this.scrollAfterNItems
                : '')
        );
    }

    get isInputReadonly() {
        if (this.isMultiEntry) {
            return false;
        }
        return this.hasSelection();
    }

    get isExpanded() {
        return this.hasResults();
    }
}