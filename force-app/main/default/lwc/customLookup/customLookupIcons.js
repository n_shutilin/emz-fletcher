function getObjectIconMap() {
    return {
        'Account' : 'standard:account',
        'Campaign' : 'standard:campaign',
        'Case' : 'standard:case',
        'Contact' : 'standard:contact',
        'Contract' : 'standrad:contract',
        'Event' : 'standard:event',
        'Lead' : 'standard:lead',
        'Opportunity' : 'standard:opportunity',
        'Pricebook2' : 'standrard:pricebook',
        'Product2' : 'standard:product',
        'Quote' : 'standard:quotes',
        'Task' : 'standard:task',
        'User' : 'standard:user'
    }
}

export {
    getObjectIconMap
}