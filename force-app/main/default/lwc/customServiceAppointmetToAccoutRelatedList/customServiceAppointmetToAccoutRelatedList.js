import { LightningElement, api, wire, track } from 'lwc';
import getAppointmentsListWithLimit from '@salesforce/apex/CustomRelatedListController.getAppointmentsListWithLimit';
import numberOfPages from '@salesforce/apex/CustomRelatedListController.calculateNumberOfPages';
import countNumberOfServiceAppointment from '@salesforce/apex/CustomRelatedListController.getNumberOfAppointments';
import getFieldSet from '@salesforce/apex/CustomRelatedListController.getFieldSet';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';

const STANDARD_FIELD_TO_DISPLAY = 'AppointmentNumber';
export default class CustomServiceAppointmetToAccoutRelatedList extends LightningElement {

    @api recordId;
    @api recordTypeName;
    @api title;
    @api channelName = '/event/Scheduled_Appointment_Event__e';

    value = 10;
    currentPage = 1;
    isFirst = true;
    isLast = false;
    offset = 0;
    isNoPages = false;

    @track loaded = false;

    @track sortDirection = 'desc';
    @track sortedBy = 'SchedStartTime';
    @track columns = [];
    @track error;
    @track data;
    @track numberOfPages;
    @track numberOfServiceAppointment;

    get options() {
        return [
            { label: '10', value: 10 },
            { label: '20', value: 20 },
            { label: '50', value: 50 },
            { label: '100', value: 100 }
        ];
    }

    connectedCallback() {
        this.initFunction();
        this.subscribe();
    }

    initFunction() {
        this.getColumns();
        this.getNumberOfPages();
        this.getNumberOfServiceAppointment();
        this.fetchData(this.sortedBy);
    }

    subscribe() {
        const messageCallback = (response) => {
            let event = JSON.parse(JSON.stringify(response));
            if(event.data.payload.Parent_Record_Id__c === this.recordId) {
                this.initFunction();
            }
        };
        subscribe(this.channelName, -1, messageCallback).then(response => {
            this.subscription = response;
        });
    }

    handleChange(event) {
        this.value = event.detail.value;
        this.currentPage = 1;
        this.isFirst = true;
        this.isLast = false;
        this.isNoPages = false;
        this.initFunction();
    }

    handleNext(event) {
        +this.currentPage++;
        this.isFirst = false;
        if (this.currentPage === this.numberOfPages) {
            this.isLast = true;
        }
        this.offset = (+this.currentPage - 1) * +this.value;
        this.fetchData(this.sortedBy);
    }

    handlePrev(event) {
        +this.currentPage--;
        this.isLast = false;
        if (this.currentPage === 1) {
            this.isFirst = true;
        }
        this.offset = (+this.currentPage - 1) * +this.value;
        this.fetchData(this.sortedBy);
    }

    handlePageChange(event) {
        this.isLast = false;
        this.isFirst = false;
        let inputPage = Math.floor(event.target.value);
        if (inputPage >= 1 && inputPage <= this.numberOfPages) {
            this.currentPage = inputPage;
            if (inputPage === 1) {
                this.isFirst = true;
            } else {
                if (inputPage === this.numberOfPages) {
                    this.isLast = true;
                }
            }
        } else if (inputPage < 1) {
            this.currentPage = 1
            this.isFirst = true;
        } else {
            this.currentPage = this.numberOfPages;
            this.isLast = true;
        }
        this.offset = (+this.currentPage - 1) * +this.value;
        this.fetchData(this.sortedBy);
    }

    getNumberOfPages() {
        numberOfPages({ onPageLimit: this.value, accountId: this.recordId, recordTypeName: this.recordTypeName }).then(result => {
            this.numberOfPages = result;
            if(result<=1){
                this.isFirst = true;
                this.isLast = true;
                this.isNoPages = true;
            }
        });
    }

    getColumns() {
        let tempColumns = [{
            label: 'Appointment Number',
            fieldName: 'url',
            type: 'url',
            typeAttributes: { label: { fieldName: "AppointmentNumber" } },
            sortable: true,
            cellAttributes: { alignment: 'left' },
            hideDefaultActions: true
        }];
        getFieldSet({ recordTypeName: this.recordTypeName }).then(result => {
            Object.keys(result).forEach(key => {
                if (result[key].fieldName !== STANDARD_FIELD_TO_DISPLAY) {
                    let column = {};
                    column["label"] = result[key].label;
                    column["fieldName"] = result[key].fieldName;
                    column["type"] = this.handleColumnType(result[key].type);
                    column["typeAttributes"] = this.handleColumnTypeAttribute(result[key].type);
                    column["sortable"] = this.handleCoumnSortable(result[key].type);
                    column["cellAttributes"] = { alignment: 'left' };
                    column["hideDefaultActions"] = true;
                    tempColumns.push(column);
                }
            });
            this.columns = tempColumns;
        });
    }

    fetchData(sortedFieldName) {
        this.loaded = false;
        if (sortedFieldName === 'url') {
            sortedFieldName = STANDARD_FIELD_TO_DISPLAY;
        }
        getAppointmentsListWithLimit({ offset: this.offset, onPageLimit: this.value, accountId: this.recordId, sortedFieldName: sortedFieldName, directionToSort: this.sortDirection, recordTypeName: this.recordTypeName }).then(result => {
            let tempData = [];
            if (result) {
                Object.keys(result).forEach(key => {
                    let temp = {}
                    temp["url"] = result[key].url;
                    let appointment = result[key].appointment;
                    Object.keys(appointment).forEach(appointmentField => {
                        const value = appointment[appointmentField];
                        if (typeof value !== 'object') {
                            temp[appointmentField] = value;
                        } else {
                            let innerObject = value;
                            Object.keys(innerObject).forEach(innerObjectField => {
                                const innerFieldValue = innerObject[innerObjectField];
                                temp[appointmentField + '.' + innerObjectField] = innerFieldValue;
                            });
                        }
                    });
                    tempData.push(temp);
                });
            }
            this.loaded = true;
            this.data = tempData;
        });
    }

    getNumberOfServiceAppointment() {
        countNumberOfServiceAppointment({ accountId: this.recordId, recordTypeName: this.recordTypeName }).then(result => {
            if (result >= 100) {
                this.numberOfServiceAppointment = '100+'
            } else {
                this.numberOfServiceAppointment = result;
            }

        });
    }
    handleColumnType(type) {
        let dataTableType = type;
        switch (type) {
            case "datetime":
                dataTableType = "date";
                break;
            case "string":
                dataTableType = "text";
                break;
        }
        return dataTableType;
    }
    handleColumnTypeAttribute(type) {
        let attribute = {};
        switch (type) {
            case "datetime":
                attribute = {
                    day: 'numeric',
                    month: 'short',
                    year: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit',
                    second: '2-digit',
                    hour12: true
                };
                break;
        }
        return attribute;
    }
    handleCoumnSortable(type) {
        let isSortable = false;
        switch (type) {
            case "datetime":
                isSortable = true;
                break;
            case "string":
                isSortable = true;
                break;
        }
        return isSortable;
    }
    onHandleSort(event) {
        const { fieldName: sortedBy, sortDirection } = event.detail;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
        this.fetchData(event.detail.fieldName);
    }
}