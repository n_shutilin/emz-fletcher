import {LightningElement, track, wire, api} from 'lwc';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled }  from 'lightning/empApi';
import getUserOptions from '@salesforce/apex/EmailManagementCtrl.getUserOptions';
import getPagesCount from '@salesforce/apex/EmailManagementCtrl.getPagesCount';
import getMessages from '@salesforce/apex/EmailManagementCtrl.getMessages';

const columns = [
    {
        label: 'Opened', fieldName: 'opened', initialWidth: 100,
        cellAttributes: {
            iconName: {
                fieldName: 'openedEmail'
            },
        }
    },
    {label: 'From', fieldName: 'fromAddress'},
    {label: 'To', fieldName: 'toAddress'},
    {
        label: 'Subject', fieldName: 'subject', type: 'reference', typeAttributes: {id: {fieldName: 'id'}}
    },
    {
        label: 'Related Records', fieldName: 'recordLink', type: 'url',
        typeAttributes: {
            label: {fieldName: 'recordName'},
            // tooltip: {fieldName: 'recordName'},
            target: '_blank'
        }
    },
    {label: 'Date/Time', fieldName: 'sendingDateTime'}
];
const minFilterLength = 3;

export default class EmailManagement extends LightningElement {
    @track isLoaded = false;
    @track userOptions;
    @track selectedUser;
    @track selectedDate;
    @track filterString;
    @track data;
    @track columns = columns;
    @track pageNum = 1;
    @track pagesCount = 1;
    @track isPrevDisabled = true;
    @track isNextDisabled = true;
    @track emailValue = 'inbound';
    @api openerEvt = '/event/Email_Opener__e';
    subscription = {};

    connectedCallback() {
        this.handleSubscribe();
    }

    handleSubscribe() {
        let self = this;

        const messageCallback = function(response) {
            let obj = JSON.parse(JSON.stringify(response));
            let openedMessageId = obj.data.payload.Email_Message_Id__c;

            for (let message of self.data) {
                if (message.id === openedMessageId) {
                    message.openedEmail = 'utility:email_open';
                    message.isOpened = true;
                    break;
                }
            }

            let dataStr = JSON.stringify(self.data);
            self.data = JSON.parse(dataStr);
        };

        subscribe(self.openerEvt, -1, messageCallback).then(response => {
            self.subscription = response;
        });
    }

    get emailOptions() {
        return [
            { label: 'All', value: 'all' },
            { label: 'Inbound', value: 'inbound'},
            { label: 'Outbound', value: 'outbound' },
            { label: 'Unread', value: 'unread' },
        ];
    }

    handleEmailOptionsChange(event) {
        this.emailValue = event.detail.value;

        this.setPagesCount();
        this.getData();
    }

    @wire(getUserOptions)
    getPicklistValues({error, data}) {
        if (data) {
            this.userOptions = data;
            this.selectedUser = data[0].value;
            this.setPagesCount();
            this.getData();
        } else if (error) {
            console.log('--- error: ' + JSON.stringify(error));
        }
    }

    getData() {
        this.isLoaded = false;

        getMessages({
            'address': this.selectedUser,
            'dateStr': this.selectedDate,
            'filterStr': this.filterString,
            'pageNum': this.pageNum,
            'emailOption': this.emailValue
        })
            .then(results => {
                let emailMessageData  = [];

                results.forEach(result => {
                    const res = {... result};

                    if (res.isInbound) {
                        if (res.isOpened) {
                            res.openedEmail = 'utility:email_open';
                        } else {
                            res.openedEmail = 'utility:email';
                        }
                    }

                    emailMessageData.push(res);
                });

                this.data = emailMessageData;
                this.isLoaded = true;
            })
            .catch((error) => {
                console.log(error);
            })
    }

    setPagesCount() {
        this.pageNum = 1;

        getPagesCount({
            'address': this.selectedUser,
            'dateStr': this.selectedDate,
            'filterStr': this.filterString,
            'emailOption': this.emailValue
        })
            .then(result => {
                this.isPrevDisabled = true;
                this.isNextDisabled = result < 2;
                this.pagesCount = result;
            })
            .catch((error) => {
                console.log('--- error: ' + JSON.stringify(error));
            })
    }

    handlePrev() {
        this.pageNum--;
        this.isPrevDisabled = this.pageNum === 1;
        this.isNextDisabled = false;
        this.getData();
    }

    handleNext() {
        this.pageNum++;
        this.isPrevDisabled = false;
        this.isNextDisabled = this.pageNum === this.pagesCount;
        this.getData();
    }

    handleUserChange(event) {
        this.selectedUser = event.detail.value;
        this.setPagesCount();
        this.getData();
    }

    handleDateChange(event) {
        this.selectedDate = event.detail.value;
        this.setPagesCount();
        this.getData();
    }

    handleTextFilterChange(event) {
        let filterString = event.detail.value;

        if (!filterString || filterString.length >= minFilterLength) {
            this.filterString = filterString;
            this.setPagesCount();
            this.getData();
        }
    }
}