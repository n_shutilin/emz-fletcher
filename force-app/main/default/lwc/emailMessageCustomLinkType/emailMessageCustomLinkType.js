import {LightningElement, api, track} from 'lwc';
import deleteOpener from '@salesforce/apex/EmailManagementCtrl.deleteOpener';

export default class EmailMessageCustomLinkType extends LightningElement {
    @api recId;
    @api recordSubject;

    @track finalUrl;


    connectedCallback() {
        const LIGHTNING_EMAIL_MESSAGE_URL = '/lightning/r/EmailMessage/';
        const VIEW_URL_PARAM = '/view';
        this.finalUrl = LIGHTNING_EMAIL_MESSAGE_URL + this.recId + VIEW_URL_PARAM;
    }

    deleteOpener() {
        deleteOpener({
            'recId': this.recId
        })
            .then(result => {

            })
            .catch(error =>{

            })
    }
}