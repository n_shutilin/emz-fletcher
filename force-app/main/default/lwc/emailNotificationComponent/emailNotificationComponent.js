import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class EmailNotificationComponent extends NavigationMixin(LightningElement) {
    @api message

    get subject() {
        return this.message.subject
    }

    get from() {
        return this.message.fromAddress
    }

    get date() {
        return this.message.sendingDateTime
    }

    openMessage(event) {
        console.log('test click')
        this.accountHomePageRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: this.message.id,
                objectApiName: 'EmailMessage',
                actionName: 'view'
            }
        };
        this[NavigationMixin.GenerateUrl](this.accountHomePageRef)
            .then(url => { window.open(url) });
    }
}