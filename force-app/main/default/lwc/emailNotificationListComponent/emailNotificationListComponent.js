import { LightningElement, track } from 'lwc';
// import getMessages from '@salesforce/apex/EmailManagementCtrl.getMessages';
import getMessages from '@salesforce/apex/EmailManagementCtrl.getInboundEmailNotifications';

export default class EmailNotificationListComponent extends LightningElement {
    @track isLoaded = false
    @track messages = []

    connectedCallback() {
        this.getData()
    }

    getData() {
        this.isLoaded = false

        getMessages()
            .then(results => {
                this.messages = results
                this.isLoaded = true
                console.log(this.messages)
            })
            .catch((error) => {
                console.log(error)
            })
    }
}