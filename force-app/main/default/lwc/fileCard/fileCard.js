import {LightningElement, api} from 'lwc';
import {NavigationMixin} from "lightning/navigation";

export default class FileCard extends NavigationMixin(LightningElement) {
    @api
    video;

    handleVideoClick(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state:{
                selectedRecordId: event.currentTarget.dataset.id
            }
        });
    }
}