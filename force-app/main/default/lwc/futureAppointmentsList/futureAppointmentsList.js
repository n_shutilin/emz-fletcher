import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { subscribe } from 'lightning/empApi';
import getAppointmentActivities from '@salesforce/apex/FutureAppointmentsController.getAppointmentActivities';
import markShownAppointment from '@salesforce/apex/FutureAppointmentsController.markShownAppointment';
import cancelAppointment from '@salesforce/apex/FutureAppointmentsController.cancelAppointment';

const actions = [
    { label: 'Cancel', name: 'cancel' },
    { label: 'Mark Shown', name: 'mark_shown' }
];

const columns = [
    { label: 'Appointment Type', fieldName: 'appointmentType', type: 'reference',
        typeAttributes: {id: {fieldName: 'appointmentId'}}, sortable: true },
    { label: 'Appointment Date/Time', fieldName: 'appointmentDateTime', type: 'date',
        typeAttributes: { year: "numeric", month: "numeric", day: "numeric", hour: "2-digit", minute: "2-digit" }, sortable: true },
    { label: 'Sales Manager', fieldName: 'salesManager' },
    { label: 'Salesperson', fieldName: 'salesperson' },
    { label: 'Created By', fieldName: 'createdBy' },
    {
        type: 'action',
        typeAttributes: { rowActions: actions },
    }
];

export default class futureAppointmentsList extends LightningElement {
    @api recordId;
    @api objectApiName;
    @api channelName = '/event/Future_Appointment_Event__e';

    fullData = [];
    data = [];
    columns = columns;
    record = {};

    dataSize = 0;
    defaultDataSize = 10;
    isDataEmpty;

    defaultSortDirection = 'asc';
    sortDirection = 'asc';
    sortedBy;

    rescheduleModalView;
    appointmentId;

    isSpinner;

    connectedCallback() {
        this.initMethod();
        this.subscribe();
    }

    initMethod() {
        this.retrieveAppointmentData();
        this.checkObjectActions();
    }

    retrieveAppointmentData() {
        getAppointmentActivities({recordId: this.recordId})
            .then(result => {
                this.fullData = result;
                if (this.fullData.length > 0) {
                    this.data = this.fullData.slice(0, this.defaultDataSize);
                    this.dataSize = this.defaultDataSize;
                    this.isDataEmpty = false;
                } else {
                    this.isDataEmpty = true;
                }
            })
            .catch();
    }

    subscribe() {
        const messageCallback = (response) => {
            let event = JSON.parse(JSON.stringify(response));
            if(event.data.payload.Parent_Record_Id__c === this.recordId) {
                this.rescheduleModalView = false;
                this.initMethod();
            }
        };
        subscribe(this.channelName, -1, messageCallback).then(response => {
            this.subscription = response;
        });
    }

    checkObjectActions() {
        const isRescheduleAction = actions.some(action => action.name === 'reschedule');

        if (this.objectApiName === 'Opportunity' && !isRescheduleAction) {
            actions.splice(1, 0, { 'label': 'Reschedule', 'name': 'reschedule' });
        }
    }

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        switch (actionName) {
            case 'cancel':
                this.handleCancelAction(row.appointmentId);
                break;
            case 'reschedule':
                this.rescheduleModalView = true;
                this.appointmentId = row.appointmentId;
                break;
            case 'mark_shown':
                this.handleMarkShownAction(row.appointmentId);
                break;
            default:
        }
    }

    handleCancelAction(appointmentId) {
        this.isSpinner = true;
        cancelAppointment({ appointmentId })
            .then(() => {
                this.retrieveAppointmentData();
                this.showToast('Success', 'The appointment cancelled', 'success');
                this.isSpinner = false;
            })
            .catch((error) => {
                let errorMessage = '';
                if (error.body.message) {
                    errorMessage = error.body.message;
                }

                this.showToast('Error', errorMessage, 'error');
                this.isSpinner = false;
            })
    }

    handleMarkShownAction(appointmentId) {
        this.isSpinner = true;
        markShownAppointment({ appointmentId })
        .then(() => {
            this.retrieveAppointmentData();
            this.showToast('Success', 'The appointment marked as shown', 'success');
            this.isSpinner = false;
        })
        .catch((error) => {
            let errorMessage = '';
            if (error.body.message) {
                errorMessage = error.body.message;
            }

            this.showToast('Error', errorMessage, 'error');
            this.isSpinner = false;
        })
    }

    sortBy(field, reverse, primer) {
        const key = primer
            ? function (x) {
                return primer(x[field]);
            }
            : function (x) {
                return x[field].toLowerCase();
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    handleSort(event) {
        const {fieldName: sortedBy, sortDirection} = event.detail;
        const cloneData = [...this.fullData];
        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.fullData = cloneData;
        this.data = cloneData.slice(0, this.dataSize);
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }

    handleShowMore() {
        this.dataSize += 10;
        this.data = this.fullData.slice(0, this.dataSize);
    }

    handleShowLess() {
        this.dataSize = this.defaultDataSize;
        this.data = this.fullData.slice(0, this.dataSize);
    }

    closeRescheduleModal() {
        this.rescheduleModalView = false;
    }

    showToast(title, message, variant) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });

        this.dispatchEvent(event);
    }

    get isDisplayShowMoreButton() {
        return this.defaultDataSize < this.fullData.length;
    }

    get allDataShown() {
        return this.dataSize >= this.fullData.length;
    }

    get displayedDataSize() {
        return `(${this.fullData.length})`
    }
}