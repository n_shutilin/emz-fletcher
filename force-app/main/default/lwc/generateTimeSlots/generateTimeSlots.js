import { LightningElement, track, wire, api } from 'lwc';
import saveTimeSlots from '@salesforce/apex/TimeSlots.saveTimeSlots';

export default class GenerateTimeSlots extends LightningElement {
    
    index = {
    "Monday": 0,
    "Tuesday": 0,
    "Wednesday": 0,
    "Thursday": 0,
    "Friday": 0,
    "Saturday": 0,
    "Sunday": 0
    }



    @api recordId;
    dayOfweek = '';


    options = [
        {value: '15', label: '15'},
        {value: '30', label: '30'},
        {value: '60', label: '60'},
        {value: '90', label: '90'}
    ];

    @track mondayRows = [];
    @track tuesdayRows = [];
    @track wednesdayRows = [];
    @track thursdayRows = [];
    @track fridayRows = [];
    @track saturdayRows = [];
    @track sundayRows = [];

    allRows = {
       "Monday": this.mondayRows,
       "Tuesday": this.tuesdayRows,
       "Wednesday": this.wednesdayRows,
       "Thursday": this.thursdayRows,
       "Friday": this.fridayRows,
       "Saturday": this.saturdayRows,
       "Sunday": this.sundayRows
    }

    passRows = [];


    handleFrom(event){
        try{
        let selectedRow = event.target;
        let dayOfWeek = selectedRow.dataset.day;
        let key = selectedRow.dataset.id;
        let inputValue = selectedRow.value;
        let inputFrom=this.template.querySelector("."+dayOfWeek+"Increment");
        let to = this.template.querySelector("."+dayOfWeek+"To");
        let timeFrom = new Date("2020-01-01T"+inputValue).getHours();
        let timeTo =  new Date("2020-01-01T"+to.value).getHours();
        if(timeTo - timeFrom < 1 && (inputFrom.value == "90" || inputFrom.value == "60")) {
            selectedRow.setCustomValidity("The selected increment is not correct for the time range.");
            selectedRow.reportValidity();
        }else {
        this.allRows[dayOfWeek][key].fromTime = event.target.value;
        console.log(JSON.parse(JSON.stringify(this.allRows[dayOfWeek])));
        //reset an error
        selectedRow.setCustomValidity('');
        inputFrom.setCustomValidity('');
        to.setCustomValidity('');
        }
        to.reportValidity();
        inputFrom.reportValidity();
        selectedRow.reportValidity();
        }catch (error) {
            console.log(error);
          }
    }
    handleTo(event){
        try{
        let selectedRow = event.target;
        let dayOfWeek = selectedRow.dataset.day;
        let inputValue = selectedRow.value;
        let key = selectedRow.dataset.id;
        let inputFrom=this.template.querySelector("."+dayOfWeek+"Increment");
        let from = this.template.querySelector("."+dayOfWeek+"From");
        let timeFrom = new Date("2020-01-01T"+from.value).getHours();
        let timeTo =  new Date("2020-01-01T"+inputValue).getHours();
        if(timeTo - timeFrom < 1 && (inputFrom.value == "90" || inputFrom.value == "60")) {
            selectedRow.setCustomValidity("The selected increment is not correct for the time range.");
            selectedRow.reportValidity();
        }else {         
            //reset an error
            selectedRow.setCustomValidity('');
            inputFrom.setCustomValidity('');
            from.setCustomValidity('');
            this.allRows[dayOfWeek][key].toTime = event.target.value;
            console.log(JSON.parse(JSON.stringify(this.allRows[dayOfWeek])));
        }
        selectedRow.reportValidity();
        inputFrom.reportValidity();
        from.reportValidity(); 
        }catch (error) {
            console.log(error);
          }
    }

    handleIncrement(event){
        try{
            let selectedRow = event.target;
            let inputValue = selectedRow.value;
            let dayOfWeek = selectedRow.dataset.day;
            let key = selectedRow.dataset.id;
            console.log(selectedRow.value);
            let from=this.template.querySelector("."+dayOfWeek+"From"); 
            let to = this.template.querySelector("."+dayOfWeek+"To");
            let timeFrom = new Date("2020-01-01T"+from.value).getHours();
            let timeTo =  new Date("2020-01-01T"+to.value).getHours();
            if(timeTo - timeFrom < 1 && (inputValue == "90" || inputValue == "60")) {
             //set an error
             selectedRow.setCustomValidity("The selected increment is not correct for the time range.");
             selectedRow.reportValidity();
            }else {         
                //reset an error
                selectedRow.setCustomValidity('');
                from.setCustomValidity('');
                to.setCustomValidity('');

                this.allRows[dayOfWeek][key].increment = selectedRow.value;
            }
            selectedRow.reportValidity();
            from.reportValidity();
            to.reportValidity();
        }catch (error) {
                console.log(error);
            }
    }

    addRow(event){
        let dayOfWeek = event.target.value;
        this.index[dayOfWeek]++;
        let i = this.index[dayOfWeek];
        let row = {
            "index": i,
            "dayOfWeek": dayOfWeek,
            "fromTime":'',
            "toTime":'',
            "increment":0
        };
        this.allRows[dayOfWeek].push(row);
    }
    removeRow(event){
        try{
            let dayOfWeek = event.target.value;
            var selectedRow = event.target;
            var key = selectedRow.dataset.id;
            console.log(selectedRow.dataset.id);
            this.allRows[dayOfWeek].splice(key,1);
            this.index[dayOfWeek]--;
            console.log(JSON.parse(JSON.stringify(this.allRows[dayOfWeek])));
            }catch (error) {
                console.log(error);
              }
    } 

    save(){
        try{
        this.passRows = [...this.mondayRows, ...this.tuesdayRows, ...this.wednesdayRows, ...this.thursdayRows, ...this.fridayRows];
        console.log(JSON.parse(JSON.stringify(this.passRows)));
        let sd = this.template.querySelector(".StartDate");
        let start = sd.value;
        let ed = this.template.querySelector(".EndDate");
        let end = ed.value;
        console.log(start);
        console.log(end);
        saveTimeSlots({json: JSON.stringify(this.passRows), recordId:this.recordId, startDate:start, endDate:end})
        }catch(error){
            console.log(error);
        }
    }
}