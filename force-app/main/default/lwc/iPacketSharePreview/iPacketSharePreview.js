import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import getVehicleVIN from '@salesforce/apex/IPacketSharePreviewController.getVehicleVIN';
import getSharedLink from '@salesforce/apex/IPacketSharePreviewController.packetSharePreview';

export default class IPacketSharePreview extends LightningElement {
    @api recordId
    @track link
    errorMessage
    vinCode
    isVehicleExist

    get isVinExist() {
        return !!this.vinCode
    }

    /* get isError() {
        return !!this.errorMessage
    } */
    
    get requestLinkLabel() {
        return !!this.link ? 'Update Link' : 'iPacket Link'
    }
    get isLinkExist() {
        return !!this.link
    }
    

    connectedCallback() {
        getVehicleVIN({recordId: this.recordId})
            .then(result => {
                console.log(result)
                this.vinCode = result.vin
                this.link = result.link
                this.isVehicleExist = !!result.vehicle
            })
        .catch(error => {
            console.log(error)
            this.errorMessage = error.body.message
            // this.showErrorMessage()
        })
    }

    getLink() {
        if(!this.vinCode) {
            this.errorMessage = 'There is neither VIN on related Vehicle'
            this.showErrorMessage()
            return
        } 

        getSharedLink({
            vinCode: this.vinCode,
            recordId: this.recordId
        })
            .then(result => {
                console.log(result)
                this.link = result
            })
        .catch(error => {
            console.log(error)
            this.errorMessage = error.body.message
            this.showErrorMessage()
        })
    }

    copyToClipboard() {
        let linkToCopy = this.template.querySelector('.slds-input')
        linkToCopy.select()
        linkToCopy.setSelectionRange(0,999)
        document.execCommand('copy')
    }

    showErrorMessage() {
        const event = new ShowToastEvent({
            variant: 'error',
            message: this.errorMessage,
        })
        this.dispatchEvent(event)
    }
}