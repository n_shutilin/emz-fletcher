import { LightningElement, api} from 'lwc';

export default class IconFieldTypeComponent extends LightningElement {
    @api iconTag;
}