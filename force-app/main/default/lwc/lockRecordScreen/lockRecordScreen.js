import { LightningElement, wire, api } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { getRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { subscribe, unsubscribe } from 'lightning/empApi';
import USER_NAME_FIELD from '@salesforce/schema/User.Name';
import userId from '@salesforce/user/Id';
import getRecordData from '@salesforce/apex/LockRecordHelper.getRecordData';
import updateRecordData from '@salesforce/apex/LockRecordHelper.updateRecordData';
import recordRequest from '@salesforce/apex/LockRecordHelper.recordRequest';

export default class LockRecordScreen extends LightningElement {

    isFirst = true;
    Id = null;
    objName = null;
    intervalId = null;
    blockUserId = null;
    url = null;
    blockUserName = null;
    showRequestWindow = false;
    showRequestDiaog = false;
    subscription = null;
    requestUserName = null;
    userIdMadeRequest = null;

    @wire(CurrentPageReference)
    async wirePageRef(data) {
        if (this.isFirst) {
            this.isFirst = false;
            if (data) {
                this.Id = data.attributes.recordId;
                this.objName = data.attributes.objectApiName;
                this.url = window.location.href;
                const request = {
                    Id: this.Id,
                    objName: this.objName,
                };
                if (!this.subscription) {
                    subscribe('/event/RequestRecordUnlock__e', -1, this.subscriptionCallback).then(res => { this.subscription = res });
                }
                const { LastModifiedDate, UserWorkingWithRecord__c } = await getRecordData(request);
                const difference = new Date() - new Date(LastModifiedDate);
                if (UserWorkingWithRecord__c === undefined || (UserWorkingWithRecord__c !== userId && difference > 30000)) { // empty field or last record update more than minute ago
                    this.customUpdateRecord();
                    this.intervalId = setInterval(this.customUpdateRecord, 10000);
                } else if (UserWorkingWithRecord__c !== userId) { // somebody block record 
                    this.blockUserId = UserWorkingWithRecord__c;
                } else if (this.intervalId === null && (UserWorkingWithRecord__c === userId)) { // refresh page case
                    this.customUpdateRecord();
                    this.intervalId = setInterval(this.customUpdateRecord, 10000);
                }
            }
        } else if (window.location.href.indexOf(this.Id) > 0) { // back to page by press back button
            if (!this.subscription) {
                subscribe('/event/RequestRecordUnlock__e', -1, this.subscriptionCallback).then(res => { this.subscription = res });
            }
            const { LastModifiedDate, UserWorkingWithRecord__c } = await getRecordData({ Id: this.Id, objName: this.objName });
            const difference = new Date() - new Date(LastModifiedDate);
            if (UserWorkingWithRecord__c === userId || (UserWorkingWithRecord__c !== userId && difference > 30000)) {
                this.customUpdateRecord();
                this.intervalId = setInterval(this.customUpdateRecord, 10000);
            } else if (this.intervalId == null && this.blockUserId !== null && this.blockUserName != null) {
                this.showRequestWindow = true;
                this.showCaution();
            } else {
                this.blockUserId = UserWorkingWithRecord__c;
            }
        }
    }

    @wire(getRecord, { recordId: '$blockUserId', fields: [USER_NAME_FIELD] })
    wiredUser({ data }) {
        if (data) {
            this.blockUserName = data.fields.Name.value;
            this.showRequestWindow = true;
            this.showCaution();
        }
    }

    disconnectedCallback() {
        this.clearData();
        this.Id = null;
        this.objName = null;
        this.url = null;
    }

    @api changeURLHandler() { // works when user leave record page
        if (this.intervalId) {
            this.clearData();
            const recordData = {
                Id: this.Id,
                objName: this.objName,
                data: '',
            };
            updateRecordData(recordData); // clear UserWorkingWithRecord__c field on record 
            const record = {
                recordId: this.Id,
                userIdBlockedRecord: '',
                userIdMadeRequest: '',
                isAnswer: true,
            };
            recordRequest(record);
        }        
    }

    clearData() {
        clearInterval(this.intervalId);
        unsubscribe(this.subscription, response => response).catch(err => console.log(JSON.stringify(err)));
        this.intervalId = null;
        this.blockUserId = null;
        this.blockUserName = null;
        this.showRequestWindow = false;
        this.subscription = null;
        this.requestUserName = null;
        this.userIdMadeRequest = null;
    }

    customUpdateRecord = () => {
        if (this.Id && this.objName) {
            const recordData = {
                Id: this.Id,
                objName: this.objName,
                data: userId,
            };
            updateRecordData(recordData);
        }
    }

    showCaution() {
        const event = new ShowToastEvent({
            title: 'Caution!',
            message: `This record blocked by ${this.blockUserName}. Сhanges will not be saved!`,
            variant: 'warning',
            mode: 'sticky',
        });
        this.dispatchEvent(event);
    }

    showRejectedRequest() {
        const event = new ShowToastEvent({
            title: 'Request was rejected!',
            message: `${this.blockUserName} rejected your request.`,
            variant: 'error',
            mode: 'sticky',
        });
        this.dispatchEvent(event);
    }

    showApprovedRequest() {
        const event = new ShowToastEvent({
            title: 'Request was approved!',
            message: `${this.blockUserName} approved your request.`,
            variant: 'success',
        });
        this.dispatchEvent(event);
    }

    showUnlockRequestMessage() {
        const event = new ShowToastEvent({
            title: 'Request was sent!',
            message: `If you have not received a response within a minute, please reload the page.`,
            variant: 'success',
            mode: 'sticky',
        });
        this.dispatchEvent(event);
    }

    showUnlockMessage() {
        const event = new ShowToastEvent({
            title: 'Record unlocked!',
            variant: 'success',
            mode: 'sticky',
        });
        this.dispatchEvent(event);
    }

    unlockRequest() {
        const recordData = {
            recordId: this.Id,
            userIdBlockedRecord: this.blockUserId,
            userIdMadeRequest: userId,
            isAnswer: false,
        };
        recordRequest(recordData);
        this.showUnlockRequestMessage();
    }

    subscriptionCallback = response => {
        const { NameOfUserMadeRquest__c, RecordId__c, UserIdBlockedRecord__c, UserIdMadeRequest__c, IsAnswer__c } = response.data.payload;
        if (IsAnswer__c && RecordId__c === this.Id) {
            if (userId === UserIdMadeRequest__c && this.blockUserId === UserIdBlockedRecord__c) { // rejected request
                this.showRejectedRequest();
            } else if (userId === UserIdMadeRequest__c && userId === UserIdBlockedRecord__c) { // approved request
                this.showRequestWindow = false;
                this.showApprovedRequest();
                this.customUpdateRecord();
                this.intervalId = setInterval(this.customUpdateRecord, 10000);
            } else if (UserIdBlockedRecord__c === null && UserIdMadeRequest__c === null) {
                const recordData = {
                    Id: this.Id,
                    objName: this.objName,
                    data: userId,
                };
                updateRecordData(recordData)
                    .then(answer => {
                        if (answer) {
                            this.showUnlockMessage();
                            setTimeout(() => window.location.reload(), 3000);
                        }
                    })
            }
        } else if (!IsAnswer__c && RecordId__c === this.Id && UserIdBlockedRecord__c === userId) { // get request
            this.requestUserName = NameOfUserMadeRquest__c;
            this.showRequestDiaog = true;
            this.userIdMadeRequest = UserIdMadeRequest__c;
        }
    }

    rejectRequest() {
        this.showRequestDiaog = false;
        const recordData = {
            recordId: this.Id,
            userIdBlockedRecord: userId,
            userIdMadeRequest: this.userIdMadeRequest,
            isAnswer: true,
        };
        recordRequest(recordData);
    }

    approveRequest() {
        this.showRequestDiaog = false;
        clearInterval(this.intervalId);
        this.intervalId = null;
        const recordData = {
            recordId: this.Id,
            userIdBlockedRecord: this.userIdMadeRequest,
            userIdMadeRequest: this.userIdMadeRequest,
            isAnswer: true,
        };
        recordRequest(recordData);
        this.blockUserId = this.userIdMadeRequest;
    }
}