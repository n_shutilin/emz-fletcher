import { LightningElement, track, wire} from 'lwc';
import getRecords from '@salesforce/apex/GetMessageBoardPosts.getRecords';
import { refreshApex } from '@salesforce/apex';

export default class MessageBoardLWC extends LightningElement {
    @track data = [];
    @track tableLoadingState = true;
    @track editMode = false;
    MessageBoardPosts;
    error;
    
    @wire(getRecords) 
    wiredRecordsMethod(value) {
        this.MessageBoardPosts = value;
        const { data, error } = value;
        if (data) {
            this.data  = data;
            console.log(data);
            this.error = undefined;
        } else if (error) {
            this.error = error;
            console.log(error);
            this.data  = undefined;
        }
        this.tableLoadingState  = false;
    }          

    handlePopup() {
        this.editMode = true;
      }

    handleSuccess(event) {
        this.editMode = false;
        return refreshApex(this.MessageBoardPosts);
    }

    closePopup(){
        this.editMode = false;
    }

    }