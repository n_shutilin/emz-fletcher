import {LightningElement, api, track, wire} from 'lwc';
import Id from '@salesforce/user/Id';
import getPicklistValues from '@salesforce/apex/NewActionFormCtrl.getPicklistValues';

export default class NewActionForm extends LightningElement {
    userId = Id;

    @api recordId;
    @api objectApiName;
    @api isRecordPageModal = false;

    @track action = {
        OwnerId: this.userId
    };
    @track subjectItems;
    @track processNameItems;
    @track isSaveDisabled = true;

    get lookupType() {
        switch (this.objectApiName) {
            case 'Opportunity':
                return 'Opportunity__c'
            case 'Lead':
                return 'Lead__c'
            case 'Account':
                return 'Account__c'
        }
    }

    @wire(getPicklistValues)
    getPicklistValues({ error, data }) {
        if (data) {
            this.subjectItems = data.Name;
            this.processNameItems = data.Process_Name__c;
        } else if (error) {
            console.log('--- error: ' + JSON.stringify(error));
        }
    }

    handleFieldChange(event) {
        let fieldName = event.target.dataset.id;
        this.action[fieldName] = event.detail.value;
        this.checkRequiredFields();
    }

    handleLookupValueChange(event) {
        let fieldName = event.target.dataset.id;

        if (event.detail.length > 0) {
            this.action[fieldName] = event.detail[0].id;
        } else {
            this.action[fieldName] = undefined;
        }

        this.checkRequiredFields();
    }

    checkRequiredFields() {
        this.isSaveDisabled = this.action.Name === undefined || this.action.OwnerId === undefined;
    }

    handleSubmit(event) {
        event.preventDefault();

        const fields = event.detail.fields;
        fields.OwnerId = this.action.OwnerId;
        fields.Name = this.action.Name;
        fields.Process_Name__c = this.action.Process_Name__c;

        this.template.querySelector('lightning-record-edit-form').submit(fields);

        if (this.isRecordPageModal) {
            this.template.querySelector('c-custom-toast').showCustomNotice('Saved', 'success');
            setTimeout(() => this.closeModal(), 1000);
        } else {
            let newRecordId = event.detail.id;
            this.dispatchModalEvent('recordCreated', newRecordId)
        }
    }

    dispatchModalEvent = (type = null, recordId = null) => {
        this.dispatchEvent(
            new CustomEvent('action', {
                detail: {
                    type: type,
                    recordId: recordId
                }
            })
        );
    }

    closeModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }
}