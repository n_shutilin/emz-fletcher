import { LightningElement, api } from 'lwc';

export default class Note extends LightningElement {
    @api note;
    @api removable;

    previewOpen = false;
    previewSize;
    thumbnailSize;

    constructor() {
        super();

        setTimeout(() => {
            const container = this.template.querySelector('.thumbnail-container');

            this.thumbnailSize = {
                height: container.offsetHeight,
                width: container.offsetWidth,
            };
        });
    }

    closePreview() {
        this.previewOpen = false;
        this.previewSize = null;
    }

    getPointerCoordinates(note, thumbnailSize, cover) { // coordinates from center
        const photoCenter = {
            x: Math.round(note.photoWidth / 2),
            y: Math.round(note.photoHeight / 2),
        };

        const photoRatio = note.photoWidth / note.photoHeight;
        const containerRatio = thumbnailSize.width / thumbnailSize.height;

        let photoFitPixels;
        let containerFitSize;

        if (photoRatio > containerRatio) {
            // cover=true (fill) - left and right are hidden
            // cover=false (fit) - free space on top and bottom

            photoFitPixels = cover ? note.photoHeight : note.photoWidth;
            containerFitSize = cover ? thumbnailSize.height : thumbnailSize.width;
        } else {
            // cover=true (fill) - top and bottom are hidden
            // cover=false (fit) - free space on left and right

            photoFitPixels = cover ? note.photoWidth : note.photoHeight;
            containerFitSize = cover ? thumbnailSize.width : thumbnailSize.height;
        }

        const pointerCoordinates = {
            x: Math.round((note.x - photoCenter.x) * (containerFitSize / photoFitPixels)),
            y: Math.round((note.y - photoCenter.y) * (containerFitSize / photoFitPixels)),
        };

        return pointerCoordinates;
    }

    openPreview() {
        this.previewOpen = true;

        setTimeout(() => {
            const container = this.template.querySelector('.photo-container');

            this.previewSize = {
                height: container.offsetHeight,
                width: container.offsetWidth,
            };
        });
    }

    get previewPanelClass() {
        const classes = [
            'panel slds-panel slds-panel_docked slds-is-open',
        ];

        if (this.previewOpen) {
            classes.push('panel--open');
        }

        return classes.join(' ');
    }

    get previewPointerStyle() {
        if (this.previewSize) {
            const coordinates = this.getPointerCoordinates(this.note, {
                height: this.previewSize.height,
                width: this.previewSize.width,
            }, false);

            return `
                margin-left: ${coordinates.x}px;
                margin-top: ${coordinates.y}px;
            `;
        } else {
            return `
                display: none;
            `;
        }
    }

    remove(event) {
        event.stopPropagation();

        this.dispatchEvent(new CustomEvent('remove', {
            detail: this.note.id,
        }));
    }

    get thumbnailPointerStyle() {
        if (this.thumbnailSize) {
            const coordinates = this.getPointerCoordinates(this.note, {
                height: this.thumbnailSize.height,
                width: this.thumbnailSize.width,
            }, false);

            return `
                margin-left: ${coordinates.x}px;
                margin-top: ${coordinates.y}px;
            `;
        } else {
            return `
                display: none;
            `;
        }
    }
}