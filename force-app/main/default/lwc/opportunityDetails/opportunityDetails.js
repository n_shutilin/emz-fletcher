import { LightningElement, api, track } from 'lwc';

import getOriginalOpportunityID from '@salesforce/apex/OpportunityDetailsController.getOriginalOpportunityID';
import getOpportunityFields from '@salesforce/apex/OpportunityDetailsController.getOpportunityFields';

export default class OpportunityDetails extends LightningElement {
    @api recordId;
    
    @track isOriginalId;

    fieldList;
    oppid;
    
    connectedCallback() {
        getOpportunityFields()
            .then(result => {
                if(result){
                    this.fieldList = JSON.parse(result);
                }
            })
        .then(() => getOriginalOpportunityID({oppId : this.recordId})) 
            .then(result => {
                if(result){
                    this.oppid = result;
                    this.isOriginalId = true;
                }
            })
        .catch(error => {
            console.log('ERROR', JSON.stringify(error));
        });
      }
}