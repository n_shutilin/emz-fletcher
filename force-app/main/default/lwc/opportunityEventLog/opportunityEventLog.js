import {LightningElement, api, track} from 'lwc';

import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import getAppointmentTypes from '@salesforce/apex/OpportunityEventLogController.getAppointmentTypes';
import isAvailableDayOfWeekForAppointment
    from '@salesforce/apex/OpportunityEventLogController.isAvailableDayOfWeekForAppointment';
import getAvailableTimePerDayAppointment
    from '@salesforce/apex/OpportunityEventLogController.getAvailableTimePerDayAppointment';
import getSalesManagerNames
    from '@salesforce/apex/OpportunityEventLogController.getSalesManagerNames';
import getSalesPersonNames
    from '@salesforce/apex/OpportunityEventLogController.getSalesPersonNames';
import createEvent
    from '@salesforce/apex/OpportunityEventLogController.createEvent';
import getPersonAccountEmail
    from '@salesforce/apex/OpportunityEventLogController.getPersonAccountEmail';
import getAppointmentInfo
    from '@salesforce/apex/OpportunityEventLogController.getAppointmentInfo';

export default class OpportunityEventLog extends LightningElement {
    @api recordId;
    @api appointmentId;
    @api preFillMode;
    appointmentTypeValues;
    appointmentTypeValuesSelected;
    appointmentDateSelected;
    @track availableTimeSlotsPerDay;
    @track availableTimeSlotsPerDaySelected;
    notesEnteredValue;

    relatedSaleManagerChosen;
    @track relatedSaleManagers;
    relatedSalesPersonChosen;
    @track relatedSalesPersons;

    @track isNOTAvailableDayOfWeekForAppointment = true;
    @track isManagerNOTChosen = true;
    @track isSaveButtonNOTAvailableFlag = true;
    @track doNotSendEmail = false;
    @track toEmail;
    @track ccEmail;

    isSpinner;

    get isSaveDisabled() {
        this.checkIfSaveButtonAvailable();
        return this.isSaveButtonNOTAvailableFlag
    } 

    connectedCallback() {
        console.log('!!!!!!');
        getAppointmentTypes()
            .then(picklistValues => {
                this.appointmentTypeValues = picklistValues;
                this.error = undefined;
            })
            .catch(error => {
                this.error = error;
                this.appointmentTypeValues = undefined;
            });

        getSalesManagerNames({opportunityId: this.recordId})
            .then(picklistValues => {
                this.relatedSaleManagers = picklistValues;
                this.error = undefined;
            })
            .catch(error => {
                this.error = error;
                this.relatedSaleManagers = undefined;
            });

        getPersonAccountEmail({opportunityId: this.recordId})
            .then(result => {
                this.toEmail = result
            })
            .catch(error => {
                this.error = error
            });

        if (this.preFillMode) {
            this.isSpinner = true;
            getAppointmentInfo({appointmentId: this.appointmentId})
                .then(result => {
                    this.appointmentTypeValuesSelected = result.eventType;

                    if (result.eventDate) {
                        this.appointmentDateSelected = result.eventDate;
                        this.handleAvailableDayOfWeekForAppointment();
                        this.availableTimeSlotsPerDaySelected = result.eventStartTime;
                    }

                    if (result.saleManagerId) {
                        this.relatedSaleManagerChosen = result.saleManagerId;
                        this.isManagerNOTChosen = false;
                        this.retrieveSalesPersonNames();
                        this.relatedSalesPersonChosen = result.salesPersonId;
                    }

                    this.notesEnteredValue = result.eventNotes;
                    this.doNotSendEmail = result.doNotSendEmail;
                    this.toEmail = result.toEmail;
                    this.ccEmail = result.ccEmail;

                    this.checkIfSaveButtonAvailable();
                    this.isSpinner = false;
                })
                .catch(error => {
                    this.error = error;
                });
        }
    }

    handleAppointmentTypeSelection(event) {
        this.appointmentTypeValuesSelected = event.detail.value;
        this.checkIfSaveButtonAvailable();
    }

    handleChangeAppointmentDate(event) {
        this.appointmentDateSelected = event.detail.value;
        this.availableTimeSlotsPerDay = undefined;
        this.availableTimeSlotsPerDaySelected = undefined;

        if (this.appointmentDateSelected != null) {
            this.handleAvailableDayOfWeekForAppointment();
        }
    }

    handleChangeAppointmentTime(event) {
        this.availableTimeSlotsPerDaySelected = event.detail.value;
        this.checkIfSaveButtonAvailable();
    }

    handleSaleManager(event) {
        this.relatedSaleManagerChosen = event.detail.value;
        this.isManagerNOTChosen = false;
        this.retrieveSalesPersonNames();
    }

    handleSalesPerson(event) {
        this.relatedSalesPersonChosen = event.detail.value;
        this.checkIfSaveButtonAvailable();
    }

    handleResponseTextAreaChanges(event) {
        this.notesEnteredValue = event.detail.value;
        this.checkIfSaveButtonAvailable();
    }

    handleCheckboxCheck(event) {
        this.doNotSendEmail = event.target.checked;
        this.validateAllEmailInputs();
    }

    handleTOInput(event) {
        this.toEmail = event.detail.value;
    }

    handleCCInput(event) {
        this.ccEmail = event.detail.value;
    }

    handleSaveButtonClick() {
        this.isSpinner = true;
        const eventInfo = {
            appointmentId: this.appointmentId ? this.appointmentId : null,
            opportunityId: this.recordId,
            eventType: this.appointmentTypeValuesSelected,
            eventDate: this.appointmentDateSelected,
            eventStartTime: this.availableTimeSlotsPerDaySelected,
            saleManagerId: this.relatedSaleManagerChosen,
            salesPersonId: this.relatedSalesPersonChosen,
            eventNotes: this.notesEnteredValue,
            doNotSendEmail: this.doNotSendEmail,
            toEmail: this.doNotSendEmail ? '' : this.toEmail,
            ccEmail: this.doNotSendEmail ? '' : this.ccEmail
        };
        createEvent({eventInfo: eventInfo})
            .then(eventCreationResult => {
                this.error = undefined;
                if (eventCreationResult.isEventCreated === true) {
                    this.showToastSuccess(
                        'Event creation',
                        'Event successfully created.'
                    );
                    this.refreshVariablesAfterSaveEvent();
                } else if (eventCreationResult.isEventUpdated === true) {
                    this.showToastSuccess(
                        'Event reschedule',
                        'Event successfully rescheduled.'
                    );
                } else {
                    this.showToast(
                        'Event creation error',
                        eventCreationResult.errorMessage
                    );
                }
                this.isSpinner = false;
            })
            .catch(error => {
                this.error = error;
                this.relatedSalesPersons = undefined;
                this.showToast(
                    'Event creation error',
                    'createEvent: ' + error
                );
                this.isSpinner = false;
            });
    }

    handleAvailableDayOfWeekForAppointment() {
        isAvailableDayOfWeekForAppointment({
            opportunityId: this.recordId,
            selectedDateString: this.appointmentDateSelected
        })
            .then(isAvailableDayOfWeekForAppointment => {
                this.isNOTAvailableDayOfWeekForAppointment = !isAvailableDayOfWeekForAppointment;

                if (isAvailableDayOfWeekForAppointment) {
                    getAvailableTimePerDayAppointment({
                        opportunityId: this.recordId,
                        selectedDateString: this.appointmentDateSelected
                    })
                        .then(availableTimesPerDay => {
                            this.availableTimeSlotsPerDay = availableTimesPerDay;
                            this.error = undefined;
                        })
                        .catch(error => {
                            this.error = error;
                            this.availableTimeSlotsPerDay = undefined;
                            this.showToast(
                                'There was some error',
                                'getAvailableTimePerDayAppointment() error: ' + error + '. Please, contact with admin',
                                'sticky'
                            );
                        });
                } else {
                    this.showToast(
                        'Wrong date.',
                        'Please choose another date.'
                    );
                }
                this.error = undefined;
                this.checkIfSaveButtonAvailable();
            })
            .catch(error => {
                this.error = error;
                this.isNOTAvailableDayOfWeekForAppointment = true;
                this.showToast(
                    'There was some error',
                    'isAvailableDayOfWeekForAppointment() error: ' + error + '. Please, contact with admin',
                );
            });
    }

    retrieveSalesPersonNames() {
        getSalesPersonNames({opportunityId: this.recordId, saleManagersName: this.relatedSaleManagerChosen})
            .then(picklistValues => {
                picklistValues.unshift({label: 'None', value: null})
                this.relatedSalesPersons = picklistValues;
                this.error = undefined;
                this.checkIfSaveButtonAvailable();
            })
            .catch(error => {
                this.error = error;
                this.relatedSalesPersons = undefined;
            });
    }

    checkIfSaveButtonAvailable() {
        let isCCEmailValidOrBlank = this.ccEmail ? this.emailIsValid(this.ccEmail) : true
        let isTOEmailValidOrBlank = this.toEmail ? this.emailIsValid(this.toEmail) : false
        let emailValidity = this.doNotSendEmail || (!this.doNotSendEmail && isCCEmailValidOrBlank && isTOEmailValidOrBlank)

        this.isSaveButtonNOTAvailableFlag =
            this.appointmentTypeValuesSelected === undefined ||
            this.appointmentDateSelected === undefined ||
            this.appointmentDateSelected === null ||
            this.availableTimeSlotsPerDaySelected === undefined ||
            this.relatedSaleManagerChosen === undefined ||
            // this.relatedSalesPersonChosen === undefined ||
            !emailValidity
    }

    refreshVariablesAfterSaveEvent(){
        this.appointmentTypeValuesSelected = undefined;
        this.appointmentDateSelected = undefined;
        this.availableTimeSlotsPerDay = undefined;
        this.availableTimeSlotsPerDaySelected = undefined;
        this.relatedSalesPersons = undefined;
        this.relatedSaleManagerChosen = undefined;
        this.relatedSalesPersons = undefined;
        this.relatedSalesPersonChosen = null;
        this.notesEnteredValue = undefined;

        this.isNOTAvailableDayOfWeekForAppointment = true;
        this.isManagerNOTChosen = true;
        this.isSaveButtonNOTAvailableFlag = true;
    }

    showToastSuccess(title, message) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: 'success',
        });
        this.dispatchEvent(event);
    }

    showToast(title, message) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            mode: 'sticky',
        });
        this.dispatchEvent(event);
    }

    emailIsValid (email) {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
    }

    validateAllEmailInputs() {
        let emailInputs = this.template.querySelectorAll('.email-input');

        if (this.doNotSendEmail) {
            for (let input of emailInputs) {
                input.classList.remove('slds-has-error');
            }
        } else {
            for (let input of emailInputs) {
                this.validateEmailInput(input);
            }
        }
    }

    handleEmailInputBlur(event) {
        this.validateEmailInput(event.target);
    }

    validateEmailInput(input) {
        let isValid = this.emailIsValid(input.value);

        if (isValid) {
            input.classList.remove('slds-has-error');
        } else {
            input.classList.add('slds-has-error');
        }
    }
}