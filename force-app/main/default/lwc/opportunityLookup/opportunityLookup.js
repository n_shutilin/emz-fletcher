import { LightningElement, track, api } from 'lwc';
import search from '@salesforce/apex/OpportunityLookupController.search';

const SEARCH_DELAY = 300;
const MIN_SEARCH_TERM_LENGTH = 3;

export default class OpportunityLookup extends LightningElement {

    @api dealer;

    @track searchResults = [];
    @track hasFocus = false;
    @track searchTerm;
    
    blurTimeout;
    cleanSearchTerm;
    searchThrottlingTimeout;

    get getListboxClass() {
        return 'slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid';
    }

    get getDropdownClass() {
        let css =
            'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click ';
        if (this.hasFocus && this.hasResults()) {
            css += 'slds-is-open';
        } else {
            css += 'slds-combobox-lookup';
        }
        return css;
    }

    get getContainerClass() {
        let css = 'slds-combobox_container ';
        if (this.hasFocus && this.hasResults()) {
            css += 'slds-has-input-focus ';
        }
        return css;
    }
    
    get isExpanded() {
        return this.hasResults();
    }

    connectedCallback(){

    }

    hasResults() {
        return this.searchResults.length > 0;
    }

    handleBlur() {
        this.blurTimeout = window.setTimeout(() => {
            this.hasFocus = false;
            this.blurTimeout = null;
        }, 300);
    }

    handleFocus(event) {
        this.hasFocus = true;
        this.handleInput(event);
    }

    handleInput(event) {
        this.updateSearchTerm(event.target.value);
    }

    updateSearchTerm(newSearchTerm) {
        this.searchTerm = newSearchTerm;
        const newCleanSearchTerm = newSearchTerm
            .trim()
            .replace(/\*/g, '')
            .toLowerCase();
        if (this.cleanSearchTerm === newCleanSearchTerm) {
            return;
        }
        this.cleanSearchTerm = newCleanSearchTerm;
        if (newCleanSearchTerm.length < MIN_SEARCH_TERM_LENGTH) {
            this.searchResults = [];
            return;
        }
        if (this.searchThrottlingTimeout) {
            clearTimeout(this.searchThrottlingTimeout);
        }
        this.searchThrottlingTimeout = setTimeout(() => {
            if (this.cleanSearchTerm.length >= MIN_SEARCH_TERM_LENGTH) {
                search({
                    searchString: this.cleanSearchTerm,
                    dealer: this.dealer
                }).then(result => {
                    this.searchResults = result;
                });
            }
            this.searchThrottlingTimeout = null;
        }, SEARCH_DELAY);
    }

    handleResultClick(event){
        const recordId = event.currentTarget.dataset.recordid;
        let selectedRecord = this.searchResults.find(record => record.id === recordId);
        if(selectedRecord){
            this.dispatchSelect(selectedRecord);
        }
    }

    handleComboboxClick() {
        if (this.blurTimeout) {
            window.clearTimeout(this.blurTimeout);
        }
        this.hasFocus = false;
    }    

    dispatchSelect(record){
        this.dispatchEvent(
            new CustomEvent("select", {
                detail: {
                    record: JSON.stringify(record)
                }
            })
        );
    }

}