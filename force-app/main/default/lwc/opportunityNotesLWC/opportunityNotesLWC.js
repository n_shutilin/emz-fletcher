import { LightningElement, track, wire, api } from 'lwc';
import getRecords from '@salesforce/apex/GetUsedVehcileManagerNotes.getRecords';
import { getRecord,  getFieldValue  } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import NAME_FIELD from '@salesforce/schema/Opportunity.Name';


const FIELDS = ['Opportunity.Name'];

export default class UsedVehcileManagerNotes extends LightningElement {
    @api recordId;
    @track data = [];
    @track tableLoadingState = true;
    @track editMode = false;
    FjNotes;
    Opportunity;
    Name;
    error;
    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord({ error, data }) {
        if (data) {
            this.Opportunity = data;
            this.Name  = getFieldValue(this.Opportunity,NAME_FIELD);
            this.error = undefined;
        } else if (error) {
            this.error = error;
            console.log('Unable to get Opportunity data');
            this.data  = undefined;
        }
    }
    @wire(getRecords, {recordId: '$recordId'}) 
    wiredRecordsMethod(value) {
        this.FjNotes = value;
        const { data, error } = value;
        if (data) {
            this.data  = data;
            console.log(data);
            this.error = undefined;
        } else if (error) {
            this.error = error;
            console.log('Unable to get FJ Note Data');
            this.data  = undefined;
        }
        this.tableLoadingState  = false;
    }          

    onSubmitHandler(event) {
        event.preventDefault();
        // Get data from submitted form
        const fields = event.detail.fields;
        // Here you can execute any logic before submit
        // and set or modify existing fields
        fields.Related_Record_Id__c = this.recordId;
        fields.Name = this.Name;
        // You need to submit the form after modifications
        this.template
            .querySelector('lightning-record-edit-form').submit(fields);
    }


    handlePopup() {
        this.editMode = true;
      }

      handleSuccess(event) {
        this.editMode = false;
        return refreshApex(this.FjNotes);
    }

    closePopup(){
        this.editMode = false;
    }

    }