import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import saveAnnotation from '@salesforce/apex/SaveAttachmentController.saveAnnotation';
import saveSignature from '@salesforce/apex/SaveAttachmentController.saveSignature';

export default class PhotoAnnotation extends NavigationMixin(LightningElement) {
    approved;
    base64;
    customNote;
    loading = false;
    nextId = 0;
    noteOptions = [
        'Broken',
        'Crack',
        'Bent',
        'Missing',
        'Hole',
        'Dent',
        'Scratch',
        'Chip',
    ];
    notes;
    pointer;
    pointerPixels;
    @api recordId;
    signature;
    signatureOpen = false;
    text;

    constructor() {
        super();
        this.resetNotes();
    }

    addCustomNote() {
        this.addNote('Other', this.text);
    }

    addDefaultNote(event) {
        const index = event.target.dataset.index;
        const note = this.noteOptions[index];

        this.addNote(note);
    }

    handleUploadFinished(event) {

    }

    addNote(type, text = null) {
        const note = {
            id: this.nextId++,
            note: type,
            photo: this.base64,
            photoHeight: this.pointerPixels.height,
            photoWidth: this.pointerPixels.width,
            x: this.pointerPixels.x,
            y: this.pointerPixels.y,
        };

        if (text) {
            note.text = text;
        }

        this.notes.unshift(note);
        this.cancelAdding();
    }

    get addNoteDisabled() {
        return !this.text.trim();
    }

    get addNotePointerStyle() {
        if (this.pointer) {
            return `
                left: ${this.pointer.x}%;
                top: ${this.pointer.y}%;
            `;
        }
    }

    get backdropVisible() {
        return (
            this.pointer ||
            this.loading
        );
    }

    get bottomLineClass() {
        const classes = [
            `bottom-line slds-m-bottom_small`,
        ];

        if (!this.notes.length) {
            classes.push('slds-p-top_medium');
        }

        return classes.join(' ');
    }

    cancelAdding() {
        this.resetPointer();
        this.base64 = null;
    }

    closeSignature() {
        this.signatureOpen = false;
        this.resetSignature();
    }

    doCustomNote() {
        this.customNote = true;
    }

    handleApprove(event) {
        const checkbox = event.target;
        this.approved = checkbox.value;
    }

    handleCustomNoteChange(event) {
        this.text = event.target.value;
    }

    async handleFile(event) {
        this.base64 = await this.readFile(event.target.files[0]);
    }

    handleRemove(event) {
        const noteId = event.detail;

        this.notes = this.notes.filter((note) => {
            return note.id !== noteId;
        });
    };

    handleSignature(event) {
        this.signature = event.detail;
    }

    get noteListClass() {
        const classes = [
            `notes`,
        ];

        if (!this.notes.length) {
            classes.push('notes--empty');
        }

        return classes.join(' ');
    }

    get notePanelClass() {
        const classes = [
            'panel slds-panel slds-panel_docked slds-is-open',
        ];

        if (this.pointer) {
            classes.push('panel--open');
        }

        return classes.join(' ');
    }

    readFile(file) {
        return new Promise((resolve, reject) => {
            try {
                const reader = new FileReader();

                reader.onload = function(e) {
                    resolve(e.target.result);
                }

                reader.readAsDataURL(file);
            } catch (e) {
                reject(e);
            }
        });
    }

    get readyToSubmit() {
        return this.notes && this.notes.length;
    }

    redirectToParent() {
        try {
        this[NavigationMixin.Navigate]({
            attributes: {
                actionName: 'view',
                objectApiName: 'ServiceAppointment',
                recordId: this.recordId,
            },
            type: 'standard__recordPage',
        });
        } catch (e) { console.log(e) }
    }

    resetNotes() {
        this.resetPointer();
        this.resetSignature();

        this.base64 = null;
        this.notes = [];
    }

    resetPointer() {
        this.pointer = null;
        this.pointerPixels = null;
        this.customNote = false;
        this.text = '';
    }

    resetSignature() {
        this.approved = false;
        this.signature = null;

        const component = this.template.querySelector('c-signature-canvas');

        if (component) {
            component.reset();
        }
    }

    selectPhoto() {
        const input = this.template.querySelector('.file-input');
        input.click();
    }

    setPointer(event) {
        const container = event.target.closest('.photo-container');
        const photo = container.querySelector('.photo');

        const photoRect = container.getBoundingClientRect();
        const x = (event.clientX - photoRect.left);
        const y = (event.clientY - photoRect.top);

        this.pointerPixels = {
            height: photo.naturalHeight,
            width: photo.naturalWidth,
            x: Math.round((x / photoRect.width) * photo.naturalWidth),
            y: Math.round((y / photoRect.height) * photo.naturalHeight),
        };

        this.pointer = {
            x: Number((x / photoRect.width * 100).toFixed(2)),
            y: Number((y / photoRect.height * 100).toFixed(2)),
        };
    }

    showToast(type, title, message) {
        this.dispatchEvent(new ShowToastEvent({
            message,
            title,
            variant: type,
        }));
    }

    get signatureClearDisabled() {
        return !this.signature;
    }

    get signaturePanelClass() {
        const classes = [
            'panel slds-panel slds-panel_docked slds-is-open',
        ];

        if (this.signatureOpen) {
            classes.push('panel--open');
        }

        return classes.join(' ');
    }

    get signatureSubmitDisabled() {
        return !this.approved || !this.signature;
    }

    async submit() {
        const promises = this.notes.map((note) => {
            const promise = saveAnnotation({
                appointmentId: this.recordId,
                notes: [note],
            }).catch((error) => {
                const message = error.body.message;
                this.showToast('error', 'Error', message);

                return Promise.reject(message);
            });

            return promise;
        });

        // promises.push(saveSignature({
        //     appointmentId: this.recordId,
        //     imageBody: this.signature,
        // }).catch((error) => {
        //     const message = error.body.message;
        //     this.showToast('error', 'Error', message);
        //
        //     return Promise.reject(message);
        // }));

        this.loading = true;

        await Promise.all(promises).then(() => {
            this.showToast('success', 'Saved');
            this.resetNotes();
            this.loading = false;

            this.redirectToParent();
        }).catch(() => {
            this.loading = false;
        });
    }

    submitNotes() {
        this.signatureOpen = true;
    }

    undoCustomNote() {
        this.text = '';
        this.customNote = false;
    }
}