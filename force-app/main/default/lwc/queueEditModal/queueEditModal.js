import { LightningElement, api, track } from "lwc";
import { formatAMPM, formatDate, formGMT, getGMTTime, formDateFromTime, formGMTDate } from "c/utils";

const STANDARD_WORKING_HOURS = 9;

const STANDARD_WORK_DAY = STANDARD_WORKING_HOURS * 60;

export default class QueueEditModal extends LightningElement {
  @api dealer;
  @api isSales;
  @api timeSlot;
  queueName;
  formattedStartTime;
  formattedEndTime;
  errorMessage = "";
  upTime = 180;
  endTime;
  accordianSection = '';
  revert = 3;
  upNext = 3;
  delayedStart = false;

  get startNow() {
    return !this.delayedStart;
  }

  currentTime = formatDate(new Date());
  estimatedEndTime = formatDate(this.calculateEstimatedTime());

  handleToggleSection(event) {
    if (this.accordianSection.length === 0) {
      this.accordianSection = ''
    }
    else {
      this.accordianSection = 'Advanced'
    }
  }

  handleDelayedStart(event) {
    this.delayedStart = event.target.checked;
  }

  connectedCallback() {
    this.errorMessage = "";
    this.startTime = this.currentTime;
    if (this.timeSlot) {
      let time = this.timeSlot.EndTime.replace("Z", "");
      let date = formDateFromTime(time);
      this.endTime = formatDate(date);
      this.estimatedEndTime = formatDate(date);
    } else {
      this.endTime = this.estimatedEndTime;
    }
    this.queueName = this.isSales ? "Sales Territory" : "Service Territory";
  }

  calculateEstimatedTime() {
    let endTime = new Date();
    let day = endTime.getDate();
    endTime.setMinutes(endTime.getMinutes() + STANDARD_WORK_DAY);
    let endDay = endTime.getDate();
    if (day !== endDay) {
      endTime.setMinutes(endTime.getMinutes() - STANDARD_WORK_DAY);
      endTime.setHours(23, 59, 59, 999);
    }
    return endTime;
  }

  closeModal() {
    this.fireEvent("close", 0, 0);
  }

  submit() {
    this.errorMessage = "";
    this.clearValidity(["queueName", "startTime", "endTime", "upNext", "revert", "upTime"]);
    let errors = this.validateTime();
    if (errors.length > 0) {
      this.displayErrorMessage(errors);
    } else {
      let gmtStartTime = formGMT(this.startTime);
      let gmtEndTime = formGMT(this.endTime);
      this.fireEvent("submit", gmtStartTime, gmtEndTime);
    }
  }

  clearValidity(values) {
    values.forEach(value => {
      var el = this.template.querySelector("[data-id=" + value + "]");
      if (el) {
        el.setCustomValidity("");
        el.reportValidity();
      }
    });
  }

  displayErrorMessage(errors) {
    errors.forEach((error) => {
      var el = this.template.querySelector("[data-id=" + error.field + "]");
      el.setCustomValidity(error.message);
      el.reportValidity();
    });
  }

  validateTime() {
    let errors = [];
    let startDate = formGMTDate(this.startTime);
    let endDate = formGMTDate(this.endTime);
    if (this.queueName === "") {
      errors.push({
        message: "Enter queue name.",
        field: "queueName"
      });
    }
    if (startDate === endDate) {
      errors.push({
        message: "Start time cannot be the same as end time.",
        field: "startTime"
      });
    }
    if (startDate > endDate) {
      errors.push({
        message: "Start time must be less than end time.",
        field: "endTime"
      });
    }
    if (this.upNext < 0) {
      errors.push({
        message: "The value cannot be negative.",
        field: "upNext"
      });
    }
    if (this.revert < 0) {
      errors.push({
        message: "The value cannot be negative.",
        field: "revert"
      });
    }
    if (this.upTime < 0) {
      errors.push({
        message: "The value cannot be negativee.",
        field: "upTime"
      });
    }
    return errors;
  }

  enterName(event) {
    let temp = event.target.value;
    if (temp.length >= 30) {
      event.target.value = this.queueName;
    } else {
      this.queueName = temp;
    }
  }

  handleStartTime(event) {
    this.errorMessage = "";
    let time = event.target.value;
    this.startTime = time;
  }
  handleEndTime(event) {
    this.errorMessage = "";
    let time = event.target.value;
    this.endTime = time;
  }

  handleUpTime(event) {
    let temp = event.target.value;
    if (!isNaN(temp) && temp.length <= 3 && this.isInteger(temp)) {
      this.upTime = temp;
    } else {
      event.target.value = this.upTime;
    }
  }

  handleRevert(event) {
    let temp = event.target.value;
    if (!isNaN(temp) && temp.length <= 3 && this.isInteger(temp)) {
      this.revert = temp;
    } else {
      event.target.value = this.revert;
    }
  }

  handleUpNext(event) {
    let temp = event.target.value;
    if (!isNaN(temp) && temp.length <= 3 && this.isInteger(temp)) {
      this.upNext = temp;
    } else {
      event.target.value = this.upNext;
    }
  }

  isInteger(value) {
    return Math.round(value) - value === 0 && !value.includes(".");
  }

  fireEvent(value, gmtStartTime, gmtEndTime) {
    this.dispatchEvent(
      new CustomEvent("modal", {
        detail: {
          type: value,
          name: this.queueName,
          dealer: this.dealer,
          startTime: gmtStartTime,
          endTime: gmtEndTime,
          upTime: this.upTime,
          revert: this.revert,
          upNext: this.upNext,
        }
      })
    );
  }
}