import { LightningElement, api, track } from 'lwc';

export default class QueueLeadModal extends LightningElement {
    @api dealer;
    @api isManager;
    @api currentResource;
    @api isSales;

    @track selectedOpportunity;
    @track selectedAccount;

    activiyInfoAvailable = true;
    newLeadForm = true;
    opportunityForm = false;
    currentReason;

    get options() {
        return [
            { label: 'Re-delivery', value: 'Re-delivery' },
            { label: 'Another sales associate', value: 'Another sales associate' },
            { label: 'Finance', value: 'Finance' },
            { label: 'Car wash', value: 'Car wash' },
        ];
    }

    get isOpportunityNotSelected() {
        return !this.selectedOpportunity;
    }

    get isAccountNotSelected() {
        return !this.selectedAccount;
    }

    get isNotReasonSelected() {
        return !this.currentReason;
    }

    connectedCallback(){
    }

    handleNoActivity(event) {
        this.activiyInfoAvailable = !this.activiyInfoAvailable;
    }

    handleReasonChange(event) {
        this.currentReason = event.detail.value;
    }

    handleFormSwitch(event){
        this.newLeadForm = !this.newLeadForm;
        this.opportunityForm = !this.opportunityForm;
    }

    closeModalWithNoLead(event){
        this.closeModal("noLead", null, this.currentReason);
    }

    closeModalWithLead(event){
        let newLeadId = event.detail.id;
        this.closeModal('newLead', newLeadId);
    }

    handleOpporunitySelect(event){
        this.selectedOpportunity = JSON.parse(event.detail.record);
    }

    handleAccountSelect(event){
        this.selectedAccount = JSON.parse(event.detail.record);
    }

    closeModalWithOpportunity(event){
        this.closeModal('opportunity', this.selectedOpportunity.id);
    }

    closeModalWithAccount(event){
        this.closeModal('account', this.selectedAccount.id);
    }

    closeModal(type="close", recordId = null, reason) {
        this.dispatchEvent(
            new CustomEvent("modal", {
                detail: {
                    type: type,
                    record: recordId,
                    reason: reason
                }
            })
        );
    }
}