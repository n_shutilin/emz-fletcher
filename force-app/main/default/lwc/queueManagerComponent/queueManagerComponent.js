import { LightningElement, track, api, wire } from "lwc";
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';
import id from "@salesforce/user/Id";
import getResourcesByUser from "@salesforce/apex/QueueManagerController.getResourcedByUserId";
import getUserInfo from "@salesforce/apex/QueueManagerController.getUserInfoById";
import isSalesTerrytory from "@salesforce/apex/QueueManagerController.isUserInSalesTerrytory";
import findeTimeSlot from "@salesforce/apex/QueueManagerController.getTimeSlotByUserId";
import { defineDayOfWeek } from "c/utils";
import { SALE_MANAGER_VALUE } from 'c/constants';

export default class QueueManagerComponent extends LightningElement {

  @api channelName = '/event/Up_System_Platform_Event__e';
  
  @track isLoaded = false;
  @track rows;
  @track isReady = false;
  @track isEmpty = false;
  @track isSales;
  @track timeSlot = this.getTimeSlot();
  @track userInfo;
  
  displayType;
  subscription = {};
  userId = id;
  sortBy;
  sortType;

  get role() {
    return this.userInfo ? this.userInfo.userRole : null;
  }

  get resourceId() {
    return this.userInfo.resourceId;
  }

  connectedCallback() {
    getUserInfo({ userId: this.userId }).then(result => {
      this.userInfo = result;
      this.displayType = "Daily"
      this.sortBy = "upNext";
      this.sortType = "DESC"
      this.isReady = true;
      isSalesTerrytory({ userId: this.userId }).then(result => {
        this.isSales = result;
      });
      this.fetchData();
      this.subscribe();
    });
  }

  getTimeSlot() {
    findeTimeSlot({ userId: id, dayOfWeek: defineDayOfWeek() }).then((result) => {
      this.timeSlot = JSON.parse(result);
    });
  }

  handleToQueue(event) {
    if(this.isLoaded){
      this.isLoaded = false;
      this.template.querySelector("c-queues-container-component").handleToQueueFromPoolRecord(event.detail.row);
    }
  }

  handleDisplayType(event) {
    let type = event.detail.type;
    if (this.displayType === type) {
      this.displayType = (this.displayType === "Daily") ? "Monthly" : "Daily";
    } else {
      this.displayType = type;
    }
    this.fetchData();
  }

  fetchData() {
    this.isLoaded = false;
    getResourcesByUser({
      userId: this.userId,
      displayType: this.displayType
    }).then((result) => {
      let tempRows = [];
      if (result) {
        let rows = JSON.parse(result);
        rows.forEach((row) => {
          tempRows.push(this.formRow(row));
        });
        if (tempRows.length > 0) {
          this.rows = tempRows;
          this.rows.sort(this.getRowsComparator(this.sortBy, this.sortType));
          this.isLoaded = true;
          this.isEmpty = false;
        } else {
          this.isLoaded = true;
          this.isEmpty = true;
        }
      }
    });
  }

  subscribe() {
    const messageCallback = (response) => {
      let event = JSON.parse(JSON.stringify(response));
      let type = event.data.payload.Type__c;
      if (type === "Queue closed.") {
        this.fetchData();
      }
      if (type === "Dropped.") {
        this.fetchData();
      }
    }
    subscribe(this.channelName, -1, messageCallback).then(response => {
      this.subscription = response;
    });
  }

  handleRowRejected(event){
    this.isLoaded = true;
    this.isEmpty = !(this.rows.length > 0);
    console.log('REJECTED')
  }

  hadnleResourceAdd(event) {
    let id = event.detail.resource;
    if(id){
      this.disableRowById(id);
      this.rows.sort(this.getRowsComparator(this.sortBy, this.sortType));
    }
    this.isLoaded = true;
  }

  handleRemove(event) {
    let id = event.detail.resource;
    this.enableRowById(id);
    this.rows.sort(this.getRowsComparator(this.sortBy, this.sortType));
  }

  handleSort(event) {
    let sortBy = event.detail.type;
    if (this.sortBy === sortBy) {
      this.sortType = (this.sortType === "ASC") ? "DESC" : "ASC";
    } else {
      this.sortType = "DESC";
      this.sortBy = sortBy;
    }
    this.rows.sort(this.getRowsComparator(this.sortBy, this.sortType));
  }

  getRowsComparator(sortBy, sortType) {
    return function (prevRow, nextRow) {
      if (prevRow.notInUse && !nextRow.notInUse) {
        return -1;
      }
      if (!prevRow.notInUse && nextRow.notInUse) {
        return 1;
      }
      return sortType === "ASC" ? prevRow[sortBy] - nextRow[sortBy] : nextRow[sortBy] - prevRow[sortBy];
    };
  }

  handleQueueClose() {
  }

  disableRowById(id) {
    this.rows.forEach((row) => {
      if (row.id === id) {
        row.notInUse = true; //temp change need rename property
        row.rowClass = "slds-hint-parent disabled";
      }
    });
  }

  enableRowById(id) {
    this.rows.forEach((row) => {
      if (row.id === id) {
        row.notInUse = false; //temp change need rename property
        row.rowClass = "slds-hint-parent";
      }
    });
  }

  formRow(tempRow) {
    return {
      "id": tempRow.id,
      "dealer": tempRow.dealer,
      "upNext": tempRow.totalUpNextTime,
      "inQueue": tempRow.totalInQueueTime,
      "withGuestTime": tempRow.totalWithGuestTime,
      "onBreakTime": tempRow.totalOnBreakTime,
      "position": 0,
      "name": tempRow.name,
      "pictureUrl": tempRow.pictureUrl,
      "status": " ",
      "upTime": 0,
      "startTime": " ",
      "endTime": " ",
      "elapsedTime": 0,
      "totalTime": tempRow.totalUpNextTime + tempRow.totalInQueueTime + tempRow.totalWithGuestTime + tempRow.totalOnBreakTime,
      "onBreak": false,
      "withGuest": false,
      "notInUse": this.isNavigationDisabled(tempRow), 
      "rowClass": tempRow.notInUse ? "slds-hint-parent" : "slds-hint-parent disabled",
      "actions": []
    };
  }

  isNavigationDisabled = (row) => {
    if(this.resourceId === row.id && row.notInUse) {
      return false;
    }
    if(this.role === SALE_MANAGER_VALUE && row.notInUse) {
      return false;
    }
    return true;
  }
}