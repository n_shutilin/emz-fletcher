import { LightningElement, api } from 'lwc';

export default class QueueSettingsModal extends LightningElement {

    @api queue;

    queueData;
    rowsData = [];
    errorMessage = "";

    get isEmptyRows() {
        return !(this.queue.rows.length > 0);
    }

    connectedCallback() {
        this.formRowsData();
        this.formQueueData();
    }

    handleNameChange(event) {
        let temp = event.target.value;
        if (temp.length >= 30) {
          event.target.value = this.queueData.name;
        } else {
            this.queueData.name = temp;
        }
    }

    handleUpTimeChange(event) {
        let temp = event.target.value;
        if (!isNaN(temp) && temp.length <= 3 && this.isInteger(temp)) {
            this.queueData.upTime = temp;
        } else {
            event.target.value = this.queueData.upTime;
        }
    }

    handleRevert(event) {
        let temp = event.target.value;
        if (!isNaN(temp) && temp.length <= 3 && this.isInteger(temp)) {
            this.queueData.revertTime = temp;
        } else {
            event.target.value = this.queueData.revertTime;
        }
    }

    handleUpNext(event) {
        let temp = event.target.value;
        if (!isNaN(temp) && temp.length <= 3 && this.isInteger(temp)) {
            this.queueData.upNextTime = temp;
        } else {
            event.target.value = this.queueData.upNextTime;
        }
    }

    handleRowUpdate(event) {
        let rowId = event.currentTarget.dataset.id;
        let temp = event.target.value;
        this.rowsData.forEach(row => {
            if (row.id === rowId) {
                if (!isNaN(temp) && temp.length <= 3 && this.isInteger(temp)) {
                    row.upTime = temp;
                } else {
                    event.target.value = row.upTime;
                }
            }
        });
    }

    formQueueData() {
        this.queueData = {
            id: this.queue.id,
            name: this.queue.name,
            upTime: this.queue.upTime,
            revertTime: this.queue.revertTime,
            upNextTime: this.queue.upNextTime
        };
    }

    formRowsData() {
        this.queue.rows.forEach(row => {
            this.rowsData.push({
                id: row.id,
                name: row.name,
                upTime: row.inQueueUpTime
            });
        });
    }

    closeModal() {
        this.fireEvent("close");
    }

    save() {
        this.clearValidity();
        let errors = this.validateTime();
        if (errors.length > 0) {
            this.displayErrorMessage(errors);
        } else {
            this.fireEvent("save");
        }
    }

    clearValidity() {
        let values = ["queueName", "upTime", "revertTime", "upNextTime"];
        this.rowsData.forEach(data => {
            values.push(JSON.stringify(data.id));
        });
        values.forEach(value => {
            var el = this.template.querySelector("[data-id=" + value + "]");
            if (el) {
                el.setCustomValidity("");
                el.reportValidity();
            }
        });
    }

    displayErrorMessage(errors) {
        errors.forEach((error) => {
            var el = this.template.querySelector("[data-id=" + error.field + "]");
            el.setCustomValidity(error.message);
            el.reportValidity();
        });
    }

    validateTime() {
        let errors = [];
        if (this.queueData.name === "") {
            errors.push({
                message: "Enter queue name.",
                field: "queueName"
            });
        }
        if (this.queueData.upTime < 0) {
            errors.push({
                message: "The value cannot be negative.",
                field: "upTime"
            });
        }
        if (this.queueData.revertTime < 0) {
            errors.push({
                message: "The value cannot be negative.",
                field: "revertTime"
            });
        }
        if (this.queueData.upNextTime < 0) {
            errors.push({
                message: "The value cannot be negative.",
                field: "upNextTime"
            });
        }
        this.rowsData.forEach(data => {
            if (data.upTime < 0) {
                errors.push({
                    message: "The value cannot be negative.",
                    field: JSON.stringify(data.id)
                });
            }
        });
        return errors;
    }

    isInteger(value) {
        return Math.round(value) - value === 0 && !value.includes(".");
    }

    fireEvent(value) {
        this.dispatchEvent(
            new CustomEvent("modal", {
                detail: {
                    type: value,
                    rows: this.rowsData,
                    queue: this.queueData
                }
            })
        );
    }
}