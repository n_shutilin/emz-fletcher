import { LightningElement, api, track } from "lwc";
import deleteQueueById from '@salesforce/apex/QueueManagerController.deleteQueueWithData';

const IN_PAIRING_ACTION = ['up','toEnd', 'down'];

export default class SalesManagerComponent extends LightningElement {

  @api isManager;
  @track _queue;

  isModalOpen = false;

  @api set queue(value) {
    this._queue = value;
  }

  get queue(){
    return this._queue;
  }

  get numberOfResources() {
    let inQueueNumber = 0;
    let onBreakNumber = 0;
    let withGuestNumber = 0;
    this._queue.rows.forEach(row => {
      if (row.withGuest) {
        withGuestNumber++;
      } else if (row.onBreak) {
        onBreakNumber++;
      } else {
        inQueueNumber++;
      }
    });
    return (inQueueNumber + onBreakNumber + withGuestNumber) === 0 ? "Queue is empty" :
      "In queue: " + inQueueNumber + "; On break: " + onBreakNumber + "; With guest: " + withGuestNumber + ";";
  }

  get isEmpty(){
    return this._queue.rows.length === 0;
  }

  get isNotManager(){
    return !this.isManager;
  }

  get rows() {
    let tempRows = this._queue.rows.map(item => ({... item, 
      displayUpTime : item.position === 1
      }));
    tempRows.forEach((item) => {
      item.upTime = (item.inQueueUpTime - item.upTime) + 1
      let tempActions = [];
      item.actions.forEach((action) => {
        let disabled = action.disabled;
        if(item.inPairing && !IN_PAIRING_ACTION.includes(action.type)){
          disabled = true;
        }
        tempActions.push({
          disabled: disabled,
          title: action.title,
          type: action.type,
        });
      });
      item.actions = tempActions
    });
    return tempRows;
  }

  connectedCallback(){
  }

  renderedCallback(){
  }
  
  //deprecated
  allowDrop(event) {
    event.preventDefault();
  }

  //deprecated
  drop(event) {
    event.preventDefault();
    let row = event.dataTransfer.getData("row");
    this.dispatchEvent(
      new CustomEvent("drop", {
        detail: {
          resource: JSON.parse(row),
          queueId: this._queue.id
        }
      })
    );
  }

  deleteQueue() {
    deleteQueueById({ queueId: this._queue.id });
    this.dispatchQueueDelete();
  }

  dispatchQueueDelete() {
    let rowsId = [];
    this._queue.rows.forEach(row => {
      rowsId.push(row.id);
    });
    this.dispatchEvent(
      new CustomEvent("delete", {
        detail: {
          queueId: this._queue.id,
          rows: rowsId
        }
      })
    );
  }

  closeQueue() {
    this.dispatchEvent(
      new CustomEvent("close", {
        detail: {
          queueId: this._queue.id,
        }
      })
    );
  }

  handleAction(event) {
    let type = event.currentTarget.dataset.type;
    let rowid = event.currentTarget.dataset.rowid;
    let rowposition = event.currentTarget.dataset.rowposition;
    let queueId = this._queue.id;
    this.dispatchEvent(
      new CustomEvent("action", {
        detail: {
          queueId: queueId,
          type: type,
          id: rowid,
          rowposition: rowposition
        }
      })
    );
  }

  handleUnPair(event) {
    let type = event.currentTarget.dataset.type;
    let rowId = event.currentTarget.dataset.rowid;
    let pairId = event.currentTarget.dataset.pairid;
    let queueId = this._queue.id;
    this.dispatchEvent(
      new CustomEvent("action", {
        detail: {
          queueId: queueId,
          type: type,
          id: rowId,
          pairId: pairId
        }
      })
    );
  }

  handleEditModal() {
    this.isModalOpen = true;
  }

  handleModal(event) {
    let type = event.detail.type;
    let queueData = event.detail.queue;
    let rowsData = event.detail.rows;
    switch (type) {
      case "close":
        this.isModalOpen = false;
        break;
      case "save":
        this.dispatchQeueuDataOpdate(queueData, rowsData);
        this.isModalOpen = false;
        break;
    }
  }

  dispatchQeueuDataOpdate(queueData, rowsData) {
    this.dispatchEvent(
      new CustomEvent("update", {
        detail: {
          queueToUpdate: queueData,
          rowsToUpdate: rowsData,
        }
      })
    );
  }
}