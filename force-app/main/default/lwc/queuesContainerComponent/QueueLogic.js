import { handlePositionChange, handleRows, handleInQueueRowsLogic, handleOnBreakWithGuestRowsLogic } from "./RowLogic";
import { formatAMPM, formDateFromTime } from "c/utils";

const incrementMinute = (queue) => {
  queue.rows.forEach((row) => {
    if (row.onBreak || row.withGuest) {
      handleOnBreakWithGuestRowsLogic(row, queue.revertTime, queue.upNextTime);
    } else {
      handleInQueueRowsLogic(row);
    }
  });
  recalculatePossitions(queue);
  queue.rows.sort(getQueuesComparator());
}

const formRemoveAction = (queue, flag, isManager, resourceId) => {
  if (queue.rows.length) {
    queue.rows.forEach(row => {
      row.actions.forEach(action => {
        if (action.type === "remove") {
          if(!flag) {
            action.disabled = isManager ? flag : (resourceId !== row.id);
          } else {
            action.disabled = flag;
          }
        }
      });
    });
  }
}

const recalculatePossitions = (queue) => {
  let isQueueDecremented = false;
  queue.rows.forEach((row) => {
    if (!row.onBreak && !row.withGuest) {
      if (row.position === 1 && row.upTime === 0) {
        let oldPosition = row.position;
        row.position = getNumberOfPositionsInQueue(queue) + 1;
        row.prevPosition = oldPosition;
        //handlePositionChange(row, queue);
        decrementPossitions(queue, oldPosition);
        isQueueDecremented = true;
      }
    }
  });
  if (!isQueueDecremented) {
    queue.rows.forEach((row) => {
      if (!row.onBreak && !row.withGuest) {
        row.prevPosition = row.position;
      }
    });
  }
};

const getNumberOfPositionsInQueue = (queue) => {
  let positions = 0;
  queue.rows.forEach((row) => {
    if (!row.onBreak && !row.withGuest && !row.leadSuspend) {
      positions++;
    }
  });
  return positions;
};

const decrementPossitions = (queue, oldPosition) => {
  queue.rows.forEach((row) => {
    if (!row.onBreak && !row.withGuest) {
      if (+row.position != 1 && +row.position > +oldPosition) {
        row.prevPosition = (row.prevPosition === 1) ? row.prevPosition : row.position;
        row.position--;
        row.elapsedTime = 0;
      } else {
        row.prevPosition = (row.prevPosition === 1) ? row.prevPosition : row.position;
      }
      //handlePositionChange(row, queue);
    }
  });
};

const getQueuesComparator = () => {
  return function (prevRow, nextRow) {
    let isPrevRowInQueue = !(prevRow.onBreak  || prevRow.withGuest || prevRow.leadSuspend);
    let isNextRowInQueue = !(nextRow.onBreak || nextRow.withGuest || nextRow.leadSuspend);

    if (isPrevRowInQueue && isNextRowInQueue) {
      return prevRow.position - nextRow.position;
    }
    if (isPrevRowInQueue && !isNextRowInQueue) {
      return -1;
    }
    if (!isPrevRowInQueue && isNextRowInQueue) {
      return 1;
    }
    if (!isPrevRowInQueue && !isNextRowInQueue) {
      return prevRow.onBreak ? 1 : -1;
    }
    return 0;
  };
};
//deprecated
const isQueueExpired = (queue) => {
  let currentDate = new Date();
  let year = currentDate.getFullYear();
  let month = currentDate.getMonth() + 1;
  let day = currentDate.getDate();
  currentDate.setSeconds(0);
  let queueEndDate = new Date(year + "-"
    + (month < 10 ? "0" + month : month) + "-"
    + (month < 10 ? "0" + month : month) + "-"
    + (month < 10 ? "0" + month : month) + "-"
    + (day < 10 ? "0" + day : day) + "T"
    + (day < 10 ? "0" + day : day) + "T"
    + (day < 10 ? "0" + day : day) + "T"
    + queue.endTime.replace(".000", ""));
  let isExpired = currentDate >= queueEndDate;
  return isExpired;
}

const formQueue = (queue, isManager, resourceId) => {
  return {
    id: queue.id,
    dealer: queue.dealer,
    startTime: queue.startTime,
    endTime: queue.endTime,
    name: queue.name,
    upTime: queue.upTime,
    revertTime: queue.revertTime,
    upNextTime: queue.upNextTime,
    isExpired: queue.isExpired,
    lastRowsDataUpdateTime: queue.lastRowsDataUpdateTime,
    formattedStartTime: formatAMPM(formDateFromTime(queue.startTime)),
    formattedEndTime: formatAMPM(formDateFromTime(queue.endTime)),
    rows: handleRows(queue, queue.endTime, queue.rows, queue.revertTime, queue.upNextTime, isManager, resourceId)
  };
};

const formEmptyRowsQueue = (queue) => {
  return {
    id: queue.id,
    dealer: queue.dealer,
    startTime: queue.startTime,
    endTime: queue.endTime,
    name: queue.name,
    upTime: queue.upTime,
    revertTime: queue.revertTime,
    upNextTime: queue.upNextTime,
    isExpired: queue.isExpired,
    lastRowsDataUpdateTime: queue.lastRowsDataUpdateTime,
    formattedStartTime: formatAMPM(formDateFromTime(queue.startTime)),
    formattedEndTime: formatAMPM(formDateFromTime(queue.endTime)),
    rows: []
  };
};

const incrementPositions = (queue) => {
  queue.rows.forEach((row) => {
    if (!row.onBreak && !row.withGuest) {
      row.prevPosition = row.position;
      row.position++;
      row.elapsedTime = 0;
      //handlePositionChange(row, queue);
    }
  });
};

const incrementPositionsInRange = (queue, oldPosition) => {
  queue.rows.forEach((row) => {
    if (!row.onBreak && !row.withGuest) {
      if (+row.position >= +oldPosition) {
        row.prevPosition = row.position;
        row.position++;
        row.elapsedTime = 0;
      } else {
        row.prevPosition = row.position;
      }
      //handlePositionChange(row, queue);
    }
  });
};

export {
  formEmptyRowsQueue,
  recalculatePossitions,
  getNumberOfPositionsInQueue,
  decrementPossitions,
  getQueuesComparator,
  formQueue,
  incrementPositions,
  incrementPositionsInRange,
  incrementMinute,
  isQueueExpired,
  formRemoveAction
};