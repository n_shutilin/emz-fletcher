import { formatAMPM, getGMTTime, formDateFromTime } from "c/utils";
import { getNumberOfPositionsInQueue } from "./QueueLogic";

const FIRST_PLACE_ACTIONS = [
  {
    title: "Up",
    type: "up",
    disabled: true
  },
  {
    title: "Down",
    type: "toEnd",
    disabled: false
  },
  {
    title: "Break",
    type: "onBreak",
    disabled: false
  },
  {
    title: "With Guest",
    type: "withGuest",
    disabled: false
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  }
];

const FIRST_PLACE_NO_DOWN_ACTIONS = [
  {
    title: "Up",
    type: "up",
    disabled: true
  },
  {
    title: "Down",
    type: "toEnd",
    disabled: true
  },
  {
    title: "Break",
    type: "onBreak",
    disabled: false
  },
  {
    title: "With Guest",
    type: "withGuest",
    disabled: false
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  }
];

const NOT_FIRST_PLACE_ACTIONS = [
  {
    title: "Up",
    type: "up",
    disabled: false
  },
  {
    title: "Down",
    type: "toEnd",
    disabled: false
  },
  {
    title: "Break",
    type: "onBreak",
    disabled: false
  },
  {
    title: "With Guest",
    type: "withGuest",
    disabled: false
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  }
];

const NOT_FIRST_PLACE_NO_DOWN_ACTIONS = [
  {
    title: "Up",
    type: "up",
    disabled: false
  },
  {
    title: "Down",
    type: "toEnd",
    disabled: true
  },
  {
    title: "Break",
    type: "onBreak",
    disabled: false
  },
  {
    title: "With Guest",
    type: "withGuest",
    disabled: false
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  }
];

const START_WITH_GUEST_ACTIONS = [
  {
    title: "To Queue",
    type: "toQueue",
    disabled: false
  },
  {
    title: "Back",
    type: "upNext",
    disabled: false
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  },
  {
    title: "Choose assistant",
    type: "assistant",
    disabled: false
  }
];

const LEAS_SUSPEND_ACTIONS = [
  {
    title: "To Queue",
    type: "toQueue",
    disabled: false
  },
  {
    title: "Back",
    type: "upNext",
    disabled: true
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  },
  {
    title: "Choose assistant",
    type: "assistant",
    disabled: false
  }
];

const WITH_GUEST_ACTIONS = [
  {
    title: "To Queue",
    type: "toQueue",
    disabled: false
  },
  {
    title: "Back",
    type: "upNext",
    disabled: true
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  },
  {
    title: "Choose assistant",
    type: "assistant",
    disabled: false
  }
];

const NO_REVERT_ON_BREAK_ACTIONS = [
  {
    title: "To Queue",
    type: "toQueue",
    disabled: false
  },
  {
    title: "Revert",
    type: "revert",
    disabled: true
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  }
];

const ON_BREAK_ACTIONS = [
  {
    title: "To Queue",
    type: "toQueue",
    disabled: false
  },
  {
    title: "Revert",
    type: "revert",
    disabled: false
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  }
];

const MANAGER_DOWN_ACTION = [
  {
    title: '˅',
    type: "down",
    disabled: false
  }
]

export const ALL_DISABLED_ACTIONS = [
  {
    title: "Up",
    type: "up",
    disabled: true
  },
  {
    title: "Down",
    type: "toEnd",
    disabled: true
  },
  {
    title: "Break",
    type: "onBreak",
    disabled: true
  },
  {
    title: "With Guest",
    type: "withGuest",
    disabled: true
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: true
  }
];

export const ALL_DISABLED_ACTIONS_ON_BREAK = [
  {
    title: "To Queue",
    type: "toQueue",
    disabled: true
  },
  {
    title: "Revert",
    type: "revert",
    disabled: true
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: true
  }
];

export const SALES_PERSON_IN_QUEUE = [
  {
    title: "Up",
    type: "up",
    disabled: true
  },
  {
    title: "Down",
    type: "toEnd",
    disabled: true
  },
  {
    title: "Break",
    type: "onBreak",
    disabled: true
  },
  {
    title: "With Guest",
    type: "withGuest",
    disabled: false
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  }
];

const ALL_DISABLED_WITH_GUEST_ACTIONS = [
  {
    title: "To Queue",
    type: "toQueue",
    disabled: true
  },
  {
    title: "Back",
    type: "upNext",
    disabled: true
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: true
  }
];

export const ALL_DISABLED_ACTIONS_ON_BREAK_WITH_REMOVE = [
  {
    title: "To Queue",
    type: "toQueue",
    disabled: true
  },
  {
    title: "Revert",
    type: "revert",
    disabled: true
  },
  {
    title: "Remove from Queue",
    type: "remove",
    disabled: false
  }
];

const handleInQueueRowsLogic = (row) => {
  row.elapsedTime++;
  row.totalTime++;
  if (+row.position === 1) {
    row.upTime--;
  }
};

const handleOnBreakWithGuestRowsLogic = (row, revertTime, upNextTime) => {
  row.elapsedTime++;
  row.totalTime++;
  row.prevPosition = row.withGuest ? -2 : -1;
  if (row.elapsedTime > upNextTime && row.withGuest) {
    row.actions = WITH_GUEST_ACTIONS;
  }
  if (row.elapsedTime > revertTime && row.onBreak) {
    row.actions = NO_REVERT_ON_BREAK_ACTIONS;
  }
};

const handlePositionChange = (row, queue, isManager, resourceId) => {
  var currentTime = new Date();
  row.status = getStatusBasedOnPosition(row);
  if (row.prevPosition !== row.position) {
    if (currentTime < formDateFromTime(queue.startTime)) {
      row.startTime = queue.formattedStartTime;
      row.gmtStartTime = queue.startTime;
      currentTime = formDateFromTime(queue.startTime);
    } else {
      row.startTime = formatAMPM(currentTime);
      row.gmtStartTime = getGMTTime();
    }
  }
  if (+row.position === 1) {
    let endDate = formDateFromTime(row.gmtStartTime);
    let rowUpTime = row.upTime === 0 ? queue.upTime : +row.inQueueUpTime;
    endDate.setMinutes(endDate.getMinutes() + rowUpTime + +row.elapsedTime);
    if (endDate > formDateFromTime(queue.endTime)) {
      row.endTime = formatAMPM(formDateFromTime(queue.endTime));
    } else {
      row.endTime = formatAMPM(endDate);
    }
  } else {
    row.endTime = "-";
  }
  if (+row.beforeBreakStatement.elapsedTime === 0) {
    row.elapsedTime = (row.prevPosition === row.position || row.prevPosition < 0) ? row.elapsedTime : 0;
  }
  if (row.onBreak || row.withGuest) {
    row.upTime = "-";
  } else if (+row.position === 1) {
    if (row.prevPosition !== row.position) {
      row.upTime = row.beforeBreakStatement.elapsedTime === 0 ? row.inQueueUpTime : row.upTime;
    }
  } else {
    row.upTime = row.inQueueUpTime;
  }
  row.actions = getActionsBasedOnPosition(row, queue, queue.revertTime, queue.upNextTime, isManager, resourceId);
};

const handleRows = (queue, endTime, rows, revertTime, upNextTime, isManager, resourceId) => {
  rows.forEach((row) => {
    row.position = row.position > 0 ? row.position : " ";
    row.upTime = row.upTime > 0 ? row.upTime : "-";
    row.enteredQueue = formatAMPM(formDateFromTime(row.gmtEnteredQueue));
    if (row.position === 1) {
      let endDate = formDateFromTime(row.gmtStartTime);
      endDate.setMinutes(endDate.getMinutes() + row.upTime + row.elapsedTime);
      if (endDate > formDateFromTime(endTime)) {
        row.endTime = formatAMPM(formDateFromTime(endTime));
      } else {
        let tempTime = formDateFromTime(row.endTime);
        row.endTime = formatAMPM(tempTime);
      }
    } else {
      row.endTime = "-";
    }
    if (new Date() < formDateFromTime(queue.startTime)) {
      row.startTime = formatAMPM(formDateFromTime(queue.startTime));
      row.gmtStartTime = queue.startTime;
    } else {
      let tempStartTime = formDateFromTime(row.gmtStartTime);
      row.startTime = formatAMPM(tempStartTime);
    }
    row.actions = getActionsBasedOnPosition(row, queue, revertTime, upNextTime, isManager, resourceId);
    if(row.havePairedRecords) {
      let pairList = [];
      row.paired.forEach((pair) => {
        let tempRow = rows.find(row => row.id === pair);
        tempRow.inPairing = true;
        pairList.push({
          id: pair + '_paired',
          name: tempRow.name,
          pictureUrl: tempRow.pictureUrl
        });
      });
      row.paired = pairList;
    }
  });
  return rows;
};

const getActionsBasedOnPosition = (row, queue, revertTime, upNextTime, isManager, resourceId) => {
  let position = row.position;
  switch (position) {
    case 1:
      if(isManager){
        return getNumberOfPositionsInQueue(queue) === 1 ? FIRST_PLACE_NO_DOWN_ACTIONS : FIRST_PLACE_ACTIONS.concat(MANAGER_DOWN_ACTION);
      } else {
        return row.id === resourceId ? SALES_PERSON_IN_QUEUE : ALL_DISABLED_ACTIONS
      }
    case -1:
    case -2:
    case " ":
      if(isManager){
        return getWithGuestBreakActions(row, revertTime, upNextTime);
      } else {
        return row.id === resourceId ? getWithGuestBreakActions(row, revertTime, upNextTime) 
        : row.withGuest ? ALL_DISABLED_WITH_GUEST_ACTIONS : ALL_DISABLED_ACTIONS_ON_BREAK
      }

    default:
      if(isManager){
        return +position === +getNumberOfPositionsInQueue(queue) ? NOT_FIRST_PLACE_NO_DOWN_ACTIONS : NOT_FIRST_PLACE_ACTIONS.concat(MANAGER_DOWN_ACTION);
      } else {
        return row.id === resourceId ? SALES_PERSON_IN_QUEUE : ALL_DISABLED_ACTIONS
      }
  }
};

const getWithGuestBreakActions = (row, revertTime, upNextTime) => {
  if (row.elapsedTime >= upNextTime && row.withGuest) {
    return WITH_GUEST_ACTIONS;
  } else if (row.elapsedTime >= revertTime && row.onBreak) {
    return NO_REVERT_ON_BREAK_ACTIONS;
  } else {
    return (row.withGuest || row.leadSuspend) ? defineWithGuestActions(row) : ON_BREAK_ACTIONS;
  }
};

const defineWithGuestActions = (row) => {
  return row.withGuest ? START_WITH_GUEST_ACTIONS : LEAS_SUSPEND_ACTIONS
}

const getStatusBasedOnPosition = (row) => {
  switch (row.position) {
    case 1:
      return "Up Next";
    case " ":
      return getWithGuestBreakStatus(row);
    default:
      return "In Queue";
  }
};

const getWithGuestBreakStatus = (row) => {
  if(row.leadSuspend){
    return "Lead Suspend";
  }
  return row.withGuest ? "With Guest" : "On Break";
};

export {
  handleRows,
  handleInQueueRowsLogic,
  handleOnBreakWithGuestRowsLogic,
  handlePositionChange,
  getActionsBasedOnPosition,
  getWithGuestBreakActions,
  getStatusBasedOnPosition,
  getWithGuestBreakStatus
};