import { incrementMinute, isQueueExpired } from "./QueueLogic";
import { getDateDifferenceInMinutes, formDateFromTime } from "c/utils";

const ONE_MINUTE_MS = 60000;

export default class TimerTask {
  queue;
  timer;
  container;
  accumulator = [];

  constructor(queue, container) {
    this.queue = queue;
    this.container = container;
    this.fillAccumulatorByValues();
    this.restoreDataByTimeShift();
    this.init();
  }

  init() {
    let currentDate = new Date();
    let queueStartDate = formDateFromTime(this.queue.startTime);
    if (currentDate < queueStartDate) {
      let timeDiff = (queueStartDate.getTime() - currentDate.getTime()) / ONE_MINUTE_MS;
      if (timeDiff > 1) {
        setTimeout(() => this.startTimerTask(), queueStartDate - currentDate);
      } else {
        this.startTimerTask();
      }
    } else {
      this.startTimerTask();
    }
  }

  timerJob() {
    if (!this.queue.isExpired) {
      if (isQueueExpired(this.queue)) {
        if (this.queue.rows.length > 0) {
          incrementMinute(this.queue, this.container, true);
          this.container.handleRowUpdate(this.queue.id);
        }

        this.queue.isExpired = true;
        this.container.handleQueueExpired(this.queue);
        clearInterval(this.timer);
      } else {
        if (this.queue.rows.length > 0) {
          incrementMinute(this.queue, this.container, true);
          this.container.handleRowUpdate(this.queue.id);
        }
      }
    }
  }

  restoreDataByTimeShift() {
    if (new Date(this.queue.lastRowsDataUpdateTime) > formDateFromTime(this.queue.startTime)) {
      if (this.queue.rows.length > 0) {
        console.log('%c ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ rows in queue: "' + this.queue.name + '" state restoring started ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~', 'background: #F85C50; color: #5BFF62');
        let difference = getDateDifferenceInMinutes(new Date(), new Date(this.queue.lastRowsDataUpdateTime));
        if (difference > 0) {
          console.log('recalculating started for time difference: ' + difference + '(min)');
          for (let i = 0; i < difference; i++) {
            incrementMinute(this.queue, this.container, false);
            this.accumulateRowsOnPositionTime();
          }
          this.container.handleAccumulatedTime(this.accumulator, this.queue.id);
        }
        console.log('%c ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  rows in queue state restoring ended  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~', 'background: #F85C50; color: #5BFF62');
      }
    }
  }

  accumulateRowsOnPositionTime() {
    this.queue.rows.forEach(row => {
      this.accumulator.forEach(data => {
        if (data.rowId === row.id) {
          if (data.totalTime.has(row.position)) {
            if (row.position === " ") {
              let position = row.onBreak ? -1 : -2;
              let tempElapsedTime = data.totalTime.get(position);
              tempElapsedTime++;
              data.totalTime.set(position, tempElapsedTime);
            } else {
              let tempElapsedTime = data.totalTime.get(row.position);
              tempElapsedTime++;
              data.totalTime.set(row.position, tempElapsedTime);
            }
          } else {
            if (row.position === " ") {
              let position = row.onBreak ? -1 : -2;
              data.totalTime.set(position, row.elapsedTime);
            } else {
              data.totalTime.set(row.position, row.elapsedTime);
            }
          }
        }
      });
    });
  }

  fillAccumulatorByValues() {
    this.queue.rows.forEach(row => {
      this.accumulator.push({
        rowId: row.id,
        inQueueUpTime: row.inQueueUpTime,
        totalTime: new Map(),
      })
    });
  }

  startTimerTask() {
    this.timer = setInterval(() => this.timerJob(), ONE_MINUTE_MS);
  }
}