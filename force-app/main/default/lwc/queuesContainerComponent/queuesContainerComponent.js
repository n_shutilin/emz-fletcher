import { LightningElement, track, api } from "lwc";
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';
import id from "@salesforce/user/Id";
import getQueuesByUserId from "@salesforce/apex/QueueManagerController.getQueuesByUserId";
import updateResourceData from "@salesforce/apex/QueueManagerController.updateResourceInQueueData";
import updateRows from "@salesforce/apex/QueueManagerController.updateRows";
import getDealer from "@salesforce/apex/QueueManagerController.getDealer";
import insertNewQueue from "@salesforce/apex/QueueManagerController.insertQueue";
import updateQueueData from "@salesforce/apex/QueueManagerController.updateQueueData";
import closeQueue from "@salesforce/apex/QueueManagerController.closeQueue";
import getQueueDataById from '@salesforce/apex/QueueManagerController.getQueueDataById';
import createShoowroomRecord from '@salesforce/apex/QueueManagerController.createShoowroomRecord';
import linkOpportunityToShowroom from '@salesforce/apex/QueueManagerController.linkOpportunityToShowroom';
import createInQueueActivity from '@salesforce/apex/QueueManagerController.createInQueueActivity';
import pairRecords from '@salesforce/apex/QueueManagerController.pairRecords';
import unPairRecords from '@salesforce/apex/QueueManagerController.unPairRecords';
import TimerTask from "./TimerTask";
import { handlePositionChange } from "./RowLogic";
import { SALE_MANAGER_VALUE } from 'c/constants';
import {
  formEmptyRowsQueue,
  getNumberOfPositionsInQueue,
  decrementPossitions,
  getQueuesComparator,
  formQueue,
  incrementPositions,
  incrementPositionsInRange,
  formRemoveAction
} from "./QueueLogic";
import { formatAMPM, generateUUIDv4, getGMTTime } from "c/utils";

export default class QueuesContainerComponent extends LightningElement {

  @api channelName = '/event/Up_System_Platform_Event__e';
  @api isSales;
  @api timeSlot;
  @api role;
  @api resourceId;

  @track queues = [];
  @track isModalOpen = false;
  @track isLeadModalOpen = false;
  @track selectedQueue;
  @track selectedRow;
  @track openAssistantModal = false;

  subscription = {};
  userId = id;
  isLoaded = false;
  isEmpty = false;
  updateInProgres = false;
  batchInProgres = false;
  dealer;
  timerTasks = [];
  updates = new Map();
  uuid;

  get activeQueues() {
    return this.queues.length > 0 ? "Active queues: " + this.queues.length : "No active queues";
  }

  get isManager() {
    return this.role === SALE_MANAGER_VALUE;
  }

  connectedCallback() {
    this.setDealer();
    this.fetchData();
    this.subscribe();
    this.uuid = generateUUIDv4();
  }

  getArrayOfQueuesByDealer(dealer) {
    let tempQueues = [];
    this.queues.forEach((queue) => {
      if (queue.dealer === dealer) {
        tempQueues.push(queue);
      }
    });
    return tempQueues;
  }

  subscribe() {
    const messageCallback = (response) => {
      let event = JSON.parse(JSON.stringify(response));
      switch (event.data.payload.Type__c) {
        case "Dropped.":
          this.refreshQueueData(event.data.payload.Queue_Id__c);
          break;
        case "Row Data Update.":
          let uuid = event.data.payload.UUID__c;
          if(uuid && uuid !== this.uuid){
            this.refreshQueueData(event.data.payload.Queue_Id__c);
          }
          break;
        case "Update.":
          this.batchInProgres = false;
          this.refreshQueueData(event.data.payload.Queue_Id__c);
          break;
        case "New queue inserted.":
          this.fetchData();
          break;
        case "Queue closed.":
          this.handleQueueExpired(event.data.payload.Queue_Id__c);
          break;
        case "Batch started.":
          this.batchInProgres = true;
          break;
      }
    };
    subscribe(this.channelName, -1, messageCallback).then(response => {
      this.subscription = response;
    });
  }

  refreshQueueData(id) {
    getQueueDataById({ queueId: id }).then((result) => {
      let tempQueues = [];
      this.queues.forEach(queue => {
        if (queue.id !== id) {
          tempQueues.push(queue);
        } else {
          let tempQueue = JSON.parse(result);
          if (tempQueue.rows.length > 0) {
            tempQueue.rows.sort(getQueuesComparator());
          }
          tempQueues.push(formQueue(tempQueue, this.isManager, this.resourceId));
        }
      });
      this.queues = tempQueues;
      this.queues.forEach((queue) => {
        let firstRow = queue.rows.find(row => row.position === 1);
        if(firstRow){
          this.unPaitIfNeeded(firstRow, queue);
        }
      })
    });
  }

  openModal() {
    this.isModalOpen = true;
  }

  openLeadModal(rowId, queueId) {
    let queue = this.queues.find(queue => queue.id === queueId)
    let row = queue.rows.find(row => row.id === rowId)
    this.selectedQueue = queue;
    this.selectedRow = row;
    this.isLeadModalOpen = true;
  }

  setDealer() {
    getDealer({ userId: this.userId }).then((result) => {
      if (result) {
        this.dealer = result.toString();
      }
    });
  }

  fetchData() {
    this.isLoaded = false;
    getQueuesByUserId({ userId: this.userId }).then((result) => {
      let tempQueues = [];
      if (result) {
        let queues = JSON.parse(result);
        queues.forEach((queue) => {
          let tempQueue = formQueue(queue, this.isManager, this.resourceId);
          if (tempQueue.rows.length > 0) {
            tempQueue.rows.sort(getQueuesComparator());
          }
          tempQueues.push(tempQueue);
        });
        this.queues = tempQueues;
        this.checkQueueContainerData();
      }
    });
  }

  handleAccumulatedTime(accumulator, id) {
    let formedData = [];
    accumulator.forEach(data => {
      formedData = Array.prototype.concat(formedData, this.formDataFromAccumulator(data));
    });
    // updateTotalOnPositionTime({ serializedData: JSON.stringify(formedData), queueId: id }).then((result) => {
    //   this.handleRowUpdate(id)
    // });
  }

  formDataFromAccumulator(rowData) {
    let formedData = [];
    let data = this.parseMapToArray(rowData.totalTime);
    data.forEach(value => {
      formedData.push({
        rowId: rowData.rowId,
        inQueueUpTime: rowData.inQueueUpTime,
        position: value.position,
        onPositionTime: value.onPositionTime
      });
    });
    return formedData;
  }

  parseMapToArray(map) {
    let data = [];
    for (let position of map.keys()) {
      data.push({
        position: position,
        onPositionTime: map.get(position)
      });
    }
    return data;
  }

  checkQueueContainerData() {
    if (this.queues.length > 0) {
      //this.createTimerTasks();
      this.isEmpty = false;
      this.isLoaded = true;
    } else {
      this.isEmpty = true;
      this.isLoaded = true;
    }
  }

  createTimerTasks() {
    this.queues.forEach((queue) => {
      if (!this.isTaskCreated(queue.id)) {
        this.timerTasks.push(new TimerTask(queue, this));
      }
    });
  }

  handleRowUpdate(id) {
    this.queues.forEach((queue) => {
      if (id === queue.id) {
        queue.rows.forEach((row) => {
          if(row.position === 1 && row.inPairing){
            this.unPaitIfNeeded(row, queue);
          }
          handlePositionChange(row, queue, this.isManager, this.resourceId);
        });
      }
    });
    if (this.updateInProgres && this.batchInProgres) {
      this.updates.set(id, true);
    } else {
      this.executeUpdate(id);
    }
  }

  executeUpdate(id) {
    this.updateInProgres = true;
    this.queues.forEach((queue) => {
      if (queue.id === id) {
        let updateData = [];
        formRemoveAction(queue, true, this.isManager, this.resourceId);
        queue.rows.forEach(row => {
          let temp = this.formDataUpdateFormat(row, id);
          updateData.push(temp);
        });
        updateRows({
          serializedRows: JSON.stringify(updateData),
          queueId: id,
          UUID: this.uuid
        }).then(() => {
          if (this.updates.has(id) && this.updates.get(id)) {
            this.updates.set(id, false);
            this.executeUpdate(id);
          } else {
            this.updateInProgres = false;
            formRemoveAction(queue, false, this.isManager, this.resourceId);
          }
        });
      }
    });
  }

  isTaskCreated(id) {
    let isCreated = false;
    this.timerTasks.forEach((task) => {
      if (task.queue.id === id) {
        isCreated = true;
      }
    });
    return isCreated;
  }

  @api handleToQueueFromPoolRecord(row) {
    let rowToInsert = JSON.parse(row);
    if(this.queues.length){
      if (!this.isRowPresent(rowToInsert.id)) {
        this.queues.forEach((queue) => {
          if (!queue.isExpired) {
            let formedRow = this.formNewRow(rowToInsert, queue);
            handlePositionChange(formedRow, queue, this.isManager, this.resourceId);
            queue.rows.push(formedRow);
            queue.rows.sort(getQueuesComparator());
            updateResourceData({
              data: JSON.stringify(this.formDataUpdateFormat(formedRow, queue.id))
            });
            this.dispatchRowInQueue(row.id);
          }
        });
      }
    } else {
      this.dispatchReject();
    }
  }

  dispatchReject() {
    this.dispatchEvent(
      new CustomEvent("reject", {
        detail: {
        }
      })
    )
  }

  dispatchRowInQueue(id) {
    this.dispatchEvent(
      new CustomEvent("resource", {
        detail: {
          resource: id
        }
      })
    );
  }

  //deprecated
  handleDrop(event) {
    if (event.detail.resource) {
      let rowToInsert = event.detail.resource;
      if (!this.isRowPresent(rowToInsert.id)) {
        this.queues.forEach((queue) => {
          if (queue.id === event.detail.queueId) {
            let formedRow = this.formNewRow(rowToInsert, queue);
            handlePositionChange(formedRow, queue, this.isManager, this.resourceId);
            queue.rows.push(formedRow);
            queue.rows.sort(getQueuesComparator());
            updateResourceData({
              data: JSON.stringify(this.formDataUpdateFormat(formedRow, queue.id))
            });
            this.dispatchRowInQueue(formedRow.id);
          }
        });
      }
    }
  }

  handleModal(event) {
    let type = event.detail.type;
    switch (type) {
      case "close":
        this.isModalOpen = false;
        break;
      case "submit":
        this.isModalOpen = false;
        this.createNewQueue(event);
        break;
    }
  }

  createNewQueue(event) {
    let name = event.detail.name;
    let dealer = event.detail.dealer;
    let startTime = event.detail.startTime;
    let endTime = event.detail.endTime;
    let upTime = event.detail.upTime;
    let revert = event.detail.revert;
    let upNext = event.detail.upNext;
    insertNewQueue({
      queueName: name,
      queueDealer: dealer,
      queueStartTime: startTime,
      queueEndTime: endTime,
      userId: this.userId,
      queueUpTime: upTime,
      revertTime: revert,
      upNextTime: upNext
    }).then((result) => {
      let tempQueue = formEmptyRowsQueue(JSON.parse(result));
      this.queues.push(tempQueue);
      this.checkQueueContainerData();
    });
  }

  formDataUpdateFormat(formedRow, queue) {
    let position = formedRow.withGuest || formedRow.leadSuspend ? -2 : formedRow.onBreak ? -1 : formedRow.position;
    let upTime = formedRow.onBreak || formedRow.withGuest ? -2 : formedRow.upTime;
    return {
      resourceId: formedRow.id,
      name: formedRow.name,
      pictureUrl: formedRow.pictureUrl,
      queueId: queue,
      oldPosition: formedRow.prevPosition,
      newPosition: position,
      newOnPositionTime: formedRow.elapsedTime,
      totalTime: formedRow.totalTime,
      startTime: formedRow.gmtStartTime,
      enteredQueue: formedRow.gmtEnteredQueue,
      status: formedRow.status,
      upTime: upTime,
      inQueueUpTime: formedRow.inQueueUpTime,
      beforeBreakStatement: formedRow.beforeBreakStatement
    };
  }

  isRowPresent(id) {
    let isPresent = false;
    this.queues.forEach((queue) => {
      queue.rows.forEach((row) => {
        if (row.id === id) {
          isPresent = true;
        }
      });
    });
    return isPresent;
  }

  formNewRow(newRow, queue) {
    let newPosition = getNumberOfPositionsInQueue(queue) + 1;
    let formedRow = {
      id: newRow.id,
      position: newPosition,
      prevPosition: 0,
      pictureUrl: newRow.pictureUrl,
      inQueueUpTime: queue.upTime,
      name: newRow.name,
      status: "New Row",
      upTime: 0,
      enteredQueue: formatAMPM(new Date()),
      gmtEnteredQueue: getGMTTime(),
      startTime: formatAMPM(new Date()),
      endTime: "",
      elapsedTime: 0,
      totalTime: 0,
      onBreak: false,
      withGuest: false,
      beforeBreakStatement: {
        position: 0,
        elapsedTime: 0,
        upTime: 0
      },
      actions: [],
      paired: [],
      havePairedRecords: false
    };
    return formedRow;
  }

  handleAction(event) {
    let type = event.detail.type;
    switch (type) {
      case "up":
        this.handleUp(event);
        break;
      case "down":
          this.handleDown(event);
          break;
      case "toEnd":
        this.handleToEnd(event);
        break;
      case "onBreak":
        this.handleBreak(event);
        break;
      case "withGuest":
        this.handleGuest(event);
        break;
      case "toQueue":
        this.handleToQueue(event);
        break;
      case "upNext":
        this.handleUpNext(event);
        break;
      case "revert":
        this.handleRevert(event);
        break;
      case "remove":
        this.handleRemoveFromQueue(event);
        break;
      case "assistant":
        this.handleAssistant(event);
        break;
      case "UnPair":
        this.handleUpPair(event);
        break;
    }
  }

  unPaitIfNeeded(row, queue) {
    let pairedId = row.id + '_paired'
    let mainRow = queue.rows.find((innerRow) => {
      return innerRow.paired.find((pairedRow) => {
        return pairedRow.id === pairedId
      })
    })
    if(mainRow){
      row.inPairing = false;
      mainRow.paired = mainRow.paired.filter(pair => pair.id !== pairedId);
      unPairRecords({
        mainRecord: mainRow.id, 
        pairedRecordId: row.id,
        queueId: queue.id,
        UUID: this.uuid
      });
    }
  }

  handleUpPair(event){
    let queueId = event.detail.queueId;
    let rowId = event.detail.id;
    let pairId = event.detail.pairId;
    let queue = this.queues.find(queue => queue.id === queueId)
    let row = queue.rows.find(row => row.id === rowId)
    let pairedRow = queue.rows.find(row => row.id === pairId.replace('_paired', ''))
    row.paired = row.paired.filter(pair => pair.id !== pairId);
    pairedRow.inPairing = false;
    if(!row.paired.length){ //to readability
      row.havePairedRecords = false;
    }
    unPairRecords({
      mainRecord: rowId, 
      pairedRecordId: pairId.replace('_paired', ''),
      queueId: queueId,
      UUID: this.uuid
    }).then((result) =>{
      console.log('UNPAIRED')
    });
  }

  clearPairing(queue, row){
    let pairedList = row.paired.map((row) => {
      return row.id = row.id.replace('_paired', '');
    });
    row.paired = [];
    unPairRecords({
      mainRecord: row.id, 
      pairedRecordId: pairedList,
      queueId: queue.id,
      UUID: this.uuid
    }).then((result) =>{
      row.havePairedRecords = false;
      pairedList.forEach((id) => {
        queue.rows.find((inQueueRow => inQueueRow.id === id)).inPairing = false;
      });
      console.log('UNPAIRED')
    });
  }

  handleAssistant(event){
    let rowId = event.detail.id;
    let queueId = event.detail.queueId;
    let queue = this.queues.find(queue => queue.id === queueId)
    let row = queue.rows.find(row => row.id === rowId)
    this.selectedQueue = queue;
    this.selectedRow = row;
    this.openAssistantModal = true;
    // row.havePairedRecords = !row.havePairedRecords
    // row.paired.push({
    //   pictureUrl :"https://fletcherjones--datacopy--c.documentforce.com/profilephoto/005/F",
    //   name: "Vasyan Petro"
    // });
  }

  //Method only for tests only
  handleQueueDelete(event) {
    let rows = event.detail.rows;
    let queueId = event.detail.queueId;
    rows.forEach(id => {
      this.dispatchRemove(id);
    })
    let tempTasks = [];
    this.timerTasks.forEach((task) => {
      if (queueId !== task.queue.id) {
        tempTasks.push(task);
      }
    });
    this.timerTasks = tempTasks;
    let tempQueues = [];
    this.queues.forEach(queue => {
      if (queueId !== queue.id) {
        tempQueues.push(queue);
      }
    });
    this.queues = tempQueues;
    this.checkQueueContainerData();
  }

  handleClose(event) {
    let id = event.detail.queueId;
    this.handleQueueExpired(id);
    closeQueue({ queueId: id }).then((result) => {
      this.dispatchEvent(
        new CustomEvent("close", {
          detail: {
            queueId: id,
          }
        })
      );
    });
  }

  handleUp(event) {
    let rowId = event.detail.id;
    let rowPosition = event.detail.rowposition;
    let rowNewPosition = rowPosition - 1;
    let queueId = event.detail.queueId;
    let queue = this.queues.find((queue) => queueId === queue.id);
    queue.rows.forEach((row) => {
      if (+row.position === +rowNewPosition && row.id != rowId) {
        row.prevPosition = row.position;
        row.position = rowPosition;
      }
      if (rowId === row.id) {
        row.prevPosition = row.position;
        row.position = rowNewPosition;
      }
    });

    queue.rows.sort(getQueuesComparator());
    this.handleRowUpdate(queueId);
  }

  handleDown(event) {
    let rowId = event.detail.id;
    let queueId = event.detail.queueId;
    this.queues.forEach((queue) => {
      if (queueId === queue.id) {
        queue.rows.forEach((row) => {
          if (row.id === rowId) {
            let oldPosition = row.position;
            queue.rows.find(element => +element.position === +oldPosition + 1).position = oldPosition
            row.position = oldPosition + 1;
          }
        });
        queue.rows.sort(getQueuesComparator());
      }
    });
    this.handleRowUpdate(queueId);
  }

  handleToEnd(event) {
    let rowId = event.detail.id;
    let queueId = event.detail.queueId;
    this.queues.forEach((queue) => {
      if (queueId === queue.id) {
        queue.rows.forEach((row) => {
          if (row.id === rowId) {
            let oldPosition = row.position;
            row.position = getNumberOfPositionsInQueue(queue) + 1;
            decrementPossitions(queue, oldPosition);
          }
        });
        queue.rows.sort(getQueuesComparator());
      }
    });
    this.handleRowUpdate(queueId);
  }

  handleBreak(event) {
    let rowId = event.detail.id;
    let queueId = event.detail.queueId;
    this.queues.forEach((queue) => {
      if (queueId === queue.id) {
        queue.rows.forEach((row) => {
          if (row.id === rowId) {
            let oldPosition = row.position;
            let statement = row.beforeBreakStatement;
            statement.elapsedTime = row.elapsedTime;
            statement.position = row.position;
            statement.upTime = row.upTime;
            row.position = " ";
            row.onBreak = true;
            row.elapsedTime = 0;
            row.prevPosition = oldPosition;
            decrementPossitions(queue, oldPosition);
          }
        });
        queue.rows.sort(getQueuesComparator());
      }
    });
    this.handleRowUpdate(queueId);
  }

  handleGuest(event) {
    let rowId = event.detail.id;
    let queueId = event.detail.queueId;
    this.queues.forEach((queue) => {
      if (queueId === queue.id) {
        queue.rows.forEach((row) => {
          if (row.id === rowId) {
            let oldPosition = row.position;
            let statement = row.beforeBreakStatement;
            statement.position = oldPosition;
            statement.elapsedTime = row.elapsedTime;
            statement.upTime = row.upTime;
            row.elapsedTime = 0;
            row.position = " ";
            row.withGuest = true;
            decrementPossitions(queue, oldPosition);
            row.prevPosition = oldPosition;
          }
        });
        queue.rows.sort(getQueuesComparator());
      }
    });
    this.handleRowUpdate(queueId);
    if(this.isSales){
      this.processShowroom(rowId);
    }
  }

  processShowroom(rowId){
      createShoowroomRecord({resourceId : rowId}).then((result) => {
        console.log('SHOWROOM CREATED => ' + result)
      });
  }

  
  handleLeadModal(event) {
    let type = event.detail.type;
    switch (type) {
      case 'noLead':
        let reason = event.detail.reason;
        this.handleNoLead(reason);
        break;
      case 'newLead':
        let recordId = event.detail.record;
        this.handleNewLead();
        this.createActivity('lead', recordId)
        break;
      case 'opportunity':
        let opportunityId = event.detail.record;
        this.linkOpportunityToShowroom(opportunityId);
        this.createActivity('opportunity', opportunityId);
        this.backToQueue(this.selectedQueue.id, this.selectedRow.id);
        break;
      case 'account':
        let accountId = event.detail.record;
        this.createActivity('account', accountId);
        this.backToQueue(this.selectedQueue.id, this.selectedRow.id);
        break;
      case 'close':
        if(!this.isSales) {
          this.backToQueue(this.selectedQueue.id, this.selectedRow.id);
        }
        break;
    }
    this.isLeadModalOpen = false;
    this.clearSelection();
  }

  createActivity(type = 'blank', recordId, reason = null) {
    createInQueueActivity({
      resourceId : this.selectedRow.id,
      type : type,
      recordId : recordId,
      noRecordReason : reason
    }).then((result) => {
      console.log('ACTIVIY CREATED => ' + result)
    })
  }

  linkOpportunityToShowroom(opportunityId){
    let resourceId = this.selectedRow.id;
    linkOpportunityToShowroom({
      resourceId : resourceId, 
      opportunityId: opportunityId, 
      dealer : this.dealer}).then((result)=>{
        console.log('SHOWROOM UPDATED => ' + result)
    })
  }

  handleAssistantModal(event) {
    let type = event.detail.type;
    switch (type) {
      case 'selected':
        let rowId = event.detail.rowId;
        let queueId = event.detail.queueId; 
        this.handleAssistantSelect(rowId, queueId);
        break;
    }
    this.openAssistantModal = false;
  }

  handleAssistantSelect(rowId, queueId) {
    let queue = this.queues.find(queue => queue.id === queueId);
    let row = queue.rows.find(row => row.id === rowId);
    row.inPairing = true;
    this.selectedRow.paired.push(
      {
        id: row.id + '_paired',
        name: row.name,
        pictureUrl: row.pictureUrl
      });
    this.selectedRow.havePairedRecords = true;
    pairRecords({
      mainRecord : this.selectedRow.id,
      pairedRecord : row.id,
      queueId: queueId,
      UUID: this.uuid
    }).then((result) => {
      console.log('PAIRED CREATED => ' + result)
    })
    this.clearSelection();
  }

  clearSelection() {
    this.selectedRow = null;
    this.selectedQueue = null;
  }

  handleNoLead(reason) {
    if(this.isManager) {
      this.handleNewLead();
      this.createActivity(null, null, reason)
    } else {
      this.selectedRow.withGuest = false;
      this.selectedRow.onBreak = false;
      this.selectedRow.leadSuspend = true;
      this.handleRowUpdate(this.selectedQueue.id);
    }
  }

  handleNewLead() {
    this.selectedRow.position = getNumberOfPositionsInQueue(this.selectedQueue) + 1;
    this.selectedRow.prevPosition = this.selectedRow.withGuest || this.selectedRow.leadSuspend ? -2 : -1;
    this.selectedRow.withGuest = false;
    this.selectedRow.onBreak = false;
    this.selectedRow.leadSuspend = false;
    this.selectedRow.elapsedTime = 0;
    let statement = this.selectedRow.beforeBreakStatement;
    statement.elapsedTime = 0;
    statement.position = 0;
    statement.upTime = 0;
    this.selectedQueue.rows.sort(getQueuesComparator());
    this.handleRowUpdate(this.selectedQueue.id);
  }

  handleToQueue(event) {
    let rowId = event.detail.id;
    let queueId = event.detail.queueId;
    let queue = this.queues.find((queue) => queueId === queue.id)
    let row = queue.rows.find((row) => row.id === rowId)
    if(row.paired.length){
      this.clearPairing(queue, row);
    }
    if(row.withGuest || row.leadSuspend) {
      this.openLeadModal(rowId, queueId);
    } else {
      this.backToQueue(queueId, rowId);
    }
  }

  backToQueue(queueId, rowId) {
    this.queues.forEach((queue) => {
      if (queueId === queue.id) {
        queue.rows.forEach((row) => {
          if (row.id === rowId) {
            row.position = getNumberOfPositionsInQueue(queue) + 1;
            row.prevPosition = row.withGuest ? -2 : -1;
            row.withGuest = false;
            if(row.onBreak){
              row.enteredQueue = formatAMPM(new Date());
              row.gmtEnteredQueue = getGMTTime(),
              row.onBreak = false;
            }
            row.elapsedTime = 0;
            let statement = row.beforeBreakStatement;
            statement.elapsedTime = 0;
            statement.position = 0;
            statement.upTime = 0;
          }
        });
        queue.rows.sort(getQueuesComparator());
      }
    });
    this.handleRowUpdate(queueId);
  }

  handleUpNext(event) {
    let rowId = event.detail.id;
    let queueId = event.detail.queueId;
    let queue = this.queues.find(queue => queue.id === queueId);
    let row = queue.rows.find(row => row.id === rowId)
    if(row.paired.length){
      this.clearPairing(queue, row);
    }
    let statement = row.beforeBreakStatement;
    let oldPosition = statement.position;
    let lastPosition = getNumberOfPositionsInQueue(queue) + 1;
    if (oldPosition > lastPosition) {
      row.position = lastPosition;
    } else {
      incrementPositionsInRange(queue, oldPosition);
      row.position = oldPosition;
    }
    row.upTime = oldPosition === 1 ? statement.upTime + row.elapsedTime : statement.upTime;
    row.elapsedTime = row.elapsedTime + statement.elapsedTime;
    row.withGuest = false;
    row.onBreak = false;
    statement.elapsedTime = 0;
    statement.position = 0;
    statement.upTime = 0;
    row.prevPosition = -2;
    queue.rows.sort(getQueuesComparator());
    this.handleRowUpdate(queueId);
  }

  handleRevert(event) {
    let rowId = event.detail.id;
    let queueId = event.detail.queueId;
    this.queues.forEach((queue) => {
      if (queueId === queue.id) {
        queue.rows.forEach((row) => {
          if (row.id === rowId) {
            let statement = row.beforeBreakStatement;
            let oldPosition = statement.position;
            let lastPosition = getNumberOfPositionsInQueue(queue) + 1;
            if (oldPosition > lastPosition) {
              row.position = lastPosition;
            } else {
              incrementPositionsInRange(queue, oldPosition);
              row.position = oldPosition;
            }
            row.upTime = oldPosition === 1 ? statement.upTime + row.elapsedTime : statement.upTime;
            row.elapsedTime = row.elapsedTime + statement.elapsedTime;
            row.onBreak = false;
            statement.elapsedTime = 0;
            statement.position = 0;
            statement.upTime = 0;
            row.prevPosition = -1;
          }
        });
        queue.rows.sort(getQueuesComparator());
      }
    });
    this.handleRowUpdate(queueId);
  }

  handleQueueExpired(id) {
    this.queues.forEach(queue => {
      if (id === queue.id) {
        queue.rows = [];
        queue.isExpired = true;
      }
    });
  }

  handleRemoveFromQueue(event) {
    let rowId = event.detail.id;
    let queueId = event.detail.queueId;
    let tempQueues = [];
    let rowToRemove;
    this.queues.forEach((queue) => {
      if (queueId === queue.id) {
        let tempQueue = formEmptyRowsQueue(queue);
        let oldPosition;
        queue.rows.forEach((row) => {
          if (rowId != row.id) {
            tempQueue.rows.push(row);
          } else {
            oldPosition = row.position;
            rowToRemove = row;
          }
        });
        if (!rowToRemove.onBreak && !rowToRemove.withGuest) {
          decrementPossitions(tempQueue, oldPosition);
        }
        tempQueue.rows.sort(getQueuesComparator());
        this.rebuildTimeTask(tempQueue);
        tempQueues.push(tempQueue);
      } else {
        tempQueues.push(queue);
      }
    });
    this.queues = tempQueues;
    this.dispatchRemove(rowId);
    this.handleRowUpdate(queueId);
  }

  handleUpdate(event) {
    this.updateQueueData(event.detail.queueToUpdate);
    this.updateRowsData(event.detail.rowsToUpdate, event.detail.queueToUpdate.id);
  }

  updateQueueData(queueData) {
    this.queues.forEach(queue => {
      if (queueData.id === queue.id) {
        queue.upTime = queueData.upTime;
        queue.name = queueData.name;
        queue.revertTime = queueData.revertTime;
        queue.upNextTime = queueData.upNextTime;
      }
    });
    updateQueueData({
      queueId: queueData.id,
      queueUpTime: queueData.upTime,
      queueName: queueData.name,
      queueRevertTime: queueData.revertTime,
      queueUpNextTime: queueData.upNextTime,
    });
  }

  updateRowsData(rowsData, queueId) {
    this.queues.forEach(queue => {
      if (queueId === queue.id) {
        queue.rows.forEach(row => {
          rowsData.forEach(updateData => {
            if (row.id === updateData.id) {
              if (!row.onBreak || !row.withGuest) {
                if (row.position === 1) {
                  row.upTime = row.upTime > updateData.upTime ? updateData.upTime : row.upTime;
                }
                row.upTime = updateData.upTime;
              }
              row.inQueueUpTime = updateData.upTime;
            }
          });
        })
      }
    });
    this.handleRowUpdate(queueId);
  }

  rebuildTimeTask(tempQueue) {
    this.timerTasks.forEach((task) => {
      if (tempQueue.id === task.queue.id) {
        task.queue = tempQueue;
      }
    });
  }

  dispatchRemove(id) {
    this.dispatchEvent(
      new CustomEvent("remove", {
        detail: {
          resource: id
        }
      })
    );
  }
}