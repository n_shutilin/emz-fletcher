import { LightningElement, api, track } from 'lwc';

export default class ReferenceFieldTypeComponent extends LightningElement {
    @api recId;
    @api recordName;

    @track finalUrl;

    connectedCallback() {
        this.finalUrl = '/' + this.recId;
    }
}