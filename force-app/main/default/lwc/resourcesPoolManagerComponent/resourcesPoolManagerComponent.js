import { LightningElement, api, track } from "lwc";

export default class ResourcesPoolManagerComponent extends LightningElement {
  
  @api rows;
  @api isEmpty;
  @api displayType;
  @api role;
  @api isLoaded;

  drag(event) {
    let row = this.getRowById(event.currentTarget.dataset.rowid);
    event.dataTransfer.setData("row", JSON.stringify(row));
  }

  getRowById(id) {
    let tempRow;
    this.rows.forEach((row) => {
      if (row.id === id) {
        tempRow = row;
      }
    });
    return tempRow;
  }

  handleToQueue(event){
    let row = this.getRowById(event.currentTarget.dataset.rowid);
    this.dispatchEvent(
      new CustomEvent("toqueue", {
        detail: {
          row: JSON.stringify(row)
         }
      })
    );
  }

  changeType() {
    this.dispatchEvent(
      new CustomEvent("type", {
        detail: {
          type: this.displayType
        }
      })
    );
  }

  sortByUpNext() {
    this.dispatchSort("upNext");
  }

  sortByInQueue() {
    this.dispatchSort("inQueue");
  }

  sortByWithGuest() {
    this.dispatchSort("withGuestTime");
  }

  sortByOnBreak() {
    this.dispatchSort("onBreakTime");
  }

  sortByTotal() {
    this.dispatchSort("totalTime");
  }

  dispatchSort(sortType) {
    this.dispatchEvent(
      new CustomEvent("sort", {
        detail: {
          type: sortType
        }
      })
    );
  }
}