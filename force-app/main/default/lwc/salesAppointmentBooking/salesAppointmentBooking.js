import { LightningElement, api, track } from 'lwc'
import Id from '@salesforce/user/Id'
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import { NavigationMixin } from 'lightning/navigation'
import getUserDetails from '@salesforce/apex/SalesAppointmentBookingController.getUserDetails'
import getSalesAppointmentTypes from '@salesforce/apex/SalesAppointmentBookingController.getSalesAppointmentTypes'
import getCurrentSObjectType from '@salesforce/apex/SalesAppointmentBookingController.getCurrentSObjectType'
import getCurrentRecordInfo from '@salesforce/apex/SalesAppointmentBookingController.getCurrentRecordInfo'
import createServiceAppointment from '@salesforce/apex/SalesAppointmentBookingController.createServiceAppointment'
import getAvailableServiceResource from '@salesforce/apex/SalesAppointmentBookingController.getAvailableServiceResource'
import getDealerTimezoneShift from '@salesforce/apex/SalesAppointmentBookingController.getDealerTimezoneShift'
import getDealerInfo from '@salesforce/apex/SalesAppointmentBookingController.getDealerInfo'
import getDefaultServiceResource from '@salesforce/apex/SalesAppointmentBookingController.getDefaultServiceResource'
import getAppointmentTypeTimeslots from '@salesforce/apex/SalesAppointmentBookingController.getAppointmentTypeTimeslots_v2'
import getAvailableTimeSlots from '@salesforce/apex/SalesAppointmentBookingController.getAvailableTimeSlots_v2'
import updateServiceAppointmentDate from '@salesforce/apex/SalesAppointmentBookingController.updateServiceAppointmentDate_v2'
import scheduleServiceAppointment from '@salesforce/apex/SalesAppointmentBookingController.scheduleServiceAppointment'
import confirmationSalesAppointmentUpdate from '@salesforce/apex/SalesAppointmentBookingController.confirmationSalesAppointmentUpdate'
// import sendConfirmationEmail from '@salesforce/apex/SalesAppointmentBookingController.sendConfirmationEmail'
import deleteAppointment from '@salesforce/apex/SalesAppointmentBookingController.deleteAppointment'
import getSalesAppointmentInfo from '@salesforce/apex/SalesAppointmentBookingController.getSalesAppointmentInfo'

const DAYS_OF_WEEK_MAP = {
    'Sun':'Sunday', 
    'Mon':'Monday', 
    'Tue':'Tuesday', 
    'Wed':'Wednesday', 
    'Thu':'Thursday', 
    'Fri':'Friday', 
    'Sat':'Saturday'
}
const MONTHS_MAP = {
    'Jan':'January', 
    'Feb':'February', 
    'Mar':'March', 
    'Apr':'April', 
    'May':'May', 
    'Jun':'June', 
    'Jul':'July', 
    'Aug':'August', 
    'Sep':'September', 
    'Oct':'October', 
    'Nov':'November', 
    'Dec':'December'
}

export default class SalesAppointmentBooking extends NavigationMixin(LightningElement) {
    @api recordId

    @track isProcess = false
    @track selectedAppointmentType
    @track appointmentTypeOptions = []
    @track salesAppointment = {}
    @track salespersons
    @track saleManagers
    @track finalTimeSlots = []
    @track isConfirmationStep = false
    @track isTemplateReady
    @track relatedAccount = {}
    @track ccEmail
    @track ccEmailList = []
    @track isCreatedSuccessfully = false

    userId = Id
    currentUser
    isUpdate
    serviceResources
    bufferTime
    dealerDatetime
    dealerDate
    opportunityOwnerId
    today = new Date().toISOString().slice(0, 10)
    period = 'All'
    schedOptionsByDateMap
    preferredDate
    scheduledTimeslot
    oldServiceAppointment = {}

    get optionsAmPm() {
        return [
            { label: 'All Day', value: 'All' },
            { label: 'AM', value: 'PM' },
            { label: 'PM', value: 'AM' },
        ];
    }

    get currentUserName() {
        return this.currentUser.Name
    }

    get primaryResourceName() {
        let resource = this.serviceResources.find(selection => selection.Id === this.salesAppointment.PrimaryResource)
        return resource ? resource.Name : 'None'
    }

    get secondaryResourceName() {
        let resource = this.serviceResources.find(selection => selection.Id === this.salesAppointment.SecondaryResource)
        return resource ? resource.Name : 'None'
    }

    get advisorTimslots() {
        return this.finalTimeSlots[this.salesAppointment.PrimaryResource];
    }

    get isTimeslotsPrepared() {
        return !!this.finalTimeSlots[this.salesAppointment.PrimaryResource];
    }

    get disableNextButton() {
        return !this.isConfirmationStep ? !this.scheduledTimeslot : false
    }

    connectedCallback (){
        window.addEventListener('beforeunload', this.beforeUnloadHandler.bind(this));
        this.init()
    }

    renderedCallback(){
        this._handleAMPM()
    }

    async init() {
        this.isProcess = true

        this.currentUser = await getUserDetails({recId: this.userId})
        this.appointmentTypeOptions = await this.getAppointmentTypeOptions()

        this.salesAppointment.SalesAppointmentType = 'Showroom visit'
        this.salesAppointment.PreferredDate = new Date().toISOString().slice(0, 10)
        this.preferredDate = this.salesAppointment.PreferredDate

        let currentRecordSobjectType = await getCurrentSObjectType({recordId: this.recordId})
        if (currentRecordSobjectType == 'Opportunity') {
            this.isUpdate = false;

            let oppotunityInfo = await getCurrentRecordInfo({recordId: this.recordId})
            if (!oppotunityInfo.Dealer_ID__c) {
                this._showToastMessage('Error', 'Dealer not selected on current Opportunity', 'error');
                this.dispatchEvent(new CustomEvent('close'));
            }
            this.salesAppointment.Dealer = oppotunityInfo.Dealer_ID__c;
            this.opportunityOwnerId = oppotunityInfo.OwnerId;
            this.relatedAccount = oppotunityInfo.Account;
            
            this.salesAppointment.Id = await createServiceAppointment({ recordId: this.recordId, dealer: this.salesAppointment.Dealer })
            if (!this.salesAppointment.Id) {
                this._showToastMessage('Error', 'Something goes wrong. Service Appointment was not created', 'error');
                this.dispatchEvent(new CustomEvent('close'));
            }
        } else if (currentRecordSobjectType == 'ServiceAppointment') {
            this.isUpdate = true;
            // Reschedule functionality
            let appointment = JSON.parse(await getSalesAppointmentInfo({ recordId: this.recordId }))

            this.salesAppointment = {...appointment}
            this.oldServiceAppointment = {...appointment}
            this.preferredDate = new Date(appointment.PreferredDate) < new Date(this.today) ? this.today : appointment.PreferredDate
            
            this.scheduledTimeslot = {
                SelectedAdvisor: appointment.PrimaryResource,
                ScheduledDate: appointment.PreferredDate,
                StartTime: appointment.BookingStartTime,
                SalesAppointmentType: appointment.SalesAppointmentType,
                FormattedDate: this._prepareFinalDateString(appointment.PreferredDate, this._formatAMPM(appointment.BookingStartTime))
            }

            let relatedOpportunity = await getCurrentRecordInfo({recordId: this.salesAppointment.ParentId})
            this.relatedAccount = relatedOpportunity.Account

            await updateServiceAppointmentDate({
                appointmentId: this.salesAppointment.Id,
                preferredDate: this.preferredDate
            })
        }

        this.getDealerBufferTime()
        this.setDealerTimeSettings()
        await this.prepareServiceResources()
        if (!this.isUpdate) {
            await this.setDefaultResources()
        }

        try {
            this.weeklyTimeslotMap = await getAppointmentTypeTimeslots({appointmentType: this.salesAppointment.SalesAppointmentType, dealer: this.salesAppointment.Dealer})
        } catch (error) {
            console.log('ERROR', error);
            this.isProcess = false;
            this._showToastMessage('Error', error, 'error');
        }
        // this.weeklyTimeslotMap = await getAppointmentTypeTimeslots({appointmentType: this.salesAppointment.SalesAppointmentType, dealer: this.salesAppointment.Dealer})
        await this.getResourceAvailableTimeslots()
        this.prepareFinalSlots()
        
        // console.log(JSON.stringify(this.finalTimeSlots))
        
        this.isProcess = false
    }

    async getAppointmentTypeOptions() {
        let appointmentTypeOptions = await getSalesAppointmentTypes()
        console.log(appointmentTypeOptions)
        let types = JSON.parse(appointmentTypeOptions)
        let appointmentTypes = []
        for (const type of types) {
            if (type.active) {
                appointmentTypes.push({value: type.value, label: type.label})
            }
        }
        return appointmentTypes
    }

    async prepareServiceResources() {
        this.serviceResources = await getAvailableServiceResource({dealer: this.salesAppointment.Dealer})

        let salespersons = []
        let saleManagers = []

        this.serviceResources.forEach(person => {
            if (person.Sales_Resource_Type__c === 'Salesperson') {
                salespersons.push({value: person.Id, label: person.Name})
            } else if (person.Sales_Resource_Type__c === 'Sale Manager') {
                saleManagers.push({value: person.Id, label: person.Name})
            }
        })

        salespersons.push({value: null, label: 'None'})

        this.salespersons = salespersons
        this.saleManagers = saleManagers
        
        if ( this.saleManagers.length == 0) {
            throw new Error('There is no Sales Persons for this Dealership');
        } else {
            this.salesAppointment.PrimaryResource = this.saleManagers[0].value
        }
    }

    async getDealerBufferTime() {
        let dealer = await getDealerInfo({dealer: this.salesAppointment.Dealer})
        this.bufferTime = dealer.BufferTime__c ? dealer.BufferTime__c : 0;
    }
    
    async setDealerTimeSettings() {
        let dealerTimeshift = await getDealerTimezoneShift({dealer: this.salesAppointment.Dealer})
        this.dealerDatetime = new Date(new Date().getTime() + dealerTimeshift);
        this.dealerDate = this.dealerDatetime.getFullYear() + '-' 
            + (this.dealerDatetime.getMonth().toString().length === 1 ? '0' + (this.dealerDatetime.getMonth() + 1) : (this.dealerDatetime.getMonth() + 1)) + '-' 
            + (this.dealerDatetime.getDate().toString().length === 1 ? '0' + this.dealerDatetime.getDate() : this.dealerDatetime.getDate());
    }

    async setDefaultResources() {
        let defaultServiceResource = await getDefaultServiceResource({userId: this.opportunityOwnerId})
        let defaultResource = defaultServiceResource.length > 0 ? defaultServiceResource[0].Id : null;

        let defSalesId = this.saleManagers.find(defResource => defResource.value === defaultResource);
        this.salesAppointment.PrimaryResource = defSalesId ? defSalesId.value : this.saleManagers[0].value;
        this.salesAppointment.SecondaryResource = null;
    }

    async getResourceAvailableTimeslots() {
        let temp = await getAvailableTimeSlots({
            appointmentId: this.salesAppointment.Id,
            dealer: this.salesAppointment.Dealer,
            appointmentType: this.salesAppointment.SalesAppointmentType
        })

        let scheduleOptionsList = JSON.parse(temp)
        let tempTimeslotsMap = {}

        scheduleOptionsList.forEach(element => {
            tempTimeslotsMap[element.ResourceID] = tempTimeslotsMap[element.ResourceID] || {}
            let dateString = element.StartDate.slice(0,10)
            tempTimeslotsMap[element.ResourceID][dateString] = tempTimeslotsMap[element.ResourceID][dateString] || []
            tempTimeslotsMap[element.ResourceID][dateString].push(element)
        })

        this.schedOptionsByDateMap = tempTimeslotsMap
    }

    prepareFinalSlots() {
        let tempFinalTimeslotsMap = {};
        let datesList = this._createDateList();

        this.saleManagers.forEach( advisor => {
            const slots = this.weeklyTimeslotMap;
            const avSlots = this.schedOptionsByDateMap[advisor.value];
        
            let advisor5DaysTimeslots = [];
            datesList.forEach( date => {
                let dailySlot = {
                    date: new Date(date.dateLabel).toUTCString().slice(0,-13),
                }
                let finalSlots = [];
                let dayOfWeek = DAYS_OF_WEEK_MAP[date.date.toUTCString().slice(0,3)] ;
                
                if(slots && slots[dayOfWeek]) {
                    slots[dayOfWeek].forEach(element => {
                        let tempStartDate = new Date(new Date(date.dateLabel).getTime() + new Date(element.StartTime).getTime())
                        let tempEndDate = new Date(new Date(date.dateLabel).getTime() + (element.EndTime == 0 ? 86400000 : new Date(element.EndTime).getTime()))
                        
                        let isDisabled = true;
                        
                        if(avSlots && avSlots[date.dateLabel]) {
                            for (let el of avSlots[date.dateLabel]) {
                                if (tempStartDate >= new Date(el.StartDate) && tempEndDate <= new Date(el.EndDate)) {
                                    if ((this.dealerDate !== new Date(el.StartDate).toISOString().slice(0,10)) || 
                                        (this.dealerDate === new Date(el.StartDate).toISOString().slice(0,10) && 
                                        tempStartDate >= new Date(new Date(this.dealerDatetime).getTime() + this.bufferTime*60000))) {
                                        isDisabled = false;
                                        break;
                                    }
                                }
                            }
                        }
                        
                        let start = new Date(element.StartTime).toISOString();
                        start = start.slice(11, 16);
                        let end = new Date(element.EndTime).toISOString();
                        end = end.slice(11, 16);

                        finalSlots.push({
                            start: start,
                            end: end,
                            isDisabled: isDisabled,
                            label: this._formatAMPM(start),
                            resId: advisor.value,
                            startTime: element.StartTime,
                            date: date.dateLabel,
                            class: (this.scheduledTimeslot &&
                                this.scheduledTimeslot.ScheduledDate === date.dateLabel &&
                                this.scheduledTimeslot.SelectedAdvisor === advisor.value &&
                                this.scheduledTimeslot.StartTime === start &&
                                this.scheduledTimeslot.SalesAppointmentType === this.salesAppointment.SalesAppointmentType
                            ) ? "slds-button slds-button_brand" : "slds-button",
                            ampm: this._isAMorPM(start),
                            dateString: this._prepareFinalDateString(date.dateLabel, this._formatAMPM(start))
                        });
                    })
                    finalSlots.sort(this._compareSlots);
                }
                dailySlot['slots'] = finalSlots;
                advisor5DaysTimeslots.push(dailySlot);
            })
            tempFinalTimeslotsMap[advisor.value] = advisor5DaysTimeslots
        })
        this.finalTimeSlots = tempFinalTimeslotsMap
    }

    async handleTypeChange(event) {
        console.log('handleTypeChange')
        
        this.isProcess = true
        // this.salesAppointment.SalesAppointmentType = event.detail.value

        try {
            this.weeklyTimeslotMap = await getAppointmentTypeTimeslots({
                    appointmentType: event.detail.value, 
                    dealer: this.salesAppointment.Dealer
                })

                await updateServiceAppointmentDate({
                    appointmentId: this.salesAppointment.Id,
                    preferredDate: this.preferredDate
                })
            
        } catch (error) {
            this.weeklyTimeslotMap = null
            console.log('ERROR', error)
            this._showToastMessage('Error', error.body.message, 'error')
        }

        await this.getResourceAvailableTimeslots()
        this.prepareFinalSlots()
        this._clearTimeslotSelection()
        this.salesAppointment.SalesAppointmentType = event.detail.value
        this.isProcess = false
    }

    handlePrimaryResourceChange(event) {
        console.log('handlePrimaryResourceChange')
        this.isProcess = true
        this.salesAppointment.PrimaryResource = event.detail.value
        this._clearTimeslotSelection()
        this.isProcess = false
    }

    handleSecondaryResourceChange(event) {
        console.log('handleSecondaryResourceChange')
        this.isProcess = true
        this.salesAppointment.SecondaryResource = event.detail.value
        this.isProcess = false
    }

    async handleDateChange(event) {
        console.log('handleDateChange')
        this.isProcess = true
        if(event.detail.value) {
            if(new Date(event.detail.value) >= new Date(this.today)) {
                this.preferredDate = event.detail.value

                await updateServiceAppointmentDate({
                    appointmentId: this.salesAppointment.Id,
                    preferredDate: this.preferredDate
                })

                await this.getResourceAvailableTimeslots()
                this.prepareFinalSlots()
                this.scheduledTimeslot = undefined
            } else {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Wrong Date chosen',
                        message: 'Preferred date cannot be erlier than today',
                        variant: 'error',
                    })
                );
                event.currentTarget.value = this.preferredDate;
            }
        } else {
            event.currentTarget.value = this.preferredDate;
        }
        

        this.isProcess = false
    }

    handleDayPeriodChange(event) {
        this.period =  event.detail.value
        this._clearTimeslotSelection()
        this._handleAMPM()
    }

    handleTimeslotSelect(event) {
        this._clearTimeslotSelection()
        console.log("handleTimeslotSelect")

        let selectedTimeslot = this.advisorTimslots[event.currentTarget.dataset.dayindex].slots[event.currentTarget.dataset.index]
        selectedTimeslot.class = "slds-button slds-button_brand"
        
        this.template.querySelectorAll('button').forEach(element => {
            element.className = 'slds-button'
        })

        event.target.className += " slds-button_brand"

        this.scheduledTimeslot = {
            SelectedAdvisor: selectedTimeslot.resId,
            ScheduledDate: selectedTimeslot.date,
            ScheduledStartTime: selectedTimeslot.start,
            ScheduledEndTime: selectedTimeslot.end,
            StartTime: selectedTimeslot.startTime,
            FormattedDate: selectedTimeslot.dateString,
            SalesAppointmentType: this.selectedAppointmentType
        }

        this.salesAppointment.PreferredDate = selectedTimeslot.date
        this.salesAppointment.BookingStartTime = selectedTimeslot.start
        this.salesAppointment.BookingEndTime = selectedTimeslot.end
    }

    async handleNext() {
        if (!this.isConfirmationStep) {
            this.isProcess = true

            let JSONsApp = JSON.stringify(this.salesAppointment)
            await scheduleServiceAppointment({ salesAppointment: JSONsApp, isRestoring: false })

            this.isConfirmationStep = true
            this.isTemplateReady = true
            this.isProcess = false
        } else {
            console.log('to final step')
            this.finishScheduling()
        }
    }

    handlePrevious() {
        this.isConfirmationStep = false
    }

    handleDoNotSendChange(event) {
        this.salesAppointment.OptOutOfConfirmation = event.target.checked
    }

    handleCCInput(event) {
        this.ccEmail = event.detail.value;
    }

    handleAddCC() {
        if(this._emailIsValid(this.ccEmail)) {
            this.ccEmailList.push(this.ccEmail)
            this.ccEmail = '';
        }
    }

    handleRemoveCC(event) {
        this.ccEmailList.splice(event.currentTarget.dataset.index , 1);
    }

    async finishScheduling() {
        this.isProcess = true;

        await confirmationSalesAppointmentUpdate({
            appointmentId    : this.salesAppointment.Id,
            doNotSendEmail   : this.salesAppointment.OptOutOfConfirmation,
        })
        
        // if(!this.salesAppointment.OptOutOfConfirmation) {
        //     await sendConfirmationEmail({
        //         ccAddresses   : this.ccEmailList,
        //         appointmentId : this.salesAppointment.Id,
        //         contactId     : this.relatedAccount.PersonContactId,
        //         isUpdate      : this.isUpdate
        //     })
        // }
    
        this.isCreatedSuccessfully = true;
        this.isConfirmation = false;
        this.isProcess = false;
    }

    viewRecord() {
        let accountHomePageRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: this.salesAppointment.Id,
                objectApiName: 'ServiceAppointment',
                actionName: 'view'
            }
        }
        this[NavigationMixin.GenerateUrl](accountHomePageRef)
            .then(url => { window.open(url)})
    }

    handleClose() {
        this.dispatchEvent(new CustomEvent('close'))
    }

    disconnectedCallback() {
        window.removeEventListener('beforeunload', this.beforeUnloadHandler)
        if (!this.isCreatedSuccessfully) {
            this.beforeUnloadHandler()
        }
    }

    beforeUnloadHandler() {
        this.isProcess = true;
        if(this.isUpdate && !this.isCreatedSuccessfully){
            let JSONsApp = JSON.stringify(this.oldServiceAppointment);
            console.log(this.oldServiceAppointment)
            scheduleServiceAppointment({
                salesAppointment: JSONsApp,
                isRestoring: true
            })
            .catch(error => {
                this.isProcess = false;
                console.log('ERROR', error);
            });
        }

        if (!this.isUpdate && !this.isCreatedSuccessfully) {
            this.handleCancel()
        }
    }

    handleCancel() {
        if(this.salesAppointment.Id) {
            deleteAppointment({appointmentId: this.salesAppointment.Id})
        }
    }

    _emailIsValid (email) {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
    }

    _createDateList() {
        let dateString = this.preferredDate;
        let firstDate = new Date(dateString);
        let datesList = [{dateLabel: dateString, date: firstDate}];
        
        for ( let i = 1; i < 3; i++ ) {
            let nextDate = new Date(dateString);
            nextDate = new Date(nextDate.setDate(nextDate.getDate() + i));
            let dateLabel = nextDate.toISOString();
            dateLabel = dateLabel.slice(0,10);
            datesList.push({dateLabel: dateLabel, date: nextDate});
        }

        return datesList;
    }

    _prepareFinalDateString(dateString, timeString) {
        let utcDateString = new Date(dateString).toUTCString().slice(0,-13)
        let dayOfWeek = DAYS_OF_WEEK_MAP[utcDateString.slice(0, 3)]
        let day = utcDateString.slice(5,7)
        let month = MONTHS_MAP[utcDateString.slice(8,11)]
        let year = utcDateString.slice(12)

        return dayOfWeek + ", " + month + " " + day + ", " + year + " " + timeString
    }

    _isAMorPM(timeString) {
        let splits = timeString.split(':');
        let hours = splits[0];
        let ampm = hours >= 12 ? 'PM' : 'AM';
        return ampm
    }

    _formatAMPM(timeString) {
        let splits = timeString.split(':');
        let hours = splits[0];
        let minutes = splits[1];
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 && minutes != '00' ? '0'+minutes : minutes;
        let strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    _showToastMessage(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            })
        );
    }

    _handleAMPM(){
        let selector = "button[data-ampm]"
        this.template.querySelectorAll(selector).forEach(element => {
            if (element.getAttribute('data-ampm') != this.period) {
                element.style.display = null
            } else {
                element.style.display = 'none'
            }
        });
    }

    _clearTimeslotSelection() {
        Object.keys(this.finalTimeSlots).forEach(advisorId => {
            let advisor = this.finalTimeSlots[advisorId];
            advisor.forEach(day => {
                day.slots.forEach(slot => {
                    slot.class = "slds-button";
                });
            });
        });

        this.scheduledTimeslot = undefined
    }

    _compareSlots(a, b) {
        if (a.startTime < b.startTime) {
            return -1;
        }
        if (a.startTime > b.startTime) {
            return 1;
        }
        return 0;
    }
}