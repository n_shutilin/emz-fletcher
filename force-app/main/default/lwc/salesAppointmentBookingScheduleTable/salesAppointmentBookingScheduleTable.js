import {LightningElement, api, track} from 'lwc';
import getCurrentRecordInfo from '@salesforce/apex/SalesAppointmentBookingController.getCurrentRecordInfo';
import getCurrentSObjectType from '@salesforce/apex/SalesAppointmentBookingController.getCurrentSObjectType';
import getDealerInfo from '@salesforce/apex/SalesAppointmentBookingController.getDealerInfo';
import getDealerTimezoneShift from '@salesforce/apex/SalesAppointmentBookingController.getDealerTimezoneShift';
import getSalesAppointmentInfo from '@salesforce/apex/SalesAppointmentBookingController.getSalesAppointmentInfo';
import getSalesAppointmentTypes from '@salesforce/apex/SalesAppointmentBookingController.getSalesAppointmentTypes';
import getDefaultServiceResource from '@salesforce/apex/SalesAppointmentBookingController.getDefaultServiceResource';
import getAvailableServiceResource from '@salesforce/apex/SalesAppointmentBookingController.getAvailableServiceResource';
import createServiceAppointment from '@salesforce/apex/SalesAppointmentBookingController.createServiceAppointment';
import getAppointmentTypeTimeslots from '@salesforce/apex/SalesAppointmentBookingController.getAppointmentTypeTimeslots';
import getAvailableTimeSlots from '@salesforce/apex/SalesAppointmentBookingController.getAvailableTimeSlots';
import updateServiceAppointmentDate from '@salesforce/apex/SalesAppointmentBookingController.updateServiceAppointmentDate';
import scheduleServiceAppointment from '@salesforce/apex/SalesAppointmentBookingController.scheduleServiceAppointment';
import deleteAppointment from '@salesforce/apex/SalesAppointmentBookingController.deleteAppointment';
import getUserDetails from '@salesforce/apex/SalesAppointmentBookingController.getUserDetails';
// import getEmailTemplate from '@salesforce/apex/SalesAppointmentBookingController.getEmailTemplate';
import confirmationSalesAppointmentUpdate from '@salesforce/apex/SalesAppointmentBookingController.confirmationSalesAppointmentUpdate';
// import sendConfirmationEmail from '@salesforce/apex/SalesAppointmentBookingController.sendConfirmationEmail';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import Id from '@salesforce/user/Id';
import { NavigationMixin }      from 'lightning/navigation';

const DAY_OF_WEEK_FULL = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const DAY_OF_WEEK_ABREVIATION = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

export default class SalesAppointmentBookingScheduleTable extends NavigationMixin(LightningElement) {
    @api recordId;
    userId = Id;
    currentUser;
    @track isProcess;
    @track isConfirmation = false;
    isUpdate;
    oldServiceAppointment;
    dealer;
    opportunityOwnerId;
    @track salesAppointment = {}
    @track isAppointmentSelected = false;
    @track appointmentTypes = [];
    @track salespersons;
    @track saleManagers;
    @track weeklyTimeslotMap;
    @track schedOptionsByDateMap;
    @track today;
    @track lastDate;
    @track todayAllFormat;
    @track lastDateAllFormat;
    @track slotDates;
    persons;
    @track preparedAppointment;
    @track isTemplateReady = false;
    @track createdSuccessfully = false;
    originalTemplateHTML;
    @track templateHTML;
    @track comment = '';
    @track relatedAccountEmail;
    @track relatedAccountName;
    @track doNotSendEmail;
    @track ccEmailList = [];
    @track ccEmail;
    dealerTimezoneShift;
    dealerDate;
    scheduledTimeslot;

    get isTypeSelected() {
        return !this.salesAppointment.SalesAppointmentType;
    }

    get showPickupSection() {
        return this.salesAppointment.SalesAppointmentType === "Pickup & Delivery";
    }

    get isTimeslotNotSelected() {
        return !this.salesAppointment.PreferredDate && !this.salesAppointment.BookingStartTime && !this.salesAppointment.BookingEndTime;
    }

    get isPreviousDateDisabled() {
        return this.today == new Date().toLocaleDateString();
    }
    
    connectedCallback() {
        window.addEventListener('beforeunload', this.beforeUnloadHandler.bind(this));
        
        this.isProcess = true;

        getUserDetails({recId: this.userId})
            .then(result => {
                this.currentUser = result;
            })
        .then(() => getSalesAppointmentTypes())
            .then(result => {
                let types = JSON.parse(result);
                let appointmentTypes = [];
                for (const type of types) {
                    if (type.active) {
                        appointmentTypes.push({value: type.value, label: type.label});
                    }
                }
                this.appointmentTypes = appointmentTypes;
                
            })
        .then(() => getCurrentSObjectType({recordId: this.recordId}))
            .then(result => {
                if(result === 'Opportunity') {
                    this.isUpdate = false;

                    getCurrentRecordInfo({recordId: this.recordId})
                        .then(result => {
                            this.dealer = result.Dealer_ID__c;
                            this.opportunityOwnerId = result.OwnerId;
                            this.relatedAccountEmail = result.Account.PersonEmail;
                            this.relatedAccountPersonId = result.Account.PersonContactId;
                            this.salesAppointment.EmailRecepientsName = result.Account.FirstName ? result.Account.FirstName : result.Account.Name;

                            if (!this.dealer) {
                                //throw new Error('Dealer not selected on current Opportunity');
                                this._showToastMessage('Error', 'Dealer not selected on current Opportunity', 'error');
                                this.dispatchEvent(new CustomEvent('close'));
                            }
                        })
                    .then(() => createServiceAppointment({
                        recordId: this.recordId,
                        dealer: this.dealer
                    }))
                        .then(result => {
                            this.salesAppointment.Id = result;
                            this.salesAppointment.Dealer = this.dealer;
                            this.salesAppointment.OptOutOfConfirmation = false;
                            if (!this.salesAppointment.Id) {
                                // throw new Error('Something goes wrong. Service Appointment was not created');
                                this._showToastMessage('Error', 'Something goes wrong. Service Appointment was not created', 'error');
                                this.dispatchEvent(new CustomEvent('close'));
                            }
                        })
                    .then(() => {
                        console.log('123123')
                        // this.template.activeElement.blur();
                        this.prepareComponent();
                    })
                } else if (result === 'ServiceAppointment') {
                    this.isUpdate = true;

                    getSalesAppointmentInfo({ recordId: this.recordId })
                        .then(result => {
                            result = JSON.parse(result);
                            this.oldServiceAppointment = {...result};
                            console.log(JSON.stringify(this.oldServiceAppointment))

                            this.dealer = result.Dealer;

                            this.salesAppointment.Id = result.Id;
                            this.salesAppointment.Dealer = result.Dealer;
                            this.salesAppointment.ParentId = result.ParentId;
                            this.salesAppointment.SalesAppointmentType = result.SalesAppointmentType;
                            
                            this.salesAppointment.PrimaryResource = result.PrimaryResource;
                            this.salesAppointment.SecondaryResource = result.SecondaryResource;
                            this.salesAppointment.PreferredDate = result.PreferredDate;
                            this.salesAppointment.BookingStartTime = result.BookingStartTime;
                            this.salesAppointment.BookingEndTime = result.BookingEndTime;

                            this.salesAppointment.PickupStreet = result.PickupStreet;
                            this.salesAppointment.PickupCity = result.PickupCity;
                            this.salesAppointment.PickupState = result.PickupState;
                            this.salesAppointment.PickupZIP = result.PickupZIP;
                            this.salesAppointment.PickupAddressComments = result.PickupAddressComments;
                            this.salesAppointment.OptOutOfConfirmation = false;

                            this.scheduledTimeslot = {
                                PrimaryResource: result.PrimaryResource,
                                StartTime: new Date(result.PreferredDate + ' ' + result.BookingStartTime).getTime()
                            };

                            console.log(JSON.stringify(this.scheduledTimeslot))
                        })
                    .then(() => getCurrentRecordInfo({recordId: this.salesAppointment.ParentId}))
                        .then(result => {
                            this.relatedAccountEmail = result.Account.PersonEmail;
                            this.relatedAccountPersonId = result.Account.PersonContactId;
                            this.salesAppointment.EmailRecepientsName = result.Account.FirstName ? result.Account.FirstName : result.Account.Name;
                        })
                    .then(() => {
                        if (this.template.activeElement) this.template.activeElement.blur();
                        this.prepareComponent();
                    })
                }
            })
        .catch(error => {
            console.log('ERROR', error);
            this._showToastMessage('Error', error.message, 'error');
            this.dispatchEvent(new CustomEvent('close'));
        })
    }

    prepareComponent = () => {
        getAvailableServiceResource({dealer: this.dealer})
            .then(result => {
                let persons = result;
                this.persons = persons;
                
                let salespersons = [];
                let saleManagers = [];
                for (const person of persons) {
                    if (person.Sales_Resource_Type__c === 'Salesperson') {
                        salespersons.push({value: person.Id, label: person.Name});
                    } else if (person.Sales_Resource_Type__c === 'Sale Manager') {
                        saleManagers.push({value: person.Id, label: person.Name});
                    }
                }

                saleManagers.push({value: null, label: 'None'});

                this.salespersons = salespersons;
                this.saleManagers = saleManagers;

                if ( this.salespersons.length == 0) {
                    throw new Error('There is no Sales Persons for this Dealership');
                }
            })
        .then(() => getDealerInfo({dealer: this.dealer}))
            .then(result => {
                this.bufferTime = result.BufferTime__c ? result.BufferTime__c : 0;
            })
        .then(() => getDealerTimezoneShift({dealer: this.dealer}))
            .then(result => {
                let dealerDatetime = new Date(new Date().getTime() + result);
                this.dealerTimezoneShift = dealerDatetime;
                this.dealerDate = dealerDatetime.getFullYear() + '-' 
                    + (dealerDatetime.getMonth().toString().length === 1 ? '0' + (dealerDatetime.getMonth() + 1) : (dealerDatetime.getMonth() + 1)) + '-' 
                    + (dealerDatetime.getDate().toString().length === 1 ? '0' + (dealerDatetime.getDate() + 1) : dealerDatetime.getDate());
            })
        .then(() => {
            if(!this.isUpdate){
                getDefaultServiceResource({userId: this.opportunityOwnerId})
                    .then(result => {
                        let defaultResource = result.length > 0 ? result[0].Id : null;

                        let defSalesId = this.salespersons.find(defResource => defResource.value === defaultResource);
                        this.salesAppointment.PrimaryResource = defSalesId ? defSalesId.value : this.salespersons[0].value;
                        this.salesAppointment.SecondaryResource = null;
                    })
            }
        })
        .catch(error => {
            console.log('ERROR', error);
            this._showToastMessage('Error', error, 'error');
            this.dispatchEvent(new CustomEvent('close'));
        })
        .finally(() => {
            this.isProcess = false;
        })
    }

    handleAppType = (event) => {
        this.salesAppointment.SalesAppointmentType = event.currentTarget.value;
    }

    handlePickupAddressChange(event) {
        let addressFieldAPIName = event.currentTarget.dataset.apiname;
        this.salesAppointment[addressFieldAPIName] = event.target.value;
    }

    goToTypePage() {
        this.isAppointmentSelected = false;
    }

    getAvailabilityCalendar = (event) => {
        if (this.salesAppointment.SalesAppointmentType == 'Pickup & Delivery' && !this.salesAppointment.PickupStreet && !this.salesAppointment.PickupCity
                && !this.salesAppointment.PickupState && !this.salesAppointment.PickupZIP) {
            this._showToastMessage('Please, fill in all Address fields', 'Please, fill in all Address fields', 'error');        
        } else {
            this.isProcess = true;
            getAppointmentTypeTimeslots({
                appointmentType: this.salesAppointment.SalesAppointmentType,
                dealer: this.dealer
            })
                .then(result => {
                    this.weeklyTimeslotMap = JSON.parse(result);
                    this.handleTodayClick();
                })
            .catch(error => {
                console.log('ERROR', error);
                let message = error.body.message ? error.body.message : error;
                this._showToastMessage('Error', error, 'error');
                this.isProcess = false;
                this.dispatchEvent(new CustomEvent('close'));
            })
            .finally(() => {
                this.isAppointmentSelected = true;
            })
        }
    }

    handleTodayClick() {
        this.isProcess = true;
        let date = new Date();
        this.todayAllFormat = date;
        this.today = date.toLocaleDateString();
        let lastDate = new Date();
        lastDate.setDate(date.getDate() + 3);
        this.lastDateAllFormat = lastDate;
        this.lastDate = lastDate.toLocaleDateString();

        // this.clearSelectedTimeslot();
        
        this.updateAvailableTimeslots();
    }

    handleNextDateClick() {
        this.isProcess = true;
        let newStartDate = this.lastDateAllFormat;
        newStartDate.setDate(newStartDate.getDate() + 1);
        this.todayAllFormat = new Date(newStartDate.getTime());
        this.today = newStartDate.toLocaleDateString();
        let newLastDate = newStartDate;
        newLastDate.setDate(newStartDate.getDate() + 3);
        this.lastDateAllFormat = new Date(newLastDate.getTime());
        this.lastDate = newLastDate.toLocaleDateString();

        // this.clearSelectedTimeslot();
        
        this.updateAvailableTimeslots();
    }

    handlePreviousDateClick() {
        this.isProcess = true;
        let newLastDate = this.todayAllFormat;
        newLastDate.setDate(newLastDate.getDate() - 1);
        this.lastDateAllFormat = new Date(newLastDate.getTime());
        this.lastDate = newLastDate.toLocaleDateString();
        let newStartDate = newLastDate;
        newStartDate.setDate(newStartDate.getDate() - 3);
        this.todayAllFormat = new Date(newStartDate.getTime());
        this.today = newStartDate.toLocaleDateString();

        // this.clearSelectedTimeslot();

        this.updateAvailableTimeslots();
    }

    updateAvailableTimeslots() {
        console.log(this.salesAppointment.Id)
        console.log(this.today)
        console.log(this.lastDate)
        updateServiceAppointmentDate({
            appointmentId: this.salesAppointment.Id,
            startDate: this.today,
            endDate: this.lastDate
        })
        .then(() => getAvailableTimeSlots({
            appointmentId: this.salesAppointment.Id,
            resourceId: this.salesAppointment.PrimaryResource,
            dealer: this.dealer,
            appointmentType: this.salesAppointment.SalesAppointmentType
        }))
            .then(result => {
                this._parseAvailableTimeSlots(result)
            })
        .catch(error => {
            console.log('ERROR', error);
        })
        .finally(() => {
            this.setSlotDateWeek(new Date(this.todayAllFormat.getTime()));
        })
    }

    _parseAvailableTimeSlots = (result) => {
        let scheduleOptionsList = JSON.parse(result);
        this.schedOptionsByDateMap = {};

        scheduleOptionsList.forEach(element => {
            let date = element.StartDate.slice(0,10);

            this.schedOptionsByDateMap[date] = this.schedOptionsByDateMap[date] || [];
            this.schedOptionsByDateMap[date].push(element);
        });
    }

    setSlotDateWeek(startDate) {
        let dates = [];
        for (let i = 0; i < 4; i++) {
            let date = new Date(startDate.getTime());
            date.setDate(date.getDate() + i);
            let dayIndex = date.getDay();
            let slots = this.prepareTimeslots(date);
            dates.push({
                dateLabel: DAY_OF_WEEK_ABREVIATION[dayIndex] + ' ' + date.getDate(), 
                slots: slots
            });
        }
        this.slotDates = dates;
        console.log('dates');
        console.log(this.slotDates);
        this.isProcess = false;
    }

    prepareTimeslots(date) {
        console.log('---------------------------------');
        let dayIndex = date.getDay();
        let formatedDate = this.formatDate(date);
        console.log(formatedDate);
        const slots = this.weeklyTimeslotMap[DAY_OF_WEEK_FULL[dayIndex]];
        const avSlots = this.schedOptionsByDateMap[formatedDate];

        let finalSlots = [];

        if(slots) {
            slots.forEach(element => {
                let tempStartDateString = formatedDate + ' ' + element.StartTime;
                let tempStartDate = new Date(tempStartDateString);
                
                let tempEndDateString = formatedDate + ' ' + element.EndTime;
                let tempEndDate = new Date(tempEndDateString);

                // let tempStartDate = new Date(new Date(formatedDate).getTime() + new Date(element.StartTime.slice(0,5)).getTime())
                // console.log(tempStartDate);
                // let tempEndDate = new Date(new Date(formatedDate).getTime() + new Date(element.EndTime.slice(0,5)).getTime())
                // console.log(tempEndDate);

                let isDisabled = true;

                if(avSlots) {
                    for (let el of avSlots) {
                        // console.log('DOSTUPNIY TIMESLOT');
                        // console.log(JSON.stringify(el));
                        // console.log(new Date(el.StartDate.replace('T',' ').slice(0, -1)));
                        // console.log(new Date(el.EndDate));
                        if(tempStartDate >= new Date(el.StartDate) && tempEndDate <= new Date(el.EndDate)) {
                            console.log('~~~~~~~~~');
                            if ((this.dealerDate !== new Date(el.StartDate).toISOString().slice(0,10)) || (this.dealerDate === new Date(el.StartDate).toISOString().slice(0,10) && tempStartDate >= new Date(new Date(this.dealerTimezoneShift).getTime() + this.bufferTime*60000))) {
                                isDisabled = false;
                                break;
                            }
                        }
                    }
                }
                
                let start = element.StartTime.slice(0,5);
                let end = element.EndTime.slice(0,5);
                let slotDate = date.toISOString().slice(0,10);

                finalSlots.push({
                    start: start,
                    end: end,
                    isDisabled: isDisabled,
                    label: this.formatAMPM(start) + ' - ' + this.formatAMPM(end),
                    date: slotDate,
                    dateTime: new Date(slotDate + ' ' + start).getTime(),
                    resId: this.salesAppointment.PrimaryResource,
                    class: (this.scheduledTimeslot &&
                        this.scheduledTimeslot.PrimaryResource === this.salesAppointment.PrimaryResource &&
                        this.scheduledTimeslot.StartTime === new Date(slotDate + ' ' + start).getTime()
                    ) ? "slds-button slds-button_brand" : "slds-button"
                });
                
            });
            finalSlots.sort(this.compareSlots);
        } 
        
        return finalSlots;
    }

    handlePrimaryResourceChange(event) {
        this.isProcess = true;
        this.salesAppointment.PrimaryResource = event.detail.value;

        // this.clearSelectedTimeslot();

        getAvailableTimeSlots({
            appointmentId: this.salesAppointment.Id,
            resourceId: this.salesAppointment.PrimaryResource,
            dealer: this.dealer,
            appointmentType: this.salesAppointment.SalesAppointmentType
        })
            .then(result => {
                this._parseAvailableTimeSlots(result)
            })
        .catch(error => {
            console.log('ERROR', error);
        })
        .finally(() => {
            this.updateAvailableTimeslots();
        })
    }

    handleSecondaryResourceChange(event) {
        this.salesAppointment.SecondaryResource = event.detail.value;
    }

    formatDate(initialDate) {
        let dd = initialDate.getDate();
        let mm = initialDate.getMonth()+1; 
        let yyyy = initialDate.getFullYear();
        if(dd<10) {
            dd='0'+dd;
        } 

        if(mm<10) {
            mm='0'+mm;
        } 
        let formatedDate = yyyy + '-' + mm + '-' + dd;
        return formatedDate;
    }

    compareSlots(a, b) {
        if (a.dateTime < b.dateTime) {
            return -1;
        }
        if (a.dateTime > b.dateTime) {
            return 1;
        }
        return 0;
    }

    formatAMPM(timeString) {
        let splits = timeString.split(':');
        let hours = splits[0];
        let minutes = splits[1];
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 && minutes != '00' ? '0'+minutes : minutes;
        let strTime = hours + ':' + minutes + ' ' + ampm;

        return strTime;
    }

    handleTimeslotSelect(event) {
        this.clearTimeslotSelection();
        let selectedDate = this.slotDates[event.currentTarget.dataset.date];
        let selectedTimeslot = selectedDate.slots[event.currentTarget.dataset.index];

        selectedTimeslot.class = "slds-button slds-button_brand";
        
        this.template.querySelectorAll('button[data-date]').forEach(element => {
            element.className = 'slds-button';
        });

        event.target.className += " slds-button_brand";

        this.scheduledTimeslot = {
            PrimaryResource: selectedTimeslot.resId,
            // ScheduledDate: result.PreferredDate,
            // BookingStartTime: result.BookingStartTime,
            // BookingEndTime: result.BookingEndTime,
            StartTime: selectedTimeslot.dateTime
        };
        
        // this.template.querySelectorAll('lightning-button').forEach(element => {
        //     element.variant = 'base';
        // });

        // event.target.variant = 'brand';

        this.salesAppointment.PreferredDate = selectedTimeslot.date;
        this.salesAppointment.BookingStartTime = selectedTimeslot.start;
        this.salesAppointment.BookingEndTime = selectedTimeslot.end;
    }

    clearTimeslotSelection() {
        this.slotDates.forEach(daySlots => {
            daySlots.slots.forEach(slot => {
                slot.class = "slds-button";
            });
        });
    }

    goToConfirmScheduling() {
        this.isProcess = true;

        let resource = this.persons.find(selection => selection.Id === this.salesAppointment.PrimaryResource);
        let manager = this.persons.find(selection => selection.Id === this.salesAppointment.SecondaryResource);
        
        let scheduledAppointment = {
            username: this.currentUser.Name,
            servAppointmentType: this.salesAppointment.SalesAppointmentType,
            selectedResource: resource ? resource.Name : null,
            selectedManager: manager ? manager.Name : null,
            appointmentDate: this.salesAppointment.PreferredDate + ' ' + this.salesAppointment.BookingStartTime,
            address: this.salesAppointment.SalesAppointmentType == 'Pickup & Delivery'
                    ? this.salesAppointment.PickupStreet + ', ' + this.salesAppointment.PickupCity + ', ' + 
                        this.salesAppointment.PickupState + ', ' + this.salesAppointment.PickupZIP 
                    : null,
        };
        this.preparedAppointment = scheduledAppointment;

        let JSONsApp = JSON.stringify(this.salesAppointment); 
        console.log(JSONsApp);
        

        scheduleServiceAppointment({
            salesAppointment: JSONsApp,
            isRestoring: false
        })
        // .then(() => getEmailTemplate({
        //     appointmentId: this.salesAppointment.Id,
        //     isUpdate: this.isUpdate
        // }))
        //     .then(result => {
        //         if (result) {
        //             let tempResult      = result;
        //             let cdataOpenIndex  = tempResult.indexOf('<![CDATA[');
        //             let cdataCloseIndex = tempResult.indexOf(']]>');
        //             tempResult          = tempResult.substring(cdataOpenIndex + 9, cdataCloseIndex);
        //             this.templateHTML   = tempResult;

        //             //this.updateTemplate();

        //             this.isTemplateReady = true;
        //         }
        //     })
        .then(() => {
            this.isTemplateReady = true;
            this.isProcess = false;
            this.isConfirmation = true;
        })
        .catch(error => {
            console.log('ERROR', error);
            this.isProcess = false;
            this._showToastMessage('Error', 'Sales Appointment not scheduled: ' + error, 'error');
        })
    }

    handleDoNotSendChange(event) {
        this.salesAppointment.OptOutOfConfirmation = event.target.checked;
    }

    // updateTemplate() {
    //     let tempTemplate = this.originalTemplateHTML;
    //     tempTemplate = tempTemplate.replace('{!ServiceAppointment.Email_Template_Additional_Comments__c}', this.comment);
    //     this.templateHTML = tempTemplate;

    // }

    handleCCInput(event) {
        this.ccEmail = event.detail.value;
    }

    handleAddCC() {
        if(this.emailIsValid(this.ccEmail)) {
            this.ccEmailList.push(this.ccEmail)
            this.ccEmail = '';
        }
    }

    handleRemoveCC(event) {
        this.ccEmailList.splice(event.currentTarget.dataset.index , 1);
    }

    emailIsValid (email) {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
    }

    goToTimeslots() {
        this.isConfirmation = false;
    }

    finishScheduling() {
        this.isProcess = true;

        confirmationSalesAppointmentUpdate({
            appointmentId    : this.salesAppointment.Id,
            doNotSendEmail   : this.salesAppointment.OptOutOfConfirmation,
        })
        // .then(() => {
        //     if(!this.salesAppointment.OptOutOfConfirmation) {
        //         sendConfirmationEmail({
        //             ccAddresses   : this.ccEmailList,
        //             appointmentId : this.salesAppointment.Id,
        //             contactId     : this.relatedAccountPersonId,
        //             isUpdate      : this.isUpdate
        //         })
        //     }
        // })
        .then(() => {
            this.createdSuccessfully = true;
            this.isConfirmation = false;
            this.isProcess = false;
        })
        .catch(error => {
            console.log('ERROR', error);
            this.createdSuccessfully = false;
            this.isConfirmation = true;
            this.isProcess = false;
            this._showToastMessage('Error', 'Sales Appointment not scheduled: ' + error, 'error');
        })
    }

    // clearSelectedTimeslot() {
    //     this.template.querySelectorAll('lightning-button').forEach(element => {
    //         element.variant = 'base';
    //     });

    //     this.salesAppointment.PreferredDate = null;
    //     this.salesAppointment.BookingStartTime = null;
    //     this.salesAppointment.BookingEndTime = null;
    // }

    viewRecord() {
        this.accountHomePageRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: this.salesAppointment.Id,
                objectApiName: 'ServiceAppointment',
                actionName: 'view'
            }
        };
        this[NavigationMixin.GenerateUrl](this.accountHomePageRef)
            .then(url => { window.open(url) });
    }

    handleClose() {
        console.log('handleClose');
        this.dispatchEvent(new CustomEvent('close'));
    }

    // disconnectedCallback() {
    //     if (!this.createdSuccessfully && this.salesAppointment.Id && this.isUpdate) {
    //         deleteAppointment({appointmentId: this.salesAppointment.Id});
    //     }
    // }

    disconnectedCallback() {
        console.log('disconnectedCallback');
        // this.beforeUnloadHandler();
        window.removeEventListener('beforeunload', this.beforeUnloadHandler);
        if (!this.createdSuccessfully) {
            this.beforeUnloadHandler();
        }
    }

    beforeUnloadHandler() {
        this.isProcess = true;
        console.log('beforeUnloadHandler')
        console.log(this.isUpdate)
        console.log('!this.createdSuccessfully')
        console.log(!this.createdSuccessfully)
        if(this.isUpdate && !this.createdSuccessfully){
            console.log('reload')
            let JSONsApp = JSON.stringify(this.oldServiceAppointment);
            console.log(this.oldServiceAppointment)
            scheduleServiceAppointment({
                salesAppointment: JSONsApp,
                isRestoring: true
            })
            .catch(error => {
                this.isProcess = false;
                console.log('ERROR', error);
            });
        }

        if (!this.isUpdate && !this.createdSuccessfully) {
            this.handleCancel();
        }
    }

    handleCancel() {
        console.log('handleCancel')

        if(this.salesAppointment.Id) {
            
            deleteAppointment({appointmentId: this.salesAppointment.Id});
        }
    }

    _showToastMessage(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            })
        );
    }
}