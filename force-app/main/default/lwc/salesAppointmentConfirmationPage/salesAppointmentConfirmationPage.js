import { LightningElement, api, track } from 'lwc';
// import getEmailTemplate from '@salesforce/apex/AppointmentBookingController.getEmailTemplate';

export default class SalesAppointmentConfirmationPage extends LightningElement {
    @api 
    get salesAppointment() {
        return this._salesAppointment;
    }
    set salesAppointment(value) {
        if(value) {
            this._salesAppointment = value;
            if (this.isTemplateReady) {
                this.updateTemplate();
                this.toEmailList = this.getEmailRecepient();
            }
            this.isReady = true;
        }
    }
    _salesAppointment;

    @track isReady = false;
    @track isTemplateReady = true;
    
    originalHTMLbody
    originalTemplateHTML;
    @track templateHTML;

    @track doNotSendEmail = false;
    @track toEmailList = [];
    @track ccEmailList = [];
    @track ccEmail;
    @track comment = '';

    emailTemplate;

    get toEmails() {
        return this.toEmailList.join(', ');
    }

    connectedCallback() {
        // getEmailTemplate()
        //     .then(result => {
        //         if (result) {
        //             this.originalHTMLbody     = result;
        //             let tempResult            = result;
        //             let cdataOpenIndex        = tempResult.indexOf('<![CDATA[');
        //             let cdataCloseIndex       = tempResult.indexOf(']]>');
        //             tempResult                = tempResult.substring(cdataOpenIndex + 9, cdataCloseIndex);
        //             this.originalTemplateHTML = tempResult;
        //         } else {
        //             this.isTemplateReady = false;
        //         }
        //     })
        // .catch(error => {
        //     console.log('ERROR', error)
        // });
    }

    getEmailRecepient() {
        return (this.salesAppointment.OwnerId != this.salesAppointment.AuthorizeUserId) && this.salesAppointment.AuthorizeUserEmail
            ? [this.salesAppointment.OwnerEmail, this.salesAppointment.AuthorizeUserEmail] 
            : [this.salesAppointment.OwnerEmail];
    }

    updateTemplate() {
        let tempTemplate = this.originalTemplateHTML;
        let recepients = (this.salesAppointment.OwnerId != this.salesAppointment.AuthorizeUserId) && this.salesAppointment.AuthorizeUserFirstName
                        ? this.salesAppointment.OwnerFirstName + ' & ' + this.salesAppointment.AuthorizeUserFirstName
                        : this.salesAppointment.OwnerFirstName;
        tempTemplate = tempTemplate.replace('{!Account.FirstName}', recepients);
        tempTemplate = tempTemplate.replace('{!salesAppointment.SchedStartTime}', this.salesAppointment.AppointmentDate);
        tempTemplate = tempTemplate.replace('{!salesAppointment.EmailAdditionalComments__c}', this.comment);
        this.templateHTML = tempTemplate;

        this.prepareEmailData();
    }
    
    prepareEmailData() {
        let doNotSendEmail  = this.doNotSendEmail;
        let toAddresses     = this.toEmailList;
        let ccAddresses     = this.ccEmailList;
        let htmlBody        = this.prepareHTMLBody();

        this.dispatchEvent(
            new CustomEvent('emailprepared', {
                detail: {
                    doNotSendEmail  : doNotSendEmail,
                    toAddresses     : toAddresses,
                    ccAddresses     : ccAddresses,
                    htmlBody        : htmlBody
                }
            })
        );
    }

    prepareHTMLBody() {
        let tempResult      = this.originalHTMLbody;
        let cdataOpenIndex  = tempResult.indexOf('<![CDATA[');
        let cdataCloseIndex = tempResult.indexOf(']]>');
        tempResult          = tempResult.replace(tempResult.substring(cdataOpenIndex, cdataCloseIndex + 3), this.templateHTML);

        return tempResult;
    }
    
    handleCheckboxCheck(event) {
        this.doNotSendEmail = event.target.checked;
        this.prepareEmailData();
    }

    handleCCInput(event) {
        this.ccEmail = event.detail.value;
    }

    handleAddCC() {
        if(this.emailIsValid(this.ccEmail)) {
            this.ccEmailList.push(this.ccEmail)
            this.ccEmail = '';
        }
    }

    handleRemoveCC(event) {
        this.ccEmailList.splice(event.currentTarget.dataset.index , 1);
    }

    handleCommentInput(event) {
        this.comment = event.detail.value;
        this.updateTemplate();
    }

    emailIsValid (email) {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
    }
}