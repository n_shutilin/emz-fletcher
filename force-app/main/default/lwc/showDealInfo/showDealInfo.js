import {LightningElement, api, track, wire} from 'lwc';
import getShownDealInfo from '@salesforce/apex/ShowDealInfoController.getShownDealInfo';

export default class ShowDealInfo extends LightningElement {

    @api recordId;
    @api dealInfo;

    connectedCallback() {
        getShownDealInfo({
            oppId: this.recordId
        })
            .then(result => {
                this.dealInfo = JSON.parse(result);
            })
            .catch(error => {
                console.log('ERROR', JSON.stringify(error));
            });
    }
}