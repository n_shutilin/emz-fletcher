import {LightningElement, wire, api, track} from 'lwc';
import {subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled} from 'lightning/empApi';
import {getRecord} from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';
import ALL_DEALER_FIELD from '@salesforce/schema/User.All_Dealer_Access__c';
import {getPicklistValues} from 'lightning/uiObjectInfoApi';
import {getObjectInfo} from 'lightning/uiObjectInfoApi';
import IN_SHOWROOM_OBJECT from '@salesforce/schema/In_Showroom__c';
import SHOWROOM_STATUS_FIELD from '@salesforce/schema/Opportunity.StageName';
import SHOWROOM_DEALER_FIELD from '@salesforce/schema/Opportunity.Dealer_ID__c';
import VEHICLE_OBJECT from '@salesforce/schema/Vehicle__c';
import VEHICLE_MAKE_FIELD from '@salesforce/schema/Vehicle__c.Make__c';

import getShowrooms from '@salesforce/apex/ShowroomTabController.getShowroomList';
import getTeams from '@salesforce/apex/ShowroomTabController.getTeamsForShowing';
import getLastActivityBy from '@salesforce/apex/ShowroomTabController.getUsers';
import getYears from '@salesforce/apex/ShowroomTabController.getYearList';
import getSortableTable from '@salesforce/apex/ShowroomTabController.sortTable';
import linkOpportunity from '@salesforce/apex/ShowroomTabController.addLinkOpportunity';

const BRAND = 'brand';
const BRAND_OUTLINE = 'brand-outline';

const columns = [
    {
        label: 'Name',
        fieldName: 'showroomLink',
        type: 'url',
        wrapText: true,
        sortable: true,
        typeAttributes: {label: {fieldName: 'name'}, tooltip: 'Name', target: '_blank'}
    },
    {label: 'Dealer ID', fieldName: 'dealerId', wrapText: true},
    {
        label: 'Type', type: 'button-icon', typeAttributes: {
            iconName: {fieldName: 'type'}, iconClass: 'slds-icon_small', variant: "bare"
        }
    },
    {
        label: 'Arrival Time', fieldName: 'arrivalTime', type: 'date', typeAttributes: {
            year: "numeric",
            month: "2-digit",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit",
            timeZone: "America/Los_Angeles"
        }, wrapText: true, sortable: true
    },
    {
        label: 'Opportunity Name',
        fieldName: 'opportunityLink',
        type: 'url',
        wrapText: true,
        sortable: true,
        typeAttributes: {label: {fieldName: 'opportunityName'}, tooltip: 'Name', target: '_blank'}
    },
    {label: 'Showroom Status', fieldName: 'showroomStatus', wrapText: true, sortable: true},
    {label: 'Appt', fieldName: 'appt', type: 'boolean', wrapText: true, sortable: true},
    {label: 'Salesperson', fieldName: 'salesperson', wrapText: true, sortable: true},
    {label: 'Spotter', fieldName: 'spotter', wrapText: true, sortable: true},
    {
        label: 'Last Activity', fieldName: 'lastActivity', type: 'date', typeAttributes: {
            year: "numeric",
            month: "2-digit",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit",
            timeZone: "America/Los_Angeles"
        }, wrapText: true, sortable: true
    },
    {label: 'Description', fieldName: 'descriptionStr', wrapText: true},
    {label: 'Vehicle', fieldName: 'vehicle', wrapText: true},
    {label: 'Lead Source', fieldName: 'opportunityLeadSource', wrapText: true, sortable: true}
];

const vehicleTypeOptions = [
    {label: 'All', value: 'All'},
    {label: 'New', value: 'New'},
    {label: 'Pre-Owned', value: 'Pre-Owned'}
];
const pageSizeOptions = [
    {label: '50', value: '50'},
    {label: '150', value: '150'},
    {label: '250', value: '250'},
    {label: '400', value: '400'}
];

export default class ShowroomTab extends LightningElement {
    @api dealer;
    @track isLoading = true;
    @track isLog = false;
    @track data = [];
    @track fullData = [];
    @track items = [];
    @track opportunity;
    showroom;
    error;
    errorVehicle;
    columns = columns;
    defaultSortDirection = 'asc';
    sortDirection = 'asc';
    sortedBy;

    /*Table pagination*/
    @track page = 1;
    @track pageSize = '50';
    @track totalRecountCount = 0;
    @track totalPage = 0;
    @track isPreviousDisabled = true;
    @track isNextDisabled = false;
    pageSizeOptions = pageSizeOptions;

    /*Arrival Date*/
    @track isArrivalDateModalOpen = false;
    @track arrivalDateVariant = BRAND;
    arrivalDateFromValue;
    arrivalDateToValue;
    errorArrivalDate;

    /*Team*/
    teamValue;

    /*Showroom Dealer*/
    showroomDealerValue = 'All';
    showroomDealerOptions = '';

    /*Last Activity By*/
    lastActivityByValue;

    /*Showroom Status*/
    @track isShowroomStatusModalOpen = false;
    @track showroomStatusVariant = BRAND_OUTLINE;
    _showroomStatusValue = [];

    /*Vehicle*/
    vehicleTypeOptions = vehicleTypeOptions;
    @track isVehicleModalOpen = false;
    @track vehicleVariant = BRAND_OUTLINE;
    vehicleTypeValue = 'All';
    vehicleMakeValue;
    vehicleDateFromValue;
    vehicleDateToValue;
    vehicleModelValue;
    vehicleTrimValue;

    @api channelName = '/event/Up_System_Platform_Event__e';

    connectedCallback() {
        this.subscribe();
        let today = new Date().toJSON().slice(0, 10);
        this.arrivalDateFromValue = today;
        this.arrivalDateToValue = today;
    }

    subscribe() {
        const messageCallback = (response) => {
            let event = JSON.parse(JSON.stringify(response));
            let type = event.data.payload.Type__c;
            let dealerId = event.data.payload.Delaer__c;
            if (type === "Showroom" && dealerId === this.dealer) {
                this.filterTable();
            }
        };
        subscribe(this.channelName, -1, messageCallback).then(response => {
            this.subscription = response;
        });
    }

    isAllDealerAccess = false;

    @wire(getRecord, {
        recordId: USER_ID,
        fields: ALL_DEALER_FIELD
    }) getUserInfo({error, data}) {
        if (data) {
            this.isAllDealerAccess = data.fields.All_Dealer_Access__c.value;
        }
    }

    @wire(getShowrooms)
    showrooms({error, data}) {
        if (data) {
            if (data.length > 0) {
                this.prepareData(data);
                this.disableButtons();
                this.dealer = data[0].dealerId;
            } else {
                this.data = undefined;
            }
            this.isLoading = !this.isLoading;
        } else if (error) {
            this.error = error;
            console.log(this.error);
            this.data = undefined;
            this.isLoading = !this.isLoading;
        }
    }

    @wire(getObjectInfo, {objectApiName: IN_SHOWROOM_OBJECT})
    showroomInfo;

    @wire(getPicklistValues,
        {
            recordTypeId: '$showroomInfo.data.defaultRecordTypeId',
            fieldApiName: SHOWROOM_STATUS_FIELD
        }
    )
    showroomStatusOptions;

    @wire(getPicklistValues,
        {
            recordTypeId: '$showroomInfo.data.defaultRecordTypeId',
            fieldApiName: SHOWROOM_DEALER_FIELD
        }
    ) getShowroomDealerOptions({error, data}) {
        if (data) {
            let dealers = [{label: "All", value: "All"}];
            data.values.forEach(
                (value => {
                    dealers.push({label: value.label, value: value.value})
                })
            );
            this.showroomDealerOptions = dealers;
        }
    }

    @wire(getObjectInfo, {objectApiName: VEHICLE_OBJECT})
    vehicleInfo;

    @wire(getPicklistValues,
        {
            recordTypeId: '$vehicleInfo.data.defaultRecordTypeId',
            fieldApiName: VEHICLE_MAKE_FIELD
        }
    )
    vehicleMakeOptions;

    @wire(getTeams)
    teams({error, data}) {
        this.teamValue = 'All';
        if (data) {
            this.teams = data;
            this.teams = this.teams.slice().sort((a, b) => (a.label.toUpperCase() > b.label.toUpperCase() && a.label !== 'All' && b.label !== 'All') ? 1 : ((b.label.toUpperCase() > a.label.toUpperCase() && a.label !== 'All' && b.label !== 'All') ? -1 : 0));
        } else if (error) {
            this.error = error.message;
            this.teams = undefined;
        }
    }

    @wire(getLastActivityBy)
    lastActivityBy({error, data}) {
        this.lastActivityByValue = 'All';
        if (data) {
            this.lastActivityBy = data;
            this.lastActivityBy = this.lastActivityBy.slice().sort((a, b) => (a.label.toUpperCase() > b.label.toUpperCase() && a.label !== 'All' && b.label !== 'All') ? 1 : ((b.label.toUpperCase() > a.label.toUpperCase() && a.label !== 'All' && b.label !== 'All') ? -1 : 0));
        } else if (error) {
            this.error = error.message;
            this.lastActivityBy = undefined;
        }
    }

    @wire(getYears)
    yearOptions({error, data}) {
        if (data) {
            this.yearOptions = data;
        } else if (error) {
            this.error = error;
            this.yearOptions = undefined;
        }
    }

    sortBy(field, reverse, primer) {
        const key = primer
            ? function (x) {
                return primer(x[field]);
            }
            : function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    onHandleSort(event) {
        const {fieldName: sortedBy, sortDirection} = event.detail;
        const cloneData = [...this.fullData];

        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
        this.items = cloneData;
        this.totalRecountCount = cloneData.length;
        this.data = this.items.slice(0, this.pageSize);
        this.page = 1;
        this.disableButtons();
    }

    /*Arrival Date START*/
    handleArrivalDateFromChange(event) {
        this.arrivalDateFromValue = event.detail.value;
    }

    handleArrivalDateToChange(event) {
        this.arrivalDateToValue = event.detail.value;
    }

    arrivalDateOpenModal() {
        this.isArrivalDateModalOpen = true;
    }

    applyArrivalDateModal() {
        if (this.arrivalDateToValue < this.arrivalDateFromValue) {
            this.errorArrivalDate = 'Date To should be bigger or equals Date From';
        } else {
            this.isArrivalDateModalOpen = false;
            if (this.arrivalDateFromValue || this.arrivalDateToValue) {
                this.arrivalDateVariant = BRAND;
            } else {
                this.arrivalDateVariant = BRAND_OUTLINE;
            }
            this.filterTable();
        }
    }

    closeArrivalDateModal() {
        this.arrivalDateFromValue = null;
        this.arrivalDateToValue = null;
        this.errorArrivalDate = null;
        this.arrivalDateVariant = BRAND_OUTLINE;
        this.isArrivalDateModalOpen = false;
        this.filterTable();
    }

    /*Arrival Date END*/

    /*Showroom Dealer START*/
    handleShowroomDealerChange(event) {
        this.showroomDealerValue = event.detail.value;
        this.filterTable();
    }

    /*Dealer END*/

    /*Last Activity By START*/
    handleLastActivityByChange(event) {
        this.lastActivityByValue = event.detail.value;
        this.filterTable();
    }

    /*Last Activity By END*/

    /*Team START*/
    handleTeamChange(event) {
        this.teamValue = event.detail.value;
        this.filterTable();
    }

    /*Team END*/

    /*Showroom Status START*/
    get showroomStatusValue() {
        return this._showroomStatusValue.length ? this._showroomStatusValue : null;
    }

    handleShowroomStatusChange(event) {
        this._showroomStatusValue = event.detail.value;
    }

    showroomStatusOpenModal() {
        this.isShowroomStatusModalOpen = true;
    }

    applyShowroomStatusModal() {
        this.isShowroomStatusModalOpen = false;
        if (this._showroomStatusValue.length > 0) {
            this.showroomStatusVariant = BRAND;
        } else {
            this.showroomStatusVariant = BRAND_OUTLINE;
        }
        this.filterTable();
    }

    closeShowroomStatusModal() {
        this._showroomStatusValue = [];
        this.isShowroomStatusModalOpen = false;
        this.showroomStatusVariant = BRAND_OUTLINE;
        this.filterTable();
    }

    /*Showroom Status END*/

    /*Vehicle START*/
    handleVehicleTypeChange(event) {
        this.vehicleTypeValue = event.detail.value;
    }

    handleVehicleDateFromChange(event) {
        this.vehicleDateFromValue = event.detail.value;
    }

    handleVehicleDateToChange(event) {
        this.vehicleDateToValue = event.detail.value;
    }

    handleVehicleMakeChange(event) {
        this.vehicleMakeValue = event.detail.value;
    }

    handleModelChange(event) {
        this.vehicleModelValue = event.detail.value;
    }

    handleTrimChange(event) {
        this.vehicleTrimValue = event.detail.value;
    }

    vehicleOpenModal() {
        this.isVehicleModalOpen = true;
    }

    applyVehicleModal() {
        if (parseInt(this.vehicleDateToValue) < parseInt(this.vehicleDateFromValue)) {
            this.errorVehicle = 'Year To should be bigger or equals Year From';
        } else {
            this.isVehicleModalOpen = false;
            if (this.vehicleTypeValue !== 'All' || this.vehicleMakeValue.length > 0 || this.vehicleDateFromValue.length > 0 ||
                this.vehicleDateToValue.length > 0 || this.vehicleModelValue.length > 0 || this.vehicleTrimValue.length > 0) {
                this.vehicleVariant = BRAND;
            } else {
                this.vehicleVariant = BRAND_OUTLINE;
            }
            this.filterTable();
        }
    }

    closeVehicleModal() {
        this.vehicleTypeValue = 'All';
        this.vehicleMakeValue = null;
        this.vehicleDateFromValue = null;
        this.vehicleDateToValue = null;
        this.vehicleModelValue = null;
        this.vehicleTrimValue = null;
        this.errorVehicle = null;
        this.isVehicleModalOpen = false;
        this.vehicleVariant = BRAND_OUTLINE;
        this.filterTable();
    }

    /*Vehicle END*/

    /*Opportunity START*/
    get isOpportunityNotSelected() {
        return !this.opportunity;
    }

    handleOpportunitySelect(event) {
        this.opportunity = JSON.parse(event.detail.record);
    }

    handleOpportunityCreate(event) {
        linkOpportunity({showroomId: this.showroom.id, opportunityId: this.opportunity.id})
            .then((data) => {
                this.isLog = !this.isLog;
                this.opportunity = undefined;
                this.filterTable();
            })
            .catch((error) => {
                this.error = error;
                console.log(this.error);
                this.isLog = !this.isLog;
            });
    }

    handleOpportunityClose(event) {
        this.opportunity = undefined;
        this.isLog = !this.isLog;
    }

    /*Opportunity END*/

    filterTable() {
        if (this.checkIfChanged()) {
            this.isLoading = !this.isLoading;
            getSortableTable({
                arrivalDateFrom: this.arrivalDateFromValue,
                arrivalDateTo: this.arrivalDateToValue,
                showroomStatus: this._showroomStatusValue,
                vehicleType: this.vehicleTypeValue,
                vehicleMake: this.vehicleMakeValue,
                vehicleDateFrom: this.vehicleDateFromValue,
                vehicleDateTo: this.vehicleDateToValue,
                vehicleModel: this.vehicleModelValue,
                vehicleTrim: this.vehicleTrimValue,
                team: this.teamValue,
                lastActivityBy: this.lastActivityByValue,
                showroomDealer: this.showroomDealerValue
            })
                .then((data) => {
                    if (data.length > 0) {
                        this.prepareData(data);
                        this.page = 1;
                        this.disableButtons();
                    } else {
                        this.data = undefined;
                    }
                    this.setValuesForSorting();
                    this.error = undefined;
                    this.isLoading = !this.isLoading;
                })
                .catch((error) => {
                    this.error = error;
                    console.log(this.error);
                    this.data = undefined;
                    this.isLoading = !this.isLoading;
                });
        }
    }

    valuesForSorting = [];

    setValuesForSorting() {
        this.valuesForSorting = [];
        this.valuesForSorting.push(this.arrivalDateFromValue, this.arrivalDateToValue, this._showroomStatusValue, this.vehicleTypeValue,
            this.vehicleMakeValue, this.vehicleDateFromValue, this.vehicleDateToValue, this.vehicleModelValue, this.vehicleTrimValue,
            this.teamValue, this.lastActivityByValue, this.showroomDealerValue);
    }

    checkIfChanged() {
        let values = [];
        values.push(this.arrivalDateFromValue, this.arrivalDateToValue, this._showroomStatusValue, this.vehicleTypeValue,
            this.vehicleMakeValue, this.vehicleDateFromValue, this.vehicleDateToValue, this.vehicleModelValue, this.vehicleTrimValue,
            this.teamValue, this.lastActivityByValue, this.showroomDealerValue);

        return JSON.stringify(this.valuesForSorting) !== JSON.stringify(values);
    }

    /*Log button START*/
    handleRowAction(event) {
        this.showroom = JSON.parse(JSON.stringify(event.detail.row));
        if (this.showroom.type === 'action:log_event') {
            this.isLog = !this.isLog;
        }
    }

    /*Log button END*/

    /*Pagination START*/
    handlePageSize(event) {
        if (this.data.length > 0) {
            this.pageSize = event.detail.value;
            this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize);
            this.data = this.items.slice(0, this.pageSize);
            this.endingRecord = this.pageSize;
            this.page = 1;
            this.disableButtons();
        }
    }

    prepareData(data) {
        this.fullData = data;
        this.items = data;
        this.totalRecountCount = data.length;
        this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize);
        this.data = this.items.slice(0, this.pageSize);
        this.endingRecord = this.pageSize;
    }

    firstPageHandler() {
        this.page = 1;
        this.disableButtons();
        this.displayRecordPerPage(this.page);
    }

    previousHandler() {
        if (this.page > 1) {
            this.page = this.page - 1;
            this.displayRecordPerPage(this.page);
        }
        this.disableButtons();
    }

    lastPageHandler() {
        this.page = this.totalPage;
        this.disableButtons();
        this.displayRecordPerPage(this.page);
    }

    nextHandler() {
        if ((this.page < this.totalPage) && this.page !== this.totalPage) {
            this.page = this.page + 1;
            this.displayRecordPerPage(this.page);
        }
        this.disableButtons();
    }

    disableButtons() {
        this.isPreviousDisabled = this.page <= 1;
        this.isNextDisabled = this.page === this.totalPage;
    }

    displayRecordPerPage(page) {
        this.startingRecord = ((page - 1) * this.pageSize);
        this.endingRecord = (this.pageSize * page);
        this.endingRecord = (this.endingRecord > this.totalRecountCount) ? this.totalRecountCount : this.endingRecord;
        this.data = this.items.slice(this.startingRecord, this.endingRecord);
        this.startingRecord = this.startingRecord + 1;
    }

    /*Pagination END*/
}