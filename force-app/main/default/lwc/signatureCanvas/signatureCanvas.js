import { LightningElement, api } from 'lwc';

export default class SignatureCanvas extends LightningElement {
    canvasWidth = 768;
    canvasHeight = this.canvasWidth / 2;
    context = null;
    isPainting = false;
    lineSize = 6;
    mousePressed = Object.create(null);
    // moves;
    // points;
    prevPoint;
    throttledSave = this.throttle(this.save, 100);

    constructor() {
        super();

        this.reset();

        window.addEventListener('mousedown', (event) => {
            if (!(event.button in this.mousePressed)) {
                this.mousePressed[event.button] = 0;
            }

            this.mousePressed[event.button]++;
        });

        window.addEventListener('mouseup', (event) => {
            this.mousePressed[event.button]--;

            if (this.mousePressed[event.button] < 0) {
                this.mousePressed[event.button] = 0;
            }
        });

        window.addEventListener('mouseleave', (event) => {
            if (
                (event.clientX < 0 || event.clientX > window.innerWidth) ||
                (event.clientY < 0 || event.clientY > window.innerHeight)
            ) {
                for (let button in this.mousePressed) {
                    this.mousePressed[button] = 0;
                }
            }
        });
    }

    addPoint(offsetX, offsetY, move = false) {
        const canvasWidth = this.context.canvas.offsetWidth;
        const canvasHeight = this.context.canvas.offsetHeight;

        const toX = Math.round(offsetX / canvasWidth * this.canvasWidth);
        const toY = Math.round(offsetY / canvasHeight * this.canvasHeight);

        let fromX, fromY;
        if (move && this.prevPoint) {
            [fromX, fromY] = this.prevPoint;
        } else {
            [fromX, fromY] = [toX + 1, toY];
        }

        this.draw(fromX, fromY, toX, toY);

        this.prevPoint = [toX, toY];
        this.throttledSave();
    }

    // addPoint(offsetX, offsetY, move = false) {
    //     const canvasWidth = this.context.canvas.offsetWidth;
    //     const canvasHeight = this.context.canvas.offsetHeight;

    //     const x = Math.round(offsetX / canvasWidth * this.canvasWidth);
    //     const y = Math.round(offsetY / canvasHeight * this.canvasHeight);

    //     this.points = [
    //         ...this.points,
    //         [x, y],
    //     ];

    //     this.moves = [
    //         ...this.moves,
    //         move,
    //     ];

    //     this.redraw();
    //     this.throttledSave();
    // }

    draw(fromX, fromY, toX, toY) {
        const context = this.context;

        context.beginPath();

        context.moveTo(fromX, fromY);
        context.lineTo(toX, toY);

        context.closePath();
        context.stroke();
    }

    handleEnd() {
        this.isPainting = false;
    }

    handleEnter(event) {
        if (this.mousePressed[event.button]) {
            this.isPainting = true;
            this.addPoint(event.layerX, event.layerY);
        }
    }

    handleLeave() {
        this.isPainting = false;
    }

    handleMove(event) {
        if (this.isPainting) {
            this.addPoint(event.layerX, event.layerY, true);
        }
    }

    handleStart(event) {
        this.isPainting = true;

        if (!this.context) {
            const canvas = event.target;
            this.initContext(canvas.getContext('2d'));
        }

        this.addPoint(event.layerX, event.layerY);
    }

    handleTouchMove(event) {
        event.preventDefault();
        event.stopPropagation();

        const touch = event.touches[0];
        const canvasRect = this.context.canvas.getBoundingClientRect();

        this.addPoint(touch.clientX - canvasRect.x, touch.clientY - canvasRect.y, true);
    }

    handleTouchStart(event) {
        this.isPainting = true;

        if (!this.context) {
            const canvas = event.target;
            this.initContext(canvas.getContext('2d'));
        }

        const touch = event.touches[0];
        const canvasRect = this.context.canvas.getBoundingClientRect();

        this.addPoint(touch.clientX - canvasRect.x, touch.clientY - canvasRect.y);
    }

    initContext(context) {
        this.context = context;

        context.strokeStyle = '#000000';
        context.lineJoin = 'round';
        context.lineWidth = this.lineSize;
    }

    // redraw() {
    //     this.resetContext();

    //     for (let i = 0; i < this.points.length; i++) {
    //         const [toX, toY] = this.points[i];
    //         let fromX, fromY;

    //         if (this.moves[i] && i) {
    //             [fromX, fromY] = this.points[i - 1];
    //         } else {
    //             [fromX, fromY] = [toX + 1, toY];
    //         }

    //         this.draw(fromX, fromY, toX, toY);
    //     }
    // }

    @api
    reset() {
        // this.moves = [];
        // this.points = [];
        this.prevPoint = null;

        if (this.context) {
            this.resetContext();
        }
    }

    resetContext() {
        this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);
    }

    save() {
        const base64 = this.context.canvas.toDataURL('image/png');

        this.dispatchEvent(new CustomEvent('save', {
            detail: base64,
        }));
    }

    throttle(func, ms) {
        let isThrottled = false,
        savedArgs,
        savedThis;

        function wrapper() {
            if (isThrottled) {
                savedArgs = arguments;
                savedThis = this;
                return;
            }

            func.apply(this, arguments);

            isThrottled = true;

            setTimeout(function() {
                isThrottled = false;
                if (savedArgs) {
                    wrapper.apply(savedThis, savedArgs);
                    savedArgs = savedThis = null;
                }
            }, ms);
        }

        return wrapper;
    }
}