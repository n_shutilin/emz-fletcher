import {LightningElement, track, api, wire} from 'lwc';
import insertRecord from '@salesforce/apex/ssrmNewServiceResourceCtrl.saveServiceResource';
import getDealer from '@salesforce/apex/ssrmNewServiceResourceCtrl.getDealer';
import getPicklistValues from '@salesforce/apex/ssrmNewServiceResourceCtrl.getPicklistValues';
import {loadStyle} from 'lightning/platformResourceLoader';
import cssResource from '@salesforce/resourceUrl/ssrmPicklistHeight';
import {NavigationMixin} from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ssrmNewServiceResource extends NavigationMixin(LightningElement) {
    @track serviceResource = {'IsActive': true};
    @api territoryId;
    @api dealerOptions;
    @track isLoaded = true;

    connectedCallback() {
        loadStyle(this, cssResource);

        getDealer({'territoryId': this.territoryId})
            .then(result => {
                this.serviceResource.Dealer__c = result;
            })
            .catch((error) => {
                console.log('ERROR', error)
            })
    }

    @wire(getPicklistValues)
    getPicklistValues({ error, data }) {
        if (data) {
            this.dealerOptions = data.Dealer__c;
        } else if (error) {
            console.log('--- error: ' + JSON.stringify(error));
        }
    }

    handleFieldChange(event) {
        let fieldName = event.target.dataset.field;
        this.serviceResource[fieldName] = event.detail.value;
    }

    handleLookupValueChange(event) {
        let fieldName = event.target.dataset.field;

        if (event.detail.length > 0) {
            this.serviceResource[fieldName] = event.detail[0].id;
        } else {
            this.serviceResource[fieldName] = undefined;
        }
    }

    save() {
        this.isLoaded = false;

        if (this.validateFields()) {

            insertRecord({
                resource: this.serviceResource,
                territoryId: this.territoryId
            })
                .then(result => {
                    this.serviceResource.Id = result;
                    this.dispatchEvent(new CustomEvent('close'));
                    this.goToSlots();
                })
                .catch((error) => {
                    let message = '';

                    if (error.body.message) {
                        message = error.body.message;

                        if (message.indexOf('RelatedRecordId') > -1) {
                            message = 'A service resource of this type already exists for this user. Select a different Resource Type or edit the existing service resource.';
                        }
                    }

                    this.showToast('Error', message, 'error');
                    this.isLoaded = true;

                    console.log('ERROR', error);
                });
        } else {
            this.isLoaded = true;
        }
    }

    validateFields() {
        this.template.querySelector('c-custom-lookup').validate();

        let isValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if (!isValid) {
            return false;
        }

        this.template.querySelectorAll('.required-lookup').forEach(element => {
            if (element.value === undefined) {
                isValid =  false;
            }
        });

        return isValid;
    }

    goToSlots() {
        let pageUrl = '/apex/ssrmSlotsPage';
        let params = '?territoryId=' + this.territoryId + '&resourceId=' + this.serviceResource.Id + '&context=create';

        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: pageUrl + params
            }
        })
    }

    cancel() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    showToast(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            })
        );
    }
}