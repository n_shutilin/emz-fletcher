import {LightningElement, track, api, wire} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getNewSlots from '@salesforce/apex/ssrmSlotsCtrl.getNewSlots';
import getExistingSlots from '@salesforce/apex/ssrmSlotsCtrl.getExistingSlots';
import getDays from '@salesforce/apex/ssrmSlotsCtrl.getDays';
import getResourceName from '@salesforce/apex/ssrmSlotsCtrl.getResourceName';
import saveSlots from '@salesforce/apex/ssrmSlotsCtrl.saveSlots';

export default class SsrmSlots extends NavigationMixin(LightningElement) {
    @track territoryId;
    @track resourceId;
    @track context;
    @track resourceName;
    @track days;
    @track slotsByDays;
    @track daysColumnsClass;
    @track hourOptions;
    @track minuteOptions;
    @track isLoaded = false;
    @track isSubmitDisabled = true;

    connectedCallback() {
        this.setParamsFromUrl();
        this.setHours();
        this.setMinutes();
        this.getDays();
        this.getResourceName();

        switch(this.context) {
            case 'create':
                this.getNewSlots();
                break;
            case 'edit':
                this.getExistingSlots();
                break;
        }
    }

    setParamsFromUrl() {
        let pageUrl = window.location.href;
        let params = new URL(pageUrl).searchParams;

        this.territoryId = params.get('territoryId');
        this.resourceId = params.get('resourceId');
        this.context = params.get('context');
    }

    setHours() {
        let options = [];

        for (let i = 0; i < 24; i++) {
            let hour = getFullHour(i);
            options.push({'label': hour, 'value': i});
        }

        this.hourOptions = options;

        function getFullHour(hour) {
            let period = hour < 12 ? ' AM' : ' PM';
            let fullHour;

            if (hour === 0 || hour === 12) {
                fullHour = '12' + period;
            } else if (hour < 12) {
                fullHour = hour + period;
            } else {
                fullHour = hour - 12 + period;
            }

            if (hour.length === 4) {
                fullHour = '0' + fullHour;
            }

            return fullHour;
        }
    }

    setMinutes() {
        let options = [];

        for (let i = 0; i < 60; i += 5) {
            let minute = i < 10 ? '0' + i : i + '';
            options.push({'label': minute, 'value': i});
        }

        this.minuteOptions = options
    }

    getDays() {
        getDays({
            'resourceId': this.resourceId
        })
            .then(result => {
                this.days = result;
            })
            .catch((error) => {
                console.log('ERROR', error);
            })
    }

    getResourceName() {
        getResourceName({
            'resourceId': this.resourceId
        })
            .then(result => {
                this.resourceName = result;
            })
            .catch((error) => {
                console.log('ERROR', error);
            })
    }

    getNewSlots() {
        getNewSlots({
            'territoryId': this.territoryId,
            'resourceId': this.resourceId
        })
            .then(result => {
                if (result) {
                    this.setSlots(result);
                } else {
                    this.template.querySelector('c-custom-toast').showCustomNotice('No slots found', 'error');
                    this.isLoaded = true;
                }
            })
            .catch((error) => {
                console.log('ERROR', error);
            })
    }

    getExistingSlots() {
        getExistingSlots({
            'resourceId': this.resourceId
        })
            .then(result => {
                if (result.length > 0) {
                    this.setSlots(result);
                } else {
                    this.template.querySelector('c-custom-toast').showCustomNotice('No slots found', 'error');
                    this.isLoaded = true;
                }
            })
            .catch((error) => {
                console.log('ERROR', error);
            })
    }

    setSlots(slots) {
        const columnWidth = 180;

        if (this.context === 'create' || slots.length === this.days.length) {
            this.slotsByDays = slots;
        } else {
            let slotList = [];

            for (let i = 0, j = 0; i < this.days.length; i++) {
                if (slots[j] && slots[j][0].day === this.days[i]) {
                    slotList.push(slots[j]);
                    j++;
                } else {
                    slotList.push([]);
                }
            }

            this.slotsByDays = slotList;
        }

        this.daysColumnsClass = 'slds-size_1-of-' + this.slotsByDays.length;
        this.template.querySelector('.main-div').style.width = this.slotsByDays.length * columnWidth + 'px';
        this.isLoaded = true;
        this.isSubmitDisabled = false;
    }

    handleCancel() {
        history.back();
    }

    handleSave() {
        this.isSubmitDisabled = true;
        this.isLoaded = false;

        let slotWrappers = [];

        this.slotsByDays.forEach(function (slots) {
            slotWrappers = slotWrappers.concat(slots);
        });

        saveSlots({
            'slotsStr': JSON.stringify(slotWrappers),
            'context': this.context,
            'resourceId': this.resourceId
        })
            .then(result => {
                this.template.querySelector('c-custom-toast').showCustomNotice('Updated', 'success');
                this.isLoaded = true;
                setTimeout(() => history.back(), 1000);
            })
            .catch((error) => {
                console.log('ERROR', error);
                this.isLoaded = true;
            });
    }

    updateSlot(event) {
        let updatedSlots = [];

        for (let i = 0; i < this.slotsByDays.length; i++) {
            if (event.detail.index === i) {
                updatedSlots.push(event.detail.slotsFromChild);
            } else {
                updatedSlots.push(this.slotsByDays[i]);
            }
        }

        this.slotsByDays = updatedSlots;
    }

    showToast(event) {
        this.template.querySelector('c-custom-toast').showCustomNotice(event.detail.message, event.detail.variant);
    }
}