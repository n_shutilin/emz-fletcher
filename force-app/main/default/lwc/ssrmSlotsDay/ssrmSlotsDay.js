import {LightningElement, track, api} from 'lwc';
import {loadStyle} from 'lightning/platformResourceLoader';
import cssResource from '@salesforce/resourceUrl/ssrmPicklistHeight';

export default class SsrmSlotsDay extends LightningElement {
    @api slots;
    @api hourOptions;
    @api minuteOptions;
    @api days;
    @api index;
    @api context;

    @track day;
    @track selectedSlot = {};
    @track slotIndex;
    @track isModalOpen = false;
    @track modalClass;
    @track modalAction;
    @track isDelete;
    @track successMessage;

    connectedCallback() {
        loadStyle(this, cssResource);
        this.day = this.days[+this.index];
    }

    handleEdit(event) {
        let index = +event.target.dataset.index;

        this.selectedSlot = Object.assign(this.selectedSlot, this.slots[index]);
        this.slotIndex = index;
        this.modalAction = 'Edit';
        this.successMessage = 'Updated';
        this.isDelete = false;
        this.modalClass = 'slds-modal__content slds-p-around_medium slds-grid slds-wrap modal-with-picklists';
        this.openModal();
    }

    handleCreate() {
        this.selectedSlot = {'day': this.day};
        this.slotIndex = this.slots.length;
        this.modalAction = 'Create';
        this.successMessage = 'Created';
        this.isDelete = false;
        this.modalClass = 'slds-modal__content slds-p-around_medium slds-grid slds-wrap modal-with-picklists';
        this.openModal();
    }

    handleDelete(event) {
        this.modalAction = 'Delete';
        this.successMessage = 'Deleted';
        this.isDelete = true;
        this.slotIndex = +event.target.dataset.index;
        this.modalClass = 'slds-modal__content slds-p-around_medium slds-grid slds-wrap';
        this.openModal();
    }

    handleTimeChange(event) {
        let fieldName = event.target.dataset.field;
        this.selectedSlot[fieldName] = undefined;

        setTimeout(function() {
            this.selectedSlot[fieldName] = +event.detail.value;
        }.bind(this), 1);
    }

    handleSubmit(event) {
        if (this.isDelete) {
            this.deleteSlot(event);
        } else {
            this.saveSlot(event);
        }
    }

    deleteSlot(event) {
        let updatedSlots = [];

        for (let i = 0; i < this.slots.length; i++) {
            if (i === this.slotIndex) continue;

            updatedSlots.push(this.slots[i]);
        }

        this.slots = updatedSlots;
        this.showToast(event, this.successMessage, 'success');
        this.changeSlot(event);
        this.closeModal();
    }

    saveSlot(event) {
        let isValid = this.validateSlotEdit(event);

        if (isValid) {
            this.selectedSlot.fullStartTime = this.getFullTime(this.selectedSlot.startTimeHour, this.selectedSlot.startTimeMinute);
            this.selectedSlot.fullEndTime = this.getFullTime(this.selectedSlot.endTimeHour, this.selectedSlot.endTimeMinute);

            let updatedSlots = [];

            this.slots.forEach(function (slot) {
                updatedSlots.push(slot);
            });

            let newSlot = {};
            updatedSlots[this.slotIndex] = Object.assign(newSlot, this.selectedSlot);

            this.slots = this.getSortedSlots(updatedSlots);
            this.showToast(event, this.successMessage, 'success');
            this.changeSlot(event);
            this.closeModal();
        }
    }

    validateSlotEdit(event) {
        let startHour = this.selectedSlot.startTimeHour;
        let endHour = this.selectedSlot.endTimeHour;
        let starMinute = this.selectedSlot.startTimeMinute;
        let endMinute = this.selectedSlot.endTimeMinute;

        if (startHour === undefined || endHour === undefined || starMinute === undefined || endMinute === undefined) {
            this.showToast(event,'Fill all fields', 'error');
            return false;
        }

        let isValid = this.checkSelected(startHour, endHour, starMinute, endMinute);

        if (isValid) {
            isValid = this.checkExisting(startHour, endHour, starMinute, endMinute);

            if (!isValid) {
                this.showToast(event, 'Slot for selected period already exists', 'error');
            }
        } else {
            this.showToast(event,'End time must be more than start time', 'error');
            return false;
        }

        return isValid;
    }

    checkSelected(startHour, endHour, startMinute, endMinute) {
        if (endHour === 0 && endMinute === 0 && (startHour > 0 || startMinute > 0)) {
            return true;
        }

        if (startHour > endHour) {
            return false;
        }

        if (startHour === endHour) {
            return endMinute > startMinute;
        }

        return true;
    }

    checkExisting(startHour, endHour, startMinute, endMinute) {
        for (let i = 0; i < this.slots.length; i++) {
            if (i === this.slotIndex) {
                continue;
            }

            let slot = this.slots[i];

            if (slot.startTimeHour > endHour) {
                continue;
            }

            if (slot.startTimeHour === endHour && slot.startTimeMinute >= endMinute) {
                continue;
            }

            if (slot.endTimeHour < startHour) {
                continue;
            }

            if (slot.endTimeHour === startHour && slot.endTimeMinute <= startMinute) {
                continue;
            }

            return false;
        }

        return true;
    }
    
    getFullTime(hour, minute) {
        let hourStr;

        if (hour <= 12) {
            hourStr = hour === 0 ? '12' : hour + '';
        } else {
            hourStr = hour - 12 + ''
        }

        if (hourStr.length === 1) {
            hourStr = '0' + hourStr;
        }

        let minuteStr = minute + '';

        if (minuteStr.length === 1) {
            minuteStr = '0' + minuteStr;
        }

        let period = hour < 12 ? 'AM' : 'PM';

        return hourStr + ':' + minuteStr + ' ' + period;
    }

    getSortedSlots(slots) {
        slots.sort((a, b) => (a.startTimeHour > b.startTimeHour) ? 1 : (a.startTimeHour === b.startTimeHour) ? ((a.startTimeMinute > b.startTimeMinute) ? 1 : -1) : -1 );
        return slots;
    }

    changeSlot(event) {
        event.preventDefault();
        let params = {slotsFromChild: this.slots, index: +this.index};
        this.dispatchEvent(new CustomEvent('slotchange', {detail: params}));
    }

    openModal() {
        this.isModalOpen = true;
    }

    closeModal() {
        this.isModalOpen = false;
    }

    showToast(event, message, variant) {
        event.preventDefault();
        let params = {message: message, variant: variant};
        this.dispatchEvent(new CustomEvent('showtoast', {detail: params}));
    }
}