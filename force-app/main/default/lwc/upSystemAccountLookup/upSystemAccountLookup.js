import { LightningElement, api } from 'lwc';

export default class UpSystemAccountLookup extends LightningElement {
    @api dealer

    objectName = 'Account'

    handleSelect(event){
        this.dispatchEvent(
            new CustomEvent("select", {
                detail: {
                    record: event.detail.record
                }
            })
        );
    }
}