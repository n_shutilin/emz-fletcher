import { LightningElement, api } from 'lwc';

export default class UpSystemOpportunityLookup extends LightningElement {
    @api dealer

    objectName = 'Opportunity'

    handleSelect(event){
        this.dispatchEvent(
            new CustomEvent("select", {
                detail: {
                    record: event.detail.record
                }
            })
        );
    }
}