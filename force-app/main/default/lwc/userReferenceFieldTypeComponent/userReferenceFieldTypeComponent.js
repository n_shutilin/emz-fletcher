import { LightningElement, api, track  } from 'lwc';

export default class UserReferenceFieldTypeComponent extends LightningElement {
    @api userId;
    @api recordName;

    @track finalUrl;
    @track isUser;

    userPrefix = '005';

    connectedCallback() {
        this.finalUrl = '/' + this.userId;
        let currentPrefix = this.userId.substring(0, 3);
        this.isUser = currentPrefix == this.userPrefix;
    }
}