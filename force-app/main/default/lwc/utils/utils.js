const ONE_MINUTE_IN_MS = 60000;

const formatAMPM = (date) => {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? "PM" : "AM";
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  var strTime = hours + ":" + minutes + " " + ampm;
  return strTime;
};

const formatDate = (date) => {
  var hours = (date.getHours() < 10 ? "0" : "") + date.getHours();
  var minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
  var seconds = (date.getSeconds() < 10 ? "0" : "") + date.getSeconds();
  var strTime = hours + ":" + minutes + ":" + seconds +".000Z";
  return strTime;
};

const getGMTWithShiftTime = (shift) => {
  let date = new Date();
  let timezoneOffset = date.getTimezoneOffset();
  date.setMinutes(date.getMinutes() + timezoneOffset + shift);
  return formatDate(date);
}

const getGMTTime = () => {
  let date = new Date();
  let timezoneOffset = date.getTimezoneOffset();
  date.setMinutes(date.getMinutes() + timezoneOffset);
  return formatDate(date);
}

const formDateFromTime = (time) => {
  let currentDate = new Date();
  let year = currentDate.getFullYear();
  let month = currentDate.getMonth() + 1;
  let day = currentDate.getDate();
  let formedDate = new Date(year + "-" + (month < 10 ? "0" + month : month) + "-" + (day < 10 ? "0" + day : day) + "T" + time.replace(".000", ""));
  if(formedDate.getDate() != day) {
    formedDate.setDate(day);
  }
  return formedDate;
}

const formGMT = (value) => {
  let date = formDateFromTime(value.replace("Z",""));
  let timezoneOffset = date.getTimezoneOffset();
  date.setMinutes(date.getMinutes() + timezoneOffset);
  return formatDate(date);
}

const formGMTDate = (value) => {
  let date = formDateFromTime(value.replace("Z",""));
  let timezoneOffset = date.getTimezoneOffset();
  date.setMinutes(date.getMinutes() + timezoneOffset);
  return date;
}

const getDateDifferenceInMinutes = (firstDate, secondDate) => {
  return Math.round((firstDate - secondDate) / ONE_MINUTE_IN_MS)
}

const dayOfWeekMap = new Map([
  [0,"Sunday"],
  [1,"Monday"],
  [2,"Tuesday"],
  [3,"Wednesday"],
  [4,"Thursday"],
  [5,"Friday"],
  [6,"Saturday"]
]);

const defineDayOfWeek = () => {
  return dayOfWeekMap.get(new Date().getDay());  
}

const generateUUIDv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var random = Math.random() * 16 | 0, v = c == 'x' ? random : (random & 0x3 | 0x8);
    return v.toString(16);
  });
}

export { generateUUIDv4, defineDayOfWeek, getDateDifferenceInMinutes, formatAMPM, formatDate, getGMTTime, getGMTWithShiftTime, formGMT, formDateFromTime, formGMTDate };