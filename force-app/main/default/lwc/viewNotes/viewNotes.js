import { LightningElement, api } from 'lwc';

import getImageIds from '@salesforce/apex/SaveAttachmentController.getImageIds';
import retrieveAnnotation from '@salesforce/apex/SaveAttachmentController.retrieveAnnotation';
import retrieveVideos from '@salesforce/apex/SaveAttachmentController.retrieveVideos';
import saveSignature from '@salesforce/apex/SaveAttachmentController.saveSignature';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ViewNotes extends LightningElement {
    @api recordId;
    notes;
    videos;
    notesLoaded = false;
    videosLoaded = false;
    loaded = false;

    signature;
    signatureOpen = false;
    approved = false;

    connectedCallback() {
        //developed at this way to easy switch to s3 storage in future
        this.retrieveNotes(this.recordId).then((notes) => {
            console.log('data will be loaded for record with id: ' + this.recordId);
            console.log('loadNotes', notes);
            this.notes = notes;
            this.notesLoaded = true;
            this.loaded = this.notesLoaded && this.videosLoaded;
        }).catch((error) => {
            console.error('loadNotes', error);
        });
        retrieveVideos({appointmentId: this.recordId}).then((videos) => {
            this.videos = [];
            for (let i = 0; i < videos.length; i++) {
                this.videos.push({id: videos[i], title: "Video " + parseInt(parseInt(i) + 1)});
            }
            this.videosLoaded = true;
            this.loaded = this.notesLoaded && this.videosLoaded;
        }).catch((error) => {
            console.error('loadVideos', error);
        });
    }

    async retrieveNotes() {
        console.log('retriving data for record with id: ' + this.recordId);
        const ids = await getImageIds({
            appointmentId: this.recordId,
        });
        const promises = ids.map((id) => {
            console.log('retriving data for image with id: ' + id);
            return retrieveAnnotation({
                vehicleImageId: id,
            }).then(([note]) => {
                return {
                    ...note,
                    id,
                    // photo: `data:image/png;base64,${note.photo}`,
                    photo: note.photo,
                };
            });
        });

        const notes = await Promise.all(promises);

        return notes;
    }

    closeSignature() {
        this.signatureOpen = false;
        this.resetSignature();
    }

    resetSignature() {
        this.approved = false;
        this.signature = null;

        const component = this.template.querySelector('c-signature-canvas');

        if (component) {
            component.reset();
        }
    }

    showSignature() {
        this.signatureOpen = true;
    }

    get signatureClearDisabled() {
        return !this.signature;
    }

    get signaturePanelClass() {
        const classes = [
            'panel slds-panel slds-panel_docked slds-is-open',
        ];

        if (this.signatureOpen) {
            classes.push('panel--open');
        }

        return classes.join(' ');
    }

    get signatureSubmitDisabled() {
        return !this.approved || !this.signature;
    }

    saveSignature() {
        saveSignature({
            appointmentId: this.recordId,
            imageBody: this.signature,
        })
        .then(result => {
            this.showToast('success', 'Saved');
            this.closeSignature();
        })
        .catch((error) => {
            const message = error.body.message;
            this.showToast('error', 'Error', message);
        });
    }

    showToast(type, title, message) {
        this.dispatchEvent(new ShowToastEvent({
            message,
            title,
            variant: type,
        }));
    }

    handleSignature(event) {
        this.signature = event.detail;
    }

    handleApprove(event) {
        const checkbox = event.target;
        this.approved = checkbox.value;
    }
}