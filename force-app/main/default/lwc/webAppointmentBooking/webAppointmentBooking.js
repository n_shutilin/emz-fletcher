import { LightningElement, track, api } from 'lwc';

import getCurrentSObjectType from '@salesforce/apex/WebAppointmentBookingController.getCurrentSObjectType';
import getDealerInfo from '@salesforce/apex/WebAppointmentBookingController.getDealerInfo';
import getServieAppointmentInfo from '@salesforce/apex/WebAppointmentBookingController.getServieAppointmentInfo';
import getPersonContactInfo from '@salesforce/apex/WebAppointmentBookingController.getPersonContactInfo';
import getAuthContacts from '@salesforce/apex/WebAppointmentBookingController.getAuthContacts';
import getVehiclesInfo from '@salesforce/apex/WebAppointmentBookingController.getVehicles';
import getVehicleMakes from '@salesforce/apex/WebAppointmentBookingController.getMakePicklist';
import createVehicle from '@salesforce/apex/WebAppointmentBookingController.insertNewVehicle';
import getServiceResources from '@salesforce/apex/WebAppointmentBookingController.getServiceResourcesByDealer';
import createServiceAppointment from '@salesforce/apex/WebAppointmentBookingController.createServiceAppointment';
import getSchedTimeSlots from '@salesforce/apex/WebAppointmentBookingController.getSchedTimeSlots';
import scheduleAppointment from '@salesforce/apex/WebAppointmentBookingController.scheduleAppointment';
import confirmationServiceAppointmentUpdate from '@salesforce/apex/WebAppointmentBookingController.confirmationServiceAppointmentUpdate';
import deleteAppointment from '@salesforce/apex/WebAppointmentBookingController.deleteAppointment';
import sendAppointmentIntoCDK from '@salesforce/apex/WebAppointmentBookingController.sendAppointmentIntoCDK';
import getTransportationNotes from '@salesforce/apex/WebAppointmentBookingController.getTransportationNotes';
import getMakeModelFile from '@salesforce/apex/WebAppointmentBookingController.getFile';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

const START_VEHICLE_YEAR = 1981
const OBJECTS_LIST = ['Recall__c', 'Fletcher_Care_PPM__c', 'Mercedes_OEM_PPM__c', 'Service_Opportunities__c']
const DAYS_OF_WEEK_MAP = {'Sun':'Sunday', 'Mon':'Monday', 'Tue':'Tuesday', 'Wed':'Wednesday', 'Thu':'Thursday', 'Fri':'Friday', 'Sat':'Saturday'};
const MONTHS_MAP = {'Jan':'January', 'Feb':'February', 'Mar':'March', 'Apr':'April', 'May':'May', 'Jun':'June', 'Jul':'July', 
    'Aug':'August', 'Sep':'September', 'Oct':'October', 'Nov':'November', 'Dec':'December'};

export default class WebAppointmentBooking extends NavigationMixin(LightningElement) {

    @api recordId;

    @track isLoaded = false;
    @track serviceAppointment = {};
    @track selectedContact = {};
    @track selectedAuthContact;
    @track authContacts = [];
    @track vehicles = [];
    @track vehMakesOptions = [];
    @track advisorsMap = {};
    @track isAddNewVehicleDisabled = true;
    @track newVehicle = {};
    @track availableTransportations = new Set();
    // @track isLoanerAvailable = false;
    // @track isRideshareAvailable = false;
    @track worktypesNotSelected = true;
    @track techDuration = 0;
    @track schedTimeSlotsJSON;
    @track earliestStartDate;
    @track bookingStartTime;
    @track bookingEndTime;
    @track createdSuccessfully = false;
    @track isLoanerRequestedForPickup = false;
    @track isOpenStore
    @track isRentalAutoAssigned
    @track vehYearOptions = [];

    isPreferredGuest = false;
    scheduledServiceAppointment;
    dealerBufferTime;
    dealerWaitCapacity;
    prefDate;
    modelYearMap;
    pickupDeliveryResources;
    isUpdate = false;
    currentStep = 'step-1'
    preselectedWorktypes;
    preselectedTransportation;
    scheduledTimeslot;
    oldServiceAppointment;
    authContactsList;
    dealer;
    // dealerTimezoneShift;

    get isAuthorizedContactsExist() {
        return this.authContacts.length;
    }

    // get isTransportationTypeCapacityRequired() {
    //     return this.serviceAppointment.TransportationType == ('Loaner' || 'Rideshare' || 'Wait');
    // }

    get authContactName() {
        let authContact = this.authContacts.find(selection => selection.value === this.selectedAuthContact);
        return authContact ? authContact.label : null;
    }

    get selectedVehName() {
        let selectedVeh = this.vehicles.find(selection => selection.value === this.serviceAppointment.Vehicle);
        return selectedVeh ? selectedVeh.label : null;
    }

    get isLoanerApprovalRequested() {
        return  this.serviceAppointment.RequestLoaner
    }

    get isOpenStoreUsed() {
        return this.isOpenStore || this.serviceAppointment.TransportationType == 'Pickup & Delivery'
    }

    get isModelYearOptionsExist() {
        return this.modelYearMap && this.newVehicle.Year__c && 
            this.modelYearMap.get(this.newVehicle.Year__c) && this.modelYearMap.get(this.newVehicle.Year__c).length > 0
    }
    
    get modelYearOptions() {
        return this.modelYearMap.get(this.newVehicle.Year__c)
    }

    connectedCallback() {
        // window.addEventListener('beforeunload', this.beforeUnloadHandler);
        window.addEventListener('beforeunload', this.beforeUnloadHandler.bind(this));
        getCurrentSObjectType({ recordId: this.recordId })
            .then(result => {
                if (result === 'Account') {
                    getPersonContactInfo({ recordId: this.recordId })
                        .then(result => {
                            if(!result.Dealer_ID__pc) {
                                this.showToastMessage('Error', 'Dealer is not selected for this Account', 'error');
                                this.handleClose();
                            }

                            this.serviceAppointment.ParentId = result.Id;
                            this.serviceAppointment.PreferredAdvisor = result.Preferred_Advisor__pc;
                            this.serviceAppointment.Dealer = result.Dealer_ID__pc;
                            this.serviceAppointment.ContactId = result.PersonContactId;
                            this.serviceAppointment.PreferredDate = new Date().toISOString().slice(0, 10);
                            this.serviceAppointment.DurationInMinutes = result.MinimalAdvisorDuration__c;

                            this.selectedContact = result;
                            this.selectedAuthContact = result.PersonContactId;
                            this.isPreferredGuest = result.PreferredGuest__c;

                            this.prefDate = new Date().toISOString().slice(0, 10);
                        })
                        .then(() => {
                            this.prepareComponent();
                        })

                } else if (result === 'ServiceAppointment') {
                    this.isUpdate = true;
                    getServieAppointmentInfo({ recordId: this.recordId })
                        .then(result => {
                            let firstStep = JSON.parse(result)
                            result = JSON.parse(JSON.parse(firstStep).body)
                            if(!result.Dealer) {
                                this.showToastMessage('Error', 'Dealer is not selected for this Service Appointment', 'error');
                                this.handleClose();
                            }

                            this.recordId = result.ParentId;
                            this.oldServiceAppointment = {...result};
                        })
                        .then(() => getPersonContactInfo({ recordId: this.recordId/*this.serviceAppointment.ParentId*/ }))
                        .then(result => {
                            this.selectedContact = result;
                            this.isPreferredGuest = result.PreferredGuest__c;
                            //this.selectedAuthContact = result.PersonContactId;
                        })
                        .then(() => {
                            this.presetAppointmentValues();
                            this.prepareComponent();
                        })
                }
            })
            .catch(error => {
                console.log('ERROR', error)
            });
    }

    presetAppointmentValues() {
        let result = this.oldServiceAppointment
        this.serviceAppointment.Id = result.Id;
        this.serviceAppointment.ParentId = result.ParentId;
        this.serviceAppointment.PreferredAdvisor = result.SelectedAdvisor;
        this.serviceAppointment.Dealer = result.Dealer;
        this.serviceAppointment.Vehicle = result.Vehicle;
        this.serviceAppointment.ContactId = result.ContactId;
        this.serviceAppointment.OptOutOfConfirmation = result.OptOutOfConfirmation;
        this.serviceAppointment.SelectedAdvisor = result.SelectedAdvisor;
        this.serviceAppointment.AdditionalInformation = result.AdditionalInformation;
        this.serviceAppointment.ServiceSpecials = result.ServiceSpecials;
        this.serviceAppointment.TransportationType = result.TransportationType;
        this.serviceAppointment.SelectedTransportationType = result.TransportationType;
        this.serviceAppointment.TechnicalDuration = result.TechnicalDuration;
        this.serviceAppointment.PreferredDate = result.PreferredDate;
        this.serviceAppointment.BookingStartTime = result.BookingStartTime;
        this.serviceAppointment.BookingEndTime = result.BookingEndTime;
        this.serviceAppointment.RequestLoaner = result.RequestLoaner;

        let wtList = [];
        result.SelectedWorkTypes.forEach(element => {
            let worktype = JSON.parse(element);
            wtList.push(worktype.Id);
        });
        this.serviceAppointment.SelectedWorkTypes = wtList;

        this.selectedAuthContact = result.ContactId;

        this.preselectedWorktypes = result.SelectedWorkTypes;
        this.preselectedTransportation = {
            transportationType: result.TransportationType,
            pickupAddress: {
                PickupStreet: result.PickupStreet,
                PickupCity: result.PickupCity,
                PickupState: result.PickupState,
                PickupZIP: result.PickupZIP,
                PickupComment: result.PickupAddressComments,
                PickupTransportationRequest: result.PickupTransportationRequest
            },
            // requestLoaner: result.RequestLoaner
        };

        this.scheduledTimeslot = {
            SelectedAdvisor: this.serviceAppointment.SelectedAdvisor,
            ScheduledDate: result.PreferredDate,
            ScheduledStartTime: result.BookingStartTime,
            ScheduledEndTime: result.BookingEndTime,
            StartTime: new Date('1970-01-01 ' + result.BookingStartTime + 'Z').getTime()
        };

        this.prefDate = result.PreferredDate;

        this.oldServiceAppointment = { ...this.serviceAppointment };
    }

    prepareComponent() {
        getDealerInfo({ dealer: this.serviceAppointment.Dealer })
            .then(result => {
                this.dealer = result;
                this.dealerBufferTime = result.BufferTime__c ? result.BufferTime__c : 0;
                this.dealerWaitCapacity = result.WaitCapacity__c ? result.WaitCapacity__c : 0;
                this.isOpenStore = result.ScheduleStoreType__c == 'Open Schedule'
                this.isRentalAutoAssigned = result.RentalAutoAssigned__c
            })
            .then(() => getAuthContacts({ recordId: this.recordId }))
            .then(result => {
                let tempOptionsList = [];
                result.forEach(element => {
                    tempOptionsList.push({
                        label: element.Authorized_Contact__r.Name,
                        value: element.Authorized_Contact__r.PersonContactId
                    });
                });
                this.authContacts = tempOptionsList;
                this.authContactsList = result;
            })
            .then(() => getVehiclesInfo({ recordId: this.recordId }))
            .then(result => {
                let tempOptionsList = [];
                result.forEach(element => {
                    tempOptionsList.push({
                        label: element.CDK_Vehicle__r.Name,
                        // label: element.CDK_Vehicle__r.Year__c + ' ' + element.CDK_Vehicle__r.Make__c + ' ' +
                        //     element.CDK_Vehicle__r.Model__c + ' ' + element.CDK_Vehicle__r.VIN__c,
                        value: element.CDK_Vehicle__c
                    });
                });
                tempOptionsList.push({ label: 'New Vehicle', value: '' });
                this.vehicles = tempOptionsList;
            })
            .then(() => getVehicleMakes())
            .then(result => {
                let tempOptionsList = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    tempOptionsList.push({ label: element.label, value: element.value });
                })
                this.vehMakesOptions = tempOptionsList;
                this.newVehicle.Make__c = 'Audi'

                let fileName = 'Scheduler_Audi_Year_To_Model'
                getMakeModelFile({name: fileName}).then(result => {
                    if(result) {
                        this.modelYearMap = this._prepareMakeModelMap(result)
                    }
                })
                .catch(error => 
                    console.log(JSON.stringify(error))
                )
            })
            .then(() => getServiceResources({ dealer: this.serviceAppointment.Dealer }))
            .then(result => {
                let tempResult = JSON.parse(JSON.stringify(result));
                let tempResourcesMap = {};
                this.pickupDeliveryResources = null
                tempResult.forEach(element => {
                    element.DailyCapacity__c = element.DailyCapacity__c ? element.DailyCapacity__c : 10;
                    element.DailyLoanerAvailability__c = element.DailyLoanerAvailability__c
                        ? Math.floor((this.dealer.LoanerCapacity__c * element.DailyLoanerAvailability__c) / 100)
                        : 0;
                    tempResourcesMap[element.Id] = element;
                    element.RideshareCapacity = this.dealer.RideshareCapacity__c;
                    element.RentalCapacity = this.dealer.RentalCapacity__c;
                    element.ExpressCapacity = this.dealer.ExpressCapacity__c;

                    if(element.IsPickUpDeliveryResource__c) {
                        this.pickupDeliveryResources = element.Id
                        // this.pickupDeliveryResources.push(element.Id)
                    }
                });
                this.advisorsMap = tempResourcesMap;
            })
            .then(() => {
                this._prepareVehicleYearOptions()

                if (this.isUpdate) {
                    let selector = "input[value='" + this.selectedAuthContact + "']";
                    this.template.querySelectorAll(selector).forEach(element => {
                        element.checked = true;
                    });

                    if (this.serviceAppointment.SelectedWorkTypes.length) {
                        this.currentStep = 'step-3'
                        this.handleSave()
                    } else {
                        this.currentStep = 'step-2'
                        this.isLoaded = true;
                    }

                    
                } else {
                    this.isLoaded = true;
                }
            })
            // .then(() => getDealerTimezoneShift({dealer: this.serviceAppointment.Dealer}))
            //     .then(result => {
            //         this.dealerTimezoneShift = result
            //     })
            .catch(error => {
                console.log('ERROR', error)
            });
    }

    onloadedHandle(event) {
        this.isLoaded = event.detail;
    }

    handleVehicleChange(event) {
        this.serviceAppointment.Vehicle = event.detail.value;
        this.isAddNewVehicleDisabled = event.detail.value !== '';
    }

    handleAuthContactChange(event) {
        if (this.selectedAuthContact === event.target.value) {
            event.target.checked = false;
            this.serviceAppointment.ContactId = this.selectedContact.PersonContactId;
            this.selectedAuthContact = this.selectedContact.PersonContactId;
        } else {
            event.target.checked = true;
            this.serviceAppointment.ContactId = event.target.value;
            this.selectedAuthContact = event.target.value;
        }
    }

    handleVehicleFieldChange(event) {
        this.newVehicle[event.currentTarget.dataset.fieldname] = event.detail.value;

        if(event.currentTarget.dataset.fieldname == 'Make__c') {
            this.isLoaded = false;
            let fileName = event.detail.value == 'Audi' ? 'Scheduler_Audi_Year_To_Model' : ''
            this.newVehicle.Model__c = null
            getMakeModelFile({name: fileName}).then(result => {
                if(result) {
                    this.modelYearMap = this._prepareMakeModelMap(result)                  
                } else {
                    this.modelYearMap = null
                }
                this.isLoaded = true;
            })
            .catch(error => {
                console.log(JSON.stringify(error))
                this.isLoaded = true;
            })
        }

        if(event.currentTarget.dataset.fieldname == 'Year__c') {
            this.newVehicle.Model__c = null
        }
    }

    handleVehicleMakeChange(event) {
        this.newVehicle.Make__c = event.target.value;
    }

    handleVehicleYearFieldChange(event) {
        let newYear = event.target.value;
        let oldYear = this.newVehicle[event.currentTarget.dataset.fieldname];
        let currentYear = new Date().getFullYear();
        if (this.validateYear(newYear) && newYear <= currentYear) {
            this.newVehicle[event.currentTarget.dataset.fieldname] = newYear;
        }
        else if (oldYear) {
            event.target.value = oldYear;
        } else {
            event.target.value = "";
        }
    }

    validateYear(newYear) {
        return /^\d*$/.test(newYear);
    }

    handleVehicleSave = async () => {
        if (this.serviceAppointment.Vehicle == undefined) {
            this.showToastMessage('Please, choose vehicle', 'Please, choose vehicle', 'error');
            return false;
        }

        if (this.serviceAppointment.Vehicle == '' && !(this.newVehicle.Year__c && this.newVehicle.Make__c && this.newVehicle.Model__c /*&& this.newVehicle.VIN__c*/)) {
            this.showToastMessage('All New Vehicle fields should be filled in', 'All New Vehicle fields should be filled in', 'error');
            return false;
        }

        if (!this.isAddNewVehicleDisabled) {
            let result = await createVehicle({
                newVehicleJSON: JSON.stringify(this.newVehicle),
                accId: this.recordId
            })
            result = JSON.parse(JSON.parse(result))
            try {
                let newVeh = result.body;
                this.serviceAppointment.Vehicle = newVeh.Id;
                this.vehicles.unshift({
                    label: newVeh.Name,
                    value: newVeh.Id
                });
                this.isAddNewVehicleDisabled = true;

                this.newVehicle = { Make__c : 'Audi' };
                return true;
            } catch (error) {
                console.log('error ' + JSON.stringify(error))
                this.showToastMessage('ERROR', newVeh, 'error');
                return false;
            }
        } else {
            return true;
        }

    }

    handleWorktypeSelect(event) {
        let minDurationForLoaner = this.dealer && this.dealer.DurationForLoaner__c ? this.dealer.DurationForLoaner__c : 0;
        let isMinDurationForLoanerEnabled = this.dealer && minDurationForLoaner > 0;

        this.serviceAppointment.SelectedWorkTypes = event.detail.worktypes;
        this.serviceAppointment.SelectedWorkTypesNames = event.detail.names;
        this.serviceAppointment.TechnicalDuration = event.detail.duration;
        this.techDuration = event.detail.duration;

        let tempTransportatiions = new Set()
        tempTransportatiions = event.detail.availableTransportations;

        if (tempTransportatiions.has('Loaner') && !this.isPreferredGuest) {
            tempTransportatiions.delete('Loaner')
        }

        if (this.isPreferredGuest && isMinDurationForLoanerEnabled && event.detail.duration >= minDurationForLoaner) {
            tempTransportatiions.add('Loaner')
        }

        this.availableTransportations = tempTransportatiions
        this.worktypesNotSelected = event.detail.worktypes.length === 0;

        let todayDate = new Date();
        todayDate.setDate(todayDate.getDate() + event.detail.horizon)
        this.earliestStartDate = todayDate.toISOString().slice(0, 10);
        this.prefDate = this.earliestStartDate;
    }

    handleAdditionalInfoInput(event) {
        this.serviceAppointment[event.currentTarget.dataset.field] = event.detail.value;
    }

    handleTransportationSelect(event) {
        this.serviceAppointment.TransportationType = event.detail.selectedTransportation;
        this.serviceAppointment.RequestLoaner = event.detail.requestLoaner;
        this.serviceAppointment.Express = event.detail.express;

        if (event.detail.pickupAddress) {
            Object.keys(event.detail.pickupAddress).forEach(element => {
                this.serviceAppointment[element] = event.detail.pickupAddress[element];
            });
            this.isLoanerRequestedForPickup = event.detail.pickupAddress.PickupTransportationRequest == 'Loaner Requested';
        } else {
            this.serviceAppointment.PickupStreet = null;
            this.serviceAppointment.PickupCity = null;
            this.serviceAppointment.PickupState = null;
            this.serviceAppointment.PickupZIP = null;
            this.serviceAppointment.PickupAddressComments = null;
            this.serviceAppointment.PickupTransportationRequest = null;
        }
    }

    handleSave = () => {
        if (this.serviceAppointment.SelectedWorkTypes == undefined || this.serviceAppointment.SelectedWorkTypes == 0) {
            this.showToastMessage('Please, choose service', 'Please, choose service', 'error');
            return false;
        }

        if (!this.serviceAppointment.TransportationType && !this.serviceAppointment.RequestLoaner) {
            this.showToastMessage('Please, choose Transportation Type', 'Please, choose Transportation Type', 'error');
            return false;
        }

        if (this.serviceAppointment.TransportationType === 'Pickup & Delivery' && (!this.serviceAppointment.PickupStreet || !this.serviceAppointment.PickupCity
            || !this.serviceAppointment.PickupState || !this.serviceAppointment.PickupZIP)) {
            this.showToastMessage('Please, fill in all Transportation Type fields', 'Please, fill in all Transportation Type fields', 'error');
            return false;
        }

        this.isLoaded = false;

        if (new Date(this.earliestStartDate) > new Date(this.serviceAppointment.PreferredDate)) {
            this.serviceAppointment.BookingStartTime = null;
            this.serviceAppointment.BookingEndTime = null;

            this.scheduledTimeslot = null;
        }

        let tempPrefDate = this.serviceAppointment.PreferredDate;
        this.serviceAppointment.PreferredDate = this.prefDate;

        let JSONsApp = JSON.stringify(this.serviceAppointment);
        this.schedTimeSlotsJSON = null;

        createServiceAppointment({ serviceAppointmentJSON: JSONsApp })
            .then(result => {
                if (result) {
                    this.serviceAppointment.Id = result;
                }
                this.serviceAppointment.PreferredDate = tempPrefDate;
            })
            .then(() => getSchedTimeSlots({
                appointmentId: this.serviceAppointment.Id,
                dealer: this.serviceAppointment.Dealer
            })
                .then(result => {
                    this.schedTimeSlotsJSON = result;
                })
            )
            .catch(error => {
                this.isLoaded = true;
                this.error = error;
            });

        return true;
    }

    handleGetNewTimeslot(event) {
        // this.serviceAppointment.BookingStartTime = null;
        // this.serviceAppointment.BookingEndTime = null;
        // this.serviceAppointment.PreferredAdvisor = event.detail.advisor;
        // this.serviceAppointment.PreferredDate = event.detail.date;
        this.prefDate = event.detail.date;

        this.handleSave();
    }

    handleTimeslotSelected(event) {
        this.serviceAppointment.SelectedAdvisor = event.detail.resourceId;
        this.serviceAppointment.PreferredDate = event.detail.prefDate;

        this.serviceAppointment.BookingStartTime = event.detail.startDate;
        this.serviceAppointment.BookingEndTime = event.detail.endDate;

        // this.serviceAppointment.TransportationType = event.detail.transportationType;
        this.serviceAppointment.SelectedTransportationType = event.detail.transportationType;
        // this.selectedDatetime = event.detail.datetimeLabel
    }

    handleLoanerRequested(event) {
        this.serviceAppointment.RequestLoaner = event.detail
    }

    // selectedDatetime

    handleOptOutOfConfirmationCheck(event) {
        this.serviceAppointment[event.currentTarget.dataset.field] = event.target.checked;
    }

    handleCancel() {
        if (this.serviceAppointment.Id) {
            deleteAppointment({ appointmentId: this.serviceAppointment.Id });
        }
    }

    prepareConfirmation = () => {
        this.isLoaded = false;

        if (!this.serviceAppointment.BookingStartTime || !this.serviceAppointment.BookingEndTime) {
            this.showToastMessage('Timeslot not choosen', 'Please, choose timeslot', 'error');
            this.isLoaded = true;
            return false;
        }

        let authContact = this.authContactsList.find(selection => selection.Authorized_Contact__r.PersonContactId === this.selectedAuthContact);
        let authorizeUserFirstName = this.serviceAppointment.ContactId != this.selectedContact.PersonContactId && authContact
            ? authContact.Authorized_Contact__r.FirstName
            : null;

        this.serviceAppointment.EmailRecepientsName = (this.selectedContact.PersonContactId != this.serviceAppointment.ContactId) && authorizeUserFirstName
            ? this.selectedContact.FirstName + ' & ' + authorizeUserFirstName
            : this.selectedContact.FirstName;

        let JSONsApp = JSON.stringify(this.serviceAppointment);
        scheduleAppointment({
            serviceAppointment: JSONsApp,
            isRestoring: false
        }).then(() => getTransportationNotes({
            appointmentId: this.serviceAppointment.Id
        }))
            .then(result => {

                let scheduledAppointment = {
                    Id: this.serviceAppointment.Id,
                    Owner: this.selectedContact.Name,
                    AuthorizeUser: this.authContactName ? this.authContactName : this.selectedContact.Name,
                    Vehicle: this.selectedVehName,
                    ServiceAdvisor: this.advisorsMap[this.serviceAppointment.SelectedAdvisor].Name,
                    AppointmentDate: this.prepareFinalDateString(this.serviceAppointment.PreferredDate, this.serviceAppointment.BookingStartTime),
                    // AppointmentDate: this.selectedDatetime,
                    // AppointmentDate: this.serviceAppointment.PreferredDate + ' ' + this.serviceAppointment.BookingStartTime,
                    Transportation: this.serviceAppointment.SelectedTransportationType,
                    Address: this.serviceAppointment.TransportationType == 'Pickup & Delivery'
                        ? this.serviceAppointment.PickupStreet + ', ' + this.serviceAppointment.PickupCity + ', ' +
                        this.serviceAppointment.PickupState + ', ' + this.serviceAppointment.PickupZIP
                        : null,
                    OwnerEmail: this.selectedContact.PersonEmail,
                    AuthorizeUserEmail: this.serviceAppointment.ContactId != this.selectedContact.PersonContactId && authContact
                        ? authContact.Authorized_Contact__r.PersonEmail
                        : null,
                    OwnerId: this.selectedContact.PersonContactId,
                    AuthorizeUserId: this.serviceAppointment.ContactId,
                    SelectedServices: this.serviceAppointment.SelectedWorkTypesNames,
                    TransportationNote: result
                };
                this.scheduledServiceAppointment = scheduledAppointment;
                
                // this.isLoaded = true;
            })
            .catch(error => {
                this.showToastMessage('Error', 'Service Appointment not scheduled', 'error');
                this.isLoaded = true;
                return false;
            });

        return true;
    }

    handleEmailPreparation(event) {
        // this.confirmationEmail = {
        //     toAddresses: event.detail.toAddresses,
        //     ccAddresses: event.detail.ccAddresses,
        //     doNotSendEmail: event.detail.doNotSendEmail
        // };

        this.confirmationEmail = event.detail
        this.serviceAppointment.OptOutOfConfirmation = event.detail.doNotSendEmail
        this.serviceAppointment.OwnerEmail = event.detail.ownerAddresses
        this.serviceAppointment.AuthContactEmail = event.detail.authContactAddresses
        this.serviceAppointment.CCAdrresses = event.detail.ccAddresses

        this.isLoaded = true;
    }

    emailIsValid (email) {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
    }

    confirmAppointmentBooking = () => {
        if(!this.emailIsValid(this.confirmationEmail.ownerAddresses)) {
            this.showToastMessage('Error', 'Owner Email Address is not valid', 'error')
            return false
        }

        if(this.confirmationEmail.authContactAddresses && !this.emailIsValid(this.confirmationEmail.authContactAddresses)) {
            this.showToastMessage('Error', 'Authorised Contact Email Address is not valid', 'error')
            return false
        }

        this.isLoaded = false

        // confirmationServiceAppointmentUpdate({
        //     appointmentId: this.serviceAppointment.Id,
        //     doNotSendEmail: this.serviceAppointment.OptOutOfConfirmation,
        // })
        confirmationServiceAppointmentUpdate({
            appointment: JSON.stringify(this.serviceAppointment)
        })
            .then(() => sendAppointmentIntoCDK({
                recordId: this.serviceAppointment.Id,
                actionType: this.isUpdate ? 'update' : 'insert'
            })
                .then(result => {
                    if (result && result !== 'OK') {
                        this.showToastMessage('Warning', result, 'warning');
                    }
                })
            ).then(() => {
                this.createdSuccessfully = true;
            })
            .catch(error => {
                this.createdSuccessfully = false;
                this.showToastMessage('Error', 'Service Appointment not scheduled', 'error');
                this.isLoaded = true;
                return false;
            });

        return true;
    }

    disconnectedCallback() {
        window.removeEventListener('beforeunload', this.beforeUnloadHandler);
        if (!this.createdSuccessfully) {
            this.beforeUnloadHandler();
        }
    }

    beforeUnloadHandler() {
        if (this.isUpdate && !this.createdSuccessfully) {
            let JSONsApp = JSON.stringify(this.oldServiceAppointment);
            scheduleAppointment({
                serviceAppointment: JSONsApp,
                isRestoring: true
            })
            .catch(error => {
                this.isLoaded = true;
                this.error = error;
            });
        }

        if (!this.isUpdate && !this.createdSuccessfully) {
            this.handleCancel();
        }
    }

    viewRecord() {
        this.accountHomePageRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: this.serviceAppointment.Id,
                objectApiName: 'ServiceAppointment',
                actionName: 'view'
            }
        };
        this[NavigationMixin.GenerateUrl](this.accountHomePageRef)
            .then(url => { window.open(url) });
    }

    handleClose() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    showToastMessage(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant,
            })
        );
    }

    prepareFinalDateString(dateString, timeString) {
        let utcDateString = new Date(dateString).toUTCString().slice(0,-13)
        let dayOfWeek = DAYS_OF_WEEK_MAP[utcDateString.slice(0, 3)]
        let day = utcDateString.slice(5,7)
        let month = MONTHS_MAP[utcDateString.slice(8,11)]
        let year = utcDateString.slice(12)

        return dayOfWeek + ", " + month + " " + day + ", " + year + " " + this._formatAMPM(timeString)
    }

    _formatAMPM(timeString) {
        let splits = timeString.split(':');
        let hours = splits[0];
        let minutes = splits[1];
        let ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 && minutes != '00' ? '0'+minutes : minutes;
        let strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    _prepareVehicleYearOptions() {
        let tempOptions = []
        for (let i = new Date().getFullYear() ; i >= START_VEHICLE_YEAR ; i--) {
            tempOptions.push({ label: i.toString(), value: i.toString() })
        }
        this.vehYearOptions = tempOptions
    }

    _prepareMakeModelMap = (input) => {
        let rows = input.split("\r\n")
        let modelMap = new Map()
        for(let i = 1; i < rows.length; i++) {
            let splitedRow = rows[i].split(',')
             if(splitedRow.length === 3){
                let make = splitedRow[0]
                let year = splitedRow[1]
                let model = splitedRow[2]
                if(modelMap.has(year)){
                    modelMap.get(year).push({label:model, value:model})
                } else {
                    modelMap.set(year,[{label:model, value:model}])
                }
            }
        }
        return modelMap
    }
}