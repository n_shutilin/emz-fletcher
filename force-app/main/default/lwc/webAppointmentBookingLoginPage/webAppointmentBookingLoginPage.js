import { LightningElement, track, api } from 'lwc';
import executeCodeSend from '@salesforce/apex/WebAppointmentBookingController.executeCodeSend';
import authorize from '@salesforce/apex/WebAppointmentBookingController.authorize';

export default class WebAppointmentBookingLoginPage extends LightningElement {

    @track recordId;
    @track haveCode = false;
    @track message;
    @track errorMessage;
    @track isLoading = false;
    @track errors = [];
    @track haveAccount = true;

    authorizationCode;
    connectType;
    mainPageURL;
    mainPageURLParams;

    get isAuthorized () {
        return this.recordId
    }

    connectedCallback(){
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        this.mainPageURL = new URL(decodeURIComponent(urlParams.get('currentURL')))
        this.mainPageURLParams = new URLSearchParams(this.mainPageURL.search);
        console.log(this.mainPageURLParams)

        this.recordId = this.mainPageURLParams ? this.mainPageURLParams.get('rescheduling') : null
    }

    handleToggleSwitch(event){
        this.haveCode = !this.haveCode;
    }

    handleAccountToggleSwitch(event){
        this.haveAccount = !this.haveAccount;
    }

    handleCreated(event){
        let id = event.detail.accoundId
        this.haveAccount = true
        this.message = 'A new account was created, you can receive the code now.'
    }

    handleConnectTypeChange(event){
        let oldValue = this.connectType;
        let newValue = event.target.value;
        if(newValue.length > 30) {
            event.target.value = oldValue;
        } else {
            this.connectType = newValue;
        }
    }

    handleCodeEnter(event) {
        let oldValue = this.authorizationCode;
        let newValue = event.target.value;
        if(!isNaN(newValue) && Number.isInteger(+newValue) && newValue > 0 && newValue.length <= 6){
            this.authorizationCode = newValue;
        } else if(newValue.length == 0){
            this.authorizationCode = null;
        } else {
            event.target.value = oldValue;
        }
    }

    executeCodeSend() {
        this.isLoading = true;
        executeCodeSend({
            connectType : this.connectType
        }).then((result) => {
            this.isLoading = false;
            this.showMessage('Code was sent on your email or phone number.')
        });
    }

    handleSend() {
        this.errors = [];
        this.message = '';
        let errors = this.validateCodeReciveData()
        if(!errors.length) {
            this.executeCodeSend();
        } else {
            this.showErrors(errors);
        }
    }

    validateAuthorizeData(){
        this.errorMessage = null;
        let errors = [];
        if(!this.connectType) {
            errors.push({
                field: "connectType",
                message: "* Please enter the phone number or email, that your account connected to."
            })
        }
        if(!this.authorizationCode || this.authorizationCode.length !== 6){
            errors.push({
                field: "authorizationCode",
                message: "* Please enter valid authorization code."
            })
        }
        return errors;
    }

    validateCodeReciveData() {
        this.errorMessage = null;
        let errors = [];
        if(!this.connectType) {
            errors.push({
                field: "connectType",
                message: "* Please enter the phone number or email, that your account connected to."
            })
        }
        return errors;
    }

    handleAuthorize(){
        this.errors = [];
        this.message = '';
        let errors = this.validateAuthorizeData();
        if(!errors.length) {
            this.executeAuthorize();
        } else {
            this.showErrors(errors);
        }
    }

    executeAuthorize() {
        this.isLoading = true;
        authorize({
            code : this.authorizationCode,
            connectType : this.connectType
        }).then((result) => {
            let response = JSON.parse(result);
            if(response.code === "202") {
                this.recordId = response.body;
            } else {
                this.showErrors([{
                    field: "code",
                    message: response.errorMessage
                }]);
            }
            this.isLoading = false;
        })
    }

    showErrors(errors) {
        this.errors = errors;
    }

    showMessage(message) {
        this.message = message;
    }
}