import { LightningElement, track, api } from 'lwc';
import createAccount from '@salesforce/apex/WebAppointmentBookingController.createAccount';

const EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default class WebAppointmentBookingLoginPageAccountForm extends LightningElement {

    @api dealer = 'AUDFJ'

    @track firstName
    @track lastName
    @track zipCode
    @track phone
    @track emailAddress
    @track inProcess = true
    @track errormessage = ''


    get isNotAllFilled () {
        return !(this.firstName && this.lastName && this.zipCode && this.phone && this.emailAddress)
    }

    connectedCallback() {
        this.inProcess = false;
    }

    handleCreateNewAccount(event) {
        this.inProcess = true;
        let dataValidity = this.allDataValidation();
        this.errormessage = '';
        if(!dataValidity.find(validity => !validity.isValid)){
            this.clearValidity(dataValidity.filter(validity => validity.isValid))
            createAccount({
                firstName: this.firstName,
                lastName: this.lastName,
                zipCode: this.zipCode,
                phone: this.phone,
                emailAddress: this.emailAddress,
                dealer: this.dealer
            }).then((result) => {
                let response = JSON.parse(result);
                if(response.code === "202") {
                    this.dispatchSucess(response.body)
                } else {
                    this.errormessage = response.errorMessage;
                }
                this.inProcess = false;
            })
        } else {
            this.reportValidity(dataValidity.filter(validity => !validity.isValid))
            this.inProcess = false;
        }
    }

    dispatchSucess(accountId) {
        this.dispatchEvent(
            new CustomEvent("created", {
                detail: {
                    account: accountId
                }
            })
        );
    }

    handleInput(event) {
        let type = event.currentTarget.dataset.type;
        let newValue = event.target.value
        let oldValue = this.getValueByType(type);
        let validity = this.validateInputByType(type, newValue);
        if(validity.isValid){
            this.setInputByType(type, newValue);
            this.clearValidity([validity])
        } else {
            event.target.value = oldValue
            this.reportValidity([validity])
        }
    }

    getValueByType(type){
        switch (type) {
            case 'firstName':
                return this.firstName;
            case 'lastName':
                return this.lastName;
            case 'zipCode':
                return this.zipCode;
            case 'phone':
                return this.phone;
            case 'emailAddress':
                return this.emailAddress;
        }
    }

    validateInputByType(type, newValue) {
        switch (type) {
            case 'firstName':
                return this.validateFirstName(newValue);
            case 'lastName':
                return this.validateLastName(newValue);
            case 'zipCode':
                return this.validateZipCode(newValue);
            case 'phone':
                return this.validatePhone(newValue);
            case 'emailAddress':
                return this.validateEmailAddress(newValue, true);
        }
    }

    setInputByType(type, newValue) {
        switch (type) {
            case 'firstName':
                this.firstName = newValue;
                break;
            case 'lastName':
                this.lastName = newValue;
                break;
            case 'zipCode':
                this.zipCode = newValue;
                break;
            case 'phone':
                this.phone = newValue;
                break;
            case 'emailAddress':
                this.emailAddress = newValue;
                break;
        }
    }

    allDataValidation = () => {
        return [this.validateFirstName(this.firstName),
                this.validateLastName(this.lastName), 
                this.validateZipCode(this.zipCode), 
                this.validatePhone(this.phone), 
                this.validateEmailAddress(this.emailAddress)]
    }

    validateFirstName = (firstName) => {
        if(firstName.length > 30) {
            return this.getValidityTemplate(false,'firstName',  'Too long')
        }
        return this.getValidityTemplate(true,'firstName')
    }

    validateLastName = (lastName) => {
        if(lastName.length > 30) {
            return this.getValidityTemplate(false,'lastName',  'Too long')
        }
        return this.getValidityTemplate(true,'lastName')
    }

    validateZipCode = (zipCode) => {
        if(isNaN(zipCode)) {
            return this.getValidityTemplate(false,'zipCode',  'Not a number')
        }
        if(zipCode.length > 10) {
            return this.getValidityTemplate(false,'zipCode',  'Too long')
        }
        return this.getValidityTemplate(true,'zipCode')
    }

    validatePhone = (phone) => {
        if(isNaN(phone)) {
            return this.getValidityTemplate(false,'phone', 'Not a number')
        }
        if(phone.length > 20) {
            return this.getValidityTemplate(false,'phone',  'Too long')
        }
        return this.getValidityTemplate(true,'phone')
    }

    validateEmailAddress = (emailAddress, inputValidation = false) => {
        if(!inputValidation && !EMAIL_REGEXP.test(String(emailAddress).toLowerCase())) {
            return this.getValidityTemplate(false,'emailAddress', 'Invalid Email format')
        }
        return this.getValidityTemplate(true,'emailAddress')
    }

    getValidityTemplate = (isValid, type = '', reason = '') => {
        return {
            type: type,
            isValid: isValid,
            reason: reason
        }
    } 

    clearValidity = (values) => {
        values.forEach(value => {
          var ellement = this.template.querySelector("[data-type=" + value.type + "]");
          if (ellement) {
            ellement.setCustomValidity("");
            ellement.reportValidity();
          }
        });
      }
    
    reportValidity = (values) => {
        values.forEach((value) => {
          var ellement = this.template.querySelector("[data-type=" + value.type + "]");
          ellement.setCustomValidity(value.reason);
          ellement.reportValidity();
        });
      }
    
}