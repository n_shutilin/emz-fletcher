import {LightningElement, api, track} from 'lwc';
import getPUDTransportationRequestPicklist from '@salesforce/apex/WebAppointmentBookingController.getPUDTransportationRequestPicklist';
import ShowDealInfo from 'c/showDealInfo';

export default class WebAppointmentBookingTransportationTypeSelection extends LightningElement {
    // @api availableTransportations;
    @api
    get availableTransportations() {
        return this._availableTransportations;
    };
    set availableTransportations(value){
        console.log('~~~~~~~~');
        if (value) {
            this._availableTransportations = value;
            if (!this._availableTransportations.has(this.selectedTransportation) ) {
                this.uncheckAll()
            }
        }
    }
    _availableTransportations = new Set();

    // @api
    // get isLoanerAvailable() {
    //     return this._isLoanerAvailable;
    // };
    // set isLoanerAvailable(value){
    //     this._isLoanerAvailable = value;
    //     if(!this._isLoanerAvailable && this.selectedTransportation === 'Loaner') this.uncheckAll();
    // }
    // _isLoanerAvailable = false;

    // @api 
    // get isRideshareAvailable() {
    //     return this._isRideshareAvailable;
    // };
    // set isRideshareAvailable(value){
    //     this._isRideshareAvailable = value;
    //     if(!this._isRideshareAvailable && this.selectedTransportation === 'Rideshare') this.uncheckAll();
    // }
    // _isRideshareAvailable = false;

    @api 
    get disableAll() {
        return this._disableAll;
    };
    set disableAll(value){
        this._disableAll = value;
        if(this._disableAll) this.uncheckAll();
    }
    _disableAll = false;

    @api 
    get preselectedTransportation() {
        return this._preselectedTransportation;
    }
    set preselectedTransportation(value) {
        if(value) {
            this._preselectedTransportation = value;
            this.selectedTransportation = value.transportationType;
            let selector = 'input[value="' + value.transportationType +'"]';
            this.template.querySelectorAll(selector).forEach(element => {
                element.checked = true;
            });

            if(this.showPickupSection){
                this.pickupAddress = value.pickupAddress
                // Object.keys(value.pickupAddress).forEach(property => {
                //     this[property] = value.pickupAddress[property];
                // });
            }

            this.requestLoaner = value.requestLoaner;

            this.sendSelectedTransportation();
        }
    }
    _preselectedTransportation;

    get shuttleDisabled() {
        return !this.availableTransportations.has('Shuttle') || this.disableAll;
    }
    get rideshareDisabled() {
        // return !this._isRideshareAvailable || this.disableAll;
        return !this.availableTransportations.has('Rideshare') || this.disableAll;
    }
    get loanerDisabled() {
        // return !this._isLoanerAvailable || this.disableAll;
        return !this.availableTransportations.has('Loaner') || this.disableAll;
    }
    get rentalDisabled() {
        return !this.availableTransportations.has('Rental') || this.disableAll;
    }
    get expressDisabled() {
        console.log('333');
        return !this.selectedTransportation || !this.availableTransportations.has('Express') || this.disableAll;
    }

    

    get showPickupSection() {
        return this.selectedTransportation === 'Pickup & Delivery';
    }

    get showLoanerRequestCheckbox() {
        return !this.disableAll && this.loanerDisabled;
    }

    @track selectedTransportation;
    @track isExpressSelected;

    // @track pickupStreet;
    // @track pickupCity;
    // @track pickupState;
    // @track pickupZIP;
    // @track pickupComment;
    // @track pickupTransportationRequest;
    @track requestLoaner = false;
    @track PUDTransportationRequestOptions;

    pickupAddress = {};

    connectedCallback() {
        getPUDTransportationRequestPicklist()
            .then(result => {
                let tempOptionsList = [];
                result = JSON.parse(result);
                result.forEach(element => {
                    tempOptionsList.push({ label: element.label, value: element.value });
                })
                this.PUDTransportationRequestOptions = tempOptionsList;
            })
    }

    handleRadioClick(event) {
        if(event.target.value === this.selectedTransportation) {
            event.target.checked = false;
            this.selectedTransportation = '';
        } else {
            this.selectedTransportation = event.target.value;
        }
        this.sendSelectedTransportation();
    }

    handleExpressClick(event) {
        console.log('express');
        event.target.checked = !this.isExpressSelected;
        this.isExpressSelected = !this.isExpressSelected;
        this.sendSelectedTransportation();
    }

    uncheckAll() {
        this.requestLoaner = false;
        this.template.querySelectorAll('input').forEach(element => {
            element.checked = false;
        });
        this.selectedTransportation = '';
        this.isExpressSelected = false;
        this.sendSelectedTransportation();
    }

    handlePickupAddressChange(event) {
        this[event.target.name] = event.target.value;
        const addressFieldAPIName = event.currentTarget.dataset.apiname;
        this.pickupAddress[addressFieldAPIName] = event.target.value;

        this.sendSelectedTransportation();
    }

    handleLoanerRequestChange(event) {
        this.requestLoaner = event.target.checked;

        this.sendSelectedTransportation();
    }
    
    sendSelectedTransportation() {
        console.log('sendSelectedTransportation');
        let finalDetails = {};

        finalDetails.selectedTransportation = this.selectedTransportation;
        finalDetails.requestLoaner = this.requestLoaner;
        finalDetails.express = this.isExpressSelected;
        
        if(this.selectedTransportation === 'Pickup & Delivery') {
            finalDetails.pickupAddress = this.pickupAddress;
        }

        console.log(JSON.stringify(finalDetails));

        this.dispatchEvent(
            new CustomEvent('transportationselected', { detail: finalDetails })
        );
    }
}