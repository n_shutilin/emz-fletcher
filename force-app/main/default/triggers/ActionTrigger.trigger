trigger ActionTrigger on Action__c (after update, after insert) {
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            ActionTriggerHandler.publishEvent(Trigger.new, Trigger.oldMap); 
        }
        if (Trigger.isInsert) {
            ActionTriggerHandler.publishEvent(Trigger.new, new Map<Id,Action__c>());
        }
    }
}