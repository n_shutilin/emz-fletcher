trigger CDKCustomerTrigger on CDK_Customer__c (before insert) {
    if(trigger.isbefore && trigger.isInsert) {
        if(TriggerSettings__c.getOrgDefaults()!=null && TriggerSettings__c.getOrgDefaults().CDKCustomerTriggerEnabled__c) {
            CDKCustomerTriggerHandler.updateSFUniqueIdentifierField(trigger.new);
        }
    }
}