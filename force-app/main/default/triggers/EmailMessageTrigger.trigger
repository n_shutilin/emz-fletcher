trigger EmailMessageTrigger on EmailMessage (before insert, after insert) {
    if(EmailMessageTriggerHelper.isInsertForLead) {
        System.debug('EmailMessageTriggerHelper.isInsertForLead');
        System.debug(EmailMessageTriggerHelper.isInsertForLead);
        return;
    } else if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            EmailMessageTriggerHelper.setRelatedToIdField(Trigger.new);
        }
    } else if(Trigger.isAfter){
        if(Trigger.isInsert){
            EmailMessageTriggerHelper.insertEmailMessageOpener(Trigger.newMap);
            EmailMessageTriggerHelper.createEmailMessageRelation(Trigger.new);
        }
    }
}