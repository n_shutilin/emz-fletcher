trigger LeadRoundRobinTrigger on Lead (after insert) {
    for(Lead l: Trigger.New){
        RoundRobinController.checkBusinessHours(Trigger.New);
        RoundRobinController.runOnce = false;
    }
}