trigger LeadTrigger on Lead (before insert, before update) {
    if(Trigger.isBefore){
        if (Trigger.isInsert) {
           LockRecordHelper.prepopulateLeadField(Trigger.new);
        } else if (Trigger.isUpdate) {
            LockRecordHelper.preventUpdateLockLeadRecord(Trigger.new);
        }
     }        
}