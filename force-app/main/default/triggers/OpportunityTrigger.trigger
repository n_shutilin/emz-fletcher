trigger OpportunityTrigger on Opportunity (before insert, before update) {
   if(Trigger.isBefore){
      if (Trigger.isInsert) {
         LockRecordHelper.prepopulateOpportunityField(Trigger.new);
      } else if (Trigger.isUpdate) {
         LockRecordHelper.preventUpdateLockOpportunityRecord(Trigger.new);
      }
   }
}