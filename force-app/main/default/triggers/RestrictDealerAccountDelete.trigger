trigger RestrictDealerAccountDelete on Account (before delete)
{
   String userId = UserInfo.getUserId();
   
   List<User> users=[select id,Allow_Dealer_Account_Deletion__c from User where id=:userId];
   
   Map<Id,Schema.RecordTypeInfo> rtMap = Account.sobjectType.getDescribe().getRecordTypeInfosById();


         for (account a : Trigger.old)      
       { 
          if( rtMap.get(a.RecordTypeId).getDeveloperName()=='Dealer' && users[0].Allow_Dealer_Account_Deletion__c==false)
        {
          
             a.addError('You can\'t delete Dealer Account records.');
}
       }
       }