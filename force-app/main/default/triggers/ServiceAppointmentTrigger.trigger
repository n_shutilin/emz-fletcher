trigger ServiceAppointmentTrigger on ServiceAppointment (before insert, before update, after update, after insert) {
    // if(trigger.isbefore && ( trigger.isInsert || trigger.isUpdate )) {
    //     if(TriggerSettings__c.getOrgDefaults()!=null && TriggerSettings__c.getOrgDefaults().ServiceAppointmentTriggerEnabled__c) {
    //         ServiceAppointmentTriggerHandler.updateRequiredFields(trigger.new);
    //     }
    // }
    if(trigger.isAfter && trigger.isUpdate) {
        ServiceAppointmentTriggerHandler.processCDKDelete(Trigger.oldMap, Trigger.newMap);
    }
    if(trigger.isAfter && trigger.isInsert) {
        ServiceAppointmentTriggerHandler.afterInsertHandler(Trigger.new);
    }
}