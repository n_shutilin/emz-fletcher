trigger ServiceTerritoryTrigger on ServiceTerritory (before insert, before update) {
    ServiceTerritoryTriggerHandler handler = new ServiceTerritoryTriggerHandler(Trigger.new, Trigger.oldMap);

    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            handler.handleBeforeInsert();
        }

        if (Trigger.isUpdate) {
            handler.handleBeforeUpdate();
        }
    }
}