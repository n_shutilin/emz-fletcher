trigger TaskTrigger on Task (after update, after insert) {
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            TaskTriggerHandler.publishEvent(Trigger.new, Trigger.oldMap); 
        }
        if (Trigger.isInsert) {
            TaskTriggerHandler.publishEvent(Trigger.new, new Map<Id, Task>());
        }
    }
}